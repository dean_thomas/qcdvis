import matplotlib.pyplot as plt

import re

from os import sys
from os import path
from collections import namedtuple
from os import walk

PolaykovFileRegex = "^polyakov.vol1"

REGEX = "^\s+(\d+)\s+(\d+)\s+(\d+)\s+([-+]?\d+.\d+)"

comp = namedtuple('comp', ['re', 'im'])

def loadVol(filename):
	#   Result is a list of complex values
	result = []
	reTot = 0.0
	imTot = 0.0
	count = 0
	
	print("Loading data from file: %s" %(filename))
	
	with open(filename) as polyakovFile:
		for line in polyakovFile.readlines():
			isMatch = re.match(REGEX, line)
			
			if isMatch:
				x = int(isMatch.group(1))
				y = int(isMatch.group(2))
				z = int(isMatch.group(3))
				
				#   For an SU(2) array we only have the real part of the number;
				#   however, for SU(3) data there is also an imaginary part
				real = float(isMatch.group(4))
				imag = 0.0
				#print("%d %d %d re=%f im=%f" %(x, y, z, real, imag))
				
				reTot += real
				imTot += imag
				count += 1
				
				result.append(comp(re=real, im=imag))
				
	average = comp(re=(reTot/count), im=(imTot/count))
	
	return {'array' : result, 'average' : average}

def main(file):
        if path.exists(file):
                data = loadVol(file)
		
                #   Retreive our data into separate variables
                complexValues = data['array']
                average = data['average']

                plt.figure("Polyakov values and average")
                plt.title(file)

                #   Setup the x axis
                plt.xlim((-1.5, 1.5))
                plt.xlabel('re')

                #   Setup the y axis
                plt.ylim((-1.5, 1.5))
                plt.ylabel(('im'))

                #   Axis lines through origin
                plt.axhline(0, color='black')
                plt.axvline(0, color='black')

                #   Plot the points
                plt.scatter([v.re for v in complexValues], [v.im for v in complexValues])

                #   Plot the average
                plt.scatter(average.re, average.im, s=3.141592 * 100, alpha=0.5)

                return 0
        else:
                print("Please provide a valid polyakov field file")
                sys.exit(255)

main("C:\\Users\\4thFloor\\Desktop\\QCD_test_output\\config.b210k1577mu0700j02s16t32\\conp0005\\cool0020\\polyakov.vol1")
main("C:\\Users\\4thFloor\\Desktop\\QCD_test_output\\config.b210k1577mu0700j02s16t32\\conp0010\\cool0020\\polyakov.vol1")
main("C:\\Users\\4thFloor\\Desktop\\QCD_test_output\\config.b210k1577mu0700j02s16t32\\conp0015\\cool0020\\polyakov.vol1")
main("C:\\Users\\4thFloor\\Desktop\\QCD_test_output\\config.b210k1577mu0700j02s16t32\\conp0020\\cool0020\\polyakov.vol1")
main("C:\\Users\\4thFloor\\Desktop\\QCD_test_output\\config.b210k1577mu0700j02s16t32\\conp0025\\cool0020\\polyakov.vol1")
main("C:\\Users\\4thFloor\\Desktop\\QCD_test_output\\config.b210k1577mu0700j02s16t32\\conp0030\\cool0020\\polyakov.vol1")
main("C:\\Users\\4thFloor\\Desktop\\QCD_test_output\\config.b210k1577mu0700j02s16t32\\conp0035\\cool0020\\polyakov.vol1")
main("C:\\Users\\4thFloor\\Desktop\\QCD_test_output\\config.b210k1577mu0700j02s16t32\\conp0040\\cool0020\\polyakov.vol1")
main("C:\\Users\\4thFloor\\Desktop\\QCD_test_output\\config.b210k1577mu0700j02s16t32\\conp0045\\cool0020\\polyakov.vol1")
main("C:\\Users\\4thFloor\\Desktop\\QCD_test_output\\config.b210k1577mu0700j02s16t32\\conp0050\\cool0020\\polyakov.vol1")
main("C:\\Users\\4thFloor\\Desktop\\QCD_test_output\\config.b210k1577mu0700j02s16t32\\conp0055\\cool0020\\polyakov.vol1")
main("C:\\Users\\4thFloor\\Desktop\\QCD_test_output\\config.b210k1577mu0700j02s16t32\\conp0060\\cool0020\\polyakov.vol1")
main("C:\\Users\\4thFloor\\Desktop\\QCD_test_output\\config.b210k1577mu0700j02s16t32\\conp0065\\cool0020\\polyakov.vol1")
main("C:\\Users\\4thFloor\\Desktop\\QCD_test_output\\config.b210k1577mu0700j02s16t32\\conp0070\\cool0020\\polyakov.vol1")
main("C:\\Users\\4thFloor\\Desktop\\QCD_test_output\\config.b210k1577mu0700j02s16t32\\conp0075\\cool0020\\polyakov.vol1")
main("C:\\Users\\4thFloor\\Desktop\\QCD_test_output\\config.b210k1577mu0700j02s16t32\\conp0080\\cool0020\\polyakov.vol1")
main("C:\\Users\\4thFloor\\Desktop\\QCD_test_output\\config.b210k1577mu0700j02s16t32\\conp0085\\cool0020\\polyakov.vol1")
main("C:\\Users\\4thFloor\\Desktop\\QCD_test_output\\config.b210k1577mu0700j02s16t32\\conp0090\\cool0020\\polyakov.vol1")
main("C:\\Users\\4thFloor\\Desktop\\QCD_test_output\\config.b210k1577mu0700j02s16t32\\conp0095\\cool0020\\polyakov.vol1")
main("C:\\Users\\4thFloor\\Desktop\\QCD_test_output\\config.b210k1577mu0700j02s16t32\\conp0100\\cool0020\\polyakov.vol1")


plt.show()


#if __name__ == "__main__":
#    if (len(sys.argv) == 2):
#        main(sys.argv[1:])
#    else:
#        print("Usage: visualize_polyakov <polyakov.vol1>")
