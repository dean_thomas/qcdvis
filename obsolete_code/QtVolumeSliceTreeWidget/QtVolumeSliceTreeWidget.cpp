#include "QtVolumeSliceTreeWidget.h"

///
/// \brief		Internal handler for user double clicking items
/// \param item
/// \param column
/// \author		Dean
/// \since		15-07-2015
///
void QtVolumeSliceTreeWidget::onItemDoubleClicked(QTreeWidgetItem *item, int column)
{
	//	Attempt to cast to a heightfield3d item
	QtHeightField3dItem *itemT =
			dynamic_cast<QtHeightField3dItem *>(item);

	//	If successful raise an event signal
	if (itemT != nullptr)
	{
		emit OnUserDoubleClickedHeightField3dItem(itemT);
	}
}



///
/// \brief QtVolumeSliceTreeWidget::QtVolumeSliceTreeWidget
/// \param parent
/// \author		Dean
/// \since		09-07-2015
///
QtVolumeSliceTreeWidget::QtVolumeSliceTreeWidget(QWidget *parent)
	: QTreeWidget(parent)
{
	//	Custom double click handler
	connect(this, SIGNAL(itemDoubleClicked(QTreeWidgetItem*,int)),
			this, SLOT(onItemDoubleClicked(QTreeWidgetItem*,int)));

	connect(this, &QtVolumeSliceTreeWidget::itemClicked,
			[&](QTreeWidgetItem *item)
	{
		//	Attempt to cast to a heightfield3d item
		QtHeightField3dItem *itemT =
				dynamic_cast<QtHeightField3dItem *>(item);

		//	If successful raise an event signal
		if (itemT != nullptr)
		{
			emit OnUserLeftClickedHeightField3dItem(itemT);
		}
	});

	//	Root item that all 4D data sets will sit beneth
	m_4dRootItem = new QTreeWidgetItem(this);
	m_4dRootItem->setText(0, "4D data");
}

///
/// \brief QtVolumeSliceTreeWidget::SetHeightField4d
/// \param heightField4d
/// \author		Dean
/// \since		09-07-2015
///
void QtVolumeSliceTreeWidget::SetDataModel(const DataModel& dataModel)
{
	m_dataModel = dataModel;

/*
	for (unsigned long i = 0; i < m_dataModel.GetHeightField4dCount(); ++i)
	{
		HeightField4D *heightField4d = m_dataModel.GetHeightField4d(i);

		m_heightField4dItemArray.push_back(
					new QtHeightField4dItem(heightField4d, m_4dRootItem));
	}
*/
/*
	m_rootItem = new QTreeWidgetItem(this);
	m_rootItem->setText(0, "4d Hypervolume");

	m_volItemXYZ = new QTreeWidgetItem(m_rootItem);
	m_volItemXYZ->setText(0, "XYZ Volume");

	m_volItemXYT = new QTreeWidgetItem(m_rootItem);
	m_volItemXYT->setText(0, "XYT Volume");

	m_volItemXZT = new QTreeWidgetItem(m_rootItem);
	m_volItemXZT->setText(0, "XZT Volume");

	m_volItemYZT = new QTreeWidgetItem(m_rootItem);
	m_volItemYZT->setText(0, "YZT Volume");
*/

	Rebuild();
}

///
/// \brief QtVolumeSliceTreeWidget::Rebuild
/// \author		Dean
/// \since		09-07-2015
///
void QtVolumeSliceTreeWidget::Rebuild()
{
	//	Add 4D heightfields
	for (unsigned long i = 0; i < m_dataModel.GetHeightField4dCount(); ++i)
	{
		HeightField4D *heightField4d = m_dataModel.GetHeightField4d(i);

		unsigned long xDim = heightField4d->GetDimX();
		unsigned long yDim = heightField4d->GetDimY();
		unsigned long zDim = heightField4d->GetDimZ();
		unsigned long tDim = heightField4d->GetDimW();

		//	See if the root node exists for this space-time configuration
		//	If not create it
		QtVolumeCategory4dItem *hvolumeRoot = findHeightField4dCategoryItem(
					xDim, yDim, zDim, tDim);
		if (hvolumeRoot == nullptr)
		{
			hvolumeRoot = new QtVolumeCategory4dItem(xDim, yDim, zDim, tDim, m_4dRootItem);
			m_volumeCategory4dArray.push_back(hvolumeRoot);
		}

		//	Hack: for presentation hardcode default colours
		size_t colourID = 0;
		const QColor defaultColours[4] = { QColor(000, 000, 000),
										   QColor(000, 000, 153),
										   QColor(000, 000, 255),
										   QColor(000, 153, 255)};


		//	Check if the item already exists in the list
		//	If not create it
		QtHeightField4dItem *heightField4dItem =
				findHeightField4dItem(heightField4d);
		if (heightField4dItem == nullptr)
		{


			heightField4dItem =
					new QtHeightField4dItem(heightField4d,
											defaultColours[i],
											hvolumeRoot);

			m_heightField4dItemArray.push_back(heightField4dItem);

			//	Hack: for presentation hardcode default colours
			++colourID;
			if (colourID > 3) colourID = 0;
		}

		//	Tell the item to update it's own hierarchy
		heightField4dItem->Rebuild();
	}
}



///
/// \brief QtVolumeSliceTreeWidget::~QtVolumeSliceTreeWidget
/// \author		Dean
/// \since		09-07-2015
///
QtVolumeSliceTreeWidget::~QtVolumeSliceTreeWidget()
{
	delete m_4dRootItem;
}

QtVolumeCategory4dItem *QtVolumeSliceTreeWidget::findHeightField4dCategoryItem(
		const unsigned long &xDim,
		const unsigned long &yDim,
		const unsigned long &zDim,
		const unsigned long &tDim) const
{
	for (unsigned long i = 0; i < m_volumeCategory4dArray.size(); ++i)
	{
		if (m_volumeCategory4dArray[i]->DimensionsMatch(xDim, yDim, zDim, tDim))
		{
			return m_volumeCategory4dArray[i];
		}
	}
	return nullptr;
}

QtHeightField4dItem *QtVolumeSliceTreeWidget::findHeightField4dItem(
		HeightField4D* heightField4d) const
{
	for (unsigned long i = 0; i < m_heightField4dItemArray.size(); ++i)
	{
		if (m_heightField4dItemArray[i]->GetHeightField4d() == heightField4d)
		{
			return m_heightField4dItemArray[i];
		}
	}
	return nullptr;
}


