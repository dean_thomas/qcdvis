#ifndef QTVOLUMECATEGORY4DITEM_H
#define QTVOLUMECATEGORY4DITEM_H

#include <QTreeWidgetItem>

class QtVolumeCategory4dItem : public QTreeWidgetItem
{
	private:
		unsigned long m_xDim;
		unsigned long m_yDim;
		unsigned long m_zDim;
		unsigned long m_tDim;

	public:
		QtVolumeCategory4dItem(const unsigned long xDim,
							   const unsigned long yDim,
							   const unsigned long zDim,
							   const unsigned long tDim,
							   QTreeWidgetItem *parent = 0);

		bool DimensionsMatch(const unsigned long xDim,
							 const unsigned long yDim,
							 const unsigned long zDim,
							 const unsigned long tDim) const;
};

#endif // QTVOLUMECATEGORY4DITEM_H
