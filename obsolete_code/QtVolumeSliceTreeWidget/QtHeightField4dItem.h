#ifndef QT_HEIGHTFIELD_4D_ITEM_H
#define QT_HEIGHTFIELD_4D_ITEM_H

#include "QTreeWidgetItem"
#include <string>
#include <vector>
#include <sstream>

#include <QSize>
#include <QPixmap>
#include <QPainter>
#include <QIcon>
#include <QColor>

using std::vector;
using std::stringstream;
using std::string;

class HeightField4D;

class QtHeightField4dItem : public QTreeWidgetItem
{
		//Q_OBJECT
	private:
		HeightField4D *m_heightField4d = nullptr;

		QColor m_swatchColour;

		QTreeWidgetItem* m_volItemXYT = nullptr;
		QTreeWidgetItem* m_volItemXYZ = nullptr;
		QTreeWidgetItem* m_volItemXZT = nullptr;
		QTreeWidgetItem* m_volItemYZT = nullptr;

		QIcon createSwatchIcon(const QColor &colour) const;

		void addXSlices();
		void addYSlices();
		void addZSlices();
		void addTSlices();

		bool itemExists(const string &itemName) const;

		vector<QTreeWidgetItem*> m_items;

		string generateItemName(const string &varName,
								const unsigned long &varValue) const;

	public:
		explicit QtHeightField4dItem(HeightField4D *heightField4d,
									 const QColor &swatchColor,
									 QTreeWidgetItem *parent = 0);
		~QtHeightField4dItem();

		void Rebuild();

		QColor GetSwatchColour() const { return m_swatchColour; }

		HeightField4D *GetHeightField4d() const { return m_heightField4d; }
};

#endif //	QT_HEIGHTFIELD_4D_ITEM_H
