#include "QtHeightField4dItem.h"
#include "QtHeightField3dItem.h"
#include "HeightField4D.h"
#include "HeightField3D.h"
///
/// \brief QtHeightField4dItem::QtHeightField4dItem
/// \param heightField4d
/// \param parent
/// \author		Dean
/// \since		27-07-2015
///
QtHeightField4dItem::QtHeightField4dItem(HeightField4D *heightField4d,
										 const QColor &swatchColor,
										 QTreeWidgetItem *parent)
	: QTreeWidgetItem(parent)
{
	m_heightField4d = heightField4d;

	//	Caption our new data item
	QString varName = QString::fromStdString(m_heightField4d->GetFieldName());
	QString coolNum = QString("%1").arg(m_heightField4d->GetCoolingSlice());
	QString caption = QString("%1 (Cool: %2)").arg(varName).arg(coolNum);
	setText(0, caption);

	//	Setup storage for sliced 3d data
	m_volItemXYZ = new QTreeWidgetItem(this);
	m_volItemXYZ->setText(0,QString("XYZ Volume - (%1)")
						  .arg(m_heightField4d->GetTSlices().size()));

	m_volItemXYT = new QTreeWidgetItem(this);
	m_volItemXYT->setText(0,QString("XYT Volume - (%1)")
						  .arg(m_heightField4d->GetZSlices().size()));

	m_volItemXZT = new QTreeWidgetItem(this);
	m_volItemXZT->setText(0,QString("XZT Volume - (%1)")
						  .arg(m_heightField4d->GetYSlices().size()));

	m_volItemYZT = new QTreeWidgetItem(this);
	m_volItemYZT->setText(0,QString("YZT Volume - (%1)")
						  .arg(m_heightField4d->GetXSlices().size()));

	//	Create an icon for this item
	m_swatchColour = swatchColor;
	setIcon(0, createSwatchIcon(swatchColor));

	Rebuild();
}

///
/// \brief QtHeightField4dItem::~QtHeightField4dItem
/// \author		Dean
/// \since		27-07-2015
///
QtHeightField4dItem::~QtHeightField4dItem()
{
	while (!m_items.empty())
	{
		delete m_items.back();
		m_items.pop_back();
	}

	delete m_volItemXYZ;
	delete m_volItemXYT;
	delete m_volItemXZT;
	delete m_volItemYZT;
}

///
/// \brief QtHeightField4dItem::Rebuild
/// \author		Dean
/// \since		27-07-2015
///
void QtHeightField4dItem::Rebuild()
{
	addXSlices();
	addYSlices();
	addZSlices();
	addTSlices();
}

///
/// \brief QtVolumeSliceTreeWidget::addXSlices
/// \author		Dean
/// \since		09-07-2015
///
void QtHeightField4dItem::addXSlices()
{
	m_volItemYZT->setText(0,QString("YZT Volume - (%1)")
						  .arg(m_heightField4d->GetXSlices().size()));

	for (unsigned long i = 0; i < m_heightField4d->GetXSlices().size(); ++i)
	{
		HeightField3D *heightField3d = m_heightField4d->GetXSlices()[i];

		if (heightField3d != nullptr)
		{
			string itemText = generateItemName(heightField3d->GetFixedVariableName(),
											   heightField3d->GetFixedVariableValue());
			if (!itemExists(itemText))
			{
				QtHeightField3dItem *newItem
						= new QtHeightField3dItem(heightField3d,
												  m_volItemYZT);
				newItem->setText(0, QString::fromStdString(itemText));

				//	Not sure of a better way to keep a reference to the object than
				//	this?
				m_items.push_back(newItem);
			}
		}
	}
}

///
/// \brief QtVolumeSliceTreeWidget::addYSlices
/// \author		Dean
/// \since		09-07-2015
///
void QtHeightField4dItem::addYSlices()
{
	m_volItemXZT->setText(0,QString("XZT Volume - (%1)")
						  .arg(m_heightField4d->GetYSlices().size()));

	for (unsigned long i = 0; i < m_heightField4d->GetYSlices().size(); ++i)
	{
		HeightField3D *heightField3d = m_heightField4d->GetYSlices()[i];

		if (heightField3d != nullptr)
		{
			string itemText = generateItemName(heightField3d->GetFixedVariableName(),
											   heightField3d->GetFixedVariableValue());
			if (!itemExists(itemText))
			{
				QtHeightField3dItem *newItem
						= new QtHeightField3dItem(heightField3d,
												  m_volItemXZT);
				newItem->setText(0, QString::fromStdString(itemText));

				//	Not sure of a better way to keep a reference to the object than
				//	this?
				m_items.push_back(newItem);
			}
		}
	}
}

///
/// \brief QtVolumeSliceTreeWidget::addZSlices
/// \author		Dean
/// \since		09-07-2015
///
void QtHeightField4dItem::addZSlices()
{
	m_volItemXYT->setText(0,QString("XYT Volume - (%1)")
						  .arg(m_heightField4d->GetZSlices().size()));

	for (unsigned long i = 0; i < m_heightField4d->GetZSlices().size(); ++i)
	{
		HeightField3D *heightField3d = m_heightField4d->GetZSlices()[i];

		if (heightField3d != nullptr)
		{
			string itemText = generateItemName(heightField3d->GetFixedVariableName(),
											   heightField3d->GetFixedVariableValue());
			if (!itemExists(itemText))
			{
				QtHeightField3dItem *newItem
						= new QtHeightField3dItem(heightField3d,
												  m_volItemXYT);
				newItem->setText(0, QString::fromStdString(itemText));

				//	Not sure of a better way to keep a reference to the object than
				//	this?
				m_items.push_back(newItem);
			}
		}
	}
}

///
/// \brief QtVolumeSliceTreeWidget::addTSlices
/// \author		Dean
/// \since		09-07-2015
///
void QtHeightField4dItem::addTSlices()
{
	m_volItemXYZ->setText(0,QString("XYZ Volume - (%1)")
						  .arg(m_heightField4d->GetTSlices().size()));

	for (unsigned long i = 0; i < m_heightField4d->GetTSlices().size(); ++i)
	{
		HeightField3D *heightField3d = m_heightField4d->GetTSlices()[i];

		if (heightField3d != nullptr)
		{
			string itemText = generateItemName(heightField3d->GetFixedVariableName(),
											   heightField3d->GetFixedVariableValue());
			if (!itemExists(itemText))
			{
				QtHeightField3dItem *newItem
						= new QtHeightField3dItem(heightField3d,
												  m_volItemXYZ);
				newItem->setText(0, QString::fromStdString(itemText));

				//	Not sure of a better way to keep a reference to the object than
				//	this?
				m_items.push_back(newItem);
			}
		}
	}
}

///
/// \brief		Checks if an item has already been created
/// \param		itemName
/// \return
/// \since		10-07-2015
/// \author		Dean
///
bool QtHeightField4dItem::itemExists(const string &itemName) const
{
	for (unsigned long i = 0; i < m_items.size(); ++i)
	{
		if (m_items[i]->text(0).toStdString() == itemName)
		{
			return true;
		}
	}
	return false;
}

///
/// \brief QtVolumeSliceTreeWidget::generateItemName
/// \param varName
/// \param varValue
/// \return
/// \author		Dean
/// \since		09-07-2015
///
string QtHeightField4dItem::generateItemName(const string &varName,
											 const unsigned long &varValue) const
{
	stringstream result;

	result << varName << " = " << varValue;

	return result.str();
}

///
/// \brief		Create a colour swatch for the item
/// \param contour
/// \return
/// \author		Dean
/// \since		28-08-2015
///
QIcon QtHeightField4dItem::createSwatchIcon(const QColor &colour) const
{
	QSize iconSize(16, 12);

	QPixmap pixmap(iconSize);
	QPainter painter(&pixmap);

	painter.fillRect(QRect(QPoint(0,0),iconSize), colour);
	painter.drawRect(0, 0,
					 iconSize.width()-1,
					 iconSize.height()-1);

	QIcon result;
	result.addPixmap(pixmap);

	return result;
}
