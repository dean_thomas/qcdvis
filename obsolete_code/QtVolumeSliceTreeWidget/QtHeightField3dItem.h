#ifndef QT_HEIGHTFIELD_3D_ITEM_H
#define QT_HEIGHTFIELD_3D_ITEM_H

#include "QTreeWidgetItem"

class HeightField3D;

class QtHeightField3dItem : public QTreeWidgetItem
{
		//Q_OBJECT
	private:
		HeightField3D *m_heightField3d = nullptr;
	public:
		explicit QtHeightField3dItem(HeightField3D *heightField3d,
											 QTreeWidgetItem *parent = 0);

		HeightField3D *GetHeightField3d() const { return m_heightField3d; }
};

#endif //	QT_HEIGHTFIELD_3D_ITEM_H
