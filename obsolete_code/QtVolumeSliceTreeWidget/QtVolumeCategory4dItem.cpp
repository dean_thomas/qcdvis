#include "QtVolumeCategory4dItem.h"

QtVolumeCategory4dItem::QtVolumeCategory4dItem(
		const unsigned long xDim,
		const unsigned long yDim,
		const unsigned long zDim,
		const unsigned long tDim,
		QTreeWidgetItem *parent) :
	QTreeWidgetItem(parent)
{
	m_xDim = xDim;
	m_yDim = yDim;
	m_zDim = zDim;
	m_tDim = tDim;

	setText(0, QString("%1 x %2 x %3 x %4")
			.arg(xDim).arg(yDim).arg(zDim).arg(tDim));
}

bool QtVolumeCategory4dItem::DimensionsMatch(const unsigned long xDim,
											 const unsigned long yDim,
											 const unsigned long zDim,
											 const unsigned long tDim) const
{
	return ((m_tDim == tDim) && (m_zDim == zDim)
			&& (m_yDim == yDim) && (m_xDim == xDim));
}
