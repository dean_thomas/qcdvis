#ifndef QTVOLUMESLICETREEWIDGET_H
#define QTVOLUMESLICETREEWIDGET_H

#include <QTreeWidget>
#include <QTreeWidgetItem>

#include <string>
#include <sstream>

#include "DataModel.h"
#include "HeightField4D.h"
#include "HeightField3D.h"
#include "QtHeightField3dItem.h"
#include "QtHeightField4dItem.h"
#include "QtVolumeCategory4dItem.h"

using std::string;
using std::stringstream;

class QtVolumeSliceTreeWidget : public QTreeWidget
{
		Q_OBJECT
	private:
		DataModel m_dataModel;

		vector<QtVolumeCategory4dItem *> m_volumeCategory4dArray;
		vector<QtHeightField4dItem *> m_heightField4dItemArray;

		QTreeWidgetItem *m_4dRootItem = nullptr;

		/*
		QTreeWidgetItem *m_rootItem = nullptr;
		QTreeWidgetItem *m_volItemXYT = nullptr;
		QTreeWidgetItem *m_volItemXYZ = nullptr;
		QTreeWidgetItem *m_volItemXZT = nullptr;
		QTreeWidgetItem *m_volItemYZT = nullptr;
*/

		QtVolumeCategory4dItem *findHeightField4dCategoryItem(
				const unsigned long &xDim,
				const unsigned long &yDim,
				const unsigned long &zDim,
				const unsigned long &tDim) const;
		QtHeightField4dItem *findHeightField4dItem(HeightField4D* heightField4d) const;

	public:
		QtVolumeSliceTreeWidget(QWidget *parent = 0);
		~QtVolumeSliceTreeWidget();

		void SetDataModel(const DataModel& dataModel);

		void Rebuild();

	signals:
		void OnUserDoubleClickedHeightField3dItem(QtHeightField3dItem *item);

		void OnUserLeftClickedHeightField3dItem(QtHeightField3dItem *item);
	private slots:
		void onItemDoubleClicked(QTreeWidgetItem *item, int column);
};

#endif // QTVOLUMESLICETREEWIDGET_H
