from subprocess import call
from collections import namedtuple
from os import listdir
from os.path import isfile, join
from os.path import exists
from os import makedirs
import sys
import re
from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *

#	Enable this and the command line will NOT be executed
#TESTING = True

#IN_DIR = 'E:/Dean/JCN study/config.b210k1577mu0450j02s16t32/conp0025/cool0034/cache/'


LOG_TO_FILE = True

LAYOUT = "mmmexamplenice"


#	Struct of packed input file command
InputData = namedtuple('InputData', 'file identifier slice slab_size')

def getListOfFields(dir, extractFields, slabSizes):
	result = []
	
	files = [f for f in listdir(dir) if isfile(join(dir, f))]
	
	#	Hacky work around for now
	extractField1 = extractFields[0]
	extractField2 = ''
	extractField3 = ''
	slabSize1 = slabSizes[0]
	if (len(extractFields) == 2) and (len(slabSizes) == 2):
		extractField2 = extractFields[1]
		slabSize2 = slabSizes[1]
	elif (len(extractFields) == 3) and (len(slabSizes) == 3):
		extractField2 = extractFields[1]
		slabSize2 = slabSizes[1]
		extractField3 = extractFields[2]
		slabSize3 = slabSizes[2]
	
	
	for file in files:
		jcnFields = []
		
		#	Full path to input field
		path = dir + '/' + file
		
		matches = re.search('^([a-zA-Z0-9\ ]+)_([xyzt]?)=(\d+)', file)
		#matches = re.search('^([a-zA-Z0-9\ ]+)_([xyzt=\d]+)', file)
		#print("%s\n%s\n%s\n%s" %(matches.group(0), matches.group(1), matches.group(2), matches.group(3)))
		
		#	Extract data from filename
		filename1 = matches.group(0)		#	filename without extension
		field1 = matches.group(1)		#	field var
		domain1 = matches.group(2)		#	x, y, z or t
		domainVar1 = matches.group(3)	#	value of x/y/z/t
		
		if (field1 == extractField1) or (extractField1 == ''):
			#print("%s" %(path))
		
			#	Just a single field per JCN (for now)
			jcnFields.append(InputData(file=path, identifier=filename1, slice=domain1+'='+domainVar1,slab_size=slabSize1))
		
			if (extractField2 != ''):
				for file2 in files:
					#	Full path to input field
					path2 = dir + file2
		
					matches2 = re.search('^([a-zA-Z0-9\ ]+)_([xyzt]?)=(\d+)', file2)
					#matches = re.search('^([a-zA-Z0-9\ ]+)_([xyzt=\d]+)', file)
					#print("%s\n%s\n%s\n%s" %(matches.group(0), matches.group(1), matches.group(2), matches.group(3)))
		
					#	Extract data from filename
					filename2 = matches2.group(0)		#	filename without extension
					field2 = matches2.group(1)		#	field var
					domain2 = matches2.group(2)		#	x, y, z or t
					domainVar2 = matches2.group(3)	#	value of x/y/z/t
		
					if ((field2 == extractField2) and (domain1 == domain2) and (domainVar1 == domainVar2)):
						#print("Matched secnod field var: %s-%s  %s-%s %s-%s" %(field1, field2, domain1, domain2, domainVar1, domainVar2))
						#	ie second, third field on same JCN...
						jcnFields.append(InputData(file=path2, identifier=filename2, slice=domain2+'='+domainVar2, slab_size=slabSize2))
						#	jcnFields.append(InputData(file=path, identifier=IN_FIELD, slab_size=slabSize))
			
						if (extractField3 != ''):
							for file3 in files:
								#	Full path to input field
								path3 = dir + file3
		
								matches3 = re.search('^([a-zA-Z0-9\ ]+)_([xyzt]?)=(\d+)', file3)
							
								#matches = re.search('^([a-zA-Z0-9\ ]+)_([xyzt=\d]+)', file)
								#print("%s\n%s\n%s\n%s" %(matches.group(0), matches.group(1), matches.group(2), matches.group(3)))
		
								#	Extract data from filename
								filename3 = matches3.group(0)		#	filename without extension
								field3 = matches3.group(1)		#	field var
								domain3 = matches3.group(2)		#	x, y, z or t
								domainVar3 = matches3.group(3)	#	value of x/y/z/t
		
								if ((field3 == extractField3) and (domain3 == domain2) and (domainVar3 == domainVar2)):
									#print("Matched third field var: %s-%s-%s  %s-%s-%s %s-%s-%s" %(field1, field2, field3, domain1, domain2, domain3, domainVar1, domainVar2, domainVar3))
							
									#	ie second, third field on same JCN...
									jcnFields.append(InputData(file=path3, identifier=filename3, slice=domain3+'='+domainVar3,slab_size=slabSize3))
									#	jcnFields.append(InputData(file=path, identifier=IN_FIELD, slab_size=slabSize))
			
			#	Array of array of tuples
			result.append(jcnFields)
	return result
	
def computeMultipleJCNs(jcnCommand, inputData, output_dir, extractFields, forwardParameters):
	#	Hacky work around for now
	fieldVar1 = extractFields[0]
	fieldVar2 = ''
	fieldVar3 = ''
	if len(extractFields) == 2:
		fieldVar2 = extractFields[1]
	elif len(extractFields) == 3:
		fieldVar2 = extractFields[1]
		fieldVar3 = extractFields[2]
		
	#	Make sure that the command line program has somewhere
	#	to write to
	if not exists(output_dir):
		makedirs(output_dir)
	
	for i in range(0, len(inputData)):
		#	Create the list of arguments to call
		cmdLine = []

		#	Executable
		cmdLine.append(jcnCommand)

		#	Extract the current JCN generator configuration
		currentJCN = inputData[i]

		#	Create the output name		
		outputFilename = fieldVar1
		if fieldVar2 != '':
			outputFilename += '_' + fieldVar2
		if fieldVar3 != '':
			outputFilename += '_' + fieldVar3
		#	slice number ('x=10', 't=5' etc.)
		outputFilename += '_' + currentJCN[0].slice
		
		#	Allow custom layouts
		if 'LAYOUT' in globals():
			cmdLine.append('-layout')
			cmdLine.append(LAYOUT)

		#	We'll want this for automation
		cmdLine.append('-jcnJpg')
		cmdLine.append(outputFilename + '.jpg')
		cmdLine.append('-jcnDot')
		cmdLine.append(outputFilename + '.dot')
		cmdLine.append('-nogui')
		cmdLine.append('-reebDot')
		cmdLine.append('-reebRb')		
		cmdLine.append('-outdir')
		cmdLine.append(output_dir)
		
		#	Forwarded parameters
		for p in forwardParameters:
			cmdLine.append(p)
		
		for j in range(0, len(currentJCN)):
			#	Each input field in the current JCN
			field = currentJCN[j]
			
			#	Add to execution command
			cmdLine.append('-input')
			cmdLine.append(field.file)
			cmdLine.append(field.identifier)
			cmdLine.append(str(field.slab_size))

			
			#	Underscore between fieldnames
			#if (j > 0):
			#	outputFilename += '_'
			#	outputFilename2 += '_'
			
			#	Not sure why, but the output filename seems to list the fields in reverse...
			#outputFilename2 += field.identifier + '(' + str(field.slab_size) +  ')'
			#outputFilename += currentJCN[len(currentJCN)-1 -j].identifier + '(' + str(currentJCN[len(currentJCN)-1 -j].slab_size) +  ')'
		
		if 'LOG_TO_FILE' in globals():
			#	Output directed to files
			stdoutFile = open((output_dir + '\\' + outputFilename + '_stdout.txt'), 'w')
			stderrFile = open((output_dir + '\\' + outputFilename + '_stderr.txt'), 'w')		
		
			print('Executing command: %s' %(cmdLine))
			call(cmdLine, stdout=stdoutFile, stderr=stderrFile)
		
			stdoutFile.close()
			stderrFile.close()
		else:
			#	Output will be displayed on screen
			call(cmdLine)
			
		#	Check if the file has been previously calculated
		#outputFilename += '.jpg'
		#outputFilename2 += '.jpg'
		#fileExists = (exists(outputFilename) or exists(outputFilename2))
		#print('File "%s" was found: %s' %(outputFilename, fileExists))
		
		#if not fileExists:
		#	if 'TESTING' not in globals():
				#	Print for debugging
		#		print ("Executing command: %s" %(cmdLine))
				
		#else:
		#	print('Skipped JCN computation')
	return
	
if __name__ == "__main__":
	app = QApplication(sys.argv)
	argv = sys.argv
	
	#	Extract the required fields and slab sizes
	fields = []
	slabSizes = []
	
	#	Allow the input dir to be scripted
	if '-in' in argv:
		#	Find the index of the 'in' flag
		index = argv.index('-in')
		
		#	input dir should be the next parameter
		in_dir = argv[index + 1]
		
		#	Remove the flags
		argv.remove('-in')
		argv.remove(in_dir)
	else:
	#	Ask the user via GUI
		in_dir = QFileDialog.getExistingDirectory(None, "Choose input directory")
		
	#	Allow the output dir to be scripted
	if '-out' in argv:
		#	Find the index of the 'out' flag
		index = argv.index('-out')
		
		#	output dir should be the next parameter
		out_dir = argv[index + 1]
		
		#	Remove the flags
		argv.remove('-out')
		argv.remove(out_dir)
	else:
		#	Ask the user via GUI
		out_dir = QFileDialog.getExistingDirectory(None, "Choose output directory")
	
	#	Allow specification of the JCN command executable
	if '-exe' in argv:
		#	Find the index of the 'out' flag
		index = argv.index('-exe')
		
		#	output dir should be the next parameter
		JCN_COMMAND = argv[index + 1]
		
		#	Remove the flags
		argv.remove('-exe')
		argv.remove(JCN_COMMAND)
		
		print('Reeb graph executable: %s' %(JCN_COMMAND))
	else:
		#	Default location
		JCN_COMMAND = 'c:/users/4thFloor/documents/qt/qtFlexibleIsosurfaces/vtkReebGraphTest/x64/Debug/vtkReebGraphTest.exe'
	
	
	#	Input fields (working from left to right)
	while '-in' in argv:
		#	Find the index of the first 'in' flag
		index = argv.index('-in')
		
		#	input field should be the next parameter
		in_field = argv[index + 1]
		fields.append(in_field)
		
		#	input slab size should be the next parameter
		in_slabSize = argv[index + 2]
		slabSizes.append(in_slabSize)
		
		#	Remove the flags
		argv.remove('-in')
		argv.remove(in_field)
		argv.remove(in_slabSize)
	
	#	Any other parameters (apart from the script name are forwarded to 
	#	the JCN program)
	forwardParameters = argv[1:]
	
	#	Run the program forwarding the appropriate fields and slab sizes
	if len(fields) in  [1, 2, 3]:
		#	1 field requested
		jcnList = getListOfFields(in_dir, fields, slabSizes)
		computeMultipleJCNs(JCN_COMMAND, jcnList, out_dir, fields, forwardParameters)
	##elif len(fields) == 2:
	#	#	2 fields requested
	#	jcnList = getListOfFields(in_dir, fields[1], slabSizes[0], fields[1], slabSizes[1])
	#	computeMultipleJCNs(JCN_COMMAND, jcnList, out_dir, fields[0], fields[1])
	#elif len(fields) == 3:
		#	3 fields requested
	#	jcnList = getListOfFields(in_dir, fields[0], slabSizes[0], fields[1], slabSizes[1], fields[2], slabSizes[2])
	#	computeMultipleJCNs(JCN_COMMAND, jcnList, out_dir, fields[0], fields[1], fields[2])
	else:
		print("Usage: py -3 [-exe <exe path>] jcn.py <field var 1> <slab size 1> [field var 2] [slab size 2] [field var 3] [slab size 3] [-in <input directory>] [-out <output directory>] [forwarded parameters]")
		print("  Note: number of input fields currently limited to 3.")
