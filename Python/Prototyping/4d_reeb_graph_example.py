"""
#######################################################################
 
 Copyright (C) 2016, Joshua A. Levine
 Clemson University
 
 Permission is hereby granted, free of charge, to any person obtaining
 a copy of this software and associated documentation files (the
 "Software"), to deal in the Software without restriction, including
 without limitation the rights to use, copy, modify, merge, publish,
 distribute, sublicense, and/or sell copies of the Software, and to
 permit persons to whom the Software is furnished to do so, subject to
 the following conditions:
 
 The above copyright notice and this permission notice shall be
 included in all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 
 
#######################################################################
"""

#######################################################################
#
# Sample VTK code to experiment with the vtkReebGraph object
# Author: Joshua A. Levine
# Email: levinej@clemson.edu
# Date: Feb. 22, 2016
#
# This code builds a sample 2 triangulated grid with random values
# It computes a vtkReebGraph, and then displays it using a
# vtkGraphLayout.  It's also a simple example of using vtkLookupTable
# and vtkColorTransferFunction and multiple render windows
#
# Some "helpful" comments begin with the phrase "USER:"
#
#######################################################################


import vtk
import random
from math import cos, sin, pi

# USER: size of the grid controlled here
x = 50
y = 50

# USER: vtkReebGraph's Build() function will rely on a vtkPolyData that
#  specifically only has triangles.  vtkUnstructuredGrid could be used
#  for tetrahedra.  
#  See http://www.vtk.org/doc/nightly/html/classvtkReebGraph.html#details
grid = vtk.vtkPolyData()

points = vtk.vtkPoints()

data = vtk.vtkFloatArray()
data.SetNumberOfComponents(1)
data.SetName("Function Value")

#   We could define an additional array to store our d-dimensional position tuples
d = 4
pos_4 = vtk.vtkFloatArray()
pos_4.SetNumberOfComponents(d)
pos_4.SetName("Position")
for x in range(1,50):
    for y in range(1, 50):
        for z in range(1, 1):
            for t in range(1, 1):
                pos_4.InsertNextValue(x, y, z, t)

points.SetNumberOfPoints(x*y)
count = 0
min_z = 9e99
max_z = -9e99
for j in range(y) :
   for i in range(x) :
      #z = random.random()
      #z = random.randint(0,10)
      z = cos(i*1.75*pi/x)*cos(j*1.5*pi/y)
      z = z * float(x/3.0*count)/(x*y) #+ sin((i+j)*3.0*pi/y)
      if z < min_z :
         min_z = z
      if z > max_z :
         max_z = z

      x0 = random.randint(0, x)
      y0 = random.randint(0, y)

      # It doesn't seem to matter how they points are laid
      # out geometrically...all that seems to matter is the
      # connectivity between them!
      # points.InsertPoint(count, i, j, z)
      points.InsertPoint(count, x0, y0, z)
      #points.InsertPoint(count, 0, 0, 0)

      data.InsertNextValue(z)
      count = count + 1

grid.SetPoints(points)
grid.GetPointData().SetScalars(data)

#   Now we can add our own d-dimensional positional tuples to the arrays...
grid.GetPointData().AddArray(pos_4)

tris = vtk.vtkCellArray()

for j in range(y-1) :
   for i in range(x-1) :
      tri = vtk.vtkIdList()	
      tri.InsertNextId(j*x+i)
      tri.InsertNextId(j*x+i+1)
      tri.InsertNextId((j+1)*x+i)
      tris.InsertNextCell(tri)
      
      tri = vtk.vtkIdList()
      tri.InsertNextId(j*x+i+1)
      tri.InsertNextId((j+1)*x+i)
      tri.InsertNextId((j+1)*x+i+1)
      tris.InsertNextCell(tri)
   #
#

grid.SetPolys(tris)


# USER: write out the input file, for testing
#writer = vtk.vtkXMLPolyDataWriter()
#writer.SetFileName("test.vtp")
#writer.SetInputData(grid)
#writer.Write()


reeb_graph = vtk.vtkReebGraph()
err = reeb_graph.Build(grid, data)
# USER: uncomment to see Build() error codes
#print err
# USER: uncomment to see stats on the Reeb Graph
print(reeb_graph)




# USER: Reeb Graph does not (seem to) pass the data array around, it
#  only stores the Vertex Ids in the graph that's constructed.  The next
#  few lines add it back for coloring in the vtkGraphLayoutView
data2 = vtk.vtkFloatArray()
data2.SetNumberOfComponents(1)
data2.SetName("Function Value")
data3 = vtk.vtkFloatArray()
data3.SetNumberOfComponents(1)
data3.SetName("Glyph Size")

info = reeb_graph.GetVertexData().GetAbstractArray("Vertex Ids")
for i in range(info.GetNumberOfTuples()) :
   # USER: uncomment the following to see data values in the Reeb graph
   # print info.GetTuple(i), data.GetTuple(int(info.GetTuple(i)[0]))
   data2.InsertNextValue(data.GetTuple(int(info.GetTuple(i)[0]))[0])
   # USER: this controls the scale / size of glyphs in graph
   data3.InsertNextValue(4.0)

reeb_graph.GetVertexData().AddArray(data2)
reeb_graph.GetVertexData().AddArray(data3)



# Set up the graph view and its render window

view = vtk.vtkGraphLayoutView()
view.AddRepresentationFromInput(reeb_graph)
theme = vtk.vtkViewTheme.CreateMellowTheme()
lut = vtk.vtkLookupTable()
lut.SetRange(min_z,max_z)
# "hawaii" color LUT, disabled
#lut.SetHueRange(0.7, 0)
#lut.SetSaturationRange(1.0, 0)
#lut.SetValueRange(0.5, 1.0)
#lut.Build()

# Manually configured CTF, green-to-white-to-orange
ctf = vtk.vtkColorTransferFunction()
ctf.SetColorSpaceToDiverging()
#ctf.AddRGBPoint(0.0, 0.085, 0.532, 0.201)
#ctf.AddRGBPoint(0.5, 0.865, 0.865, 0.865)
#ctf.AddRGBPoint(1.0, 0.677, 0.492, 0.093)
for i in range(6) :
   f = float(i)/5.0
   f2 = f*0.7 + 0.3
   if i%2 == 1 :
      ctf.AddRGBPoint(f, 0.085, f2*0.532, 0.201)
   else :
      ctf.AddRGBPoint(f, f2*0.677, f2*0.492, 0.093)

num_values = 256
lut.SetNumberOfTableValues(num_values)
lut.Build()

for i in range(num_values) :
   rgb = ctf.GetColor(float(i)/num_values)
   lut.SetTableValue(i,rgb[0],rgb[1],rgb[2])

theme.SetPointLookupTable(lut)
view.ApplyViewTheme(theme)

# USER: could color the vertices by their vertex index instead of their
#  function value
#view.SetVertexColorArrayName("Vertex Ids")
view.SetVertexColorArrayName("Function Value")
view.SetColorVertices(True)
#view.SetVertexScalarBarVisibility(True)

view.SetVertexLabelArrayName("Vertex Ids")
view.SetVertexLabelVisibility(True)
view.SetVertexLabelFontSize(20)
view.SetLayoutStrategyToSimple2D()
view.SetGlyphType(vtk.vtkGraphToGlyphs.SQUARE)
view.SetScaledGlyphs(True)
view.SetScalingArrayName("Glyph Size")

view.GetRenderWindow().SetSize(600, 600)
view.ResetCamera()
view.Render()


# Set up the view that draws the polyData

mapper = vtk.vtkPolyDataMapper()
mapper.SetInput(grid)
mapper.SetScalarRange(min_z,max_z)
mapper.SetLookupTable(lut)
bar = vtk.vtkScalarBarActor()
bar.SetLookupTable(mapper.GetLookupTable())
bar.SetTitle(data2.GetName())
actor = vtk.vtkActor()
actor.SetMapper(mapper)
renderer = vtk.vtkRenderer()
renderer.AddActor(actor)
renderer.AddActor2D(bar)
view2 = vtk.vtkRenderWindow()
view2.AddRenderer(renderer)
view2.SetSize(500,500)
renderer.ResetCamera()
view2.Render()
rwi = vtk.vtkRenderWindowInteractor()
rwi.SetInteractorStyle(vtk.vtkInteractorStyleTrackballCamera())
rwi.SetRenderWindow(view2)

view.GetInteractor().Start()
rwi.Start()
