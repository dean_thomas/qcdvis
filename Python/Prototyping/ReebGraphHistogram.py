import re
import matplotlib.pyplot as plt
import numpy as np

FLOAT_REGEX = R'([+-]?\d*\.\d*)'
INTEGER_REGEX = R'([+-]?\d+)'


def load_rbh_file(filename):
    input_file = open(filename)
    lines = input_file.readlines()
    input_file.close()

    input_type = 'None'

    x_vals = []
    y_vals = []

    for line in lines:
        if 'x axis' in line:
            input_type = 'x axis'
        elif 'y axis' in line:
            input_type = 'y axis'

        float_matches = re.search(FLOAT_REGEX, line)
        integer_matches = re.search(INTEGER_REGEX, line)
        if float_matches is not None and input_type == 'x axis':
            x_vals.append(float(float_matches.group(1)))
        elif integer_matches is not None and input_type == 'y axis':
            y_vals.append(int(integer_matches.group(1)))


    print(len(x_vals), len(y_vals))
    assert(len(x_vals) == len(y_vals)+1)

    plt.plot(x_vals[1:], y_vals)


    return


load_rbh_file(R'C:\Users\4thFloor\Desktop\histogram_test10.rbh')
load_rbh_file(R'C:\Users\4thFloor\Desktop\histogram_test11.rbh')
load_rbh_file(R'C:\Users\4thFloor\Desktop\histogram_test12.rbh')
load_rbh_file(R'C:\Users\4thFloor\Desktop\histogram_test13.rbh')
load_rbh_file(R'C:\Users\4thFloor\Desktop\histogram_test14.rbh')
load_rbh_file(R'C:\Users\4thFloor\Desktop\histogram_test15.rbh')
load_rbh_file(R'C:\Users\4thFloor\Desktop\histogram_test16.rbh')
load_rbh_file(R'C:\Users\4thFloor\Desktop\histogram_test17.rbh')
plt.show()