import matplotlib.pyplot as plt
import numpy as np
import re
import sys
from collections import defaultdict
from os import sys
from os import walk
from os import path
from os.path import isdir
from os.path import join

#REGEX_MEMORY_DELTA = R"Process '(.*)' memory stats delta:"
#REGEX_MEMORY_DELTA_PROC_NAME = 1

#REGEX_PROCESS_ID = R"\s+Process id: (\d+)"
#REGEX_PROCESS_ID_PID = 1


REGEX_JCN = R"Joint Contour Net:"
#REGEX_PEAK_WORKING_SET = R"\s+PeakWorkingSetSize: ([-+]?(?:\d*[.])?\d+)B \( ([-+]?(?:\d*[.])?\d+)kB, ([-+]?(?:\d*[.])?\d+)MB, ([-+]?(?:\d*[.])?\d+)GB \)"
#REGEX_PEAK_WORKING_SET_BYTES = 1
#REGEX_PEAK_WORKING_SET_KB = 2
#REGEX_PEAK_WORKING_SET_MB = 3
#REGEX_PEAK_WORKING_SET_GB = 4


REGEX_JCN_VERTICES = R"\s+vertex\ count:\ (\d+)"
REGEX_JCN_VERTEX_COUNT = 1
#REGEX_WORKING_SET = R"\s+WorkingSetSize: ([-+]?(?:\d*[.])?\d+)B \( ([-+]?(?:\d*[.])?\d+)kB, ([-+]?(?:\d*[.])?\d+)MB, ([-+]?(?:\d*[.])?\d+)GB \)"
#REGEX_WORKING_SET_BYTES = 1
#REGEX_WORKING_SET_KB = 2
#REGEX_WORKING_SET_MB = 3
#REGEX_WORKING_SET_GB = 4

REGEX_JCN_EDGES = R"\s+edge\ count:\ (\d+)"
REGEX_JCN_EDGE_COUNT = 1

JCN_VERTEX = 0
JCN_EDGE = 1
JCN_LOOP = 2
#PROC_PID = 1
#PROC_PEAK = 2
#PROC_WORKING = 3

LOG_FILE = "stdout"

#UNITS = 'GB'

def parse_jcn_stats(file_contents, line_num):
    line = file_contents[line_num]

    #   Find memory delta stats
    procNameMatches = re.search(REGEX_JCN, line)
    if procNameMatches is not None:
        #   Advance to next line, this should list the number of vertices in the MDRG
        line = file_contents[line_num + 1]
        mdrgVerticesMatches = re.search(REGEX_JCN_VERTICES, line)
        if mdrgVerticesMatches is not None:
            mdrgVertexCount = int(mdrgVerticesMatches.group(REGEX_JCN_VERTEX_COUNT))
            print('%d' % (mdrgVertexCount))

            #   Advance to next line, this should be the number of edges
            line = file_contents[line_num + 2]
            mdrgEdgesMatches = re.search(REGEX_JCN_EDGES, line)
            if mdrgEdgesMatches is not None:
                mdrgEdgeCount = int(mdrgEdgesMatches.group(REGEX_JCN_EDGE_COUNT))
                print('%d' % (mdrgEdgeCount))

                #   Advance to next line, this should be number of loops
                #line = file_contents[line_num + 3]
                #procWorkingMatches = re.search(REGEX_WORKING_SET, line)
                #if procPeakMatches is not None:
                #    processWorking = float(procWorkingMatches.group(REGEX_WORKING_SET_GB))
                #    print('%f' % (processWorking))

                return (mdrgVertexCount, mdrgEdgeCount)

    return None

def parseFile(filename):

    #   Read contents of the file
    file = open(filename, 'r')
    file_contents = file.read().splitlines()
    file.close()

    #   Store each array of values based upon the process it represents
    #memDeltaDictionary = defaultdict(list)

    #   Loop over file contents
    for line_num in range(0, len(file_contents)):
        # print("%s" %(line))

        #   Extract memory delta (if possible from the current line)
        mdrg_stats = parse_jcn_stats(file_contents, line_num)
        if mdrg_stats is not None:
            print(mdrg_stats)
            return (mdrg_stats)
            #memDeltaDictionary[mdrg_stats[PROC_NAME]].append(mdrg_stats)

    return None


def plot(euler_charcteristic_array, slab_size_array, fmt='-o'):
    '''
    Plots a data series on the graph
    :param process_dict_averages: contains an array of dictionaries; the outer array aligns with slab sizes,
    the dictionary contains the averages stored against function name keys
    :param process_dict_sds: contains an array of dictionaries; the outer array aligns with slab sizes,
    the dictionary contains the standard deviations stored against function name keys
    :param slab_sizes: a list of slab sizes to go along the x axis
    :param fmt:
    :return: nothing
    '''

    ANNOTATION_MIN = 1 # GB

    #print(process_dict_averages)

    FUNCTION_NAME = 'main'

    #main_array_averages = []
    main_array_standard_deviations = []

    #for slab_data in process_dict_averages:
    #    for proc_key, ave in slab_data.items():
    #        print(proc_key)

    #        if proc_key == FUNCTION_NAME:
    #            main_array_averages.append(ave)

    #for slab_data in process_dict_sds:
    #    for proc_key, sd in slab_data.items():
    #        print(proc_key)

    #        if proc_key == FUNCTION_NAME:
    #            main_array_standard_deviations.append(sd)

    #for processName, record in process_dict_averages.items():
    #plt.figure()
    plt.title('Number of vertices in Joint Contour Net (JCN)')# - function(' + FUNCTION_NAME + ')')
    plt.yscale('log')
    plt.xlabel('slab size')
    plt.ylabel('Euler characteristic')

    #i = 0
    #for slab_data in process_dict_averages:
    #    for proc_key, ave in slab_data.items():
    #        # Hacky extraction of the relevant slab size
    #        slab_size = slab_sizes[i]
    #        print("-----")
    #        print(slab_data)
    #        print(proc_key)
    #        print(ave)
    #        print(slab_size)
    #        print("-----")
    #        if proc_key == FUNCTION_NAME:
    #            if ave >= ANNOTATION_MIN:
    #                anno= plt.annotate('%.2f %s' %(ave, UNITS), xy=(slab_size, ave),  xycoords='data',
    #                xytext=(1.0 + (5 * slab_size), ave), textcoords='data',
    #                             #bbox=dict(boxstyle="round", fc="white"),
    #        arrowprops=dict(arrowstyle = '->', facecolor='black'),
    #        horizontalalignment='right', verticalalignment='center',)
    #    i += 1

    #print(slab_sizes, main_array_averages)
    #vertex_array = []
    edge_array = []

    print('Euler characteristic arry = %s' %(euler_charcteristic_array))
    print('Slab sizec array = %s' %(slab_size_array))

    plt.plot(slab_size_array, euler_charcteristic_array, fmt)


    return


#def compute_average(stats_array):
    '''
    Computes the average of multiple simulation runs and returns a map of function names to averages and standard
    deviations
    :param stats_array:
    :return:
    '''
    ##   Store each array of values based upon the process it represents
    #peak_dictionary = defaultdict(list)
    #working_dictionary = defaultdict(list)

    #for run in stats_array:
    #    for processName, record in run.items():
    #        for elem in record:
    #            print('elem=' + str(elem))

    #            proc_name = elem[PROC_NAME]
    #            pid = elem[PROC_PID]
    #            peak_memory = elem[PROC_PEAK]
    #            working_memory = elem[PROC_WORKING]

     #           peak_dictionary[proc_name].append(peak_memory)
     #           working_dictionary[proc_name].append(working_memory)


    #print(peak_dictionary)
    #print(working_dictionary)

    #peak_average_dictionary = defaultdict(list)
    #peak_standard_deviation_dictionary = defaultdict(list)

    #peak_total = 0.0
    #for key, value_list in peak_dictionary.items():
#        arr = np.array(value_list)

        # Compute the average for the process
#        peak_average_dictionary[key] = np.mean(arr)

        #   Now the standard deviation
#        peak_standard_deviation_dictionary[key] = np.std(arr)

 #   print("Averages: %s" %(str(peak_average_dictionary)))
  #  print("Standard Deviations: %s" %(str(peak_standard_deviation_dictionary)))

   # return (peak_average_dictionary, peak_standard_deviation_dictionary)


def parse_sub_dir(root_dir, sub_dir):
    '''
    Iterates over all files within a sub-directory representing a fixed slab-size
    :param root_dir:
    :param sub_dir:
    :return:
    '''
    #   Subdir name should be a float representing the slab size
    slab_size = float(sub_dir)

    print("Parsing directory with slab size = %f" %(slab_size))

    # Initialize a list of files in the current directory
    files = []

    # Build absolute path to current dir
    absolute_path = join(root_dir, sub_dir)

    # Obtain a list of (all) files in the target directory
    for (dir_path, dir_names, file_names) in walk(absolute_path):
        for file_name in file_names:
            files.append(join(absolute_path, file_name))
        break

    stats = []
    for file in files:
        print("#############################################################")
        if LOG_FILE in file:
            print(file)

            jcn_stats = parseFile(file)
            print("jcn stats = %s" %(str(jcn_stats)))

            if jcn_stats is not None:
                stats.append(jcn_stats)

        #(average_mem_delta_average, average_mem_delta_sd) = compute_average(stats)

    return (slab_size, jcn_stats)


def parse_root_dir(root_dir, fmt='-o'):
    '''
    Loops over all sub-directories from a root (representing a cooling value)
    :param root_dir:
    :param fmt:
    :return:
    '''
    # Initialize a list of dirs in the current directory
    sub_dirs = []

    # Obtain a list of sub directories (relating to slab sizes) in the target directory
    for (dir_path, dir_names, filenames) in walk(root_dir):
        sub_dirs.extend(dir_names)
        break

    #   List of slab size, the averages for each slab size and the standard deviations for each slab size
    slab_size_array = []
    jcn_stats_array = []
    #standard_deviations = []

    # Process each sub dir (which represents a computation at specified slab size)
    for sub_dir in sub_dirs:
        print(sub_dir)

        (slab_size, mdrg_stats) = parse_sub_dir(root_dir, sub_dir)

        euler_characteristic = 2 + mdrg_stats[JCN_EDGE] - mdrg_stats[JCN_VERTEX]
        print("Euler characteristic = %d" %(euler_characteristic))
        jcn_stats_array.append(euler_characteristic)
        slab_size_array.append(slab_size)
#        averages.append(proc_averages)
#        standard_deviations.append(proc_sds)

    print(slab_size_array)
    plot(jcn_stats_array, slab_size_array, fmt)


def main(args):
    """entry point"""

    parse_root_dir(R"C:\Users\4thFloor\Desktop\mu1000\conp0005\cool0010\fields1", "r-.o")
    # parse_root_dir(R"C:\Users\4thFloor\Desktop\cool0010\fields5", "-.o")
    parse_root_dir(R"C:\Users\4thFloor\Desktop\mu1000\conp0005\cool0010\fields6", "r--o")
    parse_root_dir(R"C:\Users\4thFloor\Desktop\mu1000\conp0005\cool0010\fields12", "r-o")

    parse_root_dir(R"C:\Users\4thFloor\Desktop\mu1000\conp0005\cool0015\fields1", "b-.D")
    # parse_root_dir(R"C:\Users\4thFloor\Desktop\cool0015\fields5", "-.D")
    parse_root_dir(R"C:\Users\4thFloor\Desktop\mu1000\conp0005\cool0015\fields6", "b--D")
    parse_root_dir(R"C:\Users\4thFloor\Desktop\mu1000\conp0005\cool0015\fields12", "b-D")

    parse_root_dir(R"C:\Users\4thFloor\Desktop\mu1000\conp0005\cool0020\fields1", "k-.^")
    # parse_root_dir(R"C:\Users\4thFloor\Desktop\cool0020\fields5", "-.^")
    parse_root_dir(R"C:\Users\4thFloor\Desktop\mu1000\conp0005\cool0020\fields6", "k--^")
    parse_root_dir(R"C:\Users\4thFloor\Desktop\mu1000\conp0005\cool0020\fields12", "k-^")

    plt.legend(
        ["10 cools, 1 timestep", "10 cools, 6 timesteps", "10 cools, 12 timesteps",
         "15 cools, 1 timestep", "15 cools, 6 timesteps", "15 cools, 12 timesteps",
         "20 cools, 1 timestep", "20 cools, 6 timesteps", "20 cools, 12 timesteps"])
    plt.show()


    # if memDeltaArray is not None:
    #    print(memDeltaArray)
    #    plotMemoryUsage(memDeltaArray, 2, 'bytes')

if __name__ == '__main__':
    main(sys.argv[1:])