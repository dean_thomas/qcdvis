"""
Implementation of the algorithms in [1] as Python functions.

[1] C. Weigle and D. C. Banks, “Complex-valued contour meshing,”
    in Proceedings of the 7th conference on Visualization’96, 1996, p. 173–ff.
"""
from numpy import inf


def new_vertex(points):
    if len(points) == 2:
        return Vertex2(points[0], points[1])
    elif len(points) == 3:
        return Vertex3(points[0], points[1], points[2])
    else:
        print('Unsupported action...')


class Vertex2:
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def __str__(self):
        return "(%f, %f)" %(self.x, self.y)


class Vertex3:
    def __init__(self, x, y, z):
        self.x = x
        self.y = y
        self.z = z

    def __str__(self):
        return "(%f, %f, %f)" %(self.x, self.y, self.z)


class Edge:
    def __init__(self, v0, v1):
        self.v0 = v0
        self.v1 = v1

    def __str__(self):
        return "Edge = {%s %s}" %(self.v0, self.v1)


class Square:
    def __init__(self, vertex_list, edge_list):
        self.vertex_list = vertex_list
        self.edge_list = edge_list

    def __str__(self):
        str = "Square = {\n"

        for e in self.edge_list:
            str += "\t%s\n" % e

        str += '\tCentre at %s\n' % self.midpoint()

        str += "}"
        return str

    def midpoint(self):
        X = 0
        Y = 1
        Z = 2
        T = 3

        if isinstance(self.vertex_list[0], Vertex2):
            tuple_dim = 2
        elif isinstance(self.vertex_list[0], Vertex3):
            tuple_dim = 3
        else:
            print("Unsupported action")
            tuple_dim = 0

        min_array = [inf] * 2
        max_array = [-inf] * 2

        for v in self.vertex_list:
            if v.x < min_array[X]:
                min_array[X] = v.x
            if v.x > max_array[X]:
                max_array[X] = v.x

            if v.y < min_array[Y]:
                min_array[Y] = v.y
            if v.y > max_array[Y]:
                max_array[Y] = v.y

        midpoint = [0.0] * 2
        for i in range(0, 2):
            midpoint[i] = (max_array[i] - min_array[i]) / 2.0

        if isinstance(self.vertex_list[0], Vertex3):
            midpoint.append(self.vertex_list[0].z)

        return new_vertex(midpoint)


class Cube:
    def __init__(self, vertex_list, square_list):
        self.vertex_list = vertex_list
        self.square_list = square_list

    def __str__(self):
        str = "Cube = {\n"

        for s in self.square_list:
            str += "\tSquare = {\n"

            for e in s.edge_list:
                str += "\t\t%s\n" %e
            str += "\t}\n"

        str += '\tCentre at %s\n' % self.midpoint()

        str += "}"
        return str

    def midpoint(self):
        X = 0
        Y = 1
        Z = 2
        T = 3

        if isinstance(self.vertex_list[0], Vertex2):
            tuple_dim = 2
        elif isinstance(self.vertex_list[0], Vertex3):
            tuple_dim = 3
        else:
            print("Unsupported action")
            tuple_dim = 0

        min_array = [inf] * tuple_dim
        max_array = [-inf] * tuple_dim

        for v in self.vertex_list:
            if v.x < min_array[X]:
                min_array[X] = v.x
            if v.x > max_array[X]:
                max_array[X] = v.x

            if v.y < min_array[Y]:
                min_array[Y] = v.y
            if v.y > max_array[Y]:
                max_array[Y] = v.y

            if isinstance(v, Vertex3):
                if v.z < min_array[Z]:
                    min_array[Z] = v.z
                if v.z > max_array[Z]:
                    max_array[Z] = v.z

        midpoint = [0.0] * tuple_dim
        for i in range(0, tuple_dim):
            midpoint[i] = (max_array[i] - min_array[i]) / 2.0

        return new_vertex(midpoint)


class Simplex:
    def __init__(self, vertex_array):
        self.vertex_array = vertex_array

    def __str__(self):
        result = "%i Simplex = { " % len(self.vertex_array)
        for v in self.vertex_array:
            result += "%s " % v
        result += "}"

        return result


def split_c2(square, simplex=None):
    #   If no list of simplices is passed through we can start construction with a new list
    if simplex is None:
        simplex = [Vertex2] * 3

    simplex[2] = square.midpoint()

    for e in square.edge_list:
        simplex[0] = e.v0
        simplex[1] = e.v1

        new_simplex = Simplex(simplex)
        print(new_simplex)


def split_c3(cube, simplex=None):
    #   If no list of simplices is passed through we can start construction with a new list
    if simplex is None:
        simplex = [Vertex3] * 4

    #   The 3rd vertex is fixed
    simplex[3] = cube.midpoint()

    for s in cube.square_list:
        #   Now split each square face into an n-simplex
        split_c2(s, simplex)


def test_square():
    v0 = Vertex2(0, 0)
    v1 = Vertex2(1, 0)
    v2 = Vertex2(0, 1)
    v3 = Vertex2(1, 1)

    e0 = Edge(v0, v1)
    e1 = Edge(v1, v3)
    e2 = Edge(v3, v2)
    e3 = Edge(v2, v0)

    square = Square([v0, v1, v2, v3], [e0, e1, e2, e3])

    print(square)

    split_c2(square)

    print('Done.')


def test_cube():
    v0 = Vertex3(0, 0, 0)
    v1 = Vertex3(1, 0, 0)
    v2 = Vertex3(0, 1, 0)
    v3 = Vertex3(1, 1, 0)
    v4 = Vertex3(0, 0, 1)
    v5 = Vertex3(1, 0, 1)
    v6 = Vertex3(0, 1, 1)
    v7 = Vertex3(1, 1, 1)

    #   Front face
    e0 = Edge(v0, v1)
    e1 = Edge(v1, v3)
    e2 = Edge(v3, v2)
    e3 = Edge(v2, v0)

    #   Back face
    e4 = Edge(v4, v5)
    e5 = Edge(v5, v7)
    e6 = Edge(v7, v6)
    e7 = Edge(v6, v4)

    #   Side faces
    e8 = Edge(v1, v5)
    e9 = Edge(v3, v7)
    e10 = Edge(v2, v6)
    e11 = Edge(v0, v4)

    #   Define the square faces
    s0 = Square([v0, v1, v2, v3, v4, v5, v6, v7], [e0, e1, e2, e3])
    s1 = Square([v0, v1, v2, v3, v4, v5, v6, v7], [e8, e5, e9, e1])
    s2 = Square([v0, v1, v2, v3, v4, v5, v6, v7], [e1, e5, e6, e7])
    s3 = Square([v0, v1, v2, v3, v4, v5, v6, v7], [e7, e10, e3, e11])
    s4 = Square([v0, v1, v2, v3, v4, v5, v6, v7], [e2, e9, e6, e10])
    s5 = Square([v0, v1, v2, v3, v4, v5, v6, v7], [e0, e8, e1, e11])

    #   Define the cube as the list of vertices and square faces
    cube = Cube([v0, v1, v2, v3, v4, v5, v6, v7], [s0, s1, s2, s3, s4, s5])

    print(cube)

    split_c3(cube)

    print('Done.')

test_square()
test_cube()




