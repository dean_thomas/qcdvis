import matplotlib.pyplot as plt
import numpy as np
import re
import sys
from collections import defaultdict
from os import sys
from os import walk
from os import path
from os.path import isdir
from os.path import join

REGEX_MEMORY_DELTA = R"Process '(.*)' memory stats delta:"
REGEX_MEMORY_DELTA_PROC_NAME = 1

REGEX_PROCESS_ID = R"\s+Process id: (\d+)"
REGEX_PROCESS_ID_PID = 1

REGEX_PEAK_WORKING_SET = R"\s+PeakWorkingSetSize: ([-+]?(?:\d*[.])?\d+)B \( ([-+]?(?:\d*[.])?\d+)kB, ([-+]?(?:\d*[.])?\d+)MB, ([-+]?(?:\d*[.])?\d+)GB \)"
REGEX_PEAK_WORKING_SET_BYTES = 1
REGEX_PEAK_WORKING_SET_KB = 2
REGEX_PEAK_WORKING_SET_MB = 3
REGEX_PEAK_WORKING_SET_GB = 4

REGEX_WORKING_SET = R"\s+WorkingSetSize: ([-+]?(?:\d*[.])?\d+)B \( ([-+]?(?:\d*[.])?\d+)kB, ([-+]?(?:\d*[.])?\d+)MB, ([-+]?(?:\d*[.])?\d+)GB \)"
REGEX_WORKING_SET_BYTES = 1
REGEX_WORKING_SET_KB = 2
REGEX_WORKING_SET_MB = 3
REGEX_WORKING_SET_GB = 4

PROC_NAME = 0
PROC_PID = 1
PROC_PEAK = 2
PROC_WORKING = 3

LOG_FILE = "stdlog"

UNITS = 'GB'

#   Use LaTeX to provide type1 fonts for the graphs
plt.rcParams['ps.useafm'] = True
plt.rcParams['pdf.use14corefonts'] = True
plt.rcParams['text.usetex'] = True

def parseMemoryDelta(file_contents, line_num):
    line = file_contents[line_num]

    #   Find memory delta stats
    procNameMatches = re.search(REGEX_MEMORY_DELTA, line)
    if procNameMatches is not None:
        #   Extract the scalar value
        processName = procNameMatches.group(REGEX_MEMORY_DELTA_PROC_NAME)
        print('%s' % (processName))

        #   Advance to next line, this should list the process id
        line = file_contents[line_num + 1]
        procIdMatches = re.search(REGEX_PROCESS_ID, line)
        if procIdMatches is not None:
            processId = int(procIdMatches.group(REGEX_PROCESS_ID_PID))
            print('%d' % (processId))

            #   Advance to next line, this should be peak working set
            line = file_contents[line_num + 2]
            procPeakMatches = re.search(REGEX_PEAK_WORKING_SET, line)
            if procPeakMatches is not None:
                processPeak = float(procPeakMatches.group(REGEX_PEAK_WORKING_SET_GB))
                print('%f' % (processPeak))

                #   Advance to next line, this should be working set
                line = file_contents[line_num + 3]
                procWorkingMatches = re.search(REGEX_WORKING_SET, line)
                if procPeakMatches is not None:
                    processWorking = float(procWorkingMatches.group(REGEX_WORKING_SET_GB))
                    print('%f' % (processWorking))

                    return (processName, processId, processPeak, processWorking)

    return None

def parseFile(filename):

    #   Read contents of the file
    file = open(filename, 'r')
    file_contents = file.read().splitlines()
    file.close()

    #   Store each array of values based upon the process it represents
    memDeltaDictionary = defaultdict(list)

    #   Loop over file contents
    for line_num in range(0, len(file_contents)):
        # print("%s" %(line))

        #   Extract memory delta (if possible from the current line)
        memDelta = parseMemoryDelta(file_contents, line_num)
        if memDelta is not None:
            print(memDelta)
            memDeltaDictionary[memDelta[PROC_NAME]].append(memDelta)

    return (memDeltaDictionary)


def plot(process_dict_averages, process_dict_sds, slab_sizes, fmt='-o'):
    '''
    Plots a data series on the graph
    :param process_dict_averages: contains an array of dictionaries; the outer array aligns with slab sizes,
    the dictionary contains the averages stored against function name keys
    :param process_dict_sds: contains an array of dictionaries; the outer array aligns with slab sizes,
    the dictionary contains the standard deviations stored against function name keys
    :param slab_sizes: a list of slab sizes to go along the x axis
    :param fmt:
    :return: nothing
    '''

    ANNOTATION_MIN = 1 # GB

    print(process_dict_averages)

    FUNCTION_NAME = 'main'

    main_array_averages = []
    main_array_standard_deviations = []

    for slab_data in process_dict_averages:
        for proc_key, ave in slab_data.items():
            print(proc_key)

            if proc_key == FUNCTION_NAME:
                main_array_averages.append(ave)

    for slab_data in process_dict_sds:
        for proc_key, sd in slab_data.items():
            print(proc_key)

            if proc_key == FUNCTION_NAME:
                main_array_standard_deviations.append(sd)

    #for processName, record in process_dict_averages.items():
    #plt.figure()
    plt.title('peak memory usage')# - function(' + FUNCTION_NAME + ')')
    plt.yscale('log')
    plt.xlabel('slab size')
    plt.ylabel('GigaBytes (GB)')

    i = 0
    for slab_data in process_dict_averages:
        for proc_key, ave in slab_data.items():
            # Hacky extraction of the relevant slab size
            slab_size = slab_sizes[i]
            print("-----")
            print(slab_data)
            print(proc_key)
            print(ave)
            print(slab_size)
            print("-----")
            if proc_key == FUNCTION_NAME:
                if ave >= ANNOTATION_MIN:
                    anno= plt.annotate('%.2f %s' %(ave, UNITS), xy=(slab_size, ave),  xycoords='data',
                    xytext=(1.0 + (5 * slab_size), ave), textcoords='data',
                                 #bbox=dict(boxstyle="round", fc="white"),
            arrowprops=dict(arrowstyle = '->', facecolor='black'),
            horizontalalignment='right', verticalalignment='center',)
        i += 1

    #print(slab_sizes, main_array_averages)
    plt.plot(slab_sizes, main_array_averages, fmt)
    #   plt.errorbar(slab_sizes, main_array_averages, yerr=main_array_standard_deviations, fmt=fmt)

    return


def compute_average(stats_array):
    '''
    Computes the average of multiple simulation runs and returns a map of function names to averages and standard
    deviations
    :param stats_array:
    :return:
    '''
    #   Store each array of values based upon the process it represents
    peak_dictionary = defaultdict(list)
    working_dictionary = defaultdict(list)

    for run in stats_array:
        for processName, record in run.items():
            for elem in record:
                print('elem=' + str(elem))

                proc_name = elem[PROC_NAME]
                pid = elem[PROC_PID]
                peak_memory = elem[PROC_PEAK]
                working_memory = elem[PROC_WORKING]

                peak_dictionary[proc_name].append(peak_memory)
                working_dictionary[proc_name].append(working_memory)


    print(peak_dictionary)
    print(working_dictionary)

    peak_average_dictionary = defaultdict(list)
    peak_standard_deviation_dictionary = defaultdict(list)

    peak_total = 0.0
    for key, value_list in peak_dictionary.items():
        arr = np.array(value_list)

        # Compute the average for the process
        peak_average_dictionary[key] = np.mean(arr)

        #   Now the standard deviation
        peak_standard_deviation_dictionary[key] = np.std(arr)

    print("Averages: %s" %(str(peak_average_dictionary)))
    print("Standard Deviations: %s" %(str(peak_standard_deviation_dictionary)))

    return (peak_average_dictionary, peak_standard_deviation_dictionary)


def parse_sub_dir(root_dir, sub_dir):
    '''
    Iterates over all files within a sub-directory representing a fixed slab-size
    :param root_dir:
    :param sub_dir:
    :return:
    '''
    #   Subdir name should be a float representing the slab size
    slab_size = float(sub_dir)

    print("Parsing directory with slab size = %f" %(slab_size))

    # Initialize a list of files in the current directory
    files = []

    # Build absolute path to current dir
    absolute_path = join(root_dir, sub_dir)

    # Obtain a list of (all) files in the target directory
    for (dir_path, dir_names, file_names) in walk(absolute_path):
        for file_name in file_names:
            files.append(join(absolute_path, file_name))
        break

    stats = []
    for file in files:
        print("#############################################################")
        if LOG_FILE in file:
            print(file)

            (memDeltaArray) = parseFile(file)
            print("mem_delta_array = %s" %(str(memDeltaArray)))

            if memDeltaArray is not None:
                stats.append(memDeltaArray)

        (average_mem_delta_average, average_mem_delta_sd) = compute_average(stats)

    return (slab_size, average_mem_delta_average, average_mem_delta_sd)


def parse_root_dir(root_dir, fmt='-o'):
    '''
    Loops over all sub-directories from a root (representing a cooling value)
    :param root_dir:
    :param fmt:
    :return:
    '''
    # Initialize a list of dirs in the current directory
    sub_dirs = []

    # Obtain a list of sub directories (relating to slab sizes) in the target directory
    for (dir_path, dir_names, filenames) in walk(root_dir):
        sub_dirs.extend(dir_names)
        break

    #   List of slab size, the averages for each slab size and the standard deviations for each slab size
    slab_sizes = []
    averages = []
    standard_deviations = []

    # Process each sub dir (which represents a computation at specified slab size)
    for sub_dir in sub_dirs:
        print(sub_dir)

        (slab_size, proc_averages, proc_sds) = parse_sub_dir(root_dir, sub_dir)

        slab_sizes.append(slab_size)
        averages.append(proc_averages)
        standard_deviations.append(proc_sds)

    print(slab_sizes)
    plot(averages, standard_deviations, slab_sizes, fmt)


def main(args):
    """entry point"""

    parse_root_dir(R"C:\Users\4thFloor\Desktop\cool0010\fields1", "r-.o")
    # parse_root_dir(R"C:\Users\4thFloor\Desktop\cool0010\fields5", "-.o")
    parse_root_dir(R"C:\Users\4thFloor\Desktop\cool0010\fields6", "r--o")
    parse_root_dir(R"C:\Users\4thFloor\Desktop\cool0010\fields12", "r-o")

    parse_root_dir(R"C:\Users\4thFloor\Desktop\cool0015\fields1", "b-.D")
    # parse_root_dir(R"C:\Users\4thFloor\Desktop\cool0015\fields5", "-.D")
    parse_root_dir(R"C:\Users\4thFloor\Desktop\cool0015\fields6", "b--D")
    parse_root_dir(R"C:\Users\4thFloor\Desktop\cool0015\fields12", "b-D")

    parse_root_dir(R"C:\Users\4thFloor\Desktop\cool0020\fields1", "k-.^")
    # parse_root_dir(R"C:\Users\4thFloor\Desktop\cool0020\fields5", "-.^")
    parse_root_dir(R"C:\Users\4thFloor\Desktop\cool0020\fields6", "k--^")
    parse_root_dir(R"C:\Users\4thFloor\Desktop\cool0020\fields12", "k-^")

    plt.legend(
        ["10 cools, 1 timestep", "10 cools, 6 timesteps", "10 cools, 12 timesteps",
         "15 cools, 1 timestep", "15 cools, 6 timesteps", "15 cools, 12 timesteps",
         "20 cools, 1 timestep", "20 cools, 6 timesteps", "20 cools, 12 timesteps"])
    plt.show()

    parse_root_dir(R"C:\Users\4thFloor\Desktop\cool0010\fields1")
    # parse_root_dir(R"C:\Users\4thFloor\Desktop\cool0010\fields2")
    # parse_root_dir(R"C:\Users\4thFloor\Desktop\cool0010\fields3")
    # parse_root_dir(R"C:\Users\4thFloor\Desktop\cool0010\fields4")
    parse_root_dir(R"C:\Users\4thFloor\Desktop\cool0010\fields5")
    # parse_root_dir(R"C:\Users\4thFloor\Desktop\cool0010\fields6")
    # parse_root_dir(R"C:\Users\4thFloor\Desktop\cool0010\fields7")
    parse_root_dir(R"C:\Users\4thFloor\Desktop\cool0010\fields8")
    #parse_root_dir(R"C:\Users\4thFloor\Desktop\cool0010\fields9")
    # parse_root_dir(R"C:\Users\4thFloor\Desktop\cool0010\fields10")
    # parse_root_dir(R"C:\Users\4thFloor\Desktop\cool0010\fields11")
    parse_root_dir(R"C:\Users\4thFloor\Desktop\cool0010\fields12")

    plt.legend(["1 time-step", "5 time-steps", "8 time-steps", "12 time-steps"])
    plt.show()

    parse_root_dir(R"C:\Users\4thFloor\Desktop\cool0015\fields1")
    #parse_root_dir(R"C:\Users\4thFloor\Desktop\cool0015\fields2")
    #parse_root_dir(R"C:\Users\4thFloor\Desktop\cool0015\fields3")
    #parse_root_dir(R"C:\Users\4thFloor\Desktop\cool0015\fields4")
    parse_root_dir(R"C:\Users\4thFloor\Desktop\cool0015\fields5")
    #parse_root_dir(R"C:\Users\4thFloor\Desktop\cool0015\fields6")
    #parse_root_dir(R"C:\Users\4thFloor\Desktop\cool0015\fields7")
    parse_root_dir(R"C:\Users\4thFloor\Desktop\cool0015\fields8")
    #parse_root_dir(R"C:\Users\4thFloor\Desktop\cool0015\fields9")
    #parse_root_dir(R"C:\Users\4thFloor\Desktop\cool0015\fields10")
    #parse_root_dir(R"C:\Users\4thFloor\Desktop\cool0015\fields11")
    parse_root_dir(R"C:\Users\4thFloor\Desktop\cool0015\fields12")

    plt.legend(["1 time-step", "5 time-steps", "8 time-steps", "12 time-steps"])
    plt.show()

    parse_root_dir(R"C:\Users\4thFloor\Desktop\cool0020\fields1")
    #parse_root_dir(R"C:\Users\4thFloor\Desktop\cool0020\fields2")
    #parse_root_dir(R"C:\Users\4thFloor\Desktop\cool0020\fields3")
    #parse_root_dir(R"C:\Users\4thFloor\Desktop\cool0020\fields4")
    parse_root_dir(R"C:\Users\4thFloor\Desktop\cool0020\fields5")
    #parse_root_dir(R"C:\Users\4thFloor\Desktop\cool0020\fields6")
    #parse_root_dir(R"C:\Users\4thFloor\Desktop\cool0020\fields7")
    parse_root_dir(R"C:\Users\4thFloor\Desktop\cool0020\fields8")
    #parse_root_dir(R"C:\Users\4thFloor\Desktop\cool0020\fields9")
    #parse_root_dir(R"C:\Users\4thFloor\Desktop\cool0020\fields10")
    #parse_root_dir(R"C:\Users\4thFloor\Desktop\cool0020\fields11")
    parse_root_dir(R"C:\Users\4thFloor\Desktop\cool0020\fields12")

    plt.legend(["1 time-step", "5 time-steps", "8 time-steps", "12 time-steps"])
    plt.show()
    # (memDeltaArray) = parseFile("C:/Users/4thFloor/Desktop/%5C090616_212042_stdlog.txt")

    # if memDeltaArray is not None:
    #    print(memDeltaArray)
    #    plotMemoryUsage(memDeltaArray, 2, 'bytes')

if __name__ == '__main__':
    main(sys.argv[1:])