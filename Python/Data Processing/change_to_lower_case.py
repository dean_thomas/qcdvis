"""
Renames all Polyakov.vol1 input files on the cluster to polyakov.vol1
"""

import os

def recursive_walk(path):
    for root, dirs, files in os.walk(path):
        for name in files:
            if 'Polyakov.vol1' == name:
                yield os.path.join(root, name)


for file in recursive_walk('/media/dean/raw_data/Dean/QCD Data/Field data/Pygrid/12x24'):
    input = file
    head, tail = os.path.split(input)
    output = os.path.join(head, tail.lower())

    print("Renaming %s to %s" % (input, output))
    os.rename(input, output)