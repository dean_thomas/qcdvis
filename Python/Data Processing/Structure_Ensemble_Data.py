#   Directory / file handling functionality
from os import walk
from os.path import exists
from os import makedirs
from os import rename

from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *

#   Regex
import re

#   Command line and argument parsing
import sys, getopt

#   Group indices within our Regex
COOL_INDEX = 1
CONF_INDEX = 2

#   Storage for raw file list
files = []

#   Our match string for sorting files
regex = "^cool(\d{4})_conp(\d{4})"




def do_file_move(verbose_output):
    """
    Main functionality; builds a list of files to be moved and then builds the expected output directory structure
    :param verbose_output:
    :return:
    """
    #   Keep a count of how many files have been moved
    move_count = 0

    #   Prompt user for input directory
    input_dir = get_input_path()
    print("User selected directory: %s" % input_dir)

    #   Obtain a list of files in the target directory
    for (dir_path, dir_names, filenames) in walk(input_dir):
        files.extend(filenames)
        break

    # List the files
    if (verbose_output):
        print("Raw file list")
        for file in files:
            print(file)

    # Process file list
    for file in files:
        #   Do a regex match on the filenames
        is_match = re.match(regex, file)

        if is_match:
            #   Extract strings from regex
            filename = is_match.group()
            conf = is_match.group(CONF_INDEX)
            cool = is_match.group(COOL_INDEX)

            #   List matching files
            if verbose_output:
                print("Filename: ", filename)
                print("Configuration: ", conf)
                print("Cooling: ", cool)

            # Create a path to where the file should be moved to
            output_dir = input_dir + "/conp" + conf
            if not exists(output_dir):
                print("Created directory at: ", output_dir)
                makedirs(output_dir)

            # Construct old and new paths to file
            old_file = input_dir + "/" + filename
            new_file = output_dir + "/" + filename

            if not exists(new_file):
                #   Do the move
                print("Moving file from: ", old_file, "to: ", new_file)
                rename(old_file, new_file)
                move_count += 1
            else:
                # File already exists
                print("The file '", new_file, "' already exists.  Leaving source file at '", old_file, "'")

    # Summary of process
    print("Moved a total of", move_count, "files")

    return move_count


def show_usage():
    """
    Called if unrecognised command line options are specified
    :return: Nothing
    """
    print("organise_ensemble")
    print("Options:")
    print("-v", "\t", "Verbose listing of operations")
    return


def get_input_path():
    """
    Uses PyQt to allow the user to specify the root directory
    :return:
    """
    path = QFileDialog.getExistingDirectory(None, "Choose input directory")

    return path


#   Entry point
def main(argv):
    #   Parse the command line
    try:
        opts, args = getopt.getopt(argv, "v", ["v"])
    except getopt.GetoptError:
        show_usage()
        sys.exit(2)

    verbose_output = False

    #   Let options based upon command line input
    for opt, arg in opts:
        if opt == '-v' or opt == '--v':
            verbose_output = True

    # Execute the function
    do_file_move(verbose_output)

    return 0


if __name__ == "__main__":
    app = QApplication(sys.argv)
    # app.exec_()
    main(sys.argv[1:])




