import sys
import os
from subprocess import call
import Common.FileLists as fl

LATTICE_EXE = R'C:\Users\4thFloor\Documents\Qt\QtFlexibleIsosurfaces\CommandLineTools\CommandLineTools\x64\Release\Lattice.exe'


def get_file_list(ensemble_root, file_name='', mu=[], cools=[], file_extension=''):
    """
    Recursively obtains a list of files with a specified criteria below a give root directory
    :param ensemble_root: the folder to start the recursion from
    :param file_name: a keyword that must appear in the filename's in the output (e.g. 'Polyakov')
    :param cools: a string array representing the desired number of cools (in the form 'COOL0015')
    :param file_extension: allows the selection to be limited to a specified extension (e.g. '.vol1')
    :return: a list of files represented by their absolute path
    """
    file_list = []

    for subdir, dirs, files in os.walk(ensemble_root):
        for file in files:
            # Expand to absolute path
            path_to_file = os.path.join(subdir, file)

            if any(st in path_to_file for st in mu):

                if any(st in path_to_file for st in cools):
                    # if the number of cools appears in the list...
                    if file_name in path_to_file and file_extension in path_to_file:
                        if os.path.splitext(path_to_file)[1] == file_extension:
                            print(path_to_file)
                            file_list.append(path_to_file)

    return file_list




input_root_dir = R'G:\Dean\QCD Data\Cooled Configurations\Pygrid\12x16'
output_root_dir = R'G:\Dean\QCD Data\Field data\Pygrid\12x16'

cool_array = [4, 9, 14, 19]
#mu_array = [850]
mu_array = [0, 300, 400, 450, 500, 525, 550, 575, 600, 625, 650, 675, 700, 800, 900]

failed = []

#FIELDS = ['-poly']
FIELDS = ['-poly']

#file_list = get_file_list(ensemble_root=input_root_dir, cools=['cool0005'])
for cools in cool_array:
    for mu in mu_array:
        file_list = get_file_list(ensemble_root=input_root_dir, mu=['mu{:0>4}'.format(mu)], cools=['cool{:0>4}'.format(cools)])

        #   Usage: lattice <input filename> <output root> <field 1> [field 2] ...
        for file in file_list:
            call_params = [LATTICE_EXE, file, output_root_dir]

            for f in FIELDS:
                call_params.append(f)

            result = call(call_params)

            if result != 0:
                failed.append(file, result)

print('The command failed for the following files: %s' %(failed))