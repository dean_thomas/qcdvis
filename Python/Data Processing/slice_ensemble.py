"""
Point at a directory containing multiple configurations to run the slice tool
at specified levels of cooling
"""
import sys
import os
from subprocess import call

COOLS = range(0, 21)
SLICE_EXE = R"/home/dean/qcdvis/CommandLineTools/CommandLineTools/build-command_line_tools-Desktop_Qt_5_7_0_GCC_64bit-Debug/volume_slicer/volume_slicer"


def main(args):
    """
    entry point
    """
    #assert(len(args) == 1)
    root_dir = args[0]

    fields = args[1:]

    """
    Root is intended to be an ensemble directory
    example: /Pygrid/12x24/mu0300/config.b190k1680mu0300j02s12t24

    first get a list of cooling slices
    """
    configuration_files = []
    for field in fields:
        for subdir, dirs, files in os.walk(root_dir):
            for file in files:
                # Expand to absolute path
                path_to_file = os.path.join(subdir, file)

                cool_dirs = []
                for cool in COOLS:
                    cool_dirs.append('cool{:0>4}'.format(cool))

                # if the number of cools appears in the list...
                if any(st in path_to_file for st in cool_dirs):
                    # add to the list of files to be processed
                    if field.lower() in path_to_file.lower():
                        print(path_to_file)
                        configuration_files.append(path_to_file)
                #break

    failed_files = []
    for file in configuration_files:
        retries = 0
        while retries < 3:
            # execute the command
            return_code = call([SLICE_EXE, file, '-xyz', '-np'])

            # it would appear the program cannot always open the file first time
            #   try up to 3 times before deciding the file cannot be read.
            if return_code != 0:
                if retries < 3:
                    retries += 1
                    print('Retry: %d' %(retries))
                else:
                    failed_files.append(file)
                    break
            else:
                break


    print('The process failed for the following files:')
    print(failed_files)


#main([R"E:\Dean\QCD Data\Field data\Pygrid\12x24\mu0000\config.b190k1680mu0000j00s12t24"])
main([R"/media/dean/raw_data/Dean/QCD Data/Field data/Pygrid/12x24/mu1000/config.b190k1680mu1000j02s12t24/conp0025/",
      "Topological charge density.hvol1"])


if __name__ == '__main__':
    main(sys.argv[1:])
