import re
from collections import defaultdict
from collections import namedtuple

from Common.data_keys import *

#   These Regex's are used to parse data about the JCN structure
REGEX_TOTAL_SLAB_COUNT = R"Total number of slabs:\s+(\d+)"

REGEX_TOTAL_POLYGON_COUNT = R"Total number of polygons:\s+(\d+)"
REGEX_TOTAL_TRIANGLE_COUNT = R"Total number of triangles:\s+(\d+)"
REGEX_TOTAL_SURFACE_AREA = R"Total surface area:\s+(\d+)"

REGEX_AVERAGE_POLYGON_COUNT = R"Average number of polygons per slab:\s+([-+]?[0-9]*\.?[0-9]*)"
REGEX_AVERAGE_TRIANGLE_COUNT = R"Average number of triangles per slab:\s+([-+]?[0-9]*\.?[0-9]*)"
REGEX_AVERAGE_SURFACE_AREA = R"Average surface area per slab:\s+([-+]?[0-9]*\.?[0-9]*)"

REGEX_SLAB_STATS = R"\s*Slab:\s*(\d+)\s*Polygons:\s*(\d+)\s*Triangles:\s*(\d+)\s*Surface Area:\s*([-+]?[0-9]*\.?[0-9]*)"

SlabStats = namedtuple('SlabStats', ['id', 'polygons', 'triangles', 'area'])


def __open_file__(filename):
    """
    Internal function for loading the stdlog.txt file and passing it as a string list for parsing
    :param filename: the name of the log file to be parsed
    :return: a list of strings representing the file text
    """
    #   Read contents of the file
    file = open(filename, 'r')
    file_contents = file.read().splitlines()
    file.close()

    return file_contents


def __parse_slab_stats__(line):
    """
    Internal function for parsing the block relating to statistics on the JCN
    :param line: the current line of slab_stats.txt to be parsed
    :return:
    """
    #   Number of slabs
    slab_stats_matches = re.search(REGEX_SLAB_STATS, line)
    if slab_stats_matches is not None:
        id = int(slab_stats_matches.group(1))
        polygon_count = int(slab_stats_matches.group(2))
        triangle_count = int(slab_stats_matches.group(3))
        surface_area = float(slab_stats_matches.group(4))

        #   return the parsed data
        return SlabStats(id=id, polygons=polygon_count, triangles=triangle_count, area=surface_area)

    #   not a line containing slab stats
    return None


def calculate_triangle_distribution(slab_dictionary, verbose=False):
    """
    Computes a distribution of the complexity of the slabs in the file.
    :param slab_dictionary: a raw dictionary of (id, slab_stats) tuples
    :param verbose: if True the distribution is dumped to the console
    :return: a dictionary of (triangle_count, list[slab_stats]) tuples.  To find the number of slabs in each bin,
    simply take the length of the list in that tuple
    """
    result = defaultdict(list)

    for id, slab_stats in slab_dictionary.items():
        triangle_count = slab_stats.triangles

        result[triangle_count].append(slab_stats)

    if verbose:
        print('triangles : slab count')
        for key, value in sorted(result.items()):
            print('{:} : {:}'.format(key, len(value)))

    return result


def calculate_polygon_distribution(slab_dictionary, verbose=False):
    """
    Computes a distribution of the complexity of the slabs in the file.
    :param slab_dictionary: a raw dictionary of (id, slab_stats) tuples
    :param verbose: if True the distribution is dumped to the console
    :return: a dictionary of (polygon_count, list[slab_stats]) tuples.  To find the number of slabs in each bin,
    simply take the length of the list in that tuple
    """
    result = defaultdict(list)

    for id, slab_stats in slab_dictionary.items():
        polygon_count = slab_stats.polygons

        result[polygon_count].append(slab_stats)

    if verbose:
        print('polygons : slab count')
        for key, value in sorted(result.items()):
            print('{:} : {:}'.format(key, len(value)))

    return result


def calculate_surface_area_distribution(slab_dictionary, verbose=False):
    """
    Computes a distribution of the complexity of the slabs in the file.
    :param slab_dictionary: a raw dictionary of (id, slab_stats) tuples
    :param verbose: if True the distribution is dumped to the console
    :return: a dictionary of (polygon_count, list[slab_stats]) tuples.  To find the number of slabs in each bin,
    simply take the length of the list in that tuple
    """
    result = defaultdict(list)

    for id, slab_stats in slab_dictionary.items():
        #   We'll round the slab area to the closest integer value
        slab_area = round(slab_stats.area)

        result[slab_area].append(slab_stats)

    if verbose:
        print('surface area : slab count')
        for key, value in sorted(result.items()):
            print('{:} : {:}'.format(key, len(value)))

    return result


def get_slab_stats(filename):
    """
    Parses the stdlog file extracting information about process memory usage and duration
    :param filename: the file to be parsed
    :return: a dictionary containing the stats parsed
    """
    #   Create a dictionary to return the parsed data
    data = {}

    #   Load the file
    file_contents = __open_file__(filename)
    if file_contents is None:
        return None

    #   These will be passed to the parse functions to be filled
    total_slabs = 0
    total_triangles = 0
    total_polygons = 0
    total_surface_area = 0
    slab_dictionary = defaultdict(SlabStats)

    for line_num in range(0, len(file_contents)):
        #   Loop over file contents (we'll use line numbers so that the line can be advanced manually, if required)
        line = file_contents[line_num]

        #   Number of slabs
        total_slabs_matches = re.search(REGEX_TOTAL_SLAB_COUNT, line)
        if total_slabs_matches is not None:
            total_slabs = int(total_slabs_matches.group(1))

        #   Number of triangles
        total_triangles_matches = re.search(REGEX_TOTAL_TRIANGLE_COUNT, line)
        if total_triangles_matches is not None:
            total_triangles = int(total_triangles_matches.group(1))
        #   Average number of triangles
        average_triangles_matches = re.search(REGEX_AVERAGE_TRIANGLE_COUNT, line)
        if average_triangles_matches is not None:
            average_triangles = float(average_triangles_matches.group(1))

        #   Number of polygons
        total_polygons_matches = re.search(REGEX_TOTAL_POLYGON_COUNT, line)
        if total_polygons_matches is not None:
            total_polygons = int(total_polygons_matches.group(1))
        #   Average number of polygons
        average_polygons_matches = re.search(REGEX_AVERAGE_POLYGON_COUNT, line)
        if average_polygons_matches is not None:
            average_polygons = float(average_polygons_matches.group(1))

        #   Surface Area
        total_surface_area_matches = re.search(REGEX_TOTAL_SURFACE_AREA, line)
        if total_surface_area_matches is not None:
            total_surface_area = int(total_surface_area_matches.group(1))
        #   Average surface Area
        average_surface_area_matches = re.search(REGEX_AVERAGE_SURFACE_AREA, line)
        if average_surface_area_matches is not None:
            average_surface_area = float(average_surface_area_matches.group(1))

        #   individual slab stats
        slab_stats = __parse_slab_stats__(line)
        if slab_stats is not None:
            #   store individual stats as a dictionary
            slab_dictionary[slab_stats.id] = slab_stats

    #   Dump processed information to console
    assert(len(slab_dictionary) == total_slabs)
    print('slab count: {:}; polygons: {:}; triangles: {:}; total surface area: {:}'
          .format(total_slabs, total_polygons, total_triangles, total_surface_area))
    print('average polygons: {:}; average triangles: {:}; average surface area: {:}'
          .format(average_polygons, average_triangles, average_surface_area))
    #   print('slab dictionary: {:}'.format(slab_dictionary))

    #   Return the data as their own dictionaries for parsing later
    data[KEY_SLAB_COUNT_AVERAGE] = total_slabs

    data[KEY_TRIANGLE_COUNT_AVERAGE] = total_triangles
    data[KEY_TRIANGLE_COUNT_AVERAGE_NORMALISED] = average_triangles

    data[KEY_POLYGON_COUNT_AVERAGE] = total_polygons
    data[KEY_POLYGON_COUNT_AVERAGE_NORMALISED] = average_polygons

    data[KEY_SURFACE_AREA_TOTAL_AVERAGE] = total_surface_area
    data[KEY_SURFACE_AREA_TOTAL_AVERAGE_NORMALISED] = average_surface_area

    data[KEY_SLAB_DICTIONARY] = slab_dictionary

    #   Compute the distribution of slabs to various properties
    triangle_dist = calculate_triangle_distribution(slab_dictionary, verbose=True)
    polygon_dist = calculate_polygon_distribution(slab_dictionary, verbose=True)
    surface_area_dist = calculate_surface_area_distribution(slab_dictionary, verbose=True)
    assert(triangle_dist is not None and polygon_dist is not None and surface_area_dist is not None)

    #   Add the distributed slabs to the data array
    data[KEY_SORTED_SLAB_DICTIONARY_POLYGON_COUNT] = polygon_dist
    data[KEY_SORTED_SLAB_DICTIONARY_TRIANGLE_COUNT] = triangle_dist
    data[KEY_SORTED_SLAB_DICTIONARY_SURFACE_AREA] = surface_area_dist

    #   Return the data as a dictionary.  This will allow us to add in further stats at a later date, if required.
    return data



