

def get_3_mu_array():
    return [0, 500, 1000]

def get_5_mu_array():
    return [0, 500, 900, 1000, 1100]

def get_full_mu_array():
    """
    :return: the full list of values of mu available for the 12^3.24 data set
    """
    return [0, 250, 300, 325, 350, 375, 400, 425, 450, 500, 550, 600, 650, 700, 750, 800, 850, 900, 950, 1000, 1100]