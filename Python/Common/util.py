import os


def get_output_dir(experiment_title, data_type, n_t=None, ensemble_count=None, data_sub_type=''):
    """

    :param experiment_title: Something like Polyakov or spacelike-timelike
    :param data_type: scalar, univariate, multivariate
    :param n_t:
    :param ensemble_count:
    :return:
    """
    base_dir = '/home/dean/Desktop/'
    experiment_title_linux = generate_linux_friendly_filename(experiment_title)
    data_type_linux = generate_linux_friendly_filename(data_type)
    data_sub_type_linux = generate_linux_friendly_filename(data_sub_type)

    output_dir = os.path.join(base_dir, experiment_title_linux, data_type_linux, data_sub_type_linux)

    if n_t is not None:
        output_dir = os.path.join(output_dir, '12x{:}'.format(n_t))
    else:
        output_dir = os.path.join(output_dir, 'lattice_comparison')

    if ensemble_count is not None:
        output_dir = os.path.join(output_dir, '{:}_ensembles'.format(ensemble_count))

    return output_dir


def generate_linux_friendly_filename(text):
    #   convert to lowercase
    result = text.lower()

    #   replace spaces with underscores
    result = result.replace(' ', '_')

    #   remove parentheses, square brackets and braces
    result = result.replace('(', '').replace(')', '')\
        .replace('[', '').replace(']', '')\
        .replace('{', '').replace('}', '')\
        .replace('-', '_')

    #   return resultant string
    return result
