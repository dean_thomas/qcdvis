import matplotlib.pyplot as plt
import numpy as np
import random
from matplotlib import cm
import mpl_toolkits.mplot3d
from functools import partial
from Common.util import *

#   Use LaTeX to provide type1 fonts for the graphs
plt.rcParams['ps.useafm'] = True
plt.rcParams['pdf.use14corefonts'] = True
plt.rcParams['text.usetex'] = True

#   Set defaults for saving output
plt.rcParams["savefig.format"] = 'pdf'

#   Define colour maps according to purpose
SEQUENTIAL_COLORMAP = cm.viridis
DIVERGENT_COLORMAP = cm.coolwarm

#   Set dimensions of outputs
FIG_WIDTH_CM_SINGLE = 30
FIG_WIDTH_CM_DUAL = 50
FIG_HEIGHT_CM = 15


def __row_count__(matrix):
    """
    Convenience method for counting rows in a numpy matrix
    :param matrix:
    :return:
    """
    rows, cols = matrix.shape
    return rows


def __col_count__(matrix):
    """
    Convenience method for counting columns in a numpy matrix
    :param matrix:
    :return:
    """
    rows, cols = matrix.shape
    return cols


def create_figure(z_values, fig_label='figure',x_label='x', y_label='y', z_label='z',
                  is_divergent=False, x_tick_labels=None, y_tick_labels=None):
    #   Create the figure
    fig = plt.figure('{:}'.format(fig_label), figsize=(FIG_WIDTH_CM_SINGLE / 2.54, FIG_HEIGHT_CM / 2.54))

    ax = plt.subplot(121, projection='3d')

    __add_surface_plot__(ax, z_values,
                         fig_label, x_label, y_label, z_label,
                         is_divergent, x_tick_labels, y_tick_labels)

    #   Show the surface
    plt.show()


def create_dual_figure(fig_label, z_values_0, z_values_1, plot_label_0, plot_label_1,
                       x_label='x', y_label='y', z_label='z',
                       x_tick_labels_0=None, y_tick_labels_0=None,
                       x_tick_labels_1=None, y_tick_labels_1=None,
                       is_divergent=False):
    #   Create the figure
    fig = plt.figure('{:}'.format(generate_linux_friendly_filename(fig_label)),
                     figsize=(FIG_WIDTH_CM_DUAL / 2.54, FIG_HEIGHT_CM / 2.54))

    #   Set common plot title
    plt.suptitle(fig_label)

    #   create and draw the left figure
    ax_0 = plt.subplot(1, 2, 1, projection='3d')
    __add_surface_plot__(ax_0, z_values_0,
                         plot_label_0, x_label, y_label, z_label,
                         is_divergent, x_tick_labels_0, y_tick_labels_0)

    #   create and draw the right figure
    ax_1 = plt.subplot(1, 2, 2, projection='3d')
    __add_surface_plot__(ax_1, z_values_1,
                         plot_label_1, x_label, y_label, z_label,
                         is_divergent, x_tick_labels_1, y_tick_labels_1)

    #   Avoid wasting horizontal space but allow room for the group heading text
    plt.tight_layout(pad=2)

    #   Show the surface
    plt.show()


def __add_surface_plot__(ax, z_values,
                         fig_label='figure', x_label='x', y_label='y', z_label='z',
                         is_divergent=False, x_tick_labels=None, y_tick_labels=None):
    """
    Provides a generic method for creating surface plots for our data
    :param x_min:
    :param x_max:
    :param y_min:
    :param y_max:
    :param z_values:
    :param x_step:
    :param y_step:
    :param fig_label:
    :param x_label:
    :param y_label:
    :param z_label:
    :param is_divergent:
    :return:
    """
    #   Count the number of rows and columns in the input data
    rows = __row_count__(z_values)
    cols = __col_count__(z_values)

    #   Create the x and y axis
    x_values = np.arange(0, rows)
    y_values = np.arange(0, cols)

    if x_tick_labels is not None:
        #   Every 2nd label
        x_freq = 2
        assert len(x_tick_labels) == len(x_values)

        #   Re-label the ticks
        old_x_ticks = []
        new_x_ticks = []

        #   Add each label, skipping every y_freq tick
        for i in range(0, len(x_values) - 1, x_freq):
            old_x_ticks.append(x_values[i])
            new_x_ticks.append(x_tick_labels[i])

        # Add the maximum to the tick labels
        old_x_ticks.append(x_values[-1])
        new_x_ticks.append(x_tick_labels[-1])

        #   Replace the old ticks with the new ticks
        plt.xticks(old_x_ticks, new_x_ticks)

    if y_tick_labels is not None:
        #   Every 5th label
        y_freq = 5
        assert len(y_tick_labels) == len(y_values)

        #   Re-label the ticks
        old_y_ticks = []
        new_y_ticks = []

        #   Add each label, skipping every y_freq tick
        for i in range(0, len(y_values)-1, y_freq):
            old_y_ticks.append(y_values[i])
            new_y_ticks.append(y_tick_labels[i])

        #   Add the maximum to the tick labels
        old_y_ticks.append(y_values[-1])
        new_y_ticks.append(y_tick_labels[-1])

        #   Replace the old ticks with the new ticks
        plt.yticks(old_y_ticks, new_y_ticks)


    #   Check dimensions are correct
    assert len(x_values) == rows, \
        "Matrix dimensions ({:}x{:}) doesn't match the number of x values ({:})".format(rows, cols, len(x_values))
    assert len(y_values) == cols, \
        "Matrix dimensions ({:}x{:}) doesn't match the number of y values ({:})".format(rows, cols, len(y_values))

    #   Create a mesh for the function values
    x_mesh, y_mesh = np.meshgrid(x_values, y_values)

    #   Create the surface plot as a function initially
    surf_func = partial(ax.plot_surface, x_mesh, y_mesh, z_values.transpose(),
                        rstride=1, cstride=1,linewidth=0, antialiased=False)

    #   Now create the surface with an appropriate colourmap for the data
    if is_divergent:
        surf = surf_func(cmap=DIVERGENT_COLORMAP)
    else:
        surf = surf_func(cmap=SEQUENTIAL_COLORMAP)

    #   Set axis labels
    ax.set_xlabel('{:}'.format(x_label))
    ax.set_ylabel('{:}'.format(y_label))
    ax.set_zlabel('{:}'.format(z_label))

    #   Add a color bar
    plt.colorbar(surf, shrink=1.0, aspect=10)

    #   Set plot title
    plt.title(fig_label)


#def set_x_axis_labels(label_array):
#    ax = plt.gca()


def create_data_matrix(x_dim, y_dim, values=None):
    """
    Convenience method for creating an empty numpy matrix
    :param x_dim: number of rows required
    :param y_dim: number of cols required
    :param values: if supplied the data will be arranged directly onto the 2d matrix
    :return: an empty matrix with specified dimension
    """
    result = np.zeros((x_dim, y_dim))

    if values is not None:
        assert x_dim * y_dim == len(values), \
            "Supplied data array length ({:}) doesn't match the output matrix dimensions ({:}x{:})"\
                .format(len(values), x_dim, y_dim)

        #   Assign the data array to the matrix
        for x in range(0, x_dim):
            for y in range(0, y_dim):
                result[x][y] = values[(x * y_dim) + y]

    return result


# z_array = [0]*200
# for i in range(0, 200):
#     z_array[i] = random.random()
#
# z_values = create_data_matrix(20, 10, z_array)
#
# surface_plot(0, 20, 0, 10, z_values)