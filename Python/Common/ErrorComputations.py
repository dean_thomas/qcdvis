import math


def compute_error_bars(ensemble_freq_array, configuration_array, configuration_count, x_max):
    """
    Computes the error bars for an ensemble average array
    :param ensemble_freq_array: an array representing all the bins for values across the ensemble as an average
    :param configuration_array: an array of arrays representing the bins for values in each configuration
    :param configuration_count: the number of configurations in the configuration_array
    :param x_max: the number of bins expected in the output (should be the same size as the input / output arrays)
    :return: an array of the calculated error for each bin in the ensemble average
    """
    ensemble_variance = [0] * x_max
    for configuration in configuration_array:
        # loop over each configuration...
        for i in range(0, x_max):
            # ...and compute the variance from the ensemble mean in each bin
            ensemble_variance[i] += ((configuration[i] - ensemble_freq_array[i]) ** 2) / configuration_count

    # Now, compute the standard deviation by square-rooting the variance in each bin
    for i in range(0, x_max):
        if configuration_count > 2:
            ensemble_variance[i] = math.sqrt(ensemble_variance[i] / (configuration_count - 1))
        else:
            ensemble_variance[i] = math.sqrt(ensemble_variance[i])

    return ensemble_variance