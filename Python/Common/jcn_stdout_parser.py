import re
from collections import namedtuple

#   These Regex's are used to parse data about the JCN structure
REGEX_JCN = R"Joint Contour Net:"
REGEX_JCN_VERTICES = R"\s+vertex\ count:\ (\d+)"
REGEX_JCN_VERTEX_COUNT = 1
REGEX_JCN_EDGES = R"\s+edge\ count:\ (\d+)"
REGEX_JCN_EDGE_COUNT = 1
REGEX_JACOBI_NUMBER = R"\s+jacobi number:\ (\d+)"
REGEX_JACOBI_NUMBER_COUNT = 1


MdrgStats = namedtuple('MdrgStats', ['vertex_count', 'edge_count'])
ReebGraphStats = namedtuple('ReebStats', ['vertex_count', 'edge_count', 'loop_count'])
JcnStats = namedtuple('JcnStats', ['vertex_count', 'edge_count', 'jacobi_number'])


def __open_file__(filename):
    """
    Internal function for loading the stdout.txt file and passing it as a string list for parsing
    :param filename:
    :return:
    """
    #   Read contents of the file
    file = open(filename, 'r')
    file_contents = file.read().splitlines()
    file.close()

    return file_contents


def __parse_jcn_stats__(file_contents, line_num):
    """
    Internal function for parsing the block relating to statistics on the JCN
    :param file_contents:
    :param line_num:
    :return:
    """
    line = file_contents[line_num]

    #   Find memory delta stats
    jcn_block_matches = re.search(REGEX_JCN, line)
    if jcn_block_matches is not None:
        #   Advance to next line, this should list the number of vertices in the MDRG
        line = file_contents[line_num + 1]
        jcn_vertices_matches = re.search(REGEX_JCN_VERTICES, line)
        if jcn_vertices_matches is not None:
            jcn_vertex_count = int(jcn_vertices_matches.group(REGEX_JCN_VERTEX_COUNT))
            #print('%d' % (jcn_vertex_count))

            #   Advance to next line, this should be the number of edges
            line = file_contents[line_num + 2]
            jcn_edges_matches = re.search(REGEX_JCN_EDGES, line)
            if jcn_edges_matches is not None:
                jcn_edge_count = int(jcn_edges_matches.group(REGEX_JCN_EDGE_COUNT))
                #print('%d' % (jcn_edge_count))

                #   Advance to next line, this should be jacobi number
                line = file_contents[line_num + 3]
                jacobi_number_matches = re.search(REGEX_JACOBI_NUMBER, line)
                if jacobi_number_matches is not None:
                    jacobi_number = float(jacobi_number_matches.group(REGEX_JACOBI_NUMBER_COUNT))
                    #print('%d' % (jacobi_number))

                return JcnStats(vertex_count=jcn_vertex_count, edge_count=jcn_edge_count, jacobi_number=jacobi_number)

    return None


def get_jcn_stats(filename):
    """
    Parses the stdout file extraction information about the JCN from the block
        Joint Contour Net:
            vertex count: 60
            edge count: 62
            jacobi number: 39 mdrgList.
    :param filename: the file to be parsed
    :return: a namedtuple (vertex_count, edge_count, jacobi_number)
    """
    #   Load the file
    file_contents = __open_file__(filename)

    if file_contents is None:
        return None

    #   Store each array of values based upon the process it represents
    #memDeltaDictionary = defaultdict(list)

    #   Loop over file contents
    for line_num in range(0, len(file_contents)):
        # print("%s" %(line))

        #   Extract memory delta (if possible from the current line)
        jcn_stats = __parse_jcn_stats__(file_contents, line_num)
        if jcn_stats is not None:
            #print(jcn_stats)
            return (jcn_stats)
            #memDeltaDictionary[mdrg_stats[PROC_NAME]].append(mdrg_stats)

    return None


