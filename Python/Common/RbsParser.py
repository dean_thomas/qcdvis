from collections import namedtuple


Superarc = namedtuple('Superarc',
                      ['arc_id',
                       'top_height',
                       'top_height_n',
                       'bottom_height',
                       'bottom_height_n',
                       'height_range',
                       'top_pos',
                       'bottom_pos'])

Supernode = namedtuple('Supernode',
                       ['node_id',
                        'height',
                        'height_n',
                        'pos',
                        'degree',
                        'up_degree',
                        'down_degree',
                        'upward_node_count',
                        'upward_arc_count',
                        'downward_node_count',
                        'downward_arc_count',
                        'upward_height_sum',
                        'downward_height_sum'])

def parse_superarc(lines, current_line_number):
    """
    Reads the contents of the file, parsing the values into a Superarc tuple
    :param lines:
    :param current_line_number:
    :return:
    """
    #   Read the height (ignore the norm heigh for now)
    current_line = lines[current_line_number]
    #print(current_line)

    #   Extract the id
    id_tokens = current_line.split()
    assert ('Supearc' in id_tokens[0])
    arc_id = int(id_tokens[1])

    #   Advance to the next line (top height)
    if current_line_number < len(lines) - 1:
        current_line_number += 1

    #   Read the height (ignore the norm heigh for now)
    current_line = lines[current_line_number]
    #print(current_line)

    #   Matched the value we are looking for
    top_height_tokens = current_line.split(':')
    assert('top height' in top_height_tokens[0])
    top_height_tokens_2 = top_height_tokens[1].split()
    top_height = float(top_height_tokens_2[0])
    #   Todo: parse norm height (remove parentheses)
    norm_top_height = -1.0
    #norm_top_height = top_height_tokens_2[1]

    #
    #   Advance to the next line (bottom height)
    if current_line_number < len(lines) - 1:
        current_line_number += 1

    # Read the height (ignore the norm heigh for now)
    current_line = lines[current_line_number]
    #print(current_line)

    #   Matched the value we are looking for
    bottom_height_tokens = current_line.split(':')
    assert ('bottom height' in bottom_height_tokens[0])
    bottom_height_tokens_2 = bottom_height_tokens[1].split()
    bottom_height = float(bottom_height_tokens_2[0])
    #   Todo: parse norm height (remove parentheses)
    norm_botom_height = -1.0
    # norm_bottom_height = bottom_height_tokens_2[1]

    #
    #   Advance to the next line (height range)
    if current_line_number < len(lines) - 1:
        current_line_number += 1

    # Read the height (ignore the norm heigh for now)
    current_line = lines[current_line_number]
    #print(current_line)

    #   Matched the value we are looking for
    height_range_tokens = current_line.split(':')
    assert ('height range' in height_range_tokens[0])
    height_range = float(height_range_tokens[1])

    #
    #   Advance to the next line (top pos)
    if current_line_number < len(lines) - 1:
        current_line_number += 1

    # Read the height (ignore the norm heigh for now)
    current_line = lines[current_line_number]
    #print(current_line)

    #   Matched the value we are looking for
    top_pos_tokens = current_line.split(':')
    assert ('top location' in top_pos_tokens[0])
    top_pos_tokens_2 = top_height_tokens[1].split(',')
    #   Todo: parse coordinates (remove white space and brackets)
    top_pos = (-1, -1, -1)
    #top_pos = (int(top_pos_tokens_2[0]), int(top_pos_tokens_2[1], int(top_pos_tokens_2[2])))

    #
    #   Advance to the next line (top pos)
    if current_line_number < len(lines) - 1:
        current_line_number += 1

    # Read the height (ignore the norm heigh for now)
    current_line = lines[current_line_number]
    #print(current_line)

    #   Matched the value we are looking for
    bottom_pos_tokens = current_line.split(':')
    assert ('bottom location' in bottom_pos_tokens[0])
    bottom_pos_tokens_2 = bottom_height_tokens[1].split(',')
    #   Todo: parse coordinates (remove white space and brackets)
    bottom_pos = (-1, -1, -1)
    #bottom_pos = (int(bottom_pos_tokens_2[0]), int(bottom_pos_tokens_2[1], int(bottom_pos_tokens_2[2])))

    #   Return the parsed values
    return Superarc(arc_id=arc_id,
                    top_height=top_height,
                    top_height_n=norm_top_height,
                    bottom_height=bottom_height,
                    bottom_height_n=norm_botom_height,
                    height_range=height_range,
                    top_pos=top_pos,
                    bottom_pos=bottom_pos)


def parse_supernode(lines, current_line_number):
    #   Read the current line
    current_line = lines[current_line_number]
    # print(current_line)

    #   Extract the id
    id_tokens = current_line.split()
    assert ('Supernode' in id_tokens[0])
    node_id = int(id_tokens[1])

    #   Advance to the next line (height)
    if current_line_number < len(lines) - 1:
        current_line_number += 1

    #
    #   Read the current line
    current_line = lines[current_line_number]
    # print(current_line)

    #   Extract the id
    height_tokens = current_line.split()
    assert ('height' in height_tokens[0])
    node_height = float(height_tokens[1])

    #   Advance to the next line (norm height)
    if current_line_number < len(lines) - 1:
        current_line_number += 1

    #
    #   Read the current line
    current_line = lines[current_line_number]
    # print(current_line)

    #   Extract the id
    norm_tokens = current_line.split()
    assert ('norm' in norm_tokens[0])
    node_n_height = float(norm_tokens[1])

    #   Advance to the next line (pos)
    if current_line_number < len(lines) - 1:
        current_line_number += 1

    #
    #   Read the current line
    current_line = lines[current_line_number]
    # print(current_line)

    #   Extract the id
    pos_tokens = current_line.split()
    assert ('pos' in pos_tokens[0])
    #   Todo: parse coordinates (remove white space and brackets)
    node_pos = (-1, -1, -1)

    #   Advance to the next line (degree)
    if current_line_number < len(lines) - 1:
        current_line_number += 1

    #
    #   Read the current line
    current_line = lines[current_line_number]
    # print(current_line)

    #   Extract the id
    degree_tokens = current_line.split()
    assert ('degree' in degree_tokens[0])
    node_degree = int(degree_tokens[1])

    #   Advance to the next line (up degeee)
    if current_line_number < len(lines) - 1:
        current_line_number += 1

    #
    #   Read the current line
    current_line = lines[current_line_number]
    #print(current_line)

    #   Extract the id
    up_degree_tokens = current_line.split(':')
    assert('up degree' in up_degree_tokens[0])
    node_up_degree = int(up_degree_tokens[1])

    #   Advance to the next line (down degree)
    if current_line_number < len(lines) - 1:
        current_line_number += 1

    #
    #   Read the current line
    current_line = lines[current_line_number]
    # print(current_line)

    #   Extract the id
    down_degree_tokens = current_line.split(':')
    assert ('down degree' in down_degree_tokens[0])
    node_down_degree = int(down_degree_tokens[1])

    #   Advance to the next line (upward node count)
    if current_line_number < len(lines) - 1:
        current_line_number += 1

    #
    #   Read the current line
    current_line = lines[current_line_number]
    # print(current_line)

    #   Extract the id
    upward_nodes_tokens = current_line.split(':')
    assert ('upward subtree supernode count' in upward_nodes_tokens[0])
    node_upward_nodes = int(upward_nodes_tokens[1])

    #   Advance to the next line (upward arc count)
    if current_line_number < len(lines) - 1:
        current_line_number += 1

    #
    #   Read the current line
    current_line = lines[current_line_number]
    # print(current_line)

    #   Extract the id
    upward_arcs_tokens = current_line.split(':')
    assert ('upward subtree superarc count' in upward_arcs_tokens[0])
    node_upward_arcs = int(upward_arcs_tokens[1])

    #   Advance to the next line (downward node count)
    if current_line_number < len(lines) - 1:
        current_line_number += 1

    #
    #   Read the current line
    current_line = lines[current_line_number]
    # print(current_line)

    #   Extract the id
    downward_nodes_tokens = current_line.split(':')
    assert ('downward subtree supernode count' in downward_nodes_tokens[0])
    node_downward_nodes = int(downward_nodes_tokens[1])

    #   Advance to the next line (downward arc count)
    if current_line_number < len(lines) - 1:
        current_line_number += 1

    #
    #   Read the current line
    current_line = lines[current_line_number]
    # print(current_line)

    #   Extract the id
    downward_arcs_tokens = current_line.split(':')
    assert ('downward subtree superarc count' in downward_arcs_tokens[0])
    node_downward_arcs = int(downward_arcs_tokens[1])

    #   Advance to the next line (upward height sum)
    if current_line_number < len(lines) - 1:
        current_line_number += 1

    #
    #   Read the current line
    current_line = lines[current_line_number]
    # print(current_line)

    #   Extract the id
    upward_height_tokens = current_line.split(':')
    assert ('upward height sum' in upward_height_tokens[0])
    node_upward_height_sum = float(upward_height_tokens[1])

    #   Advance to the next line (downward height sum)
    if current_line_number < len(lines) - 1:
        current_line_number += 1

    #
    #   Read the current line
    current_line = lines[current_line_number]
    # print(current_line)

    #   Extract the id
    downward_height_tokens = current_line.split(':')
    assert ('downward height sum' in downward_height_tokens[0])
    node_downward_height_sum = float(downward_height_tokens[1])

    return Supernode(node_id=node_id,
                     height=node_height,
                     height_n=node_n_height,
                     pos=node_pos,
                     degree=node_degree,
                     up_degree=node_up_degree,
                     down_degree=node_down_degree,
                     upward_node_count=node_upward_nodes,
                     upward_arc_count=node_upward_arcs,
                     downward_node_count=node_downward_nodes,
                     downward_arc_count=node_downward_arcs,
                     upward_height_sum=node_upward_height_sum,
                     downward_height_sum=node_downward_height_sum)


def parse_rbs_stats(lines):
    #   Key identifying items in the file (oops spelling mistake in the file writer)
    superarc_key = 'Supearc'
    supernode_key = 'Supernode'

    superarcs = []
    supernodes = []

    for current_line_number in range(0, len(lines)):
        #   Retreive the next line
        current_line = lines[current_line_number]

        if superarc_key in current_line:
            #   Parse the object
            new_superarc = parse_superarc(lines, current_line_number)

            #   Add to the list, if valid
            if (new_superarc is not None):
                superarcs.append(new_superarc)
        elif supernode_key in current_line:
            #   Parse the object
            new_supernode = parse_supernode(lines, current_line_number)

            #   Add to the list, if valid
            if (new_supernode is not None):
                supernodes.append(new_supernode)

    #print('Parsed %i supernodes and %i superarcs' %(len(supernodes), len(superarcs)))
    #print('Supernodes = %s' % (supernodes))
    #print('Superarcs = %s' %(superarcs))

    return superarcs, supernodes


def is_upper_leaf(supernode):
    assert hasattr(supernode,'up_degree')
    return getattr(supernode, 'up_degree') == 0


def is_lower_leaf(supernode):
    assert hasattr(supernode, 'down_degree')
    return getattr(supernode, 'down_degree') == 0