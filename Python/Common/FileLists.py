import os


def get_file_list(ensemble_root, cools, file_name, file_extension):
    """
    Returns a list of files within a recursive file structure, with specified parameters
    :param ensemble_root:
    :param cools:
    :param file_name:
    :param file_extension:
    :return:
    """
    file_list = []

    for subdir, dirs, files in os.walk(ensemble_root):
        for file in files:
            # Expand to absolute path
            path_to_file = os.path.join(subdir, file)

            # if the number of cools appears in the list...
            if 'cool{:0>4}'.format(cools) in path_to_file:
                if file_name in path_to_file and file_extension in path_to_file:
                    print(path_to_file)
                    file_list.append(path_to_file)

    return file_list


def get_list_of_jcn_files_for_ensemble(data_root_dir, mu, cools, file_name, fields='', slab_size=''):
    # The list of files to be returned
    jcn_files = []

    # Create a path for the ensemble root directory
    ensemble_root_dir = os.path.join(data_root_dir, 'mu{:0>4}'.format(mu))
    #print(ensemble_root_dir)

    # List of the directories for each sub-ensemble (i.e. 'config.b190k1680mu0000j00s12t24')
    ensemble_dirs = []

    # Obtain a list of (all) dirs in the target directory
    for (dir_path, dir_names, file_names) in os.walk(ensemble_root_dir):
        for dir_name in dir_names:
            ensemble_dirs.append(os.path.join(ensemble_root_dir, dir_name))
        break

    configuration_dirs = []
    # Obtain a list of (all) files in the target directory
    for ensemble_dir in ensemble_dirs:
        for (dir_path, dir_names, file_names) in os.walk(ensemble_dir):
            for dir_name in dir_names:
                configuration_dirs.append(os.path.join(ensemble_dir, dir_name))
            break

    #print(configuration_dirs)

    # Iteratively walk through the directory structure to reach the JCN output file
    for configuration_dir in configuration_dirs:
        cooling_dir = os.path.join(configuration_dir, 'cool{:0>4}'.format(cools))

        search_dir = cooling_dir

        if fields is not '':
            search_dir = os.path.join(search_dir, 'fields{:}'.format(fields))

            if slab_size is not '':
                search_dir = os.path.join(search_dir, '{:}'.format(slab_size))

        jcn_file = os.path.join(search_dir, file_name)

        #print(jcn_file)

        if os.path.isfile(jcn_file):
            jcn_files.append(jcn_file)
        else:
            print('Could not locate a file at: %s' %jcn_file)

    # return the completed list of files
    return jcn_files
