import re

from Common.data_keys import *

#   These Regex's are used to parse data about the JCN structure
REGEX_PROCESS_DURATION = \
    R"Process\s+'(\w+[\w+\s+]+)'\s+duration:\s+(?:\d+\s+hours,\s+)?(?:\d+\s+minutes,\s+)?" \
    R"\d+\s+seconds\s+\((\d+)\s+milliseconds\)"
REGEX_PROCESS_MEMORY_DELTA = R"Process\s+'(\w+)'\smemory\s+stats\s+delta:"
REGEX_PEAK_WORKING_SET = R"PeakWorkingSetSize:\s+(\d+)B"
REGEX_WORKING_SET = R"WorkingSetSize:\s+(\d+)B"
REGEX_PROCESS_ID = R"Process\s+id:\s+(\d+)"


def __open_file__(filename):
    """
    Internal function for loading the stdlog.txt file and passing it as a string list for parsing
    :param filename: the name of the log file to be parsed
    :return: a list of strings representing the file text
    """
    #   Read contents of the file
    file = open(filename, 'r')
    file_contents = file.read().splitlines()
    file.close()

    return file_contents


def __parse_duration_stats__(file_contents, line_num):
    """
    Attempts to parse stats about a processes duration from the current line of the log file
    :param file_contents: the entire log as a list of strings
    :param line_num: current line number being parsed in the file
    :return: if successful a tuple containing the process name and duration in milliseconds
    """
    #   Find memory delta stats
    duration_stats_matches = re.search(REGEX_PROCESS_DURATION, file_contents[line_num])
    if duration_stats_matches is not None:
        #   Store in temporary variables
        proc_name = duration_stats_matches.group(1)
        duration_ms = duration_stats_matches.group(2)

        #   Return the process information
        return proc_name, duration_ms
    else:
        return None, None


def __parse_memory_stats__(file_contents, line_num):
    """
    Attempts to parse memory usage information from the current line of the log file
    :param file_contents: the entire log as a list of strings
    :param line_num: current line number being parsed in the file
    :return: if successful a tuple containing the process name, peak working memory and working memory stats
    """
    #   Find the start of the block
    process_memory_delta_matches = re.search(REGEX_PROCESS_MEMORY_DELTA, file_contents[line_num])

    if process_memory_delta_matches is not None:
        #   We should be expecting 3 more lines of information
        assert(len(file_contents) >= line_num + 3)

        #   Try to extract the information
        process_id_matches = re.search(REGEX_PROCESS_ID, file_contents[line_num + 1])
        process_peak_working_matches = re.search(REGEX_PEAK_WORKING_SET, file_contents[line_num + 2])
        process_working_matches = re.search(REGEX_WORKING_SET, file_contents[line_num + 3])

        #   Make sure it parsed correctly
        assert process_id_matches is not None
        assert process_peak_working_matches is not None
        assert process_working_matches is not None

        #   Store info in temporary variables
        proc_name = process_memory_delta_matches.group(1)
        proc_id = process_id_matches.group(1)
        proc_peak = process_peak_working_matches.group(1)
        proc_working = process_working_matches.group(1)

        #   Return the process information
        return proc_name, proc_id, proc_peak, proc_working
    return None, None, None, None


def __parse_jcn_stats__(file_contents, line_num, process_duration, process_peak_memory, process_working_memory):
    """
    Internal function for parsing the block relating to statistics on the JCN
    :param file_contents:
    :param line_num:
    :return:
    """
    #   First try to sample the process duration data
    process_name, duration = __parse_duration_stats__(file_contents, line_num)
    if process_name is not None and duration is not None:
        #   Store in the dictionary and return to outer loop
        process_duration[process_name] = duration
        return

    #   Now try to parse memory data
    process_name, proc_id, proc_peak, proc_working = __parse_memory_stats__(file_contents, line_num)
    if process_name is not None and proc_id is not None and proc_peak is not None and proc_working is not None:
        #   Store memory stats in the dictionaries
        process_peak_memory[process_name] = proc_peak
        process_working_memory[process_name] = proc_working
        return

    #   Nothing else to parse...
    return


def get_jcn_stats(filename):
    """
    Parses the stdlog file extracting information about process memory usage and duration
    :param filename: the file to be parsed
    :return: a dictionary containing the stats parsed
    """
    #   Create a dictionary to return the parsed data
    data = {}

    #   Load the file
    file_contents = __open_file__(filename)
    if file_contents is None:
        return None

    #   These will be passed to the parse functions to be filled
    process_duration = {}
    peak_working_memory = {}
    working_memory = {}

    for line_num in range(0, len(file_contents)):
        #   Loop over file contents (we'll use line numbers so that the line can be advanced manually, if required)
        # print("%s" %(line))

        #   Attempt to parse the current line, adding stats to the relevant dictionary
        __parse_jcn_stats__(file_contents, line_num,
                            process_duration=process_duration,
                            process_peak_memory=peak_working_memory,
                            process_working_memory=working_memory)

    #   Dump processed information to console
    #   print(process_duration)
    #   print(peak_working_memory)
    #   print(working_memory)

    #   Return the data as their own dictionaries for parsing later
    data[KEY_PROCESS_DURATION] = process_duration
    data[KEY_PROCESS_PEAK_MEMORY] = peak_working_memory
    data[KEY_PROCESS_WORKING_MEMORY] = working_memory

    #   Return the data as a dictionary.  This will allow us to add in further stats at a later date, if required.
    return data


#get_jcn_stats(R'/home/dean/Desktop/qcd_downloads/experiment_2_polyakov_fields/mu0000/conp0005/fields2/0.0625/cools_00_01/%5C260916_143809_stdlog.txt')
