import itertools


def get_line_format_array(ensemble_count=None):
    """
    Returns a format iterator, allowing multiple plots to be draw using a pre-defined set of styles.
    :param: ensemble_count ; optional parameter stating the number of ensembles to be plotted
    :return:
    """
    if ensemble_count is not None:
        if ensemble_count <= 3:
            format_array = itertools.cycle(('k-o', 'b-v', 'r-^'))
            return format_array
        elif ensemble_count <= 6:
            format_array = itertools.cycle(('k-o', 'b-v', 'r-^', 'g-<', 'm->', 'c-d'))
            return format_array
        elif ensemble_count <= 8:
            format_array = itertools.cycle(('k-x', 'k-o', 'r-x', 'r-o', 'g-x', 'g-o', 'b-x', 'b-o'))
            return format_array

    format_array =  itertools.cycle(('k-o', 'k-v', 'k-^', 'k-<', 'k->', 'k-d',
                                        'b-o', 'b-v', 'b-^', 'b-<', 'b->', 'b-d',
                                        'r-o', 'r-v', 'r-^', 'r-<', 'r->', 'r-d',
                                        'g-o', 'g-v', 'g-^', 'g-<', 'g->', 'g-d',
                                        'm-o', 'm-v', 'm-^', 'm-<', 'm->', 'm-d'))
    return format_array


def get_dashed_line_format_array(ensemble_count=None):
    """
    Returns a format iterator, allowing multiple plots to be draw using a pre-defined set of styles.
    :param: ensemble_count ; optional parameter stating the number of ensembles to be plotted
    :return:
    """
    if ensemble_count is not None:
        if ensemble_count <= 3:
            format_array = itertools.cycle(('k--o', 'b--v', 'r--^'))
            return format_array
        elif ensemble_count <= 6:
            format_array = itertools.cycle(('k--o', 'b--v', 'r--^', 'g--<', 'm-->', 'c--d'))
            return format_array
        elif ensemble_count <= 8:
            format_array = itertools.cycle(('k--x', 'k--o', 'r--x', 'r--o', 'g--x', 'g--o', 'b--x', 'b--o'))
            return format_array

    format_array =  itertools.cycle(('k--o', 'k--v', 'k--^', 'k--<', 'k-->', 'k--d',
                                        'b--o', 'b--v', 'b--^', 'b--<', 'b-->', 'b--d',
                                        'r--o', 'r--v', 'r--^', 'r--<', 'r-->', 'r--d',
                                        'g--o', 'g--v', 'g--^', 'g--<', 'g-->', 'g--d',
                                        'm--o', 'm--v', 'm--^', 'm--<', 'm-->', 'm--d'))
    return format_array


def get_colour_from_format_string(fmt_string):
    return fmt_string[0]

