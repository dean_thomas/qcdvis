import re
from collections import namedtuple

JCN_VERTEX_COMMENT_REGEX = R"^#\s*vertex\s*=\s*{\s*id:\s*(\d+),\s*isovalues\s*=\s*{([\s*\d+,]*)\s*},\s*degree:\s*(\d+)\s*\(inDegree:\s*(\d+),\s*outDegree:\s*(\d+)\)\s*}"

jcn_vertex_stats = namedtuple('jcn_vertex_stats', ['degree', 'in_degree', 'out_degree', 'isovalues'])


def get_jcn_stats(filename):
    vertex_dict = {}

    try:
        file = open(filename, 'r')
        file_contents = file.readlines()
        file.close()

        for line in file_contents:
            # print('Parsing line: %s' %(line))
            id, stats = extract_jcn_vertex_properties(line)

            if id is not None:
                #   Add to the dictionary
                vertex_dict[id] = stats

        print('Extracted {:} vertices from JCN: {:}'.format(len(vertex_dict), filename))

        return vertex_dict
    except FileNotFoundError:
        print("The file: '{:}' could not be found.".format(filename))
        return None


def extract_jcn_vertex_properties(line):
    try:
        matches = re.search(JCN_VERTEX_COMMENT_REGEX, line)

        #	If the line doesn't contain a comment detailing a vertex
        #	it will return None
        if (matches != None):
            vertex_id = int(matches.group(1))

            #	Store as a raw string for now (will need to be split into a list of values)
            #	if we want to do anything more advanced with them
            vertex_isovalues = matches.group(2)

            #	Only really interested in the 'degree' but we can use the in/outDegree
            #	degrees to test for consistency
            vertex_degree = int(matches.group(3))
            vertex_in_degree = int(matches.group(4))
            vertex_out_degree = int(matches.group(5))

            assert (vertex_degree == vertex_in_degree) and (vertex_degree == vertex_out_degree), "Unexpected values for in/out degree for vertex: %i" % (vertex_id)

            return vertex_id, jcn_vertex_stats(in_degree=vertex_in_degree,
                                               out_degree=vertex_out_degree,
                                               degree=vertex_degree,
                                               isovalues=vertex_isovalues)
    except ValueError:
        print('There was an error parsing the line: %s' % (line))

    return None, None