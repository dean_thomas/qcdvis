import os

cools = range(0, 21)
base_dir = R'/media/dean/raw_data/Dean/QCD Data/Field data/Pygrid/12x24/mu1000/config.b190k1680mu1000j02s12t24/conp0025'
filename = R'Topological charge density_t=12.vol1'

output_file = open(R'/home/dean/Desktop/tcd_cooling.visit_mu0500',mode='w')

for cool in cools:
    path = os.path.join(base_dir, 'cool{:0>4}'.format(cool))
    file = os.path.join(path, filename)

    output_file.write(file+'\n')

output_file.close()