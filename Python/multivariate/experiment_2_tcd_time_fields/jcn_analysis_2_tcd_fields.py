"""
Used to run an analysis on the JCN output created on the cluster for the 2 topological charge density fields
experiment.  This data can then be graphed using one of the other scripts in the directory.
"""
import math
import os
import pickle

from multivariate.experiment_2_polyakov_fields.data_keys import *

import Common.jcn_parser as jcn
import Common.jcn_stdout_parser as stdout
from multivariate.experiment_2_polyakov_fields.analysis.jcn_analysis_2_polyakov_fields import compute_array_average_and_variance
from multivariate.experiment_2_polyakov_fields.analysis.jcn_analysis_2_polyakov_fields import compute_jcn_vertex_degree_distribution


def merge_dicts(*dict_args):
    """
    Given any number of dicts, shallow copy and merge into a new dict,
    precedence goes to key value pairs in latter dicts.
    """
    result = {}
    for dictionary in dict_args:
        result.update(dictionary)
    return result


def save_cache_file(file_name, data):
    """
    Saves computed data to a cache file so that it can be processed later
    :param file_name: the file name to be used to store the cache data
    :param data: a dictionary of (key, value) pairs relating to computed attributes of the data
    :return: nothing
    """
    output_file = open(file_name, 'wb')

    #   Write the dictionary to the file
    pickle.dump(data, output_file)

    output_file.close()
    return


def load_cache_file(file_name):
    """
    Loads a cache file for pre-computed data
    :param file_name:
    :return:
    """
    if os.path.exists(file_name):
        try:
            output_file = open(file_name, 'rb')
            data = pickle.load(output_file)
            output_file.close()
            return data
        except pickle.UnpicklingError:
            print('Loading failed from file: "{:}"'.format(file_name))
            output_file.close()
    return None


def compute_average_and_variance(data_series):
    """
    Computes an average and a variance for a list of numeric inputs
    :param data_series:
    :return:
    """
    #   Compute the average for each stat for the configuration
    configuration_average = sum(data_series) / len(data_series)

    configuration_variance = 0
    for data_slice in data_series:
        configuration_variance += ((data_slice - configuration_average) ** 2) / len(data_series)

    if len(data_series) > 2:
        configuration_variance = math.sqrt(configuration_variance / (len(data_series) - 1))
    else:
        configuration_variance = math.sqrt(configuration_variance / (len(data_series)))

    return configuration_average, configuration_variance


def get_list_of_configuration_root_directories(ensemble_root_directory, cools):
    """
    Retreives a list of directories holding 4D slice data
    :param ensemble_root_directory:
    :param cools:
    :return:
    """
    configuration_directories = []
    for (dir_path, sub_dir_names, filenames) in os.walk(ensemble_root_directory):
        for sub_dir in sub_dir_names:
            #   Each sub_dir should be a configuration root (example: conp0005).
            configuration_base_dir = os.path.join(ensemble_root_directory, sub_dir)

            #   Try to find the correct level of cooling
            target_cooling_dir = os.path.join(configuration_base_dir, 'cool{:0>4}'.format(cools))

            if os.path.exists(target_cooling_dir):
                #   We also need to step through the field count and slab size folders
                target_configuration_dir = os.path.join(target_cooling_dir, 'fields2', '0.5')

                if os.path.exists(target_configuration_dir):
                    #   Finally, we have found the root for this configuration
                    configuration_directories.append(target_configuration_dir)

    print(configuration_directories)

    return configuration_directories


def analyse_ensemble(ensemble_root_directory, ensemble_name, cools):
    """
    Analyses an entire ensemble at a specified value of cooling
    :param ensemble_root_directory: root directory, where all the configurations can be found for a value of mu
    (example: G:\Dean\data_from_physics_cluster\2_tcd_fields_study\output\12x24\mu0000)
    :param ensemble_name: the name given to the ensemble (example: mu0250)
    :param cools: target number of cools to be analysed
    :return:
    """
    ensemble_vertex_counts = []
    ensemble_edge_counts = []
    ensemble_jacobi_numbers = []

    ensemble_vertex_degree_distribution_array = []

    #   Construct a filename for the output (example: mu0000_cool0000.cache)
    ensemble_cache_filename = '{:}_cool{:0>4}.cache'.format(ensemble_name, cools)

    #   Use a key,value map to store the parsed data
    ensemble_data = {}

    for configuration_root_directory in get_list_of_configuration_root_directories(ensemble_root_directory, cools):
        #   Now call the analysis on each individual configuration
        configuration_stats = analyse_multiple_configuration_slices(configuration_root_directory)

        if configuration_stats is not None:
            #   Values could come back as none if the output could not be parsed (corrupt or incorrectly configured
            #   experiments)
            if KEY_ENSEMBLE_VERTEX_COUNT in configuration_stats:
                ensemble_vertex_counts.append(configuration_stats.get(KEY_ENSEMBLE_VERTEX_COUNT))
            if KEY_ENSEMBLE_EDGE_COUNT in configuration_stats:
                ensemble_edge_counts.append(configuration_stats.get(KEY_ENSEMBLE_EDGE_COUNT))
            if KEY_ENSEMBLE_JACOBI_NUMBER in configuration_stats:
                ensemble_jacobi_numbers.append(configuration_stats.get(KEY_ENSEMBLE_JACOBI_NUMBER))

            if KEY_VERTEX_DEGREE_COUNT_DICT in configuration_stats:
                ensemble_vertex_degree_distribution_array.append(configuration_stats[KEY_VERTEX_DEGREE_COUNT_DICT])
        else:
            print("Was unable to analyse configurations in directory '{:}'".format(configuration_root_directory))

    #   Make sure it the configurations were correctly analysed
    assert len(ensemble_vertex_counts) == len(ensemble_edge_counts)
    assert len(ensemble_vertex_counts) == len(ensemble_jacobi_numbers)

    if len(ensemble_vertex_counts) > 0:
        #   Summary
        print('Configuration analysis complete.  Found {:} configurations.'.format(len(ensemble_vertex_counts)))
    else:
        #   No data was found for the ensemble.  Skip computing averages and saving cache.
        print('No configurations were found for the ensemble "{:}" in root directory {:}'
              .format(ensemble_name, ensemble_root_directory))
        return False

    #   First up, store the number of configurations we have analysed
    ensemble_data[KEY_NUMBER_OF_CONFIGURATIONS] = len(ensemble_vertex_counts)

    #   Compute the average and variance in the vertex count statistics and add the results to the data dictionary
    ensemble_vertex_count_average, ensemble_vertex_count_variance = compute_average_and_variance(
        ensemble_vertex_counts)
    print('Ensemble average vertex count: %d (variance: %d) from %i configurations'
          % (ensemble_vertex_count_average, ensemble_vertex_count_variance, len(ensemble_vertex_counts)))
    ensemble_data[KEY_ENSEMBLE_VERTEX_COUNT] = ensemble_vertex_count_average
    ensemble_data[KEY_ENSEMBLE_VERTEX_VARIANCE] = ensemble_vertex_count_variance

    #   Compute the average and variance in the vertex count statistics and add the results to the data dictionary
    print('Edge counts: {:}'.format(ensemble_edge_counts))
    ensemble_edge_count_average, ensemble_edge_count_variance = compute_average_and_variance(ensemble_edge_counts)
    print('Ensemble average edge count: %d (variance: %d) from %i configurations'
          % (ensemble_edge_count_average, ensemble_edge_count_variance, len(ensemble_edge_counts)))
    ensemble_data[KEY_ENSEMBLE_EDGE_COUNT] = ensemble_edge_count_average
    ensemble_data[KEY_ENSEMBLE_EDGE_VARIANCE] = ensemble_edge_count_variance

    #   Compute the average and variance in the edge count statistics and add the results to the data dictionary
    ensemble_jacobi_number_average, ensemble_jacobi_number_variance \
        = compute_average_and_variance(ensemble_jacobi_numbers)
    print('Ensemble average jacobi number: %d (variance: %d) from %i configurations'
          % (ensemble_jacobi_number_average, ensemble_jacobi_number_variance, len(ensemble_jacobi_numbers)))
    ensemble_data[KEY_ENSEMBLE_JACOBI_NUMBER] = ensemble_jacobi_number_average
    ensemble_data[KEY_ENSEMBLE_JACOBI_VARIANCE] = ensemble_jacobi_number_variance

    #   Compute the average and variance of the vertex degree distribution and add the results to the data dictionary
    ensemble_vertex_degree_distribution_average, ensemble_vertex_degree_distribution_variance = \
        compute_array_average_and_variance(ensemble_vertex_degree_distribution_array)
    print(ensemble_vertex_degree_distribution_average)
    print(ensemble_vertex_degree_distribution_variance)
    ensemble_data[KEY_VERTEX_DEGREE_COUNT_DICT] = ensemble_vertex_degree_distribution_average
    ensemble_data[KEY_VERTEX_DEGREE_VARIANCE_DICT] = ensemble_vertex_degree_distribution_variance

    #   Save the stats in a cache file
    save_cache_file(os.path.join(ensemble_root_directory, '..', ensemble_cache_filename), data=ensemble_data)

    return True


def basic_jcn_analyse_multiple_configuration_slices(slice_dirs):
    """
    Reads each configuration slice from the supplied directory list and computes an average for the conmfiguration
    :param slice_dirs: a list of directories containing the individual slices to be analysed
    :return: a dictionary containing the configuration average as a set of key, value pairs
    """
    #   Arrays to store the parsed data for each individual slice
    configuration_vertex_count_array = []
    configuration_edge_count_array = []
    configuration_jacobi_number_array = []

    #   Dictionary to return the configuration average (across all slices) as key,value pairs
    configuration_average_dict = {}

    #   Now visit each slice in turn extracting the relevant JCN configuration_average_dict
    for slice_dir in slice_dirs:
        #   Parse the stdout file
        jcn_vertex_count, jcn_edge_count, jacobi_number = analyse_configuration_slice(slice_dir)

        if jcn_vertex_count is not None:
            configuration_vertex_count_array.append(jcn_vertex_count)
        if jcn_edge_count is not None:
            configuration_edge_count_array.append(jcn_edge_count)
        if jacobi_number is not None:
            configuration_jacobi_number_array.append(jacobi_number)

    if len(configuration_vertex_count_array) > 0:
        #   Compute the average and variance for this configuration
        configuration_average_vertex_count, configuration_variance_vertex_count = compute_average_and_variance(
            configuration_vertex_count_array)
        configuration_average_dict[KEY_ENSEMBLE_VERTEX_COUNT] = configuration_average_vertex_count
        configuration_average_dict[KEY_ENSEMBLE_VERTEX_VARIANCE] = configuration_variance_vertex_count

        configuration_average_edge_count, configuration_variance_edge_count = compute_average_and_variance(
            configuration_edge_count_array)
        configuration_average_dict[KEY_ENSEMBLE_EDGE_COUNT] = configuration_average_edge_count
        configuration_average_dict[KEY_ENSEMBLE_EDGE_VARIANCE] = configuration_variance_edge_count

        configuration_average_jacobi_number, configuration_variance_jacobi_number = compute_average_and_variance(
            configuration_jacobi_number_array)
        configuration_average_dict[KEY_ENSEMBLE_JACOBI_NUMBER] = configuration_average_jacobi_number
        configuration_average_dict[KEY_ENSEMBLE_JACOBI_VARIANCE] = configuration_variance_jacobi_number

        #   For debugging
        print('Average JCN vertex count for configuration: %d (variance: %d) from %i slices'
              % (configuration_average_vertex_count, configuration_variance_vertex_count,
                 len(configuration_vertex_count_array)))
        print('Average JCN edge count for configuration: %d (variance: %d)  from %i slices'
              % (configuration_average_edge_count, configuration_variance_edge_count, len(configuration_edge_count_array)))
        print('Average Jacobi number for configuration: %d (variance: %d) from %i slices'
              % (configuration_average_jacobi_number, configuration_variance_jacobi_number,
                 len(configuration_jacobi_number_array)))
    else:
        print('Unable to correctly parse any configuration_average_dict for configuration')
        return None

    return configuration_average_dict


def jcn_connectivity_analyse_multiple_configuration_slices(configuration_root_directory,
                                                           slice_dirs,
                                                           use_cache=True):

    configuration_average_cache_filename = os.path.join(configuration_root_directory,
                                                        'vertex_distribution_average.cache')

    if os.path.exists(configuration_average_cache_filename) and use_cache:
        #   Try to load from the cache if we can
        result = load_cache_file(configuration_average_cache_filename)
        if result is not None:
            return result

    #   Dictionary to return the configuration average (across all slices) as key,value pairs
    configuration_vertex_degree_average_dict = {}

    vertex_degree_slice_array = []

    #   Now visit each slice in turn extracting the relevant JCN configuration_average_dict
    for slice_dir in slice_dirs:
        #   Next parse the jcn stats file (for vertex degree distribution)
        jcn_file = None
        for (dir_path, dir_names, filenames) in os.walk(slice_dir):
            for filename in filenames:
                if '%5Cjoint contour net.jcn)' in filename:
                    jcn_file = os.path.join(slice_dir, filename)
                    break
        if jcn_file is not None:
            #   Parse the jcn file
            slice_stats = jcn.get_jcn_stats(jcn_file)

            if slice_stats is not None:
                #   Add the data parsed from the log to the return data
                #   BUT first compute the vertex degree distribution
                vertex_degree_slice_array.append(compute_jcn_vertex_degree_distribution(slice_stats))

    #   Once this has computed we should have an array of dicts mapping vertex degree to counts, now we need to
    #   flatten this array
    for slice in vertex_degree_slice_array:
        for key, value in slice.items():
            configuration_vertex_degree_average_dict[key] = configuration_vertex_degree_average_dict.get(key, 0) + value

    #   Now compute the average by dividing by the number of slices
    for key, value in configuration_vertex_degree_average_dict.items():
        configuration_vertex_degree_average_dict[key] = configuration_vertex_degree_average_dict.get(key) / len(vertex_degree_slice_array)

    print(configuration_vertex_degree_average_dict)

    #   Wrap up a dict to return, should we want to compute any other values here later...
    result = {KEY_VERTEX_DEGREE_COUNT_DICT: configuration_vertex_degree_average_dict}

    save_cache_file(configuration_average_cache_filename, result)
    return result


def analyse_multiple_configuration_slices(configuration_root_directory):
    """
    Calls the analyse_configuration_slice method for each time slice in the current configuration.  Then computes
     and caches statistics for each.
    :param configuration_root_directory: root directory for an entire configuration containing multiple JCNs
    (example: G:\Dean\data_from_physics_cluster\2_tcd_fields_study\output\mu0000\conp0005\cool0000\fields2\0.5)
    :return:
    """

    #   Attempt to load pre-processed values from cache
    cache_filename = os.path.join(configuration_root_directory, 'jcn_stats.cache')
    configuration_average_dict = load_cache_file(cache_filename)

    if configuration_average_dict is None or type(configuration_average_dict) is not dict:
        #   Loading cache failed, so recompute
        print('Analysing configuration: {:}'.format(configuration_root_directory))

        #   Build a list of sub directories to visit
        slice_dirs = []
        for (dir_path, dir_names, filenames) in os.walk(configuration_root_directory):
            for sub_dir in dir_names:
                slice_dirs.append(os.path.join(configuration_root_directory, sub_dir))

        #   Now perform the each of analysis
        #   1.  Basic analysis - number of vertices, jacobi nodes etc of the JCN graph
        #   2.  Connectivity analysis - distribution of vertex (degree) connectivity in the JCN
        #   3.  Slab analysis - persistence stats of the generated slabs
        #   4.  Performance analysis - memory requirements and timing
        basic_stats_dict = basic_jcn_analyse_multiple_configuration_slices(slice_dirs)
        connectivity_stats_dict = jcn_connectivity_analyse_multiple_configuration_slices(configuration_root_directory,
                                                                                         slice_dirs)

        if basic_stats_dict is not None and connectivity_stats_dict is not None:
            #   Merge the output of each analysis into one single dict for return
            configuration_average_dict = merge_dicts(basic_stats_dict, connectivity_stats_dict)

            #   Caching for next time
            save_cache_file(cache_filename, configuration_average_dict)
        else:
            return None

    #   Return the configuration averages for use in ensemble analysis
    return configuration_average_dict


def analyse_configuration_slice(slice_root_directory):
    """
    Extracts JCN statistics for a single bi-variate JCN slice.  Caches them for next time.
    :param slice_root_directory: the directory holding the JCN data for a single slice of the JCN
    (example: G:\Dean\data_from_physics_cluster\2_tcd_fields_study\output\mu0000\conp0005\cool0000\fields2\0.5\t_01_02)
    :return:
    """
    jcn_stdout_file = None
    for (dir_path, dir_names, filenames) in os.walk(slice_root_directory):
        for filename in filenames:
            if 'stdout.txt' in filename:
                jcn_stdout_file = os.path.join(slice_root_directory, filename)
                break

    if jcn_stdout_file is not None:
        print('Analysing slice data: {:}'.format(jcn_stdout_file))
        jcn_stats  = stdout.get_jcn_stats(jcn_stdout_file)

        if jcn_stats is not None:
            jcn_vertex_count, jcn_edge_count, jacobi_number = jcn_stats

            #   print('JCN vertex count: %i' % jcn_vertex_count)

            return jcn_vertex_count, jcn_edge_count, jacobi_number
        else:
            return None, None, None
    else:
        return None, None, None


def analyse_experiment(root_directory, cool_array):
    """
    Roots the analysis on all values of mu
    :param root_directory:
    :return:
    """
    for (dir_path, dir_names, filenames) in os.walk(root_directory):
        for dir_name in dir_names:
            for cool in cool_array:
                print('Ensemble: {:} at {:} cools'.format(dir_name, cool))
                result = analyse_ensemble(os.path.join(root_directory, dir_name), ensemble_name=dir_name, cools=cool)
                print('Ensemble: {:} at cool {:} completed successfully: {:}'
                      .format(dir_name, cool, result))
        break

#delete_cache(R'G:\Dean\data_from_physics_cluster\2_tcd_fields_study\output\12x24')

#if __name__ == "__main__":
#    analyse_experiment(R'E:\Dean\data_from_physics_cluster\12x24\2_tcd_fields_study\output', cool_array=[0, 5, 10, 15, 20])

if __name__ == "__main__":
    #   delete_cache(R'/media/dean/jcn_data/Dean/data_from_physics_cluster/12x16/experiment_2_tcd_time_fields')
    analyse_experiment(R'/media/dean/jcn_data/Dean/data_from_physics_cluster/12x24/experiment_2_tcd_time_fields',
                       cool_array=[0, 5, 10, 15, 20])
    analyse_experiment(R'/media/dean/jcn_data/Dean/data_from_physics_cluster/12x16/experiment_2_tcd_time_fields',
                       cool_array=[0, 5, 10, 15, 20])