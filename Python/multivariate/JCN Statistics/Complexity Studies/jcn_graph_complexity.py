"""
Graphs the complexity of the JCN in terms of the number edges and vertices with regard to slab_size and frequency
"""

import matplotlib.pyplot as plt
import numpy as np
import re
import sys
from collections import defaultdict
from os import sys
from os import walk
from os import path
from os.path import isdir
from os.path import join
from collections import namedtuple

import Common.jcn_stdout_parser as jcn_out

#REGEX_MEMORY_DELTA = R"Process '(.*)' memory stats delta:"
#REGEX_MEMORY_DELTA_PROC_NAME = 1

#REGEX_PROCESS_ID = R"\s+Process id: (\d+)"
#REGEX_PROCESS_ID_PID = 1



#REGEX_PEAK_WORKING_SET = R"\s+PeakWorkingSetSize: ([-+]?(?:\d*[.])?\d+)B \( ([-+]?(?:\d*[.])?\d+)kB, ([-+]?(?:\d*[.])?\d+)MB, ([-+]?(?:\d*[.])?\d+)GB \)"
#REGEX_PEAK_WORKING_SET_BYTES = 1
#REGEX_PEAK_WORKING_SET_KB = 2
#REGEX_PEAK_WORKING_SET_MB = 3
#REGEX_PEAK_WORKING_SET_GB = 4


#REGEX_WORKING_SET = R"\s+WorkingSetSize: ([-+]?(?:\d*[.])?\d+)B \( ([-+]?(?:\d*[.])?\d+)kB, ([-+]?(?:\d*[.])?\d+)MB, ([-+]?(?:\d*[.])?\d+)GB \)"
#REGEX_WORKING_SET_BYTES = 1
#REGEX_WORKING_SET_KB = 2
#REGEX_WORKING_SET_MB = 3
#REGEX_WORKING_SET_GB = 4



JCN_VERTEX = 0
JCN_EDGE = 1
JCN_LOOP = 2
#PROC_PID = 1
#PROC_PEAK = 2
#PROC_WORKING = 3

LOG_FILE = "stdout"

#UNITS = 'GB'

#   Setup a standarised way for passing parameters for axes
AxisOptions = namedtuple('AxisOptions', ['title', 'range', 'log'])


def plot_jcn_data(figure, slab_size_array, vertex_data_array, fmt, mu, field_count, cools, configuration_count):
    #vertex_array = []
    #edge_array = []
    #   Make sure we're drawing to the right graph
    plt.figure(figure.number)

    #   todo: we probably don't need both the number of cools and fields - maybe one or the other?
    label = 'mu = {:} [cools = {:}, fields = {:}] ({:})'.format(mu, cools, field_count, configuration_count)

    plt.errorbar(x=slab_size_array, y=vertex_data_array, fmt=fmt, label=label)

    i = 0
    for v_count in vertex_data_array:
        if v_count > 150000:
            slab_size = slab_size_array[i]
            anno = plt.annotate('%.2d' % (v_count), xy=(slab_size, v_count), xycoords='data',
                            xytext=(0.5 + (5 * slab_size), v_count), textcoords='data',
                                         #bbox=dict(boxstyle="round", fc="white"),
                    arrowprops=dict(arrowstyle = '->', facecolor='black'),
                    horizontalalignment='left', verticalalignment='center',)
            i += 1

    #   plt.errorbar(slab_sizes, main_array_averages, yerr=main_array_standard_deviations, fmt=fmt)
    return


#def compute_average(stats_array):
    '''
    Computes the average of multiple simulation runs and returns a map of function names to averages and standard
    deviations
    :param stats_array:
    :return:
    '''
    ##   Store each array of values based upon the process it represents
    #peak_dictionary = defaultdict(list)
    #working_dictionary = defaultdict(list)

    #for run in stats_array:
    #    for processName, record in run.items():
    #        for elem in record:
    #            print('elem=' + str(elem))

    #            proc_name = elem[PROC_NAME]
    #            pid = elem[PROC_PID]
    #            peak_memory = elem[PROC_PEAK]
    #            working_memory = elem[PROC_WORKING]

     #           peak_dictionary[proc_name].append(peak_memory)
     #           working_dictionary[proc_name].append(working_memory)


    #print(peak_dictionary)
    #print(working_dictionary)

    #peak_average_dictionary = defaultdict(list)
    #peak_standard_deviation_dictionary = defaultdict(list)

    #peak_total = 0.0
    #for key, value_list in peak_dictionary.items():
#        arr = np.array(value_list)

        # Compute the average for the process
#        peak_average_dictionary[key] = np.mean(arr)

        #   Now the standard deviation
#        peak_standard_deviation_dictionary[key] = np.std(arr)

 #   print("Averages: %s" %(str(peak_average_dictionary)))
  #  print("Standard Deviations: %s" %(str(peak_standard_deviation_dictionary)))

   # return (peak_average_dictionary, peak_standard_deviation_dictionary)


def parse_sub_dir(root_dir, sub_dir):
    '''
    Iterates over all files within a sub-directory representing a fixed slab-size
    :param root_dir:
    :param sub_dir:
    :return:
    '''
    #   Subdir name should be a float representing the slab size
    slab_size = float(sub_dir)

    print("Parsing directory with slab size = %f" %(slab_size))

    # Initialize a list of files in the current directory
    files = []

    # Build absolute path to current dir
    absolute_path = join(root_dir, sub_dir)

    # Obtain a list of (all) files in the target directory
    for (dir_path, dir_names, file_names) in walk(absolute_path):
        for file_name in file_names:
            files.append(join(absolute_path, file_name))
        break

    stats = []
    for file in files:
        print("#############################################################")
        if LOG_FILE in file:
            print(file)

            jcn_stats = jcn_out.get_jcn_stats(file)
            print("jcn stats = %s" %(str(jcn_stats)))

            if jcn_stats is not None:
                stats.append(jcn_stats)

        #(average_mem_delta_average, average_mem_delta_sd) = compute_average(stats)

    return (slab_size, jcn_stats)


def parse_root_dir(root_dir, mu, conp, cools, field_count, fmt, vertex_figure, edge_figure=None, jacobi_figure=None):
    '''
    Loops over all sub-directories from a root (representing a cooling value)
    :param root_dir: the base directory of the complexity data to be parsed
    :param mu: the level of mu to be graphed
    :param conp: the configuration id to be graphed
    :param cools: the number of cools to be graphed
    :param field_count: the number of fields to be graphed
    :param fmt:
    :return:
    '''
    # Initialize a list of dirs in the current directory
    sub_dirs = []

    root_dir = join(root_dir, 'mu{:0>4}'.format(mu))
    root_dir = join(root_dir, 'conp{:0>4}'.format(conp))
    root_dir = join(root_dir, 'cool{:0>4}'.format(cools))
    root_dir = join(root_dir, 'fields{:}'.format(field_count))

    # Obtain a list of sub directories (relating to slab sizes) in the target directory
    for (dir_path, dir_names, filenames) in walk(root_dir):
        sub_dirs.extend(dir_names)
        break

    #   List of slab size, the averages for each slab size and the standard deviations for each slab size
    slab_size_array = []
    jcn_stats_array = []
    #standard_deviations = []

    # Process each sub dir (which represents a computation at specified slab size)
    for sub_dir in sub_dirs:
        print(sub_dir)

        (slab_size, jcn_stats) = parse_sub_dir(root_dir, sub_dir)

        slab_size_array.append(slab_size)
        jcn_stats_array.append(jcn_stats)
#        averages.append(proc_averages)
#        standard_deviations.append(proc_sds)

    #   Used to split the JCN stats into 3 separate graphs
    vertex_data = []
    edge_data = []
    jacobi_data = []

    #   Now split the 3 properties into separate lists for 3 plots
    for vertex_count, edge_count, jacobi_num in jcn_stats_array:
        vertex_data.append(vertex_count)
        edge_data.append(edge_count)
        jacobi_data.append(jacobi_num)

    configuration_count = 1

    print(slab_size_array)
    #   Always plot the vertex number stats
    plot_jcn_data(figure=vertex_figure,
                  vertex_data_array=vertex_data,
                  slab_size_array=slab_size_array,
                  fmt=fmt,
                  mu=mu,
                  field_count=field_count,
                  cools=cools,
                  configuration_count=configuration_count)

    if edge_figure is not None:
        #   Plot the edge number stats (if required)
        plot_jcn_data(figure=edge_figure,
                      vertex_data_array=edge_data,
                      slab_size_array=slab_size_array,
                      fmt=fmt,
                      mu=mu,
                      field_count=field_count,
                      cools=cools,
                      configuration_count=configuration_count)

    if jacobi_figure is not None:
        #   Plot the Jacobi number stats (if required)
        plot_jcn_data(figure=jacobi_figure,
                      vertex_data_array=jacobi_data,
                      slab_size_array=slab_size_array,
                      fmt=fmt,
                      mu=mu,
                      field_count=field_count,
                      cools=cools,
                      configuration_count=configuration_count)


def new_plot(graph_title, x_axis_options, y_axis_options):
    assert (isinstance(x_axis_options, AxisOptions))
    assert (isinstance(y_axis_options, AxisOptions))

    figure = plt.figure(graph_title)
    plt.title(graph_title)

    if x_axis_options.log:
        plt.xscale('log')

    if y_axis_options.log:
        plt.yscale('log')

    if x_axis_options.log:
        plt.xlabel(x_axis_options.title + ' (log scale)')
    else:
        plt.xlabel(x_axis_options.title)

    if y_axis_options.log:
        plt.ylabel(y_axis_options.title + ' (log scale)')
    else:
        plt.ylabel(y_axis_options.title)

    #   todo: add support for ranges (or None)

    return figure


def field_count_comparison_plot():
    """
    Plots the increase in complexity as the number of fields is increased
    :return:
    """

    #   Axis options for the 3 graphs
    x_axis = AxisOptions(title='slab size', range=None, log=False)
    vertices_y_axis = AxisOptions(title='vertex count', range=None, log=True)
    edges_y_axis = AxisOptions(title='edge count', range=None, log=True)
    jacobi_y_axis = AxisOptions(title='Jacobi number', range=None, log=True)

    #   Generate the 3 plots
    jcn_vertex_graph = new_plot('Joint Contour Net complexity (vertices)', x_axis, vertices_y_axis)
    jcn_edge_graph = new_plot('Joint Contour Net complexity (edges)', x_axis, edges_y_axis)
    jacobi_graph = new_plot('Joint Contour Net complexity (Jacobi number)', x_axis, jacobi_y_axis)

    #   Add data to the graphs
    parse_root_dir(R"E:\Dean\data_from_physics_cluster\memory_complexity_study", conp=52, mu=0, cools=10, field_count=1,
                   fmt="r-.o", vertex_figure=jcn_vertex_graph, edge_figure=jcn_edge_graph, jacobi_figure=jacobi_graph)
    # parse_root_dir(R"C:\Users\4thFloor\Desktop\cool0010\fields5", "-.o")
    parse_root_dir(R"E:\Dean\data_from_physics_cluster\memory_complexity_study", conp=52, mu=0, cools=10, field_count=6,
                   fmt="r--o", vertex_figure=jcn_vertex_graph, edge_figure=jcn_edge_graph, jacobi_figure=jacobi_graph)
    parse_root_dir(R"E:\Dean\data_from_physics_cluster\memory_complexity_study", conp=52, mu=0, cools=10,
                   field_count=12, fmt="r-o", vertex_figure=jcn_vertex_graph, edge_figure=jcn_edge_graph,
                   jacobi_figure=jacobi_graph)

    # parse_root_dir(R"E:\Dean\data_from_physics_cluster", "b-.D")
    # parse_root_dir(R"C:\Users\4thFloor\Desktop\cool0015\fields5", "-.D")
    # parse_root_dir(R"C:\Users\4thFloor\Desktop\cool0015\fields6", "b--D")
    # parse_root_dir(R"C:\Users\4thFloor\Desktop\cool0015\fields12", "b-D")

    # parse_root_dir(R"C:\Users\4thFloor\Desktop\cool0020\fields1", "k-.^")
    # parse_root_dir(R"C:\Users\4thFloor\Desktop\cool0020\fields5", "-.^")
    # parse_root_dir(R"C:\Users\4thFloor\Desktop\cool0020\fields6", "k--^")
    # parse_root_dir(R"C:\Users\4thFloor\Desktop\cool0020\fields12", "k-^")


def main(args):
    """entry point"""

    #   Graph the increase in complexity as the number of fields is increased
    field_count_comparison_plot()

    for f in plt.get_fignums():
        #   Make sure the figure is added to each plot
        plt.figure(f)
        plt.legend()

    plt.show()


    # if memDeltaArray is not None:
    #    print(memDeltaArray)
    #    plotMemoryUsage(memDeltaArray, 2, 'bytes')

if __name__ == '__main__':
    main(sys.argv[1:])