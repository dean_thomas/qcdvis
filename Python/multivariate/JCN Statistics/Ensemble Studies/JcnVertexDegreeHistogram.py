import re
from collections import namedtuple
from collections import defaultdict
import matplotlib.pyplot as plt
import sys
import os.path
from os import walk
import pickle
import math
from os.path import join
from os.path import isfile
import Common.GraphFormatIterator as gf
import Common.ErrorComputations as er
import Common.Mu as mu

#   Use LaTeX to provide type1 fonts for the graphs
plt.rcParams['ps.useafm'] = True
plt.rcParams['pdf.use14corefonts'] = True
plt.rcParams['text.usetex'] = True

JcnVertex = namedtuple('JcnVertex', 'id degree isoValues')

JCN_VERTEX_COMMENT_REGEX = R"^#\s*vertex\s*=\s*{\s*id:\s*(\d+),\s*isovalues\s*=\s*{([\s*\d+,]*)\s*},\s*degree:\s*(\d+)\s*\(inDegree:\s*(\d+),\s*outDegree:\s*(\d+)\)\s*}"


#	\brief		Parses a JCN file extract information about the connectivity
#				of the data
#	\author		Dean
#	\since		14-03-2016
#
def parseJcnFile(filename):
    #	Obtain a list of vertices from the file
    vertices = []

    try:
        file = open(filename, 'r')
        fileContents = file.readlines()
        file.close()

        for line in fileContents:
            # print('Parsing line: %s' %(line))
            jcnVertex = extractJcnVertexProperties(line)

            if (jcnVertex != None):
                vertices.append(jcnVertex)

        print('Extracted %i vertices from JCN: %s' % (len(vertices), filename))

        return vertices
    except FileNotFoundError:
        print("The file: '%s' could not be found." % (filename))


# \brief`		Extracts properties of JCN vertices from the
#				comments in the file using a Regex.
#	\author		Dean
#	\since		14-03-2016
#
def extractJcnVertexProperties(line):
    try:
        matches = re.search(JCN_VERTEX_COMMENT_REGEX, line)

        #	If the line doesn't contain a comment detailing a vertex
        #	it will return None
        if (matches != None):
            vertex_id = int(matches.group(1))

            #	Store as a raw string for now (will need to be split into a list of values)
            #	if we want to do anything more advanced with them
            vertex_isoValues = matches.group(2)

            #	Only really interested in the 'degree' but we can use the in/outDegree
            #	degrees to test for consistency
            vertex_degree = int(matches.group(3))
            vertex_inDegree = int(matches.group(4))
            vertex_outDegree = int(matches.group(5))

            assert (vertex_degree == vertex_inDegree) and (
                vertex_degree == vertex_outDegree), "Unexpected values for in/out degree for vertex: %i" % (vertex_id)

            return JcnVertex(vertex_id, vertex_degree, vertex_isoValues)
    except ValueError:
        print('There was an error parsing the line: %s' % (line))


def load_cache_file(file_name):
    cache_file_name = file_name + '.cache'

    if os.path.exists(cache_file_name):
        print('Loading from cache file: %s' %cache_file_name)
        output_file = open(cache_file_name, 'rb')
        try:
            partitioned_vertices = pickle.load(output_file)
        except pickle.UnpicklingError:
            print('Loading failed, will regenerate data')
            output_file.close()
            return None

        output_file.close()
        return partitioned_vertices
    else:
        return None


def save_cache_file(partitioned_vertices, file_name):
    output_file = open(file_name + '.cache', 'wb')
    pickle.dump(partitioned_vertices, output_file)
    output_file.close()

    return

# \brief		Partitions the list in to a number of sets
#				depending on the degree of the vertex
#	\author		Dean
#	\since		14-03-2016
#
def doVertexPartitioning(vertexList, file_name):
    #	Create a map for each vertex degree present in the JCN
    #	Each key points to a set of vertices with the specified degree
    partitionedVertices = defaultdict(set)

    if vertexList is not None:
        for vertex in vertexList:
            #	Use the degree as the key
            degree = vertex.degree

            #	Store the vertex in the corresponding set
            partitionedVertices[degree].add(vertex)

        for key, value in partitionedVertices.items():
            print('Degree %i : %i vertices' % (key, len(value)))

        #   Cache the file for later..
        save_cache_file(partitionedVertices, file_name)

        return partitionedVertices
    else:
        print('Unable to partition vertices')
        return None


#	\brief		Takes the partitioned vertex lists
#				and displays as a histogram
#	\author		Dean
#	\since		14-03-2016
#
def displayHistogram(partitionedVertices, pattern='-o'):
    """
    :param partitionedVertices:
    :param pattern:
    :return:
    """
    degreeVals = []
    freqVals = []

    for key, value in partitionedVertices.items():
        freqVals.append(len(value))
        degreeVals.append(key)

    plt.plot(degreeVals, freqVals, pattern)

    return


def add_jcn_to_histogram(file_name, pattern):
    """
    :param file_name:
    :param pattern:
    :return:
    """
    print('Parisng file: %s' % (file_name))

    partitionedVertices = load_cache_file(file_name)
    #print(partitionedVertices)

    if partitionedVertices is None:
        vertexList = parseJcnFile(file_name)
        partitionedVertices = doVertexPartitioning(vertexList, file_name)
    else:
        print('Loaded vertex table from cache')

    displayHistogram(partitionedVertices, pattern)
    print('Done.')

    return


def jcn_vertex_count(partitioned_vertices):
    """
    Counts the number of actual vertices in the JCN
    :param partitioned_vertices:
    :return:
    """
    total = 0

    # Add the totals from each bin
    if partitioned_vertices is not None:
        for key, value in partitioned_vertices.items():
            total += len(value)
    return total


def add_jcn_average_to_histogram(file_name_array, pattern, mu, normalise = True, cool_label = None):
    """
    Takes a list of configuration files, computes the average of each and
    adds the average to plot
    :param file_name_array: list of files for processing
    :param pattern: the line and vertex pattern for drawing the line
    :param normalise: if set to true, each value is normalised to the number of vertices
    in the JCN of each configuration
    :return: nothing
    """

    # Maximum value allowed on the x-axis
    x_max = 256

    # Ensemble data for the plots
    ensemble_vertex_degree = range(0, x_max)
    ensemble_freq = [0] * x_max

    # Number of configurations successfully read in
    configuration_count = 0

    # Temporary storage for the processed configurations
    input_configurations = []

    # Loop over each file
    for file_name in file_name_array:
        # Local binning of each frequency in the configuration
        configuration_freq = [0] * x_max

        # Load the data, partitioned into a table of vertex counts and the corresponding frequency
        # Use a cached copy of the data is it exists, else reparse the file
        partitioned_vertices = load_cache_file(file_name)
        if partitioned_vertices is None:
            vertex_list = parseJcnFile(file_name)
            partitioned_vertices = doVertexPartitioning(vertex_list, file_name)

        # Process the configuration
        if partitioned_vertices is not None:
            configuration_count += 1

            # If the data is to be normalised to the vertex count in each configuration JCN we compute it here
            # else set it to 1 for the division later
            if normalise:
                total_vertices_in_jcn = jcn_vertex_count(partitioned_vertices)
            else:
                total_vertices_in_jcn = 1

            # Load the data from the partitioned configuration input, add (and normalise) to the configuration total
            # and the global total
            for key, value in partitioned_vertices.items():
                ensemble_freq[key] += (len(value) / total_vertices_in_jcn)
                configuration_freq[key] += len(value) / total_vertices_in_jcn

            # Store the configuration values for computing the variances later
            input_configurations.append(configuration_freq)

    #   Compute the ensemble average for each bin
    if configuration_count > 0:
        for i in range(0, x_max):
            ensemble_freq[i] /= configuration_count

    # Compute the error bars for each sample in the ensemble
    ensemble_variance = er.compute_error_bars(ensemble_freq_array=ensemble_freq,
                                           configuration_array=input_configurations,
                                           configuration_count=configuration_count,
                                           x_max=x_max)

    if cool_label is None:
        plot_label = 'mu = {:} ({:})'.format(mu, configuration_count)
    else:
        plot_label = 'mu = {:} [{:} cools] ({:})'.format(mu, cool_label, configuration_count)

    # Plot the average, complete with error bars
    # plt.plot(ensemble_vertex_degree, ensemble_freq, pattern)
    plt.errorbar(ensemble_vertex_degree,
                 ensemble_freq,
                 yerr=ensemble_variance,
                 fmt=pattern,
                 label=plot_label)
    return


def get_list_of_jcn_files_for_ensemble(data_root_dir, mu, cools, fields, slab_size):
    # Filename as written by the JCN computation tool
    default_jcn_filename = '%5Cjoint contour net.jcn)'

    # The list of files to be returned
    jcn_files = []

    # Create a path for the ensemble root directory
    ensemble_root_dir = join(data_root_dir, 'mu{:0>4}'.format(mu))
    print(ensemble_root_dir)

    # List of the directories for each configuration in the ensemble
    configuration_dirs = []

    # Obtain a list of (all) files in the target directory
    for (dir_path, dir_names, file_names) in walk(ensemble_root_dir):
        for dir_name in dir_names:
            configuration_dirs.append(join(ensemble_root_dir, dir_name))
        break

    print(configuration_dirs)

    # Iteratively walk through the directory structure to reach the JCN output file
    for configuration_dir in configuration_dirs:
        cooling_dir = join(configuration_dir, 'cool{:0>4}'.format(cools))
        fields_dir = join(cooling_dir, 'fields{:}'.format(fields))
        slab_size_dir = join(fields_dir, '{:}'.format(slab_size))
        jcn_file = join(slab_size_dir, default_jcn_filename)

        if isfile(jcn_file):
            jcn_files.append(jcn_file)
        else:
            print('Could not locate a JCN file at: %s' %jcn_file)

    # return the completed list of files
    return jcn_files


def compute_multiple_histograms(mu_array, cool_array, number_of_fields, slab_size, normalise, x_log=False, y_log=False, fixed_cools=None, x_limits=None, y_limits=None):
    for cools in cool_array:
        plt.figure('Vertex degrees')
        plt.title('JCN vertex degrees (cools = {:}, slab size = {:}, time slices = {:})'.format(cools, slab_size, number_of_fields))
        plt.xlabel('degree')

        if x_log:
            plt.xscale('log')
        #    plt.xlim(10,250)
        #    ax = plt.gca()

        if y_log:
            plt.yscale('log')
         #   plt.ylim(0, 10)

        if x_limits is not None:
            plt.xlim(x_limits)

        if y_limits is not None:
            plt.ylim(y_limits)

        if normalise:
            plt.ylabel('freq (%)')
        else:
            plt.ylabel('freq')

        # Get a new format iterator for each graph
        solid_format_iterator = gf.get_line_format_array(len(mu_array))

        if fixed_cools is not None:
            dashed_format_iterator = gf.get_dashed_line_format_array(len(mu_array))

        for mu in mu_array:
            # Next line style
            solid_fmt = next(solid_format_iterator)

            if fixed_cools is not None:
                cool_label = cools
            else:
                cool_label = None

            add_jcn_average_to_histogram(
                get_list_of_jcn_files_for_ensemble(data_root_dir=R'G:\Dean\data_from_physics_cluster\12_tcd_fields_study',
                                                   mu=mu,
                                                   cools=cools,
                                                   fields=number_of_fields,
                                                   slab_size=0.25),
                solid_fmt, mu=mu, normalise=normalise, cool_label=cool_label)

            if fixed_cools is not None:
                # Next dashed line style
                dashed_fmt = next(dashed_format_iterator)

                add_jcn_average_to_histogram(
                    get_list_of_jcn_files_for_ensemble(data_root_dir=R'G:\Dean\data_from_physics_cluster\12_tcd_fields_study',
                                                       mu=mu,
                                                       cools=fixed_cools,
                                                       fields=number_of_fields,
                                                       slab_size=0.25),
                    dashed_fmt, mu=mu, normalise=normalise, cool_label=fixed_cools)
        plt.legend()

    plt.show()

    return




def main(args):
    """
    entry point
    """
    compute_multiple_histograms(mu_array=mu.get_full_mu_array(),
                                cool_array=[20],
                                number_of_fields=12,
                                slab_size=0.25,
                                normalise=False,
                                x_log=True,
                                y_log=False)
    #compute_histograms_12_slices(cools=20, fields=12, slab_size=0.25, normalise=False)

if __name__ == '__main__':
    main(sys.argv[1:])
