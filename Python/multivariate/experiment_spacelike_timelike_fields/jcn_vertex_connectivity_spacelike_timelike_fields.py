import os

import matplotlib.pyplot as plt
from matplotlib.ticker import ScalarFormatter
from multivariate.experiment_2_polyakov_fields.data_keys import *

import Common.GraphFormatIterator as gf
from multivariate.experiment_2_polyakov_fields.analysis.jcn_analysis_2_polyakov_fields import load_cache_file

#   Use LaTeX to provide type1 fonts for the graphs
plt.rcParams['ps.useafm'] = True
plt.rcParams['pdf.use14corefonts'] = True
plt.rcParams['text.usetex'] = True

#   Set defaults for saving output
plt.rcParams["savefig.directory"] = os.path.dirname(R'/home/dean/Documents/LaTeX/latex-documents/'
                                                    'jcn_ensemble_computations/figures/plaquette_fields/')
plt.rcParams["savefig.format"] = 'pdf'


FIG_WIDTH_CM = 30
FIG_HEIGHT_CM = 15


def create_figure(experiment_root_directory, mu_array, slab_size, first_cool, last_cool, cooling_step=1,
                  hold=False, dashed_line=False, x_log_axis=True,
                  figure_label=None,data_label=None, x_axis_limit = None):
    for cool_a in range(first_cool, last_cool, cooling_step):
        #   Setup axis and titles
        fig = plt.figure('average_jcn_connectivity_stats_at_cools_{:}'.format(cool_a),
                         figsize=(FIG_WIDTH_CM / 2.54, FIG_HEIGHT_CM / 2.54))
        if figure_label is None:
            plt.title('Average JCN connectivity stats at cools {:} (slab size = {:}'.format(cool_a, slab_size))
        else:
            plt.title('Average JCN connectivity stats at cools {:} ({:}, slab size = {:})'.format(cool_a, figure_label, slab_size))
        plt.xlabel('jcn vertex degree')
        plt.ylabel('freq')
        if x_log_axis:
            plt.xscale('log')
            ax = plt.gca()
            ax.get_xaxis().set_major_formatter(ScalarFormatter())

        if x_axis_limit is not None:
            plt.xlim(0, x_axis_limit)

        # get a new format iterator for each graph
        if dashed_line:
            format_iterator = gf.get_dashed_line_format_array(len(mu_array))
        else:
            format_iterator = gf.get_line_format_array(len(mu_array))

        #   This will hold the values for the y-axis, along with variances for error bars for each plot
        for mu in mu_array:
            #   cache filename for the current ensemble
            ensemble_cache_filename = 'mu{:0>4}_cool{:0>4}_slabsize_{:}.cache' \
                .format(mu, cool_a, slab_size)
            path_to_ensemble_data_cache = os.path.join(experiment_root_directory, ensemble_cache_filename)

            #   get the next format style
            fmt = next(format_iterator)

            #   load the data from the cache for the current ensemble
            if os.path.exists(path_to_ensemble_data_cache):
                data = load_cache_file(path_to_ensemble_data_cache)

                vertex_degree_count = data.get(KEY_VERTEX_DEGREE_COUNT_DICT)
                vertex_degree_variance = data.get(KEY_VERTEX_DEGREE_VARIANCE_DICT)

                if vertex_degree_count is not None:
                    #   Find the highest degree vertex in the data and create arrays to hold the data and errors
                    vertex_degree_array = [0]*(max(vertex_degree_count.keys())+1)
                    vertex_degree_error_array = [0]*(max(vertex_degree_count.keys())+1)

                    #   Map the sparse data to arrays for display
                    for key, value in vertex_degree_count.items():
                        vertex_degree_array[key] = value
                    #   Map the sparse data to arrays for display
                    for key, value in vertex_degree_variance.items():
                        vertex_degree_error_array[key] = value

                    #   count the number of input configurations (just measure one of the other arrays in the file)
                    configurations_in_ensemble=data.get(KEY_NUMBER_OF_CONFIGURATIONS)

                    if data_label is None:
                        #   No label text specified for legend, just print chemical potential
                        plt.errorbar(x=range(0, (max(vertex_degree_count.keys()) + 1)),
                                     y=vertex_degree_array,
                                     yerr=vertex_degree_error_array,
                                     fmt=fmt,
                                     label='$\mu$ = {:} [{:}]'.format(mu / 1000, configurations_in_ensemble))
                    else:
                        #   label text specified for legend append this to the legend
                        plt.errorbar(x=range(0, (max(vertex_degree_count.keys())+1)),
                                     y=vertex_degree_array,
                                     yerr=vertex_degree_error_array,
                                     fmt=fmt,
                                     label='$\mu$ = {:} ({:}) [{:}]'
                                     .format(mu / 1000, data_label, configurations_in_ensemble))
                else:
                    print('Unable to find a vertex connectivity data in file {:}.'
                          '  Perhaps you need to re-run the analysis?'.format(path_to_ensemble_data_cache))
            else:
                print('Unable to find a cache file at: {:}.  Perhaps you need to re-run the analysis?'
                      .format(path_to_ensemble_data_cache))

        plt.legend(loc='best', ncol=(len(mu_array)//7)+1).draggable()

    if not hold:
        #   Only show if we aren't expecting extra data
        plt.show()

    return


def create_overview_graph_full_cold_lattice():
    """
    Plots all available values of mu on the cold lattice (12^3 . 24)
    :return:
    """
    create_figure(experiment_root_directory=
                  R'/media/dean/jcn_data/Dean/data_from_physics_cluster/12x24/experiment_spacelike_timelike_fields',
                  mu_array=[0, 300, 325, 350, 375, 400, 425, 450, 500, 550,
                            600, 650, 700, 750, 800, 850, 900, 1000, 1100],
                  first_cool=0,
                  last_cool=21,
                  cooling_step=5,
                  dashed_line=False,
                  figure_label='full $12^3\cdot24$ data set')
    return


def create_overview_graph_full_hot_lattice(slab_size):
    """
    Plots all available values of mu on the cold lattice (12^3 . 16)
    :return:
    """
    create_figure(experiment_root_directory=
                  R'/media/dean/jcn_data/Dean/data_from_physics_cluster/12x16/experiment_spacelike_timelike_fields',
                  mu_array=[0, 300, 400, 450, 500, 550, 600, 650, 700, 800, 900],
                  first_cool=0,
                  last_cool=21,
                  cooling_step=5,
                  dashed_line=True,
                  figure_label = 'full $12^3\cdot16$ data set',
                  slab_size=slab_size)
    return


def create_lattice_comparison_multiple_mu(slab_size):
    """
    Creates a graph with the two lattices plotted together at common chemical potentials across the full range
    :return: None
    """
    # create_figure(experiment_root_directory=
    #               R'/media/dean/jcn_data/Dean/data_from_physics_cluster/12x24/experiment_2_tcd_time_fields',
    #               data_label='$12^3\cdot24$',
    #               mu_array=[0, 300, 500, 700, 900],
    #               first_cool=0,
    #               last_cool=21,
    #               cooling_step=5,
    #               dashed_line=False,
    #               hold=True,
    #               figure_label='lattice comparison')

    create_figure(experiment_root_directory=
                  R'/media/dean/jcn_data/Dean/data_from_physics_cluster/12x16/experiment_spacelike_timelike_fields',
                  data_label='$12^3\cdot16$',
                  mu_array=[0, 300, 500, 700, 900],
                  first_cool=0,
                  last_cool=21,
                  cooling_step=5,
                  dashed_line=True,
                  figure_label='lattice comparison',
                  slab_size=slab_size)
    return


def create_lattice_comparison_deconfinement_region_graph(slab_size):
    """
    Compares the two lattices in the regions where de-confinement is expected
    :return:
    """
    # create_figure(experiment_root_directory=
    #               R'/media/dean/jcn_data/Dean/data_from_physics_cluster/12x24/experiment_2_tcd_time_fields',
    #               data_label='$12^3\cdot24$',
    #               mu_array=[550, 600, 650, 700, 750, 800, 850, 900],
    #               first_cool=0,
    #               last_cool=21,
    #               cooling_step=5,
    #               dashed_line=False,
    #               hold=True,
    #               figure_label='lattice comparison',
    #               x_log_axis=False,
    #               x_axis_limit=10)

    create_figure(experiment_root_directory=
                  R'/media/dean/jcn_data/Dean/data_from_physics_cluster/12x16/experiment_spacelike_timelike_fields',
                  data_label='$12^3\cdot16$',
                  mu_array=[550, 600, 650, 700, 750, 800, 850, 900],
                  first_cool=0,
                  last_cool=21,
                  cooling_step=5,
                  dashed_line=True,
                  figure_label='lattice comparison',
                  x_log_axis=False,
                  x_axis_limit=10,
                  slab_size=slab_size)
    return


if __name__ == "__main__":

    #create_overview_graph_full_cold_lattice()
    #create_overview_graph_full_hot_lattice(0.0625)
    create_lattice_comparison_multiple_mu(0.0625)
    create_lattice_comparison_deconfinement_region_graph(0.0625)

    create_overview_graph_full_hot_lattice(0.03125)
    create_lattice_comparison_multiple_mu(0.03125)
    create_lattice_comparison_deconfinement_region_graph(0.03125)