import os

import matplotlib.pyplot as plt
from multivariate.experiment_2_polyakov_fields.data_keys import  *

import Common.GraphFormatIterator as gf
from multivariate.experiment_2_tcd_time_fields.jcn_analysis_2_tcd_fields import load_cache_file

#   Use LaTeX to provide type1 fonts for the graphs
plt.rcParams['ps.useafm'] = True
plt.rcParams['pdf.use14corefonts'] = True
plt.rcParams['text.usetex'] = True

#   Set defaults for saving output
plt.rcParams["savefig.directory"] = os.path.dirname(R'/home/dean/Documents/LaTeX/latex-documents/'
                                                    'jcn_ensemble_computations/figures/plaquette_fields/')
plt.rcParams["savefig.format"] = 'pdf'


def create_figure(experiment_root_directory, slab_size, label, mu_array, cool_array, hold = True, dashed_line = False):
    """
    Creates the graphs to display the simple Joint Contour Analysis against chemical potential
    :param experiment_root_directory: the directory that holds the data downloaded from the cluster
    :param label: text to be appended to the graph legend (can be formatted for LaTeX)
    :param mu_array: array of values of mu to display.  These will form the x-axis of the plot
    :param cool_array: array of values of cooling.  Each level of cooling will form it's own plot.
    :param hold: if set to true the graph will not be displayed after creation, allowing further data to be plotted
    for comparison.
    :param dashed_line: if set to True, the line iterator will use a dashed variation.
    :return: None
    """
    for cools in cool_array:
        #   Setup axis and titles
        plt.figure('average_jcn_stats_at_{:}_cools'.format(cools))
        plt.title('Average JCN stats at {:} cools (slab size = {:})'.format(cools, slab_size))
        plt.xlabel('chemical potential ($\mu$)')
        plt.ylabel('ensemble average count')

        # get a new format iterator for each graph
        if dashed_line:
            format_iterator = gf.get_dashed_line_format_array(3)
        else:
            format_iterator = gf.get_line_format_array(3)

        #   This will hold the values of mu to use to label the data
        x_axis_labels = []

        #   This will hold the values for the y-axis, along with variances for error bars for each plot
        data_array_vertex_count = []
        data_array_vertex_variance = []

        data_array_edge_count = []
        data_array_edge_variance = []

        data_array_jacobi_count = []
        data_array_jacobi_variance = []

        #   load the data for each value of mu
        for mu in mu_array:
            #   cache filename for the current ensemble
            ensemble_cache_filename = 'mu{:0>4}_cool{:0>4}_slabsize_{:}.cache'.format(mu, cools, slab_size)
            path_to_ensemble_data_cache = os.path.join(experiment_root_directory, ensemble_cache_filename)

            #   load the data from the cache for the current ensemble
            if os.path.exists(path_to_ensemble_data_cache):
                data = load_cache_file(path_to_ensemble_data_cache)

                #   Test the data type of returned cache for backward compatibility (previously was multiple floats)
                if type(data) is dict:
                    #   Bring down to normal values of mu (example: 0.25)
                    x_axis_labels.append('{:}'.format(mu / 1000))

                    print(data, ensemble_cache_filename)

                    #   Add the data to each series included pre-computed variances
                    data_array_vertex_count.append(data.get(KEY_ENSEMBLE_VERTEX_COUNT))
                    data_array_vertex_variance.append(data.get(KEY_ENSEMBLE_VERTEX_VARIANCE))

                    data_array_edge_count.append(data.get(KEY_ENSEMBLE_EDGE_COUNT))
                    data_array_edge_variance.append(data.get(KEY_ENSEMBLE_EDGE_VARIANCE))

                    data_array_jacobi_count.append(data.get(KEY_ENSEMBLE_JACOBI_NUMBER))
                    data_array_jacobi_variance.append(data.get(KEY_ENSEMBLE_JACOBI_VARIANCE))
                else:
                    print("Expected data dictionary in cache file, instead got {:}.  You probably need to re-run"
                          "the analysis on ensemble at '{:}'".format(type(data), path_to_ensemble_data_cache))

            else:
                print('Unable to find a cache file at: {:}.  Perhaps you need to re-run the analysis?'
                      .format(path_to_ensemble_data_cache))

        #   Plot the JCN vertex count data
        fmt = next(format_iterator)

        if len(data_array_vertex_count) == 0:
            #   If none of the ensembles have been analysed don't plot the graph (it causes an matplotlib internal
            #   error
            print('No data to plot.  Skipping graph.')
        else:
            #   Else go ahead and create the graph
            plt.errorbar(x=x_axis_labels, y=data_array_vertex_count, yerr=data_array_vertex_variance, fmt=fmt,
                         label='JCN vertex count ({:})'.format(label))

            #   Plot the JCN edge count data
            fmt = next(format_iterator)
            plt.errorbar(x=x_axis_labels, y=data_array_edge_count, yerr=data_array_edge_variance, fmt=fmt,
                         label='JCN edge count ({:})'.format(label))

            #   Plot the Jacobi Number data
            fmt = next(format_iterator)
            plt.errorbar(x=x_axis_labels, y=data_array_jacobi_count, yerr=data_array_jacobi_variance, fmt=fmt,
                         label='jacobi number ({:})'.format(label))

            #   Add a legend to the graph
            plt.legend(loc='best').draggable()

    if not hold:
        #   Only show if we aren't expecting extra data
        plt.show()

    return


def create_jacobi_ratio_figure(experiment_root_directory, slab_size, label, mu_array, cool_array, hold=True, dashed_line=False):
    """
    Plots the ratio of regular JCN nodes to Jacobi nodes across the chemical potential range
    :param experiment_root_directory:
    :param label:
    :param mu_array:
    :param cool_array:
    :param hold:
    :param dashed_line:
    :return:
    """
    for cools in cool_array:
        #   Setup axis and titles
        plt.figure('jacobi_node_ratio_at_{:}_cools'.format(cools))
        plt.title('Ratio of Jacobi nodes / JCN vertices at {:} cools (slab size = {:})'.format(cools, slab_size))
        plt.xlabel('chemical potential ($\mu$)')
        plt.ylabel('ensemble average ratio')

        # get a new format iterator for each graph
        if dashed_line:
            format_iterator = gf.get_dashed_line_format_array(3)
        else:
            format_iterator = gf.get_line_format_array(3)

        # This will hold the values of mu to use to label the data
        x_axis_labels = []

        #   This will hold the values for the y-axis, along with variances for error bars for each plot
        data_array_jacobi_ratio = []

        #   load the data for each value of mu
        for mu in mu_array:
            #   cache filename for the current ensemble
            ensemble_cache_filename = 'mu{:0>4}_cool{:0>4}_slabsize_{:}.cache'.format(mu, cools, slab_size)
            path_to_ensemble_data_cache = os.path.join(experiment_root_directory, ensemble_cache_filename)

            #   load the data from the cache for the current ensemble
            if os.path.exists(path_to_ensemble_data_cache):
                data = load_cache_file(path_to_ensemble_data_cache)

                #   Bring down to normal values of mu (example: 0.25)
                x_axis_labels.append('{:}'.format(mu / 1000))

                #   Add the data to each series included pre-computed variances
                vertex_count = data.get(KEY_ENSEMBLE_VERTEX_COUNT)
                jacobi_count = data.get(KEY_ENSEMBLE_JACOBI_NUMBER)

                data_array_jacobi_ratio.append(jacobi_count / vertex_count)

            else:
                print('Unable to find a cache file at: {:}.  Perhaps you need to re-run the analysis?'
                      .format(path_to_ensemble_data_cache))

        if len(data_array_jacobi_ratio) == 0:
            #   If none of the ensembles have been analysed don't plot the graph (it causes an matplotlib internal
            #   error
            print('No data to plot.  Skipping graph.')
        else:
            # Else go ahead and create the graph
            #   Plot the JCN vertex count data
            fmt = next(format_iterator)
            plt.errorbar(x=x_axis_labels, y=data_array_jacobi_ratio, fmt=fmt,
                         label='ratio ({:})'.format(label))

            #   Add a legend to the graph
            plt.legend(loc='best')

    if not hold:
        #   Only show if we aren't expecting extra data
        plt.show()

    return


def create_comparison_graph(slab_size):
    """
    Creates a graph plotting the two lattices against one another for a side-by-side comparison of the output
    :return: None
    """
    #   Missing JCN data is commented out...
    create_figure(
        experiment_root_directory=R'/media/dean/jcn_data/Dean/data_from_physics_cluster/12x24/experiment_spacelike_timelike_fields',
        label='$12^3\cdot24$',
        mu_array=[0, 300, 325, 350, 375, 400, 425, 450, 475, 500, 550, 600, 650, 700, 750, 800, 850, 900, 1000, 1100],
        cool_array=[0, 5, 10, 15, 20],
        slab_size=slab_size)

    #   Missing JCN data is commented out...
    create_figure(
        experiment_root_directory=R'/media/dean/jcn_data/Dean/data_from_physics_cluster/12x16/experiment_spacelike_timelike_fields',
        label='$12^3\cdot16$',
        mu_array=[0, 300, 400, 450, 500, 550, 600, 650, 700, 800, 900],
        cool_array=[0, 5, 10, 15, 20],
        dashed_line=True,
        slab_size = slab_size)

    plt.show()
    return


def create_separate_graphs(slab_size):
    """
    Plots a separate graph for each value of mu.  Each lattice is shown on it's own graph.
    :return: None
    """
    #   Missing JCN data is commented out...
    # create_figure(
    #     experiment_root_directory=R'/media/dean/jcn_data/Dean/data_from_physics_cluster/12x24/experiment_spacelike_timelike_fields',
    #     label='$12^3\cdot24$',
    #     mu_array=[0, 300, 325, 350, 375, 400, 425, 450, 475, 500, 550, 600, 650, 700, 750, 800, 850, 900, 1000, 1100],
    #     cool_array=[0, 5, 10, 15, 20],
    #     hold=False,
    #     slab_size = slab_size)
    #
    # plt.show()

    #   Missing JCN data is commented out...
    create_figure(
        experiment_root_directory=R'/media/dean/jcn_data/Dean/data_from_physics_cluster/12x16/experiment_spacelike_timelike_fields',
        label='$12^3\cdot16$',
        mu_array=[0, 300, 400, 450, 500, 550, 600, 650, 700, 800, 900],
        cool_array=[0, 5, 10, 15, 20],
        dashed_line=True,
        hold=False,
        slab_size = slab_size)

    plt.show()
    return


def create_jacobi_comparison_graph(slab_size):
    # create_jacobi_ratio_figure(
    #     experiment_root_directory=R'/media/dean/jcn_data/Dean/data_from_physics_cluster/12x24/experiment_spacelike_timelike_fields',
    #     label='$12^3\cdot24$',
    #     mu_array=[0, 300, 325, 350, 375, 400, 425, 450, 475, 500, 550, 600, 650, 700, 750, 800, 850, 900, 1000, 1100],
    #     cool_array=[0, 5, 10, 15, 20],
    #     slab_size = slab_size,
    #     dashed_line=False)

    create_jacobi_ratio_figure(
        experiment_root_directory=R'/media/dean/jcn_data/Dean/data_from_physics_cluster/12x16/experiment_spacelike_timelike_fields',
        label='$12^3\cdot16$',
        mu_array=[0, 300, 400, 450, 500, 525, 550, 575, 600, 625, 650, 675, 700, 800, 900],
        cool_array=[0, 5, 10, 15, 20],
        dashed_line=True,
        slab_size=slab_size,
        hold=False)

    plt.show()
    return

if __name__ == "__main__":
    #create_separate_graphs(0.0625)
    #create_comparison_graph(0.0625)
    create_jacobi_comparison_graph(0.0625)

    create_separate_graphs(0.03125)
    #create_comparison_graph(0.03125)
    create_jacobi_comparison_graph(0.03125)