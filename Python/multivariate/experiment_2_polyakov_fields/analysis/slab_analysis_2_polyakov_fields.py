"""
Used to run an analysis on the JCN output created on the cluster for the 2 Polyakov fields experiment.
This data can then be graphed using one of the other scripts in the directory.
"""
import math
import os
import pickle
from collections import defaultdict

from Common.data_keys import *

import Common.jcn_parser as jcn
import Common.jcn_slab_stats_parser as jcn_stats_parser


def save_cache_file(file_name, data):
    """
    Saves computed data to a cache file so that it can be processed later
    :param file_name: the file name to be used to store the cache data
    :param data: a dictionary of (key, value) pairs relating to computed attributes of the data
    :return: nothing
    """
    output_file = open(file_name, 'wb')

    #   Write the dictionary to the file
    pickle.dump(data, output_file)

    output_file.close()

    print('Saved ensemble data in file: "{:}"'.format(file_name))
    return


def load_cache_file(file_name):
    """
    Loads a cache file for pre-computed data
    :param file_name:
    :return:
    """
    if os.path.exists(file_name):
        try:
            output_file = open(file_name, 'rb')
            data = pickle.load(output_file)
            output_file.close()
            return data
        except pickle.UnpicklingError:
            print('Loading failed from file: "{:}"'.format(file_name))
            output_file.close()
    return None


def get_list_of_configuration_root_directories(ensemble_root_directory, cool_a, cool_b, slab_size):
    """
    Retreives a list of directories holding 3D Polyakov data
    :param ensemble_root_directory:
    :param cools:
    :return:
    """
    configuration_directories = []
    for (dir_path, sub_dir_names, filenames) in os.walk(ensemble_root_directory):
        for sub_dir in sub_dir_names:
            #   Each sub_dir should be a configuration root (example: conp0005).
            configuration_base_dir = os.path.join(ensemble_root_directory, sub_dir)

            #   We also need to step through the field count and slab size folders
            target_configuration_dir = os.path.join(configuration_base_dir, 'fields2', '{:}'.format(slab_size))

            if os.path.exists(target_configuration_dir):
                #   Try to find the correct level of cooling
                target_cooling_dir = os.path.join(target_configuration_dir, 'cools_{:0>2}_{:0>2}'.format(cool_a, cool_b))

                if os.path.exists(target_cooling_dir):
                    #   Finally, we have found the root for this configuration
                    configuration_directories.append(target_cooling_dir)

    if len(configuration_directories) > 0:
        print(configuration_directories)
    else:
        print("There were no configuration directories found.  Maybe the slab size ({:}) or number of cools ({:}) "
              "and ({:}) are incorrect?".format(slab_size, cool_a, cool_b))

    return configuration_directories


def analyse_configuration(configuration_root_directory, slab_size):
    """
    Calls the analyse_configuration_slice method for each time slice in the current configuration.  Then computes
     and caches statistics for each.
    :param configuration_root_directory: root directory for an entire configuration containing multiple JCNs
    (example: G:\Dean\data_from_physics_cluster\2_tcd_fields_study\output\mu0000\conp0005\cool0000\fields2\0.5)
    :return:
    """

    #   Attempt to load pre-processed values from cache
    cache_filename = os.path.join(configuration_root_directory, 'slab_stats.cache')
    #load_cache_file(cache_filename)

    #   Return the configuration averages for use in ensemble analysis, first parsing data from the stdout file
    jcn_slab_stats_file = None
    for (dir_path, dir_names, filenames) in os.walk(configuration_root_directory):
        for filename in filenames:
            if 'slab_stats.txt' in filename:
                jcn_slab_stats_file = os.path.join(configuration_root_directory, filename)
                break
    if jcn_slab_stats_file is not None:
        jcn_slab_stats = jcn_stats_parser.get_slab_stats(jcn_slab_stats_file)

        if jcn_slab_stats is not None:
            return jcn_slab_stats

    #   Return all the parsed values for the current configuration
    return None


def compute_array_average_and_variance(data_series_array):
    """
    Computes an average and a variance for a array of lists of numeric inputs
    :param data_series_array:
    :return:
    """
    configuration_average_dict = defaultdict(list)
    configuration_variance_dict = {}

    #   Count the number of configurations in the ensemble
    configuration_count = len(data_series_array)

    #   Iterate over each configuration in the ensemble to flatten to a single list
    for data_series in data_series_array:
        #   Now, iterate over the key value pairs for each configuration
        for degree, count in data_series.items():
            #   If the storage is actually a list of items, rather than an actual number, we'll count them here...
            if type(count) is list:
                count = len(count)

            #   Add to the ensemble array (either as a new item or appending to an existing list)
            configuration_average_dict[degree].append(count)

    for degree in configuration_average_dict.keys():
        ensemble_value = sum(configuration_average_dict.get(degree)) / configuration_count
        configuration_variance = 0

        for configuration in data_series_array:
            #   Look up the value for this degree, if it doesn't exist assign it as zero
            configuration_value = configuration.get(degree, 0)

            #   If the storage is actually a list of items, rather than an actual number, we'll count them here...
            if type(configuration_value) is list:
                configuration_value = len(configuration_value)

            #   Compute the variance as the sum of the contribution from each configuration in the ensemble
            configuration_variance += ((configuration_value - ensemble_value) ** 2) / configuration_count

            #   Store the result
            configuration_variance_dict[degree] = configuration_variance

    #   Now we've flattened the input to a single (sparse) array we can compute the average and variance for each bin
    for key, value in configuration_average_dict.items():
        #   Compute the average for this bin (use the configuration count NOT the size of the bin, as some
        #   configurations will not have extended the list)
        configuration_average_dict[key] = sum(value) / configuration_count

        #   And the variance
        if configuration_count > 2:
            configuration_variance = math.sqrt(configuration_variance_dict.get(key) / (configuration_count - 1))
        else:
            configuration_variance = math.sqrt(configuration_variance_dict.get(key) / configuration_count)

        configuration_variance_dict[key] = configuration_variance

    #   Return the (sparse) arrays of averages and variances
    return configuration_average_dict, configuration_variance_dict


def compute_average_and_variance(data_series):
    """
    Computes an average and a variance for a list of numeric inputs
    :param data_series:
    :return:
    """
    #   Compute the average for each stat for the configuration
    configuration_average = sum(data_series) / len(data_series)

    configuration_variance = 0
    for data_slice in data_series:
        configuration_variance += ((data_slice - configuration_average) ** 2) / len(data_series)

    if len(data_series) > 2:
        configuration_variance = math.sqrt(configuration_variance / (len(data_series) - 1))
    else:
        configuration_variance = math.sqrt(configuration_variance / (len(data_series)))

    return configuration_average, configuration_variance


def analyse_ensemble(ensemble_root_directory, ensemble_name, cool_a, cool_b, slab_size):
    """
    Analyses an entire ensemble at a specified value of cooling
    :param ensemble_root_directory: root directory, where all the configurations can be found for a value of mu
    (example: G:\Dean\data_from_physics_cluster\2_tcd_fields_study\output\12x24\mu0000)
    :param ensemble_name: the name given to the ensemble (example: mu0250)
    :param cool_a: target number of cools to be analysed
    :param cool_b: target number of cools to be analysed
    :param slab_size: target slab size for analysis
    :return: True if the analysis completed successfully
    """
    ensemble_slab_counts = []

    ensemble_polygon_counts = []
    ensemble_polygon_normalised_counts = []

    ensemble_triangle_counts = []
    ensemble_triangle_normalised_counts = []

    ensemble_total_surface_areas = []
    ensemble_normalised_surface_areas = []

    ensemble_polygon_distributions = []
    ensemble_triangle_distributions = []
    ensemble_surface_area_distributions = []

    #ensemble_vertex_degree_distribution_array = []

    #   Use a key,value map to store the parsed data
    ensemble_data = {}

    #   Construct a filename for the output (example: mu0000_cool0000.cache)
    ensemble_cache_filename = \
        '{:}_cool{:0>4}_cool{:0>4}_slabsize_{:}.slab_stats'.format(ensemble_name, cool_a, cool_b, slab_size)

    configuration_directory_list = get_list_of_configuration_root_directories(ensemble_root_directory, cool_a, cool_b,
                                                                              slab_size=slab_size)

    for configuration_root_directory in configuration_directory_list:
        #   Now call the analysis on each individual configuration
        configuration_stats = analyse_configuration(configuration_root_directory, slab_size)

        if configuration_stats is not None:
            #   Values could come back as none if the output could not be parsed (corrupt or incorrectly configured
            #   experiments)
            if KEY_SLAB_COUNT_AVERAGE in configuration_stats:
                ensemble_slab_counts.append(configuration_stats.get(KEY_SLAB_COUNT_AVERAGE))

            if KEY_POLYGON_COUNT_AVERAGE in configuration_stats:
                ensemble_polygon_counts.append(configuration_stats.get(KEY_POLYGON_COUNT_AVERAGE))
            if KEY_POLYGON_COUNT_AVERAGE_NORMALISED in configuration_stats:
                ensemble_polygon_normalised_counts.append(configuration_stats.get(KEY_POLYGON_COUNT_AVERAGE_NORMALISED))

            if KEY_TRIANGLE_COUNT_AVERAGE in configuration_stats:
                ensemble_triangle_counts.append(configuration_stats.get(KEY_TRIANGLE_COUNT_AVERAGE))
            if KEY_TRIANGLE_COUNT_AVERAGE_NORMALISED in configuration_stats:
                ensemble_triangle_normalised_counts.append(configuration_stats.get(KEY_TRIANGLE_COUNT_AVERAGE_NORMALISED))

            if KEY_SURFACE_AREA_TOTAL_AVERAGE in configuration_stats:
                ensemble_total_surface_areas.append(configuration_stats.get(KEY_SURFACE_AREA_TOTAL_AVERAGE))
            if KEY_SURFACE_AREA_TOTAL_AVERAGE_NORMALISED in configuration_stats:
                ensemble_normalised_surface_areas.append(configuration_stats.get(KEY_SURFACE_AREA_TOTAL_AVERAGE_NORMALISED))

            if KEY_SORTED_SLAB_DICTIONARY_POLYGON_COUNT in configuration_stats:
                ensemble_polygon_distributions.append(
                    configuration_stats.get(KEY_SORTED_SLAB_DICTIONARY_POLYGON_COUNT))
            if KEY_SORTED_SLAB_DICTIONARY_TRIANGLE_COUNT in configuration_stats:
                ensemble_triangle_distributions.append(
                    configuration_stats.get(KEY_SORTED_SLAB_DICTIONARY_TRIANGLE_COUNT))
            if KEY_SORTED_SLAB_DICTIONARY_SURFACE_AREA in configuration_stats:
                ensemble_surface_area_distributions.append(
                    configuration_stats.get(KEY_SORTED_SLAB_DICTIONARY_SURFACE_AREA))

                    #   ensemble_vertex_degree_distribution_array.append(configuration_stats[KEY_VERTEX_DEGREE_COUNT_DICT])

    #   Make sure it the configurations were correctly analysed
    assert len(ensemble_polygon_counts) == len(ensemble_triangle_counts)
    assert len(ensemble_polygon_counts) == len(ensemble_total_surface_areas)
    assert len(ensemble_polygon_counts) == len(ensemble_slab_counts)
    assert len(ensemble_polygon_counts) == len(ensemble_triangle_normalised_counts)
    assert len(ensemble_polygon_counts) == len(ensemble_polygon_normalised_counts)
    assert len(ensemble_polygon_counts) == len(ensemble_normalised_surface_areas)
    assert len(ensemble_polygon_counts) == len(ensemble_polygon_distributions)
    assert len(ensemble_polygon_counts) == len(ensemble_triangle_distributions)
    assert len(ensemble_polygon_counts) == len(ensemble_surface_area_distributions)

    if len(ensemble_polygon_counts) > 0:
        #   Summary
        print('Configuration analysis complete.  Found {:} configurations.'.format(len(ensemble_polygon_counts)))
    else:
        #   No data was found for the ensemble.  Skip computing averages and saving cache.
        print('No configurations were found for the ensemble "{:}" in root directory {:}'
              .format(ensemble_name, ensemble_root_directory))
        return False

    #   First up, store the number of configurations we have analysed
    ensemble_data[KEY_NUMBER_OF_CONFIGURATIONS] = len(ensemble_polygon_counts)

    #   Compute the average and variance in the vertex count statistics and add the results to the data dictionary
    ensemble_slab_count_average, ensemble_slab_count_variance = compute_average_and_variance(
        ensemble_slab_counts)
    print('Ensemble slab count: %d (variance: %d) from %i configurations'
          % (ensemble_slab_count_average, ensemble_slab_count_variance, len(ensemble_slab_counts)))
    ensemble_data[KEY_ENSEMBLE_SLAB_COUNT_AVERAGE] = ensemble_slab_count_average
    ensemble_data[KEY_ENSEMBLE_SLAB_COUNT_VARIANCE] = ensemble_slab_count_variance

    #   Compute the average and variance in the triangle count statistics and add the results to the data dictionary
    ensemble_triangle_count_average, ensemble_triangle_count_variance = \
        compute_average_and_variance(ensemble_triangle_counts)
    print('Ensemble triangle count: %d (variance: %d) from %i configurations'
          % (ensemble_triangle_count_average, ensemble_triangle_count_variance, len(ensemble_triangle_counts)))
    ensemble_data[KEY_ENSEMBLE_TRIANGLE_COUNT_AVERAGE] = ensemble_triangle_count_average
    ensemble_data[KEY_ENSEMBLE_TRIANGLE_COUNT_VARIANCE] = ensemble_triangle_count_variance

    ensemble_triangle_count_normalised_average, ensemble_triangle_count_normalised_variance = \
        compute_average_and_variance(ensemble_triangle_normalised_counts)
    print('Normalised ensemble triangle count: %d (variance: %d) from %i configurations'
          % (ensemble_triangle_count_normalised_average, ensemble_triangle_count_normalised_variance,
             len(ensemble_triangle_normalised_counts)))
    ensemble_data[KEY_ENSEMBLE_TRIANGLE_COUNT_AVERAGE_NORMALISED] = ensemble_triangle_count_normalised_average
    ensemble_data[KEY_ENSEMBLE_TRIANGLE_COUNT_VARIANCE_NORMALISED] = ensemble_triangle_count_normalised_variance

    #   Compute the average and variance in the vertex count statistics and add the results to the data dictionary
    ensemble_polygon_count_average, ensemble_polygon_count_variance = compute_average_and_variance(ensemble_polygon_counts)
    print('Ensemble polygon count: %d (variance: %d) from %i configurations'
          % (ensemble_polygon_count_average, ensemble_polygon_count_variance, len(ensemble_polygon_counts)))
    ensemble_data[KEY_ENSEMBLE_POLYGON_COUNT_AVERAGE] = ensemble_polygon_count_average
    ensemble_data[KEY_ENSEMBLE_POLYGON_COUNT_VARIANCE] = ensemble_polygon_count_variance

    #   Compute the average and variance in the vertex count statistics and add the results to the data dictionary
    ensemble_polygon_count_normalised_average, ensemble_polygon_count_normalised_variance = \
        compute_average_and_variance(ensemble_polygon_normalised_counts)
    print('Normalised ensemble polygon count: %d (variance: %d) from %i configurations'
          % (ensemble_polygon_count_normalised_average, ensemble_polygon_count_normalised_variance,
             len(ensemble_polygon_normalised_counts)))
    ensemble_data[KEY_ENSEMBLE_POLYGON_COUNT_AVERAGE_NORMALISED] = ensemble_polygon_count_normalised_average
    ensemble_data[KEY_ENSEMBLE_POLYGON_COUNT_VARIANCE_NORMALISED] = ensemble_polygon_count_normalised_variance

    #   Compute the average and variance in the edge count statistics and add the results to the data dictionary
    ensemble_total_surface_area_average, ensemble_total_surface_area_variance \
        = compute_average_and_variance(ensemble_total_surface_areas)
    print('Ensemble average surface area: %d (variance: %d) from %i configurations'
          % (ensemble_total_surface_area_average, ensemble_total_surface_area_variance, len(ensemble_total_surface_areas)))
    ensemble_data[KEY_ENSEMBLE_SURFACE_AREA_AVERAGE] = ensemble_total_surface_area_average
    ensemble_data[KEY_ENSEMBLE_SURFACE_AREA_VARIANCE] = ensemble_total_surface_area_variance

    #   Compute the average and variance in the edge count statistics and add the results to the data dictionary
    ensemble_normalised_surface_area_average, ensemble_normalised_surface_area_variance \
        = compute_average_and_variance(ensemble_normalised_surface_areas)
    print('Normalised ensemble average surface area: %d (variance: %d) from %i configurations'
          % (
          ensemble_normalised_surface_area_average, ensemble_normalised_surface_area_variance,
          len(ensemble_normalised_surface_areas)))
    ensemble_data[KEY_ENSEMBLE_SURFACE_AREA_AVERAGE_NORMALISED] = ensemble_normalised_surface_area_average
    ensemble_data[KEY_ENSEMBLE_SURFACE_AREA_VARIANCE_NORMALISED] = ensemble_normalised_surface_area_variance

    #   Compute the average and variance of the triangle distribution and add the results to the data dictionary
    ensemble_triangle_distribution_average, ensemble_triangle_distribution_variance = \
        compute_array_average_and_variance(ensemble_triangle_distributions)
    print(ensemble_triangle_distribution_average)
    print(ensemble_triangle_distribution_variance)
    ensemble_data[KEY_ENSEMBLE_SORTED_SLAB_DICTIONARY_TRIANGLE_COUNT_AVERAGE] = ensemble_triangle_distribution_average
    ensemble_data[KEY_ENSEMBLE_SORTED_SLAB_DICTIONARY_TRIANGLE_COUNT_VARIANCE] = ensemble_triangle_distribution_variance

    #   Compute the average and variance of the polygon distribution and add the results to the data dictionary
    ensemble_polygon_distribution_average, ensemble_polygon_distribution_variance = \
        compute_array_average_and_variance(ensemble_polygon_distributions)
    print(ensemble_polygon_distribution_average)
    print(ensemble_polygon_distribution_variance)
    ensemble_data[KEY_ENSEMBLE_SORTED_SLAB_DICTIONARY_POLYGON_COUNT_AVERAGE] = ensemble_polygon_distribution_average
    ensemble_data[KEY_ENSEMBLE_SORTED_SLAB_DICTIONARY_POLYGON_COUNT_VARIANCE] = ensemble_polygon_distribution_variance

    #   Compute the average and variance of the surface area distribution and add the results to the data dictionary
    ensemble_surface_area_distribution_average, ensemble_surface_area_distribution_variance = \
        compute_array_average_and_variance(ensemble_surface_area_distributions)
    print(ensemble_surface_area_distribution_average)
    print(ensemble_surface_area_distribution_variance)
    ensemble_data[KEY_ENSEMBLE_SORTED_SLAB_DICTIONARY_SURFACE_AREA_AVERAGE] = ensemble_surface_area_distribution_average
    ensemble_data[KEY_ENSEMBLE_SORTED_SLAB_DICTIONARY_SURFACE_AREA_VARIANCE] = ensemble_surface_area_distribution_variance

    #   Save the stats in a cache file
    save_cache_file(os.path.join(ensemble_root_directory, '..', ensemble_cache_filename), data=ensemble_data)
    return True


def analyse_experiment(root_directory, first_cool, last_cool, slab_size=0.0625):
    """
    Roots the analysis on all values of mu
    :param root_directory:
    :return:
    """
    assert(first_cool < last_cool)

    for (dir_path, dir_names, filenames) in os.walk(root_directory):
        for dir_name in dir_names:
            for cool_a in range(first_cool, last_cool):
                cool_b = cool_a + 1

                result = analyse_ensemble(os.path.join(root_directory, dir_name),
                                          ensemble_name=dir_name, cool_a=cool_a, cool_b=cool_b, slab_size=slab_size)
                print('Ensemble: {:} at cools {:} and {:} completed successfully: {:}'
                      .format(dir_name, cool_a, cool_b, result))

        break


def analyse_slabs(filename):
    jcn_vertex_dict = jcn.parse_jcn_file(filename)

    if jcn_vertex_dict is not None:
        jcn_degree_dict = compute_jcn_vertex_degree_distribution(jcn_vertex_dict)


def compute_jcn_vertex_degree_distribution(jcn_vertex_dict):
    """
    Takes the data parsed from a JCN file and computes the number of vertices partitioned by degree
    :param jcn_vertex_dict: a dictionary in the following format { id: stats }
    :return: a dictionary in the format { degree, count }
    """
    jcn_degree_dict = {}

    for vertex_id, stats in jcn_vertex_dict.items():
        #   Iterate over the vertices and asscoiated stats from the file
        # print(key, value)

        vertex_degree = stats.degree

        if vertex_degree in jcn_degree_dict:
            #   Already in the dictionary, add to existing element
            jcn_degree_dict[vertex_degree] += 1
        else:
            #   Not currently in the dictionary, so create a new element
            jcn_degree_dict[vertex_degree] = 1

    #print(jcn_degree_dict)

    return jcn_degree_dict


if __name__ == "__main__":
    #analyse_jcn_vertex_degree_distribution(R'/media/dean/jcn_data/Dean/data_from_physics_cluster/12x24/polyakov_study/output/mu0300/conp0005/fields2/0.0625/cools_00_01/%5Cjoint contour net.jcn)')

    #delete_cache(R'/media/dean/jcn_data/Dean/data_from_physics_cluster/12x16/experiment_2_tcd_time_fields')
    #analyse_experiment(R'/media/dean/jcn_data/Dean/data_from_physics_cluster/12x24/polyakov_study/output', first_cool=0, last_cool=20)
    analyse_experiment(R'/media/dean/jcn_data/Dean/data_from_physics_cluster/12x24/experiment_2_polyakov_fields',
                       first_cool=0,
                       last_cool=20)


#data = jcnstats.get_slab_stats('/home/dean/Desktop/slab_stats.txt')
#save_cache_file('/home/dean/Desktop/slab_stats.cache', data)