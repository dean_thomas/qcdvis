"""
Used to run an analysis on the JCN output created on the cluster for the 2 Polyakov fields experiment.
This data can then be graphed using one of the other scripts in the directory.
"""
import math
import os
import pickle
from collections import defaultdict

from Common.data_keys import *

import Common.jcn_parser as jcn
import Common.jcn_stdlog_parser as stdlog
import Common.jcn_stdout_parser as stdout


def save_cache_file(file_name, data):
    """
    Saves computed data to a cache file so that it can be processed later
    :param file_name: the file name to be used to store the cache data
    :param data: a dictionary of (key, value) pairs relating to computed attributes of the data
    :return: nothing
    """
    output_file = open(file_name, 'wb')

    #   Write the dictionary to the file
    pickle.dump(data, output_file)

    output_file.close()
    return


def load_cache_file(file_name):
    """
    Loads a cache file for pre-computed data
    :param file_name:
    :return:
    """
    if os.path.exists(file_name):
        try:
            output_file = open(file_name, 'rb')
            data = pickle.load(output_file)
            output_file.close()
            return data
        except pickle.UnpicklingError:
            print('Loading failed from file: "{:}"'.format(file_name))
            output_file.close()
    return None


def get_list_of_configuration_root_directories(ensemble_root_directory, cool_a, cool_b, slab_size):
    """
    Retreives a list of directories holding 3D Polyakov data
    :param ensemble_root_directory:
    :param cools:
    :return:
    """
    configuration_directories = []
    for (dir_path, sub_dir_names, filenames) in os.walk(ensemble_root_directory):
        for sub_dir in sub_dir_names:
            #   Each sub_dir should be a configuration root (example: conp0005).
            configuration_base_dir = os.path.join(ensemble_root_directory, sub_dir)

            #   We also need to step through the field count and slab size folders
            target_configuration_dir = os.path.join(configuration_base_dir, 'fields2', '{:}'.format(slab_size))

            if os.path.exists(target_configuration_dir):
                #   Try to find the correct level of cooling
                target_cooling_dir = os.path.join(target_configuration_dir, 'cools_{:0>2}_{:0>2}'.format(cool_a, cool_b))

                if os.path.exists(target_cooling_dir):
                    #   Finally, we have found the root for this configuration
                    configuration_directories.append(target_cooling_dir)

    if len(configuration_directories) > 0:
        print(configuration_directories)
    else:
        print("There were no configuration directories found.  Maybe the slab size ({:}) or number of cools ({:}) "
              "and ({:}) are incorrect?".format(slab_size, cool_a, cool_b))

    return configuration_directories


def analyse_configuration(configuration_root_directory, slab_size):
    """
    Calls the analyse_configuration_slice method for each time slice in the current configuration.  Then computes
     and caches statistics for each.
    :param configuration_root_directory: root directory for an entire configuration containing multiple JCNs
    (example: G:\Dean\data_from_physics_cluster\2_tcd_fields_study\output\mu0000\conp0005\cool0000\fields2\0.5)
    :return:
    """

    #   Parsed data will be returned in a dictionary
    data = {}

    #   Attempt to load pre-processed values from cache
    cache_filename = os.path.join(configuration_root_directory, 'jcn_stats.cache')
    #load_cache_file(cache_filename)

    #   Return the configuration averages for use in ensemble analysis, first parsing data from the stdout file
    jcn_stdout_file = None
    for (dir_path, dir_names, filenames) in os.walk(configuration_root_directory):
        for filename in filenames:
            if 'stdout.txt' in filename:
                jcn_stdout_file = os.path.join(configuration_root_directory, filename)
                break
    if jcn_stdout_file is not None:
        jcn_stats = stdout.get_jcn_stats(jcn_stdout_file)

        if jcn_stats is not None:
            jcn_vertex_count, jcn_edge_count, jacobi_number = jcn_stats

            data[KEY_ENSEMBLE_VERTEX_COUNT] = jcn_vertex_count
            data[KEY_ENSEMBLE_EDGE_COUNT] = jcn_edge_count
            data[KEY_ENSEMBLE_JACOBI_NUMBER] = jacobi_number

    #   Next, parse the stdlog file (for memory usage and duration stats)
    jcn_stdlog_file = None
    for (dir_path, dir_names, filenames) in os.walk(configuration_root_directory):
        for filename in filenames:
            if 'stdlog.txt' in filename:
                jcn_stdlog_file = os.path.join(configuration_root_directory, filename)
                break
    if jcn_stdlog_file is not None:
        jcn_log_stats = stdlog.get_jcn_stats(jcn_stdlog_file)

        if jcn_log_stats is not None:
            #   Add the data parsed from the log to the return data
            for key, value in jcn_log_stats.items():
                data[key] = value

    #   Next parse the jcn stats file (for vertex degree distribution)
    jcn_file = None
    for (dir_path, dir_names, filenames) in os.walk(configuration_root_directory):
        for filename in filenames:
            if '%5Cjoint contour net.jcn)' in filename:
                jcn_file = os.path.join(configuration_root_directory, filename)
                break
    if jcn_file is not None:
        jcn_vertex_stats = jcn.get_jcn_stats(jcn_file)

        if jcn_vertex_stats is not None:
            #   Add the data parsed from the log to the return data
            #   BUT first compute the vertex degree distribution
            data[KEY_VERTEX_DEGREE_COUNT_DICT] = compute_jcn_vertex_degree_distribution(jcn_vertex_stats)

    #   Return all the parsed values for the current configuration
    return data


def compute_array_average_and_variance(data_series_array):
    """
    Computes an average and a variance for a array of lists of numeric inputs
    :param data_series_array:
    :return:
    """
    configuration_average_dict = defaultdict(list)
    configuration_variance_dict = {}

    #   Count the number of configurations in the ensemble
    configuration_count = len(data_series_array)

    #   Iterate over each configuration in the ensemble to flatten to a single list
    for data_series in data_series_array:
        #   Now, iterate over the key value pairs for each configuration
        for degree, count in data_series.items():
            #   Add to the ensemble array (either as a new item or appending to an existing list)
            configuration_average_dict[degree].append(count)

    for degree in configuration_average_dict.keys():
        ensemble_value = sum(configuration_average_dict.get(degree)) / configuration_count
        configuration_variance = 0

        for configuration in data_series_array:
            #   Look up the value for this degree, if it doesn't exist assign it as zero
            configuration_value = configuration.get(degree, 0)

            #   Compute the variance as the sum of the contribution from each configuration in the ensemble
            configuration_variance += ((configuration_value - ensemble_value) ** 2) / configuration_count

            #   Store the result
            configuration_variance_dict[degree] = configuration_variance

    #   Now we've flattened the input to a single (sparse) array we can compute the average and variance for each bin
    for key, value in configuration_average_dict.items():
        #   Compute the average for this bin (use the configuration count NOT the size of the bin, as some
        #   configurations will not have extended the list)
        configuration_average_dict[key] = sum(value) / configuration_count

        #   And the variance
        if configuration_count > 2:
            configuration_variance = math.sqrt(configuration_variance_dict.get(key) / (configuration_count - 1))
        else:
            configuration_variance = math.sqrt(configuration_variance_dict.get(key) / configuration_count)

        configuration_variance_dict[key] = configuration_variance

    #   Return the (sparse) arrays of averages and variances
    return configuration_average_dict, configuration_variance_dict


def compute_average_and_variance(data_series):
    """
    Computes an average and a variance for a list of numeric inputs
    :param data_series:
    :return:
    """
    #   Compute the average for each stat for the configuration
    configuration_average = sum(data_series) / len(data_series)

    configuration_variance = 0
    for data_slice in data_series:
        configuration_variance += ((data_slice - configuration_average) ** 2) / len(data_series)

    if len(data_series) > 2:
        configuration_variance = math.sqrt(configuration_variance / (len(data_series) - 1))
    else:
        configuration_variance = math.sqrt(configuration_variance / (len(data_series)))

    return configuration_average, configuration_variance


def analyse_ensemble(ensemble_root_directory, ensemble_name, cool_a, cool_b, slab_size):
    """
    Analyses an entire ensemble at a specified value of cooling
    :param ensemble_root_directory: root directory, where all the configurations can be found for a value of mu
    (example: G:\Dean\data_from_physics_cluster\2_tcd_fields_study\output\12x24\mu0000)
    :param ensemble_name: the name given to the ensemble (example: mu0250)
    :param cool_a: target number of cools to be analysed
    :param cool_b: target number of cools to be analysed
    :param slab_size: target slab size for analysis
    :return: True if the analysis completed successfully
    """
    ensemble_vertex_counts = []
    ensemble_edge_counts = []
    ensemble_jacobi_numbers = []

    ensemble_vertex_degree_distribution_array = []

    #   Use a key,value map to store the parsed data
    ensemble_data = {}

    #   Construct a filename for the output (example: mu0000_cool0000.cache)
    ensemble_cache_filename = \
        '{:}_cool{:0>4}_cool{:0>4}_slabsize_{:}.cache'.format(ensemble_name, cool_a, cool_b, slab_size)

    configuration_directory_list = get_list_of_configuration_root_directories(ensemble_root_directory, cool_a, cool_b,
                                                                              slab_size=slab_size)

    for configuration_root_directory in configuration_directory_list:
        #   Now call the analysis on each individual configuration
        configuration_stats = analyse_configuration(configuration_root_directory, slab_size)

        #   Values could come back as none if the output could not be parsed (corrupt or incorrectly configured
        #   experiments)
        if KEY_ENSEMBLE_VERTEX_COUNT in configuration_stats:
            ensemble_vertex_counts.append(configuration_stats[KEY_ENSEMBLE_VERTEX_COUNT])
        if KEY_ENSEMBLE_EDGE_COUNT in configuration_stats:
            ensemble_edge_counts.append(configuration_stats[KEY_ENSEMBLE_EDGE_COUNT])
        if KEY_ENSEMBLE_JACOBI_NUMBER in configuration_stats:
            ensemble_jacobi_numbers.append(configuration_stats[KEY_ENSEMBLE_JACOBI_NUMBER])

        if KEY_VERTEX_DEGREE_COUNT_DICT in configuration_stats:
            ensemble_vertex_degree_distribution_array.append(configuration_stats[KEY_VERTEX_DEGREE_COUNT_DICT])

    #   Make sure it the configurations were correctly analysed
    assert len(ensemble_vertex_counts) == len(ensemble_edge_counts)
    assert len(ensemble_vertex_counts) == len(ensemble_jacobi_numbers)

    if len(ensemble_vertex_counts) > 0:
        #   Summary
        print('Configuration analysis complete.  Found {:} configurations.'.format(len(ensemble_vertex_counts)))
    else:
        #   No data was found for the ensemble.  Skip computing averages and saving cache.
        print('No configurations were found for the ensemble "{:}" in root directory {:}'
              .format(ensemble_name, ensemble_root_directory))
        return False

    #   First up, store the number of configurations we have analysed
    ensemble_data[KEY_NUMBER_OF_CONFIGURATIONS] = len(ensemble_vertex_counts)

    #   Compute the average and variance in the vertex count statistics and add the results to the data dictionary
    ensemble_vertex_count_average, ensemble_vertex_count_variance = compute_average_and_variance(ensemble_vertex_counts)
    print('Ensemble average vertex count: %d (variance: %d) from %i configurations'
          % (ensemble_vertex_count_average, ensemble_vertex_count_variance, len(ensemble_vertex_counts)))
    ensemble_data[KEY_ENSEMBLE_VERTEX_COUNT] = ensemble_vertex_count_average
    ensemble_data[KEY_ENSEMBLE_VERTEX_VARIANCE] = ensemble_vertex_count_variance

    #   Compute the average and variance in the vertex count statistics and add the results to the data dictionary
    ensemble_edge_count_average, ensemble_edge_count_variance = compute_average_and_variance(ensemble_edge_counts)
    print('Ensemble average edge count: %d (variance: %d) from %i configurations'
          % (ensemble_edge_count_average, ensemble_edge_count_variance, len(ensemble_edge_counts)))
    ensemble_data[KEY_ENSEMBLE_EDGE_COUNT] = ensemble_edge_count_average
    ensemble_data[KEY_ENSEMBLE_EDGE_VARIANCE] = ensemble_edge_count_variance

    #   Compute the average and variance in the edge count statistics and add the results to the data dictionary
    ensemble_jacobi_number_average, ensemble_jacobi_number_variance \
        = compute_average_and_variance(ensemble_jacobi_numbers)
    print('Ensemble average jacobi number: %d (variance: %d) from %i configurations'
          % (ensemble_jacobi_number_average, ensemble_jacobi_number_variance, len(ensemble_jacobi_numbers)))
    ensemble_data[KEY_ENSEMBLE_JACOBI_NUMBER] = ensemble_jacobi_number_average
    ensemble_data[KEY_ENSEMBLE_JACOBI_VARIANCE] = ensemble_jacobi_number_variance

    #   Compute the average and variance of the vertex degree distribution and add the results to the data dictionary
    ensemble_vertex_degree_distribution_average, ensemble_vertex_degree_distribution_variance = \
        compute_array_average_and_variance(ensemble_vertex_degree_distribution_array)
    print(ensemble_vertex_degree_distribution_average)
    print(ensemble_vertex_degree_distribution_variance)
    ensemble_data[KEY_VERTEX_DEGREE_COUNT_DICT] = ensemble_vertex_degree_distribution_average
    ensemble_data[KEY_VERTEX_DEGREE_VARIANCE_DICT] = ensemble_vertex_degree_distribution_variance

    #   Save the stats in a cache file
    save_cache_file(os.path.join(ensemble_root_directory, '..', ensemble_cache_filename), data=ensemble_data)
    return True


def analyse_experiment(root_directory, first_cool, last_cool, slab_size=0.0625):
    """
    Roots the analysis on all values of mu
    :param root_directory:
    :return:
    """
    assert(first_cool < last_cool)

    for (dir_path, dir_names, filenames) in os.walk(root_directory):
        for dir_name in dir_names:
            for cool_a in range(first_cool, last_cool):
                cool_b = cool_a + 1

                result = analyse_ensemble(os.path.join(root_directory, dir_name),
                                          ensemble_name=dir_name, cool_a=cool_a, cool_b=cool_b, slab_size=slab_size)
                print('Ensemble: {:} at cools {:} and {:} completed successfully: {:}'
                      .format(dir_name, cool_a, cool_b, result))

        break


def analyse_jcn_vertex_degree_distribution(filename):
    jcn_vertex_dict = jcn.parse_jcn_file(filename)

    if jcn_vertex_dict is not None:
        jcn_degree_dict = compute_jcn_vertex_degree_distribution(jcn_vertex_dict)


def compute_jcn_vertex_degree_distribution(jcn_vertex_dict):
    """
    Takes the data parsed from a JCN file and computes the number of vertices partitioned by degree
    :param jcn_vertex_dict: a dictionary in the following format { id: stats }
    :return: a dictionary in the format { degree, count }
    """
    jcn_degree_dict = {}

    for vertex_id, stats in jcn_vertex_dict.items():
        #   Iterate over the vertices and asscoiated stats from the file
        # print(key, value)

        vertex_degree = stats.degree

        if vertex_degree in jcn_degree_dict:
            #   Already in the dictionary, add to existing element
            jcn_degree_dict[vertex_degree] += 1
        else:
            #   Not currently in the dictionary, so create a new element
            jcn_degree_dict[vertex_degree] = 1

    #print(jcn_degree_dict)

    return jcn_degree_dict


if __name__ == "__main__":
    #analyse_jcn_vertex_degree_distribution(R'/media/dean/jcn_data/Dean/data_from_physics_cluster/12x24/polyakov_study/output/mu0300/conp0005/fields2/0.0625/cools_00_01/%5Cjoint contour net.jcn)')

    #delete_cache(R'/media/dean/jcn_data/Dean/data_from_physics_cluster/12x16/experiment_2_tcd_time_fields')
    #analyse_experiment(R'/media/dean/jcn_data/Dean/data_from_physics_cluster/12x24/polyakov_study/output', first_cool=0, last_cool=20)
    analyse_experiment(R'/media/dean/jcn_data/Dean/data_from_physics_cluster/12x24/experiment_2_polyakov_fields',
                       first_cool=0,
                       last_cool=20)
