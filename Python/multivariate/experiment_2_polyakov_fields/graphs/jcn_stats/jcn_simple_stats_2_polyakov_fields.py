import os
from functools import partial

import matplotlib.pyplot as plt
from Common.data_keys import *

import Common.GraphFormatIterator as gf
from multivariate.experiment_2_polyakov_fields.analysis.jcn_analysis_2_polyakov_fields import load_cache_file

from Common.util import *

SAVE_PDF = True

#   Use LaTeX to provide type1 fonts for the graphs
plt.rcParams['ps.useafm'] = True
plt.rcParams['pdf.use14corefonts'] = True
plt.rcParams['text.usetex'] = True

#   Set defaults for saving output
plt.rcParams["savefig.directory"] = os.path.dirname(R'/home/dean/Documents/LaTeX/latex-documents/'
                                                    'jcn_ensemble_computations/figures/polyakov/')
plt.rcParams["savefig.format"] = 'pdf'

FIG_WIDTH_CM = 30
FIG_HEIGHT_CM = 15


def divide_array(array, divisor):
    result = []

    for x in array:
        result.append(x / divisor)

    return result


def __create_figure__(graph_title,
                      experiment_root_directory, size_t, data_label, data_key, error_key,
                      mu_array, first_cool, last_cool, cooling_step=1,
                      slab_size=0.0625, hold=True, dashed_line=False, scale_by=1):
    """
    Creates the graphs to display the simple Joint Contour Analysis against chemical potential
    :param experiment_root_directory: the directory that holds the data downloaded from the cluster
    :param label: text to be appended to the graph legend (can be formatted for LaTeX)
    :param mu_array: array of values of mu to display.  These will form the x-axis of the plot
    :param first_cool: cooling iteration to appear in the first graph output
    :param last_cool: cooling iteration to appear in the last graph output
    :param cooling_step: if defined every nth graph will be generated instead of every combination
    :param slab_size: target slab size for the JCN input field
    :param hold: if set to true the graph will not be displayed after creation, allowing further data to be plotted
    for comparison.
    :param dashed_line: if set to True, the line iterator will use a dashed variation.
    :param scale_by: scales the output values by the specified number
    :return: None
    """
    experiment_root_directory = os.path.join(experiment_root_directory, '12x{:}'.format(size_t),
                                             'experiment_2_polyakov_fields')

    for cool_a in range(first_cool, last_cool, cooling_step):
        cool_b = cool_a + 1

        #   Setup axis and titles
        if scale_by is not 1:
            #   Create a new graph
            fig_title = graph_title + ' ({:}-{:} cools) scaled by $n_t$'.format(cool_a, cool_b, size_t)

            plt.figure(generate_linux_friendly_filename(fig_title), figsize=(FIG_WIDTH_CM / 2.54, FIG_HEIGHT_CM / 2.54))
            plt.title(fig_title)
        else:
            #   Create a new graph
            fig_title = graph_title + ' ({:}-{:} cools)'.format(cool_a, cool_b)

            plt.figure(generate_linux_friendly_filename(fig_title), figsize=(FIG_WIDTH_CM / 2.54, FIG_HEIGHT_CM / 2.54))
            plt.title(fig_title)

        plt.xlabel('chemical potential ($\mu$)')
        plt.ylabel('ensemble average count')

        # get a new format iterator for each graph
        if dashed_line:
            format_iterator = gf.get_dashed_line_format_array(len(mu_array))
        else:
            format_iterator = gf.get_line_format_array(len(mu_array))

        # This will hold the values of mu to use to label the data
        x_axis_labels = []

        #   This will hold the values for the y-axis, along with variances for error bars for each plot
        data_array = []
        data_array_variance = []

        #   load the data for each value of mu
        for mu in mu_array:

            #   cache filename for the current ensemble
            ensemble_cache_filename = 'mu{:0>4}_cool{:0>4}_cool{:0>4}_slabsize_{:}.cache' \
                .format(mu, cool_a, cool_b, slab_size)
            path_to_ensemble_data_cache = os.path.join(experiment_root_directory, ensemble_cache_filename)

            #   load the data from the cache for the current ensemble
            if os.path.exists(path_to_ensemble_data_cache):
                data = load_cache_file(path_to_ensemble_data_cache)

                #   Bring down to normal values of mu (example: 0.25)
                x_axis_labels.append('{:}'.format(mu / 1000))

                #   Add the data to each series included pre-computed variances
                if data_key in data and error_key in data:
                    data_array.append(data.get(data_key))
                    data_array_variance.append(data.get(error_key))
                else:
                    print('Was unable to match data and error keys ({:} and {:})'.format(data_key, error_key))
            else:
                print('Unable to find a cache file at: {:}.  Perhaps you need to re-run the analysis?'
                      .format(path_to_ensemble_data_cache))

        if len(data_array) == 0:
            #   If none of the ensembles have been analysed don't plot the graph (it causes an matplotlib internal
            #   error
            print('No data to plot.  Skipping graph.')
        else:
            # Else go ahead and create the graph
            #   Plot the JCN vertex count data

            #   Hot lattice is red, cold lattice is blue
            if size_t == 16:
                fmt = 'r^-'
            else:
                fmt = 'bv--'

            plt.errorbar(x=x_axis_labels,
                         y=divide_array(data_array, scale_by),
                         yerr=divide_array(data_array_variance, scale_by),
                         fmt=fmt,
                         label='{:}'.format(data_label))

            #   Add a legend to the graph
            plt.legend(loc='best').draggable()

            if not hold:
                #   Only show if we aren't expecting extra data
                if SAVE_PDF:
                    if scale_by is 1:
                        output_dir = get_output_dir(experiment_title='polyakov',
                                                    data_type='multivariate',
                                                    data_sub_type=graph_title)
                    else:
                        output_dir = get_output_dir(experiment_title='polyakov',
                                                    data_type='multivariate',
                                                    data_sub_type=graph_title + '_scaled')

                    if not os.path.exists(output_dir):
                        os.makedirs(output_dir)

                    output_file = os.path.join(output_dir, generate_linux_friendly_filename(fig_title))

                    plt.savefig(output_file, format="pdf")
                    print('Saved file: {:}'.format(output_file))
                else:
                    plt.show()

    return


def __create_jacobi_ratio_figure__(graph_title, experiment_root_directory, size_t, data_label, mu_array,
                                   first_cool, last_cool, cooling_step=1,
                                   slab_size=0.0625, hold=True, dashed_line=False, scale_by=1):
    """
    Plots the ratio of regular JCN nodes to Jacobi nodes across the chemical potential range
    :param experiment_root_directory:
    :param label:
    :param mu_array:
    :param first_cool:
    :param last_cool:
    :param cooling_step:
    :param slab_size:
    :param hold:
    :param dashed_line:
    :return:
    """
    experiment_root_directory = os.path.join(experiment_root_directory, '12x{:}'.format(size_t),
                                             'experiment_2_polyakov_fields')

    for cool_a in range(first_cool, last_cool, cooling_step):
        cool_b = cool_a + 1

        #   Create a new graph
        if scale_by is not 1:
            #   Create a new graph
            fig_title = graph_title + ' ({:}-{:} cools) scaled by $n_t$'.format(cool_a, cool_b, size_t)

            plt.figure(generate_linux_friendly_filename(fig_title), figsize=(FIG_WIDTH_CM / 2.54, FIG_HEIGHT_CM / 2.54))
            plt.title(fig_title)
        else:
            #   Create a new graph
            fig_title = graph_title + ' ({:}-{:} cools)'.format(cool_a, cool_b)

            plt.figure(generate_linux_friendly_filename(fig_title), figsize=(FIG_WIDTH_CM / 2.54, FIG_HEIGHT_CM / 2.54))
            plt.title(fig_title)

        # Setup axis and titles
        plt.xlabel('chemical potential ($\mu$)')
        plt.ylabel('ensemble average ratio')

        # get a new format iterator for each graph
        if dashed_line:
            format_iterator = gf.get_dashed_line_format_array(1)
        else:
            format_iterator = gf.get_line_format_array(1)

        # This will hold the values of mu to use to label the data
        x_axis_labels = []

        #   This will hold the values for the y-axis, along with variances for error bars for each plot
        data_array_jacobi_ratio = []

        #   load the data for each value of mu
        for mu in mu_array:

            #   cache filename for the current ensemble
            ensemble_cache_filename = 'mu{:0>4}_cool{:0>4}_cool{:0>4}_slabsize_{:}.cache' \
                .format(mu, cool_a, cool_b, slab_size)
            path_to_ensemble_data_cache = os.path.join(experiment_root_directory, ensemble_cache_filename)

            #   load the data from the cache for the current ensemble
            if os.path.exists(path_to_ensemble_data_cache):
                data = load_cache_file(path_to_ensemble_data_cache)

                #   Bring down to normal values of mu (example: 0.25)
                x_axis_labels.append('{:}'.format(mu / 1000))

                #   Add the data to each series included pre-computed variances
                vertex_count = data.get(KEY_ENSEMBLE_VERTEX_COUNT)
                jacobi_count = data.get(KEY_ENSEMBLE_JACOBI_NUMBER)

                data_array_jacobi_ratio.append((jacobi_count / scale_by) / (vertex_count / scale_by))

            else:
                print('Unable to find a cache file at: {:}.  Perhaps you need to re-run the analysis?'
                      .format(path_to_ensemble_data_cache))

        if len(data_array_jacobi_ratio) == 0:
            #   If none of the ensembles have been analysed don't plot the graph (it causes an matplotlib internal
            #   error
            print('No data to plot.  Skipping graph.')
        else:
            # Else go ahead and create the graph
            #   Plot the JCN vertex count data
            fmt = next(format_iterator)
            plt.errorbar(x=x_axis_labels,
                         y=data_array_jacobi_ratio,
                         fmt=fmt,
                         label='ratio ({:})'.format(data_label))

            #   Add a legend to the graph
            plt.legend(loc='best')

            if not hold:
                #   Only show if we aren't expecting extra data
                if SAVE_PDF:
                    if scale_by is 1:
                        output_dir = get_output_dir(experiment_title='polyakov',
                                                    data_type='multivariate',
                                                    data_sub_type=graph_title)
                    else:
                        output_dir = get_output_dir(experiment_title='polyakov',
                                                    data_type='multivariate',
                                                    data_sub_type=graph_title + '_scaled')

                    if not os.path.exists(output_dir):
                        os.makedirs(output_dir)

                    output_file = os.path.join(output_dir, generate_linux_friendly_filename(fig_title))

                    plt.savefig(output_file, format="pdf")
                    print('Saved file: {:}'.format(output_file))
                else:
                    plt.show()

    return


def __create_jcn_vertices_edges_ratio_figure__(graph_title, experiment_root_directory, data_label,
                                               mu_array, size_t, first_cool,
                                               last_cool, cooling_step=1, slab_size=0.0625, hold=True,
                                               dashed_line=False,
                                               scale_by=1):
    experiment_root_directory = os.path.join(experiment_root_directory, '12x{:}'.format(size_t),
                                             'experiment_2_polyakov_fields')

    for cool_a in range(first_cool, last_cool, cooling_step):
        cool_b = cool_a + 1
        #   Setup axis and titles
        if scale_by is not 1:
            #   Create a new graph
            fig_title = graph_title + ' ({:}-{:} cools) scaled by $n_t$'.format(cool_a, cool_b, size_t)

            plt.figure(generate_linux_friendly_filename(fig_title), figsize=(FIG_WIDTH_CM / 2.54, FIG_HEIGHT_CM / 2.54))
            plt.title(fig_title)
        else:
            #   Create a new graph
            fig_title = graph_title + ' ({:}-{:} cools)'.format(cool_a, cool_b)

            plt.figure(generate_linux_friendly_filename(fig_title), figsize=(FIG_WIDTH_CM / 2.54, FIG_HEIGHT_CM / 2.54))
            plt.title(fig_title)
        plt.xlabel('chemical potential ($\mu$)')
        plt.ylabel('ensemble average ratio')

        # get a new format iterator for each graph
        if dashed_line:
            format_iterator = gf.get_dashed_line_format_array(1)
        else:
            format_iterator = gf.get_line_format_array(1)

        # This will hold the values of mu to use to label the data
        x_axis_labels = []

        #   This will hold the values for the y-axis, along with variances for error bars for each plot
        data_array_ratio = []

        #   load the data for each value of mu
        for mu in mu_array:

            #   cache filename for the current ensemble
            ensemble_cache_filename = 'mu{:0>4}_cool{:0>4}_cool{:0>4}_slabsize_{:}.cache' \
                .format(mu, cool_a, cool_b, slab_size)
            path_to_ensemble_data_cache = os.path.join(experiment_root_directory, ensemble_cache_filename)

            #   load the data from the cache for the current ensemble
            if os.path.exists(path_to_ensemble_data_cache):
                data = load_cache_file(path_to_ensemble_data_cache)

                #   Bring down to normal values of mu (example: 0.25)
                x_axis_labels.append('{:}'.format(mu / 1000))

                #   Add the data to each series included pre-computed variances
                vertex_count = data.get(KEY_ENSEMBLE_VERTEX_COUNT)
                edge_count = data.get(KEY_ENSEMBLE_EDGE_COUNT)

                data_array_ratio.append((vertex_count / scale_by) / (edge_count / scale_by))

            else:
                print('Unable to find a cache file at: {:}.  Perhaps you need to re-run the analysis?'
                      .format(path_to_ensemble_data_cache))

        if len(data_array_ratio) == 0:
            #   If none of the ensembles have been analysed don't plot the graph (it causes an matplotlib internal
            #   error
            print('No data to plot.  Skipping graph.')
        else:
            # Else go ahead and create the graph
            #   Plot the JCN vertex count data
            fmt = next(format_iterator)
            plt.errorbar(x=x_axis_labels,
                         y=data_array_ratio, fmt=fmt,
                         label='ratio ({:})'.format(data_label))

            #   Add a legend to the graph
            plt.legend(loc='best')

            if not hold:
                #   Only show if we aren't expecting extra data
                if SAVE_PDF:
                    if scale_by is 1:
                        output_dir = get_output_dir(experiment_title='polyakov',
                                                    data_type='multivariate',
                                                    data_sub_type=graph_title)
                    else:
                        output_dir = get_output_dir(experiment_title='polyakov',
                                                    data_type='multivariate',
                                                    data_sub_type=graph_title + '_scaled')

                    if not os.path.exists(output_dir):
                        os.makedirs(output_dir)

                    output_file = os.path.join(output_dir, generate_linux_friendly_filename(fig_title))

                    plt.savefig(output_file, format="pdf")
                    print('Saved file: {:}'.format(output_file))
                else:
                    plt.show()

    return


def create_jcn_vertices_edges_ratio_comparison_graph(first_cool, last_cool, figure_title,
                                                     mu_array_16, mu_array_24, cool_step=1, scale_by_nt=True):
    f_16 = partial(__create_jcn_vertices_edges_ratio_figure__,
                   graph_title=figure_title,
                   experiment_root_directory=R'/media/dean/jcn_data/Dean/data_from_physics_cluster',
                   data_label='$12^3\cdot16$',
                   mu_array=mu_array_16,
                   size_t=16,
                   first_cool=first_cool,
                   last_cool=last_cool,
                   cooling_step=cool_step,
                   dashed_line=True)

    f_24 = partial(__create_jcn_vertices_edges_ratio_figure__,
                   graph_title=figure_title,
                   experiment_root_directory=R'/media/dean/jcn_data/Dean/data_from_physics_cluster',
                   data_label='$12^3\cdot24$',
                   mu_array=mu_array_24,
                   size_t=24,
                   first_cool=first_cool,
                   last_cool=last_cool,
                   cooling_step=cool_step,
                   dashed_line=False,
                   hold=False)

    if not scale_by_nt:
        #   Now we'll call our functions
        f_16()
        f_24()
    else:
        #   Call our functions but scale the output by n_t
        f_16(scale_by=16)
        f_24(scale_by=24)

    #plt.hold(False)
    return


def create_jacobi_comparison_graph(first_cool, last_cool, figure_title,
                                   mu_array_16, mu_array_24, cool_step=1, scale_by_nt=True):
    f_16 = partial(__create_jacobi_ratio_figure__,
                   graph_title=figure_title,
                   experiment_root_directory=R'/media/dean/jcn_data/Dean/data_from_physics_cluster',
                   data_label='$12^3\cdot16$',
                   mu_array=mu_array_16,
                   size_t=16,
                   first_cool=first_cool,
                   last_cool=last_cool,
                   cooling_step=cool_step,
                   dashed_line=True)

    f_24 = partial(__create_jacobi_ratio_figure__,
                   graph_title=figure_title,
                   experiment_root_directory=R'/media/dean/jcn_data/Dean/data_from_physics_cluster',
                   data_label='$12^3\cdot24$',
                   mu_array=mu_array_24,
                   size_t=24,
                   first_cool=first_cool,
                   last_cool=last_cool,
                   cooling_step=cool_step,
                   dashed_line=False,
                   hold=False)

    if not scale_by_nt:
        #   Now we'll call our functions
        f_16()
        f_24()
    else:
        #   Call our functions but scale the output by n_t
        f_16(scale_by=16)
        f_24(scale_by=24)

    #plt.hold(False)
    return


def create_comparison_graph(figure_title, data_key, error_key, first_cool,
                            last_cool, mu_array_16, mu_array_24, cool_step=1, scale_by_nt=True):
    """
    Creates a graph plotting the two lattices against one another for a side-by-side comparison of the output
    :param first_cool: specifies the first cool to create graphs from
    :param last_cool: specifies the last cool to create graphs from
    :param cooling_step: the number of cooling slices to skip between graphs (defaults to 1)
    :param scale_by_nt: if set to true, the output will be scaled by the temporal dimension
    :return: None
    """

    #   Bind our function for the n_t = 16 lattice, but don't call it yet
    f_16 = partial(__create_figure__,
                   graph_title=figure_title,
                   experiment_root_directory=R'/media/dean/jcn_data/Dean/data_from_physics_cluster/',
                   size_t=16,
                   data_label='Hot lattice ($12^3\cdot16$)',
                   data_key=data_key,
                   error_key=error_key,
                   mu_array=mu_array_16,
                   first_cool=first_cool,
                   last_cool=last_cool,
                   cooling_step=cool_step,
                   dashed_line=True)

    #   Bind our function for the n_t = 24 lattice, but don't call it yet
    f_24 = partial(__create_figure__,
                   graph_title=figure_title,
                   experiment_root_directory=R'/media/dean/jcn_data/Dean/data_from_physics_cluster/',
                   size_t=24,
                   data_label='Cold lattice ($12^3\cdot24$)',
                   data_key=data_key,
                   error_key=error_key,
                   mu_array=mu_array_24,
                   first_cool=first_cool,
                   last_cool=last_cool,
                   cooling_step=cool_step,
                   dashed_line=False,
                   hold=False)

    if not scale_by_nt:
        #   Now we'll call our functions
        f_16()
        f_24()
    else:
        #   Call our functions but scale the output by n_t
        f_16(scale_by=16)
        f_24(scale_by=24)

    #plt.hold(False)
    return


def create_separate_graphs(figure_title, data_key, error_key, first_cool,
                           last_cool, mu_array_16, mu_array_24, cool_step=1):
    """
    Plots a separate graph for each value of mu.  Each lattice is shown on it's own graph.
    :return: None
    """
    #   Missing JCN data is commented out...
    __create_figure__(
        figure_title=figure_title,
        experiment_root_directory=R'/media/dean/jcn_data/Dean/data_from_physics_cluster',
        size_t=24,
        data_label='Cold lattice ($12^3\cdot24$)',
        data_key=data_key,
        error_key=error_key,
        mu_array=mu_array_16,
        first_cool=first_cool,
        last_cool=last_cool,
        cooling_step=cool_step,
        hold=False)

    # plt.show()

    #   Missing JCN data is commented out...
    __create_figure__(
        figure_title=figure_title,
        experiment_root_directory=R'/media/dean/jcn_data/Dean/data_from_physics_cluster',
        size_t=16,
        data_label='Hot lattice($12^3\cdot16$)',
        mu_array=mu_array_24,
        first_cool=first_cool,
        last_cool=last_cool,
        cooling_step=cool_step,
        dashed_line=True,
        hold=False)

    return


def generate_comparison_graphs(mu_array_16, mu_array_24, scale_by_nt):
    print('Generating basic JCN statistic graphs')
    create_comparison_graph(figure_title='JCN vertex count',
                            data_key=KEY_ENSEMBLE_VERTEX_COUNT,
                            error_key=KEY_ENSEMBLE_VERTEX_VARIANCE,
                            first_cool=14, last_cool=15,
                            mu_array_16=mu_array_16,
                            mu_array_24=mu_array_24,
                            scale_by_nt=scale_by_nt)

    # create_comparison_graph(figure_title='JCN edge count',
    #                         data_key=KEY_ENSEMBLE_EDGE_COUNT,
    #                         error_key=KEY_ENSEMBLE_EDGE_VARIANCE,
    #                         first_cool=19, last_cool=20,
    #                         mu_array_16=mu_array_16,
    #                         mu_array_24=mu_array_24,
    #                         scale_by_nt=scale_by_nt)
    #
    # create_comparison_graph(figure_title='JCN jacobi node count',
    #                         data_key=KEY_ENSEMBLE_JACOBI_NUMBER,
    #                         error_key=KEY_ENSEMBLE_JACOBI_VARIANCE,
    #                         first_cool=19, last_cool=20,
    #                         mu_array_16=mu_array_16,
    #                         mu_array_24=mu_array_24,
    #                         scale_by_nt=scale_by_nt)


MU_16_ARRAY_5 = [0, 300, 500, 700, 900]
MU_16_ARRAY_7 = [0, 300, 500, 600, 700, 800, 900]
MU_16_ARRAY_FULL = [0, 300, 400, 450, 500, 525, 550, 575, 600, 625, 650, 675, 700, 800, 900]
MU_16_DECONFINEMENT_REGION = [300, 400, 450, 500, 550, 600, 650, 700, 800]

MU_24_ARRAY_5 = [0, 300, 500, 700, 900]
MU_24_ARRAY_7 = [0, 300, 850, 900, 950, 1000, 1100]
MU_24_DECONFINEMENT_REGION = [500, 550, 600, 650, 700, 750, 800, 850, 900]
MU_24_ARRAY_FULL = [0, 250, 300, 325, 350, 375, 400, 425, 450, 500, 550, 600, 700, 750, 800, 850, 900, 950, 1000,
                    1100]

if __name__ == "__main__":
    print('Generating graphs...')

    generate_comparison_graphs(mu_array_16=MU_16_ARRAY_FULL, mu_array_24=MU_24_ARRAY_FULL, scale_by_nt=False)

    # create_jacobi_comparison_graph(first_cool=4,
    #                                last_cool=5,
    #                                figure_title='jacobi node ratio',
    #                                 mu_array_16=MU_16_ARRAY_FULL,
    #                                 mu_array_24=MU_24_ARRAY_FULL,
    #                                 scale_by_nt=False)

    # create_jcn_vertices_edges_ratio_comparison_graph(first_cool=14, last_cool=15,
    #                                                 figure_title='JCN vertex to edge ratio',
    #                                                 mu_array_16=MU_16_ARRAY_FULL,
    #                                                 mu_array_24=MU_24_ARRAY_FULL,
    #                                                 scale_by_nt=False)
