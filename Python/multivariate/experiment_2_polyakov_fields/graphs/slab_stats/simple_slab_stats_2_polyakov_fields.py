import os
from functools import partial

import matplotlib.pyplot as plt
from Common.data_keys import *
from Common.util import *

import Common.GraphFormatIterator as gf
from multivariate.experiment_2_polyakov_fields.analysis.jcn_analysis_2_polyakov_fields import load_cache_file

SAVE_PDF = True

#   Use LaTeX to provide type1 fonts for the graphs
plt.rcParams['ps.useafm'] = True
plt.rcParams['pdf.use14corefonts'] = True
plt.rcParams['text.usetex'] = True

#   Set defaults for saving output
plt.rcParams["savefig.directory"] = os.path.dirname(R'/home/dean/Desktop/')
plt.rcParams["savefig.format"] = 'pdf'

FIG_WIDTH_CM = 30
FIG_HEIGHT_CM = 15


def divide_array(array, divisor):
    result = []

    for x in array:
        result.append(x / divisor)

    return result


def create_figure(experiment_root_directory, label, mu_array, first_cool, last_cool, cooling_step=1,
                  slab_size=0.0625, hold=True, dashed_line=False, scale_by=1, normalised=False,
                  show_triangles=True, show_polygons=False, show_surface_area=False):
    """
    Creates the graphs to display the simple Joint Contour Analysis against chemical potential
    :param experiment_root_directory: the directory that holds the data downloaded from the cluster
    :param label: text to be appended to the graph legend (can be formatted for LaTeX)
    :param mu_array: array of values of mu to display.  These will form the x-axis of the plot
    :param first_cool: cooling iteration to appear in the first graph output
    :param last_cool: cooling iteration to appear in the last graph output
    :param cooling_step: if defined every nth graph will be generated instead of every combination
    :param slab_size: target slab size for the JCN input field
    :param hold: if set to true the graph will not be displayed after creation, allowing further data to be plotted
    for comparison.
    :param dashed_line: if set to True, the line iterator will use a dashed variation.
    :param scale_by: scales the output values by the specified number
    :param normalised: if set to true graphs will feature the stats normalised (averaged) by the number of slabs
    :return: None
    """
    if normalised:
        #   Stats are averaged by the number of slabs in the configurations
        graph_type = 'normalised'
    else:
        #   Stats are the totals in the configurations (ignoring number of slabs)
        graph_type = 'total'

    persistence_metric = None
    if show_triangles:
        if persistence_metric is None:
            persistence_metric = 'triangle count'
        else:
            persistence_metric += ' and triangle count'
    if show_polygons:
        if persistence_metric is None:
            persistence_metric = 'polygon count'
        else:
            persistence_metric += 'and polygon count'
    if show_surface_area:
        if persistence_metric is None:
            persistence_metric = 'surface area'
        else:
            persistence_metric += 'and surface area'

    for cool_a in range(first_cool, last_cool, cooling_step):
        cool_b = cool_a + 1


        #   Setup axis and titles
        if scale_by is not 1:
            title = '{:} slab {:} at cools {:} and {:} (scaled by $n_t$)'.format(graph_type, persistence_metric, cool_a, cool_b)

            plt.figure(generate_linux_friendly_filename(title),
                       figsize=(FIG_WIDTH_CM / 2.54, FIG_HEIGHT_CM / 2.54))
            plt.title(title)
        else:
            title = '{:} slab {:} at cools {:} and {:}'.format(graph_type, persistence_metric, cool_a, cool_b)

            plt.figure(generate_linux_friendly_filename(title), figsize=(FIG_WIDTH_CM / 2.54, FIG_HEIGHT_CM / 2.54))
            plt.title(title)

        plt.xlabel('chemical potential ($\mu$)')
        plt.ylabel('ensemble average count')

        # get a new format iterator for each graph
        if dashed_line:
            format_iterator = gf.get_dashed_line_format_array(len(mu_array))
        else:
            format_iterator = gf.get_line_format_array(len(mu_array))

        # This will hold the values of mu to use to label the data
        x_axis_labels = []

        #   This will hold the values for the y-axis, along with variances for error bars for each plot
        data_array_triangle_count = []
        data_array_triangle_variance = []

        data_array_polygon_count = []
        data_array_polygon_variance = []

        data_array_surface_area = []
        data_array_surface_area_variance = []

        data_array_slab_count = []
        data_array_slab_variance = []

        #   load the data for each value of mu
        for mu in mu_array:

            #   cache filename for the current ensemble
            ensemble_cache_filename = 'mu{:0>4}_cool{:0>4}_cool{:0>4}_slabsize_{:}.slab_stats' \
                .format(mu, cool_a, cool_b, slab_size)
            path_to_ensemble_data_cache = os.path.join(experiment_root_directory, ensemble_cache_filename)

            #   load the data from the cache for the current ensemble
            if os.path.exists(path_to_ensemble_data_cache):
                data = load_cache_file(path_to_ensemble_data_cache)

                #   Bring down to normal values of mu (example: 0.25)
                x_axis_labels.append('{:}'.format(mu / 1000))

                data_array_slab_count.append(data.get(KEY_ENSEMBLE_SLAB_COUNT_AVERAGE))
                data_array_slab_variance.append(data.get(KEY_ENSEMBLE_SLAB_COUNT_VARIANCE))

                if normalised:
                    #   Add the data to each series included pre-computed variances (using normalised outputs)
                    data_array_triangle_count.append(data.get(KEY_ENSEMBLE_TRIANGLE_COUNT_AVERAGE_NORMALISED))
                    data_array_triangle_variance.append(data.get(KEY_ENSEMBLE_TRIANGLE_COUNT_VARIANCE_NORMALISED))

                    data_array_polygon_count.append(data.get(KEY_ENSEMBLE_POLYGON_COUNT_AVERAGE_NORMALISED))
                    data_array_polygon_variance.append(data.get(KEY_ENSEMBLE_POLYGON_COUNT_VARIANCE_NORMALISED))

                    data_array_surface_area.append(data.get(KEY_ENSEMBLE_SURFACE_AREA_AVERAGE_NORMALISED))
                    data_array_surface_area_variance.append(data.get(KEY_ENSEMBLE_SURFACE_AREA_VARIANCE_NORMALISED))
                else:
                    #   Add the data to each series included pre-computed variances
                    data_array_triangle_count.append(data.get(KEY_ENSEMBLE_TRIANGLE_COUNT_AVERAGE))
                    data_array_triangle_variance.append(data.get(KEY_ENSEMBLE_TRIANGLE_COUNT_VARIANCE))

                    data_array_polygon_count.append(data.get(KEY_ENSEMBLE_POLYGON_COUNT_AVERAGE))
                    data_array_polygon_variance.append(data.get(KEY_ENSEMBLE_POLYGON_COUNT_VARIANCE))

                    data_array_surface_area.append(data.get(KEY_ENSEMBLE_SURFACE_AREA_AVERAGE))
                    data_array_surface_area_variance.append(data.get(KEY_ENSEMBLE_SURFACE_AREA_VARIANCE))
            else:
                print('Unable to find a cache file at: {:}.  Perhaps you need to re-run the analysis?'
                      .format(path_to_ensemble_data_cache))

        if len(data_array_triangle_count) == 0:
            #   If none of the ensembles have been analysed don't plot the graph (it causes an matplotlib internal
            #   error
            print('No data to plot.  Skipping graph.')
        else:
            # Else go ahead and create the graph
            if not normalised:
                #   Plot the number of slabs (don't show this if we are using normalised data as it skews the output)
                fmt = next(format_iterator)
                plt.errorbar(x=x_axis_labels,
                             y=divide_array(data_array_slab_count, scale_by),
                             yerr=divide_array(data_array_slab_variance, scale_by),
                             fmt=fmt,
                             label='total slabs ({:})'.format(label))

            if show_triangles:
                #   Plot triangle data
                fmt = next(format_iterator)
                plt.errorbar(x=x_axis_labels,
                             y=divide_array(data_array_triangle_count, scale_by),
                             yerr=divide_array(data_array_triangle_variance, scale_by),
                             fmt=fmt,
                             label='{:} slab triangle count ({:})'.format(graph_type, label))

            if show_polygons:
                #   Plot polygon data
                fmt = next(format_iterator)
                plt.errorbar(x=x_axis_labels,
                             y=divide_array(data_array_polygon_count, scale_by),
                             yerr=divide_array(data_array_polygon_variance, scale_by),
                             fmt=fmt,
                             label='{:} slab polygon count ({:})'.format(graph_type, label))

            if show_surface_area:
                #   Plot surface area data
                fmt = next(format_iterator)
                plt.errorbar(x=x_axis_labels,
                             y=divide_array(data_array_surface_area, scale_by),
                             yerr=divide_array(data_array_surface_area_variance, scale_by),
                             fmt=fmt,
                             label='{:} surface area ({:})'.format(graph_type, label))

            #   Add a legend to the graph
            plt.legend(loc='best').draggable()

            if not hold:
                if not hold:
                    #   Only show if we aren't expecting extra data
                    if SAVE_PDF:
                        output_dir = get_output_dir(experiment_title='polyakov',
                                                    data_type='multivariate',
                                                    data_sub_type=persistence_metric,
                                                    ensemble_count=len(mu_array))

                        if not os.path.exists(output_dir):
                            os.makedirs(output_dir)

                        output_file = os.path.join(output_dir, generate_linux_friendly_filename(title))

                        plt.savefig(output_file, format="pdf")
                        print('Saved file: {:}'.format(output_file))
                    else:
                        plt.show()



    return


def create_jacobi_ratio_figure(experiment_root_directory, label, mu_array, first_cool, last_cool, size_t, cooling_step=1,
                               slab_size=0.0625, hold=True, dashed_line=False):
    """
    Plots the ratio of regular JCN nodes to Jacobi nodes across the chemical potential range
    :param experiment_root_directory:
    :param label:
    :param mu_array:
    :param first_cool:
    :param last_cool:
    :param cooling_step:
    :param slab_size:
    :param hold:
    :param dashed_line:
    :return:
    """
    for cool_a in range(first_cool, last_cool, cooling_step):
        cool_b = cool_a + 1
        #   Setup axis and titles
        title = 'jacobi_node_ratio_at_cools_{:}_and_{:}'.format(cool_a, cool_b)
        plt.figure(title, figsize=(FIG_WIDTH_CM / 2.54, FIG_HEIGHT_CM / 2.54))
        plt.title('Ratio of Jacobi nodes / JCN vertices at cools {:} and {:}'.format(cool_a, cool_b))
        plt.xlabel('chemical potential ($\mu$)')
        plt.ylabel('ensemble average ratio')

        # get a new format iterator for each graph
        if dashed_line:
            format_iterator = gf.get_dashed_line_format_array(3)
        else:
            format_iterator = gf.get_line_format_array(3)

        # This will hold the values of mu to use to label the data
        x_axis_labels = []

        #   This will hold the values for the y-axis, along with variances for error bars for each plot
        data_array_jacobi_ratio = []

        #   load the data for each value of mu
        for mu in mu_array:

            #   cache filename for the current ensemble
            ensemble_cache_filename = 'mu{:0>4}_cool{:0>4}_cool{:0>4}_slabsize_{:}.cache' \
                .format(mu, cool_a, cool_b, slab_size)
            path_to_ensemble_data_cache = os.path.join(experiment_root_directory, ensemble_cache_filename)

            #   load the data from the cache for the current ensemble
            if os.path.exists(path_to_ensemble_data_cache):
                data = load_cache_file(path_to_ensemble_data_cache)

                #   Bring down to normal values of mu (example: 0.25)
                x_axis_labels.append('{:}'.format(mu / 1000))

                #   Add the data to each series included pre-computed variances
                vertex_count = data.get(KEY_ENSEMBLE_VERTEX_COUNT)
                jacobi_count = data.get(KEY_ENSEMBLE_JACOBI_NUMBER)

                data_array_jacobi_ratio.append(jacobi_count / vertex_count)

            else:
                print('Unable to find a cache file at: {:}.  Perhaps you need to re-run the analysis?'
                      .format(path_to_ensemble_data_cache))

        if len(data_array_jacobi_ratio) == 0:
            #   If none of the ensembles have been analysed don't plot the graph (it causes an matplotlib internal
            #   error
            print('No data to plot.  Skipping graph.')
        else:
            # Else go ahead and create the graph
            #   Plot the JCN vertex count data
            #fmt = next(format_iterator)
            if size_t == 16:
                fmt = 'r^-'
            else:
                fmt = 'bv--'
            plt.errorbar(x=x_axis_labels, y=data_array_jacobi_ratio, fmt=fmt,
                         label='ratio ({:})'.format(label))

            #   Add a legend to the graph
            plt.legend(loc='best')

            if not hold:
                if not hold:
                    #   Only show if we aren't expecting extra data
                    if SAVE_PDF:
                        output_dir = get_output_dir(experiment_title='polyakov',
                                                    data_type='multivariate',
                                                    data_sub_type='jacobi_ratio',
                                                    ensemble_count=len(mu_array))

                        if not os.path.exists(output_dir):
                            os.makedirs(output_dir)

                        output_file = os.path.join(output_dir, generate_linux_friendly_filename(title))

                        plt.savefig(output_file, format="pdf")
                        print('Saved file: {:}'.format(output_file))
                    else:
                        plt.show()

    return


def create_jcn_vertices_edges_ratio_figure(experiment_root_directory, label, mu_array, first_cool,
                                           last_cool, size_t, cooling_step=1, slab_size=0.0625, hold=True, dashed_line=False):
    for cool_a in range(first_cool, last_cool, cooling_step):
        cool_b = cool_a + 1
        #   Setup axis and titles
        title = 'jcn_vertex_to_edge_ratio_{:}_{:}_cools'.format(cool_a, cool_b)
        plt.figure(title,figsize=(FIG_WIDTH_CM / 2.54, FIG_HEIGHT_CM / 2.54))
        plt.title('Ratio of JCN vertices / JCN edges at cools {:} and {:}'.format(cool_a, cool_b))
        plt.xlabel('chemical potential ($\mu$)')
        plt.ylabel('ensemble average ratio')

        # get a new format iterator for each graph
        if dashed_line:
            format_iterator = gf.get_dashed_line_format_array(3)
        else:
            format_iterator = gf.get_line_format_array(3)

        # This will hold the values of mu to use to label the data
        x_axis_labels = []

        #   This will hold the values for the y-axis, along with variances for error bars for each plot
        data_array_ratio = []

        #   load the data for each value of mu
        for mu in mu_array:

            #   cache filename for the current ensemble
            ensemble_cache_filename = 'mu{:0>4}_cool{:0>4}_cool{:0>4}_slabsize_{:}.cache' \
                .format(mu, cool_a, cool_b, slab_size)
            path_to_ensemble_data_cache = os.path.join(experiment_root_directory, ensemble_cache_filename)

            #   load the data from the cache for the current ensemble
            if os.path.exists(path_to_ensemble_data_cache):
                data = load_cache_file(path_to_ensemble_data_cache)

                #   Bring down to normal values of mu (example: 0.25)
                x_axis_labels.append('{:}'.format(mu / 1000))

                #   Add the data to each series included pre-computed variances
                vertex_count = data.get(KEY_ENSEMBLE_VERTEX_COUNT)
                edge_count = data.get(KEY_ENSEMBLE_EDGE_COUNT)

                data_array_ratio.append(vertex_count / edge_count)

            else:
                print('Unable to find a cache file at: {:}.  Perhaps you need to re-run the analysis?'
                      .format(path_to_ensemble_data_cache))

        if len(data_array_ratio) == 0:
            #   If none of the ensembles have been analysed don't plot the graph (it causes an matplotlib internal
            #   error
            print('No data to plot.  Skipping graph.')
        else:
            # Else go ahead and create the graph
            #   Plot the JCN vertex count data
            if size_t == 16:
                fmt = 'r^-'
            else:
                fmt = 'bv--'
            #fmt = next(format_iterator)
            plt.errorbar(x=x_axis_labels, y=data_array_ratio, fmt=fmt,
                         label='ratio ({:})'.format(label))

            #   Add a legend to the graph
            plt.legend(loc='best')

    if not hold:
        if not hold:
            #   Only show if we aren't expecting extra data
            if SAVE_PDF:
                output_dir = get_output_dir(experiment_title='polyakov',
                                            data_type='multivariate',
                                            data_sub_type='vertex_to_edge_ratio',
                                            ensemble_count=len(mu_array))

                if not os.path.exists(output_dir):
                    os.makedirs(output_dir)

                output_file = os.path.join(output_dir, generate_linux_friendly_filename(title))

                plt.savefig(output_file, format="pdf")
                print('Saved file: {:}'.format(output_file))
            else:
                plt.show()

    return


def create_jcn_vertices_edges_ratio_compariosn_graph(first_cool, last_cool, cooling_step=1):
    create_jcn_vertices_edges_ratio_figure(
        experiment_root_directory=R'/media/dean/jcn_data/Dean/data_from_physics_cluster/12x16/experiment_2_polyakov_fields',
        label='$12^3\cdot16$',
        mu_array=[0, 300, 400, 450, 500, 525, 550, 575, 600, 625, 650, 675, 700, 800, 900],
        first_cool=first_cool,
        last_cool=last_cool,
        cooling_step=cooling_step,
        dashed_line=True,
        hold=True,
        size_t=16)

    create_jcn_vertices_edges_ratio_figure(
        experiment_root_directory=R'/media/dean/jcn_data/Dean/data_from_physics_cluster/12x24/experiment_2_polyakov_fields',
        label='$12^3\cdot24$',
        mu_array=[0, 300, 325, 350, 375, 400, 425, 450, 475, 500, 550, 600, 650, 700, 750, 800, 850, 900, 1000, 1100],
        first_cool=first_cool,
        last_cool=last_cool,
        cooling_step=cooling_step,
        dashed_line=False,
        hold=False,
        size_t=24)

    plt.show()
    return


def create_jacobi_comparison_graph(first_cool, last_cool, cooling_step=1):
    create_jacobi_ratio_figure(
        experiment_root_directory=R'/media/dean/jcn_data/Dean/data_from_physics_cluster/12x16/experiment_2_polyakov_fields',
        label='$12^3\cdot16$',
        mu_array=[0, 300, 400, 450, 500, 525, 550, 575, 600, 625, 650, 675, 700, 800, 900],
        first_cool=first_cool,
        last_cool=last_cool,
        cooling_step=cooling_step,
        size_t=16,
        dashed_line=True,
        hold=True)

    create_jacobi_ratio_figure(
        experiment_root_directory=R'/media/dean/jcn_data/Dean/data_from_physics_cluster/12x24/experiment_2_polyakov_fields',
        label='$12^3\cdot24$',
        mu_array=[0, 300, 325, 350, 375, 400, 425, 450, 475, 500, 550, 600, 650, 700, 750, 800, 850, 900, 1000, 1100],
        first_cool=first_cool,
        last_cool=last_cool,
        cooling_step=cooling_step,
        size_t=24,
        dashed_line=False,
        hold=False)

    plt.show()
    return


def create_comparison_graph(first_cool, last_cool=None, cooling_step=1, scale_by_nt=True, normalised=False,
                            show_triangles=False, show_polygons=False, show_surface_area=False):
    """
    Creates a graph plotting the two lattices against one another for a side-by-side comparison of the output
    :param first_cool: specifies the first cool to create graphs from
    :param last_cool: specifies the last cool to create graphs from
    :param cooling_step: the number of cooling slices to skip between graphs (defaults to 1)
    :param scale_by_nt: if set to true, the output will be scaled by the temporal dimension
    :param normalised: if set to true graphs will feature the stats normalised (averaged) by the number of slabs
    :return: None
    """

    MU_16_ARRAY_5 = [0, 300, 500, 700, 900]
    MU_16_ARRAY_7 = [0, 300, 500, 600, 700, 800, 900]
    MU_16_ARRAY_FULL = [0, 300, 400, 450, 500, 525, 550, 575, 600, 625, 650, 675, 700, 800, 900]
    MU_16_DECONFINEMENT_REGION = [300, 400, 450, 500, 550, 600, 650, 700, 800]

    MU_24_ARRAY_5 = [0, 300, 500, 700, 900]
    MU_24_ARRAY_7 = [0, 300, 850, 900, 950, 1000, 1100]
    MU_24_DECONFINEMENT_REGION = [500, 550, 600, 650, 700, 750, 800, 850, 900]
    MU_24_ARRAY_FULL = [0, 250, 300, 325, 350, 375, 400, 425, 450, 500, 550, 600, 700, 750, 800, 850, 900, 950, 1000,
                        1100]

    if last_cool is None:
        last_cool = first_cool + 1

    #   Bind our function for the n_t = 16 lattice, but don't call it yet
    f_16 = partial(create_figure,
                   experiment_root_directory=R'/home/dean/Documents/jcn_data/12x16/experiment_2_polyakov_fields',
                   label='$12^3\cdot16$',
                   mu_array=MU_16_ARRAY_FULL,
                   first_cool=first_cool,
                   last_cool=last_cool,
                   cooling_step=cooling_step,
                   dashed_line=True,
                   normalised=normalised,
                   show_polygons=show_polygons,
                   show_triangles=show_triangles,
                   show_surface_area=show_surface_area)

    #   Bind our function for the n_t = 24 lattice, but don't call it yet
    f_24 = partial(create_figure,
                   experiment_root_directory=R'/home/dean/Documents/jcn_data/12x24/experiment_2_polyakov_fields',
                   label='$12^3\cdot24$',
                   mu_array=MU_24_ARRAY_FULL,
                   first_cool=first_cool,
                   last_cool=last_cool,
                   cooling_step=cooling_step,
                   dashed_line=False,
                   hold=False,
                   normalised=normalised,
                   show_polygons=show_polygons,
                   show_triangles=show_triangles,
                   show_surface_area=show_surface_area)

    if not scale_by_nt:
        #   Now we'll call our functions
        f_16()
        f_24()
    else:
        #   Call our functions but scale the output by n_t
        f_16(scale_by=16)
        f_24(scale_by=24)

        #plt.show()
    return


def create_separate_graphs():
    """
    Plots a separate graph for each value of mu.  Each lattice is shown on it's own graph.
    :return: None
    """
    #   Missing JCN data is commented out...
    create_figure(
        experiment_root_directory=R'/home/dean/Documents/jcn_data/12x24/experiment_2_polyakov_fields',
        label='$12^3\cdot24$',
        mu_array=[0, 300, 325,
                  350,
                  375, 400, 425, 450, 475, 500, 550, 600, 650, 700, 750,
                  800, 850,
                  900, 1000,
                  1100
                  ],
        first_cool=0,
        last_cool=6,
        cooling_step=1,
        hold=False)

    plt.show()

    #   Missing JCN data is commented out...
    create_figure(
        experiment_root_directory=R'/home/dean/Documents/jcn_data/12x16/experiment_2_polyakov_fields',
        label='$12^3\cdot16$',
        mu_array=[0, 300, 400, 450, 500, 525, 550, 575, 600, 625, 650, 675, 700, 800, 900],
        first_cool=0,
        last_cool=6,
        cooling_step=1,
        dashed_line=True,
        hold=False)


    plt.show()
    return


if __name__ == "__main__":
    # create_separate_graphs()

    #create_comparison_graph(0, 6, scale_by_nt=False)

    # create_comparison_graph(0, 6, scale_by_nt=False, normalised=True, show_triangles=True)
    #create_comparison_graph(0, 20, scale_by_nt=False, normalised=True, show_triangles=True)
#create_comparison_graph(0, 20, scale_by_nt=False, normalised=True, show_polygons=True)
#create_comparison_graph(0, 20, scale_by_nt=False, normalised=True, show_surface_area=True)
    #create_comparison_graph(0, 20, scale_by_nt=False, normalised=True, show_triangles=True, show_polygons=True)

    #create_comparison_graph(0, 6, scale_by_nt=False, normalised=True, show_polygons=True)
    #create_comparison_graph(9, 20, 5, scale_by_nt=False, normalised=True, show_polygons=True)

    #create_comparison_graph(0, 6, scale_by_nt=False, normalised=True, show_surface_area=True)
    #create_comparison_graph(9, 20, 5, scale_by_nt=False, normalised=True, show_surface_area=True)

    # create_jacobi_comparison_graph(0, 6)
    create_jacobi_comparison_graph(4, 5)

    #create_jcn_vertices_edges_ratio_compariosn_graph(0, 6)
    #create_jcn_vertices_edges_ratio_compariosn_graph(9, 10)
