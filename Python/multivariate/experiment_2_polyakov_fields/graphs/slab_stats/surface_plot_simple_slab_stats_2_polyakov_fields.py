import os
import Common.surface_plot as sp
from Common.data_keys import *

import multivariate.experiment_2_polyakov_fields.analysis.slab_analysis_2_polyakov_fields as slab_analysis




def create_surface(root_dir, mu_array, slab_size=0.0625, data_key='KEY_ENSEMBLE_SLAB_COUNT_AVERAGE'):
    #   Create a grid of cools vs. mu samples
    data_array = sp.create_data_matrix(x_dim=len(mu_array), y_dim=20)

    x_tick_labels = []
    y_tick_labels = []

    x = 0
    for mu in mu_array:
        x_tick_labels.append('{:}'.format(mu / 1000))

        y = 0
        for cool_a in range(0, 20):
            cool_b = cool_a + 1

            if x == 0:
                y_tick_labels.append('{:} and {:}'.format(cool_a, cool_b))

            file_name = 'mu{:0>4}_cool{:0>4}_cool{:0>4}_slabsize_{:}.slab_stats'.format(mu, cool_a, cool_b, slab_size)
            input_file = os.path.join(root_dir, file_name)

            configuration_stats = slab_analysis.load_cache_file(input_file)

            sample = 0.0

            if configuration_stats is None:
                print('Unable to find file "{:}"'.format(input_file))
            else:
                print('Loading data from file "{:}"'.format(input_file))

                if data_key in configuration_stats:
                    sample = configuration_stats.get(data_key)
                    print('read sample value: {:}'.format(sample))

            data_array[x, y] = sample

            y += 1
        x += 1

    sp.create_figure(x_label='chemical potenial ($\mu$)', x_tick_labels=x_tick_labels,
                     y_label='cools', y_tick_labels=y_tick_labels,
                     z_values=data_array)


def create_dual_surfaces(figure_label,
                         root_dir_0, mu_array_0, root_dir_1, mu_array_1,
                         slab_size=0.0625, data_key='KEY_ENSEMBLE_SLAB_COUNT_AVERAGE', z_label='z'):
    #   Create a grid of cools vs. mu samples
    data_array_0 = sp.create_data_matrix(x_dim=len(mu_array_0), y_dim=20)

    x_tick_labels_0 = []
    y_tick_labels_0 = []

    x = 0
    for mu in mu_array_0:
        x_tick_labels_0.append('{:}'.format(mu / 1000))

        y = 0
        for cool_a in range(0, 20):
            cool_b = cool_a + 1

            if x == 0:
                y_tick_labels_0.append('({:}, {:})'.format(cool_a, cool_b))

            file_name = 'mu{:0>4}_cool{:0>4}_cool{:0>4}_slabsize_{:}.slab_stats'.format(mu, cool_a, cool_b, slab_size)
            input_file = os.path.join(root_dir_0, file_name)

            configuration_stats = slab_analysis.load_cache_file(input_file)

            sample = 0.0

            if configuration_stats is None:
                print('Unable to find file "{:}"'.format(input_file))
            else:
                print('Loading data from file "{:}"'.format(input_file))

                if data_key in configuration_stats:
                    sample = configuration_stats.get(data_key)
                    print('read sample value: {:}'.format(sample))

            data_array_0[x, y] = sample

            y += 1
        x += 1

    #   Create a grid of cools vs. mu samples
    data_array_1 = sp.create_data_matrix(x_dim=len(mu_array_1), y_dim=20)

    x_tick_labels_1 = []
    y_tick_labels_1 = []

    x = 0
    for mu in mu_array_1:
        x_tick_labels_1.append('{:}'.format(mu / 1000))

        y = 0
        for cool_a in range(0, 20):
            cool_b = cool_a + 1

            if x == 0:
                y_tick_labels_1.append('({:}, {:})'.format(cool_a, cool_b))

            file_name = 'mu{:0>4}_cool{:0>4}_cool{:0>4}_slabsize_{:}.slab_stats'.format(mu, cool_a, cool_b, slab_size)
            input_file = os.path.join(root_dir_1, file_name)

            configuration_stats = slab_analysis.load_cache_file(input_file)

            sample = 0.0

            if configuration_stats is None:
                print('Unable to find file "{:}"'.format(input_file))
            else:
                print('Loading data from file "{:}"'.format(input_file))

                if data_key in configuration_stats:
                    sample = configuration_stats.get(data_key)
                    print('read sample value: {:}'.format(sample))

            data_array_1[x, y] = sample

            y += 1
        x += 1

    sp.create_dual_figure(fig_label=figure_label,
                          x_label='chemical potenial ($\mu$)',
                          x_tick_labels_0=x_tick_labels_0, x_tick_labels_1=x_tick_labels_1,
                          y_label='cools',
                          y_tick_labels_0=y_tick_labels_0, y_tick_labels_1=y_tick_labels_1,
                          z_label=z_label,
                          z_values_0=data_array_0, z_values_1=data_array_1,
                          plot_label_0='$12^3 \cdot 24$ lattice', plot_label_1='$12^3 \cdot 16$ lattice')


print('Creating surface plots...')
# create_dual_surfaces(figure_label='polyakov cooling landscape (number of slabs)',
#                      root_dir_0=R'/home/dean/Documents/jcn_data/12x24/experiment_2_polyakov_fields',
#                      mu_array_0=[0, 250, 300, 350, 400, 450, 500, 550, 600, 650, 700, 750, 800, 850, 900, 1000, 1100],
#                      root_dir_1=R'/home/dean/Documents/jcn_data/12x16/experiment_2_polyakov_fields',
#                      mu_array_1=[0, 300, 400, 450, 500, 550, 600, 650, 700, 800, 900],
#                      data_key=KEY_ENSEMBLE_SLAB_COUNT_AVERAGE,
#                      z_label='slab count')

#   Normalised surface area
create_dual_surfaces(figure_label='polyakov cooling landscape (normalised slab surface area)',
                     root_dir_0=R'/home/dean/Documents/jcn_data/12x24/experiment_2_polyakov_fields',
                     mu_array_0=[0, 250, 300, 350, 400, 450, 500, 550, 600, 650, 700, 750, 800, 850, 900, 1000, 1100],
                     root_dir_1=R'/home/dean/Documents/jcn_data/12x16/experiment_2_polyakov_fields',
                     mu_array_1=[0, 300, 400, 450, 500, 550, 600, 650, 700, 800, 900],
                     data_key=KEY_ENSEMBLE_SURFACE_AREA_AVERAGE_NORMALISED,
                     z_label='surface area per slab')

#   Normalised triangle count
create_dual_surfaces(figure_label='polyakov cooling landscape (normalised triangle count)',
                     root_dir_0=R'/home/dean/Documents/jcn_data/12x24/experiment_2_polyakov_fields',
                     mu_array_0=[0, 250, 300, 350, 400, 450, 500, 550, 600, 650, 700, 750, 800, 850, 900, 1000, 1100],
                     root_dir_1=R'/home/dean/Documents/jcn_data/12x16/experiment_2_polyakov_fields',
                     mu_array_1=[0, 300, 400, 450, 500, 550, 600, 650, 700, 800, 900],
                     data_key=KEY_ENSEMBLE_TRIANGLE_COUNT_AVERAGE_NORMALISED,
                     z_label='number of triangles per slab')

#   Normalised polygon count
create_dual_surfaces(figure_label='polyakov cooling landscape (normalised polygon count)',
                     root_dir_0=R'/home/dean/Documents/jcn_data/12x24/experiment_2_polyakov_fields',
                     mu_array_0=[0, 250, 300, 350, 400, 450, 500, 550, 600, 650, 700, 750, 800, 850, 900, 1000, 1100],
                     root_dir_1=R'/home/dean/Documents/jcn_data/12x16/experiment_2_polyakov_fields',
                     mu_array_1=[0, 300, 400, 450, 500, 550, 600, 650, 700, 800, 900],
                     data_key=KEY_ENSEMBLE_POLYGON_COUNT_AVERAGE_NORMALISED,
                     z_label='number of polygons per slab')

#   Un-normalised surface area
# create_dual_surfaces(figure_label='polyakov cooling landscape (total slab surface area)',
#                      root_dir_0=R'/home/dean/Documents/jcn_data/12x24/experiment_2_polyakov_fields',
#                      mu_array_0=[0, 250, 300, 350, 400, 450, 500, 550, 600, 650, 700, 750, 800, 850, 900, 1000, 1100],
#                      root_dir_1=R'/home/dean/Documents/jcn_data/12x16/experiment_2_polyakov_fields',
#                      mu_array_1=[0, 300, 400, 450, 500, 550, 600, 650, 700, 800, 900],
#                      data_key=KEY_ENSEMBLE_SURFACE_AREA_AVERAGE,
#                      z_label='total slab surface area')
#
# #   Un-normalised triangle count
# create_dual_surfaces(figure_label='polyakov cooling landscape (total triangle count)',
#                      root_dir_0=R'/home/dean/Documents/jcn_data/12x24/experiment_2_polyakov_fields',
#                      mu_array_0=[0, 250, 300, 350, 400, 450, 500, 550, 600, 650, 700, 750, 800, 850, 900, 1000, 1100],
#                      root_dir_1=R'/home/dean/Documents/jcn_data/12x16/experiment_2_polyakov_fields',
#                      mu_array_1=[0, 300, 400, 450, 500, 550, 600, 650, 700, 800, 900],
#                      data_key=KEY_ENSEMBLE_TRIANGLE_COUNT_AVERAGE,
#                      z_label='total number of triangles')
#
# #   Un-normalised polygon count
# create_dual_surfaces(figure_label='polyakov cooling landscape (total polygon count)',
#                      root_dir_0=R'/home/dean/Documents/jcn_data/12x24/experiment_2_polyakov_fields',
#                      mu_array_0=[0, 250, 300, 350, 400, 450, 500, 550, 600, 650, 700, 750, 800, 850, 900, 1000, 1100],
#                      root_dir_1=R'/home/dean/Documents/jcn_data/12x16/experiment_2_polyakov_fields',
#                      mu_array_1=[0, 300, 400, 450, 500, 550, 600, 650, 700, 800, 900],
#                      data_key=KEY_ENSEMBLE_POLYGON_COUNT_AVERAGE,
#                      z_label='total number of polygons')