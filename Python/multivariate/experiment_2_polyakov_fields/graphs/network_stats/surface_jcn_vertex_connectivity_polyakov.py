import os
import Common.surface_plot as sp
import numpy as np
from Common.data_keys import *
from multivariate.experiment_2_polyakov_fields.analysis.jcn_analysis_2_polyakov_fields import load_cache_file


def create_dual_surfaces(figure_label,
                         root_dir_0, mu_array_0, cool_0, root_dir_1=None, mu_array_1=None,
                         slab_size=0.0625, z_label='z'):
    #   Create a grid of vertex degree vs. mu samples
    #data_array_0 = sp.create_data_matrix(x_dim=250, y_dim=len(mu_array_0))

    global_max_vertex_degree_0 = 0

    y_tick_labels_0 = []

    sample_array_0 = []

    x = 0
    for mu in mu_array_0:

        y = 0
        cool_b = cool_0 + 1

        #   load the data from the cache for the current ensemble
        #   cache filename for the current ensemble
        ensemble_cache_filename = 'mu{:0>4}_cool{:0>4}_cool{:0>4}_slabsize_{:}.cache' \
            .format(mu, cool_0, cool_b, slab_size)
        path_to_ensemble_data_cache = os.path.join(root_dir_0, ensemble_cache_filename)
        if os.path.exists(path_to_ensemble_data_cache):
            print('Loading data from "{:}"'.format(path_to_ensemble_data_cache))
            data = load_cache_file(path_to_ensemble_data_cache)

            vertex_degree_count = data.get(KEY_VERTEX_DEGREE_COUNT_DICT)
            #vertex_degree_variance = data.get(KEY_VERTEX_DEGREE_VARIANCE_DICT)

            if vertex_degree_count is not None:
                #   Find the highest degree vertex in the data and create arrays to hold the data and errors
                vertex_degree_array = [0] * (max(vertex_degree_count.keys()) + 1)
                #vertex_degree_error_array = [0] * (max(vertex_degree_count.keys()) + 1)

                if (max(vertex_degree_count.keys()) + 1) > global_max_vertex_degree_0:
                    global_max_vertex_degree_0 = max(vertex_degree_count.keys()) + 1

                #   Map the sparse data to arrays for display
                for key, value in vertex_degree_count.items():
                    vertex_degree_array[key] = value
                # Map the sparse data to arrays for display
                #for key, value in vertex_degree_variance.items():
                #    vertex_degree_error_array[key] = value

                y_tick_labels_0.append('{:}'.format(mu / 1000))

                sample_array_0.append(vertex_degree_array)
            else:
                print('vertex degree data was missing in file "{:}"'.format(path_to_ensemble_data_cache))
        else:
            print('Was unable to find data at "{:}"'.format(path_to_ensemble_data_cache))


    #   Create a matrix to store the read in samples
    data_array_0 = sp.create_data_matrix(x_dim=global_max_vertex_degree_0, y_dim=len(y_tick_labels_0))

    x_tick_labels_0 = []
    for m in range(len(sample_array_0)):
        vertex_dist = sample_array_0[m]
        for v in range(len(vertex_dist)):
            print('(mu={:}, vd={:}) : {:}'.format(m, v, vertex_dist[v]))
            data_array_0[v][m] = vertex_dist[v]
            x_tick_labels_0.append('{:}'.format(v))

    data_array_0 = np.delete(data_array_0, range(9, global_max_vertex_degree_0), axis=0)
    x_tick_labels_0 = x_tick_labels_0[:9]

    sp.create_figure(data_array_0, x_tick_labels=x_tick_labels_0, y_tick_labels=y_tick_labels_0)


create_dual_surfaces('test',
                     root_dir_0='/media/dean/jcn_data/Dean/data_from_physics_cluster/12x24/experiment_2_polyakov_fields',
                     mu_array_0=[0, 250, 300, 350, 400, 450, 500, 550, 600, 650, 700, 750, 800, 850, 900, 1000, 1100],
                     cool_0=19)

create_dual_surfaces('test',
                     root_dir_0='/media/dean/jcn_data/Dean/data_from_physics_cluster/12x16/experiment_2_polyakov_fields',
                     mu_array_0=[0, 300, 400, 450, 500, 550, 600, 650, 700, 800, 900],
                     cool_0=19)