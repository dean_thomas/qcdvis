import os

import matplotlib.pyplot as plt
from matplotlib.ticker import ScalarFormatter
from Common.data_keys import *

import Common.GraphFormatIterator as gf
from multivariate.experiment_2_polyakov_fields.analysis.jcn_analysis_2_polyakov_fields import load_cache_file

from Common.util import *

SAVE_PDF = True

#   Use LaTeX to provide type1 fonts for the graphs
plt.rcParams['ps.useafm'] = True
plt.rcParams['pdf.use14corefonts'] = True
plt.rcParams['text.usetex'] = True

#   Set defaults for saving output
plt.rcParams["savefig.directory"] = os.path.dirname(R'/home/dean/Documents/LaTeX/latex-documents/'
                                                    'jcn_ensemble_computations/figures/polyakov/')
plt.rcParams["savefig.format"] = 'pdf'

FIG_WIDTH_CM = 30
FIG_HEIGHT_CM = 15


def create_figure(graph_title, experiment_root_directory, mu_array, size_t, first_cool, last_cool, cooling_step=1,
                  slab_size=0.0625, hold=False, dashed_line=False, x_log_axis=True, figure_label=None, data_label=None,
                  x_lim=None, y_lim=None):
    experiment_root_directory = os.path.join(experiment_root_directory, '12x{:}'.format(size_t),
                                             'experiment_2_polyakov_fields')

    for cool_a in range(first_cool, last_cool, cooling_step):
        cool_b = cool_a + 1

        fig_title = graph_title + ' ({:}-{:} cools)'.format(cool_a, cool_b)
        plt.figure(generate_linux_friendly_filename(fig_title), figsize=(FIG_WIDTH_CM / 2.54, FIG_HEIGHT_CM / 2.54))
        plt.title(fig_title)

        #   Setup axis and titles
        # plt.figure('average_jcn_connectivity_stats_at_cools_{:}_and_{:}'.format(cool_a, cool_b),
        #           figsize=(FIG_WIDTH_CM / 2.54, FIG_HEIGHT_CM / 2.54))
        # if figure_label is None:
        #    plt.title('Average JCN connectivity stats at cools {:} and {:}'.format(cool_a, cool_b))
        # else:
        #    plt.title('Average JCN connectivity stats at cools {:} and {:} ({:})'.format(cool_a, cool_b, figure_label))
        plt.xlabel('jcn vertex degree')
        plt.ylabel('freq')
        if x_log_axis:
            plt.xscale('log')
            ax = plt.gca()
            ax.get_xaxis().set_major_formatter(ScalarFormatter())

        # get a new format iterator for each graph
        if dashed_line:
            format_iterator = gf.get_dashed_line_format_array(len(mu_array))
        else:
            format_iterator = gf.get_line_format_array(len(mu_array))

        # This will hold the values for the y-axis, along with variances for error bars for each plot
        for mu in mu_array:
            #   cache filename for the current ensemble
            ensemble_cache_filename = 'mu{:0>4}_cool{:0>4}_cool{:0>4}_slabsize_{:}.cache' \
                .format(mu, cool_a, cool_b, slab_size)
            path_to_ensemble_data_cache = os.path.join(experiment_root_directory, ensemble_cache_filename)

            #   get the next format style
            fmt = next(format_iterator)

            #   load the data from the cache for the current ensemble
            if os.path.exists(path_to_ensemble_data_cache):
                data = load_cache_file(path_to_ensemble_data_cache)

                vertex_degree_count = data.get(KEY_VERTEX_DEGREE_COUNT_DICT)
                vertex_degree_variance = data.get(KEY_VERTEX_DEGREE_VARIANCE_DICT)

                if vertex_degree_count is not None:
                    #   Find the highest degree vertex in the data and create arrays to hold the data and errors
                    vertex_degree_array = [0] * (max(vertex_degree_count.keys()) + 1)
                    vertex_degree_error_array = [0] * (max(vertex_degree_count.keys()) + 1)

                    #   Map the sparse data to arrays for display
                    for key, value in vertex_degree_count.items():
                        vertex_degree_array[key] = value
                    # Map the sparse data to arrays for display
                    for key, value in vertex_degree_variance.items():
                        vertex_degree_error_array[key] = value

                    # count the number of input configurations (just measure one of the other arrays in the file)
                    configurations_in_ensemble = data.get(KEY_NUMBER_OF_CONFIGURATIONS)

                    if data_label is None:
                        plt.errorbar(x=range(0, (max(vertex_degree_count.keys()) + 1)),
                                     y=vertex_degree_array,
                                     yerr=vertex_degree_error_array,
                                     fmt=fmt,
                                     label='$\mu$ = {:} [{:}]'.format(mu / 1000, configurations_in_ensemble))
                    else:
                        plt.errorbar(x=range(0, (max(vertex_degree_count.keys()) + 1)),
                                     y=vertex_degree_array,
                                     yerr=vertex_degree_error_array,
                                     fmt=fmt,
                                     label='$\mu$ = {:} ({:}) [{:}]'.format(mu / 1000, data_label,
                                                                            configurations_in_ensemble))
                else:
                    print(
                        'Unable to find a vertex connectivity data in file {:}.  Perhaps you need to re-run the analysis?'
                        .format(path_to_ensemble_data_cache))
            else:
                print('Unable to find a cache file at: {:}.  Perhaps you need to re-run the analysis?'
                      .format(path_to_ensemble_data_cache))

        if x_lim is not None:
            plt.xlim(x_lim)

        if y_lim is not None:
            plt.ylim(y_lim)

        plt.legend(loc='best', ncol=(len(mu_array) // 6) + 1).draggable()

        if not hold:
            #   Only show if we aren't expecting extra data
            if SAVE_PDF:
                output_dir = get_output_dir(experiment_title='polyakov',
                                            data_type='multivariate',
                                            data_sub_type=graph_title,
                                            ensemble_count=len(mu_array))

                if not os.path.exists(output_dir):
                    os.makedirs(output_dir)

                output_file = os.path.join(output_dir, generate_linux_friendly_filename(fig_title))

                plt.savefig(output_file, format="pdf")
                print('Saved file: {:}'.format(output_file))
            else:
                plt.show()

    return


# def create_overview_graph_full_cold_lattice(first_cool, last_cool, cooling_step=1):
#     create_figure(experiment_root_directory=
#                   R'/media/dean/jcn_data/Dean/data_from_physics_cluster/12x24/experiment_2_polyakov_fields',
#                   mu_array=[0, 300, 325, 350, 375, 400, 425, 450, 500, 550,
#                             600, 650, 700, 750, 800, 850, 900, 1000, 1100],
#                   first_cool=first_cool,
#                   last_cool=last_cool,
#                   cooling_step=cooling_step,
#                   dashed_line=False,
#                   figure_label='full $12^3\cdot24$ data set')
#
#     return
#
#
# def create_overview_graph_full_hot_lattice(first_cool, last_cool, cooling_step=1):
#     create_figure(experiment_root_directory=
#                   R'/media/dean/jcn_data/Dean/data_from_physics_cluster/12x16/experiment_2_polyakov_fields',
#                   mu_array=[0, 300, 400, 450, 500, 550, 600, 650, 700, 800, 900],
#                   first_cool=first_cool,
#                   last_cool=last_cool,
#                   cooling_step=cooling_step,
#                   dashed_line=False,
#                   figure_label='full $12^3\cdot16$ data set')
#
#     return
#
#
# def create_comparion_graph_multiple_mu(first_cool, last_cool, cooling_step=1):
#     create_figure(experiment_root_directory=
#                   R'/media/dean/jcn_data/Dean/data_from_physics_cluster/12x24/experiment_2_polyakov_fields',
#                   data_label='$12^3\cdot24$',
#                   mu_array=[0, 300, 500, 700, 900],
#                   first_cool=first_cool,
#                   last_cool=last_cool,
#                   cooling_step=cooling_step,
#                   dashed_line=False,
#                   hold=True)
#
#     create_figure(experiment_root_directory=
#                   R'/media/dean/jcn_data/Dean/data_from_physics_cluster/12x16/experiment_2_polyakov_fields',
#                   data_label='$12^3\cdot16$',
#                   mu_array=[0, 300, 500, 700, 900],
#                   first_cool=first_cool,
#                   last_cool=last_cool,
#                   cooling_step=cooling_step,
#                   dashed_line=True)

def create_single_lattice_diagram(first_cool, last_cool, size_t, mu_array, cooling_step=1):
    create_figure(graph_title='vertex connectivity in JCN',
                  experiment_root_directory=R'/media/dean/jcn_data/Dean/data_from_physics_cluster',
                  data_label='$12^3\cdot{:}$'.format(size_t),
                  mu_array=mu_array,
                  size_t=size_t,
                  first_cool=first_cool,
                  last_cool=last_cool,
                  cooling_step=cooling_step,
                  dashed_line=False,
                  hold=False,
                  x_log_axis=False,
                  x_lim=(10, 150),
                  y_lim=(0, 5))


def create_comparison_graph_low_x(first_cool, last_cool, mu_array_16, mu_array_24, cooling_step=1):
    create_figure(graph_title='vertex connectivity in JCN',
                  experiment_root_directory=R'/media/dean/jcn_data/Dean/data_from_physics_cluster',
                  data_label='$12^3\cdot24$',
                  mu_array=mu_array_16,
                  size_t=16,
                  first_cool=first_cool,
                  last_cool=last_cool,
                  cooling_step=cooling_step,
                  dashed_line=False,
                  hold=True,
                  x_log_axis=False,
                  x_lim=(0,10))
    # x_axis_limit=10)

    create_figure(graph_title='vertex connectivity in JCN',
                  experiment_root_directory=R'/media/dean/jcn_data/Dean/data_from_physics_cluster',
                  data_label='$12^3\cdot16$',
                  mu_array=mu_array_24,
                  size_t=24,
                  first_cool=first_cool,
                  last_cool=last_cool,
                  cooling_step=cooling_step,
                  dashed_line=True,
                  hold=False,
                  x_log_axis=False,
                  x_lim=(0,10))
    # x_axis_limit=10,

    return


def create_comparison_graph_high_x(first_cool, last_cool, mu_array_16, mu_array_24, cooling_step=1):
    create_figure(graph_title='vertex connectivity in JCN',
                  experiment_root_directory=R'/media/dean/jcn_data/Dean/data_from_physics_cluster',
                  data_label='$12^3\cdot16$',
                  mu_array=mu_array_16,
                  size_t=16,
                  first_cool=first_cool,
                  last_cool=last_cool,
                  cooling_step=cooling_step,
                  dashed_line=False,
                  hold=True,
                  x_log_axis=False,
                  x_lim=(10, 250),
                  y_lim=(0, 10))

    create_figure(graph_title='vertex connectivity in JCN',
                  experiment_root_directory=R'/media/dean/jcn_data/Dean/data_from_physics_cluster',
                  data_label='$12^3\cdot24$',
                  mu_array=mu_array_24,
                  size_t=24,
                  first_cool=first_cool,
                  last_cool=last_cool,
                  cooling_step=cooling_step,
                  dashed_line=True,
                  hold=False,
                  x_log_axis=False,
                  x_lim=(10, 250),
                  y_lim=(0, 10))

    return


MU_16_ARRAY_5 = [0, 300, 500, 700, 900]
MU_16_ARRAY_7 = [0, 300, 500, 600, 700, 800, 900]
MU_16_ARRAY_FULL = [0, 300, 400, 450, 500, 525, 550, 575, 600, 625, 650, 675, 700, 800, 900]
MU_16_DECONFINEMENT_REGION = [300, 400, 450, 500, 550, 600, 650, 700, 800]

MU_24_ARRAY_5 = [0, 300, 500, 700, 900]
MU_24_ARRAY_7 = [0, 300, 850, 900, 950, 1000, 1100]
MU_24_DECONFINEMENT_REGION = [500, 550, 600, 650, 700, 750, 800, 850, 900]
MU_24_ARRAY_FULL = [0, 250, 300, 325, 350, 375, 400, 425, 450, 500, 550, 600, 700, 750, 800, 850, 900, 950, 1000,
                    1100]

if __name__ == "__main__":
    # create_overview_graph_full_cold_lattice(0, 6)
    # create_overview_graph_full_cold_lattice(10, 20, 5)

    create_single_lattice_diagram(first_cool=0, last_cool=20, size_t=16,
                                  mu_array=MU_16_ARRAY_5)
    # create_overview_graph_full_hot_lattice(0, 6)
    # create_overview_graph_full_hot_lattice(10, 20, 5)

    # create_comparion_graph_multiple_mu(0, 6)
    # create_comparion_graph_multiple_mu(10, 20, 5)

    #create_comparison_graph_low_x(mu_array_16=MU_16_ARRAY_7, mu_array_24=MU_24_ARRAY_7, first_cool=0,
    #                              last_cool=20)
    #create_comparison_graph_high_x(mu_array_16=MU_16_ARRAY_FULL, mu_array_24=MU_24_ARRAY_FULL, first_cool=0, last_cool=20)
    # create_comparison_graph(10, 20, 5)
