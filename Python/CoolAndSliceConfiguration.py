import sys
import os
from subprocess import call
import matplotlib as plt

SLICE_EXE = R"E:\Dean\QCD project executables\Qt5.6_MSVC_x64\VolumeSlicer.exe"
JCN_EXE = R"E:\Dean\QCD project executables\vtkReebGraphTest.exe"

def main(args, skip_slicing_step, skip_reeb_graph_computation_step):
    """
    entry point
    """
    assert (len(args) == 1)
    root_dir = args[0]

    if not skip_slicing_step:
        """
        Root is intended to be a configuration directory
        example: /Pygrid/12x24/mu0300/config.b190k1680mu0300j02s12t24/conp0005

        first get a list of cooling slices
        """
        configuration_files = []
        for subdir, dirs, files in os.walk(root_dir):
            for file in files:
                # Expand to absolute path
                path_to_file = os.path.join(subdir, file)

                if '.hvol1' in path_to_file:
                    print(path_to_file)
                    configuration_files.append(path_to_file)

        failed_files = []
        for file in configuration_files:
            retries = 0
            while retries < 3:
                # execute the command
                return_code = call([SLICE_EXE, file, '-np'])

                # it would appear the program cannot always open the file first time
                #   try up to 3 times before deciding the file cannot be read.
                if return_code != 0:
                    if retries < 3:
                        retries += 1
                        print('Retry: %d' % (retries))
                    else:
                        failed_files.append(file)
                        break
                else:
                    break

        print('The process failed for the following files:')
        print(failed_files)

    if not skip_reeb_graph_computation_step:
        cooled_files = []
        for subdir, dirs, files in os.walk(root_dir):
            for file in files:
                # Expand to absolute path
                path_to_file = os.path.join(subdir, file)

                if '.vol1' and 't=12' in path_to_file:
                    print(path_to_file)
                    cooled_files.append(path_to_file)

        failed_files = []
        computed_reeb_graph_files = []
        for file in cooled_files:
            #   Extract the location of the input file
            absolute_path, file_name = os.path.split(file)

            retries = 0
            while retries < 3:
                field_name = 'Topological charge density 11'
                output_file_name = field_name + '.dot'

                # execute the command
                return_code = call([JCN_EXE, '-input', file, field_name, 'i256', '-periodic', '-outdir', absolute_path, '-reebRb', '-reebDot', '-nogui'])

                # it would appear the program cannot always open the file first time
                #   try up to 3 times before deciding the file cannot be read.
                if return_code != 0:
                    if retries < 3:
                        retries += 1
                        print('Retry: %d' % (retries))
                    else:
                        failed_files.append(file)
                        break
                else:
                    computed_reeb_graph_files.append(output_file_name)

                    break

        print('The process failed for the following files:')
        print(failed_files)

        print('The following reeb graphs were computed:')
        print(computed_reeb_graph_files)


main([R"E:\Dean\QCD Data\Field data\Pygrid\12x24\mu0650\config.b190k1680mu0650j00s12t24"], False, False)

if __name__ == '__main__':
    main(sys.argv[1:])
