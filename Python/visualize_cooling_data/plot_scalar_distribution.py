import matplotlib.pyplot as plt
import numpy as np
import re
import time

#   Use LaTeX to provide type1 fonts for the graphs
plt.rcParams['ps.useafm'] = True
plt.rcParams['pdf.use14corefonts'] = True
plt.rcParams['text.usetex'] = True

#   eg: 1 2 3 4 -0.5444411
SCALAR_REGEX_3D = R'(\d+)\s*(\d+)\s*(\d+)\s*([+-]?\d*\.\d*)'
SCALAR_INDEX_3D = 4


SCALAR_REGEX_4D = R'(\d+)\s*(\d+)\s*(\d+)\s*(\d+)\s*([+-]?\d*\.\d*)'
SCALAR_INDEX_4D = 5

#x_values = [-0.9, -.73, -.62, 0.0, 0.3, 0.6, 0.9]
#y_values = [4, 5, 12, 7, 33, 1, 5]


def parse_scalars(filename):
    """
    Reads a volume containing a scalar hyper-volume and returns the scalars as a list
    """
    print('Processing file: %s' %(filename))

    # if os.path.exists(filename + '.cache'):
    #     #   If we can we'll load a cached version of the data
    #     cache_file = open(filename + '.cache', 'rb')
    #     try:
    #         result = pickle.load(cache_file)
    #         cache_file.close()
    #         return result
    #     except pickle.UnpicklingError:
    #         print('Loading failed, will regenerate data')
    #         cache_file.close()

    #   Read contents of the file
    file = open(filename, 'r')
    file_contents = file.read().splitlines()
    file.close()

    result = []

    #   Loop over file contents
    for line in file_contents:
        # print("%s" %(line))

        #   Check if the line contains a scalar value
        matches_4d = re.search(SCALAR_REGEX_4D, line)
        if matches_4d is not None:
            #   Extract the scalar value (from 4D heightfield)
            scalar_value = float(matches_4d.group(SCALAR_INDEX_4D))
            result.append(scalar_value)
            # print('%f' % (scalarValue))
        else:
            matches_3d = re.search(SCALAR_REGEX_3D, line)
            if matches_3d is not None:
                #   Extract the scalar value (from 3D heightfield)
                scalar_value = float(matches_3d.group(SCALAR_INDEX_3D))
                result.append(scalar_value)

    #   Cache for next time
    #cache_file = open(filename + '.cache', 'wb')
    #pickle.dump(result, cache_file)
    #cache_file.close()
    return result


def node_count_graph(x_values, label, range=(-1.0, 1.0)):
    fig = plt.figure(label)
    ax = fig.add_subplot(1, 1, 1)
    plt.title("Plaquette field distribution")
    plt.xlabel("plaquette value ($0.5 \cdot Re(tr)$)")
    plt.ylabel("count")

    #   Plot the number of nodes, arcs
    #plt.plot(x_values, y_values, label='nodes on arc')
    plt.hist(x_values, bins=256, range=range, label=label)
    #plt.plot(x_vals, nodes_downward, label='nodes downward')

    #   Use a grid with integer resolution
    #plt.minorticks_on()
    #start, end = ax.get_ylim()
    #ax.set_yticks(range(int(start), int(end), 5), minor=True)
    plt.grid(b=True, which='major', color='k', linestyle='-', alpha=0.5)
    plt.grid(b=True, which='minor', color='k', linestyle='-', alpha=0.1)

    plt.legend(loc='best').draggable()

#x_values = parse_scalars('/media/dean/raw_data/Dean/QCD Data/Field data/Pygrid/12x16/mu0300/config.b190k1680mu0300j04s12t16/conp0150/cool0000/spacelike plaquette.hvol1')

#node_count_graph(x_values)

#x_values = parse_scalars('/media/dean/raw_data/Dean/QCD Data/Field data/Pygrid/12x16/mu0300/config.b190k1680mu0300j04s12t16/conp0150/cool0005/spacelike plaquette.hvol1')

#node_count_graph(x_values)


# for conp in [150]:
#
#     sp = parse_scalars(
#         '/media/dean/raw_data/Dean/QCD Data/Field data/Pygrid/12x16/mu0300/config.b190k1680mu0300j04s12t16/conp{:0>4}/cool0000/spacelike plaquette.hvol1'.format(
#             conp))
#     #tp = parse_scalars(
#     #    '/media/dean/raw_data/Dean/QCD Data/Field data/Pygrid/12x16/mu0300/config.b190k1680mu0300j04s12t16/conp{:0>4}/cool0015/timelike plaquette.hvol1'.format(
#     #        conp))
#     plt.close()
#
#     node_count_graph(sp, label='space-like plaquette\n(conp{:0>4})'.format(conp))
#     #node_count_graph(tp, label='time-like plaquette (conp{:0>4})'.format(tp))
#
#     #plt.ion()
#     plt.show()
#
#     #plt.pause(1)

sp = parse_scalars('/media/dean/raw_data/Dean/QCD Data/Field data/Pygrid/12x24/mu0000/config.b190k1680mu0000j00s12t24/conp0068/cool0005/Spacelike Plaquette_t=12.vol1')
node_count_graph(sp,  label='spacelike plaquette')

tp = parse_scalars('/media/dean/raw_data/Dean/QCD Data/Field data/Pygrid/12x24/mu0000/config.b190k1680mu0000j00s12t24/conp0068/cool0005/Timelike Plaquette_t=12.vol1')
node_count_graph(tp,  label='timelike plaquette')

ap = parse_scalars('/media/dean/raw_data/Dean/QCD Data/Field data/Pygrid/12x24/mu0000/config.b190k1680mu0000j00s12t24/conp0068/cool0005/Average Plaquette_t=12.vol1')
node_count_graph(ap,  label='average plaquette')

dp = parse_scalars('/media/dean/raw_data/Dean/QCD Data/Field data/Pygrid/12x24/mu0000/config.b190k1680mu0000j00s12t24/conp0068/cool0005/Difference Plaquette_t=12.vol1')
node_count_graph(dp,  label='difference plaquette')

tcd = parse_scalars('/media/dean/raw_data/Dean/QCD Data/Field data/Pygrid/12x24/mu0000/config.b190k1680mu0000j00s12t24/conp0068/cool0005/Topological charge density_t=12.vol1')
node_count_graph(tcd,  label='topological charge density', range=(-45.0, 45.0))


plt.show()