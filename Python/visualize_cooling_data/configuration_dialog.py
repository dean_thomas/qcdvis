# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'configuration_dialog.ui'
#
# Created by: PyQt5 UI code generator 5.7.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtGui, QtWidgets, Qt
from PyQt5.QtCore import *

class Ui_Dialog(QtWidgets.QDialog):
    def __init__(self, parent=None):
        super(QtWidgets.QDialog, self).__init__(parent)
        self.setupUi()

    def setupUi(self):
        self.setObjectName("Dialog")
        self.resize(400, 300)
        self.buttonBox = QtWidgets.QDialogButtonBox(self)
        self.buttonBox.setGeometry(QRect(290, 20, 81, 241))
        self.buttonBox.setOrientation(Qt.Vertical)
        self.buttonBox.setStandardButtons(QtWidgets.QDialogButtonBox.Cancel|QtWidgets.QDialogButtonBox.Ok)
        self.buttonBox.setObjectName("buttonBox")
        self.listWidget = QtWidgets.QListWidget(self)
        self.listWidget.setGeometry(QRect(5, 11, 271, 281))
        self.listWidget.setAlternatingRowColors(True)
        self.listWidget.setSelectionMode(QtWidgets.QAbstractItemView.NoSelection)
        self.listWidget.setObjectName("listWidget")
        self.retranslateUi()

        #   Respond to user pressing OK and Cancel
        self.buttonBox.accepted.connect(self.accept)
        self.buttonBox.rejected.connect(self.reject)
        QMetaObject.connectSlotsByName(self)

    def retranslateUi(self):
        _translate = QCoreApplication.translate
        self.setWindowTitle(_translate("Dialog", "Choose configurations"))

    def set_configurations(self, configuration_list, selected_configurations):
        for configuration in configuration_list:
            new_item = QtWidgets.QListWidgetItem(configuration, self.listWidget)
            new_item.setFlags(new_item.flags() | Qt.ItemIsUserCheckable)
            if configuration in selected_configurations:
                new_item.setCheckState(Qt.Checked)
            else:
                new_item.setCheckState(Qt.Unchecked)
            self.listWidget.addItem(new_item)

    def get_checked_configurations(self):
        result = []

        for index in range(self.listWidget.count()):
            item = self.listWidget.item(index)
            if item.checkState() == Qt.Checked:
                result.append(item.text())

        return result


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    dialog = QtWidgets.QDialog()
    ui = Ui_Dialog()
    #ui.setupUi(dialog)
    #

    print(dialog.exec())
    sys.exit(app.exec_())

