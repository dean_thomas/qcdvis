#   Regex and graphing
import re
import matplotlib.pyplot as plt

#   For finding files
from os import sys
from os import walk
from os import path
from PyQt5.QtWidgets import *
from math import pi
import configuration_dialog as configuration_dialog
import itertools

import numpy as np

#   Use LaTeX to provide type1 fonts for the graphs (for submission to journals etc)
plt.rcParams['ps.useafm'] = True
plt.rcParams['pdf.use14corefonts'] = True
plt.rcParams['text.usetex'] = True
plt.rcParams["savefig.format"] = 'pdf'

FIG_WIDTH_CM = 30
FIG_HEIGHT_CM = 15

conf_regex = "(config\.b\d+k\d+mu\d+j\d+s\d+t\d+)"

#   Our match string for matching files
ff_dual_file_regex = "^ffdual_conp(\d{4}).txt"

#   Our match string for parsing the ffdual data file
ff_dual_regex = "^\s*([+-]?\d*\.\d+)\s*([+-]?\d*\.\d+)\s*([+-]?\d*\.\d+)\s*([+-]?\d*\.\d+)"

#   UI parameters
HIGHLIGHTED_LINE_WIDTH = 5.0
NORMAL_LINE_WIDTH = 1.0

#   Capture group index of the CONF variable in our regex string
CONF_INDEX = 1

#   This will provide a lookup for the data series for each configuration in the ensemble referenced by configuration
#   name
ensemble_dict = {}

#   Name of the ensemble being visualised
ensemble_name = "unknown"

#   Labels for graphs
title_ffdual_min_max_figure = "Peak topological charge density"
title_ffdual_min_max_y_axis = "Peak topological charge density\n$Q_{PEAK}$"
title_ffdual_tot_figure = "Total topological charge density"
title_ffdual_tot_y_axis = "Total topological charge density\n$Q_{TOTAL}$"
title_ffdual_mod_figure = "Topological charge density modulus"
title_ffdual_mod_y_axis = "Topological charge density modulus\n$|Q|$"

#   Global reference to the figures
figure_ff_dual_min_max = None
figure_ff_dual_tot = None
figure_ff_dual_mod = None

#   Retains a list of configurations selected for plotting by the user and the whole list in an ensemble
configurations_to_plot = []
full_configuration_list = []

#   Display grid-lines off by default
grid_lines = True

#   Number of columns for legend
legend_cols = 1

#   Highlighted configuration
selected_configuration = -1

#   Colour of plot
current_colour_map = 'winter'

def colour_map():
    return itertools.cycle(('Blues', 'BuGn', 'BuPu',
                           'GnBu', 'Greens', 'Greys', 'Oranges', 'OrRd',
                           'PuBu', 'PuBuGn', 'PuRd', 'Purples', 'RdPu',
                           'Reds', 'YlGn', 'YlGnBu', 'YlOrBr', 'YlOrRd',
                           'afmhot', 'autumn', 'bone', 'cool',
                           'copper', 'gist_heat', 'gray', 'hot',
                           'pink', 'spring', 'summer', 'winter'))


colourmap_iterator = colour_map()

def char_range(c1, c2):
    """
    Generates the characters from `c1` to `c2`, inclusive.
    """
    for c in range(ord(c1), ord(c2) + 1):
        yield chr(c)


def plot_ffdual_min_max_data(figure):
    """
    Plots the given data on the specific figure
    :param figure:
    :param values:
    :param data_title:
    :return:
    """

    #   Select the figure
    plt.figure(figure.number, figsize=(FIG_WIDTH_CM / 2.54, FIG_HEIGHT_CM / 2.54))

    #   Clear the current data
    plt.clf()

    #   Relabel the axes
    plt.title(title_ffdual_min_max_figure + " - [" + ensemble_name + "]")
    plt.xlabel("cools")
    plt.ylabel(title_ffdual_min_max_y_axis)

    #   Calculate how many colours will be needed
    cmap = plt.cm.get_cmap(current_colour_map)
    # MAX_COLORS = 10
    # MAX_COLORS = len(ensemble_dict.items())
    MAX_COLORS = len(configurations_to_plot)
    line_colors = cmap(np.linspace(0, 1, MAX_COLORS))
    current_index = 0

    #   Unpack the dictionary: key is the configuration name, data is a tuple of the action total and maximum
    for configuration_id, data in sorted(ensemble_dict.items()):
        #   Only plot the data if it is required
        if configuration_id in configurations_to_plot:
            #   Unpack the four lists of values
            ffd_tot, ffd_mod, ffd_max, ffd_min = data

            if current_index == selected_configuration:
                #   Plot the min line - retreive a reference to the line...
                #   ...so that we can plot any other lines with the same colour
                line, = plt.plot(ffd_min, label=configuration_id, color='black', linewidth=HIGHLIGHTED_LINE_WIDTH)
                line, = plt.plot(ffd_min, color=line_colors[current_index], linewidth=HIGHLIGHTED_LINE_WIDTH / 2.0)
                #   ...so that we can plot the max line with the same colour
                plt.plot(ffd_max, color='black', linewidth=HIGHLIGHTED_LINE_WIDTH)
                plt.plot(ffd_max, color=line_colors[current_index], linewidth=HIGHLIGHTED_LINE_WIDTH / 2.0)
            else:
                #   Plot the min line - retreive a reference to the line...
                #   ...so that we can plot any other lines with the same colour
                line, = plt.plot(ffd_min, label=configuration_id, color=line_colors[current_index], linewidth=NORMAL_LINE_WIDTH)
                plt.plot(ffd_max, color=line_colors[current_index], linewidth=NORMAL_LINE_WIDTH)

            current_index += 1
            if current_index >= MAX_COLORS:
                current_index = 0

    if grid_lines:
        plt.minorticks_on()
        plt.grid(b=True, which='major', color='k', linestyle='-', alpha=0.5)
        plt.grid(b=True, which='minor', color='k', linestyle='-', alpha=0.1)

    plt.legend(loc='best', ncol=legend_cols).draggable()

    return


def plot_ffdual_mod_data(figure):
    """
    Plots the given data on the specific figure
    :param figure:
    :param values:
    :param data_title:
    :return:
    """

    #   Select the figure
    plt.figure(figure.number, figsize=(FIG_WIDTH_CM / 2.54, FIG_HEIGHT_CM / 2.54))

    #   Clear the current data
    plt.clf()

    #   Relabel the axes
    plt.title(title_ffdual_mod_figure + " - [" + ensemble_name + "]")
    plt.xlabel("cools")
    plt.ylabel(title_ffdual_mod_y_axis)

    #   Calculate how many colours will be needed
    cmap = plt.cm.get_cmap(current_colour_map)
    # MAX_COLORS = 10
    # MAX_COLORS = len(ensemble_dict.items())
    MAX_COLORS = len(configurations_to_plot)
    line_colors = cmap(np.linspace(0, 1, MAX_COLORS))
    current_index = 0

    #   Unpack the dictionary: key is the configuration name, data is a tuple of the action total and maximum
    for configuration_id, data in sorted(ensemble_dict.items()):
        #   Only plot the data if it is required
        if configuration_id in configurations_to_plot:
            #   Unpack the four lists of values
            ffd_tot, ffd_mod, ffd_max, ffd_min = data

            if current_index == selected_configuration:
                #   Plot the min line - retreive a reference to the line...
                #   ...so that we can plot any other lines with the same colour
                line, = plt.plot(ffd_mod, label=configuration_id, color='black', linewidth=HIGHLIGHTED_LINE_WIDTH)
                line, = plt.plot(ffd_mod, color=line_colors[current_index], linewidth=HIGHLIGHTED_LINE_WIDTH / 2.0)
            else:
                #   Plot the min line - retreive a reference to the line...
                #   ...so that we can plot any other lines with the same colour
                line, = plt.plot(ffd_mod, label=configuration_id, color=line_colors[current_index], linewidth=NORMAL_LINE_WIDTH)

            current_index += 1
            if current_index >= MAX_COLORS:
                current_index = 0

    if grid_lines:
        plt.minorticks_on()
        plt.grid(b=True, which='major', color='k', linestyle='-', alpha=0.5)
        plt.grid(b=True, which='minor', color='k', linestyle='-', alpha=0.1)

    plt.legend(loc='best', ncol=legend_cols).draggable()

    return


def plot_ffdual_tot_data(figure):
    """
    Plots the given data on the specific figure
    :param figure:
    :param values:
    :param data_title:
    :return:
    """

    #   Select the figure
    plt.figure(figure.number, figsize=(FIG_WIDTH_CM / 2.54, FIG_HEIGHT_CM / 2.54))

    #   Clear the current data
    plt.clf()

    #   Relabel the axes
    plt.title(title_ffdual_tot_figure + " - [" + ensemble_name + "]")
    plt.xlabel("cools")
    plt.ylabel(title_ffdual_tot_y_axis)

    #   Calculate how many colours will be needed
    cmap = plt.cm.get_cmap(current_colour_map)
    #MAX_COLORS = 10
    #MAX_COLORS = len(ensemble_dict.items())
    MAX_COLORS = len(configurations_to_plot)
    line_colors = cmap(np.linspace(0, 1, MAX_COLORS))
    current_index = 0

    #   Unpack the dictionary: key is the configuration name, data is a tuple of the action total and maximum
    for configuration_id, data in sorted(ensemble_dict.items()):
        #   Only plot the data if it is required
        if configuration_id in configurations_to_plot:
            #   Unpack the four lists of values
            ffd_tot, ffd_mod, ffd_max, ffd_min = data

            if current_index == selected_configuration:
                #   Plot the min line - retreive a reference to the line...
                #   ...so that we can plot any other lines with the same colour
                line, = plt.plot(ffd_tot, label=configuration_id, color='black', linewidth=HIGHLIGHTED_LINE_WIDTH)
                line, = plt.plot(ffd_tot, color=line_colors[current_index], linewidth=HIGHLIGHTED_LINE_WIDTH / 2.0)
            else:
                #   Plot the min line - retreive a reference to the line...
                #   ...so that we can plot any other lines with the same colour
                line, = plt.plot(ffd_tot, label=configuration_id, color=line_colors[current_index], linewidth=NORMAL_LINE_WIDTH)

            current_index += 1
            if current_index >= MAX_COLORS:
                current_index = 0

    if grid_lines:
        plt.minorticks_on()
        plt.grid(b=True, which='major', color='k', linestyle='-', alpha=0.5)
        plt.grid(b=True, which='minor', color='k', linestyle='-', alpha=0.1)

    plt.legend(loc='best', ncol=legend_cols).draggable()

    return


def key_press_handler(event):
    """
    Add a keyboard handler so that we can interact with the plots at run-time
    :param event:
    :return:
    """
    print("User pressed the '{:}' key".format(event.key))

    global configurations_to_plot, full_configuration_list

    if event.key.lower() == "x":
        #   Use the 'x' key to close all active figures
        for fig_num in plt.get_fignums():
            #   Loop over all the active figures and close the figure
            plt.figure(fig_num, figsize=(FIG_WIDTH_CM / 2.54, FIG_HEIGHT_CM / 2.54))
            plt.close()
        return

    if event.key.lower() == "c":
        #   Use the 'c' key to allow selection of configurations
        print("Showing configuration selection dialog")

        dialog = configuration_dialog.Ui_Dialog()

        dialog.set_configurations(full_configuration_list, configurations_to_plot)

        if dialog.exec_() == QDialog.Accepted:
            print('Selected configurations: {:}'.format(dialog.get_checked_configurations()))

            configurations_to_plot = dialog.get_checked_configurations()

    if event.key in char_range('1', '9'):
        #   Use the keys 1..9 to reconfigure the number of columns in the legends
        cols = int(event.key)

        global legend_cols
        legend_cols = cols

    if event.key.lower() == "n":
        #   Use the 'n' key to clear the graphs
        configurations_to_plot = []

    if event.key.lower() == "a":
        #   Use the 'a' key to select all
        configurations_to_plot = full_configuration_list

    if event.key.lower() == " ":
        #   Use the ' ' key to select the first 5 configurations
        configurations_to_plot = full_configuration_list[:5]

    #   if event.key.lower() == "[":
    #    global current_colour_map, colourmap_iterator
    #    current_colour_map = next(colourmap_iterator, False)
    #    print('Selected colour map: {:}'.format(current_colour_map))

    if event.key.lower() == "]":
        global current_colour_map
        current_colour_map = next(colourmap_iterator, True)
        print('Selected colour map: {:}'.format(current_colour_map))

    if event.key.lower() == "g":
        #   Use the 'g' key to enable grid lines
        global grid_lines
        grid_lines = not grid_lines

    if event.key.lower() == "up":
        global selected_configuration
        if selected_configuration > -1:
            #   select prev
            selected_configuration -= 1
        else:
            #   select last
            selected_configuration = len(configurations_to_plot) - 1

    if event.key.lower() == "down":
        global selected_configuration
        if selected_configuration < len(configurations_to_plot) - 1:
            #   select next
            selected_configuration += 1
        else:
            #   no selection
            selected_configuration = - 1

    # Re-plot selected graphs
    if figure_ff_dual_min_max is not None:
        plot_ffdual_min_max_data(figure=figure_ff_dual_min_max)

    if figure_ff_dual_mod is not None:
        plot_ffdual_mod_data(figure=figure_ff_dual_mod)

    if figure_ff_dual_tot is not None:
        plot_ffdual_tot_data(figure=figure_ff_dual_tot)

    # Make sure the graphs are redrawn
    plt.show()

    return


def parse_ff_dual_file(filename):
    """
    Parses an action file, returns the list of maxima and total values as separate arrays (19-10-2015)
    :param filename:
    :return:
    """
    scaling_factor = 1.0 / (32.0 * pi ** 2)

    print("Doing match on file:", filename)
    ffd_tot = []
    ffd_mod = []
    ffd_max = []
    ffd_min = []

    with open(filename) as action_file:
        for line in action_file.readlines():
            # print("Parsing line:", line)
            is_match = re.match(ff_dual_regex, line)

            if is_match:
                # print(is_match.group())
                temp_ffd_tot = is_match.group(1)
                temp_ffd_mod = is_match.group(2)
                temp_ffd_max = is_match.group(3)
                temp_ffd_min = is_match.group(4)

                ffd_tot.append(float(temp_ffd_tot) * scaling_factor)
                ffd_mod.append(float(temp_ffd_mod) * scaling_factor)
                ffd_max.append(float(temp_ffd_max) * scaling_factor)
                ffd_min.append(float(temp_ffd_min) * scaling_factor)

                # print("Cools = ", cools)
                # print("Action total: ", actTot)
                # print("Action max:", actMax)

    return ffd_tot, ffd_mod, ffd_max, ffd_min


def main(argv, show_min_max_plot=True, show_total_plot=True, show_mod_plot=False):
    """
    Entry point
    :param argv:
    :return:
    """
    if path.exists(argv[0]):
        input_dir = argv[0] + "/"
    else:
        print("Please provide a valid configuration directory")
        print(argv[0])
        sys.exit(255)

    # Attempt to extract the configuration name
    # isConfName = re.match(conf_regex, input_dir)
    # if (not isConfName):
    #	print("Please provide a valid configuration directory")
    #	print(argv[0])
    #	sys.exit(255)

    # confName = isConfName.group()
    global ensemble_name
    ensemble_name = argv[1]

    # 	Create figures for each of our graphs
    global figure_ff_dual_min_max, figure_ff_dual_tot, figure_ff_dual_mod

    if show_min_max_plot:
        figure_ff_dual_min_max = plt.figure(title_ffdual_min_max_figure, figsize=(FIG_WIDTH_CM / 2.54, FIG_HEIGHT_CM / 2.54))

    if show_total_plot:
        figure_ff_dual_tot = plt.figure(title_ffdual_tot_figure, figsize=(FIG_WIDTH_CM / 2.54, FIG_HEIGHT_CM / 2.54))

    if show_mod_plot:
        figure_ff_dual_mod = plt.figure(title_ffdual_mod_figure, figsize=(FIG_WIDTH_CM / 2.54, FIG_HEIGHT_CM / 2.54))

    # Initialize a list of files in the current directory
    files = []

    # 	Obtain a list of files in the target directory
    for (dirpath, dirnames, filenames) in walk(input_dir):
        files.extend(filenames)
        break

    # Process each file that represents an action series,
    # 	or ffdual series
    for file in files:
        # 	Do a regex match on the filenames
        is_ff_dual_file = re.match(ff_dual_file_regex, file)

        if is_ff_dual_file:
            # 	Extract strings from regex
            filename = is_ff_dual_file.group()
            conf = is_ff_dual_file.group(CONF_INDEX)

            in_file = input_dir + filename
            ffd_tot, ffd_mod, ffd_max, ffd_min = parse_ff_dual_file(in_file)

            #   Store the data series
            ensemble_dict["conp{:}".format(conf)] = (ffd_tot, ffd_mod, ffd_max, ffd_min)

    # To begin with mark all configurations for plotting and store the full list for reference
    global configurations_to_plot, full_configuration_list
    for configuration in ensemble_dict.keys():
        full_configuration_list.append(configuration)
        configurations_to_plot.append(configuration)

    # Sort into ascending order
    full_configuration_list.sort()
    configurations_to_plot.sort()

    #   respond to the user pressing keys on the keyboard (objects pointing to 'None' refer to plot deselected by user)
    if figure_ff_dual_min_max is not None:
        cid_0 = figure_ff_dual_min_max.canvas.mpl_connect('key_press_event', key_press_handler)

    if figure_ff_dual_tot is not None:
        cid_1 = figure_ff_dual_tot.canvas.mpl_connect('key_press_event', key_press_handler)

    if figure_ff_dual_mod is not None:
        cid_2 = figure_ff_dual_mod.canvas.mpl_connect('key_press_event', key_press_handler)

    # Plot the figures selected for viewing (they will be 'None' if not)
    if figure_ff_dual_min_max is not None:
        plot_ffdual_min_max_data(figure=figure_ff_dual_min_max)

    if figure_ff_dual_tot is not None:
        plot_ffdual_tot_data(figure=figure_ff_dual_tot)

    if figure_ff_dual_mod is not None:
        plot_ffdual_mod_data(figure=figure_ff_dual_mod)

    # Show our figures
    plt.show()

    # 	Everything is ok
    return 0


if __name__ == "__main__":
    app = QApplication(sys.argv)
    if len(sys.argv) == 3:
        main(sys.argv[1:])
    else:
        main([
                 "/media/dean/raw_data/Dean/QCD Data/Cooled Configurations/Pygrid/12x24/mu0900/config.b190k1680mu0900j02s12t24",
                 "config.b190k1680mu0900j02s12t24"])
        print("Usage: visualize_ensemble <ensemble dir> <ensemble title>")
