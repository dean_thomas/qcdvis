#   Regex and graphing
import re
import matplotlib.pyplot as plt
from PyQt5.QtWidgets import *
import configuration_dialog as configuration_dialog
import itertools

#   For finding files
from os import sys
from os import walk
from os import path
import numpy as np

#   Use LaTeX to provide type1 fonts for the graphs (for submission to journals etc)
plt.rcParams['ps.useafm'] = True
plt.rcParams['pdf.use14corefonts'] = True
plt.rcParams['text.usetex'] = True
plt.rcParams["savefig.format"] = 'pdf'

#   Our match string for matching files
actionFileRegex = "^action_conp(\d{4}).txt"

#   Our match string for parsing the action data file
actionRegex = "^\s*(\d*\.\d+|\d+)\s+(\d*\.\d+|\d+)"

FIG_WIDTH_CM = 30
FIG_HEIGHT_CM = 15

#   UI parameters
HIGHLIGHTED_LINE_WIDTH = 5.0
NORMAL_LINE_WIDTH = 1.0

#   Capture group index of the CONF variable in our regex string
CONF_INDEX = 1

#   This will provide a lookup for the data series for each configuration in the ensemble referenced by configuration
#   name
ensemble_dict = {}

#   Name of the ensemble being visualised
ensemble_name = "unknown"

#   Labels for graphs
title_action_max_figure = "Peak Wilson action"
title_action_max_y_axis = "Peak Wilson action\n$S_{PEAK}$"
title_action_tot_figure = "Total Wilson action"
title_action_tot_y_axis = "Total Wilson action\n$S_{TOTAL}$"

#   Global reference to the figures
figure_action_total_lin = None
figure_action_total_log = None
figure_action_max = None

#   Retains a list of configurations selected for plotting by the user and the whole list in an ensemble
configurations_to_plot = []
full_configuration_list = []

#   Display grid-lines off by default
grid_lines = True

#   Number of columns for legend
legend_cols = 1

#   Colour of plot
current_colour_map = 'winter'

#   Highlighted configuration
selected_configuration = -1

def colour_map():
    return itertools.cycle(('Blues', 'BuGn', 'BuPu',
                           'GnBu', 'Greens', 'Greys', 'Oranges', 'OrRd',
                           'PuBu', 'PuBuGn', 'PuRd', 'Purples', 'RdPu',
                           'Reds', 'YlGn', 'YlGnBu', 'YlOrBr', 'YlOrRd',
                           'afmhot', 'autumn', 'bone', 'cool',
                           'copper', 'gist_heat', 'gray', 'hot',
                           'pink', 'spring', 'summer', 'winter'))


colourmap_iterator = colour_map()


def char_range(c1, c2):
    """
    Generates the characters from `c1` to `c2`, inclusive.
    """
    for c in range(ord(c1), ord(c2)+1):
        yield chr(c)


def plot_action_total_data(figure, log_scale=False):
    """
    Plots the given data on the specific figure
    :param figure:
    :param values:
    :param data_title:
    :return:
    """
    #   Select the figure
    plt.figure(figure.number, figsize=(FIG_WIDTH_CM / 2.54, FIG_HEIGHT_CM / 2.54))

    #   Clear the current data
    plt.clf()

    #   Relabel the axes
    plt.title(title_action_tot_figure + " - [" + ensemble_name + "]")
    plt.xlabel("cools")
    plt.ylabel(title_action_tot_y_axis)

    if log_scale:
        plt.yscale('log')

    #   Calculate how many colours will be needed
    cmap = plt.cm.get_cmap(current_colour_map)
    # MAX_COLORS = 10
    # MAX_COLORS = len(ensemble_dict.items())
    MAX_COLORS = len(configurations_to_plot)
    line_colors = cmap(np.linspace(0, 1, MAX_COLORS))
    current_index = 0

    #   Unpack the dictionary: key is the configuration name, data is a tuple of the action total and maximum
    for configuration_id, data in sorted(ensemble_dict.items()):
        #   Only plot the data if it is required
        if configuration_id in configurations_to_plot:
            #   Unpack the two lists of values
            act_tot, act_max = data

            #   Extract the next colour from the colourmap and increment the counter
            #rgba = cmap((1.0 / n) * color_index)
            #color_index += 1

            if current_index == selected_configuration:
                #   Plot the min line - retreive a reference to the line...
                #   ...so that we can plot any other lines with the same colour
                line, = plt.plot(act_tot, label=configuration_id, color='black', linewidth=HIGHLIGHTED_LINE_WIDTH)
                line, = plt.plot(act_tot, color=line_colors[current_index], linewidth=HIGHLIGHTED_LINE_WIDTH / 2.0)
            else:
                #   Plot the min line - retreive a reference to the line...
                #   ...so that we can plot any other lines with the same colour
                line, = plt.plot(act_tot, label=configuration_id, color=line_colors[current_index], linewidth=NORMAL_LINE_WIDTH)

            current_index += 1
            if current_index >= MAX_COLORS:
                current_index = 0

    if grid_lines:
        plt.minorticks_on()
        plt.grid(b=True, which='major', color='k', linestyle='-', alpha=0.5)
        plt.grid(b=True, which='minor', color='k', linestyle='-', alpha=0.1)

    plt.legend(loc='best',ncol=legend_cols).draggable()

    return


def plot_action_max_data(figure):
    """
    Plots the given data on the specific figure
    :param figure:
    :param values:
    :param data_title:
    :return:
    """
    #   Select the figure
    plt.figure(figure.number, figsize=(FIG_WIDTH_CM / 2.54, FIG_HEIGHT_CM / 2.54))

    #   Clear the current data
    plt.clf()

    #   Relabel the axes
    plt.title(title_action_max_figure + " - [" + ensemble_name + "]")
    plt.xlabel("cools")
    plt.ylabel(title_action_max_y_axis)

    #   Calculate how many colours will be needed
    cmap = plt.cm.get_cmap(current_colour_map)
    # MAX_COLORS = 10
    # MAX_COLORS = len(ensemble_dict.items())
    MAX_COLORS = len(configurations_to_plot)
    line_colors = cmap(np.linspace(0, 1, MAX_COLORS))
    current_index = 0

    #   Unpack the dictionary: key is the configuration name, data is a tuple of the action total and maximum
    for configuration_id, data in sorted(ensemble_dict.items()):
        #   Only plot the data if it is required
        if configuration_id in configurations_to_plot:
            #   Unpack the two lists of values
            act_tot, act_max = data

            if current_index == selected_configuration:
                #   Plot the min line - retreive a reference to the line...
                #   ...so that we can plot any other lines with the same colour
                line, = plt.plot(act_max, label=configuration_id, color='black', linewidth=HIGHLIGHTED_LINE_WIDTH)
                line, = plt.plot(act_max, color=line_colors[current_index], linewidth=HIGHLIGHTED_LINE_WIDTH / 2.0)
            else:
                #   Plot the min line - retreive a reference to the line...
                #   ...so that we can plot any other lines with the same colour
                line, = plt.plot(act_max, label=configuration_id, color=line_colors[current_index], linewidth=NORMAL_LINE_WIDTH)

            current_index += 1
            if current_index >= MAX_COLORS:
                current_index = 0

    if grid_lines:
        plt.minorticks_on()
        plt.grid(b=True, which='major', color='k', linestyle='-', alpha=0.5)
        plt.grid(b=True, which='minor', color='k', linestyle='-', alpha=0.1)

    plt.legend(loc='best',ncol=legend_cols).draggable()

    return


def parse_action_file(filename):
    """
    Parses an action file, returns the list of maxima and total values as separate arrays
    :param filename:
    :return:
    """
    print("Doing match on file:", filename)
    action_max = []
    action_tot = []

    with open(filename) as actionFile:
        for line in actionFile.readlines():
            #   print("Parsing line:", line)
            is_match = re.match(actionRegex, line)

            if is_match:
                #   print(is_match.group())
                temp_act_tot = is_match.group(1)
                temp_act_max = is_match.group(2)
                action_max.append(float(temp_act_max))
                action_tot.append(float(temp_act_tot))

                #   print("Cools = ", cools)
                #   print("Action total: ", actTot)
                #   print("Action max:", actMax)

    return action_tot, action_max


def key_press_handler(event):
    """
    Add a keyboard handler so that we can interact with the plots at run-time
    :param event:
    :return:
    """
    print("User pressed the '{:}' key".format(event.key))

    global configurations_to_plot, full_configuration_list

    if event.key.lower() == "x":
        #   Use the 'x' key to close all active figures
        for fig_num in plt.get_fignums():
            #   Loop over all the active figures and close the figure
            plt.figure(fig_num, figsize=(FIG_WIDTH_CM / 2.54, FIG_HEIGHT_CM / 2.54))
            plt.close()
        return

    if event.key.lower() == "c":
        #   Use the 'c' key to allow selection of configurations
        print("Showing configuration selection dialog")

        dialog = configuration_dialog.Ui_Dialog()

        dialog.set_configurations(full_configuration_list, configurations_to_plot)

        if dialog.exec_() == QDialog.Accepted:
            print('Selected configurations: {:}'.format(dialog.get_checked_configurations()))

            configurations_to_plot = dialog.get_checked_configurations()

    if event.key in char_range('1', '9'):
        #   Use the keys 1..9 to reconfigure the number of columns in the legends
        cols = int(event.key)

        global legend_cols
        legend_cols = cols

    if event.key.lower() == "n":
        #   Use the 'n' key to clear the graphs
        configurations_to_plot = []

    if event.key.lower() == "a":
        #   Use the 'a' key to select all
        configurations_to_plot = full_configuration_list

    if event.key.lower() == " ":
        #   Use the ' ' key to select the first 5 configurations
        configurations_to_plot = full_configuration_list[:5]
        selected_configuration = -1

    if event.key.lower() == "g":
        #   Use the 'g' key to enable grid lines
        global grid_lines
        grid_lines = not grid_lines

    if event.key.lower() == "up":
        global selected_configuration
        if selected_configuration > -1:
            #   select prev
            selected_configuration -= 1
        else:
            #   select last
            selected_configuration = len(configurations_to_plot) - 1

    if event.key.lower() == "down":
        global selected_configuration
        if selected_configuration < len(configurations_to_plot) - 1:
            #   select next
            selected_configuration += 1
        else:
            #   no selection
            selected_configuration = - 1

    #   if event.key.lower() == "[":
    #    global current_colour_map, colourmap_iterator
    #    current_colour_map = next(colourmap_iterator, False)
    #    print('Selected colour map: {:}'.format(current_colour_map))

    if event.key.lower() == "]":
        global current_colour_map
        current_colour_map = next(colourmap_iterator, True)
        print('Selected colour map: {:}'.format(current_colour_map))

    if figure_action_total_lin is not None:
        plot_action_total_data(figure=figure_action_total_lin, log_scale=False)
    if figure_action_total_log is not None:
        plot_action_total_data(figure=figure_action_total_log, log_scale=True)
    if figure_action_max is not None:
        plot_action_max_data(figure=figure_action_max)

    #   Make sure the graphs are redrawn
    plt.show()

    return


def main(argv, plot_maximum_graph=True, plot_total_log_graph=True, plot_total_lin_graph=True):
    """
    Entry point to script
    :param argv:
    :return:
    """
    if path.exists(argv[0]):
        inputDir = argv[0] + "/"
    else:
        print("Please provide a valid configuration directory")
        sys.exit(255)

    global ensemble_name
    ensemble_name = argv[1]

    #   Create figures for each of our graphs
    global figure_action_total_lin, figure_action_total_log, figure_action_max
    if plot_total_lin_graph:
        figure_action_total_lin = plt.figure("action total (linear)", figsize=(FIG_WIDTH_CM / 2.54, FIG_HEIGHT_CM / 2.54))
    if plot_total_log_graph:
        figure_action_total_log = plt.figure("action total (log)", figsize=(FIG_WIDTH_CM / 2.54, FIG_HEIGHT_CM / 2.54))
    if plot_maximum_graph:
        figure_action_max = plt.figure("action max", figsize=(FIG_WIDTH_CM / 2.54, FIG_HEIGHT_CM / 2.54))

    #   figureActionTotalEnsembleAverage = make_figure("action total - ensemble average",
    # "V16X32\\J02\\MU0700\\config.b210k1577mu0700j02s16t32", "action total")

    #action_total_ensemble = []

    #   Initialize a list of files in the current directory
    files = []
    
    #   Obtain a list of files in the target directory
    for (dir_path, dir_names, file_names) in walk(inputDir):
        files.extend(file_names)
        break

    #   Process each file that represents an action series,
    #   or ffdual series
    for file in files:
        #   Do a regex match on the filenames
        is_action_file = re.match(actionFileRegex, file)

        if is_action_file:
            print("Found action data in file '{:}'".format(file))

            #   Extract strings from regex
            filename = is_action_file.group()
            conf = is_action_file.group(CONF_INDEX)

            inFile = inputDir + filename
            act_tot, act_max = parse_action_file(inFile)

            #   Store the data series
            ensemble_dict["conp{:}".format(conf)] = (act_tot, act_max)

    #   To begin with mark all configurations for plotting and store the full list for reference
    global configurations_to_plot, full_configuration_list
    for configuration in ensemble_dict.keys():
        full_configuration_list.append(configuration)
        configurations_to_plot.append(configuration)

    #   Sort into ascending order
    full_configuration_list.sort()
    configurations_to_plot.sort()

            #action_total_ensemble = map(operator.add, action_total_ensemble, result['actTot'])

    #   respond to the user pressing keys on the keyboard
    if figure_action_max is not None:
        cid_0 = figure_action_max.canvas.mpl_connect('key_press_event', key_press_handler)

    if figure_action_total_lin is not None:
        cid_1 = figure_action_total_lin.canvas.mpl_connect('key_press_event', key_press_handler)

    if figure_action_total_log is not None:
        cid_2 = figure_action_total_log.canvas.mpl_connect('key_press_event', key_press_handler)

    #   Plot the three figures
    if figure_action_total_lin is not None:
        plot_action_total_data(figure_action_total_lin, log_scale=False)

    if figure_action_total_log is not None:
        plot_action_total_data(figure_action_total_log, log_scale=True)

    if figure_action_max is not None:
        plot_action_max_data(figure_action_max)

    #   Show our figures
    plt.show()

    return


if __name__ == "__main__":
    print("Number of arguments: {:}".format(len(sys.argv)))

    if len(sys.argv) == 3:
        app = QApplication(sys.argv)
        print("Requesting action graph for dir '{:}'".format(sys.argv[1]))
        main(sys.argv[1:])
    else:
        app = QApplication(sys.argv)
        main(["/media/dean/raw_data/Dean/QCD Data/Cooled Configurations/Pygrid/12x24/mu0900/config.b190k1680mu0900j02s12t24",
              "config.b190k1680mu0900j02s12t24"])
        print("Usage: visualize_ensemble <ensemble dir> <ensemble title>")
