import matplotlib.pyplot as plt
import numpy as np
import re
import sys
import Common.GraphFormatIterator as gf
import Common.ErrorComputations as er
import Common.FileLists as fl
import Common.Mu as mu
import os
import pickle
from Common.util import *

#   filename1 = R'E:\Dean\QCD Data\Field data\Pygrid\12x24\mu0500\config.b190k1680mu0500j02s12t24\conp0005\cool0020\topological charge density.hvol1'
#   filename2 = R'E:\Dean\QCD Data\Field data\Pygrid\12x24\mu1000\config.b190k1680mu1000j02s12t24\conp0005\cool0020\topological charge density.hvol1'

#   Minimum / maximum values (+/-) magnitude
#   DATA_MAGNITUDE = 46.0

#   Use LaTeX to provide type1 fonts for the graphs
plt.rcParams['ps.useafm'] = True
plt.rcParams['pdf.use14corefonts'] = True
plt.rcParams['text.usetex'] = True

#   eg: 1 2 3 4 -0.5444411
SCALAR_REGEX_3D = R'(\d+)\s*(\d+)\s*(\d+)\s*([+-]?\d*\.\d*)'
SCALAR_INDEX_3D = 4


SCALAR_REGEX_4D = R'(\d+)\s*(\d+)\s*(\d+)\s*(\d+)\s*([+-]?\d*\.\d*)'
SCALAR_INDEX_4D = 5


SAVE_PDF = True




def parse_scalars(filename):
    """
    Reads a volume containing a scalar hyper-volume and returns the scalars as a list
    """
    print('Processing file: %s' %(filename))

    if os.path.exists(filename + '.cache'):
        #   If we can we'll load a cached version of the data
        cache_file = open(filename + '.cache', 'rb')
        try:
            result = pickle.load(cache_file)
            cache_file.close()
            return result
        except pickle.UnpicklingError:
            print('Loading failed, will regenerate data')
            cache_file.close()

    #   Read contents of the file
    file = open(filename, 'r')
    file_contents = file.read().splitlines()
    file.close()

    result = []

    #   Loop over file contents
    for line in file_contents:
        # print("%s" %(line))

        #   Check if the line contains a scalar value
        matches_4d = re.search(SCALAR_REGEX_4D, line)
        if matches_4d is not None:
            #   Extract the scalar value (from 4D heightfield)
            scalar_value = float(matches_4d.group(SCALAR_INDEX_4D))
            result.append(scalar_value)
            # print('%f' % (scalarValue))
        else:
            matches_3d = re.search(SCALAR_REGEX_3D, line)
            if matches_3d is not None:
                #   Extract the scalar value (from 3D heightfield)
                scalar_value = float(matches_3d.group(SCALAR_INDEX_3D))
                result.append(scalar_value)

    #   Cache for next time
    cache_file = open(filename + '.cache', 'wb')
    pickle.dump(result, cache_file)
    cache_file.close()
    return result


def gen_float_array(min_val, max_val, step):
    """Generates a float array in the range [min_val, max_val)"""

    assert (min_val < max_val)
    assert (isinstance(min_val, (float, int)))
    assert (isinstance(max_val, (float, int)))
    assert (isinstance(step, (float, int)))

    result = []
    i = min_val
    result.append(i)

    while i < max_val:
        i += step
        result.append(i)

    return result


def process_ensemble_files(file_list):
    #   Ensemble average
    ensemble_raw_scalars = []

    #   individual configurations
    input_configurations = []

    for file_name in file_list:
        #

        # Obtain a list of the scalars it the configuration
        raw_scalars = parse_scalars(file_name)

        for raw_scalar in raw_scalars:
            # Add each of the scalars to the ensemble array
            ensemble_raw_scalars.append(raw_scalar)

        input_configurations.append(raw_scalars)

    return ensemble_raw_scalars, input_configurations


def add_ensemble_to_histogram(bins, mu, cools, size_t, fmt, show_bars=False, show_error_bars=True):
    """
    Adds an entire ensemble to the current histogram
    :param bins: an array setting out the limits of the bins for the binning
    :param mu: the value of mu for the ensemble
    :param fmt: a formatting string for the error bar plot
    :param show_bars: shows/hides the actual histogram bars
    :param show_error_bars: shows/hides error bars
    :return: None
    """

    #   Retrieve the colour string from the current format
    colour = gf.get_colour_from_format_string(fmt)

    #ensemble_scalars, configuration_array = process_ensemble_files([ R"E:/Dean/QCD Data/Field data/Pygrid/12x24/mu0500/config.b190k1680mu0500j02s12t24/conp0015/cool0000/Polyakov.vol1"])

    #   Starting point of the ensemble traversalplt.figure('Distribution of objects in Polyakov field at {:} cools {:} ensembles'.format(cools, len(mu_array)))
    ensemble_root_dir = '/media/dean/raw_data/Dean/QCD Data/Field data/Pygrid/12x{:}'.format(size_t)
    print(ensemble_root_dir)

    #   Retrieve a list of all the files in the ensemble
    #ensemble_files = fl.get_list_of_jcn_files_for_ensemble(data_root_dir=ensemble_root_dir, mu=mu, cools=cools, file_name='Polyakov.vol1')
    ensemble_files = fl.get_list_of_jcn_files_for_ensemble(data_root_dir=ensemble_root_dir, mu=mu, cools=cools,
                                                           file_name='polyakov.vol1')

    #   Retreive an array of all the scalars in the ensemble and an array of individual configurations
    ensemble_scalars, configuration_array = process_ensemble_files(ensemble_files)

    #   Retreive the number of configurations in the ensemble
    configuration_count = len(configuration_array)

    #   compute the histogram (ensemble sum) but don't plot it yet
    ensemble_hist, bin_edges = np.histogram(ensemble_scalars, bins)
    #ensemble_hist, bin_edges, patches = plt.hist(ensemble_scalars, isovalue_array, histtype='step', color=colour)

    if configuration_count > 1:
        for b in range(len(ensemble_hist)):
            #   Compute the ensemble average in each bin
            ensemble_hist[b] /= configuration_count

    if show_error_bars:
        #   Compute the mid-point of each bin
        bin_centres = 0.5 * (bin_edges[1:] + bin_edges[:-1])

        #   Compute the variance array
        error_bars = er.compute_error_bars(ensemble_hist, configuration_array, configuration_count, len(bins)-1)

        #   Draw the error bars
        plt.errorbar(bin_centres, ensemble_hist, fmt=fmt, label='mu = {:} ({:})'.format(mu, configuration_count),
                     yerr=error_bars)

    if show_bars:
        #   Widths of each bin in the histogram
        widths = np.diff(bin_edges)

        #   Plot the bars
        plt.bar(bin_edges[:-1], ensemble_hist, widths)



    #   Obtain a list of non-zero bins
    #non_zero_bins = np.nonzero(histogram != 0)[0]

    #   Get the index of the first and last non-zero values
    #first = non_zero_bins[0]
    #last = non_zero_bins[len(non_zero_bins) - 1]

    #   Convert to isovalues
    #first_isovalue = bin_edges[first]
    #last_isovalue = bin_edges[last + 1]

    #   Draw a vertical line at the non-zero limits and label with the isovalues
    # plt.axvline(first_isovalue)
    # plt.text(first_isovalue + 1, 5, str(first_isovalue), rotation=90)
    # plt.axvline(last_isovalue)
    # plt.text(last_isovalue + 1, 5, str(last_isovalue), rotation=90)
    return


def plot_histograms(graph_title, cool_array, mu_array, y_step, magnitude, size_t):
    """Plots multiple scalar arrays on the same histogram, with bin sizes specified by y_step"""
    assert (isinstance(y_step, (float, int)))

    #   Define the limits of the histograms
    x_min = -magnitude
    x_max = +magnitude

    #   generate an array of floats for samples on the x axis
    float_array = gen_float_array(x_min, x_max, y_step)
    print(float_array)

    for cools in cool_array:
        fig_title = graph_title + ' ({:} cools) $n_t = {:}$'.format(cools, size_t)

        #   New figure per cooling slice
        plt.figure(generate_linux_friendly_filename(fig_title))

        #   Setup the plots
        plt.xlim(x_min, x_max)
        #plt.yscale('log')
        plt.xlabel('isovalue')
        plt.ylabel('freq')
        plt.title(fig_title)

        #   Get a new format iterator for each graph
        format_iterator = gf.get_line_format_array(len(mu_array))

        for mu in mu_array:
            #   Add each scalar to the histogram
            fmt = next(format_iterator)

            #   Add the histogram for this value of mu
            add_ensemble_to_histogram(bins=float_array, mu=mu, cools=cools, fmt=fmt, size_t=size_t)

        #    Add the legend
        plt.legend(loc='lower center', ncol=len(mu_array)//3)

        if SAVE_PDF:
            output_dir = get_output_dir(n_t = size_t, ensemble_count=len(mu_array))

            if not os.path.exists(output_dir):
                os.makedirs(output_dir)

            output_file = os.path.join(output_dir, generate_linux_friendly_filename(fig_title))

            plt.savefig(output_file, format="pdf")
            print('Saved file: {:}'.format(output_file))

    # Show the histograms
    if not SAVE_PDF:
        plt.show()

    return


def main(graph_title, cool_array, mu_array, magnitude, bin_range, size_t):
    """entry point"""

    #   Plot each set of scalars on the same histogram
    plot_histograms(graph_title, size_t = size_t,
                    cool_array=cool_array, mu_array=mu_array, y_step=bin_range, magnitude=magnitude)



# main([R"E:/Dean/QCD Data/Field data/Pygrid/12x24/mu0500/config.b190k1680mu0500j02s12t24/conp0005/cool0010/topological charge density.hvol1",
#       R"E:/Dean/QCD Data/Field data/Pygrid/12x24/mu0500/config.b190k1680mu0500j02s12t24/conp0005/cool0015/topological charge density.hvol1",
#       R"E:/Dean/QCD Data/Field data/Pygrid/12x24/mu0500/config.b190k1680mu0500j02s12t24/conp0005/cool0020/topological charge density.hvol1"],
#      ['10 cools', '15 cools', '20 cools'], 46.0)

# main(graph_title='Distribution of Polyakov isovalues',
#      args=[R"E:/Dean/QCD Data/Field data/Pygrid/12x24/mu0500/config.b190k1680mu0500j02s12t24/conp0005/cool0000/Polyakov.vol1",
#        R"E:/Dean/QCD Data/Field data/Pygrid/12x24/mu0500/config.b190k1680mu0500j02s12t24/conp0005/cool0005/Polyakov.vol1",
#        R"E:/Dean/QCD Data/Field data/Pygrid/12x24/mu0500/config.b190k1680mu0500j02s12t24/conp0005/cool0010/Polyakov.vol1",
#           R"E:/Dean/QCD Data/Field data/Pygrid/12x24/mu0500/config.b190k1680mu0500j02s12t24/conp0005/cool0015/Polyakov.vol1",
#           R"E:/Dean/QCD Data/Field data/Pygrid/12x24/mu0500/config.b190k1680mu0500j02s12t24/conp0005/cool0020/Polyakov.vol1"],
#       labels=['0 cools', '5 cools' , '10 cools', '15 cools', '20 cools'], magnitude=1.0, bin_range=0.05)



#   Range of input fields:
#   Polyakov [-1, 1]
#   Spacelike plaquette [,]
# main(graph_title='Distribution of time-like plaquette isovalues',
#      cool_array=[0, 10, 20],
#      mu_array=mu.get_full_mu_array(),
#      magnitude=1.0,
#      bin_range=0.05)

MU_16_ARRAY_5 = [0, 300, 500, 700, 900]
MU_16_ARRAY_7 = [0, 300, 500, 600, 700, 800, 900]
MU_16_ARRAY_FULL = [0, 300, 400, 450, 500, 525, 550, 575, 600, 625, 650, 675, 700, 800, 900]
MU_16_DECONFINEMENT_REGION = [300, 400, 450, 500, 550, 600, 650, 700, 800]

# main(graph_title='Distribution of Polyakov isovalues',
#      cool_array=range(0, 21),
#      size_t=16,
#      mu_array=MU_16_ARRAY_5,
#      magnitude=1.0, bin_range=0.05)

MU_24_ARRAY_5 = [0, 300, 500, 700, 900]
MU_24_ARRAY_7 = [0, 300, 850, 900, 950, 1000, 1100]
MU_24_DECONFINEMENT_REGION = [500, 550, 600, 650, 700, 750, 800, 850, 900]
MU_24_ARRAY_FULL = [0, 250, 300, 325, 350, 375, 400, 425, 450, 500, 550, 600, 700, 750, 800, 850, 900, 950, 1000, 1100]

# main(graph_title='Distribution of Polyakov isovalues',
#      cool_array=range(0, 21),
#      size_t=24,
#      mu_array=MU_24_ARRAY_FULL,
#      magnitude=1.0, bin_range=0.05)


# if __name__ == '__main__':
#    main(sys.argv[1:])
