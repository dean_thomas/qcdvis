import os


def delete_cache(root_directory):
    """
    Deletes all cache file from the given root directory
    :param root_directory:
    :return:
    """
    count = 0
    for (dir_path, dir_names, filenames) in os.walk(root_directory):
        for filename in filenames:
            if '.cache' in filename:
                file = os.path.join(dir_path, filename)
                print('Removing file: %s' % file)
                os.remove(file)
                count += 1

    print('Removed %i files.' % count)


delete_cache(R'/media/dean/jcn_data/Dean/data_from_physics_cluster/12x24/experiment_2_tcd_time_fields')