import sys
import os
from subprocess import call
import matplotlib.pyplot as plt
import re

#   Use LaTeX to provide type1 fonts for the graphs
plt.rcParams['ps.useafm'] = True
plt.rcParams['pdf.use14corefonts'] = True
plt.rcParams['text.usetex'] = True

COOLS_MAX = 50

VERTEX_REGEX = R"#\s*Nodes:\s*(\d*)"
EDGE_REGEX = R"#\s*Arcs:\s*(\d*)"
VERSION_REGEX = R"#\s*version\s*(\d*\.\d*)"

COOLS_REGEX = R"cool(\d{4})"

def parseGraphStats(path_to_file):
    print(path_to_file)
    file = open(path_to_file, 'r')
    lines = file.readlines()
    file.close()

    edge_count = 0
    vertex_count = 0

    version_matches = re.search(VERSION_REGEX, lines[0])

    for line in lines:
        if version_matches is None:
            # Reeb Graph output is from a JCN computation
            if '#' not in line:
                elems = line.split()

                if len(elems) == 2:
                    #   Line defines a vertex
                    v_id = int(elems[0])

                    if v_id > vertex_count:
                        vertex_count = v_id
                elif len(elems) == 3:
                    #   Line defines an edge
                    e_id = int(elems[0])

                    if e_id > edge_count:
                        edge_count = int(e_id)
        else:
            # Reeb Graph output is from a Reeb Graph computation
            print(line)

            vertex_matches = re.search(VERTEX_REGEX, line)
            edge_matches = re.search(EDGE_REGEX, line)

            if vertex_matches is not None:
                vertex_count = int(vertex_matches.group(1))

            if edge_matches is not None:
                edge_count = int(edge_matches.group(1))

    if version_matches is not None:
        # JCN algorithm will just store the max id's, so we need to increment these by one for counts
        vertex_count += 1
        edge_count += 1

    # Indices run from 0
    return vertex_count, edge_count

def main(args):
    """
    entry point
    """
    assert (len(args) == 1)
    root_dir = args[0]

    #   If some of the data is unavailable it will default to -255
    cools_array = list(range(0, COOLS_MAX + 1))
    vertex_count_array = [-255] * (COOLS_MAX + 1)
    edge_count_array = [-255] * (COOLS_MAX + 1)
    difference_array = [-255] * (COOLS_MAX + 1)

    for subdir, dirs, files in os.walk(root_dir):
        for file in files:
            # Expand to absolute path
            path_to_file = os.path.join(subdir, file)

            if os.path.splitext(path_to_file)[1] == '.rb':
                #   Slice number (t=11) etc.
                if '12' in path_to_file:
                    print(path_to_file)

                    #   Extract the cooling sample number from the path
                    cools_matches = re.search(COOLS_REGEX, path_to_file)
                    if cools_matches:
                        cool = int(cools_matches.group(1))

                        #   Find the id of the maximum vertex and edge
                        vertex_count, edge_count = parseGraphStats(path_to_file)
                        assert(vertex_count is not None)
                        assert(edge_count is not None)

                        #   Save into the array
                        vertex_count_array[cool] = vertex_count
                        edge_count_array[cool] = edge_count
                        difference_array[cool] = abs(vertex_count - edge_count)


    print(len(vertex_count_array), len(edge_count_array), len(difference_array), len(cools_array))

    plt.plot(cools_array, vertex_count_array, 'k-^')
    plt.plot(cools_array, edge_count_array, 'b-v')
    plt.plot(cools_array, difference_array, 'g-o')

    plt.legend(['vertices ($v$)', 'edges ($e$)', '$|v - e|$'])
    plt.xlabel('cools')
    plt.ylabel('count')
    plt.title('3D Reeb graph properties under cooling')
    #plt.yscale('log')
    plt.show()

main([R"E:\Dean\QCD Data\Field data\Pygrid\12x24\mu0000\config.b190k1680mu0000j00s12t24\conp0072"])