"""
Plot the ensemble average for a single value of mu as the number of cools is varied
"""
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
from matplotlib.ticker import LinearLocator, FormatStrFormatter
import matplotlib.pyplot as plt
import numpy as np
import pickle
import Common.Mu as mu
import subprocess

#   Location of FFMPEG exe
FFMPEG = R'G:\Dean\ffmpeg\bin\ffmpeg.exe'


def load_cache_file(file_name):
    """
    Loads a cache file for all the configuration data in an ensemble
    :param file_name:
    :return:
    """
    output_file = open(file_name, 'rb')
    try:
        x_values_array = pickle.load(output_file)
        y_values_array = pickle.load(output_file)
        output_file.close()
        return x_values_array, y_values_array
    except pickle.UnpicklingError:
        print('Loading failed, will regenerate data')
        output_file.close()
        return None


def compute_ensemble_average(x_vals, y_vals):
    """
    Computes the average for an ensemble and plots it on the histogram
    :param x_vals:
    :param y_vals:
    :param mu:
    :param fmt_iterator: an iterator defining a list of color/marker combinations
    :return: the number of configurations included in this ensemble
    """
    if len(x_vals) is 0:
        return 0, 0

    #   We'll assume that all the frequencies are calculated with the same number of bins
    x_axis = x_vals[0]
    bin_count = len(x_axis)

    #   Number of configurations in the sample
    configuration_count = len(y_vals)

    #   For computing the ensemble average for each bin
    ensemble_y_vals = [0] * bin_count

    for configuration_y_vals in y_vals:
        #   Iterate over each configuration in the input
        #print('conf = %s' %configuration_y_vals)

        #configuration_y_vals = compute_difference(x_axis, configuration_y_vals)

        #   Iterate over each bin in the conficuration and update the global totals
        for bin in range(len(configuration_y_vals)):
            ensemble_y_vals[bin] += configuration_y_vals[bin]

    for bin in range(len(ensemble_y_vals)):
        #   Now average each bin according to the sample size (number of configurations)
        ensemble_y_vals[bin] /= configuration_count

    return x_vals, ensemble_y_vals


def plot_ensemble_average(mu_array, max_cools = 21, z_max = None, save_gif=False):
    frame = 0

    for mu in mu_array:
        fig = plt.figure('ensemble_average_number_of_objects_mu_{:0>4}'.format(mu))
        print('Processing mu={:0>4} ensemble'.format(mu))
        ax = fig.gca(projection='3d')
        x_values = np.arange(-1.0, +1.0, 2 / 256)
        y_values = np.arange(0, 21, 1)
        x_values, y_values = np.meshgrid(x_values, y_values)
        z_values = []

        for cool in range(0, max_cools):
            ensemble_x_vals, ensemble_y_vals = load_cache_file(R'/media/dean/raw_data/Dean/QCD Data/Field data/Pygrid/12x24/mu{:0>4}/polyakov_object_count_cools_{:0>4}.cache'.format(mu, cool))
            average_x_values, average_y_values = compute_ensemble_average(ensemble_x_vals, ensemble_y_vals)
            z_values.append(average_y_values)

        print('x_values (len={:}): {:}'.format(len(x_values), x_values))
        print('y_values (len={:}): {:}'.format(len(y_values), y_values))
        print('Number of z values: {:}'.format(len(z_values)))

        surf = ax.plot_surface(x_values, y_values, z_values, rstride=1, cstride=1, cmap=cm.coolwarm,
                           linewidth=0, antialiased=False)

        #   Set axis labels
        ax.set_xlabel('Isovalue (Polyakov loop)')
        ax.set_ylabel('Cooling iterations')
        ax.set_zlabel('Number of objects\n(Ensemble average)')

        #   Set plot title
        plt.title('Ensemble average object count mu={:0>4}'.format(mu))

        if z_max is not None:
            ax.set_zlim(0.0, z_max)

        ax.zaxis.set_major_locator(LinearLocator(10))
        #ax.zaxis.set_major_formatter(FormatStrFormatter('%.02f'))

        fig.colorbar(surf, shrink=0.5, aspect=5)

        temp_files = []

        if save_gif:
            filename = '{:0>4}.png'.format(frame)
            fig.savefig(filename)
            temp_files.append(filename)
            frame += 1

    if save_gif:
        cmd = [FFMPEG, '-framerate', '10' , '-i', '%04d.png', 'movie.avi']
        subprocess.call(cmd)


    plt.show()


plot_ensemble_average(mu_array=[500], z_max=100, save_gif=False)