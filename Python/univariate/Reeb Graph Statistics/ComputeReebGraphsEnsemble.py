"""
Point at a directory containing multiple configurations to run the slice tool
at specified levels of cooling
"""
import sys
import os
import re
from os.path import isfile, exists
from subprocess import call
import Common.FileLists as fl

#FIELD = 'Polyakov'
REEB_EXE = R"/home/dean/Documents/programming/qcdvis/qcdvis/CommandLineTools/CommandLineTools/build-command_line_tools-Desktop_Qt_5_7_0_GCC_64bit-Release/reeb/reeb"
REEB_STATS_EXE = R"/home/dean/Documents/programming/qcdvis/qcdvis/CommandLineTools/CommandLineTools/build-command_line_tools-Desktop_Qt_5_7_0_GCC_64bit-Release/reeb_histogram/reeb_histogram"


def get_file_list(ensemble_root, file_name, cools, file_extension):
    """
    Recursively obtains a list of files with a specified criteria below a give root directory
    :param ensemble_root: the folder to start the recursion from
    :param file_name: a keyword that must appear in the filename's in the output (e.g. 'Polyakov')
    :param cools: a string array representing the desired number of cools (in the form 'COOL0015')
    :param file_extension: allows the selection to be limited to a specified extension (e.g. '.vol1')
    :return: a list of files represented by their absolute path
    """
    file_list = []

    for subdir, dirs, files in os.walk(ensemble_root):

        for file in files:
            # Expand to absolute path
            path_to_file = os.path.join(subdir, file)

            #print(cools)
            #print(path_to_file)

            # if the number of cools appears in the list...
            if any(st in path_to_file for st in cools):
                if file_name in path_to_file and file_extension in path_to_file:
                    print(path_to_file)
                    file_list.append(path_to_file)

    return file_list


def process_files(file_list, command_line, output_file_name):
    failed = []
    skipped = []

    for input_file in file_list:
        output_dir = os.path.dirname(input_file)

        output_file = os.path.join(output_dir, output_file_name)
        if os.path.exists(output_file):
            #print('File "%s" already exists.  Skipped.' %output_file)
            skipped.append(output_file)
            continue

        if exists(input_file) and isfile(input_file):
            retries = 0
            while retries < 3:
                # execute the command
                return_code = call([command_line, input_file])

                # it would appear the program cannot always open the file first time
                #   try up to 3 times before deciding the file cannot be read.
                if return_code != 0:
                    if retries < 3:
                        retries += 1
                        print('Retry: %d' %(retries))
                    else:
                        failed.append(input_file)
                        break
                else:
                    break
            else:
                print('File "%s" is invalid' %input_file)

    return failed, skipped


def main(args):
    """
    entry point
    """
    assert(len(args) == 1)
    root_dir = args[0]

    """
    Root is intended to be an ensemble directory
    example: /Pygrid/12x24/mu0300/config.b190k1680mu0300j02s12t24

    first get a list of cooling slices
    """
    for cools in range(0, 21):
        for mu in [0, 300, 400, 450, 500, 525, 550, 575, 600, 625, 650, 675, 700, 800, 900]:
            print('Doing Reeb graph computations for mu=%d, cools=%d' %(mu, cools))


            #dir = os.path.join(root_dir, 'mu{:0>4}'.format(mu))

            #   get a list fo scalar fields for processing
            polyakov_vol_files = fl.get_list_of_jcn_files_for_ensemble(data_root_dir=root_dir, mu=mu, cools=cools, file_name='polyakov.vol1')
            failed_vol_files, skipped_vol_files = process_files(polyakov_vol_files, REEB_EXE, 'polyakov.rb')

            reeb_graph_files = fl.get_list_of_jcn_files_for_ensemble(data_root_dir=root_dir, mu=mu, cools=cools, file_name='polyakov.rb')
            failed_reeb_files, skipped_reeb_files = process_files(reeb_graph_files, REEB_STATS_EXE, 'polyakov.rbs')

    print('The Reeb process failed for the following files: %s' % failed_vol_files)
    print('The Reeb process was skipped for the following files: %s' % skipped_vol_files)

    print('The Reeb stats process failed for the following files: %s' % failed_reeb_files)
    print('The Reeb stats process was skipped for the following files: %s' % skipped_reeb_files)



main([R"/media/dean/raw_data/Dean/QCD Data/Field data/Pygrid/12x16/"])


#if __name__ == '__main__':
#    main(sys.argv[1:])
