import re
import matplotlib.pyplot as plt
import numpy as np
import Common.GraphFormatIterator as gf
import Common.ErrorComputations as er
import Common.FileLists as fl
import os
import Common.Mu as mu
import pickle
from Common.util import *

FLOAT_REGEX = R'([+-]?\d*\.?\d+)'

SAVE_PDF = True


def plot_frequency_distributions(graph_title, mu_array, cool_array, root_directory, size_t):
    """
    This function does the main work in the program.  Given a set of cooling iterations and values of mu it will
    generate a graph for each level of mu, with the ensenble avearge at each level of mu plotted.
    :param mu_array: an array of levels of mu to include on each plot
    :param cool_array: a set of cooling level, each will be plotted as a separate graph
    """

    root_directory = os.path.join(root_directory, '12x{:}'.format(size_t))
    for cools in cool_array:
        fig_title = graph_title + ' ({:} cools) $n_t = {:}$'.format(cools, size_t)

        plt.figure(generate_linux_friendly_filename(fig_title))

        plt.title(fig_title)
        plt.xlabel('Isovalue (Polyakov loop)')
        plt.ylabel('Average number of objects')

        # get a new format iterator for each graph
        format_iterator = gf.get_line_format_array(len(mu_array))

        for mu in mu_array:
            # increment the iterator for the next plot
            fmt = next(format_iterator)
            sample_count = plot_ensemble(mu, cools, fmt, root_directory=root_directory)
            #legend_text.append()

        plt.legend(loc='upper center', ncol=len(mu_array)//3)

        if SAVE_PDF:
            output_dir = get_output_dir(experiment_title='polyakov',
                                        data_type='univariate',
                                        data_sub_type='object_count',
                                        n_t=size_t, ensemble_count=len(mu_array))

            if not os.path.exists(output_dir):
                os.makedirs(output_dir)

            output_file = os.path.join(output_dir, generate_linux_friendly_filename(fig_title))

            plt.savefig(output_file, format="pdf")
            print('Saved file: {:}'.format(output_file))

    # Show the histograms
    if not SAVE_PDF:
        plt.show()

    return


def compute_mean(vals):
    total = 0.0

    for i in vals:
        total += i

    total /= len(vals)

    return total


def load_and_plot_single_freq_dist(input_file, pattern):
    x_vals, y_vals = load_rbf_file(input_file)
    plot_single_freq_dist(x_vals, y_vals, pattern)
    return


def plot_single_freq_dist(x_vals, y_vals, pattern):
    plt.errorbar(x=x_vals, y=y_vals, fmt=pattern)

    #   Compute the mean of the values
    mean = compute_mean(x_vals)
    plt.plot((mean, mean), (plt.axes().get_ylim()), pattern)

    return


def compute_difference(x_axis, configuration_y_vals):
    """
    Computes the difference between a sample and the corresponding negative sample
    :param x_axis:
    :param configuration_y_vals:
    :return:
    """
    # Must be an even number of bins to be accurate
    assert(len(configuration_y_vals) == len(x_axis))
    assert(len(configuration_y_vals) % 2 == 0)

    configuration_y_difference_vals = [0] * len(configuration_y_vals)

    # For a index i, returns the corresponding index with a negative sample value
    neg_index_func = lambda i: len(configuration_y_vals) - i - 1

    for i in range(len(configuration_y_vals)):
        neg_i = neg_index_func(i)
        print(i, x_axis[i], neg_i, x_axis[neg_i])
        assert (float('{0:.4f}'.format(x_axis[i])) == -float('{0:.4f}'.format(x_axis[neg_i])))

        configuration_y_difference_vals[i] = x_axis[i] - x_axis[neg_i]
        print(configuration_y_difference_vals[i])

    return configuration_y_difference_vals


def plot_average_freq_dist(x_vals, y_vals, mu, fmt):
    """
    Computes the average for an ensemble and plots it on the histogram
    :param x_vals:
    :param y_vals:
    :param mu:
    :param fmt_iterator: an iterator defining a list of color/marker combinations
    :return: the number of configurations included in this ensemble
    """
    if len(x_vals) is 0:
        return 0

    #   We'll assume that all the frequencies are calculated with the same number of bins
    x_axis = x_vals[0]
    bin_count = len(x_axis)

    #   Number of configurations in the sample
    configuration_count = len(y_vals)

    #   For computing the ensemble average for each bin
    ensemble_y_vals = [0] * bin_count

    for configuration_y_vals in y_vals:
        #   Iterate over each configuration in the input
        print('conf = %s' %configuration_y_vals)

        #configuration_y_vals = compute_difference(x_axis, configuration_y_vals)

        #   Iterate over each bin in the conficuration and update the global totals
        for bin in range(len(configuration_y_vals)):
            ensemble_y_vals[bin] += configuration_y_vals[bin]

    for bin in range(len(ensemble_y_vals)):
        #   Now average each bin according to the sample size (number of configurations)
        ensemble_y_vals[bin] /= configuration_count

    # Compute the error bars for each sample in the ensemble
    error_bars = er.compute_error_bars(ensemble_y_vals, y_vals, configuration_count, bin_count)

    # Plot the data with error bars, then retrieve the colour for use in plot the average
    ebar = plt.errorbar(x=x_axis, y=ensemble_y_vals,
                        yerr=error_bars,
                        fmt=fmt,
                        label='mu = {:} ({:})'.format(mu, configuration_count))
    plot_color = ebar[0].get_color()

    if False:
        #   Compute the mean of the histograms
        ensemble_total_area = 0
        ensemble_total_height = 0

        for bin in range(len(ensemble_y_vals)):
            #   To do: compute this for each configuration to help compute the sd.
            ensemble_total_area += (x_axis[bin] * ensemble_y_vals[bin])
            ensemble_total_height += ensemble_y_vals[bin]

        #   Draw a vertical line through the ensemble average
        ensemble_mean = ensemble_total_area / ensemble_total_height
        plt.errorbar((ensemble_mean, ensemble_mean), (plt.axes().get_ylim()),
                fmt=fmt)

    return configuration_count


def save_cache_file(x_values_array, y_values_array, file_name):
    """
    Saves all of the configuration data for an entire ensemble to a cache file, so that it doesn't need
    recomputing each time
    :param x_values_array:
    :param y_values_array:
    :param file_name:
    :return:
    """
    output_file = open(file_name, 'wb')
    pickle.dump(x_values_array, output_file)
    pickle.dump(y_values_array, output_file)
    output_file.close()

    return


def load_cache_file(file_name):
    """
    Loads a cache file for all the configuration data in an ensemble
    :param file_name:
    :return:
    """
    output_file = open(file_name, 'rb')
    try:
        x_values_array = pickle.load(output_file)
        y_values_array = pickle.load(output_file)
        output_file.close()
        return x_values_array, y_values_array
    except pickle.UnpicklingError:
        print('Loading failed, will regenerate data')
        output_file.close()
        return None


def load_rbf_file(filename):
    """
    Loads a '.rbf' file.  This contains a list of isovalues that the Reeb graph is sampled at, along with the number
    of unique objects at each sample value as we sweep through the function height
    :param filename:
    :return:
    """
    input_file = open(filename)
    print('opening file: {:}'.format(filename))
    lines = input_file.readlines()
    input_file.close()

    input_type = 'None'

    x_vals = []
    y_vals = []

    for line in lines:
        if 'x axis' in line:
            input_type = 'x axis'
        elif 'y axis' in line:
            input_type = 'y axis'

        float_matches = re.search(FLOAT_REGEX, line)

        if float_matches is not None:
            if input_type is 'x axis':
                x_vals.append(float(float_matches.group(1)))
            elif input_type is 'y axis':
                y_vals.append(float(float_matches.group(1)))

    #print(len(x_vals), len(y_vals))
    assert(len(x_vals) == len(y_vals))

    return x_vals, y_vals


def plot_ensemble(mu, cools, fmt_iterator, root_directory):
    """
    Obtains a list of files for all configurations in an ensemble, reads them, and then plots them on a common
    graph shared with multiple ensemble averages
    :param mu:
    :param cools:
    :param fmt_iterator:
    :return:
    """
    x_val_array = []
    y_val_array = []

    #   Root directory of where to look for the data for this ensemble
    ensemble_root_dir = os.path.join(root_directory, 'mu{:0>4}'.format(mu))
    print('Ensemeble root: %s' % ensemble_root_dir)

    #   For caching the output
    cache_filename = os.path.join(ensemble_root_dir, 'polyakov_object_count_cools_{:0>4}.cache'.format(cools))

    if not os.path.exists(cache_filename):
        #   Cache file doesn't exist, so we parse all the files again
        input_files = fl.get_file_list(ensemble_root_dir, file_name='polyakov', file_extension='rbf', cools=cools)

        for file in input_files:
            #   Load the values for each configuration in the ensemble
            conf_x_vals, conf_y_vals = load_rbf_file(file)
            x_val_array.append(conf_x_vals)
            y_val_array.append(conf_y_vals)

        #   Cache the complete ensemble output for next time...
        save_cache_file(x_val_array, y_val_array, file_name=cache_filename)
    else:
        print('found cache file: %s' % cache_filename)

        #   Load from the cache
        x_val_array, y_val_array = load_cache_file(file_name=cache_filename)

    return plot_average_freq_dist(x_val_array, y_val_array, mu, fmt_iterator)

# plot_frequency_distributions(mu_array=[0, 300, 400, 450, 500, 525,
#                                        550, 575, 600, 625, 650, 675, 700, 800, 900],
#                              cool_array=range(0, 21),
#                              root_directory='/media/dean/raw_data/Dean/QCD Data/Field data/Pygrid/12x16')


MU_16_ARRAY_5 = [0, 300, 500, 700, 900]
MU_16_ARRAY_7 = [0, 300, 500, 600, 700, 800, 900]
MU_16_ARRAY_FULL = [0, 300, 400, 450, 500, 525, 550, 575, 600, 625, 650, 675, 700, 800, 900]
MU_16_DECONFINEMENT_REGION = [300, 400, 450, 500, 550, 600, 650, 700, 800]

# plot_frequency_distributions(mu_array=MU_16_DECONFINEMENT_REGION,
#                              cool_array=range(0, 21),
#                              size_t=16,
#                              root_directory='/media/dean/raw_data/Dean/QCD Data/Field data/Pygrid',
#                              graph_title='Distribution of objects in Polyakov field')

MU_24_ARRAY_5 = [0, 300, 500, 700, 900]
MU_24_ARRAY_7 = [0, 300, 850, 900, 950, 1000, 1100]
MU_24_DECONFINEMENT_REGION = [500, 550, 600, 650, 700, 750, 800, 850, 900]
MU_24_ARRAY_FULL = [0, 250, 300, 325, 350, 375, 400, 425, 450, 500, 550, 600, 700, 750, 800, 850, 900, 950, 1000, 1100]

plot_frequency_distributions(mu_array=MU_24_ARRAY_5,
                             cool_array=range(0, 21),
                             size_t=24,
                             root_directory='/media/dean/raw_data/Dean/QCD Data/Field data/Pygrid',
                             graph_title='Distribution of objects in Polyakov field')
#create_histogram([0])



#create_histogram([0, 250, 300, 325, 350])
#create_histogram([0, 500, 1000])

