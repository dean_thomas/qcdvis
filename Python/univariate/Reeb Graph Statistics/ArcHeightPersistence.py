import Common.RbsParser as rbs
import matplotlib.pyplot as plt
import Common.GraphFormatIterator as gf
import Common.Mu as mu
import numpy as np
import Common.ErrorComputations as er
import Common.FileLists as fl
from matplotlib.ticker import FuncFormatter
from Common.util import *

SAVE_PDF = True


def new_histogram(mu_array, cools, sub_key, graph_title, x_axis_title, y_axis_title, size_t, x_limits=None, y_limits=None,
                  x_log=False, y_log=False, upper_leaves_only=False, lower_leaves_only=False, bin_count=256):
    #   Create a new graph
    fig_title = graph_title + ' ({:} cools) $n_t = {:}$'.format(cools, size_t)

    fig = plt.figure(generate_linux_friendly_filename(fig_title))
    plt.title(fig_title)

    #   Label the axes
    plt.xlabel(x_axis_title)
    plt.ylabel(y_axis_title)

    if x_log:
        plt.xscale('log')
        x_formatter = FuncFormatter(lambda x, pos: "%.4f" % (x))
        ax = plt.gca()
        ax.xaxis.set_major_formatter(x_formatter)

    if y_log:
        plt.yscale('log')

    if x_limits is not None:
        plt.xlim(x_limits)

    if y_limits is not None:
        plt.ylim(y_limits)

    # Create a new format iterator
    format_iterator = gf.get_line_format_array(len(mu_array))

    for mu in mu_array:
        #   Next format string
        fmt = next(format_iterator)

        add_ensemble_to_histogram(mu=mu, cools=cools, fmt=fmt, sub_key=sub_key, upper_leaves_only=upper_leaves_only,
                                  lower_leaves_only=lower_leaves_only, bin_count=bin_count, size_t=size_t)

    plt.legend(loc='upper center', ncol=2)

    if SAVE_PDF:
        output_dir = get_output_dir(experiment_title='polyakov',
                                    data_type='univariate',
                                    data_sub_type=sub_key,
                                    n_t=size_t, ensemble_count=len(mu_array))

        if not os.path.exists(output_dir):
            os.makedirs(output_dir)

        output_file = os.path.join(output_dir, generate_linux_friendly_filename(fig_title))

        plt.savefig(output_file, format="pdf")
        print('Saved file: {:}'.format(output_file))

    return


def extract(reeb_graph, sub_key, upper_leaves_only=False, lower_leaves_only=False):
    #   We can't filter nodes to only be upper and lower leaves - we'll have nothing left
    assert (not ((upper_leaves_only == True) and (lower_leaves_only == True)))
    values = []

    superarcs, supernodes = reeb_graph

    if upper_leaves_only:
        # print('Filtering upper nodes.  Current node count is %i' % (len(supernodes)))
        supernodes = [n for n in supernodes if rbs.is_upper_leaf(n)]
        # print('After filtering: %i' %(len(supernodes)))

    if lower_leaves_only:
        # print('Filtering lower nodes.  Current node count is %i' % (len(supernodes)))
        supernodes = [n for n in supernodes if rbs.is_lower_leaf(n)]
        # print('After filtering: %i' %(len(supernodes)))

    # if 'Supearc' in key:
    for arc in superarcs:
        #   Loop over superarcs
        if hasattr(arc, sub_key):
            # print('Looking up %s in superarc' %sub_key)
            lookup_value = getattr(arc, sub_key)
            values.append(lookup_value)

    for node in supernodes:
        #   Loop over superarcs
        if hasattr(node, sub_key):
            # print('Node %i is a lower leaf %s' %(getattr(node, 'node_id'), rbs.is_lower_leaf(node)))
            # print('Node %i is an upper leaf %s' % (getattr(node, 'node_id'), rbs.is_upper_leaf(node)))
            lookup_value = getattr(node, sub_key)
            values.append(lookup_value)

    # assert(len(values) > 0)

    return values


def add_ensemble_to_histogram(mu, cools, sub_key, fmt, size_t, show_bars=False, show_error_bars=True, bin_count=256,
                              upper_leaves_only=False, lower_leaves_only=False):
    file_list = fl.get_list_of_jcn_files_for_ensemble(
        '/media/dean/raw_data/Dean/QCD Data/Field data/Pygrid/12x{:}'.format(size_t),
        mu, cools, 'polyakov.rbs')

    ensemble_value_array = []
    configuration_value_array = []

    for file in file_list:
        reeb_graph = parse_file(file_name=file)

        configuration_values = extract(reeb_graph, sub_key, upper_leaves_only=upper_leaves_only,
                                       lower_leaves_only=lower_leaves_only)
        # print(configuration_values)

        if configuration_values is not None and len(configuration_values) > 0:
            configuration_value_array.append(configuration_values)

            for value in configuration_values:
                ensemble_value_array.append(value)

    # Count the number of available configurations
    configuration_count = len(configuration_value_array)

    ensemble_hist, bin_edges = np.histogram(ensemble_value_array, bins=bin_count)

    #   Compute the average of the ensemble histogram
    for b in range(len(ensemble_hist)):
        ensemble_hist[b] /= configuration_count

    if show_bars:
        widths = np.diff(bin_edges)
        plt.bar(bin_edges[:-1], ensemble_hist, widths)

    if show_error_bars:
        configuration_hist_array = []
        for c in range(0, configuration_count):
            print(c, len(configuration_value_array))
            configuration_hist, edges = np.histogram(configuration_value_array[c], bins=bin_count,
                                                     range=(min(ensemble_value_array), max(ensemble_value_array)))
            configuration_hist_array.append(configuration_hist)

        # Compute the variance array
        error_bars = er.compute_error_bars(ensemble_hist, configuration_hist_array, configuration_count, bin_count)

        #   Compute the mid-point of each bin
        bin_centres = 0.5 * (bin_edges[1:] + bin_edges[:-1])

        #   Draw the error bars
        plt.errorbar(bin_centres, ensemble_hist, fmt=fmt, label='mu = {:0>4} ({:})'.format(mu, configuration_count),
                     yerr=error_bars)


def parse_file(file_name):
    file = open(file_name, 'r')
    lines = file.readlines()
    file.close()

    reeb_graph = rbs.parse_rbs_stats(lines)

    return reeb_graph


#   Call for each value of cooling


def plot_object_persistence_graphs(cool_array, mu_array, size_t):
    """
    Plots a graph of the raw object persistence (the actual superarc lengths)
    :param cool_array: an array of cooling slices to be evaluated, each will be assigned to a separate figure
    :return: None
    """
    for cools in cool_array:
        new_histogram(mu_array=mu_array,
                      cools=cools,
                      sub_key='height_range',
                      graph_title='topological object persistence',
                      x_axis_title='object isovalue range (single arc)',
                      y_axis_title='freq',
                      x_limits=(10 ** -3, 2),
                      x_log=True,
                      y_limits=(0, 60),
                      size_t=size_t)

    # Show the histograms
    if not SAVE_PDF:
        plt.show()


def plot_object_upward_persistence_graphs(cool_array, mu_array, size_t):
    """
    Plots a graph of the upward object persistence (the sum of all the upward subtree branches from all supernodes)
    :param cool_array: an array of cooling slices to be evaluated, each will be assigned to a separate figure
    :return: None
    """
    for cools in cool_array:
        new_histogram(mu_array=mu_array,
                      cools=cools,
                      sub_key='upward_height_sum',
                      graph_title='topological object persistence [upward sweep]',
                      x_axis_title='object isovalue range (sub-tree total)',
                      y_axis_title='freq',
                      lower_leaves_only=True,
                      bin_count=8,
                      y_limits=(0, 50),
                      x_limits=(0, 40),
                      size_t=size_t)

    # Show the histograms
    if not SAVE_PDF:
        plt.show()


def plot_object_downward_persistence_graphs(cool_array, mu_array, size_t):
    """
    Plots a graph of the downward object persistence (the sum of all the downward subtree branches from all supernodes)
    :param cool_array: an array of cooling slices to be evaluated, each will be assigned to a separate figure
    :return: None
    """
    for cools in cool_array:
        new_histogram(mu_array=mu_array,
                      cools=cools,
                      sub_key='downward_height_sum',
                      graph_title='topological object persistence [downward sweep]',
                      x_axis_title='object isovalue range (sub-tree total)',
                      y_axis_title='freq',
                      upper_leaves_only=True,
                      bin_count=8,
                      y_limits=(0, 50),
                      x_limits=(0, 40),
                      size_t=size_t)

    # Show the histograms
    if not SAVE_PDF:
        plt.show()


cools_array = range(0, 21)

MU_16_ARRAY_5 = [0, 300, 500, 700, 900]
MU_16_ARRAY_7 = [0, 300, 500, 600, 700, 800, 900]
MU_16_ARRAY_FULL = [0, 300, 400, 450, 500, 525, 550, 575, 600, 625, 650, 675, 700, 800, 900]
MU_16_DECONFINEMENT_REGION = [300, 400, 450, 500, 550, 600, 650, 700, 800]

MU_24_ARRAY_5 = [0, 300, 500, 700, 900]
MU_24_ARRAY_7 = [0, 300, 850, 900, 950, 1000, 1100]
MU_24_DECONFINEMENT_REGION = [500, 550, 600, 650, 700, 750, 800, 850, 900]
MU_24_ARRAY_FULL = [0, 250, 300, 325, 350, 375, 400, 425, 450, 500, 550, 600, 700, 750, 800, 850, 900, 950, 1000, 1100]

mu_array = MU_24_ARRAY_FULL
#mu_array = mu.get_5_mu_array()

plot_object_persistence_graphs(cools_array, mu_array, size_t=24)
plot_object_upward_persistence_graphs(cools_array, mu_array=mu_array, size_t=24)
plot_object_downward_persistence_graphs(cools_array, mu_array=mu_array, size_t=24)
