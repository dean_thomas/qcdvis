#   Used to generate a .sh script for each joint contour net computation in an ensemble
#   Set the variables below to generate the paths to files etc.
#   Then move to the cluster for execution
#
#   Note:   before execution the 'x' flag will need to be set for script to allow it to be run.  use the:
#               $ chmod 777 *.sh command to do this
#           all scripts can then be executed using the following command:
#               for f in *.sh; do ./"$f"; done

import time
import os

mu = 0
slab_size = 2.0 / 32.0
field_count = 2
cool_array = range(0, 21)
conp_array = range(52, 100, 4)

#   Number of slices on the named axis (1..n)
first_slice = 1
last_slice = 24
slice_axis = 't'


def get_next_c(c):
    assert(c + 1) <= max(cool_array)
    return c + 1


FILE_NAME_TEMPLATE = 'conp{:0>4}_cool{:0>4}_cool{:0>4}_fields{:}_ss{:}.sh'

for conp in conp_array:
    for cool in cool_array:
        if cool < max(cool_array):
            next_cool = get_next_c(cool)

            file_name = FILE_NAME_TEMPLATE.format(conp, cool, next_cool, field_count, slab_size)

            #   sub-dir for each pair in the form 'cools_01_02/'
            sub_dir = 'cools_{:0>2}_{:0>2}'.format(cool, next_cool)

            print(file_name)

            script_file = open(file_name, 'w', newline='')

            #   Header
            script_file.write('#!/bin/bash\n')
            script_file.write('#\tAuto generated at: ')
            script_file.write(time.strftime("%Y-%m-%d %H:%M\n"))
            script_file.write('\n')
            script_file.write('MU={:0>4}\n'.format(mu))
            script_file.write('SLAB_SIZE={:}\n'.format(slab_size))
            script_file.write('CONP={:0>4}\n'.format(conp))
            script_file.write('FIELD_COUNT={:}\n'.format(field_count))
            script_file.write('\n')

            #   Paths
            script_file.write('EXE=/home/dean/exe/vtkReebGraphTest\n')
            script_file.write('PATH_TO_DATA_{:}=/mnt/nas/dean/data/12x{:}/mu$MU/conp$CONP/cool{:0>4}\n'.format(cool, last_slice, cool))
            script_file.write('PATH_TO_DATA_{:}=/mnt/nas/dean/data/12x{:}/mu$MU/conp$CONP/cool{:0>4}\n'.format(next_cool, last_slice, next_cool))
            script_file.write('OUT_DIR=/mnt/nas/dean/output/12x{:}/experiment_2_polyakov_fields/mu$MU/conp$CONP/fields$FIELD_COUNT/$SLAB_SIZE/{:}\n'.format(last_slice, sub_dir))
            script_file.write('\n')
            script_file.write('mkdir -p $OUT_DIR\n')
            script_file.write('\n')

            #   Define fields (identifiers) two per experiment
            script_file.write('POLYAKOV_FIELD_{:}="polyakov_{:0>4}"\n'.format(cool, cool))
            script_file.write('POLYAKOV_FIELD_{:}="polyakov_{:0>4}"\n'.format(next_cool, next_cool))

            script_file.write('\n')

            #   Define input files (filenames) two per experiment
            script_file.write('POLYAKOV_FILE_{:}="polyakov.vol1"\n'.format(cool))
            script_file.write('POLYAKOV_FILE_{:}="polyakov.vol1"\n'.format(next_cool))

            script_file.write('\n')

            #   Construct input parameters
            script_file.write('FIELD_{:}="-input \\"\"$PATH_TO_DATA_{:}/$POLYAKOV_FILE_{:}\"\\" \\"$POLYAKOV_FIELD_{:}\\" $SLAB_SIZE"\n'.format(cool,cool,cool,cool))
            script_file.write('FIELD_{:}="-input \\"\"$PATH_TO_DATA_{:}/$POLYAKOV_FILE_{:}\"\\" \\"$POLYAKOV_FIELD_{:}\\" $SLAB_SIZE"\n'.format(next_cool, next_cool, next_cool, next_cool))
            script_file.write('\n')

            #   Write the command
            # script_file.write('CMD="$EXE -reebRb -jcnDot jcn.dot -nogui -outdir \\"$OUT_DIR\\" -periodic -save_logs')
            script_file.write('CMD="$EXE -nogui -outdir \\"\"$OUT_DIR/\"\\" -periodic -slab_stats -frag_stats -save_logs')
            script_file.write(' $FIELD_{:}'.format(cool))
            script_file.write(' $FIELD_{:}'.format(next_cool))
            script_file.write('"\n')
            script_file.write('\n')
            script_file.write('echo $CMD\n')
            script_file.write('\n')
            script_file.write('eval $CMD')

            script_file.close()

            os.chmod(file_name, 0o777)

print("go")

#   Create the go script to run all the experiments.  On completion we will create a file 'done.txt'
#   to signify that the process has completed
go_script_file = open('go', 'w', newline='')
go_script_file.write('#   Run with the following command to run all scripts in a remote directory in a detached shell\n')
go_script_file.write('#       $> nohup ./go <dev/null >stdout.log 2>stderr.log &\n')
go_script_file.write('#\n')
go_script_file.write('#   Progress can be monitored using top\n')
go_script_file.write('#       $> top -u dean\n')
go_script_file.write('#\n')
go_script_file.write('for f in *.sh; do ./"$f"; done\n')
go_script_file.write('echo done > done.txt')
go_script_file.close()
os.chmod('go', 0o777)

#   For submitting via qsub
launch_script_filename = 'poly_mu{:0>4}'.format(mu)
print(launch_script_filename)
launch_script_file = open(launch_script_filename, 'w', newline='')
launch_script_file.write('#!/bin/bash\n')
launch_script_file.write('#\n')
launch_script_file.write('#$ -cwd\n')
launch_script_file.write('##$ -j y\n')
launch_script_file.write('#$ -S /bin/bash\n')
launch_script_file.write('#$ -pe orte 4\n')
launch_script_file.write('#$ -o test.out\n')
launch_script_file.write('#$ -e test.err\n')
launch_script_file.write('#\n')
launch_script_file.write('for f in *.sh; do ./"$f"; done\n')
launch_script_file.write('echo done > done.txt\n')
launch_script_file.close()
os.chmod(launch_script_filename, 0o777)

