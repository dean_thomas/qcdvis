#   Used to generate a .sh script for each joint contour net computation in an ensemble
#   Set the variables below to generate the paths to files etc.
#   Then move to the cluster for execution
#
#   Note:   before execution the 'x' flag will need to be set for script to allow it to be run.  use the:
#               $ chmod 777 *.sh command to do this
#           all scripts can then be executed using the following command:
#               for f in *.sh; do ./"$f"; done

import time
import os

mu = 650
slab_size = 0.5
field_count = 2
cool_array = [0, 5, 10, 15, 20]
#cool_array = [0]

conp_array = range(5, 255, 5)

#   Number of slices on the named axis (1..n)
first_slice = 1
last_slice = 16
slice_axis = 't'


def get_next_n(n):
    return 1 + ((n) % (last_slice))


FILE_NAME_TEMPLATE = 'cool{:0>4}/conp{:0>4}_cool{:0>4}_fields{:}_ss{:}_{:}_{:0>2}_{:0>2}.sh'

for conp in conp_array:
    for cool in cool_array:
        if not os.path.exists('cool{:0>4}'.format(cool)):
            os.makedirs('cool{:0>4}'.format(cool))

        for n in range(first_slice, last_slice+1):
            next_n = get_next_n(n)

            file_name = FILE_NAME_TEMPLATE.format(cool, conp, cool, field_count, slab_size, slice_axis, n, next_n)

            #   sub-dir for each pair in the form 't_01_02/'
            sub_dir = '{:}_{:0>2}_{:0>2}'.format(slice_axis, n, next_n)

            print(file_name)

            script_file = open(file_name, 'w', newline='')

            #   Header
            script_file.write('#!/bin/bash\n')
            script_file.write('#\tAuto generated at: ')
            script_file.write(time.strftime("%Y-%m-%d %H:%M\n"))
            script_file.write('\n')
            script_file.write('MU={:0>4}\n'.format(mu))
            script_file.write('SLAB_SIZE={:}\n'.format(slab_size))
            script_file.write('CONP={:0>4}\n'.format(conp))
            script_file.write('COOL={:0>4}\n'.format(cool))
            script_file.write('FIELD_COUNT={:}\n'.format(field_count))
            script_file.write('\n')

            #   Paths
            script_file.write('EXE=~/exe/vtkReebGraphTest\n')
            script_file.write('PATH_TO_DATA=/mnt/nas/dean/data/12x{:}/mu$MU/conp$CONP/cool{:0>4}\n'.format(last_slice, cool))
            script_file.write('OUT_DIR=/mnt/nas/dean/output/12x{:}/experiment_2_tcd_time_fields/mu$MU/conp$CONP/cool$COOL/fields$FIELD_COUNT/$SLAB_SIZE/{:}/\n'.format(last_slice, sub_dir))
            script_file.write('\n')
            script_file.write('mkdir -p $OUT_DIR\n')
            script_file.write('\n')

            #   Define fields (identifiers) two per experiment
            script_file.write('TCD_FIELD_{:}="Topological charge density {:}={:}"\n'.format(n,slice_axis,n))
            script_file.write('TCD_FIELD_{:}="Topological charge density {:}={:}"\n'.format(next_n, slice_axis, next_n))

            script_file.write('\n')

            #   Define input files (filenames) two per experiment
            script_file.write('TCD_FILE_{:}="Topological charge density_{:}={:}.vol1"\n'.format(n, slice_axis,n))
            script_file.write('TCD_FILE_{:}="Topological charge density_{:}={:}.vol1"\n'.format(next_n, slice_axis, next_n))

            script_file.write('\n')

            #   Construct input parameters
            script_file.write('FIELD_{:}="-input \\"$PATH_TO_DATA/$TCD_FILE_{:}\\" \\"$TCD_FIELD_{:}\\" $SLAB_SIZE"\n'.format(n,n,n))
            script_file.write('FIELD_{:}="-input \\"$PATH_TO_DATA/$TCD_FILE_{:}\\" \\"$TCD_FIELD_{:}\\" $SLAB_SIZE"\n'.format(next_n, next_n, next_n))
            script_file.write('\n')

            #   Write the command
            script_file.write('CMD="$EXE -nogui -outdir \\"$OUT_DIR\\" -periodic -save_logs -slab_stats -frag_stats -save_logs')
            script_file.write(' $FIELD_{:}'.format(n))
            script_file.write(' $FIELD_{:}'.format(next_n))
            script_file.write('"\n')
            script_file.write('\n')
            script_file.write('echo $CMD\n')
            script_file.write('\n')
            script_file.write('eval $CMD')

            script_file.close()

            os.chmod(file_name, 0o777)

for cool in cool_array:
    print("go")

    #   Create the go script to run all the experiments.  On completion we will create a file 'done.txt'
    #   to signify that the process has completed
    go_script_file = open('cool{:0>4}/go'.format(cool), 'w', newline='')
    go_script_file.write('#   Run with the following command to run all scripts in a remote directory in a detached shell\n')
    go_script_file.write('#       $> nohup ./go <dev/null >stdout.log 2>stderr.log &\n')
    go_script_file.write('#\n')
    go_script_file.write('#   Progress can be monitored using top\n')
    go_script_file.write('#       $> top -u dean\n')
    go_script_file.write('#\n')
    go_script_file.write('for f in *.sh; do ./"$f"; done\n')
    go_script_file.write('echo done > done.txt')
    go_script_file.close()

    os.chmod('cool{:0>4}/go'.format(cool), 0o777)

#   For submitting via qsub
launch_script_filename = 't_{:}_mu_{:0>4}_tcd'.format(last_slice, mu)
print(launch_script_filename)
launch_script_file = open(launch_script_filename, 'w', newline='')
launch_script_file.write('#!/bin/bash\n')
launch_script_file.write('#\n')
launch_script_file.write('#$ -cwd\n')
launch_script_file.write('##$ -j y\n')
launch_script_file.write('#$ -S /bin/bash\n')
launch_script_file.write('#$ -pe orte 4\n')
launch_script_file.write('#$ -o test.out\n')
launch_script_file.write('#$ -e test.err\n')
launch_script_file.write('#\n')
launch_script_file.write('cd cool0020\n')
launch_script_file.write('for f in *.sh; do ./"$f"; done\n')
launch_script_file.write('echo done > done.txt\n')
launch_script_file.write('cd ..\n')
launch_script_file.write('#\n')
launch_script_file.write('cd cool0015\n')
launch_script_file.write('for f in *.sh; do ./"$f"; done\n')
launch_script_file.write('echo done > done.txt\n')
launch_script_file.write('cd ..\n')
launch_script_file.write('#\n')
launch_script_file.write('cd cool0010\n')
launch_script_file.write('for f in *.sh; do ./"$f"; done\n')
launch_script_file.write('echo done > done.txt\n')
launch_script_file.write('cd ..\n')
launch_script_file.write('#\n')
launch_script_file.write('cd cool0005\n')
launch_script_file.write('for f in *.sh; do ./"$f"; done\n')
launch_script_file.write('echo done > done.txt\n')
launch_script_file.write('cd ..\n')
launch_script_file.write('#\n')
launch_script_file.write('cd cool0000\n')
launch_script_file.write('for f in *.sh; do ./"$f"; done\n')
launch_script_file.write('echo done > done.txt\n')
launch_script_file.write('cd ..\n')
os.chmod(launch_script_filename, 0o777)