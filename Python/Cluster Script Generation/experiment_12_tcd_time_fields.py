#   Used to generate a .sh script for each joint contour net computation in an ensemble
#   Set the variables below to generate the paths to files etc.
#   Then move to the cluster for execution
#
#   Note:   before execution the 'x' flag will need to be set for script to allow it to be run.  use the:
#               $ chmod 777 *.sh command to do this
#           all scripts can then be executed using the following command:
#               for f in *.sh; do ./"$f"; done

import time
import os

mu = 1000
slab_size = 0.25
field_count = 12
cool_array = [10, 15, 20]
conp_array = range(5, 255, 5)

FILE_NAME_TEMPLATE = 'conp{:0>4}_cool{:0>4}_fields{:}_ss{:}.sh'

for conp in conp_array:
    for cool in cool_array:
        file_name = FILE_NAME_TEMPLATE.format(conp, cool, field_count, slab_size)

        print(file_name)

        script_file = open(file_name, 'w', newline='')

        #   Header
        script_file.write('#!/bin/bash\n')
        script_file.write('#\tAuto generated at: ')
        script_file.write(time.strftime("%Y-%m-%d %H:%M\n"))
        script_file.write('\n')
        script_file.write('MU={:0>4}\n'.format(mu))
        script_file.write('SLAB_SIZE={:}\n'.format(slab_size))
        script_file.write('CONP={:0>4}\n'.format(conp))
        script_file.write('COOL={:0>4}\n'.format(cool))
        script_file.write('FIELD_COUNT={:}\n'.format(field_count))
        script_file.write('\n')

        #   Paths
        script_file.write('EXE=/home/dean/exe/vtkReebGraphTest\n')
        script_file.write('PATH_TO_DATA="/scratch/dean/data/12x16/mu$MU/conp$CONP/cool$COOL"\n')
        script_file.write('OUT_DIR="/scratch/dean/output/12x16/experiment_12_tcd_time_fields/mu$MU/conp$CONP/cool$COOL/fields$FIELD_COUNT/$SLAB_SIZE/"\n')
        script_file.write('\n')
        script_file.write('mkdir -p $OUT_DIR\n')
        script_file.write('\n')

        #   Define fields
        for t in range(1, field_count+1):
            script_file.write('TCD_FIELD_{:}="Topological charge density t={:}"\n'.format(t,t))
        script_file.write('\n')

        #   Define input files
        for t in range(1, field_count + 1):
            script_file.write('TCD_FILE_{:}="Topological charge density_t={:}.vol1"\n'.format(t, t))
        script_file.write('\n')

        #   Construct input parameters
        for t in range(1, field_count + 1):
            script_file.write('FIELD_{:}="-input \\"$PATH_TO_DATA/$TCD_FILE_{:}\\" \\"$TCD_FIELD_{:}\\" $SLAB_SIZE"\n'.format(t,t,t))
        script_file.write('\n')

        #   Write the command
        script_file.write('CMD="$EXE -reebRb -jcnDot jcn.dot -nogui -outdir \\"$OUT_DIR\\" -periodic -save_logs')
        for t in range(1, field_count+1):
            script_file.write(' $FIELD_{:}'.format(t))
        script_file.write('"\n')
        script_file.write('\n')
        script_file.write('echo $CMD\n')
        script_file.write('\n')
        script_file.write('eval $CMD')

        script_file.close()

        os.chmod(file_name, 0o777)

print("go")

#   Create the go script to run all the experiments.  On completion we will create a file 'done.txt'
#   to signify that the process has completed
go_script_file = open('go', 'w', newline='')
go_script_file.write('#   Run with the following command to run all scripts in a remote directory in a detached shell\n')
go_script_file.write('#       $> nohup ./go <dev/null >stdout.log 2>stderr.log &\n')
go_script_file.write('#\n')
go_script_file.write('#   Progress can be monitored using top\n')
go_script_file.write('#       $> top -u dean\n')
go_script_file.write('#\n')
go_script_file.write('for f in *.sh; do ./"$f"; done\n')
go_script_file.write('echo done > done.txt')
go_script_file.close()

os.chmod('go', 0o777)