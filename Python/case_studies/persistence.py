import matplotlib.pyplot as plt

#   Use LaTeX to provide type1 fonts for the graphs (for submission to journals etc)
plt.rcParams['ps.useafm'] = True
plt.rcParams['pdf.use14corefonts'] = True
plt.rcParams['text.usetex'] = True
plt.rcParams["savefig.format"] = 'pdf'

plt.rcParams['savefig.directory'] = '/home/dean/Documents/LaTeX/latex-documents/phd_thesis/graphics/univariate/case_study_1'

x_vals = range(24, 37)

arc_length = [13.1372, 13.4193, 13.6312,
              13.7541, 13.7699, 13.6454,
              13.3194, 12.6709, 11.438,
              9.0266, 4.30106, 0.582465, 0.171363]

nodes_downward = [131, 146, 157,
                  161, 162, 166,
                  166, 165, 163,
                  165, 161, 141, 13]
nodes_on_arc = [130, 145, 156,
                160, 161, 165,
                165, 164, 162,
                164, 160, 140, 12]

samples_downward = [13.1882, 13.4603, 13.6635,
                  13.7949, 13.7957, 13.6697,
                  13.343, 12.746, 11.4576,
                  9.04785, 4.32498, 0.606914, 0.264042]
samples_upward = [-75.2505, -74.8056, -73.8523,
                    -72.363, -70.445, -68.1277,
                    -65.0228, -60.7917, -54.6398,
                    -45.0566, -29.1321, -11.9801, -1.68305]
samples_on_arc = [-62.0669, -61.3495, -60.1924,
                  -58.5832, -56.6507, -54.4589,
                  -51.6805, -48.0984, -43.1796,
                  -36.0087, -24.8108, -11.3779, -1.42474]

#   Take the absolute value
samples_on_arc = [abs(i) for (i) in samples_on_arc]


def arc_length_graph():
    fig = plt.figure('reeb_graph_arc_length')
    ax = fig.add_subplot(1, 1, 1)
    plt.title("Persistence: superarc length")
    plt.xlabel("cools")
    plt.ylabel("length")

    #   Plot the number of nodes, arcs
    plt.plot(x_vals, arc_length, label='arc length')

    #   Use a grid with integer resolution
    plt.minorticks_on()
    #start_y, end_y = ax.get_ylim()
    #ax.set_yticks(range(int(start_y), int(end_y), 1), minor=True)

    start_x, end_x = ax.get_xlim()
    ax.set_xticks(range(int(start_x), int(end_x), 1), minor=True)

    plt.grid(b=True, which='major', color='k', linestyle='-', alpha=0.5)
    plt.grid(b=True, which='minor', color='k', linestyle='-', alpha=0.1)

    plt.legend(loc='best').draggable()


def node_count_graph():
    fig = plt.figure('reeb_graph_node_count')
    ax = fig.add_subplot(1, 1, 1)
    plt.title("Persistence: node count")
    plt.xlabel("cools")
    plt.ylabel("count")

    #   Plot the number of nodes, arcs
    plt.plot(x_vals, nodes_on_arc, label='nodes on arc')
    #plt.plot(x_vals, nodes_downward, label='nodes downward')

    #   Use a grid with integer resolution
    plt.minorticks_on()
    start, end = ax.get_ylim()
    ax.set_yticks(range(int(start), int(end), 5), minor=True)
    plt.grid(b=True, which='major', color='k', linestyle='-', alpha=0.5)
    plt.grid(b=True, which='minor', color='k', linestyle='-', alpha=0.1)

    plt.legend(loc='best').draggable()


def sample_sum_above_below_graph():
    fig = plt.figure('reeb_graph_sample_sum_above_below')
    ax = fig.add_subplot(1, 1, 1)
    plt.title("Persistence: sample sums")
    plt.xlabel("cools")
    plt.ylabel("sum")

    #   Plot the number of nodes, arcs
    plt.plot(x_vals, samples_upward, label='samples upward')
    plt.plot(x_vals, samples_downward, label='samples downward')

    #   Use a grid with integer resolution
    plt.minorticks_on()
    start_y, end_y = ax.get_ylim()
    ax.set_yticks(range(int(start_y), int(end_y), 5), minor=True)

    start_x, end_x = ax.get_xlim()
    ax.set_xticks(range(int(start_x), int(end_x), 1), minor=True)


    plt.grid(b=True, which='major', color='k', linestyle='-', alpha=0.5)
    plt.grid(b=True, which='minor', color='k', linestyle='-', alpha=0.1)


    plt.axhline(0, color='black')

    plt.legend(loc='best').draggable()


def sample_sum_on_arc_graph():
    fig = plt.figure('reeb_graph_sample_sum_on_arc')
    ax = fig.add_subplot(1, 1, 1)
    plt.title("Persistence: sample sum on arc")
    plt.xlabel("cools")
    plt.ylabel("sum")

    #   Plot the number of nodes, arcs
    plt.plot(x_vals, samples_on_arc, label='sample sum on arc')

    #   Use a grid with integer resolution
    plt.minorticks_on()
    start, end = ax.get_ylim()
    ax.set_yticks(range(int(start), int(end), 5), minor=True)
    plt.grid(b=True, which='major', color='k', linestyle='-', alpha=0.5)
    plt.grid(b=True, which='minor', color='k', linestyle='-', alpha=0.1)

    plt.legend(loc='best').draggable()


arc_length_graph()
node_count_graph()
sample_sum_above_below_graph()
sample_sum_on_arc_graph()

plt.show()
