import matplotlib.pyplot as plt

#   Use LaTeX to provide type1 fonts for the graphs (for submission to journals etc)
plt.rcParams['ps.useafm'] = True
plt.rcParams['pdf.use14corefonts'] = True
plt.rcParams['text.usetex'] = True
plt.rcParams["savefig.format"] = 'pdf'

plt.rcParams['savefig.directory'] = '/home/dean/Documents/LaTeX/latex-documents/phd_thesis/graphics/univariate/case_study_1'

nan = float('nan')

x_vals = range(0, 20)

node_count = [nan, nan, nan, nan, 1, 2, 4, 10, 16, 12, 23, 22, 21, 26, 27, 35, 35, 42, 45, 63]#, 88918]
arc_count = [nan, nan, nan, nan, 0, 1, 3, 10, 18, 15, 30, 26, 25, 30, 28, 34, 34, 41, 44, 62]#, 256347]

time_no_io = [nan, nan, nan, nan, nan, 89, 71, 87, 116, 179, 280, 460, 820, 1555, 3019, 6033, 12455, 26776, 59867, 142168]#, 240297]
time_with_io = [nan, nan, nan, nan, nan, 4443, 4724, 5772, 7929, 12005, 20046, 36994, 96112, 162312, 309881, 542465, 991555, 1888546, 3759914, 7908377]#, nan]
memory = [nan, nan, nan, nan, nan, 99.335938, 98.691406, 99.132812, 100.816406, 104.339844, 109.789062, 119.109375, 135.796875, 172.117188, 232.871094, 368.183594, 681.378906 ,1334.269531, 2635.457031, 5234.265625]#, nan]

frag_triangles = [nan, nan, nan, nan, nan, 42560, 44712, 53600, 72432, 108960, 171740, 291064, 534484, 1032540, 2021592, 4001588, 7961244, 15883096, 31745000, 63546569]#, nan]


five_mins = 300000
quarter_gigabyte = 256

node_count_ground_truth = [44]*20
arc_count_ground_truth = [43]*20


def reeb_graph_statistics_graph():
    fig = plt.figure('reeb_graph_approximation')
    ax = fig.add_subplot(1, 1, 1)
    plt.title("Reeb graph approximation")
    plt.xlabel("slab size")
    plt.ylabel("count")

    x_label = []
    for x in x_vals:
        new_x = '$2^{-' + '{:}'.format(x) + '}$'
        print(new_x)
        x_label.append(new_x)

    #   Plot the number of nodes, arcs
    plt.errorbar(x=x_vals, y=node_count, label='supernodes in JCN ($v_{TOT}$)', fmt='rs-')
    plt.errorbar(x=x_vals, y=node_count_ground_truth, fmt='r-', alpha=0.5)

    plt.errorbar(x=x_vals, y=arc_count, label='superarcs in JCN ($a$)', fmt='bd-')
    plt.errorbar(x=x_vals, y=arc_count_ground_truth, fmt='b-', alpha=0.5)

    plt.xticks(x_vals, x_label, rotation=90)
    #plt.xscale('log')
    #   Use a grid with integer resolution
    plt.minorticks_on()
    start_y, end_y = ax.get_ylim()
    ax.set_yticks(range(int(start_y), int(end_y), 2), minor=True)

    start_x, end_x = ax.get_xlim()
    ax.set_xticks(range(int(start_x), int(end_x), 1), minor=True)

    plt.grid(b=True, which='major', color='k', linestyle='-', alpha=0.5)
    plt.grid(b=True, which='minor', color='k', linestyle='-', alpha=0.1)

    plt.legend(loc='best').draggable()


def time_graph():
    fig = plt.figure('jcn_compute_time')
    ax = fig.add_subplot(1, 1, 1)
    plt.title("JCN computation time (computation only)")
    plt.xlabel("slab size")
    plt.ylabel("milliseconds ($10^{-3}$s)")

    x_label = []
    for x in x_vals:
        new_x = '$2^{-' + '{:}'.format(x) + '}$'
        print(new_x)
        x_label.append(new_x)

    #   Plot the number of nodes, arcs
    plt.errorbar(x=x_vals, y=time_no_io, label='computation time', fmt='rs-')
    #plt.errorbar(x=x_vals, y=time_with_io, label='computation time', fmt='rs-')
    #plt.errorbar(x=range(0, 21), y=node_count_ground_truth, fmt='r-', alpha=0.5)

    #plt.errorbar(x=range(0, 21), y=arc_count, label='superarcs in JCN ($a$)', fmt='bd-')
    #plt.errorbar(x=range(0, 21), y=arc_count_ground_truth, fmt='b-', alpha=0.5)

    plt.xticks(x_vals, x_label, rotation=90)
    #plt.yscale('log')
    #   Use a grid with integer resolution
    plt.minorticks_on()
    start_y, end_y = ax.get_ylim()
    ax.set_yticks(range(int(start_y), int(end_y), 5000), minor=True)
    ax.set_yticks(range(int(start_y), int(end_y), 15000), minor=False)

    start_x, end_x = ax.get_xlim()
    ax.set_xticks(range(int(start_x), int(end_x), 1), minor=True)

    plt.grid(b=True, which='major', color='k', linestyle='-', alpha=0.5)
    plt.grid(b=True, which='minor', color='k', linestyle='-', alpha=0.1)

    plt.legend(loc='best').draggable()


def time_with_io_graph():
    fig = plt.figure('jcn_compute_time_with_io')
    ax = fig.add_subplot(1, 1, 1)
    plt.title("JCN computation time (including file output)")
    plt.xlabel("slab size")
    plt.ylabel("milliseconds ($10^{-3}$s)")

    x_label = []
    for x in x_vals:
        new_x = '$2^{-' + '{:}'.format(x) + '}$'
        print(new_x)
        x_label.append(new_x)

    #   Plot the number of nodes, arcs
    #plt.errorbar(x=x_vals, y=time_no_io, label='computation time', fmt='rs-')
    plt.errorbar(x=x_vals, y=time_with_io, label='computation time', fmt='rs-')
    #plt.errorbar(x=range(0, 21), y=node_count_ground_truth, fmt='r-', alpha=0.5)

    #plt.errorbar(x=range(0, 21), y=arc_count, label='superarcs in JCN ($a$)', fmt='bd-')
    #plt.errorbar(x=range(0, 21), y=arc_count_ground_truth, fmt='b-', alpha=0.5)

    plt.xticks(x_vals, x_label, rotation=90)
    #plt.yscale('log')
    #   Use a grid with integer resolution
    plt.minorticks_on()
    start_y, end_y = ax.get_ylim()
    ax.set_yticks(range(int(start_y), int(end_y), five_mins), minor=True)
    ax.set_yticks(range(int(start_y), int(end_y), 3* five_mins), minor=False)

    start_x, end_x = ax.get_xlim()
    ax.set_xticks(range(int(start_x), int(end_x), 1), minor=True)

    plt.grid(b=True, which='major', color='k', linestyle='-', alpha=0.5)
    plt.grid(b=True, which='minor', color='k', linestyle='-', alpha=0.1)

    plt.legend(loc='best').draggable()


def memory_graph():
    fig = plt.figure('jcn_computation_peak_memory_usage')
    ax = fig.add_subplot(1, 1, 1)
    plt.title("JCN computation peak memory usage")
    plt.xlabel("slab size")
    plt.ylabel("Peak memory ussage (megabytes MB)")

    x_label = []
    for x in x_vals:
        new_x = '$2^{-' + '{:}'.format(x) + '}$'
        print(new_x)
        x_label.append(new_x)

    #   Plot the number of nodes, arcs
    plt.errorbar(x=x_vals, y=memory, label='peak memory usage', fmt='rs-')
    #plt.errorbar(x=range(0, 21), y=node_count_ground_truth, fmt='r-', alpha=0.5)

    #plt.errorbar(x=range(0, 21), y=arc_count, label='superarcs in JCN ($a$)', fmt='bd-')
    #plt.errorbar(x=range(0, 21), y=arc_count_ground_truth, fmt='b-', alpha=0.5)

    plt.xticks(x_vals, x_label, rotation=90)
    #plt.yscale('log')
    #   Use a grid with integer resolution
    plt.minorticks_on()
    start_y, end_y = ax.get_ylim()
    ax.set_yticks(range(int(start_y), int(end_y), quarter_gigabyte), minor=True)
    ax.set_yticks(range(int(start_y), int(end_y), 4 * quarter_gigabyte), minor=False)

    start_x, end_x = ax.get_xlim()
    ax.set_xticks(range(int(start_x), int(end_x), 1), minor=True)

    plt.grid(b=True, which='major', color='k', linestyle='-', alpha=0.5)
    plt.grid(b=True, which='minor', color='k', linestyle='-', alpha=0.1)

    plt.legend(loc='best').draggable()



def triangle_graph():
    fig = plt.figure('jcn_computation_triangle_count')
    ax = fig.add_subplot(1, 1, 1)
    plt.title("JCN computation triangle count")
    plt.xlabel("slab size")
    plt.ylabel("Number of triangles")

    x_label = []
    for x in x_vals:
        new_x = '$2^{-' + '{:}'.format(x) + '}$'
        print(new_x)
        x_label.append(new_x)

    # Plot the number of nodes, arcs
    plt.errorbar(x=x_vals, y=frag_triangles, label='triangles', fmt='rs-')
    # plt.errorbar(x=range(0, 21), y=node_count_ground_truth, fmt='r-', alpha=0.5)

    # plt.errorbar(x=range(0, 21), y=arc_count, label='superarcs in JCN ($a$)', fmt='bd-')
    # plt.errorbar(x=range(0, 21), y=arc_count_ground_truth, fmt='b-', alpha=0.5)

    plt.xticks(x_vals, x_label, rotation=90)
    # plt.yscale('log')
    #   Use a grid with integer resolution
    plt.minorticks_on()
    start_y, end_y = ax.get_ylim()
    #ax.set_yticks(range(int(start_y), int(end_y), quarter_gigabyte), minor=True)
    ax.set_yticks(range(int(start_y), int(end_y), 10000000), minor=False)

    start_x, end_x = ax.get_xlim()
    ax.set_xticks(range(int(start_x), int(end_x), 1), minor=True)

    plt.grid(b=True, which='major', color='k', linestyle='-', alpha=0.5)
    plt.grid(b=True, which='minor', color='k', linestyle='-', alpha=0.1)

    plt.legend(loc='best').draggable()

#reeb_graph_statistics_graph()
#time_graph()
#time_with_io_graph()
memory_graph()
triangle_graph()

plt.show()
