import matplotlib.pyplot as plt

#   Use LaTeX to provide type1 fonts for the graphs (for submission to journals etc)
plt.rcParams['ps.useafm'] = True
plt.rcParams['pdf.use14corefonts'] = True
plt.rcParams['text.usetex'] = True
plt.rcParams["savefig.format"] = 'pdf'

plt.rcParams[
    'savefig.directory'] = '/home/dean/Documents/LaTeX/latex-documents/phd_thesis/multivariate_case_studies/case_study_2/mu0700'

x_vals = ['t = (1, 2)', 't = (2, 3)', 't = (3, 4)', 't = (4, 5)', 't = (5, 6)', 't = (6, 7)', 't = (7, 8)', 't = (8, 1)']
instanton_nodes = [0, 5, 39, 199, 212, 44, 6, 0]
jcn_nodes = [572, 371, 339, 584, 472, 237, 360, 806]

#   Does a element wise percentage of two lists
jcn_percentage = [(a / b)*100 for (a, b) in zip(instanton_nodes, jcn_nodes)]


def instanton_node_count():
    fig = plt.figure('instanton_slab_count')
    ax = fig.add_subplot(1, 1, 1)
    plt.title("Number of nodes in joint contour net that relate to instanton")
    plt.xlabel("input fields")
    plt.ylabel("number of nodes")

    #   Plot the number of nodes, arcs
    plt.plot(range(0, len(x_vals)), instanton_nodes, label='Instanton sub-graph')
    plt.plot(range(0, len(x_vals)), jcn_nodes, label='Entire jcn')
    ax.set_xticklabels(x_vals)

    #   Use a grid with integer resolution
    plt.minorticks_on()
    #start_y, end_y = ax.get_ylim()
    #ax.set_yticks(range(int(start_y), int(end_y)), minor=True)

    #start_x, end_x = ax.get_xlim()
    #ax.set_xticks(range(int(start_x), int(end_x), 1), minor=True)

    plt.grid(b=True, which='major', color='k', linestyle='-', alpha=0.5)
    plt.grid(b=True, which='minor', color='k', linestyle='-', alpha=0.1)

    plt.legend(loc='best').draggable()


def instanton_node_percentage():
    fig = plt.figure('instanton_slab_percentage')
    ax = fig.add_subplot(1, 1, 1)
    plt.title("Percentage of nodes in joint contour net that relate to instanton")
    plt.xlabel("input fields")
    plt.ylabel("percentage of nodes")

    #   Plot the number of nodes, arcs
    plt.plot(range(0, len(x_vals)), jcn_percentage)#, label='instanton structure')
    ax.set_xticklabels(x_vals)

    #   Use a grid with integer resolution
    plt.minorticks_on()
    # start_y, end_y = ax.get_ylim()
    ax.set_yticks(range(0, 55, 5), minor=True)

    # start_x, end_x = ax.get_xlim()
    # ax.set_xticks(range(int(start_x), int(end_x), 1), minor=True)

    plt.grid(b=True, which='major', color='k', linestyle='-', alpha=0.5)
    plt.grid(b=True, which='minor', color='k', linestyle='-', alpha=0.1)

    #plt.legend(loc='best').draggable()


# def reeb_graph_critical_wilson_peak():
#     fig = plt.figure('zz')
#     ax = fig.add_subplot(1,1,1)
#     plt.title("Number of +ve and -ve supernodes in Reeb graph")
#     plt.xlabel("cools")
#     plt.ylabel("count")
#
#     #   Use a grid with integer resolution
#     plt.minorticks_on()
#     ax.set_xticks(x_vals, minor=True)
#     #   Defined between the minimum and maximums of the nodes and arcs lists
#     ax.set_yticks(range( min( min(nodes_neg), min(nodes_pos)), max(max(nodes_pos), max(nodes_neg))+1), minor=True)
#     plt.grid(b=True, which='major', color='k', linestyle='-', alpha=0.5)
#     plt.grid(b=True, which='minor', color='k', linestyle='-', alpha=0.1)
#
#     #   Plot the number of nodes, arcs
#     plt.plot(x_vals, nodes_dif, label='Positive supernodes ($V_{+}$)')
#     plt.plot(x_vals, peak_wilson, label='Negative supernodes ($V_{-}$)')


instanton_node_count()
instanton_node_percentage()
#reeb_graph_pos_neg()
# reeb_graph_critical_wilson_peak()

plt.show()
