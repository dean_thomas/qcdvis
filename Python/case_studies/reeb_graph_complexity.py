import matplotlib.pyplot as plt

#   Use LaTeX to provide type1 fonts for the graphs (for submission to journals etc)
plt.rcParams['ps.useafm'] = True
plt.rcParams['pdf.use14corefonts'] = True
plt.rcParams['text.usetex'] = True
plt.rcParams["savefig.format"] = 'pdf'

plt.rcParams['savefig.directory'] = '/home/dean/Documents/LaTeX/latex-documents/phd_thesis/graphics/univariate/case_study_1'

x_vals = range(24, 38)
nodes_pos = [17, 17, 19, 15, 15, 21, 19, 19, 15, 14, 14, 16, 14, 14]
nodes_neg = [9, 7, 7, 9, 9, 11, 7, 7, 9, 12, 6, 6, 6, 6]
arcs = [25, 23, 25, 23, 23, 31, 25, 25, 23, 25, 19, 21, 19, 19]

peak_wilson = [1.370814, 1.484201, 1.610457,
               1.691549, 1.495125, 1.521501,
               1.72172,	1.691776, 1.461593,
               1.630406, 1.88011, 1.80897,
               1.491789]

#   Does a element wise addition of two lists
nodes_tot = [a + b for (a, b) in zip(nodes_pos, nodes_neg)]

#   Does an element wise subtraction of the two lists
nodes_dif = [a - b for (a, b) in zip(nodes_pos, nodes_neg)]
nodes_ave = [(a + b) / 2.0 for (a, b) in zip(nodes_pos, nodes_neg)]

peak_wilson = [12 * i for (i) in peak_wilson]


def reeb_graph_complexity():
    fig = plt.figure('reeb_graph_complexity')
    ax = fig.add_subplot(1,1,1)
    plt.title("Number of supernodes and superarcs in Reeb graph")
    plt.xlabel("cools")
    plt.ylabel("count")

    #   Plot the number of nodes, arcs
    plt.plot(x_vals, nodes_tot, label='Supernodes ($v_{TOT}$)')
    plt.plot(x_vals, arcs, label='Superarcs ($a$)')

    #   Use a grid with integer resolution
    plt.minorticks_on()
    start_y, end_y = ax.get_ylim()
    ax.set_yticks(range(int(start_y), int(end_y)), minor=True)

    start_x, end_x = ax.get_xlim()
    ax.set_xticks(range(int(start_x), int(end_x), 1), minor=True)

    plt.grid(b=True, which='major', color='k', linestyle='-', alpha=0.5)
    plt.grid(b=True, which='minor', color='k', linestyle='-', alpha=0.1)

    plt.legend(loc='best').draggable()


def reeb_graph_pos_neg():
    fig = plt.figure('reeb_graph_critical_node_distribution')
    ax = fig.add_subplot(1,1,1)
    plt.title("Number of +ve and -ve supernodes in Reeb graph")
    plt.xlabel("cools")
    plt.ylabel("count")

    #   Plot the number of nodes, arcs
    plt.plot(x_vals, nodes_pos, label='Positive supernodes ($v_{+}$)', alpha=0.5)
    plt.plot(x_vals, nodes_neg, label='Negative supernodes ($v_{-}$)', alpha=0.5)
    plt.plot(x_vals, nodes_dif, label='Difference ($v_{DIF} = v_{+} - v_{-}$)')

    #   Use a grid with integer resolution
    plt.minorticks_on()
    ax.set_xticks(x_vals, minor=True)

    start_y, end_y = ax.get_ylim()
    ax.set_yticks(range(int(start_y), int(end_y)), minor=True)

    start_x, end_x = ax.get_xlim()
    ax.set_xticks(range(int(start_x), int(end_x), 1), minor=True)

    plt.grid(b=True, which='major', color='k', linestyle='-', alpha=0.5)
    plt.grid(b=True, which='minor', color='k', linestyle='-', alpha=0.1)

    plt.legend(loc='best').draggable()


# def reeb_graph_critical_wilson_peak():
#     fig = plt.figure('zz')
#     ax = fig.add_subplot(1,1,1)
#     plt.title("Number of +ve and -ve supernodes in Reeb graph")
#     plt.xlabel("cools")
#     plt.ylabel("count")
#
#     #   Use a grid with integer resolution
#     plt.minorticks_on()
#     ax.set_xticks(x_vals, minor=True)
#     #   Defined between the minimum and maximums of the nodes and arcs lists
#     ax.set_yticks(range( min( min(nodes_neg), min(nodes_pos)), max(max(nodes_pos), max(nodes_neg))+1), minor=True)
#     plt.grid(b=True, which='major', color='k', linestyle='-', alpha=0.5)
#     plt.grid(b=True, which='minor', color='k', linestyle='-', alpha=0.1)
#
#     #   Plot the number of nodes, arcs
#     plt.plot(x_vals, nodes_dif, label='Positive supernodes ($V_{+}$)')
#     plt.plot(x_vals, peak_wilson, label='Negative supernodes ($V_{-}$)')


reeb_graph_complexity()
reeb_graph_pos_neg()
# reeb_graph_critical_wilson_peak()

plt.show()