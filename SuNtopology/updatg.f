!     
! File:   updatg.f
! Author: Dean - moved from main source code
!
! Created on 26 June 2014, 14:39
!
!***********************************************************
! ok
!  here we update the links
!***********************************************************
      subroutine updatg(ibias,actl,betag,srat,trat)
      implicit real*8 (a-h,o-z)
      include 'parameters.inc'
c
      common/arrays/u11(ncol2,lsize,4)
      common/next/iup(lsize,4),idn(lsize,4)
      common/hb/bbetag,iibias
      dimension uint11(ncol2),dum11(ncol2)
     &,a11(ncol2),b11(ncol2),c11(ncol2)
c
      double complex u11,a11,b11,c11,uint11,dum11
c
      bbetag=betag
      iibias=ibias
      sum=0.
c     all coordinates start from 1
c     point = (ix,iy,iz,it)
c     lsize = ix + iy*(lx1 - 1) + iz*(lx1 - 1)*(lx2 - 1) +
c     it*(lx1 - 1)*(lx2 - 1)*(lx3 - 1)

      do 1 nn=1,lsize
      m1=nn
c
      do 10 mu=1,4
c
      do 11 ij=1,ncol2
      uint11(ij)=(0.,0.)
11    continue
      m5=iup(m1,mu)
c
      do 20 nu=1,4
      if(mu.eq.nu) go to 20
      if(mu.eq.4.or.nu.eq.4)then
      crat=trat
      else
      crat=srat
      endif
c
      do 15 ij=1,ncol2
      a11(ij)=u11(ij,m1,nu)
15    continue
      m3=iup(m1,nu)
      do 17 ij=1,ncol2
      b11(ij)=u11(ij,m3,mu)
17    continue
      call vmx(1,a11,b11,c11,1)
      do 19 ij=1,ncol2
      a11(ij)=u11(ij,m5,nu)
19    continue
      call herm(1,a11,dum11,1)
      call vmx(1,c11,a11,b11,1)
      do 23 ij=1,ncol2
      uint11(ij)=uint11(ij)+b11(ij)*crat
23    continue
      m3=idn(m1,nu)
      do 25 ij=1,ncol2
      b11(ij)=u11(ij,m3,mu)
25    continue
      do 27 ij=1,ncol2
      a11(ij)=u11(ij,m3,nu)
27    continue
      call herm(1,a11,dum11,1)
      call vmx(1,a11,b11,c11,1)
      m3=idn(m5,nu)
      do 31 ij=1,ncol2
      a11(ij)=u11(ij,m3,nu)
31    continue
      call vmx(1,c11,a11,b11,1)
      do 32 ij=1,ncol2
      uint11(ij)=uint11(ij)+b11(ij)*crat
32    continue
20    continue
c
      call herm(1,uint11,dum11,1)
c
      do 36 ij=1,ncol2
      c11(ij)=u11(ij,m1,mu)
36    continue
      call vmx(1,uint11,c11,b11,1)
c
      do icdist=1, ncol-1
         do i1col=1, ncol - icdist 
            i2col = i1col + icdist
            call subgrh(i1col, i2col, b11, c11)
         enddo
      enddo
c
      do 40 ij=1,ncol2
      u11(ij,m1,mu)=c11(ij)
40    continue
      do 42 i=1,ncol
      nc=i+ncol*(i-1)
      sum=sum+real(b11(nc))
42    continue
c
10    continue
1     continue
c
      apq=1.-sum/(24.*ncol*lsize)
      actl=apq
c
      return
      end


