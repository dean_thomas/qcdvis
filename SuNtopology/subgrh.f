!     
! File:   subgrh.f
! Author: Dean - moved from main source code
!
! Created on 26 June 2014, 14:34
!
!*****************************************************
      subroutine subgrh(i1col,i2col,b11,c11)
      implicit real*8 (a-h,o-z)
      include 'parameters.inc'
c c11 is the link field being updated SU(2)
c b11 is the product: staple * link NOT SU(2)
c See pg.353 of Rothe for explanation
      common/hb/betag,ibias,umag,a0,a1,a2,a3
c
      double complex b11(ncol2),c11(ncol2)
      double complex f11,f12,a11(2,2)
      double complex e11,e12,g11,g12
c
      i1 = i1col + (i1col - 1)*ncol
      i2 = i2col + (i2col - 1)*ncol
      i3 = i2col + (i1col - 1)*ncol
      i4 = i1col + (i2col - 1)*ncol
c      write (*,*) i1, i2, i3, i4 
c Re-unitarize b11 to be in SU(2)
c eq. (17.44)
      f11=(b11(i1)+conjg(b11(i2)))*0.5
      f12=(b11(i3)-conjg(b11(i4)))*0.5
      umag=sqrt(f11*conjg(f11)+f12*conjg(f12))
      umag=1./umag
      f11=f11*umag
      f12=f12*umag
c
      if(ibias.eq.1)then
      call heatb
      e11=cmplx(a0,a1)
      e12=cmplx(a2,a3)
      g11=conjg(f11)*e11+conjg(f12)*e12
      g12=-f12*e11+f11*e12
      a11(1,1)=g11
      a11(1,2)=-conjg(g12)
      a11(2,1)=g12
      a11(2,2)=conjg(g11)
      endif
      if(ibias.eq.2)then
      g11= f11*f11-conjg(f12)*f12
      g12= f12*f11+conjg(f11)*f12
      a11(1,1)=conjg(g11)
      a11(1,2)=conjg(g12)
      a11(2,1)=-g12
      a11(2,2)=g11
      endif
      if(ibias.eq.3)then
c using this code for cooling
c a11 is in SU(2) and is proportional to inverse b11
      a11(1,1)=conjg(f11)
      a11(1,2)=conjg(f12)
      a11(2,1)=-f12
      a11(2,2)=f11
      endif
c updates the link
      call vmxsu2(i1col,i2col,c11,a11)
c updates the action
      call vmxsu2(i1col,i2col,b11,a11)
c
      return
      end