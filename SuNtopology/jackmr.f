!     
! File:   jackmr.f
! Author: Dean - moved from main source code
!
! Created on 26 June 2014, 14:33
!
!************************************************************
!  jack-knife errors for ratio avu to avd
!***********************************************************
      subroutine jackmr(num,nmax,avu,avd,aver,err)
      implicit real*8 (a-h,o-z)
c
      dimension avu(nmax),avd(nmax)
      dimension difu(nmax),difd(nmax)
c
      aver=0.0d0
      err=0.0d0
c
      sumu=0.0d0
      sumd=0.0d0
      do 10 n=1,num
      sumu=sumu+avu(n)
      sumd=sumd+avd(n)
10    continue
      do 12 n=1,num
      difu(n)=sumu-avu(n)
      difd(n)=sumd-avd(n)
12    continue
      do 14 n=1,num
      if(difd(n).eq.0.0d0)then
      err=99.0d0
      goto99
      endif
14    continue
c
      asum=0.0d0
      esum=0.0d0
      do 16 n=1,num
      dif=difu(n)/difd(n)
      asum=asum+dif
      esum=esum+dif*dif
16    continue
      asum=asum/num
      esum=esum/num
      esum=esum-asum*asum
c
      if(sumd.ne.0.0) aver=sumu/sumd
      if(esum.gt.0.0) err=sqrt(esum*num)
c
99    continue
      return
      end
