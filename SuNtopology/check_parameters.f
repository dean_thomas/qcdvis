!     
! File:   check_parameters.f
! Author: Dean - moved from main source code
!
! Created on 26 June 2014, 14:41
!
!*************************************************************
!     checks the compatibility between Simon's parameters and mine
!*************************************************************
      subroutine check_parameters()
      include 'parameters.inc'
      
c     in Simon's code, the lattice need to be spatially symmetric
      if ((lx1.ne.lx2).or.(lx1.ne.lx3)) then
         print *, "The lattice is not spatially symmetric"
         stop 111
      endif

c     the spatial size must be equal
      if (lx1.ne.ksize) then
         print *, "Spatial lattice size mismatch"
         stop 112
      endif

c     check of the temporal size
      if (lx4.ne.(ksizet*numtasks)) then
         print *, "Temporal lattice size mismatch"
         stop 113
      endif

c     OK, let's go
      print *, "Check of parameters passed"

      return
      end


