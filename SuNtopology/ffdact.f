!     
! File:   ffdact.f
! Author: Dean - moved from main source file
!
! Created on 03 June 2014, 15:14
!

c***************************************************************
c   here we measure ffdual and action on whole lattice ;
c    - unsymmetrised ffdual for speed
c   we print sites where they are large .
c   vectorised ... except for pieces that are done only
c                  when ipr=2 i.e. usually only last cool.
c**************************************************************
        subroutine ffdact(pathBuffer, confBuffer)
        
        implicit real*8 (a-h,o-z)
        include 'parameters.inc'
c
        character(len=*) :: pathBuffer  
        character(len=*) :: confBuffer
        character(len=len_trim(pathBuffer)) :: trimmedPath
        character(len=len_trim(confBuffer)) :: trimmedConf
               
        common/next/iup(lsize,4),idn(lsize,4)
        common/arrays/u11(ncol2,lsize,4)
        dimension pl11(ncol2,6),dum11(ncol2)
     &  ,a11(ncol2),b11(ncol2),c11(ncol2),d11(ncol2)
        double complex u11,pl11,a11,b11,c11,d11,dum11,cff
        common/dummy/ffd(lsize),act(lsize)
        dimension ixm(4),ixn(4),ixp(4)
c
        do 1 nn=1,lsize
        m1=nn
c
        act(nn)=0.0
        ffd(nn)=0.0
c
      do 10 mu=1,3
        do 10 nu=mu+1,4
            iplaq=mu+nu-2+nu/4
c
            do 20 ij=1,ncol2
                a11(ij)=u11(ij,m1,mu)
20          continue
            
            m5=iup(m1,mu)
            
            do 21 ij=1,ncol2
                b11(ij)=u11(ij,m5,nu)
21          continue
      
            call vmx(1,a11,b11,c11,1)
            m5=iup(m1,nu)
      
            do 22 ij=1,ncol2
                a11(ij)=u11(ij,m5,mu)
22          continue
      
            call herm(1,a11,dum11,1)
            call vmx(1,c11,a11,b11,1)
            
            do 23 ij=1,ncol2
                a11(ij)=u11(ij,m1,nu)
23          continue

            call herm(1,a11,dum11,1)
            call vmx(1,b11,a11,c11,1)
      
            do 25 ij=1,ncol2
                pl11(ij,iplaq)=c11(ij)
      
c               write (6, *) pl11(ij, iplaq)
      
25          continue
c
10    continue
c
      do 30 ij=1,ncol2
        c11(ij)=pl11(ij,4)
        d11(ij)=pl11(ij,3)
30    continue
      
      do 31 ij=1,ncol2
        a11(ij)=c11(ij)
        b11(ij)=d11(ij)
31    continue

      call herm(1,c11,dum11,1)
      call herm(1,d11,dum11,1)

      do 32 ij=1,ncol2
        a11(ij)=a11(ij)-c11(ij)
        b11(ij)=b11(ij)-d11(ij)
32    continue
      
      call trvmx(1,a11,b11,cff,1)
      ffdd=real(cff)
      
      do 35 ij=1,ncol2
        c11(ij)=pl11(ij,5)
        d11(ij)=pl11(ij,2)
35    continue
      
      do 36 ij=1,ncol2
        a11(ij)=c11(ij)
        b11(ij)=d11(ij)
36    continue
      
      call herm(1,c11,dum11,1)
      call herm(1,d11,dum11,1)
      
      do 37 ij=1,ncol2
        a11(ij)=a11(ij)-c11(ij)
        b11(ij)=b11(ij)-d11(ij)
37    continue
      
      call trvmx(1,a11,b11,cff,1)
      ffdd=ffdd-real(cff)
      
      do 40 ij=1,ncol2
        c11(ij)=pl11(ij,6)
        d11(ij)=pl11(ij,1)
40    continue

      do 41 ij=1,ncol2
          a11(ij)=c11(ij)
          b11(ij)=d11(ij)
41    continue

      call herm(1,c11,dum11,1)
      call herm(1,d11,dum11,1)

      do 42 ij=1,ncol2
        a11(ij)=a11(ij)-c11(ij)
        b11(ij)=b11(ij)-d11(ij)
42    continue

      call trvmx(1,a11,b11,cff,1)
      ffdd=ffdd+real(cff)
c
      ffd(nn)=ffdd*2.0
c
      actx=0.0
      div3=1.0/ncol
c     Sum over 6 planes

      do 46 i=1,6
      do 46 n1=1,ncol
c       calculate trace      
        ij=n1+ncol*(n1-1)
        
        actx=actx+real(pl11(ij,i))
      
c       write (6, *) real(pl11(ij, i))
46    continue

      act(nn)=6.0-actx*div3
      
c     write (6, *) nn, act(nn)
      
      actx=0.0
c
1     continue
c
      acttot=0.0
      ffdtot=0.0
      ffdmod=0.0
c
      do 50 n=1,lsize
      ffdc=ffd(n)
      actt=act(n)
      acttot=acttot+actt
      ffdtot=ffdtot+ffdc
      ffdmod=ffdmod+abs(ffdc)
50    continue
c
      actmax=0.0
      ffdmax=0.0
      ffdmin=0.0
c
      do 52 n=1,lsize
      ffdc=ffd(n)
      actt=act(n)
      
c     Find mix and max values in the lattice
      if(ffdc.gt.ffdmax)then
      ffdmax=ffdc
      imax=n
      endif
      if(ffdc.lt.ffdmin)then
      ffdmin=ffdc
      imin=n
      endif
      if(actt.gt.actmax)then
      actmax=actt
      jmax=n
      endif
52    continue
c
        lx123=lx1*lx2*lx3
        jmax=jmax-1
        ixp(4)=jmax/lx123+1
        lx12=lx1*lx2
        jmax=jmax-(ixp(4)-1)*lx123
        ixp(3)=jmax/lx12+1
        jmax=jmax-(ixp(3)-1)*lx12
        ixp(2)=jmax/lx1+1
        jmax=jmax-(ixp(2)-1)*lx1
        ixp(1)=jmax+1
        imax=imax-1
        ixm(4)=imax/lx123+1
        imax=imax-(ixm(4)-1)*lx123
        ixm(3)=imax/lx12+1
        imax=imax-(ixm(3)-1)*lx12
        ixm(2)=imax/lx1+1
        imax=imax-(ixm(2)-1)*lx1
        ixm(1)=imax+1
        imin=imin-1
        ixn(4)=imin/lx123+1
        imin=imin-(ixn(4)-1)*lx123
        ixn(3)=imin/lx12+1
        imin=imin-(ixn(3)-1)*lx12
        ixn(2)=imin/lx1+1
        imin=imin-(ixn(2)-1)*lx1
        ixn(1)=imin+1

        !   output the cooling iteration statistics
        write(6,85)acttot,actmax,ffdtot,ffdmod,ffdmax,ffdmin
85      format(' tot/max(mod,min) s,q= ',6f12.2)

        !   output the position of maxima
        write(6,86)ixp,ixm,ixn
86      format('site smax qmax,min = ',4i3,i10,3i3,i10,3i3)
        
!       Remove trailing spaces
        trimmedPath = trim(pathBuffer)
        trimmedConf = trim(confBuffer)
        
!       write the maxima stats to a file
        open(unit=33,file=trimmedPath//"maxima"//
     1  '_conp'//trimmedConf//".txt",
     1  position="append")
        write(33, 902)ixp,'',ixm,'',ixn
        close(33)

        !   write the action density stats to a file
        open(unit=31,file=trimmedPath//"action"//
     1  '_conp'//trimmedConf//".txt",
     1  position="append")
        write(31, 900)acttot,actmax
        close(31)

        !   write the ffdual stats to a file
        open(unit=32,file=trimmedPath//"ffdual"//
     1  '_conp'//trimmedConf//".txt",
     1  position="append")
        write(32, 903)ffdtot,'',ffdmod,'',ffdmax,'',ffdmin
        close(32)
        
        !   Format string for action.txt file
900     format('',6f12.6)
        !   Format string for maxima.txt file
902     format(4i3,a6,4i3,a6,4i3)

903     format(f12.2,a6,f12.2,a6,f12.2,a6,f12.2)
c
        return
        end
