!     
! File:   herm.f
! Author: Dean - moved from main code
!
! Created on 03 June 2014, 15:12
!

c**********************************************************
c  hermitian conjugate
c**********************************************************
      subroutine herm(nnn1,a11,dum11,nnn2)
      implicit real*8 (a-h,o-z)
      include 'parameters.inc'
c
      double complex a11(ncol,ncol),dum11(ncol,ncol)
c
      do 1  i=1,ncol
      do 1  j=1,ncol
      dum11(i,j)=conjg(a11(j,i))
2     continue
1     continue
      do 4  i=1,ncol
      do 4  j=1,ncol
      a11(i,j)=dum11(i,j)
6     continue
4     continue
c
      return
      end
