!     
! File:   heatb.f
! Author: Dean - moved from main source code
!
! Created on 26 June 2014, 14:36
!
!*****************************************************
! su2 subgroup heatbath
!*****************************************************
      subroutine heatb
      implicit real*8 (a-h,o-z)
      include 'parameters.inc'
c
      common/hb/betag,ibias,umag,a0,a1,a2,a3
      real*8 rndnum
c
      pi=4.0*datan(1.0d0)
      btg=betag*2.0d0/ncol
      temp=1.d0/btg
      dd=rndnum()
c
      umag=umag*temp
      a1=-dlog(rndnum())*umag
      a2=-dlog(rndnum())*umag
      a3=dcos(2.0d0*pi*rndnum())
      a3=a3*a3
      a1=a1*a3
      a2=a2+a1
      a0=1.0d0-a2
      a3=rndnum()
      a3=a3*a3-1+a2*0.5d0
      if(a3.gt.0.0d0)then
45    x11=-dlog(rndnum())*umag
      x22=-dlog(rndnum())*umag
      ccc=dcos(2.0d0*pi*rndnum())
      ccc=ccc*ccc
      aa=x11*ccc
      dbb=x22+aa
      xx=rndnum()
      xx=xx*xx-1.0d0+dbb*0.5d0
      if(xx.gt.0.0d0)goto45
      a0=1.0d0-dbb
      endif
c
54    continue
      x1=2.0*rndnum()-1.0d0
      x2=2.0*rndnum()-1.0d0
      x3=2.0*rndnum()-1.0d0
      cc=x1*x1+x2*x2+x3*x3-1.0d0
      if(cc.le.0.0)then
      a1=x1
      a2=x2
      a3=x3
      else
      goto54
      endif
      rad=(1.d0-a0*a0)
      rad=(a1*a1+a2*a2+a3*a3)/rad
      rad=1.d0/dsqrt(rad)
      a1=a1*rad
      a2=a2*rad
      a3=a3*rad
c
      return
      end

