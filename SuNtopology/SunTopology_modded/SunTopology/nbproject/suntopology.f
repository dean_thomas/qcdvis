c***************************************************************
c 20-12-01
c d=4 su(ncol) code with  q,  j=0,2   p,c=+,+  glueballs 
c n-ality = 1,2,3 flux loops
c
c*****
c
c blocking uses products of 2 smeared links :
c itype different parameter sets
c 
c warning: poly vacs are real -- no good for finite t!
c
c***************************************************************
        implicit real*8 (a-h,o-z)
        include 'parameters.f'
c
        common/arrays/u11(ncol2*lsize*4)
        common/anisotropy/srat,trat
        real*8 rndnum
        complex u11
c
        write (*,*) 'Hello!'
        srat=1.0
        trat=1.0
        call check_parameters()
        iseed=19752
        call rini(iseed)
        call setup
c
        betag=2.5d0
c
        write(6,90)
90      format(' ****************************************************')
        write(6,91)
91      format(' *')
        write(6,92) lx1,lx2,lx3,lx4,betag,ncol
92      format('  lx = ',4i6,'    beta =',f8.4,'   ncolor = ',i4)
        write(6,91)
        write(6,91)
        write(6,90)
c
c        goto999
c
        iconf = ifirstc
        do iter=1,niter
           call read_conf(iconf)   
           call action(0,iter)
           call qcool
           if (iter.eq.itermax) goto 111
           iconf = iconf + isave
        enddo
c
 111    continue
c
c        call action(1,0)
c
        stop
        end

c*************************************************************
c     checks the compatibility between Simon's parameters and mine
      subroutine check_parameters()
      include 'parameters.f'
      
c     in Simon's code, the lattice need to be spatially symmetric
      if ((lx1.ne.lx2).or.(lx1.ne.lx3)) then
         print *, "The lattice is not spatially symmetric"
         stop 111
      endif

c     the spatial size must be equal
      if (lx1.ne.ksize) then
         print *, "Spatial lattice size mismatch"
         stop 112
      endif

c     check of the temporal size
      if (lx4.ne.(ksizet*numtasks)) then
         print *, "Temporal lattice size mismatch"
         stop 113
      endif

c     OK, let's go
      print *, "Check of parameters passed"

      return
      end

c***************************************************************
        subroutine read_conf(isweep)
        implicit real*8 (a-h,o-z)
        include 'parameters.f'

        common/arrays/u11(ncol2,lsize,4)
        dimension v11(kvol,4),v12(kvol,4)
        complex u11,v11,v12
        character*4 c

        write(c,'(i4.4)') isweep
        open(unit=31,file='conz'//c,
     1       status='old',form='unformatted')
        do itasks=1, numtasks
           read (31) v11,v12
           do idir=1, 4
              ipoint = (itasks - 1)*kvol
              do kpoint = 1, kvol
                 ipoint = ipoint + 1
                 u11(1,ipoint,idir) = v11(kpoint,idir)
                 u11(2,ipoint,idir) = -conjg(v12(kpoint,idir))
                 u11(3,ipoint,idir) = v12(kpoint,idir)
                 u11(4,ipoint,idir) = conjg(v11(kpoint,idir))
              if (ipoint .lt. 3) then
                    write(6, *)ipoint, idir, u11(1,ipoint,idir)
                    write(6, *)ipoint, idir, u11(2,ipoint,idir)
                    write(6, *)ipoint, idir, u11(3,ipoint,idir)
                    write(6, *)ipoint, idir, u11(4,ipoint,idir)
              endif
              enddo
           enddo
        enddo

        return
        end

c***************************************************************
        subroutine qcool
        implicit real*8 (a-h,o-z)
        include 'parameters.f'

        common/anisotropy/srat,trat
        common/arrays/u11(ncol2*lsize*4)
        common/hb/betag,ibias
        dimension v11(ncol2*lsize*4)
        complex u11,v11
c
        do 10 ns=1,ncol2*lsize*4
        v11(ns)=u11(ns)
10      continue
c
        ibias=3
        do 20 ic=1,ncool
        call updatg(ibias,actl,betag,srat,trat)
c        call action(0,1)        
        call ffdact
        if(ic.eq.ncool) call search
20      continue
c
        do 11 ns=1,ncol2*lsize*4
        u11(ns)=v11(ns)
11      continue
c
        return
        end
c*********************************************************************
c ok
c*********************************************************************
      subroutine action(ipr,iter)
      implicit real*8 (a-h,o-z)
      include 'parameters.f'
c
      common/astore/actn(numbin,6)
      common/arrays/u11(ncol2,lsize,4)
      common/next/iup(lsize,4),idn(lsize,4)
      dimension dum11(ncol2)
     &,a11(ncol2),b11(ncol2),c11(ncol2),d11(ncol2)
      complex u11,a11,b11,c11,d11,dum11,act
c
      dimension avac(numbin),avacsq(numbin),avacs(numbin),avact(numbin)
      dimension val(numbin),av(numbin)
c
      ibin=niter/numbin
c      write (*,*) 'niter', niter, numbin, ibin
c
      if(ipr.eq.0)then
      jbin=(iter-1)/ibin+1
c
      if(iter.eq.1)then
      do 3 nb=1,numbin
      avac(nb)=0.0d0
      avacsq(nb)=0.0d0
      avacs(nb)=0.0d0
      avact(nb)=0.0d0
3     continue
      endif
c
      vacts=0.0
      vactt=0.0
c
      do 10 nn=1,lsize
      do 10 mu=1,3
      m1=nn
c
      do 6  ij=1,ncol2
      a11(ij)=u11(ij,m1,mu)
6     continue
      m2=iup(m1,mu)
c
      do 11 nu=mu+1,4
      iplaq=6-nu-mu+5*(nu/4)
c
      do 7  ij=1,ncol2
      b11(ij)=u11(ij,m2,nu)
7     continue
      call vmx(1,a11,b11,c11,1)
      m3=iup(m1,nu)
      do 8  ij=1,ncol2
      d11(ij)=u11(ij,m3,mu)
8     continue
      call herm(1,d11,dum11,1)
      call vmx(1,c11,d11,b11,1)
      do 9  ij=1,ncol2
      d11(ij)=u11(ij,m1,nu)
9     continue
      call herm(1,d11,dum11,1)
      call trvmx(1,b11,d11,act,1)
      ann=1.0/ncol
      act=ann*real(act)
      actn(jbin,iplaq)=actn(jbin,iplaq)+act
      if(nu.ne.4)then
      vacts=vacts+act
      else
      vactt=vactt+act
      endif
c
11    continue
10    continue
c
      vacts=vacts/(3.0*lsize)
      vactt=vactt/(3.0*lsize)
      write (91,*) vacts, vactt, 0.5*(vacts + vactt)
c
      avacs(jbin)=avacs(jbin)+vacts
      avact(jbin)=avact(jbin)+vactt
c
      if(iter.eq.niter)then
      do 17 ip=1,6
      do 17 jb=1,numbin
      actn(jb,ip)=actn(jb,ip)/(ibin*lsize)
17    continue
      endif
c
      endif
c
      if(ipr.eq.1)then
c
      write(6,80)
80    format(' *****************************************************')
      write(6,100)
100   format('                 action            ')
      write(6,80)
      write(6,81)
81    format(' *  ')
      do 20 nb=1,numbin
      av(nb)=avacs(nb)/ibin
20    continue
      call jackm(numbin,av,avrs,ers)
      do 22 nb=1,numbin
      av(nb)=avact(nb)/ibin
22    continue
      call jackm(numbin,av,avrt,ert)
      write(6,110) avrs,ers
110   format('   aver,err   space action =',2f12.8)
      write(6,112) avrt,ert
112   format('   aver,err   time  action =',2f12.8)
      write(6,81)
c
      endif
c
      return
      end
c**********************************************************
c  ok
c  this routine reimposes the unitarity constraints on
c  our su(3) matrices
c**********************************************************
        subroutine renorm
        implicit real*8 (a-h,o-z)
        include 'parameters.f'
c
        common/arrays/u11(ncol,ncol,lsize,4)
        complex adum(ncol,ncol),u11,csum
c
        do 100 mu=1,4
        do 100 nn=1,lsize
c
        do 1 j=1,ncol
        do 1 i=1,ncol
        adum(i,j)=u11(i,j,nn,mu)
1       continue
c
        do 20 n2=1,ncol
c
        do 10 n3=1,n2-1
c
        csum=(0.0,0.0)
        do 5 n1=1,ncol
        csum=csum+adum(n1,n2)*conjg(adum(n1,n3))
5       continue
        do 6 n1=1,ncol
        adum(n1,n2)=adum(n1,n2)-csum*adum(n1,n3)
6       continue
c
10      continue
c
        sum=0.0
        do 7 n1=1,ncol
        sum=sum+adum(n1,n2)*conjg(adum(n1,n2))
7       continue
        anorm=1.0/sqrt(sum)
        do 8 n1=1,ncol
        adum(n1,n2)=adum(n1,n2)*anorm
8       continue
c
20      continue
c
        do 2 j=1,ncol
        do 2 i=1,ncol
        u11(i,j,nn,mu)=adum(i,j)
2       continue
c
100     continue
        return
        end
c***********************************************************
c ok
c  here we set up  link pointers for full lattice
c***********************************************************
      subroutine setup
      implicit real*8 (a-h,o-z)
      include 'parameters.f'
c
      common/next/iup(lsize,4),idn(lsize,4)
c
      lx12=lx1*lx2
      lx123=lx12*lx3
      lx1234=lx123*lx4
c
      nn=0
      do 2 l4=1,lx4
      do 2 l3=1,lx3
      do 2 l2=1,lx2
      do 2 l1=1,lx1
      nn=nn+1
c
      nu1=nn+1
      if((l1+1).gt.lx1) nu1=nu1-lx1
      iup(nn,1)=nu1
      nd1=nn-1
      if((l1-1).lt.1) nd1=nd1+lx1
      idn(nn,1)=nd1
c
      nu2=nn+lx1
      if((l2+1).gt.lx2) nu2=nu2-lx12
      iup(nn,2)=nu2
      nd2=nn-lx1
      if((l2-1).lt.1) nd2=nd2+lx12
      idn(nn,2)=nd2
c
      nu3=nn+lx12
      if((l3+1).gt.lx3) nu3=nu3-lx123
      iup(nn,3)=nu3
      nd3=nn-lx12
      if((l3-1).lt.1) nd3=nd3+lx123
      idn(nn,3)=nd3
c
      nu4=nn+lx123
      if((l4+1).gt.lx4) nu4=nu4-lx1234
      iup(nn,4)=nu4
      nd4=nn-lx123
      if((l4-1).lt.1) nd4=nd4+lx1234
      idn(nn,4)=nd4
c
2     continue
c
      return
      end
c***********************************************************
c ok
c  here we update the links
c***********************************************************
      subroutine updatg(ibias,actl,betag,srat,trat)
      implicit real*8 (a-h,o-z)
      include 'parameters.f'
c
      common/arrays/u11(ncol2,lsize,4)
      common/next/iup(lsize,4),idn(lsize,4)
      common/hb/bbetag,iibias
      dimension uint11(ncol2),dum11(ncol2)
     &,a11(ncol2),b11(ncol2),c11(ncol2)
c
      complex u11,a11,b11,c11,uint11,dum11
c
      bbetag=betag
      iibias=ibias
      sum=0.
c     all coordinates start from 1
c     point = (ix,iy,iz,it)
c     lsize = ix + iy*(lx1 - 1) + iz*(lx1 - 1)*(lx2 - 1) +
c     it*(lx1 - 1)*(lx2 - 1)*(lx3 - 1)

      do 1 nn=1,lsize
      m1=nn
c
      do 10 mu=1,4
c
      do 11 ij=1,ncol2
      uint11(ij)=(0.,0.)
11    continue
      m5=iup(m1,mu)
c
      do 20 nu=1,4
      if(mu.eq.nu) go to 20
      if(mu.eq.4.or.nu.eq.4)then
      crat=trat
      else
      crat=srat
      endif
c
      do 15 ij=1,ncol2
      a11(ij)=u11(ij,m1,nu)
15    continue
      m3=iup(m1,nu)
      do 17 ij=1,ncol2
      b11(ij)=u11(ij,m3,mu)
17    continue
      call vmx(1,a11,b11,c11,1)
      do 19 ij=1,ncol2
      a11(ij)=u11(ij,m5,nu)
19    continue
      call herm(1,a11,dum11,1)
      call vmx(1,c11,a11,b11,1)
      do 23 ij=1,ncol2
      uint11(ij)=uint11(ij)+b11(ij)*crat
23    continue
      m3=idn(m1,nu)
      do 25 ij=1,ncol2
      b11(ij)=u11(ij,m3,mu)
25    continue
      do 27 ij=1,ncol2
      a11(ij)=u11(ij,m3,nu)
27    continue
      call herm(1,a11,dum11,1)
      call vmx(1,a11,b11,c11,1)
      m3=idn(m5,nu)
      do 31 ij=1,ncol2
      a11(ij)=u11(ij,m3,nu)
31    continue
      call vmx(1,c11,a11,b11,1)
      do 32 ij=1,ncol2
      uint11(ij)=uint11(ij)+b11(ij)*crat
32    continue
20    continue
c
      call herm(1,uint11,dum11,1)
c
      do 36 ij=1,ncol2
      c11(ij)=u11(ij,m1,mu)
36    continue
      call vmx(1,uint11,c11,b11,1)
c
      do icdist=1, ncol-1
         do i1col=1, ncol - icdist 
            i2col = i1col + icdist
            call subgrh(i1col, i2col, b11, c11)
         enddo
      enddo
c
      do 40 ij=1,ncol2
      u11(ij,m1,mu)=c11(ij)
40    continue
      do 42 i=1,ncol
      nc=i+ncol*(i-1)
      sum=sum+real(b11(nc))
42    continue
c
10    continue
1     continue
c
      apq=1.-sum/(24.*ncol*lsize)
      actl=apq
c
      return
      end
c*****************************************************
      subroutine subgrh(i1col,i2col,b11,c11)
      implicit real*8 (a-h,o-z)
      include 'parameters.f'
c
      common/hb/betag,ibias,umag,a0,a1,a2,a3
c
      complex b11(ncol2),c11(ncol2)
      complex f11,f12,a11(2,2)
      complex e11,e12,g11,g12
c
      i1 = i1col + (i1col - 1)*ncol
      i2 = i2col + (i2col - 1)*ncol
      i3 = i2col + (i1col - 1)*ncol
      i4 = i1col + (i2col - 1)*ncol
c      write (*,*) i1, i2, i3, i4 
c
      f11=(b11(i1)+conjg(b11(i2)))*0.5
      f12=(b11(i3)-conjg(b11(i4)))*0.5
      umag=sqrt(f11*conjg(f11)+f12*conjg(f12))
      umag=1./umag
      f11=f11*umag
      f12=f12*umag
      if(ibias.eq.1)then
      call heatb
      e11=cmplx(a0,a1)
      e12=cmplx(a2,a3)
      g11=conjg(f11)*e11+conjg(f12)*e12
      g12=-f12*e11+f11*e12
      a11(1,1)=g11
      a11(1,2)=-conjg(g12)
      a11(2,1)=g12
      a11(2,2)=conjg(g11)
      endif
      if(ibias.eq.2)then
      g11= f11*f11-conjg(f12)*f12
      g12= f12*f11+conjg(f11)*f12
      a11(1,1)=conjg(g11)
      a11(1,2)=conjg(g12)
      a11(2,1)=-g12
      a11(2,2)=g11
      endif
      if(ibias.eq.3)then
      a11(1,1)=conjg(f11)
      a11(1,2)=conjg(f12)
      a11(2,1)=-f12
      a11(2,2)=f11
      endif
      call vmxsu2(i1col,i2col,c11,a11)
      call vmxsu2(i1col,i2col,b11,a11)
c
      return
      end
c*****************************************************
c su2 subgroup heatbath
c*****************************************************
      subroutine heatb
      implicit real*8 (a-h,o-z)
      include 'parameters.f'
c
      common/hb/betag,ibias,umag,a0,a1,a2,a3
      real*8 rndnum
c
      pi=4.0*datan(1.0d0)
      btg=betag*2.0d0/ncol
      temp=1.d0/btg
      dd=rndnum()
c
      umag=umag*temp
      a1=-dlog(rndnum())*umag
      a2=-dlog(rndnum())*umag
      a3=dcos(2.0d0*pi*rndnum())
      a3=a3*a3
      a1=a1*a3
      a2=a2+a1
      a0=1.0d0-a2
      a3=rndnum()
      a3=a3*a3-1+a2*0.5d0
      if(a3.gt.0.0d0)then
45    x11=-dlog(rndnum())*umag
      x22=-dlog(rndnum())*umag
      ccc=dcos(2.0d0*pi*rndnum())
      ccc=ccc*ccc
      aa=x11*ccc
      dbb=x22+aa
      xx=rndnum()
      xx=xx*xx-1.0d0+dbb*0.5d0
      if(xx.gt.0.0d0)goto45
      a0=1.0d0-dbb
      endif
c
54    continue
      x1=2.0*rndnum()-1.0d0
      x2=2.0*rndnum()-1.0d0
      x3=2.0*rndnum()-1.0d0
      cc=x1*x1+x2*x2+x3*x3-1.0d0
      if(cc.le.0.0)then
      a1=x1
      a2=x2
      a3=x3
      else
      goto54
      endif
      rad=(1.d0-a0*a0)
      rad=(a1*a1+a2*a2+a3*a3)/rad
      rad=1.d0/dsqrt(rad)
      a1=a1*rad
      a2=a2*rad
      a3=a3*rad
c
      return
      end
c***************************************************************
c   here we measure ffdual and action on whole lattice ;
c    - unsymmetrised ffdual for speed
c   we print sites where they are large .
c   vectorised ... except for pieces that are done only
c                  when ipr=2 i.e. usually only last cool.
c**************************************************************
        subroutine ffdact
        implicit real*8 (a-h,o-z)
        include 'parameters.f'
c
        common/next/iup(lsize,4),idn(lsize,4)
        common/arrays/u11(ncol2,lsize,4)
        dimension pl11(ncol2,6),dum11(ncol2)
     &  ,a11(ncol2),b11(ncol2),c11(ncol2),d11(ncol2)
        complex u11,pl11,a11,b11,c11,d11,dum11,cff
        common/dummy/ffd(lsize),act(lsize)
        dimension ixm(4),ixn(4),ixp(4)
c
        do 1 nn=1,lsize
        m1=nn
c
        act(nn)=0.0
        ffd(nn)=0.0
c
      do 10 mu=1,3
      do 10 nu=mu+1,4
      iplaq=mu+nu-2+nu/4
c
      do 20 ij=1,ncol2
      a11(ij)=u11(ij,m1,mu)
20    continue
      m5=iup(m1,mu)
      do 21 ij=1,ncol2
      b11(ij)=u11(ij,m5,nu)
21    continue
      call vmx(1,a11,b11,c11,1)
      m5=iup(m1,nu)
      do 22 ij=1,ncol2
      a11(ij)=u11(ij,m5,mu)
22    continue
      call herm(1,a11,dum11,1)
      call vmx(1,c11,a11,b11,1)
      do 23 ij=1,ncol2
      a11(ij)=u11(ij,m1,nu)
23    continue
      call herm(1,a11,dum11,1)
      call vmx(1,b11,a11,c11,1)
      do 25 ij=1,ncol2
      pl11(ij,iplaq)=c11(ij)
25    continue
c
10    continue
c
      do 30 ij=1,ncol2
      c11(ij)=pl11(ij,4)
      d11(ij)=pl11(ij,3)
30    continue
      do 31 ij=1,ncol2
      a11(ij)=c11(ij)
      b11(ij)=d11(ij)
31    continue
      call herm(1,c11,dum11,1)
      call herm(1,d11,dum11,1)
      do 32 ij=1,ncol2
      a11(ij)=a11(ij)-c11(ij)
      b11(ij)=b11(ij)-d11(ij)
32    continue
      call trvmx(1,a11,b11,cff,1)
      ffdd=real(cff)
      do 35 ij=1,ncol2
      c11(ij)=pl11(ij,5)
      d11(ij)=pl11(ij,2)
35    continue
      do 36 ij=1,ncol2
      a11(ij)=c11(ij)
      b11(ij)=d11(ij)
36    continue
      call herm(1,c11,dum11,1)
      call herm(1,d11,dum11,1)
      do 37 ij=1,ncol2
      a11(ij)=a11(ij)-c11(ij)
      b11(ij)=b11(ij)-d11(ij)
37    continue
      call trvmx(1,a11,b11,cff,1)
      ffdd=ffdd-real(cff)
      do 40 ij=1,ncol2
      c11(ij)=pl11(ij,6)
      d11(ij)=pl11(ij,1)
40    continue
      do 41 ij=1,ncol2
      a11(ij)=c11(ij)
      b11(ij)=d11(ij)
41    continue
      call herm(1,c11,dum11,1)
      call herm(1,d11,dum11,1)
      do 42 ij=1,ncol2
      a11(ij)=a11(ij)-c11(ij)
      b11(ij)=b11(ij)-d11(ij)
42    continue
      call trvmx(1,a11,b11,cff,1)
      ffdd=ffdd+real(cff)
c
      ffd(nn)=ffdd*2.0
c
      actx=0.0
      div3=1.0/ncol
      do 46 i=1,6
      do 46 n1=1,ncol
      ij=n1+ncol*(n1-1)
      actx=actx+real(pl11(ij,i))
46    continue
      act(nn)=6.0-actx*div3
      actx=0.0
c
1     continue
c
      acttot=0.0
      ffdtot=0.0
      ffdmod=0.0
c
      do 50 n=1,lsize
      ffdc=ffd(n)
      actt=act(n)
      acttot=acttot+actt
      ffdtot=ffdtot+ffdc
      ffdmod=ffdmod+abs(ffdc)
50    continue
c
      actmax=0.0
      ffdmax=0.0
      ffdmin=0.0
c
      do 52 n=1,lsize
      ffdc=ffd(n)
      actt=act(n)
      if(ffdc.gt.ffdmax)then
      ffdmax=ffdc
      imax=n
      endif
      if(ffdc.lt.ffdmin)then
      ffdmin=ffdc
      imin=n
      endif
      if(actt.gt.actmax)then
      actmax=actt
      jmax=n
      endif
52    continue
c
        lx123=lx1*lx2*lx3
        jmax=jmax-1
        ixp(4)=jmax/lx123+1
        lx12=lx1*lx2
        jmax=jmax-(ixp(4)-1)*lx123
        ixp(3)=jmax/lx12+1
        jmax=jmax-(ixp(3)-1)*lx12
        ixp(2)=jmax/lx1+1
        jmax=jmax-(ixp(2)-1)*lx1
        ixp(1)=jmax+1
        imax=imax-1
        ixm(4)=imax/lx123+1
        imax=imax-(ixm(4)-1)*lx123
        ixm(3)=imax/lx12+1
        imax=imax-(ixm(3)-1)*lx12
        ixm(2)=imax/lx1+1
        imax=imax-(ixm(2)-1)*lx1
        ixm(1)=imax+1
        imin=imin-1
        ixn(4)=imin/lx123+1
        imin=imin-(ixn(4)-1)*lx123
        ixn(3)=imin/lx12+1
        imin=imin-(ixn(3)-1)*lx12
        ixn(2)=imin/lx1+1
        imin=imin-(ixn(2)-1)*lx1
        ixn(1)=imin+1
c
        write(6,85)acttot,actmax,ffdtot,ffdmod,ffdmax,ffdmin
85      format(' tot/max(mod,min) s,q= ',6f12.2)
        write(6,86)ixp,ixm,ixn
86      format('site smax qmax,min = ',4i3,i10,3i3,i10,3i3)
c
        return
        end
c***************************************************************
c   here we search for points where ffd(n,lt) is a maximum
c   or minimum
c**************************************************************
        subroutine search
        implicit real*8 (a-h,o-z)
        include 'parameters.f'
c
        common/dummy/ffd(lsize)
        common/next/iup(lsize,4),idn(lsize,4)
        dimension delu(4),deld(4)
     &  ,ixx(max),iyy(max),izz(max),itt(max)
     &  ,ix1(maxp),iy1(maxp),iz1(maxp),it1(maxp)
        dimension q(max),eps(max),in(max),it(max)
        dimension q1(maxp),eps1(maxp)
c
      nnum=0
      do 4 n=1,max
      q(n)=0.0
4     continue
      do 6 n=1,maxp
      q1(n)=0.0
6     continue
c
      do 10 nn=1,lsize
      m1=nn
      if(nnum.eq.max)goto10
c
      fff=ffd(m1)
      do 11 mu=1,4
      m2=iup(m1,mu)
      m3=idn(m1,mu)
      ffdu=ffd(m2)
      ffdd=ffd(m3)
      delu(mu)=abs(fff)-abs(ffdu)
      deld(mu)=abs(fff)-abs(ffdd)
11    continue
c
      do 15 mu=1,4
      if(delu(mu).lt.0.or.deld(mu).lt.0)goto10
15    continue
      if(abs(fff).lt.qcut)goto10
      nnum=nnum+1
      q(nnum)=fff
      eps(nnum)=delu(1)+deld(1)+delu(2)+deld(2)
     &+delu(3)+deld(3)+delu(4)+deld(4)
      in(nnum)=nn
c
10    continue
c
        lx123=lx1*lx2*lx3
        lx12=lx1*lx2
        do 30 nn=1,max
        if(q(nn).eq.0)goto30
        jmax=in(nn)-1
        is4=jmax/(lx123)+1
        itt(nn)=is4
        jmax=jmax-(is4-1)*lx123
        is3=jmax/(lx12)+1
        izz(nn)=is3
        jmax=jmax-(is3-1)*lx12
        is2=jmax/lx1+1
        iyy(nn)=is2
        jmax=jmax-(is2-1)*lx1
        is1=jmax+1
        ixx(nn)=is1
30      continue
c
        num=0
        do 32 nn=1,nnum
        if(num.eq.maxp)goto32
        do 33 mm=1,nnum
        if(mm.eq.nn)goto33
        if(abs(q(mm)).lt.abs(q(nn))) goto33
        delx=abs(ixx(nn)-ixx(mm))
        if(delx.gt.lx1/2) delx=lx1-delx
        dely=abs(iyy(nn)-iyy(mm))
        if(dely.gt.lx2/2) dely=lx2-dely
        delz=abs(izz(nn)-izz(mm))
        if(delz.gt.lx3/2) delz=lx3-delz
        delt=abs(itt(nn)-itt(mm))
        if(delt.gt.lx4/2) delt=lx4-delt
        iss=delx*delx+dely*dely+delz*delz+delt*delt
        if(iss.gt.rrcut)goto33
        goto32
33      continue
        num=num+1
        q1(num)=q(nn)
        ix1(num)=ixx(nn)
        iy1(num)=iyy(nn)
        iz1(num)=izz(nn)
        it1(num)=itt(nn)
        eps1(num)=eps(nn)
32      continue
c
        write(6,93) qcut,rrcut
93      format(' global maxima of abs(ffdual); q,r.r cuts =',f10.4,f8.2)
        write(6,94)
94      format(' ffdual  x   y   z   t   av diff ')
        do 35 nn=1,maxp
        if(q1(nn).eq.0)goto35
        qq=q1(nn)
        epss=eps1(nn)*0.125
        is4=it1(nn)
        is3=iz1(nn)
        is2=iy1(nn)
        is1=ix1(nn)
        write(6,95) qq,is1,is2,is3,is4,epss
95      format(f10.4,4i5,f10.5)
35      continue
c
        return
        end
c************************************************************
c  jack-knife errors for av(i=1,..,num)
c***********************************************************
      subroutine jackm(num,av,aver,err)
      implicit real*8 (a-h,o-z)
c
      dimension av(*)
c
      sum=0.0d0
      do 10 n=1,num
      sum=sum+av(n)
10    continue
      sum=sum/num
c
      esum=0.0d0
      do 12 n=1,num
      dif=sum-av(n)
      esum=esum+dif*dif
12    continue
      esum=esum/num
c
      aver=sum
      err=sqrt(esum*num)/(num-1)
c
      return
      end
c************************************************************
c  jack-knife errors for ratio avu to avd
c***********************************************************
      subroutine jackmr(num,nmax,avu,avd,aver,err)
      implicit real*8 (a-h,o-z)
c
      dimension avu(nmax),avd(nmax)
      dimension difu(nmax),difd(nmax)
c
      aver=0.0d0
      err=0.0d0
c
      sumu=0.0d0
      sumd=0.0d0
      do 10 n=1,num
      sumu=sumu+avu(n)
      sumd=sumd+avd(n)
10    continue
      do 12 n=1,num
      difu(n)=sumu-avu(n)
      difd(n)=sumd-avd(n)
12    continue
      do 14 n=1,num
      if(difd(n).eq.0.0d0)then
      err=99.0d0
      goto99
      endif
14    continue
c
      asum=0.0d0
      esum=0.0d0
      do 16 n=1,num
      dif=difu(n)/difd(n)
      asum=asum+dif
      esum=esum+dif*dif
16    continue
      asum=asum/num
      esum=esum/num
      esum=esum-asum*asum
c
      if(sumd.ne.0.0) aver=sumu/sumd
      if(esum.gt.0.0) err=sqrt(esum*num)
c
99    continue
      return
      end
c**********************************************************
c  vector matrix multiply ... 5*5 complex
c**********************************************************
        subroutine vmx(nnn1,a,b,c,nnn2)
        implicit real*8 (a-h,o-z)
        include 'parameters.f'
c
        complex a(ncol,ncol),b(ncol,ncol),c(ncol,ncol),csum
c
        do 1 j=1,ncol
        do 1 i=1,ncol
        csum=(0.0,0.0)
        do 2 k=1,ncol
        csum=csum+a(i,k)*b(k,j)
2       continue
        c(i,j)=csum
1       continue
c
        return
        end
c**********************************************************
c  Semplification of vmx when b is an SU(2) matrix
c**********************************************************
        subroutine vmxsu2(i1,i2,a,b)
        implicit real*8 (a-h,o-z)
c
        include 'parameters.f'

        complex a(ncol,ncol),b(2,2),c(ncol,2)!,csum
        
c
        do i=1,ncol
           c(i,1) = a(i,i1)*b(1,1) + a(i,i2)*b(2,1)
           c(i,2) = a(i,i1)*b(1,2) + a(i,i2)*b(2,2)
        enddo
c
        do i=1,ncol
           a(i,i1) = c(i,1)
           a(i,i2) = c(i,2)
        enddo
c   
        return
        end
c**********************************************************
c  trace product .. 5*5 complex
c**********************************************************
        subroutine trvmx(nnn1,a,b,cc,nnn2)
        implicit real*8 (a-h,o-z)
        include 'parameters.f'
c
        complex a(ncol,ncol),b(ncol,ncol),cc
c
        cc=(0.0,0.0)
        do 1 i=1,ncol
        do 1 k=1,ncol
        cc=cc+a(i,k)*b(k,i)
1       continue
c
        return
        end
c**********************************************************
c  hermitian conjugate
c**********************************************************
      subroutine herm(nnn1,a11,dum11,nnn2)
      implicit real*8 (a-h,o-z)
      include 'parameters.f'
c
      complex a11(ncol,ncol),dum11(ncol,ncol)
c
      do 1  i=1,ncol
      do 1  j=1,ncol
      dum11(i,j)=conjg(a11(j,i))
2     continue
1     continue
      do 4  i=1,ncol
      do 4  j=1,ncol
      a11(i,j)=dum11(i,j)
6     continue
4     continue
c
      return
      end

C If the variable XX is supposed to get a random value, just say 

C XX=rndnum() .

C For the initialisation, in the main program just after reading in the
C old configs ( the REWIND and READ for units 40,50) a statement
C  CALL rini(iseed)
C is needed.

*  file random.f, Ulli Wolff, 15.9.95
*
*  random generator modified RCARRY, 
*  see M. Luescher, Comp. Phys. Comm. 79(1994)100
*  and literature quoted there
*
*  This generator is relatively slow, but of KNOWN good quality
*  The speed strongly depends on the value ithrow
*  One way to speed up would be vectoriztion over parallel copies
*
*  ALWAYS CHANGE PARAMETERS IN BOTH ROUTINES OF THIS FILE
*
*  values for ithrow: 0 for simple uses, 24 for normal, 199 for
*  practically no detectable correlations, 365 for tests in delicated cases;
*  the bigger ithrow, the slower the generator!
*
*  0 <= rndnum < 1  (rndnum=0 DOES occur occasionally)
*  
**************************************************************************

      real*8 function rndnum()
*
      integer ir,is,irs,ir1,ikeep,ithrow,ivl1,ivl,ibase,iv,icall
      real basein
      parameter (ir=24,is=10,irs=ir-is,ir1=ir-1)
      parameter (ikeep=ir,ithrow=24,ivl1=ikeep+ithrow,ivl=ivl1+ir1)
      parameter (ibase=2**24,basein=1.0/ibase)
*
*  state of the generator (to be saved and reloaded for continuation):
      common/rrand/ iv(0:ivl),icall
      save /rrand/
*
      integer i,j,ivn
*
      if (icall.eq.ikeep) then
*  disregard ithrow numbers:
        do j=1,ithrow
          ivn=iv(irs+icall)-iv(icall)
          icall=icall+1
*  carry bit:
          if (ivn.lt.0) then
            iv(icall)=iv(icall)+1
            ivn=ivn+ibase
          endif
*
          iv(icall+ir1)=ivn
        enddo
*  copy last ir numbers to the beginning:
        do i=0,ir1
          iv(i)=iv(ivl1+i)
        enddo
      icall=0
      endif
*
*  basic cycle:
*
      ivn=iv(irs+icall)-iv(icall)
      icall=icall+1
*  carry bit:
      if (ivn.lt.0) then
        iv(icall)=iv(icall)+1
        ivn=ivn+ibase
      endif
*
      iv(icall+ir1)=ivn
*  convert to floating point:
      rndnum=float(ivn)*basein
      if (rndnum.eq.0.0) rndnum = 4.5d-9
      end


C**************************************************************************

c     This gets num random numbers at one time, to save on overheads.
c     There is the pesky IF (icall .EQ. ikeep) line here.

c     SInce ikeep is only 24, this statement is unavoidable.

C**************************************************************************

      SUBROUTINE MULTI_RNDNUM(num,rndnum)
*
      INTEGER num
      REAL*8 rndnum(num)

      integer ir,is,irs,ir1,ikeep,ithrow,ivl1,ivl,ibase,iv,icall
      real basein
      parameter (ir=24,is=10,irs=ir-is,ir1=ir-1)
      parameter (ikeep=ir,ithrow=24,ivl1=ikeep+ithrow,ivl=ivl1+ir1)
      parameter (ibase=2**24,basein=1.0/ibase)
*
*  state of the generator (to be saved and reloaded for continuation):
      common/rrand/ iv(0:ivl),icall
      save /rrand/
*
      integer i,j,ivn

      INTEGER nn
*
      DO nn = 1,num

      if (icall.eq.ikeep) then
*  disregard ithrow numbers:
        do j=1,ithrow
          ivn=iv(irs+icall)-iv(icall)
          icall=icall+1
*  carry bit:
          if (ivn.lt.0) then
            iv(icall)=iv(icall)+1
            ivn=ivn+ibase
          endif
*
          iv(icall+ir1)=ivn
        enddo
*  copy last ir numbers to the beginning:
        do i=0,ir1
          iv(i)=iv(ivl1+i)
        enddo
      icall=0
      endif
*
*  basic cycle:
*
      ivn=iv(irs+icall)-iv(icall)
      icall=icall+1
*  carry bit:
      if (ivn.lt.0) then
        iv(icall)=iv(icall)+1
        ivn=ivn+ibase
      endif
*
      iv(icall+ir1)=ivn
*  convert to floating point:
      rndnum(nn)=float(ivn)*basein
      if (rndnum(nn).eq.0.0d0) rndnum(nn)=4.5d-9

       ENDDO

      end


**************************************************************************
*==========================================================
**************************************************************************

*  initialize from a random seed iran, 0 <= iran < 259200, 
*  with the help of a "quick and dirty generator
*
      subroutine rini(iran)
*
      integer ir,is,irs,ir1,ikeep,ithrow,ivl1,ivl,ibase,iv,icall
      real basein
      parameter (ir=24,is=10,irs=ir-is,ir1=ir-1)
      parameter (ikeep=ir,ithrow=24,ivl1=ikeep+ithrow,ivl=ivl1+ir1)
      parameter (ibase=2**24,basein=1.0/ibase)
*
*  state of the generator (to be saved and reloaded for continuation):
      common/rrand/ iv(0:ivl),icall
      save /rrand/
*
      integer im,ia,ic,iran,jran,i,ifac
*
*  parameters for auxiliary generator used for initialization:
      parameter(im=259200,ia=7141,ic=54773)
*
      if(iran.lt.0.or.iran.ge.im)stop
     X 'rini: iran out of range'
      jran=iran
*  warm up the auxiliary generator a little
      do i=1,10
*  cycle of auxiliary generator:
        jran=mod(jran*ia+ic,im)
      enddo
*
      ifac=(ibase-1)/im
*  initialize 0 <= iv < ibase:
      do i=0,ir1
        jran=mod(jran*ia+ic,im)
        iv(i)=ifac*jran
      enddo
      icall=0
      write(*,'(/)')
      write(*,*)' rini: random #s newly initialized, iran = ',iran
      end
*==========================================================
