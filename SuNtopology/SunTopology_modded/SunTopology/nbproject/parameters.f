c     size of the lattice
      parameter(lx1=12,lx2=12,lx3=12,lx4=16)
      parameter(lsizeb=lx1*lx2*lx3,lsize=lsizeb*lx4)
c     SU(2) gauge group parameters 
      parameter(ncol=2,ncol2=ncol*ncol)
c
c     Jack-knife analysis of the action
c     niter = icallg*ibing*numbin
c     number of measures = niter/icallg
c     ibing >= 20
c     numbin >= 20
      parameter(icallg=5,niter=100,nmeasl=niter/icallg)
      parameter(ibing=10,numbin=20)
c
c     Topological susceptibility
      parameter(ncool=20)
      parameter(qcut=0.02,rrcut=4.0,max=999,maxp=99)
c
c     For reading in Simon's configs
c     numtasks: number of mpi tasks
c     ksize: spatial dimension
c     ksizet: temporal dimension
c     kvol: volume of the lattice
c     ifirstconf: index of the first configuration
c     isave: sweeps between two successive configurations
c     itermax: total number of configurations
      parameter(numtasks=1)
      parameter(ksize=12,ksizet=16,kvol=ksizet*ksize*ksize*ksize)
      parameter(ifirstc=50,isave=50,itermax=48)