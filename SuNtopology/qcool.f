!     
! File:   qcool.f
! Author: Dean - moved from main source code
!
! Created on 26 June 2014, 14:39
!
!***************************************************************
        subroutine qcool(conp, maximumCools)
            
        implicit real*8 (a-h,o-z)
        include 'parameters.inc'

        common/anisotropy/srat,trat
        common/arrays/u11(ncol2*lsize*4)
        common/hb/betag,ibias
        integer conp
        dimension v11(ncol2*lsize*4)
        double complex u11,v11
        
        
        character(len=:), allocatable :: path
        character(len=:), allocatable :: outputFilenameUncooled
        character(len=:), allocatable :: outputFilenameCooled
        
        character*4 conpString
        character*4 coolingCycleString
!        character*250 simulationFilename
        
        integer coolingCycle
        
        write(conpString,'(i4.4)') conp
        
c
        do 10 ns=1,ncol2*lsize*4
            v11(ns)=u11(ns)
10      continue
c
!       Working directory
        path = '/media/dean/raw_data/Dean/QCD Data/'
     1  //'Raw configurations/Pygrid/'
     1  //'gauge_fixed/16x08/mu0700/'
     
        outputFilenameUncooled = path//'cool0000_conp'//conpString
        
        !write(6, *)simulationFilename
     
        !   open a new file to write the existing configuration
        open(unit=31,file=outputFilenameUncooled,status='unknown',
     1  form='unformatted')
     
        !   Write the uncooled file and close
        write (31) v11
        close(31)
            
        ibias=3
        
        write (6, 900)outputFilenameUncooled, maximumCools, 
     1  'cooling steps.'
        
!       Create a file to track statistics for this cooling cycle
        call file_stats_headers(path, conpString)
            
        do 20 coolingCycle = 1,maximumCools
            !   return the current cooling cycle number as a string            
            write(coolingCycleString,'(i4.4)') coolingCycle
            
            !   Create a string representing the filename to be cooled
            outputFilenameCooled = path//'cool'//coolingCycleString
     1      //'_conp'//conpString
        
            
            call updatg(ibias,actl,betag,srat,trat)
            call action(0,1)
            
            call ffdact(path,conpString)
            
!           open a new file to write the existing (uncooled) configuration
            open(unit=31,file=outputFilenameCooled,
     1      status='unknown',form='unformatted')
            
            write (31) u11
            close(31)
            
            if ( coolingCycle .eq. maximumCools ) call search
20      continue
c
        do 11 ns=1,ncol2*lsize*4
            u11(ns)=v11(ns)
11      continue
c
        
        return

        !   Format string for current simulation
        !   and maximum number of cools
900     format(a20, i4, a15)
        end
        
        
