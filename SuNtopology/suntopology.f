c***************************************************************
c 20-12-01
c d=4 su(ncol) code with  q,  j=0,2   p,c=+,+  glueballs 
c n-ality = 1,2,3 flux loops
c
c*****
c
c blocking uses products of 2 smeared links :
c itype different parameter sets
c 
c warning: poly vacs are real -- no good for finite t!
c
c***************************************************************
        implicit real*8 (a-h,o-z)
        include 'parameters.inc'
c
        common/arrays/u11(ncol2*lsize*4)
        common/anisotropy/srat,trat
        real*8 rndnum
        double complex u11
c
        write (*,*) 'Hello!'
        srat=1.0
        trat=1.0
        call check_parameters()
        iseed=19752
        call rini(iseed)
        call setup
c
        betag=2.5d0
c
        write(6,90)
90      format(' ****************************************************')
        write(6,91)
91      format(' *')
        write(6,92) lx1,lx2,lx3,lx4,betag,ncol
92      format('  lx = ',4i6,'    beta =',f8.4,'   ncolor = ',i4)
        write(6,91)
        write(6,91)
        write(6,90)
c
c        goto999
c
        
        
        iconf = ifirstc
        do iter=1,niter
           call read_conf(iconf)
           call action(0,iter)
           call qcool(iconf, 50)
           if (iter.eq.itermax) goto 111
           iconf = iconf + isave
           !write u11, u22 arrays
        enddo
c
 111    continue
c
c        call action(1,0)
c
        stop
        end

C If the variable XX is supposed to get a random value, just say 

C XX=rndnum() .

C For the initialisation, in the main program just after reading in the
C old configs ( the REWIND and READ for units 40,50) a statement
C  CALL rini(iseed)
C is needed.

*  file random.f, Ulli Wolff, 15.9.95
*
*  random generator modified RCARRY, 
*  see M. Luescher, Comp. Phys. Comm. 79(1994)100
*  and literature quoted there
*
*  This generator is relatively slow, but of KNOWN good quality
*  The speed strongly depends on the value ithrow
*  One way to speed up would be vectoriztion over parallel copies
*
*  ALWAYS CHANGE PARAMETERS IN BOTH ROUTINES OF THIS FILE
*
*  values for ithrow: 0 for simple uses, 24 for normal, 199 for
*  practically no detectable correlations, 365 for tests in delicated cases;
*  the bigger ithrow, the slower the generator!
*
*  0 <= rndnum < 1  (rndnum=0 DOES occur occasionally)
*  
**************************************************************************

      real*8 function rndnum()
*
      integer ir,is,irs,ir1,ikeep,ithrow,ivl1,ivl,ibase,iv,icall
      real basein
      parameter (ir=24,is=10,irs=ir-is,ir1=ir-1)
      parameter (ikeep=ir,ithrow=24,ivl1=ikeep+ithrow,ivl=ivl1+ir1)
      parameter (ibase=2**24,basein=1.0/ibase)
*
*  state of the generator (to be saved and reloaded for continuation):
      common/rrand/ iv(0:ivl),icall
      save /rrand/
*
      integer i,j,ivn
*
      if (icall.eq.ikeep) then
*  disregard ithrow numbers:
        do j=1,ithrow
          ivn=iv(irs+icall)-iv(icall)
          icall=icall+1
*  carry bit:
          if (ivn.lt.0) then
            iv(icall)=iv(icall)+1
            ivn=ivn+ibase
          endif
*
          iv(icall+ir1)=ivn
        enddo
*  copy last ir numbers to the beginning:
        do i=0,ir1
          iv(i)=iv(ivl1+i)
        enddo
      icall=0
      endif
*
*  basic cycle:
*
      ivn=iv(irs+icall)-iv(icall)
      icall=icall+1
*  carry bit:
      if (ivn.lt.0) then
        iv(icall)=iv(icall)+1
        ivn=ivn+ibase
      endif
*
      iv(icall+ir1)=ivn
*  convert to floating point:
      rndnum=float(ivn)*basein
      if (rndnum.eq.0.0) rndnum = 4.5d-9
      end


C**************************************************************************

c     This gets num random numbers at one time, to save on overheads.
c     There is the pesky IF (icall .EQ. ikeep) line here.

c     SInce ikeep is only 24, this statement is unavoidable.

C**************************************************************************

      SUBROUTINE MULTI_RNDNUM(num,rndnum)
*
      INTEGER num
      REAL*8 rndnum(num)

      integer ir,is,irs,ir1,ikeep,ithrow,ivl1,ivl,ibase,iv,icall
      real basein
      parameter (ir=24,is=10,irs=ir-is,ir1=ir-1)
      parameter (ikeep=ir,ithrow=24,ivl1=ikeep+ithrow,ivl=ivl1+ir1)
      parameter (ibase=2**24,basein=1.0/ibase)
*
*  state of the generator (to be saved and reloaded for continuation):
      common/rrand/ iv(0:ivl),icall
      save /rrand/
*
      integer i,j,ivn

      INTEGER nn
*
      DO nn = 1,num

      if (icall.eq.ikeep) then
*  disregard ithrow numbers:
        do j=1,ithrow
          ivn=iv(irs+icall)-iv(icall)
          icall=icall+1
*  carry bit:
          if (ivn.lt.0) then
            iv(icall)=iv(icall)+1
            ivn=ivn+ibase
          endif
*
          iv(icall+ir1)=ivn
        enddo
*  copy last ir numbers to the beginning:
        do i=0,ir1
          iv(i)=iv(ivl1+i)
        enddo
      icall=0
      endif
*
*  basic cycle:
*
      ivn=iv(irs+icall)-iv(icall)
      icall=icall+1
*  carry bit:
      if (ivn.lt.0) then
        iv(icall)=iv(icall)+1
        ivn=ivn+ibase
      endif
*
      iv(icall+ir1)=ivn
*  convert to floating point:
      rndnum(nn)=float(ivn)*basein
      if (rndnum(nn).eq.0.0d0) rndnum(nn)=4.5d-9

       ENDDO

      end


**************************************************************************
*==========================================================
**************************************************************************

*  initialize from a random seed iran, 0 <= iran < 259200, 
*  with the help of a "quick and dirty generator
*
      subroutine rini(iran)
*
      integer ir,is,irs,ir1,ikeep,ithrow,ivl1,ivl,ibase,iv,icall
      real basein
      parameter (ir=24,is=10,irs=ir-is,ir1=ir-1)
      parameter (ikeep=ir,ithrow=24,ivl1=ikeep+ithrow,ivl=ivl1+ir1)
      parameter (ibase=2**24,basein=1.0/ibase)
*
*  state of the generator (to be saved and reloaded for continuation):
      common/rrand/ iv(0:ivl),icall
      save /rrand/
*
      integer im,ia,ic,iran,jran,i,ifac
*
*  parameters for auxiliary generator used for initialization:
      parameter(im=259200,ia=7141,ic=54773)
*
      if(iran.lt.0.or.iran.ge.im)stop
     X 'rini: iran out of range'
      jran=iran
*  warm up the auxiliary generator a little
      do i=1,10
*  cycle of auxiliary generator:
        jran=mod(jran*ia+ic,im)
      enddo
*
      ifac=(ibase-1)/im
*  initialize 0 <= iv < ibase:
      do i=0,ir1
        jran=mod(jran*ia+ic,im)
        iv(i)=ifac*jran
      enddo
      icall=0
      write(*,'(/)')
      write(*,*)' rini: random #s newly initialized, iran = ',iran
      end
*==========================================================
