!     
! File:   action.f
! Author: Dean - moved from main source code
!
! Created on 26 June 2014, 14:28
!
!*********************************************************************
! action
!*********************************************************************
      subroutine action(ipr,iter)
      implicit real*8 (a-h,o-z)
      include 'parameters.inc'

      common/astore/actn(numbin,6)
      common/arrays/u11(ncol2,lsize,4)
      common/next/iup(lsize,4),idn(lsize,4)
      dimension dum11(ncol2)
     &,a11(ncol2),b11(ncol2),c11(ncol2),d11(ncol2)
      double complex u11,a11,b11,c11,d11,dum11,act
c
      dimension avac(numbin),avacsq(numbin),avacs(numbin),avact(numbin)
      dimension val(numbin),av(numbin)
c
      ibin=niter/numbin
c      write (*,*) 'niter', niter, numbin, ibin
c
      if(ipr.eq.0)then
      jbin=(iter-1)/ibin+1
c
      if(iter.eq.1)then
      do 3 nb=1,numbin
      avac(nb)=0.0d0
      avacsq(nb)=0.0d0
      avacs(nb)=0.0d0
      avact(nb)=0.0d0
3     continue
      endif
c
      vacts=0.0
      vactt=0.0
c loop over all the sites on the lattice
      do 10 nn=1,lsize
c loop over the first direction
      do 10 mu=1,3
      m1=nn
c reconstruct the 2x2 matrix from its compressed form
      do 6  ij=1,ncol2
      a11(ij)=u11(ij,m1,mu)
6     continue
c Address of next link in mu direction
      m2=iup(m1,mu)
c loop over the second direction
      do 11 nu=mu+1,4
      iplaq=6-nu-mu+5*(nu/4)
c reconstruct the 2x2 matrix from its compressed form
      do 7  ij=1,ncol2
      b11(ij)=u11(ij,m2,nu)
7     continue
c compute the matrix product c = a.b
      call vmx(1,a11,b11,c11,1)
c Address of next link in nu direction
      m3=iup(m1,nu)
      do 8  ij=1,ncol2
      d11(ij)=u11(ij,m3,mu)
8     continue
c compute hermitian conjugate (dagger)
      call herm(1,d11,dum11,1)
c compute the matrix product b = c.d_dag
      call vmx(1,c11,d11,b11,1)
      do 9  ij=1,ncol2
      d11(ij)=u11(ij,m1,nu)
9     continue
c compute hermitian conjugate (dagger)
      call herm(1,d11,dum11,1)
c compute the trace of the matrix product act = tr(b.d_dag)
      call trvmx(1,b11,d11,act,1)
      ann=1.0/ncol
      act=ann*real(act)
      actn(jbin,iplaq)=actn(jbin,iplaq)+act
      if(nu.ne.4)then
c update the spacelike total action
      vacts=vacts+act
      else
c update the timelike total action
      vactt=vactt+act
      endif
c
11    continue
10    continue
c compute average spacelike / timelike plaquette
      vacts=vacts/(3.0*lsize)
      vactt=vactt/(3.0*lsize)
      write (91,*) vacts, vactt, 0.5*(vacts + vactt)
c
      avacs(jbin)=avacs(jbin)+vacts
      avact(jbin)=avact(jbin)+vactt
c
      if(iter.eq.niter)then
      do 17 ip=1,6
      do 17 jb=1,numbin
      actn(jb,ip)=actn(jb,ip)/(ibin*lsize)
17    continue
      endif
c
      endif
c
      if(ipr.eq.1)then
c
      write(6,80)
80    format(' *****************************************************')
      write(6,100)
100   format('                 action            ')
      write(6,80)
      write(6,81)
81    format(' *  ')
      do 20 nb=1,numbin
      av(nb)=avacs(nb)/ibin
20    continue
c jack knife method of analysing correlated data
      call jackm(numbin,av,avrs,ers)
      do 22 nb=1,numbin
      av(nb)=avact(nb)/ibin
22    continue
      call jackm(numbin,av,avrt,ert)
      write(6,110) avrs,ers
110   format('   aver,err   space action =',2f12.8)
      write(6,112) avrt,ert
112   format('   aver,err   time  action =',2f12.8)
      write(6,81)
c
      endif
c
      return
      end
