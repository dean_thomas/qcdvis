!     
! File:   file_stats.f
! Author: 4thFloor
!
! Created on 18 August 2014, 13:34
!

      subroutine file_stats_headers(pathBuffer, confBuffer)
        character(len=*) :: pathBuffer  
        character(len=*) :: confBuffer
        character(len=len_trim(pathBuffer)) :: trimmedPath
        character(len=len_trim(confBuffer)) :: trimmedConf
        
!       Remove trailing spaces
        trimmedPath = trim(pathBuffer)
        trimmedConf = trim(confBuffer)
        
        !   Create the action file
        open(unit = 31, file=trimmedPath//"action"//
     1  '_conp'//trimmedConf//".txt")
        write(31, 801)"acttot","actmax"
        close(31)
        
        !   Create the ffdual file
        open(unit = 32, file=trimmedPath//"ffdual"//
     1  '_conp'//trimmedConf//".txt")
        write(32, 802)"ffdtot",'',"ffdmod",'',
     1  "ffdmax",'',"ffdmin"
        close(32)
        
         !   Create the maxima file
        open(unit = 33, file=trimmedPath//"maxima"//
     1  '_conp'//trimmedConf//".txt")
        write(33, 803)"s_max",'',"q_max",'',"min"
        close(33)
        
        !   Header format for action.txt
801     format ('',6a12)
        !   Header format for ffdual.txt
802     format (a12,a6,a12,a6,a12,a6,a12)
        !   Header format for maxima.txt
803     format(a12,a6,a12,a6,a12,a18)
      end