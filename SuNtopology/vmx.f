!     
! File:   vmx.f
! Author: Dean - moved from main code
!
! Created on 03 June 2014, 15:10
!

c**********************************************************
c  vector matrix multiply ... 5*5 complex
c      a    input1
c      b    input2
c      c    result
c**********************************************************
        subroutine vmx(nnn1,a,b,c,nnn2)
        implicit real*8 (a-h,o-z)
        include 'parameters.inc'
c
        double complex a(ncol,ncol),b(ncol,ncol),c(ncol,ncol),csum
c
        
        do 1 j=1,ncol
        do 1 i=1,ncol
        csum=(0.0,0.0)
        do 2 k=1,ncol
        csum=csum+a(i,k)*b(k,j)
2       continue
        c(i,j)=csum
1       continue
c

c        write (6, *) "a=", a
c        write (6, *) "b=", b
c        write (6, *) "c=", c
c        write (6, *)
        
        return
        end
