/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaapplication1;

/**
 *
 * @author dean
 */
public class JavaApplication1 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        int GENE_COUNT = 6;
        int PARENT_COUNT = 4;
        
        int parent = 0;
        for (int gene = 0; gene < GENE_COUNT; ++gene)
        {
            System.out.println("Gene " + gene + " comes from parent " + parent);
            
            //  i.e loops 0, 1, 2, 3, 0, 1, 2, 3, 0, .. etc
            parent = ((parent + 1) % PARENT_COUNT);
            
            //  alternatively could be write using an if statement
            //  ++parent;
            //  if (parent == PARENT_COUNT) parent = 0;
        }
    }
    
}
