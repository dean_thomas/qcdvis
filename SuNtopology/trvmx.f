!     
! File:   trvmx.f
! Author: Dean - moved from main source code
!
! Created on 26 June 2014, 14:32
!
!**********************************************************
!  trace product .. 5*5 complex
!**********************************************************
        subroutine trvmx(nnn1,a,b,cc,nnn2)
        implicit real*8 (a-h,o-z)
        include 'parameters.inc'
c
        double complex a(ncol,ncol),b(ncol,ncol),cc
c
        cc=(0.0,0.0)
        do 1 i=1,ncol
        do 1 k=1,ncol
        cc=cc+a(i,k)*b(k,i)
1       continue
c
        return
        end


