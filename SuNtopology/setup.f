!     
! File:   setup.f
! Author: Dean - moved from main source code
!
! Created on 26 June 2014, 14:40
!
!***********************************************************
! ok
!  here we set up  link pointers for full lattice
!***********************************************************
      subroutine setup
      implicit real*8 (a-h,o-z)
      include 'parameters.inc'
c
      common/next/iup(lsize,4),idn(lsize,4)
c
      lx12=lx1*lx2
      lx123=lx12*lx3
      lx1234=lx123*lx4
c
      nn=0
      do 2 l4=1,lx4
      do 2 l3=1,lx3
      do 2 l2=1,lx2
      do 2 l1=1,lx1
      nn=nn+1
c
      nu1=nn+1
      if((l1+1).gt.lx1) nu1=nu1-lx1
      iup(nn,1)=nu1
      nd1=nn-1
      if((l1-1).lt.1) nd1=nd1+lx1
      idn(nn,1)=nd1
c
      nu2=nn+lx1
      if((l2+1).gt.lx2) nu2=nu2-lx12
      iup(nn,2)=nu2
      nd2=nn-lx1
      if((l2-1).lt.1) nd2=nd2+lx12
      idn(nn,2)=nd2
c
      nu3=nn+lx12
      if((l3+1).gt.lx3) nu3=nu3-lx123
      iup(nn,3)=nu3
      nd3=nn-lx12
      if((l3-1).lt.1) nd3=nd3+lx123
      idn(nn,3)=nd3
c
      nu4=nn+lx123
      if((l4+1).gt.lx4) nu4=nu4-lx1234
      iup(nn,4)=nu4
      nd4=nn-lx123
      if((l4-1).lt.1) nd4=nd4+lx1234
      idn(nn,4)=nd4
c
2     continue
c
      return
      end
