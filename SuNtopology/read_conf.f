!     
! File:   read_conf.f
! Author: Dean - moved from main source code
!
! Created on 03 June 2014, 15:15
!

c***************************************************************
        subroutine read_conf(isweep)
        implicit real*8 (a-h,o-z)
        include 'parameters.inc'

        common/arrays/u11(ncol2,lsize,4)
        dimension v11(kvol,4),v12(kvol,4)
        double complex u11,v11,v12
        character*6 c

        character*(*) dir
        PARAMETER (dir = '/media/dean/raw_data/Dean/QCD Data/'
     1      //'Raw configurations/Pygrid/')
        
        character*(*) confDir
        PARAMETER (confDir = 'gauge_fixed/16x08/mu0700/')
        
        character*(*) confName
        PARAMETER (confName = 'config.b190k1680mu0700j02s16t08')
        
        character*(*) fileSuffix
        PARAMETER (fileSuffix = '_gfixed')
c       PARAMETER (fileSuffix = '')
               
        write(c,'(i6.6)') isweep
        open(unit=31,file=dir//confDir//confName
     1      //'.'//c//fileSuffix,
     1      status='old',form='unformatted')
        do itasks=1, numtasks
           read (31) v11,v12
           do idir=1, 4
              ipoint = (itasks - 1)*kvol
              do kpoint = 1, kvol
                 ipoint = ipoint + 1
                 u11(1,ipoint,idir) = v11(kpoint,idir)
                 u11(2,ipoint,idir) = -conjg(v12(kpoint,idir))
                 u11(3,ipoint,idir) = v12(kpoint,idir)
                 u11(4,ipoint,idir) = conjg(v11(kpoint,idir))
              enddo
           enddo
        enddo

        return
        end
