!     
! File:   jackm.f
! Author: Dean - moved from main source code
!
! Created on 26 June 2014, 14:38
!
!************************************************************
!  jack-knife errors for av(i=1,..,num)
!***********************************************************
      subroutine jackm(num,av,aver,err)
      implicit real*8 (a-h,o-z)
c
      dimension av(*)
c
      sum=0.0d0
      do 10 n=1,num
      sum=sum+av(n)
10    continue
      sum=sum/num
c
      esum=0.0d0
      do 12 n=1,num
      dif=sum-av(n)
      esum=esum+dif*dif
12    continue
      esum=esum/num
c
      aver=sum
      err=sqrt(esum*num)/(num-1)
c
      return
      end
