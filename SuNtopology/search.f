!     
! File:   search.f
! Author: Dean - moved from main source code
!
! Created on 26 June 2014, 14:37
!
!***************************************************************
!   here we search for points where ffd(n,lt) is a maximum
!   or minimum
!**************************************************************
        subroutine search
        implicit real*8 (a-h,o-z)
        include 'parameters.inc'
c
        common/dummy/ffd(lsize)
        common/next/iup(lsize,4),idn(lsize,4)
        dimension delu(4),deld(4)
     &  ,ixx(max),iyy(max),izz(max),itt(max)
     &  ,ix1(maxp),iy1(maxp),iz1(maxp),it1(maxp)
        dimension q(max),eps(max),in(max),it(max)
        dimension q1(maxp),eps1(maxp)
c
      nnum=0
      do 4 n=1,max
      q(n)=0.0
4     continue
      do 6 n=1,maxp
      q1(n)=0.0
6     continue
c
      do 10 nn=1,lsize
      m1=nn
      if(nnum.eq.max)goto10
c
      fff=ffd(m1)
      do 11 mu=1,4
      m2=iup(m1,mu)
      m3=idn(m1,mu)
      ffdu=ffd(m2)
      ffdd=ffd(m3)
      delu(mu)=abs(fff)-abs(ffdu)
      deld(mu)=abs(fff)-abs(ffdd)
11    continue
c
      do 15 mu=1,4
      if(delu(mu).lt.0.or.deld(mu).lt.0)goto10
15    continue
      if(abs(fff).lt.qcut)goto10
      nnum=nnum+1
      q(nnum)=fff
      eps(nnum)=delu(1)+deld(1)+delu(2)+deld(2)
     &+delu(3)+deld(3)+delu(4)+deld(4)
      in(nnum)=nn
c
10    continue
c
        lx123=lx1*lx2*lx3
        lx12=lx1*lx2
        do 30 nn=1,max
        if(q(nn).eq.0)goto30
        jmax=in(nn)-1
        is4=jmax/(lx123)+1
        itt(nn)=is4
        jmax=jmax-(is4-1)*lx123
        is3=jmax/(lx12)+1
        izz(nn)=is3
        jmax=jmax-(is3-1)*lx12
        is2=jmax/lx1+1
        iyy(nn)=is2
        jmax=jmax-(is2-1)*lx1
        is1=jmax+1
        ixx(nn)=is1
30      continue
c
        num=0
        do 32 nn=1,nnum
        if(num.eq.maxp)goto32
        do 33 mm=1,nnum
        if(mm.eq.nn)goto33
        if(abs(q(mm)).lt.abs(q(nn))) goto33
        delx=abs(ixx(nn)-ixx(mm))
        if(delx.gt.lx1/2) delx=lx1-delx
        dely=abs(iyy(nn)-iyy(mm))
        if(dely.gt.lx2/2) dely=lx2-dely
        delz=abs(izz(nn)-izz(mm))
        if(delz.gt.lx3/2) delz=lx3-delz
        delt=abs(itt(nn)-itt(mm))
        if(delt.gt.lx4/2) delt=lx4-delt
        iss=delx*delx+dely*dely+delz*delz+delt*delt
        if(iss.gt.rrcut)goto33
        goto32
33      continue
        num=num+1
        q1(num)=q(nn)
        ix1(num)=ixx(nn)
        iy1(num)=iyy(nn)
        iz1(num)=izz(nn)
        it1(num)=itt(nn)
        eps1(num)=eps(nn)
32      continue
c
        write(6,93) qcut,rrcut
93      format(' global maxima of abs(ffdual); q,r.r cuts =',f10.4,f8.2)
        write(6,94)
94      format(' ffdual  x   y   z   t   av diff ')
        do 35 nn=1,maxp
        if(q1(nn).eq.0)goto35
        qq=q1(nn)
        epss=eps1(nn)*0.125
        is4=it1(nn)
        is3=iz1(nn)
        is2=iy1(nn)
        is1=ix1(nn)
        write(6,95) qq,is1,is2,is3,is4,epss
95      format(f10.4,4i5,f10.5)
35      continue
c
        return
        end

