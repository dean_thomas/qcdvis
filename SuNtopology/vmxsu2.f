!     
! File:   vmxsu2.f
! Author: Dean - moved from main source code 
!
! Created on 26 June 2014, 14:31
!
!**********************************************************
!  Semplification of vmx when b is an SU(2) matrix
!**********************************************************
        subroutine vmxsu2(i1,i2,a,b)
        implicit real*8 (a-h,o-z)
c
        include 'parameters.inc'

        double complex a(ncol,ncol),b(2,2),c(ncol,2)!,csum
        
c
        do i=1,ncol
           c(i,1) = a(i,i1)*b(1,1) + a(i,i2)*b(2,1)
           c(i,2) = a(i,i1)*b(1,2) + a(i,i2)*b(2,2)
        enddo
c
        do i=1,ncol
           a(i,i1) = c(i,1)
           a(i,i2) = c(i,2)
        enddo
c   
        return
        end