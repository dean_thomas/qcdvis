!     
! File:   renorm.f
! Author: Dean - moved from main source code
!
! Created on 26 June 2014, 14:30
!
!**********************************************************
!  ok
!  this routine reimposes the unitarity constraints on
!  our su(3) matrices
!**********************************************************
        subroutine renorm
        implicit real*8 (a-h,o-z)
        include 'parameters.inc'
c
        common/arrays/u11(ncol,ncol,lsize,4)
        double complex adum(ncol,ncol),u11,csum
c
        do 100 mu=1,4
        do 100 nn=1,lsize
c
        do 1 j=1,ncol
        do 1 i=1,ncol
        adum(i,j)=u11(i,j,nn,mu)
1       continue
c
        do 20 n2=1,ncol
c
        do 10 n3=1,n2-1
c
        csum=(0.0,0.0)
        do 5 n1=1,ncol
        csum=csum+adum(n1,n2)*conjg(adum(n1,n3))
5       continue
        do 6 n1=1,ncol
        adum(n1,n2)=adum(n1,n2)-csum*adum(n1,n3)
6       continue
c
10      continue
c
        sum=0.0
        do 7 n1=1,ncol
        sum=sum+adum(n1,n2)*conjg(adum(n1,n2))
7       continue
        anorm=1.0/sqrt(sum)
        do 8 n1=1,ncol
        adum(n1,n2)=adum(n1,n2)*anorm
8       continue
c
20      continue
c
        do 2 j=1,ncol
        do 2 i=1,ncol
        u11(i,j,nn,mu)=adum(i,j)
2       continue
c
100     continue
        return
        end