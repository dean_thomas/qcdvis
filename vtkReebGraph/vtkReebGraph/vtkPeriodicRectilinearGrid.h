#pragma once
#include <vtkDataSet.h>
#include <vtkPointSet.h>
#include <vtkSmartPointer.h>
#include <vtkObjectFactory.h>
#include <vtkRectilinearGrid.h>
#include <iostream>

#ifndef __PRETTY_FUNCTION__
#define __PRETTY_FUNCTION__ __FUNCSIG__
#endif

using namespace std;

class vtkPeriodicRectilinearGrid : public vtkRectilinearGrid
{

public:
	vtkTypeMacro(vtkPeriodicRectilinearGrid, vtkRectilinearGrid);

	static vtkPeriodicRectilinearGrid *New();

protected:
	vtkPeriodicRectilinearGrid()
		: vtkRectilinearGrid() 
	{ 
		cout << __PRETTY_FUNCTION__ << endl;
	};

	vtkCell *vtkPeriodicRectilinearGrid::GetCell(vtkIdType cellId)
	{
		auto data = vtkRectilinearGrid::GetCell(cellId);

		cout << __PRETTY_FUNCTION__ << endl;

		return data;
	}

	void GetCell(vtkIdType cellId, vtkGenericCell *cell)
	{
		vtkRectilinearGrid::GetCell(cellId, cell);

		cout << __PRETTY_FUNCTION__ << endl;
	}

	vtkIdType FindCell(double x[3], vtkCell *cell, vtkIdType cellId, double tol2,
					   int& subId, double pcoords[3], double *weights)
	{
		auto result = vtkRectilinearGrid::FindCell(x, cell, cellId, tol2, subId, pcoords, weights);

		cout << __PRETTY_FUNCTION__ << endl;

		return result;
	}

	vtkIdType FindCell(double x[3], vtkCell *cell, vtkGenericCell *gencell,
					   vtkIdType cellId, double tol2, int& subId,
					   double pcoords[3], double *weights)
	{
		auto result = vtkRectilinearGrid::FindCell(x, cell, gencell, cellId, tol2, subId, pcoords, weights);

		cout << __PRETTY_FUNCTION__ << endl;

		return result;
	}

	vtkCell *FindAndGetCell(double x[3], vtkCell *cell, vtkIdType cellId,
							double tol2, int& subId, double pcoords[3],
							double *weights)
	{
		auto result = vtkRectilinearGrid::FindAndGetCell(x, cell, cellId, tol2, subId, pcoords, weights);

		cout << __PRETTY_FUNCTION__ << endl;

		return result;
	}

	//vtkIdType GetNumberOfCells() 
	//{
	//	auto result = vtkRectilinearGrid::GetNumberOfCells();

	//	cout << __PRETTY_FUNCTION__ << endl;

	//	return result;
	//}

	//vtkIdType GetNumberOfPoints()
	//{
	//	auto result = vtkRectilinearGrid::GetNumberOfPoints();

	//	cout << __PRETTY_FUNCTION__ << endl;

	//	return result;
	//}

	~vtkPeriodicRectilinearGrid() 
	{ 
	}

private:
	vtkPeriodicRectilinearGrid(const vtkPeriodicRectilinearGrid&);  // Not implemented.
	void operator=(const vtkPeriodicRectilinearGrid&);  // Not implemented.
};
