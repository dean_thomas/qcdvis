#include "DataParser.h"
#include <cassert>
#include <fstream>

using namespace std;

///
///		\since		17-11-2015
///		\author		Dean
///
DataParser::DataParser(const std::string& filename)
	: m_filename { filename }
{
	cout << "Created parser." << endl;
}

///
///		\since		17-11-2015
///		\author		Dean
///
DataParser::vtkPoints_vptr DataParser::generatePoints(const size_type& xDim, const size_type& yDim, const size_type& zDim)
{
	vtkPoints_vptr result = vtkPoints::New();

	for (auto x = 0; x < xDim; ++x)
	{
		for (auto y = 0; y < yDim; ++y)
		{
			for (auto z = 0; z < zDim; ++z)
			{
				result->InsertNextPoint(x, y, z);
			}
		}
	}
	return result;
}

///
///		\since		17-11-2015
///		\author		Dean
///
DataParser::vtkUnstructuredGrid_vptr DataParser::doParse()
{
	cout << "Parsing file: " << m_filename << "." << endl;
	
	//	Read the data file into temporary storage
	auto fileRead = readFile();

	if (fileRead)
	{
		//	Generate a rectangular grid of points
		auto rectGrid = generateRectilinearGrid(m_fileHeader.sizeX, 
												m_fileHeader.sizeY, 
												m_fileHeader.sizeZ);

		//	Put the STL vector of values into a form that VTK can handle
		auto scalarField = generateDoubleArray();

		//	Convert the rectangular grid to a tetraheadral grid
		auto tetGrid6 = convertToTetrahedralGrid(rectGrid, TetraPerCube::SIX);

		//	Assign the scalar data to the tetrahedral grid
		tetGrid6->GetPointData()->SetScalars(scalarField);

		cout << "done." << endl;

		//	Return the tetrahedral grid
		return tetGrid6;
	}
	else
	{
		cerr << "Could not readfile." << endl;
		return nullptr;
	}
}

///
///		\since		17-11-2015
///		\author		Dean
///
DataParser::vtkDoubleArray_vptr DataParser::generateDoubleArray() const
{
	vtkDoubleArray_vptr result = vtkDoubleArray::New();
	
	for (auto i = 0; i < m_values.size(); ++i)
	{
		result->InsertNextTuple1(m_values[i]);
	}
	return result;
}

///
///		\since		17-11-2015
///		\author		Dean
///
bool DataParser::readFile()
{
	const char TAB = '\t';

	ifstream inFile(m_filename);
	if (inFile.is_open())
	{
		//	Read the header
		getline(inFile, m_fileHeader.ensemble);
		getline(inFile, m_fileHeader.configuration);
		getline(inFile, m_fileHeader.cool);
		getline(inFile, m_fileHeader.time);
		getline(inFile, m_fileHeader.variable);
		inFile >> m_fileHeader.sizeX >> m_fileHeader.sizeY >> m_fileHeader.sizeZ;
		getline(inFile, m_fileHeader.buffer);
		getline(inFile, m_fileHeader.buffer);
		getline(inFile, m_fileHeader.min);
		getline(inFile, m_fileHeader.max);
		getline(inFile, m_fileHeader.tot);
		getline(inFile, m_fileHeader.ave);

		auto valCount = m_fileHeader.sizeX * m_fileHeader.sizeY * m_fileHeader.sizeZ;
		cout << "Expecting " << valCount << " scalar values." << endl;

		//	Reserve space in the vector
		m_values.reserve(valCount);

		auto count = 0;
		while (!inFile.eof())
		{
			const auto MAX_INT = numeric_limits<size_type>::max();

			size_type x = MAX_INT;
			size_type y = MAX_INT;
			size_type z = MAX_INT;
			double val;

			inFile >> x >> y >> z >> val;
			//cout << x << TAB << y << TAB << z << TAB << val << endl;

			if ((x != MAX_INT) && (y != MAX_INT) && (z != MAX_INT))
			{
				m_values.push_back(val);
				++count;
			}
		}

		inFile.close();

		//	Make sure we read in as many values as were expected
		assert(valCount == count);

		return true;
	}
	return false;
}

///
///		\since		17-11-2015
///		\author		Dean
///
DataParser::vtkPeriodicRectilinearGrid_vptr DataParser::generateRectilinearGrid(const size_type& xDim,
																		const size_type& yDim,
																		const size_type& zDim,
																		const int xLower,
																		const int yLower, 
																		const int zLower) const
{
	//	Create our grid and the set the dimensionality
	auto result = vtkPeriodicRectilinearGrid::New();
	result->SetDimensions(xDim, yDim, zDim);

	cout << "Created rectilinear grid with " << result->GetNumberOfPoints()
		<< " points and " << result->GetNumberOfCells() << " cells." << endl;

	//	Calculate the maxima
	const auto X_MAX = xLower + xDim;
	const auto Y_MAX = yLower + yDim;
	const auto Z_MAX = zLower + zDim;

	//	Create the number lines for each dimension
	vtkDoubleArray_vptr xValues = vtkDoubleArray::New();
	vtkDoubleArray_vptr yValues = vtkDoubleArray::New();
	vtkDoubleArray_vptr zValues = vtkDoubleArray::New();

	//	Fill out the coordinate number lines
	for (auto x = xLower; x < X_MAX; ++x)
	{
		xValues->InsertNextValue((double)x);
	}
	for (auto y = yLower; y < Y_MAX; ++y)
	{
		yValues->InsertNextValue((double)y);
	}
	for (auto z = zLower; z < Z_MAX; ++z)
	{
		zValues->InsertNextValue((double)z);
	}

	//	Set the coordinate to each number line
	result->SetXCoordinates(xValues);
	result->SetYCoordinates(yValues);
	result->SetZCoordinates(zValues);

	return result;
}

///
///		\since		17-11-2015
///		\author		Dean
///
DataParser::vtkUnstructuredGrid_vptr DataParser::convertToTetrahedralGrid(vtkPeriodicRectilinearGrid_vptr rectGrid,
																		  const TetraPerCube& tetraCells) const
{
	assert(rectGrid != nullptr);

	auto result = vtkPeriodicRectilinearGridToTetrahedra::New();

	switch (tetraCells)
	{
		case TetraPerCube::FIVE:
			result->SetTetraPerCellTo5();
			break;
		case TetraPerCube::SIX:
			result->SetTetraPerCellTo6();
			break;
		case TetraPerCube::TWELVE:
			result->SetTetraPerCellTo12();
			break;
		case TetraPerCube::FIVE_TWELVE:
			result->SetTetraPerCellTo5And12();
			break;
	}

	cout << result->GetClassName() << endl;
	result->SetInputData(rectGrid);
	result->Update();

	auto tetGrid = result->GetOutput();

	cout << "Created tetrahedral grid with " << tetGrid->GetNumberOfPoints()
		<< " points and " << tetGrid->GetNumberOfCells() << " cells." << endl;

	return tetGrid;
}