#pragma once
#ifndef DATA_PARSER_H
#define DATA_PARSER_H

#include <vtkUnstructuredGrid.h>
#include <vtkPoints.h>
#include <vtkSmartPointer.h>
#include "vtkPeriodicRectilinearGrid.h"
#include "vtkPeriodicRectilinearGridToTetrahedra.h"
#include <vtkDoubleArray.h>
#include <vtkPointData.h>

#include <string>

class DataParser
{
public:
	enum class TetraPerCube
	{
		FIVE,
		SIX,
		TWELVE,
		FIVE_TWELVE
	};

	using size_type = size_t;

	struct FileHeader
	{
		std::string ensemble;
		std::string configuration;
		std::string cool;
		std::string time;
		std::string variable;
		size_type sizeX;
		size_type sizeY;
		size_type sizeZ;
		std::string buffer;
		std::string min;
		std::string max;
		std::string tot;
		std::string ave;
	};

	
	using vtkUnstructuredGrid_vptr = vtkSmartPointer<vtkUnstructuredGrid>;
	using vtkPoints_vptr = vtkSmartPointer<vtkPoints>;
	using vtkPeriodicRectilinearGrid_vptr = vtkSmartPointer<vtkPeriodicRectilinearGrid>;
	using vtkPeriodicRectilinearGridToTetrahedra_vptr = vtkSmartPointer<vtkPeriodicRectilinearGridToTetrahedra>;
	using vtkDoubleArray_vptr = vtkSmartPointer<vtkDoubleArray>;

	FileHeader m_fileHeader;
	std::vector<double> m_values;

	std::string m_filename;

	DataParser(const std::string& filename);
	vtkPoints_vptr generatePoints(const size_type& xDim, const size_type& yDim, const size_type& zDim);
	vtkPeriodicRectilinearGrid_vptr generateRectilinearGrid(const size_type& xDim,
													const size_type& yDim,
													const size_type& zDim,
													const int xLower = 0,
													const int yLower = 0,
													const int zLower = 0) const;
	vtkUnstructuredGrid_vptr convertToTetrahedralGrid(vtkPeriodicRectilinearGrid_vptr rectGrid,
													  const TetraPerCube& tetraCells = TetraPerCube::TWELVE) const;
	vtkDoubleArray_vptr generateDoubleArray() const;

	vtkUnstructuredGrid_vptr doParse();

	bool readFile();

	vtkUnstructuredGrid_vptr operator()()
	{
		return doParse();
	};
};

#endif