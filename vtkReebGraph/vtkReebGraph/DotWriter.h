#pragma once
#ifndef DOT_WRITER_H
#define DOT_WRITER_H

#include <vtkReebGraph.h>
#include <vtkSmartPointer.h>
#include <vtkUnstructuredGrid.h>
#include <vtkDoubleArray.h>

#include <map>

class DotWriter
{
public:
	using Decimal = double;
	using size_type = size_t;
	using VertexHeightMap = std::map<size_type, Decimal>;

	//	Number of height steps
	const size_type SCALE = 255;
	const char TAB = '\t';

	struct DataExtrema
	{
		Decimal max;
		Decimal min;
	};

	using vtkDoubleArray_ptr = vtkDoubleArray*;

	using vtkReebGraph_vptr = vtkSmartPointer<vtkReebGraph>;
	using vtkReebGraph_ptr = vtkReebGraph*;

	using vtkUnstructuredGrid_vptr = vtkSmartPointer<vtkUnstructuredGrid>;
	using vtkUnstructuredGrid_ptr = vtkUnstructuredGrid*;

private:
	vtkUnstructuredGrid_vptr m_data;
	vtkReebGraph_vptr m_reebGraph;

	std::string generateDot() const;
	std::string dotHeader() const;
	std::string edgeString() const;
	std::string vertexListing() const;

	DataExtrema findMinMax() const;
	void dumpScalars() const;
	size_type normalizeHeight(const DataExtrema& range, 
							  const Decimal& height) const;
	VertexHeightMap mapHeightsToVertices() const;

public:
	bool WriteFile(const std::string& filename) const;
	bool operator()(const std::string& filename) const {
		return WriteFile(filename);
	}

	DotWriter(const vtkUnstructuredGrid_ptr data, 
			  const vtkReebGraph_ptr reebGraph);
	~DotWriter();
};

#endif