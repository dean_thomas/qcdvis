#include "ReebGraphExtractor.h"
#include <cassert>

using namespace std;

///
///		\since		17-11-2015
///		\author		Dean
///
ReebGraphExtractor::ReebGraphExtractor(vtkUnstructuredGrid* tetGridData)
{
	m_data = tetGridData;
}

///
///		\since		17-11-2015
///		\author		Dean
///
ReebGraphExtractor::~ReebGraphExtractor()
{
}

///
///		\since		17-11-2015
///		\author		Dean
///
ReebGraphExtractor::vtkReebGraph_vptr ReebGraphExtractor::ComputeReebGraph() const
{
	assert(m_data != nullptr);

	cout << "Computing reeb graph." << endl;

	vtkUnstructuredGridToReebGraphFilter *volumeReebGraphFilter =
		vtkUnstructuredGridToReebGraphFilter::New();

	volumeReebGraphFilter->SetInputData(m_data);
	volumeReebGraphFilter->Update();
	vtkReebGraph *volumeReebGraph = volumeReebGraphFilter->GetOutput();

	cout << "Reeb graph contains " << volumeReebGraph->GetNumberOfEdges()
		<< " edges, and " << volumeReebGraph->GetNumberOfVertices()
		<< " vertices." << endl;
	cout << "done." << endl;

	return volumeReebGraph;
}
