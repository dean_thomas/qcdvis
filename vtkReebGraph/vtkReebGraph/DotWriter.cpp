#include "DotWriter.h"
#include <cassert>
#include <vtkPointData.h>
#include <iostream>
#include <fstream>
#include <vtkEdgeListIterator.h>
#include <vtkVertex.h>
#include <sstream>

using namespace std;

///
///		\since		17-11-2015
///		\author		Dean
///
DotWriter::DotWriter(const vtkUnstructuredGrid_ptr data,
					 const vtkReebGraph_ptr reebGraph)
{
	assert(data != nullptr);
	assert(reebGraph != nullptr);

	m_data = data;
	m_reebGraph = reebGraph;
}

///
///		\since		17-11-2015
///		\author		Dean
///
DotWriter::~DotWriter()
{
}

///
///		\since		17-11-2015
///		\author		Dean
///
void DotWriter::dumpScalars() const
{
	auto scalarData = vtkDoubleArray::SafeDownCast(m_data->GetPointData()->GetScalars());

	ofstream outFile("scalars.txt");
	assert(outFile.is_open());
	for (auto i = 0; i < scalarData->GetNumberOfTuples(); ++i)
	{
		outFile << scalarData->GetTuple1(i) << endl;
	}
	outFile.close();
}

///
///		\since		17-11-2015
///		\author		Dean
///
DotWriter::DataExtrema DotWriter::findMinMax() const
{
	auto scalarData = vtkDoubleArray::SafeDownCast(m_data->GetPointData()->GetScalars());
	
	auto dataMin = scalarData->GetRange()[0];
	auto dataMax = scalarData->GetRange()[1];

	cout << "Data min: " << dataMin
		<< ", Data max: " << dataMax << endl;

	return { dataMin, dataMax };
}

///
///		\since		17-11-2015
///		\author		Dean
///
std::string DotWriter::generateDot() const
{
	stringstream result;

	//	Add the header
	result << dotHeader();

	//	Add edges
	result << edgeString();

	//	Add vertices
	result << vertexListing();

	//	Add the footer
	result << "}";

	return result.str();
}

///
///	\since			18-11-2015
///	\author			Dean
///
std::string DotWriter::dotHeader() const
{
	stringstream result;

	//	Graph header
	result << "digraph G {" << endl;

	//	Height Scale (0..255)
	for (auto i = 0; i < SCALE; ++i)
	{
		//	ie 255..0
		auto val = SCALE - i;

		result << TAB << val << " -> " << (val - 1) << endl;
	}

	return result.str();
}

///
///	\since			18-11-2015
///	\author			Dean
///
std::string DotWriter::edgeString() const
{
	stringstream result; 

	//	Iterate over the edge list 
	vtkSmartPointer<vtkEdgeListIterator> itE = vtkSmartPointer<vtkEdgeListIterator>::New();
	m_reebGraph->GetEdges(itE);

	while (itE->HasNext())
	{
		auto edge = itE->Next();

		result << TAB;
		result << "v" << edge.Source;
		result << " -> ";
		result << "v" << edge.Target;
		result << " [label=\"e" << edge.Id << "\"]";
		result << endl;
	}

	return result.str();
}

///
///	\since			18-11-2015
///	\author			Dean
///
DotWriter::size_type DotWriter::normalizeHeight(const DataExtrema& range,
												const Decimal& height) const
{	
	//	Height normalized 0..1
	auto normF = (height - range.min) / (range.max - range.min);
	assert((normF >= 0.0) && (normF <= 1.0));
	
	//	Height normalized 0..SCALE
	auto normI = (int)((Decimal)SCALE * normF);
	assert((normI >= 0) && (normI <= SCALE));

	return normI;
}

///
///	\since			18-11-2015
///	\author			Dean
///
DotWriter::VertexHeightMap DotWriter::mapHeightsToVertices() const
{
	//	Access to scalar field data
	auto scalarData = vtkDoubleArray::SafeDownCast(m_data->GetPointData()->GetScalars());
	assert(scalarData != nullptr);

	//	Access to Reeb graph vertices
	vtkDataArray *vertexInfo = vtkDataArray::SafeDownCast(
		m_reebGraph->GetVertexData()->GetAbstractArray("Vertex Ids"));
	assert(vertexInfo);

	//	Store a map of Reeb-graph nodes to their height's
	VertexHeightMap result;

	for (auto i = 0; i < vertexInfo->GetNumberOfTuples(); ++i)
	{
		//	Lookup the index in the heightfield array for this
		//	critical point
		auto heightIndex = (int)*(vertexInfo->GetTuple(i));

		//	Lookup the height of the critical point
		auto height = scalarData->GetTuple1(heightIndex);

		//	Store the heigh against the vertex id
		result[i] = height;
	}
	return result;
}

///
///	\since			18-11-2015
///	\author			Dean
///
bool DotWriter::WriteFile(const std::string & filename) const
{
	//	Open the file, return false if failed
	ofstream outFile(filename);
	if (!outFile.is_open()) return false;

	//	Write the Dot and close the file
	outFile << generateDot();
	outFile.close();

	return true;
}

///
///	\since			18-11-2015
///	\author			Dean
///
std::string DotWriter::vertexListing() const
{
	stringstream result;

	//	Retreive a list of critical points and heights
	auto vertexHeights = mapHeightsToVertices();

	//	For normalization
	auto range = findMinMax();

	//	Height's
	for (auto i = 0; i < SCALE; ++i)
	{
		//	ie 255..0
		auto val = SCALE - i;
		
		result << TAB << "{ rank = same; " << val << ";";

		for (auto it = vertexHeights.cbegin(); it != vertexHeights.cend(); ++it)
		{
			//	Retreive the vertex index (in the reeb graph)
			//	and the relevant height (from the scalar field)
			auto vertexIndex = (*(it)).first;
			auto height = (*(it)).second;

			//	Normalize in the range 0..255
			auto norm = normalizeHeight(range, height);

			//	And write to the graph for alignment
			if (norm == i)
			{
				result << " v" << vertexIndex << ";";
			}
		}
		result << " }" << endl;
	}
	return result.str();
}
