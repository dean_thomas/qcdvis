#pragma once
#ifndef REEB_GRAPH_EXTRACTOR_H
#define REEB_GRAPH_EXTRACTOR_H

#include <vtkUnstructuredGrid.h>
#include <vtkSmartPointer.h>
#include <vtkReebGraph.h>
#include <vtkUnstructuredGridToReebGraphFilter.h>

class ReebGraphExtractor
{
public:
	using vtkUnstructuredGrid_vptr = vtkSmartPointer<vtkUnstructuredGrid>;
	using vtkReebGraph_vptr = vtkSmartPointer<vtkReebGraph>;

	vtkUnstructuredGrid_vptr m_data;

	vtkReebGraph_vptr ComputeReebGraph() const;

	ReebGraphExtractor(vtkUnstructuredGrid* tetGridData);
	~ReebGraphExtractor();
};

#endif