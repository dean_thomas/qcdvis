#include <cstdlib>
#include <iostream>
#include <vtkPeriodicDataArray.h>

#include "vtkPeriodicRectilinearGrid.h"
#include "DataParser.h"
#include "ReebGraphExtractor.h"
#include "DotWriter.h"
//#include "PeriodicDataAdapter.h"

int main()
{
	//std::cout << "Loading file." << std::endl;
	const std::string outFile = "C:\\Users\\4thFloor\\Desktop\\reebGraph.dot";

	DataParser parser("C:\\Users\\4thFloor\\Documents\\Qt\\QtFlexibleIsosurfaces\\vtkReebGraph\\vtkReebGraph\\polyakov.vol1");

	auto data = parser.doParse();

	ReebGraphExtractor reebGraphExtractor(data);

	auto reebGraph = reebGraphExtractor.ComputeReebGraph();

	DotWriter dotWriter(data, reebGraph);

	std::cout << "Sucessfully wrote file '" << outFile
		<< std::boolalpha << "': " << dotWriter(outFile) << endl;

	std::cout << "Press enter." << std::endl;
	getchar();

	return 0;
}