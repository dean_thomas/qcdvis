import re
from collections import namedtuple
from collections import defaultdict
import matplotlib.pyplot as plt

JcnVertex = namedtuple('JcnVertex', 'id degree isoValues')

JCN_VERTEX_COMMENT_REGEX = R"^#\s*vertex\s*=\s*{\s*id:\s*(\d+),\s*isovalues\s*=\s*{([\s*\d+,]*)\s*},\s*degree:\s*(\d+)\s*\(inDegree:\s*(\d+),\s*outDegree:\s*(\d+)\)\s*}"

#	\brief		Parses a JCN file extract information about the connectivity
#				of the data
#	\author		Dean
#	\since		14-03-2016
#
def parseJcnFile(filename):
	#	Obtain a list of vertices from the file
	vertices = []

	try:
		file = open(filename, 'r')
		fileContents = file.readlines()
		file.close()
		
		for line in fileContents:
			#print('Parsing line: %s' %(line))
			jcnVertex = extractJcnVertexProperties(line)
		
			if (jcnVertex != None):
				vertices.append(jcnVertex)
				
		print('Extracted %i vertices from JCN: %s' %(len(vertices),filename))
		
		return vertices		
	except FileNotFoundError:
		print("The file: '%s' could not be found." %(filename))

#	\brief`		Extracts properties of JCN vertices from the
#				comments in the file using a Regex.
#	\author		Dean
#	\since		14-03-2016
#
def extractJcnVertexProperties(line):
	try:
		matches = re.search(JCN_VERTEX_COMMENT_REGEX, line)
	
		#	If the line doesn't contain a comment detailing a vertex
		#	it will return None
		if (matches != None):
			vertex_id = int(matches.group(1))
		
			#	Store as a raw string for now (will need to be split into a list of values)
			#	if we want to do anything more advanced with them
			vertex_isoValues = matches.group(2)
		
			#	Only really interested in the 'degree' but we can use the in/outDegree
			#	degrees to test for consistency
			vertex_degree = int(matches.group(3))
			vertex_inDegree = int(matches.group(4))
			vertex_outDegree = int(matches.group(5))
		
			assert (vertex_degree == vertex_inDegree) and (vertex_degree == vertex_outDegree), "Unexpected values for in/out degree for vertex: %i" %(vertex_id)
		
			return JcnVertex(vertex_id, vertex_degree, vertex_isoValues)
	except ValueError:
		print('There was an error parsing the line: %s' %(line))
		
#	\brief		Partitions the list in to a number of sets
#				depending on the degree of the vertex
#	\author		Dean
#	\since		14-03-2016
#
def doVertexPartitioning(vertexList):
	#	Create a map for each vertex degree present in the JCN
	#	Each key points to a set of vertices with the specified degree
	partitionedVertices = defaultdict(set)
	
	for vertex in vertexList:
		#	Use the degree as the key
		degree = vertex.degree
		
		#	Store the vertex in the corresponding set
		partitionedVertices[degree].add(vertex)
		
		
	for key, value in partitionedVertices.items():
		print('Degree %i : %i vertices' %(key, len(value)))
		
	return partitionedVertices

#	\brief		Takes the partitioned vertex lists
#				and displays as a histogram
#	\author		Dean
#	\since		14-03-2016
#
def displayHistogram(partitionedVertices):
	plt.figure('Vertex degrees')
	plt.title('Vertex degrees')
	plt.xlabel('degree')
	plt.ylabel('freq')
	
	degreeVals = []
	freqVals = []
	
	for key, value in partitionedVertices.items():
		freqVals.append(len(value)) 
		degreeVals.append(key)
		
	plt.plot(degreeVals, freqVals)
	plt.show()
	
vertexList = parseJcnFile(R'C:\Users\4thFloor\Desktop\jcn_file.jcn')
partitionedVertices = doVertexPartitioning(vertexList)
displayHistogram(partitionedVertices)