#   Directory / file handling functionality
from os import walk
from os.path import exists
from os import makedirs
from os import rename

from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *

#   Regex
import re

#   Command line and argument parsing
import sys, getopt

#   Group indices within our Regex
COOL_INDEX = 1
CONF_INDEX = 2

#   Keep a count of how many files have been moved
moveCount = 0

#   Storage for raw file list
files = []

#   Our match string for sorting files
regex = "^cool(\d{4})_conp(\d{4})"

#   Main functionality
def doFileMove(verboseOutput):
   #   Prompt user for input directory
    inputDir = getInputPath()
    print("User selected directory: %s" %(inputDir))

    #   Obtain a list of files in the target directory
    for (dirpath, dirnames, filenames) in walk(inputDir):
        files.extend(filenames)
        break

    #   List the files
    if (verboseOutput):
        print("Raw file list")
        for file in files:
            print(file)

    #   Process file list
    for file in files:
        #   Do a regex match on the filenames
        isMatch = re.match(regex, file)

        if (isMatch):
            #   Extract strings from regex
            filename = isMatch.group()
            conf = isMatch.group(CONF_INDEX)
            cool = isMatch.group(COOL_INDEX)

            #   List matching files
            if (verboseOutput):
                print("Filename: ", filename)
                print("Configuration: ", conf)
                print("Cooling: ", cool)

            #   Create a path to where the file should be moved to
            outputDir = inputDir + "/conp" + conf
            if not exists(outputDir):
                print("Created directory at: ", outputDir)
                makedirs(outputDir)

            #   Construct old and new paths to file
            oldFile = inputDir + "/" + filename
            newFile = outputDir + "/" + filename

            #   Do the move
            print("Moving file from: ", oldFile, "to: ", newFile)
            rename(oldFile, newFile)
            ++moveCount

    #   Summary of process
    print("Moved a total of", moveCount, "files")

#   Called if unrecognised command line options are specified
def showUsage():
    print("organise_ensemble")
    print("Options:")
    print("-v", "\t", "Verbose listing of operations")

def getInputPath():
	path = QFileDialog.getExistingDirectory(None, "Choose input directory")
	
	return path
	
#   Entry point
def main(argv):
    #   Parse the command line
    try:
        opts, args = getopt.getopt(argv, "v",["v"])
    except getopt.GetoptError:
        showUsage()
        sys.exit(2)

    verboseOutput = False

    #   Let options based upon command line input
    for opt, arg in opts:
        if opt == '-v' or opt == '--v':
            verboseOutput = True
        
    #   Execute the function
    doFileMove(verboseOutput)


if __name__ == "__main__":
	app = QApplication(sys.argv)
	#app.exec_()
	main(sys.argv[1:])




