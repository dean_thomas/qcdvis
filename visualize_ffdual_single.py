#	Regex
import re

import operator

#		For finding files
from os import sys
from os import walk
from os import path

from math import pi

#		For graphing
import matplotlib.pyplot as plt

confRegex = "(config\.b\d+k\d+mu\d+j\d+s\d+t\d+)"

#	Our match string for matching files
ffDualFileRegex = "^ffdual_conp(\d{4}).txt"

#	Our match string for parsing the ffdual data file
ffDualRegex = "^\s*([+-]?\d*\.\d+)\s*([+-]?\d*\.\d+)\s*([+-]?\d*\.\d+)\s*([+-]?\d*\.\d+)"


#		Capture group index of the CONF variable in our regex string
CONF_INDEX = 1


#
#		\brief			Creates a new figure and returns a reference
#						to it
#
#		\since			19-10-2015
#		\author			Dean
#
def makeFigure(figureTitle, ensembleTitle, yAxisTitle):
		plt.figure(figureTitle)
		plt.title(ensembleTitle)
		plt.xlabel("cools")
		plt.ylabel(yAxisTitle)
		#plt.xlim([1, len(actionTot)])
		
		return plt.gcf()

#
#		\brief			Plots the given data on the specifei figure
#
#		\since			19-10-2015
#		\author			Dean
#
def plotData(figure, vals, dataTitle):
		plt.figure(figure.number)
		plt.plot(vals, label=dataTitle)
		plt.legend(loc='best')
		plt.legend().draggable()
		return

#
#		\brief			Parses an action file
#		\details		Parses an action file, returns the list of
#						maxima and total values as separate arrays
#
#		\since			19-10-2015
#		\author			Dean
#
def parseFfDualFile(filename):
		scalingFactor = 1.0 / (32.0 * pi**2)

		print("Doing match on file:", filename)
		ffdTot = []
		ffdMod = []
		ffdMax = []
		ffdMin = []
		
		with open(filename) as actionFile:
				for line in actionFile.readlines():
						#print("Parsing line:", line)
						isMatch = re.match(ffDualRegex, line)
						
						if isMatch:
								#print(isMatch.group())
								tempFfdTot = isMatch.group(1)
								tempFfdMod = isMatch.group(2)
								tempFfdMax = isMatch.group(3)
								tempFfdMin = isMatch.group(4)
								
								ffdTot.append(float(tempFfdTot) * scalingFactor)
								ffdMod.append(float(tempFfdMod) * scalingFactor)
								ffdMax.append(float(tempFfdMax) * scalingFactor)
								ffdMin.append(float(tempFfdMin) * scalingFactor)
								
								#print("Cools = ", cools)
								#print("Action total: ", actTot)
								#print("Action max:", actMax)

		return {'ffdTot': ffdTot, 'ffdMod' : ffdMod, 'ffdMax': ffdMax, 'ffdMin' : ffdMin }

#
#		\brief			Entry point
#
#		\since			19-10-2015
#		\author			Dean
#
def main(argv):
		if not path.exists(argv[0]):
			print("Please provide a valid configuration directory")
			print(argv[0])
			sys.exit(255)

		#confName = isConfName.group()
		confName = "config.b210k1577mu0700j02s16t32"
			
		#	Create figures for each of our graphs
		figureFfDualMinMax = makeFigure("ffdual min/max", confName, "ffdual")
		figureFfDualTot = makeFigure("ffdual tot", confName, "ffdual total")
		figureFfDualMod = makeFigure("ffdual mod", confName, "ffdual mod")

		#	Do a regex match on the filenames
		isFfDualFile = re.match(ffDualFileRegex, file)
				
		if (isFfDualFile):
			#	Extract strings from regex
			filename = isFfDualFile.group()
			conf = isFfDualFile.group(CONF_INDEX)

			inFile = inputDir + filename
			result = parseFfDualFile(inFile)
			plotData(figureFfDualMinMax, result['ffdMin'], ("conp"+conf+"_ffdMin"))
			plotData(figureFfDualMinMax, result['ffdMax'], ("conp"+conf+"_ffdMax"))
			plotData(figureFfDualTot, result['ffdTot'], ("conp"+conf))
			plotData(figureFfDualMod, result['ffdMod'], ("conp"+conf))
						
		#		Show our figures
		plt.show()
		
		#		Everything is ok
		return 0

if __name__ == "__main__":
	if len(sys.argv) == 2:
		main(sys.argv[1:])
	else:
		print("Usage: visualize_ffdual_single <configuration file>")
