#ifndef DATAMODEL_H
#define DATAMODEL_H

#include "HeightField/HeightField3D.h"
#include "HeightField/HeightField4D.h"
#include "ProgramParameters.h"
#include "Graphs/ContourTree.h"
#include "DataExploration/IsovalueBookmarkList.h"

#include "libDesignPatterns/AbstractObservable.h"

#include <fstream>
#include <iostream>
#include <limits>
#include <map>

#include <DataExploration/IsovalueBookmark.h>

#define VERBOSE_OUTPUT

using DesignPatterns::AbstractObservable;

class DataModel : public AbstractObservable
{
	private:
		std::vector<HeightField4D*> m_heightField4dArray;
		//HeightField4D *m_heightField4D = nullptr;

		//	This will become an array of open heightfields
		vector<HeightField3D *> m_heightField3dArray;

		ProgramParameters m_parameters;

		IsovalueBookmarkList m_bookmarkArray;

		std::map<HeightField4D*, std::string> m_cacheDirLookup4;


	private:

		//  Used to keep track of min/max across
		//  multiple cooling or time slices
		Real m_dataSetMaxima;
		Real m_dataSetMinima;

		bool processFilelist();
		void processSingleDataSet();

		int parseFiletype(const char *filename) const;

	public:
		~DataModel();
		DataModel();
		DataModel(const ProgramParameters parameters);

		void AddCacheDirLookup(HeightField4D* const heightField4,
							   const std::string& cacheDir);
		std::string GetCacheDir(HeightField4D* const heightField4) const;


		bool SaveWorkspace(const std::string &filename) const;

		HeightField4D *LoadData4d(const string &filename);
		HeightField3D *LoadData3d(const std::string &filename,
								  const bool& computeContourTree);
		//  NEW ACCESSOR FOR A PROCESSED RENDER MESH
		//m_dataModel->GetRenderMesh();

		//	Needed for compatibility with log collapse widget
		HeightField3D *GetHeightField3d(const unsigned long &index) const;
		unsigned long GetHeightField3dCount() const { return m_heightField3dArray.size(); }

		HeightField4D *GetHeightField4d(const unsigned long &index) const;
		unsigned long GetHeightField4dCount() const { return m_heightField4dArray.size(); }

		IsovalueBookmarkList GetBookmarkList() const { return m_bookmarkArray; }
		void AddBookmark(const IsovalueBookmark &bookmark);
		void RemoveBookmark(const IsovalueBookmark &bookmark);
		//	void MergeBookmarkArray(const vector<IsovalueBookmark*> &bookmarkArray;
};

#endif // DATAMODEL_H
