precision highp float;

varying vec3 fNormal;
varying vec3 fPosition;

uniform mat3 normalMatrix;
uniform mat4 modelViewMatrix;
uniform mat4 projectionMatrix;

//  Hold Info about our lights - maybe this could be sent from the CPU as
//  a uniform struct [see: https://www.opengl.org/wiki/Uniform_%28GLSL%29]
struct Light
{
    vec4 pos;
    vec4 diffuse;
    vec4 ambient;
    vec4 specular;
    bool active;
};

struct Material
{
    vec4 color;
    float shininess;
};

Light light;
Material material;

void main (void)
{

  // Material Color:
  material.color = vec4(0.0, 0.8, 0.0, 1.0);
  material.shininess = 30.0;

  // Silhouette Color:
  vec4 colorSilhouette = vec4(0.0, 0.0, 0.0, 1.0);

  // Lighting / Specular Color:
  light.specular = vec4(0.0, 0.8, 0.0, 1.0);
  light.pos = vec4(0.0,5.0,5.0,1.0);


  vec3 eyePos = vec3(0.0,0.0,5.0);

  vec3 Normal = normalize(normalMatrix * fNormal);
  vec3 EyeVert = normalize(eyePos - fPosition);
  vec3 LightVert = normalize(light.pos.xyz - fPosition);
  vec3 EyeLight = normalize(LightVert+EyeVert);

  // Simple Silhouette
  float sil = max(dot(Normal,EyeVert), 0.0);

  if (sil < 0.3)
    gl_FragColor = colorSilhouette;
  else
  {
      gl_FragColor = material.color;

      // Specular part
      float spec = pow(max(dot(Normal,EyeLight),0.0), material.shininess);

      if (spec < 0.2)
	gl_FragColor *= 0.8;
      else
	gl_FragColor = light.specular;

      // Diffuse part
      float diffuse = max(dot(Normal,LightVert),0.0);

      if (diffuse < 0.5)
	gl_FragColor *=0.8;
   }
}
