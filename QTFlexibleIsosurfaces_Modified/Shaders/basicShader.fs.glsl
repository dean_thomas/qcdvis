#version 130

//  From vertex shader
in vec3 fColor;

void main(void)
{
    //	Set the fragment colour to opaque red
    gl_FragColor = vec4(fColor, 1.0);
}
