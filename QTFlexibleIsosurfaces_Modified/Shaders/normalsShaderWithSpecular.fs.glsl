//  From vertex shader
varying vec3 fNormal;

varying vec3  ReflectVec;
varying vec3  ViewVec;

void main()
{
    //	Shift from -1..1 to 0..1
    vec3 color = 0.5 + (fNormal / 2.0);

    vec3 nreflect = normalize(ReflectVec);
    vec3 nview    = normalize(ViewVec);

    float spec    = max(dot(nreflect, nview), 0.0);
    spec          = pow(spec, 32.0);

    //	Set the fragment to the corrected color
    gl_FragColor = vec4(min(color + spec, 1.0), 1.0);
}
