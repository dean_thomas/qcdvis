precision highp float;

uniform mat3 normalMatrix;
uniform mat4 modelViewMatrix;
uniform mat4 projectionMatrix;

attribute vec3 position;
attribute vec3 normal;

varying vec3 fNormal;
varying vec3 fPosition;

void main()
{
    fPosition = vec3(modelViewMatrix * vec4(position, 1.0));
    fNormal = normalize(normalMatrix * normal);

    gl_Position = projectionMatrix * modelViewMatrix * vec4(position, 1.0);
}
