precision highp float;

//  From vertex shader
varying vec3 fPosition;
varying vec3 fNormal;

//  Hold Info about our lights - maybe this could be sent from the CPU as
//  a uniform struct [see: https://www.opengl.org/wiki/Uniform_%28GLSL%29]
struct Light
{
    vec4 pos;
    vec4 diffuse;
    vec4 ambient;
    vec4 specular;
    bool active;
};

//  Maximum allowable lights (should be defined at compile time for performance)
const int MAX_LIGHTS = 2;
Light light[MAX_LIGHTS];

float materialShininess = 0.8;

vec4 sceneColor = vec4(0.0, 0.0, 0.0, 1.0);

void main()
{
    //  Set up our lights
    light[0].pos = vec4(10.0, 10.0, 1.0, 1.0);
    light[0].diffuse = vec4(0.3, 0.2, 0.4, 1.0);
    light[0].ambient = vec4(0.2, 0.3, 0.4, 1.0);
    light[0].specular = vec4(0.8, 0.3, 0.5, 1.0);
    light[0].active = true;

    light[1].pos = vec4(-5.0, -3.0, 1.0, 1.0);
    light[1].diffuse = vec4(0.3, 0.0, 0.5, 1.0);
    light[1].ambient = vec4(0.5, 0.5, 0.0, 1.0);
    light[1].specular = vec4(0.0, 0.0, 0.0, 1.0);
    light[1].active = true;

    //  Will be updated by adding together individual lights
    vec4 finalColor = vec4(0.0, 0.0, 0.0, 0.0);

    //  This vector is constant for all lights
    vec3 E = normalize(-fPosition);

    //  Loop over all the lights
    for (int i = 0; i < MAX_LIGHTS; ++i)
    {
	//  Allows us to turn lights on/off
	if (light[i].active)
	{
	    vec3 L = normalize(light[i].pos - vec4(fPosition,1.0)).xyz;
	    vec3 R = normalize(-reflect(L, fNormal));

	    //  Ambient term
	    vec4 Iamb = light[i].ambient;

	    //  Diffuse term
	    vec4 Idiff = light[i].diffuse * max(dot(fNormal, L), 0.0);
	    Idiff = clamp(Idiff, 0.0, 1.0);

	    //  Specular term
	    vec4 Ispec = light[i].specular
		    * pow(max(dot(R,E),0.0),0.3 * materialShininess);
	    Ispec = clamp(Ispec, 0.0, 1.0);

	    //  Add to running totals
	    finalColor += Iamb + Idiff+ Ispec;
	}
    }

    //	Our output colour (adding in any scene lighting)
    gl_FragColor = sceneColor + finalColor;
}
