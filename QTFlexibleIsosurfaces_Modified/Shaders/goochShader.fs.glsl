//precision highp float;
//
// Fragment shader for Gooch shading
//
// Author: Randi Rost
//
// Copyright (c) 2002-2005 3Dlabs Inc. Ltd. 
//
// See 3Dlabs-License.txt for license information
//
//  Modified by Dean [24-04-2015]
//
vec3 CoolColor = vec3(0, 0, 0.6);
float DiffuseCool = 0.45;
float DiffuseWarm = 0.45;
vec3 LightPosition = vec3(0, 10, 4);
vec3 SurfaceColor = vec3(0.75, 0.5, 0.5);
vec3 WarmColor = vec3(0.6, 0.6, 0);

varying float NdotL;
varying vec3  ReflectVec;
varying vec3  ViewVec;

void main()
{
    vec3 kcool    = min(CoolColor + DiffuseCool * SurfaceColor, 1.0);
    vec3 kwarm    = min(WarmColor + DiffuseWarm * SurfaceColor, 1.0); 

    //	For Counter Clockwise Triangulations (normal)
    //vec3 kfinal   = mix(kcool, kwarm, NdotL);

    //	For Clockwise Triangulations (reverse's gradient)
    vec3 kfinal   = mix(kwarm, kcool, NdotL);

    vec3 nreflect = normalize(ReflectVec);
    vec3 nview    = normalize(ViewVec);

    float spec    = max(dot(nreflect, nview), 0.0);
    spec          = pow(spec, 32.0);

    gl_FragColor = vec4(min(kfinal + spec, 1.0),1.0);
}
