//  From vertex shader
varying vec3 fNormal;

void main()
{
    //	Shift from -1..1 to 0..1
    vec3 color = 0.5 + (fNormal / 2.0);

    //	Set the fragment to the corrected color
    gl_FragColor = vec4(color, 1.0);
}
