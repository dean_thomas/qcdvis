#version 130

//  From vertex shader
in vec3 fNormal;
in vec4 fColor;
in vec3 fReflectVec;
in vec3 fViewVec;

//  Ambient light colour
const vec4 ambient = vec4(0.1, 0.1, 0.1, 1.0);

void main()
{
    //	Diffuse surface colour is passed in from previous pipeline stage
    vec4 diffuse = fColor;

    vec3 nreflect = normalize(fReflectVec);
    vec3 nview    = normalize(fViewVec);

    float spec    = max(dot(nreflect, nview), 0.0);
    spec          = pow(spec, 32.0);

    //	Set the fragment to the corrected color
    //gl_FragColor = vec4(min(spec + ambient, 1.0) * abs(dot(nview, fNormal)), 1.0);
    gl_FragColor = min((diffuse * abs(dot(nview, fNormal))) + spec + ambient, 1.0);
}
