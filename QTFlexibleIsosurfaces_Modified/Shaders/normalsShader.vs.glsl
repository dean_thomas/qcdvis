uniform mat4 modelViewMatrix;
uniform mat4 projectionMatrix;
uniform mat3 normalMatrix;

attribute vec3 position;
attribute vec3 normal;

//  Send to fragment shader
varying vec3 fNormal;
varying vec3 fPosition;

void main()
{
    //	normals multiplied by normal matrix (transpose normal modesViewMatrix)
    //fNormal = normalize(normalMatrix * normal);

    //	raw normals from mesh
    fNormal = normal;

    //	Position of the vertex after applying the model and view matrices
    vec4 pos = modelViewMatrix * vec4(position, 1.0);
    fPosition = pos.xyz;

    //	Position of the vertex after applying our projection matrix
    gl_Position = projectionMatrix * pos;
}
