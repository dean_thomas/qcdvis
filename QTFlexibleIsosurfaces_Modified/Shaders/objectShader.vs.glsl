uniform mat4 modelViewMatrix;
uniform mat4 projectionMatrix;

attribute vec3 normal;
attribute float objectID;
uniform float objectCount;
uniform mat3 normalMatrix;

varying float f_objectID;
varying vec3 fNormal;
varying vec3 fPosition;
//varying float f_objectCount;

attribute vec3 position;
attribute vec3 color;



void main()
{
    //	Calculate the position of the vector
    vec4 pos = modelViewMatrix * vec4(position, 1.0);

    f_objectID = objectID;
    fNormal = normalize(normalMatrix * normal);

    fPosition = pos;
    //f_objectCount = objectCount;

    //	Apply projection matrix to the vector
    gl_Position = projectionMatrix * pos;
}
