//precision highp float;

varying vec3 fPosition;
varying vec3 fNormal;

vec3 lightPos = vec3(0.5, 10.0, 10.0);
vec3 lightDiffuse = vec3(1.0, 0.0, 1.0);

void main()
{
    vec3 L = normalize(lightPos - fPosition);

    //	Calculate the intensity of the diffuse component
    vec4 diffuseIntensity = vec4(lightDiffuse, 1.0) * max(dot(fNormal, L), 0.0);
    diffuseIntensity = clamp(diffuseIntensity, 0.0, 1.0);

    gl_FragColor = diffuseIntensity;
}
