#version 130

//  Same for the entire vertex array
uniform mat4 uModelViewMatrix;
uniform mat4 uProjectionMatrix;
uniform mat3 uNormalMatrix;

uniform vec3 uLightPosition;

//  Read from C++ program vertex attribute arrays (version <130 (GL3.0))
//attribute vec3 color;
//attribute vec3 position;
//attribute vec3 normal;

//  Send to fragment shader (version <130 (GL3.0))
//varying vec3 fNormal;
//varying vec3 fPosition;
//varying vec3 fColor;
//varying vec3 fReflectVec;
//varying vec3 fViewVec;

//  Read from C++ program vertex attribute arrays (version 130 (GL3.0) onwards)
in vec4 vColor;
in vec3 vPosition;
in vec3 vNormal;

//  Send to fragment shader (version 130 (GL3.0) onwards)
out vec3 fNormal;
out vec3 fPosition;
out vec4 fColor;
out vec3 fReflectVec;
out vec3 fViewVec;

//const vec3 uLightPosition = vec3(10.0, 30.0, 22.0);

void main()
{
    //	normals multiplied by normal matrix (transpose normal modesViewMatrix)
    //fNormal = normalize(normalMatrix * normal);

    //	Forward to frag shader
    fColor = vColor;

    //	raw normals from mesh
    fNormal = vNormal;

    //	Position of the vertex after applying the model and view matrices
    vec4 pos = uModelViewMatrix * vec4(vPosition, 1.0);
    fPosition = pos.xyz;

    vec3 ecPos      = vec3(uModelViewMatrix * vec4(vPosition,1.0));
    vec3 lightVec   = normalize(uLightPosition - ecPos);
    vec3 tnorm      = normalize(uNormalMatrix * vNormal);
    fReflectVec      = normalize(reflect(-lightVec, tnorm));
    fViewVec         = normalize(-ecPos);

    //	Position of the vertex after applying our projection matrix
    gl_Position = uProjectionMatrix * pos;
}
