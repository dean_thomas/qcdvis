#version 130

uniform mat4 uModelViewMatrix;
uniform mat4 uProjectionMatrix;

//  Send to fragment shader (version 130 (GL3.0) onwards)
in vec3 vPosition;
in vec3 vColor;

//  Send to fragment shader (version 130 (GL3.0) onwards)
out vec3 fColor;

void main()
{
    //	Calculate the position of the vector
    vec4 pos = uModelViewMatrix * vec4(vPosition, 1.0);

    fColor = vColor;

    //	Apply projection matrix to the vector
    gl_Position = uProjectionMatrix * pos;
}
