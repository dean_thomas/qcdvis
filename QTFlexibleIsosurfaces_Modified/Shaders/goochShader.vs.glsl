//
// Vertex shader for Gooch shading
//
// Author: Randi Rost
//
// Copyright (c) 2002-2005 3Dlabs Inc. Ltd. 
//
// See 3Dlabs-License.txt for license information
//
//  Modified by Dean [24-04-2015]
//
//precision highp float;

uniform mat3 normalMatrix;
uniform mat4 modelViewMatrix;
uniform mat4 projectionMatrix;

attribute vec3 position;
attribute vec3 normal;

varying vec3 fNormal;
varying vec3 fPosition;

vec3  LightPosition = vec3(0.0, 10.0, 4.0);

varying float NdotL;
varying vec3  ReflectVec;
varying vec3  ViewVec;

void main()
{
    vec3 ecPos      = vec3(modelViewMatrix * vec4(position,1.0));
    vec3 tnorm      = normalize(normalMatrix * normal);
    vec3 lightVec   = normalize(LightPosition - ecPos);

    ReflectVec      = normalize(reflect(-lightVec, tnorm));
    ViewVec         = normalize(-ecPos);
    
    NdotL           = (dot(lightVec, tnorm) + 1.0) * 0.5;
    
    gl_Position     = projectionMatrix * modelViewMatrix * vec4(position, 1.0);
}
