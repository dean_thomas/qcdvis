varying float f_objectID;
uniform float objectCount;

varying vec3 fNormal;
varying vec3 fPosition;

vec3 lightPos = vec3(0.5, 10.0, 10.0);
vec3 lightDiffuse = vec3(1.0, 0.0, 1.0);

vec3 minColor = vec3(0.0, 0.0, 0.15);
vec3 maxColor = vec3(0.0, 0.0, 0.85);

void main(void)
{
    vec3 L = normalize(lightPos - fPosition);

    float index = 0.0;

    if (objectCount != 0.0)
    {
	index = f_objectID / objectCount;
    }

    vec3 fcolor = vec3(0.0, 0.0, index);

    //vec3 fcolor = mix(minColor, maxColor, index);

    vec4 diffuseIntensity = vec4(fcolor,1.0) * max(dot(fNormal, L), 0.0);
    diffuseIntensity = clamp(diffuseIntensity, 0.0, 1.0);

    gl_FragColor = diffuseIntensity;

    //	Set the fragment colour to opaque red
    gl_FragColor = vec4(fcolor, 1.0);
}
