uniform mat4 modelViewMatrix;
uniform mat4 projectionMatrix;
uniform mat3 normalMatrix;

attribute vec3 position;
attribute vec3 normal;

//  Send to fragment shader
varying vec3 fNormal;
varying vec3 fPosition;

vec3  LightPosition = vec3(10.0, 30.0, 22.0);
varying vec3  ReflectVec;
varying vec3  ViewVec;

void main()
{
    //	normals multiplied by normal matrix (transpose normal modesViewMatrix)
    //fNormal = normalize(normalMatrix * normal);

    //	raw normals from mesh
    fNormal = normal;

    //	Position of the vertex after applying the model and view matrices
    vec4 pos = modelViewMatrix * vec4(position, 1.0);
    fPosition = pos.xyz;

    vec3 ecPos      = vec3(modelViewMatrix * vec4(position,1.0));
    vec3 lightVec   = normalize(LightPosition - ecPos);
    vec3 tnorm      = normalize(normalMatrix * normal);
    ReflectVec      = normalize(reflect(-lightVec, tnorm));
    ViewVec         = normalize(-ecPos);

    //	Position of the vertex after applying our projection matrix
    gl_Position = projectionMatrix * pos;
}
