#ifndef NEIGHBOURQUEUE_H
#define NEIGHBOURQUEUE_H

#include <cstdlib>
#include <vector>
#include <cassert>
#include <functional>
#include <iostream>

#include "../HeightField3D.h"


#define MAX_NEIGHBOURS 24											//	actually, it's less than this, but this covers all versions so far

//struct


class NeighbourQueue
{
	public:
		using Index3d = HeightField3D::Index3d;

		using CellList = std::vector<Index3d>;
		using NeighbourFunction = std::function<CellList(const Index3d &)>;

		std::vector<Index3d> m_neighbours;

	public:
		NeighbourQueue();

		void QueueNeighbours(const Index3d &origin, const NeighbourFunction &functor);

		Index3d GetNeighbourAt(const unsigned long &index) const;
		unsigned long GetNeighbourCount() const { return m_neighbours.size(); }
};

inline std::ostream& operator<<(std::ostream& os, const NeighbourQueue& queue)
{
	os << "Queue length: " << queue.m_neighbours.size() << std::endl;
	for (auto i = 0ul; i < queue.m_neighbours.size(); ++i)
	{
		os << "\t{ "
		   << queue.m_neighbours[i].x << ", "
		   << queue.m_neighbours[i].y << ", "
		   << queue.m_neighbours[i].z << " }"
		   << std::endl;
	}
	return os;
}

#endif // NEIGHBOURQUEUE_H
