#ifndef NEIGHBOUR_FUNCTOR_6
#define NEIGHBOUR_FUNCTOR_6

#include "NeighbourQueue.h"
#include <iostream>
#include "../HeightField3D.h"

struct NeighbourFunctor6
{
		using Index3d = HeightField3D::Index3d;

		unsigned long m_dimX, m_dimY, m_dimZ = 0;
		NeighbourQueue::CellList m_neighbours;

		NeighbourFunctor6(const unsigned long &dimX,
									   const unsigned long &dimY,
									   const unsigned long &dimZ)
		{
			m_dimX = dimX;
			m_dimY = dimY;
			m_dimZ = dimZ;
		}

		///
		/// \brief	Queues 6 immediate neighbours on the x, y or z axis only
		///			(no diagonals)
		/// \param origin
		/// \return
		/// \since		14-09-2015
		/// \author		Dean
		///
		NeighbourQueue::CellList operator()(const Index3d &origin)
		{
			using std::cout;
			using std::endl;

			assert(m_dimX > 0);
			assert(m_dimY > 0);
			assert(m_dimZ > 0);

			m_neighbours.clear();
#ifdef VERBOSE_OUTPUT
			cout << "Processing neighbours for cell(" << origin.x;
			cout << ", " << origin.y << ", " << origin.z << ")." << endl;
#endif
			queueNeighbour(origin.x-1, origin.y, origin.z);
			queueNeighbour(origin.x+1, origin.y, origin.z);

			queueNeighbour(origin.x, origin.y-1, origin.z);
			queueNeighbour(origin.x, origin.y+1, origin.z);

			queueNeighbour(origin.x, origin.y, origin.z-1);
			queueNeighbour(origin.x, origin.y, origin.z+1);
#ifdef VERBOSE_OUTPUT
			cout << "Number of neighbours: " << m_neighbours.size() << endl;
#endif
			return m_neighbours;
		}

		///
		/// \brief		Adds a cell to the list of neighbours (discarding any
		///				that exist across the boundary)
		/// \param x
		/// \param y
		/// \param z
		/// \since		14-09-2015
		/// \author		Dean
		///
		void queueNeighbour(const unsigned long x,
							const unsigned long y,
							const unsigned long z)
		{
			using std::cout;
			using std::endl;

			if (x >= m_dimX) return;
			if (y >= m_dimY) return;
			if (z >= m_dimZ) return;
#ifdef VERBOSE_OUTPUT
			cout << "Adding neighbour cell(" << x;
			cout << ", " << y << ", " << z << ")." << endl;
#endif
			m_neighbours.push_back({x, y, z});
		}

};

#endif // NEIGHBOUR_FUNCTOR_6

