#ifndef NEIGHBOUR_FUNCTOR_18P
#define NEIGHBOUR_FUNCTOR_18P

#include "../HeightField3D.h"

struct NeighbourFunctor18P
{
		using Index3d = HeightField3D::Index3d;

		unsigned long m_dimX, m_dimY, m_dimZ = 0;
		NeighbourQueue::CellList m_neighbours;

		NeighbourFunctor18P(const unsigned long &dimX,
							const unsigned long &dimY,
							const unsigned long &dimZ)
		{
			m_dimX = dimX;
			m_dimY = dimY;
			m_dimZ = dimZ;
		}

		///
		/// \brief	Queues upto 18 neighbours on the x, y or z axis
		///			(including diagonals)
		/// \param origin
		/// \return
		/// \since		14-09-2015
		/// \author		Dean
		///
		NeighbourQueue::CellList operator()(const Index3d &origin)
		{
			using std::cout;
			using std::endl;

			assert(m_dimX > 0);
			assert(m_dimY > 0);
			assert(m_dimZ > 0);

			m_neighbours.clear();
#ifdef VERBOSE_OUTPUT
			cout << "Processing neighbours for cell(" << origin.x;
			cout << ", " << origin.y << ", " << origin.z << ")." << endl;
#endif
			//	1, 2
			queueNeighbour(origin.x-1, origin.y, origin.z);
			queueNeighbour(origin.x+1, origin.y, origin.z);

			//	3, 4
			queueNeighbour(origin.x, origin.y-1, origin.z);
			queueNeighbour(origin.x, origin.y+1, origin.z);

			//	5, 6
			queueNeighbour(origin.x, origin.y, origin.z-1);
			queueNeighbour(origin.x, origin.y, origin.z+1);

			//	7, 8
			queueNeighbour(origin.x-1, origin.y-1, origin.z);
			queueNeighbour(origin.x+1, origin.y+1, origin.z);

			//	9, 10
			queueNeighbour(origin.x+1, origin.y-1, origin.z);
			queueNeighbour(origin.x-1, origin.y+1, origin.z);

			//	11, 12
			queueNeighbour(origin.x, origin.y-1, origin.z-1);
			queueNeighbour(origin.x, origin.y+1, origin.z+1);

			//	13, 14
			queueNeighbour(origin.x, origin.y+1, origin.z-1);
			queueNeighbour(origin.x, origin.y-1, origin.z+1);

			//	15, 16
			queueNeighbour(origin.x-1, origin.y, origin.z-1);
			queueNeighbour(origin.x+1, origin.y, origin.z+1);

			//	17, 18
			queueNeighbour(origin.x-1, origin.y, origin.z+1);
			queueNeighbour(origin.x+1, origin.y, origin.z-1);
#ifdef VERBOSE_OUTPUT
			cout << "Number of neighbours: " << m_neighbours.size() << endl;
#endif
			return m_neighbours;
		}

		///
		/// \brief		Adds a cell to the list of neighbours (discarding any
		///				that exist across the boundary)
		/// \param x
		/// \param y
		/// \param z
		/// \since		14-09-2015
		/// \author		Dean
		///
		void queueNeighbour(const long x,
							const long y,
							const long z)
		{
			using std::cout;
			using std::endl;

			unsigned long x1 = x % m_dimX;
			unsigned long y1 = y % m_dimY;
			unsigned long z1 = z % m_dimZ;

			assert(x1 < m_dimX);
			assert(y1 < m_dimY);
			assert(z1 < m_dimZ);
#ifdef VERBOSE_OUTPUT
			cout << "Adding neighbour cell(" << x1;
			cout << ", " << y1 << ", " << z1 << ")." << endl;
#endif
			m_neighbours.push_back({x1, y1, z1});
		}
};

#endif // NEIGHBOUR_FUNCTOR_18P

