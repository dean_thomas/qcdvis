#include "NeighbourQueue.h"

///
/// \brief		Returns the neighbour at the specified position in the queue
/// \param index
/// \return
/// \since		14-09-2015
/// \author		Dean
///
NeighbourQueue::Index3d NeighbourQueue::GetNeighbourAt(const unsigned long &index) const
{
	assert(index < m_neighbours.size());

	return m_neighbours[index];
}
///
/// \brief      Provides a generic interface for queue neighbour vertices
/// \since      21-11-2014
/// \author     Dean
/// \author     Dean: remove need to know dimensions of data [14-09-2015]
///
NeighbourQueue::NeighbourQueue()
{

}

///
/// \brief		Queues neighbour using the provide neighbour functor
/// \param origin
/// \param functor
/// \since		14-09-2015
/// \author		Dean
///
void NeighbourQueue::QueueNeighbours(const Index3d &origin,
			const NeighbourFunction &functor)
{
	m_neighbours.clear();

	CellList cells = functor(origin);

	m_neighbours = cells;
}
