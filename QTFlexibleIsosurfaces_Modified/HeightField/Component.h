//////////////////////////////////////////////////////////////////
//
//	Simplified Flexible Isosurface
//
//	Hamish Carr, 2003
//
//	Module:	Component.h
//
//	See Component.tex for details
//
//////////////////////////////////////////////////////////////////

#ifndef COMPONENT_H
#define COMPONENT_H 1

#include <iostream>
#include <stddef.h>
#include <string>
//	needed for NULL, of all things
#include "Globals/TypeDefines.h"
//	this class represents three different things:
//		i)	a component in the union-find structure when computing the join (split) tree
//		ii)	a superarc in the join (split) tree
//		iii)	additional information on seeds to be used later for generating paths
//	for i), we store a local pointer to the representative component, accessing it using Component() to perform path-compression
//	Remaining members are related to the superarc
//	Since we know that the values at the supernodes are unique, we can assume that there is a high-valued and low-valued vertex
//	this in turn allows us to avoid using half-edge representation, and to economize on storage as a result
//	We also need to be able to retrieve the superarcs at a given supernode, without wasting storage at non-super nodes:
//	Since we must already store a component pointer for each node, we can do the following:
//		i)	an ordinary (non-super) node N will point to a component which does not have N as hiEnd or loEnd: this can be detected, if needed
//		ii)	a supernode S will always point (in the join tree) to the superarc whose hiEnd is S (v.v. in the split tree)
//		iii)	we then need to associate the other superarcs at S with this initial superarc:  this is done by having circular edge-lists at each
//			supernode S:  thus Component(S) will give us the downwards superarc, followed by Component(S)->nextHi, Component(S)->nextHi->nextLo, &c.
//	Finally, for each superarc whose loEnd is S, we need to keep track of an adjacent vertex in that component:  once transferred to the contour tree, this will allow
//	us to find a path to use for interpolation

class Component													//	a component in Union-Find: dual existence as superarc in JT/ST
{
	friend std::ostream& operator <<(std::ostream&, const Component&);


	private:
		//	current (path-compressed) component in union-find structure
		//	private is to enforce path-compression
		Component* m_ufComponent = nullptr;
	public:
		operator std::string() const;

		//	pointers to the top & bottom of the superarc
		Real* m_hiEnd = nullptr;
		Real* m_loEnd = nullptr;

		//	neighbours in circular edge-lists at supernodes
		Component* m_nextHi = nullptr;
		Component* m_lastHi = nullptr;

		//	ditto for back-link
		Component* m_nextLo = nullptr;
		Component* m_lastLo = nullptr;

		//	seed for transferral to contour tree
		Real* m_seedFrom = nullptr;
		Real* m_seedTo = nullptr;

		///
		/// \brief		Default constructor (initialize everything to nullpt)
		/// \since
		/// \author		Hamish
		///
		Component()
		{
			m_ufComponent = this;

			m_nextHi = nullptr;
			m_nextLo = nullptr;
			m_lastHi = nullptr;
			m_lastLo = nullptr;

			m_seedFrom = nullptr;
			m_seedTo = nullptr;
			m_hiEnd = nullptr;
			m_loEnd = nullptr;
		}

		///
		/// \brief		Constructor to set the component to point to a point
		/// \param endHiLo
		/// \since		24-09-2015
		/// \author		Dean
		///
		Component(Real* endHiLo)
		{
			assert (endHiLo != nullptr);

			m_ufComponent = this;

			m_nextHi = this;
			m_nextLo = this;
			m_lastHi = this;
			m_lastLo = this;

			m_seedFrom = nullptr;
			m_seedTo = nullptr;

			//	with the vertex at the high end (loEnd too, to give something to draw)
			m_loEnd = endHiLo;
			m_hiEnd = endHiLo;

		}

		///
		/// \brief		Constructor to set the component to point to a point
		/// \param endHiLo
		/// \since		24-09-2015
		/// \author		Dean
		///
		Component(Real* endHiLo, Component* nextHi, Component* lastHi)
		{
			assert (endHiLo != nullptr);
			assert (nextHi != nullptr);
			assert (lastHi != nullptr);

			m_ufComponent = this;

			m_nextHi = nextHi;
			m_nextLo = this;
			m_lastHi = lastHi;
			m_lastLo = this;

			m_seedFrom = nullptr;
			m_seedTo = nullptr;

			//	with the vertex at the high end (loEnd too, to give something to draw)
			m_loEnd = endHiLo;
			m_hiEnd = endHiLo;
		}

		///
		/// \brief		path-compressing call to obtain the Union-Find component
		/// \return
		/// \since
		/// \author		Hamish
		///
		Component *component()
		{
			using namespace std;
			auto depth = 0ul;
			//	as long as it's not pointing to the final representative
			//	path-compress it
			while (m_ufComponent->m_ufComponent != m_ufComponent)
			{
				++depth;
				m_ufComponent = m_ufComponent->m_ufComponent;
			}
			cout << "Using path compression: depth = " << depth << endl;;

			//	return the one at the end
			return m_ufComponent;
		}

		void SetSeeds(Real* seedFrom, Real* seedTo)
		{
			assert (seedFrom != nullptr);
			assert (seedTo != nullptr);

			m_seedFrom = seedFrom;

			//	update the neighbour's component
			//	set the seed pointer to nbr (it's adjacent to the vertex)
			m_seedTo = seedTo;
		}

		///
		/// \brief		merges component onto another component (for jointree)
		/// \param		newUF
		/// \since		24-09-2015
		/// \author		Dean
		///
		void MergeDown(Component *jComp)
		{
			assert (jComp != nullptr);

			//	set the nextLo pointer
			m_nextLo = jComp;

			//	and the lastLo pointer (NB: jComp is downwards)
			m_lastLo = jComp->m_lastHi;

			//	reset the nextLo pointer of the old lastHi
			m_lastLo->m_nextLo = this;

			m_ufComponent = jComp;

			//	and finally, the lastHi pointer
			jComp->m_lastHi = this;
		}

		///
		/// \brief		merges component onto another component
		/// \param		newUF
		/// \since
		/// \author		Hamishoperator std::string(const Component&) const;
		///
		void MergeTo(Component *newUF)
		{
			assert (newUF != nullptr);

			m_ufComponent = newUF;
		}

};

inline Component::operator std::string() const
{
	using namespace std;
	stringstream ss;

	auto f_hexString= [](void* pointer)
	{
		stringstream a;
		a << (void const *)pointer;
		return a.str();
	};

	ss << "Component" << endl;

	ss << "\tUnion-Find component: " <<
		  (m_ufComponent == nullptr ? "nullptr" : f_hexString(m_ufComponent)) << endl;

	ss << "\tHi-end: " <<
		  (m_hiEnd == nullptr ? "nullptr" : f_hexString(m_hiEnd)) << endl;
	ss << "\tLo-end: " <<
		  (m_loEnd == nullptr ? "nullptr" : f_hexString(m_loEnd)) << endl;

	ss << "\tNext hi: " <<
		  (m_nextHi == nullptr ? "nullptr" : f_hexString(m_nextHi)) << endl;
	ss << "\tLast hi: " <<
		  (m_lastHi == nullptr ? "nullptr" : f_hexString(m_lastHi)) << endl;

	ss << "\tNext lo: " <<
		  (m_nextLo == nullptr ? "nullptr" : f_hexString(m_nextLo)) << endl;
	ss << "\tLast lo: " <<
		  (m_lastLo == nullptr ? "nullptr" : f_hexString(m_lastLo)) << endl;

	ss << "\tSeed from: " <<
		  (m_seedFrom == nullptr ? "nullptr" : f_hexString(m_seedFrom)) << endl;
	ss << "\tSeed to: " <<
		  (m_seedTo == nullptr ? "nullptr" : f_hexString(m_seedTo)) << endl;

	return ss.str();
}

inline std::ostream& operator<<(std::ostream& os, const Component& comp)
{
	using namespace std;

	os << static_cast<string>(comp);
	return os;
}

#endif
