#ifndef HEIGHTFIELD4D_H
#define HEIGHTFIELD4D_H

#include <string>
#include <Vector4.h>
#include <Vector3.h>
//#include "Globals/Globals.h"
#include <fstream>
//#include "DataModel.h"
//#include "Statistics/SuperarcDistribution.h"

class SuperarcDistribution;
class DataModel;
class HeightField3D;

#define USE_GHOST_VERTICES true

class HeightField4D
{
	using Real = float;
	public:
		using HeightField3DList = std::vector<HeightField3D*>;
		using Array4f = Storage::Vector4<Real>;
		using Array3f = Storage::Vector3<Real>;
		using Vector2f = Storage::Vector2<Real>;

		//	What structure is our data, and how can we access elements
		using ContainerType = Array4f;
		using Index4 = ContainerType::Index4;

		using size_type = size_t;

		enum class SpaceTimeDomain
		{
			DomainX,
			DomainY,
			DomainZ,
			DomainT
		};

		struct HeightField4Vertex
		{
				Index4 position;
				Real height;
		};

	private:
		//	4D array holding the height values
		ContainerType m_data;

		//	3d volumes generated by slicing the 4d data
		HeightField3DList m_yztSlices;
		HeightField3DList m_xztSlices;
		HeightField3DList m_xytSlices;
		HeightField3DList m_xyzSlices;

		//	Surface plot / histogram of number of arcs per isovalue
		SuperarcDistribution* m_superarcDistributionX = nullptr;
		SuperarcDistribution* m_superarcDistributionY = nullptr;
		SuperarcDistribution* m_superarcDistributionZ = nullptr;
		SuperarcDistribution* m_superarcDistributionT = nullptr;

		DataModel* m_dataModel = nullptr;

		bool isComment(const std::string& line) const;

		//	Properties of the simulation
		std::string m_ensembleName;
		std::string m_configurationName;
		std::string m_fieldName;
		size_type m_coolingSlice;
		size_type m_timeStep;
		float m_mu;

		//	Directory used to store sub-volumes etc
		std::string m_cacheDir;

		float m_sampleSum;

		bool m_isBusy;
		bool m_isLoaded;

		bool m_periodicBoundaries;

		//Real m_maxHeight;
		//Real m_minHeight;

		Array3f sliceYZW(const unsigned long &x) const;
		Array3f sliceXYW(const unsigned long &z) const;
		Array3f sliceXZW(const unsigned long &y) const;
		Array3f sliceXYZ(const unsigned long &w) const;

		HeightField4Vertex m_min;
		HeightField4Vertex m_max;

		void clearSliceCaches();

#ifndef NO_TOPOLOGY
		SuperarcDistribution computeSuperarcDistribution(const SpaceTimeDomain &domain);
#endif
		void sliceIntoVolumesYZT(const bool &generateGhostCells,
								 const bool &generateContourTrees);
		void sliceIntoVolumesXZT(const bool &generateGhostCells,
								 const bool &generateContourTrees);
		void sliceIntoVolumesXYT(const bool &generateGhostCells,
								 const bool &generateContourTrees);
		void sliceIntoVolumesXYZ(const bool &generateGhostCells,
								 const bool &generateContourTrees);

	public:
		HeightField4Vertex GetMaxima() const { return m_max; }
		HeightField4Vertex GetMinima() const { return m_min; }

		SuperarcDistribution *GetSuperarcDistribution(const SpaceTimeDomain &domain);

		void GenerateVolumesYZT(const std::string& cacheDir = "");
		void GenerateVolumesXZT(const std::string& cacheDir = "");
		void GenerateVolumesXYT(const std::string& cacheDir = "");
		void GenerateVolumesXYZ(const std::string& cacheDir = "");

		bool createCacheFiles(const HeightField3DList& heightfield3List,
						 const std::string& cacheDir) const;

		HeightField3D *GetVolumeYZT(const size_t& x, const bool addGhostVertices = false);
		HeightField3D *GetVolumeXZT(const size_t& y, const bool addGhostVertices = false);
		HeightField3D *GetVolumeXYT(const size_t& z, const bool addGhostVertices = false);
		HeightField3D *GetVolumeXYZ(const size_t& t, const bool addGhostVertices = false);

		Vector2f GetSliceXY(const size_t& z, const size_t& t);
		Vector2f GetSliceXZ(const size_t& y, const size_t& t);
		Vector2f GetSliceXT(const size_t& y, const size_t& z);
		Vector2f GetSliceYZ(const size_t& x, const size_t& t);
		Vector2f GetSliceYT(const size_t& x, const size_t& z);
		Vector2f GetSliceZT(const size_t& x, const size_t& y);

		HeightField3DList GetXSlices() const { return m_yztSlices; }
		HeightField3DList GetYSlices() const { return m_xztSlices; }
		HeightField3DList GetZSlices() const { return m_xytSlices; }
		HeightField3DList GetTSlices() const { return m_xyzSlices; }

		void SetEnsembleName(const std::string& value) { m_ensembleName = value; }
		std::string GetEnsembleName() const { return m_ensembleName; }

		std::string GetConfigurationName() const { return m_configurationName; }

		std::string GetFieldName() const { return m_fieldName; }

		void SetCoolingSlice(const size_type value) { m_coolingSlice = value; }
		size_type GetCoolingSlice() const { return m_coolingSlice; }

		size_type GetDimX() const { return m_data.size_x(); }
		size_type GetDimY() const { return m_data.size_y(); }
		size_type GetDimZ() const { return m_data.size_z(); }
		size_type GetDimW() const { return m_data.size_w(); }

		Real GetGlobalMinima() const { return m_min.height; }
		Real GetGlobalMaxima() const { return m_max.height; }

		HeightField4D();
		HeightField4D(DataModel* dataModel);
		HeightField4D(DataModel *dataModel, const std::string &filename);

		bool LoadFromFile(const std::string &filename);

		//void SetCacheDirectory(const string& path);
		//std::string GetCacheDir() const { return m_cacheDir; }
};

#endif // HEIGHTFIELD4D_H
