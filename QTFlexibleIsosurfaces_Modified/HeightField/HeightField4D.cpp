#include "HeightField4D.h"
#include "HeightField3D.h"
#include "../Globals/Functions.h"
#include "../Globals/Exceptions.h"
#ifndef NO_TOPOLOGY
#include "../Statistics/SuperarcDistribution.h"
#endif

#include <limits>

#define NO_CACHING

//#include
using namespace std;
using std::string;
using std::ifstream;
using std::cout;
using std::cerr;
using std::endl;
using std::boolalpha;

///
/// \brief		Slice a 2d plane from the 4d data
/// \param		z
/// \param		t
/// \return
/// \since		28-09-2015
/// \author		Dean
///
HeightField4D::Vector2f HeightField4D::GetSliceXY(const size_t& z,
												  const size_t& t)
{
	///	{ x, y, z, t } -> { x, y, z }
	HeightField3D* volXYZ = GetVolumeXYZ(t);

	///	{ x, y, z } -> { x, y }
	return volXYZ->GetPlaneXY(z);
}

///
/// \brief		Slice a 2d plane from the 4d data
/// \param		y
/// \param		t
/// \return
/// \since		28-09-2015
/// \author		Dean
///
HeightField4D::Vector2f HeightField4D::GetSliceXZ(const size_t& y,
												  const size_t& t)
{
	///	{ x, y, z, t } -> { x, y, z }
	HeightField3D* volXYZ = GetVolumeXYZ(t);

	///	{ x, y, z } -> { x, z }
	return volXYZ->GetPlaneXZ(y);
}

///
/// \brief		Slice a 2d plane from the 4d data
/// \param		y
/// \param		z
/// \return
/// \since		28-09-2015
/// \author		Dean
///
HeightField4D::Vector2f HeightField4D::GetSliceXT(const size_t& y,
												  const size_t& z)
{
	///	{ x, y, z, t } -> { x, y, t }
	HeightField3D* volXYT = GetVolumeXYZ(z);

	///	{ x, y, t } -> { x, t }
	return volXYT->GetPlaneXZ(y);
}

///
/// \brief		Slice a 2d plane from the 4d data
/// \param		x
/// \param		t
/// \return
/// \since		28-09-2015
/// \author		Dean
///
HeightField4D::Vector2f HeightField4D::GetSliceYZ(const size_t& x,
												  const size_t& t)
{
	///	{ x, y, z, t } -> { x, y, z }
	HeightField3D* volXYZ = GetVolumeXYZ(t);

	///	{ x, y, z } -> { y, z }
	return volXYZ->GetPlaneYZ(x);
}

///
/// \brief		Slice a 2d plane from the 4d data
/// \param		x
/// \param		z
/// \return
/// \since		28-09-2015
/// \author		Dean
///
HeightField4D::Vector2f HeightField4D::GetSliceYT(const size_t& x,
												  const size_t& z)
{
	///	{ x, y, z, t } -> { y, z, t }
	HeightField3D* volYZT = GetVolumeYZT(x);

	///	{ y, z, t } -> { y, t }
	return volYZT->GetPlaneXZ(z);
}

///
/// \brief		Slice a 2d plane from the 4d data
/// \param		x
/// \param		y
/// \return
/// \since		28-09-2015
/// \author		Dean
///
HeightField4D::Vector2f HeightField4D::GetSliceZT(const size_t& x,
												  const size_t& y)
{
	///	{ x, y, z, t } - > { y, z, t }
	HeightField3D* volYZT = GetVolumeYZT(x);

	///	{ y, z, t } -> { z, t }
	return volYZT->GetPlaneYZ(y);
}

///
/// \brief		Deletes the cached slices
/// \details	If the data is changed (translated etc), the slices will be
///				invalid.  Hence, we delete them and they can be regenerated
///				when needed next.
/// \since		28-09-2015
/// \author		Dean
///
void HeightField4D::clearSliceCaches()
{
	//	Delete anything that might already exist in the array
	while (!m_xztSlices.empty())
	{
		delete m_xztSlices.back();
		m_xztSlices.pop_back();
	}

	//	Delete anything that might already exist in the array
	while (!m_yztSlices.empty())
	{
		delete m_yztSlices.back();
		m_yztSlices.pop_back();
	}

	//	Delete anything that might already exist in the array
	while (!m_xytSlices.empty())
	{
		delete m_xytSlices.back();
		m_xytSlices.pop_back();
	}

	//	Delete anything that might already exist in the array
	while (!m_xyzSlices.empty())
	{
		delete m_xyzSlices.back();
		m_xyzSlices.pop_back();
	}
}

///
/// \brief		Allows us to schedule a 4D->3D slicing as required
/// \since		28-09-2015
/// \author		Dean
///
void HeightField4D::GenerateVolumesYZT(const std::string& cacheDir)
{
	if (m_yztSlices.empty())
	{
		sliceIntoVolumesYZT(USE_GHOST_VERTICES, true);

		if (cacheDir != "")
		{
			cout << "Creating cache completed without errors: " << boolalpha
				 << createCacheFiles(m_yztSlices, cacheDir) << endl;
		}
	}
}

///
/// \brief		Allows us to schedule a 4D->3D slicing as required
/// \since		28-09-2015
/// \author		Dean
///
void HeightField4D::GenerateVolumesXZT(const std::string& cacheDir)
{
	if (m_xztSlices.empty())
	{
		sliceIntoVolumesXZT(USE_GHOST_VERTICES, true);

		if (cacheDir != "")
		{
			cout << "Creating cache completed without errors: " << boolalpha
				 << createCacheFiles(m_xztSlices, cacheDir) << endl;
		}
	}
}

///
/// \brief		Allows us to schedule a 4D->3D slicing as required
/// \since		28-09-2015
/// \author		Dean
///
void HeightField4D::GenerateVolumesXYT(const std::string& cacheDir)
{
	if (m_xytSlices.empty())
	{
		sliceIntoVolumesXYT(USE_GHOST_VERTICES, true);

		if (cacheDir != "")
		{
			cout << "Creating cache completed without errors: " << boolalpha
				 << createCacheFiles(m_xytSlices, cacheDir) << endl;
		}
	}
}

///
/// \brief		Allows us to schedule a 4D->3D slicing as required
/// \since		28-09-2015
/// \author		Dean
///
void HeightField4D::GenerateVolumesXYZ(const std::string& cacheDir)
{
	if (m_xyzSlices.empty())
	{
		sliceIntoVolumesXYZ(USE_GHOST_VERTICES, true);

		if (cacheDir != "")
		{
			cout << "Creating cache completed without errors: " << boolalpha
				 << createCacheFiles(m_xyzSlices, cacheDir) << endl;
		}
	}
}

///
/// \brief		Saves each sub volume as a separate file in the specified cache
///				location
/// \param heightfield3List
/// \param cacheDir
/// \return
/// \since		19-11-2015
/// \author		Dean
///
bool HeightField4D::createCacheFiles(const HeightField3DList& heightfield3List,
									 const std::string& cacheDir) const
{
#ifndef NO_CACHING
	//	Keep track of any errors
	bool result = true;

	//	Loop over all the sub-volumes provided
	for (auto it = heightfield3List.cbegin();
		 it != heightfield3List.cend();
		 ++it)
	{
		auto heightfield3 = *it;
		auto cacheFilename = heightfield3->GetCacheFilename();

		//	We'll need to create an absolute path from the base dir
		//	and the cache filename
		stringstream absolutePath;
		absolutePath << cacheDir << "\\" << cacheFilename;

		//	Do the save and issue a message to say if it was a success
		bool result = heightfield3->SaveToFile(absolutePath.str());
		if (result)
		{
			cout << "Saved cache file: " << absolutePath.str() << endl;
		}
		else
		{
			cerr << "Failed to save file: " << absolutePath.str() << endl;
			result = false;
		}
	}
	return result;
#else
	return false;
#endif
}

///
/// \brief		Returns the specified YZT volume of the data.  If the data has
///				not	yet been cached, it will be sliced
/// \param		x
/// \return
/// \since		28-09-2015
/// \author		Dean
///
HeightField3D *HeightField4D::GetVolumeYZT(const size_t& x, const bool addGhostVertices)
{
	//	Valid x index?
	assert (x <= m_data.size_x());

	//	We still need to compute the individual slices
	if (m_yztSlices.empty())
	{
		sliceIntoVolumesYZT(addGhostVertices, true);
	}

	//	Check that the number of computed slices matches the requested slice
	assert (x < m_yztSlices.size());

	return m_yztSlices[x];
}

///
/// \brief		Returns the specified XZT plane of the data.  If the data has
///				not	yet been cached, it will be sliced
/// \param		y
/// \return
/// \since		28-09-2015
/// \author		Dean
///
HeightField3D *HeightField4D::GetVolumeXZT(const size_t& y, const bool addGhostVertices)
{
	//	Valid y index?
	assert (y <= m_data.size_y());

	//	We still need to compute the individual slices
	if (m_xztSlices.empty())
	{
		sliceIntoVolumesXZT(addGhostVertices, true);
	}

	//	Check that the number of computed slices matches the requested slice
	assert (y < m_xztSlices.size());

	return m_xztSlices[y];
}

///
/// \brief		Returns the specified XYT plane of the data.  If the data has
///				not	yet been cached, it will be sliced
/// \param		z
/// \return
/// \since		28-09-2015
/// \author		Dean
///
HeightField3D *HeightField4D::GetVolumeXYT(const size_t& z, const bool addGhostVertices)
{
	//	Valid z index?
	assert (z <= m_data.size_z());

	//	We still need to compute the individual slices
	if (m_xytSlices.empty())
	{
		sliceIntoVolumesXYT(addGhostVertices, true);
	}

	//	Check that the number of computed slices matches the requested slice
	assert (z < m_xytSlices.size());

	return m_xytSlices[z];
}

///
/// \brief		Returns the specified XYZ plane of the data.  If the data has
///				not	yet been cached, it will be sliced
/// \param		t
/// \return
/// \since		28-09-2015
/// \author		Dean
///
HeightField3D *HeightField4D::GetVolumeXYZ(const size_t& t, const bool addGhostVertices)
{
	//	Valid t index?
	assert (t <= m_data.size_w());

	//	We still need to compute the individual slices
	if (m_xyzSlices.empty())
	{
		sliceIntoVolumesXYZ(addGhostVertices, true);
	}

	//	Check that the number of computed slices matches the requested slice
	assert (t < m_xyzSlices.size());

	return m_xyzSlices[t];
}


///
/// \brief HeightField4D::SliceByX
/// \param generateContourTrees
/// \author		Dean
/// \since		09-07-2015
///
void HeightField4D::sliceIntoVolumesYZT(const bool& generateGhostCells, const bool &generateContourTrees)
{
	for (auto x = 0; x < m_data.size_x(); ++x)
	{
		HeightField3D *newHeightField3d = new HeightField3D(m_dataModel);
		newHeightField3d->LoadFromData(sliceYZW(x), generateContourTrees);

		newHeightField3d->SetEnsembleName(GetEnsembleName());
		newHeightField3d->SetFieldName(GetFieldName());
		newHeightField3d->SetConfigurationName(GetConfigurationName());
		newHeightField3d->SetFixedVariableName("x");
		newHeightField3d->SetFixedVariableValue(x + 1);
		newHeightField3d->SetAxisLabels('y', 'z', 't');
		newHeightField3d->SetCoolingSlice(GetCoolingSlice());

		//	If we need to generate ghost cells
		if (generateGhostCells)
		{
			newHeightField3d->GenerateGhostCells();
		}

		m_yztSlices.push_back(newHeightField3d);
	}
#ifndef NO_TOPOLOGY
	//	Obviously we can only compute this if the data has been segmented
	if (generateContourTrees)
	{
		delete m_superarcDistributionX;
		m_superarcDistributionX = new SuperarcDistribution(computeSuperarcDistribution(SpaceTimeDomain::DomainX));
	}
#endif
}

///
/// \brief		Returns the computed distribution as a 2d surface
/// \param domain
/// \return
/// \author		Dean
/// \since		13-07-2015
///
SuperarcDistribution *HeightField4D::GetSuperarcDistribution(const SpaceTimeDomain &domain)
{
	switch (domain)
	{
		case SpaceTimeDomain::DomainX:
			return m_superarcDistributionX;
		case SpaceTimeDomain::DomainY:
			return m_superarcDistributionY;
		case SpaceTimeDomain::DomainZ:
			return m_superarcDistributionZ;
		case SpaceTimeDomain::DomainT:
			return m_superarcDistributionT;
		default:
			fprintf(stderr, "Unknown domain!  In func: %s"
							" (file: %s, line: %ld).\n", __func__, __FILE__, __LINE__);
			fflush(stderr);
			break;
	}
}

#ifndef NO_TOPOLOGY
///
/// \brief		Computes a 2D surface of the superarc distribution by slicing
///				across a specified axis
/// \param domain
/// \author		Dean
/// \since		13-07-2015
///
SuperarcDistribution HeightField4D::computeSuperarcDistribution(const SpaceTimeDomain &domain)
{
	//	Pointers to the source and destination of the data to be computed
	//	allowing us to swap the domain as required
	SuperarcDistribution destinationDistribution;
	vector<HeightField3D*> *sourceData = nullptr;
	string axisLabel;

	//	A temporary store for the data used in the computation
	vector<ContourTree*> contourTrees;

	switch (domain)
	{
		case SpaceTimeDomain::DomainX:
			sourceData = &m_yztSlices;
			//destinationDistribution = &m_superarcDistributionX;
			axisLabel = "x slice";
			break;
		case SpaceTimeDomain::DomainY:
			sourceData = &m_xztSlices;
			//destinationDistribution = &m_superarcDistributionY;
			axisLabel = "y slice";
			break;
		case SpaceTimeDomain::DomainZ:
			sourceData = &m_xytSlices;
			//destinationDistribution = &m_superarcDistributionZ;
			axisLabel = "z slice";
			break;
		case SpaceTimeDomain::DomainT:
			sourceData = &m_xyzSlices;
			//destinationDistribution = &m_superarcDistributionT;
			axisLabel = "t slice";
			break;
	}

	//	Add the source data to the temporary vector
	for (unsigned long i = 0; i < sourceData->size(); ++i)
	{
		ContourTree *ct = (*sourceData)[i]->GetContourTree();

		if (ct != nullptr) contourTrees.push_back(ct);
	}

	//	Write to the destination
	//if (*destinationDistribution != nullptr) delete *destinationDistribution;
	//*destinationDistribution = new SuperarcDistribution;
	destinationDistribution.AddContourTrees(contourTrees, axisLabel, true);

	return destinationDistribution;
}
#endif

///
/// \brief HeightField4D::sliceByY
/// \param generateContourTrees
/// \author		Dean
/// \since		09-07-2015
///
void HeightField4D::sliceIntoVolumesXZT(const bool& generateGhostCells, const bool &generateContourTrees)
{
	for (auto y = 0; y < m_data.size_y(); ++y)
	{
		HeightField3D *newHeightField3d = new HeightField3D(m_dataModel);
		newHeightField3d->LoadFromData(sliceXZW(y), generateContourTrees);

		newHeightField3d->SetEnsembleName(GetEnsembleName());
		newHeightField3d->SetFieldName(GetFieldName());
		newHeightField3d->SetConfigurationName(GetConfigurationName());
		newHeightField3d->SetFixedVariableName("y");
		newHeightField3d->SetFixedVariableValue(y + 1);
		newHeightField3d->SetAxisLabels('x', 'z', 't');
		newHeightField3d->SetCoolingSlice(GetCoolingSlice());

		//	If we need to generate ghost cells
		if (generateGhostCells)
		{
			newHeightField3d->GenerateGhostCells();
		}

		m_xztSlices.push_back(newHeightField3d);
	}
#ifndef NO_TOPOLOGY
	//	Obviously we can only compute this if the data has been segmented
	if (generateContourTrees)
	{
		delete m_superarcDistributionY;
		m_superarcDistributionY = new SuperarcDistribution(computeSuperarcDistribution(SpaceTimeDomain::DomainY));
	}
#endif
}

///
/// \brief HeightField4D::sliceByZ
/// \param generateContourTrees
/// \author		Dean
/// \since		09-07-2015
///
void HeightField4D::sliceIntoVolumesXYT(const bool& generateGhostCells, const bool &generateContourTrees)
{
	for (auto z = 0; z < m_data.size_z(); ++z)
	{
		HeightField3D *newHeightField3d = new HeightField3D(m_dataModel);
		newHeightField3d->LoadFromData(sliceXYW(z),	generateContourTrees);

		newHeightField3d->SetEnsembleName(GetEnsembleName());
		newHeightField3d->SetFieldName(GetFieldName());
		newHeightField3d->SetConfigurationName(GetConfigurationName());
		newHeightField3d->SetFixedVariableName("z");
		newHeightField3d->SetFixedVariableValue(z + 1);
		newHeightField3d->SetAxisLabels('x', 'y', 't');
		newHeightField3d->SetCoolingSlice(GetCoolingSlice());

		//	If we need to generate ghost cells
		if (generateGhostCells)
		{
			newHeightField3d->GenerateGhostCells();
		}

		m_xytSlices.push_back(newHeightField3d);
	}
#ifndef NO_TOPOLOGY
	//	Obviously we can only compute this if the data has been segmented
	if (generateContourTrees)
	{
		delete m_superarcDistributionZ;
		m_superarcDistributionZ = new SuperarcDistribution(computeSuperarcDistribution(SpaceTimeDomain::DomainZ));
	}
#endif
}

///
/// \brief HeightField4D::sliceByT
/// \param generateContourTrees
/// \author		Dean
/// \since		09-07-2015
///
void HeightField4D::sliceIntoVolumesXYZ(const bool& generateGhostCells,
										const bool &generateContourTrees)
{
	for (auto t = 0; t < m_data.size_w(); ++t)
	{
		HeightField3D *newHeightField3d = new HeightField3D(m_dataModel);
		newHeightField3d->LoadFromData(sliceXYZ(t), generateContourTrees);

		newHeightField3d->SetEnsembleName(GetEnsembleName());
		newHeightField3d->SetFieldName(GetFieldName());
		newHeightField3d->SetConfigurationName(GetConfigurationName());
		newHeightField3d->SetFixedVariableName("t");
		newHeightField3d->SetFixedVariableValue(t + 1);
		newHeightField3d->SetAxisLabels('x', 'y', 'z');
		newHeightField3d->SetCoolingSlice(GetCoolingSlice());

		//	If we need to generate ghost cells
		if (generateGhostCells)
		{
			newHeightField3d->GenerateGhostCells();
		}

		m_xyzSlices.push_back(newHeightField3d);
	}
#ifndef NO_TOPOLOGY
	//	Obviously we can only compute this if the data has been segmented
	if (generateContourTrees)
	{
		delete m_superarcDistributionT;
		m_superarcDistributionT = new SuperarcDistribution(computeSuperarcDistribution(SpaceTimeDomain::DomainT));
	}
#endif
}

HeightField3D::Array3f HeightField4D::sliceXYW(const unsigned long &z) const
{
	//	Attempt to access a z slice outside the allowable range
	if (z >= m_data.size_z())
	{
		throw InvalidIndexException(z, __func__, __FILE__, __LINE__);
	}

	Array3f result(m_data.size_x(), m_data.size_y(), m_data.size_w());

	//	Fix z
	for (auto x = 0; x < m_data.size_x(); ++x)
	{
		for (auto y = 0; y < m_data.size_y(); ++y)
		{
			for (auto w = 0; w < m_data.size_w(); ++w)
			{
				result(x, y, w) = m_data.at(x, y, z, w);
			}
		}
	}

	return result;
}

HeightField3D::Array3f HeightField4D::sliceXZW(const unsigned long &y) const
{
	//	Attempt to access a y slice outside the allowable range
	if (y >= m_data.size_y())
	{
		throw InvalidIndexException(y, __func__, __FILE__, __LINE__);
	}

	Array3f result(m_data.size_x(), m_data.size_z(), m_data.size_w());

	//	Fix y
	for (auto x = 0; x < m_data.size_x(); ++x)
	{
		for (auto z = 0; z < m_data.size_z(); ++z)
		{
			for (auto w = 0; w < m_data.size_w(); ++w)
			{
				result(x, z, w) = m_data.at(x, y, z, w);
			}
		}
	}

	return result;
}

HeightField3D::Array3f HeightField4D::sliceYZW(const unsigned long &x) const
{
	//	Attempt to access a y slice outside the allowable range
	if (x >= m_data.size_x())
	{
		throw InvalidIndexException(x, __func__, __FILE__, __LINE__);
	}

	Array3f result(m_data.size_y(), m_data.size_z(), m_data.size_w());

	//	Fix x
	for (auto y = 0; y < m_data.size_y(); ++y)
	{
		for (auto z = 0; z < m_data.size_z(); ++z)
		{
			for (auto w = 0; w < m_data.size_w(); ++w)
			{
				result(y, z, w) = m_data.at(x, y, z, w);
			}
		}
	}

	return result;
}

HeightField3D::Array3f HeightField4D::sliceXYZ(const unsigned long &w) const
{
	//	Attempt to access a w slice outside the allowable range
	if (w >= m_data.size_w())
	{
		throw InvalidIndexException(w, __func__, __FILE__, __LINE__);
	}

	Array3f result(m_data.size_x(), m_data.size_y(), m_data.size_z());

	//	Fix w
	for (auto x = 0; x < m_data.size_x(); ++x)
	{
		for (auto y = 0; y < m_data.size_y(); ++y)
		{
			for (auto z = 0; z < m_data.size_z(); ++z)
			{
				result(x, y, z) = m_data.at(x, y, z, w);
			}
		}
	}

	return result;
}

///
/// \brief		Assesses if a string should be discarded as a comment
/// \param line
/// \return
/// \since		03-07-2015
/// \author		Dean
///
bool HeightField4D::isComment(const string &line) const
{
	//	Remove any whitespace
	string temp = line;
	temp.erase(0, temp.find_first_not_of(" \t\r\n"));

	if (temp.length() > 0)
	{
		//	Find the first character
		if (temp[0] == '#')
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	else
	{
		//	String is empty
		return true;
	}
}

///
/// \brief		Loads data from a 4D .hvol1 file
/// \since		03-07-2015
/// \author		Dean
///
bool HeightField4D::LoadFromFile(const string &filename)
{
#define SUPRESS_OUTPUT
	unsigned long dimX, dimY, dimZ, dimW;
	unsigned long expectedVertexCount, actualVertexCount;
	float expectedMinVal;
	float expectedMaxVal;

	float actualMinVal;
	float actualMaxVal;

	Index4 actualMinPos;
	Index4 actualMaxPos;

	float totVal;
	float aveVal;

	ifstream file(filename.c_str(), ifstream::in);

	if (file.is_open())
	{
		string buffer;

		//	Read in the configuration name and field variable
		//	Read our configuration name and field variable (could contain spaces)
		std::getline(file,m_ensembleName);
		std::getline(file,m_configurationName);

		//	Next two lines should contain the coolinc cycle and time step
		file >> std::skipws >> m_coolingSlice >> std::ws;

		//	Next we should have the field variable (could contain spaces)
		std::getline(file, m_fieldName);

		//	Now the field dimensions
		file >> std::skipws >> dimX >> dimY >> dimZ >> dimW;
		expectedVertexCount = dimX * dimY * dimZ * dimW;

		//	Next we should have a blank line
		std::getline(file, buffer);

		//	Next four values are computed stats)
		file >> std::skipws >> expectedMinVal >> expectedMaxVal >> totVal >> aveVal;

		if (!((dimX > 0) && (dimY > 0) && (dimZ > 0) && (dimW > 0)))
		{
			fprintf(stderr,"Loading failed - invalid dimensionality!\n");
			fflush(stderr);
			m_isBusy = false;
			return false;
		}

		m_data = Array4f(dimX, dimY, dimZ, dimW);

		//	Initialize the sorted heights
		actualMaxVal = -numeric_limits<Real>::infinity();
		actualMinVal = numeric_limits<Real>::infinity();

		//	Initialize counter
		actualVertexCount = 0;

		while ((!file.eof()) && (actualVertexCount < expectedVertexCount))
		{
			//	Reading line by line
			unsigned long x, y, z, w;
			Real value;

			//	Read the coordinates and the data
			std::getline(file, buffer);

			if (!isComment(buffer))
			{
				std::istringstream(buffer) >> x >> y >> z >> w >> value;

#if defined(VERBOSE_OUTPUT) && !defined(SUPRESS_OUTPUT)
				printf("%ld: \t %ld \t %ld \t %ld \t %ld \t %f.\n",
					   actualVertexCount, x, y, z, w, value);
#endif
				//	Set the value
				m_data.at(x,y,z,w) = value;

				//	add to the running sum
				m_sampleSum += value;

				//	update maxHeight, minHeight
				if (value > actualMaxVal)
				{
					actualMaxVal = value;
					actualMaxPos = { x, y, z, w };
				}
				if (value < actualMinVal)
				{
					actualMinVal = value;
					actualMinPos = { x, y, z, w };
				}
				//	Increment the counter
				++actualVertexCount;
			}
		}

		m_min = { actualMinPos, actualMinVal };
		m_max = { actualMaxPos, actualMaxVal};
	}
	else
	{
		fprintf(stderr, "There was a problem opening the file %s!\n"
						"In func: %s (file: %s, line: %ld).\n", filename.c_str(),
				__func__, __FILE__, __LINE__);
		fflush(stderr);
	}

	file.close();

	cout << "Minima (" << m_min.height << " identified at: ("
		 << m_min.position.x << ", " << m_min.position.y << ", "
		 << m_min.position.z << ", " << m_min.position.t << ")." << endl;
	cout << "Maxima (" << m_max.height << " identified at: ("
		 << m_max.position.x << ", " << m_max.position.y << ", "
		 << m_max.position.z << ", " << m_max.position.t << ")." << endl;
}

HeightField4D::HeightField4D()
	: HeightField4D(nullptr)
{

}

HeightField4D::HeightField4D(DataModel *dataModel)
	: HeightField4D(dataModel, "")
{
	clearSliceCaches();
}

///
/// \brief		Creates a HeightField4D and loads the specified file
/// \param dataModel
/// \param filename
/// \since		06-07-2015
/// \author		Dean
///
HeightField4D::HeightField4D(DataModel *dataModel, const string &filename)
	: m_dataModel{dataModel}
{
	if (filename != "") LoadFromFile(filename);

	m_cacheDir = "";

	clearSliceCaches();
}

///
/// \brief		Sets the location of a cache for this object
/// \param path
/// \author		Dean
/// \since		28-08-2015
///
//void HeightField4D::SetCacheDirectory(const string& path)
//{
//	m_cacheDir = path;
//cout << "Cache directory set to:" << path << "." << endl;
//}
