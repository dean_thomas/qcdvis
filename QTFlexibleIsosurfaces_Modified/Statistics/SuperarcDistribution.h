#ifndef SUPERARCDISTRIBUTION_H
#define SUPERARCDISTRIBUTION_H

#include <array>
#include "Graphs/ContourTreeSuperarc.h"
#include "Graphs/ContourTreeSupernode.h"
#include <vector>
#include <string>
#include <sstream>
#include <iostream>
#include <fstream>


class ContourTree;

class SuperarcDistribution
{

	public:
		using SuperarcIDSet = std::vector<size_t>;
		using RealArray2d = Storage::Vector2<Real>;

		struct HistogramData
		{
				ContourTree* m_contourTree;
				vector<SuperarcIDSet> m_distribution;

				HistogramData(ContourTree *contourTree)
					: m_contourTree{contourTree}
				{ }
		};

		//  Static constants / typedef's etc
		//static const unsigned long NUMBER_OF_DISCRETE_ISOVALUES = 256;
		static const bool IGNORE_BOUNDARIES = false;
	public:
		//  Constructors / Initializers / Destructors
		//SuperarcDistribution(ContourTree *contourTree, const bool
		//					 &findGlobalMinimumAndMaximum);
		//SuperarcDistribution(vector<ContourTree*> contourTrees,
		//					 const string &yAxisLabel,
		//					 const bool &findGlobalMinimumAndMaximum);

		void AddContourTree(ContourTree *contourTree,
							const bool &findGlobalMinimumAndMaximum);
		void AddContourTrees(vector<ContourTree*> contourTrees,
							 const string &yAxisLabel,
							 const bool &findGlobalMinimumAndMaximum);

		SuperarcDistribution() = default;
		SuperarcDistribution(const SuperarcDistribution&) = default;
		SuperarcDistribution& operator=(const SuperarcDistribution&) = default;

		RealArray2d ToArray2d() const;
	private:
		//  Member variables
		string m_yAxisLabel;

		unsigned long m_numberOfBins = 256;

		//	Data stored as a 2D surface distribution (with generating contour tree)
		vector<HistogramData*> m_data;

		vector<unsigned long> m_binSizes;

		//vector<SuperarcIDSet> m_superarcDistribution;
		//vector<ContourTree*> m_data;

		//	Size of the largest histogram bin
		unsigned long m_binMaximum = 0;

		double valueNormalizedToGlobalScale(const double &localValue) const;
	private:
		//  Member functions
		SuperarcIDSet calculateSetOfSuperarcIDsWithIsovalue(
				const unsigned long &index,
				const unsigned char &isovalue,
				const bool &ignoreBoundaries) const;

		void recalculateGlobalMinimumAndMaximum();

		double m_globalMinimum;
		double m_globalMaximum;
	public:
		//  Public method
		//void RecalculateDistribution(const float normalMinimum, const float normalMaximum);
		void RecalculateDistributionOfData();

	public:
		void flattenToHistogram();

		void SetNumberOfBins(const unsigned long &value);
		unsigned long GetNumberOfBins() const { return m_numberOfBins; }

		unsigned long GetMaximumBinSize() const { return m_binMaximum; }

		void SetGlobalMinimumAndMaximum(const Real &min, const Real &max);
		void SetGlobalMinimumAndMaximumFromData();

		double GetGlobalMinimum() const { return m_globalMinimum; }
		double GetGlobalMaximum() const { return m_globalMaximum; }

		//  Public accessors
		unsigned long GetNumberOfSuperarcWithIsovalue1d(const unsigned int isovalue) const;
		//SuperarcIDSet GetSuperarcIDSetWithIsovalue(const unsigned int isovalue) const;

		string ToString() const;
		string ToString(const unsigned long isovalue) const;

		string ToMatlabString() const;

		void SaveCountsToFile(const char* filename, const bool append, const float dataMin, const float dataMax) const;
};

#endif // SUPERARCDISTRIBUTION_H
