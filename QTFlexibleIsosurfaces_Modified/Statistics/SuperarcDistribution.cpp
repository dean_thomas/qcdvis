#include "SuperarcDistribution.h"
#include "Graphs/ContourTree.h"
#include <HeightField/HeightField3D.h>

using std::array;
using std::vector;
using std::string;
using std::stringstream;
using std::ofstream;

///
/// \brief		Sets the global minima and maxima to that within the data
///				and recalculates the histogram / surface plot data
/// \since		04-08-2015
/// \author		Dean
///
void SuperarcDistribution::SetGlobalMinimumAndMaximumFromData()
{
	recalculateGlobalMinimumAndMaximum();

	RecalculateDistributionOfData();
}

///
/// \brief		Sets the global minima and maxima to specifed values
///				and recalculates the histogram / surface plot data
/// \since		04-08-2015
/// \author		Dean
///
void SuperarcDistribution::SetGlobalMinimumAndMaximum(const Real &min,
													  const Real &max)
{
	assert(max > min);

	m_globalMaximum = max;
	m_globalMinimum = min;

	RecalculateDistributionOfData();
}

void SuperarcDistribution::AddContourTree(ContourTree *contourTree,
										  const bool &findGlobalMinimumAndMaximum)
{
	//	Create a record for this contour tree
	m_data.push_back(new HistogramData(contourTree));

	//	Calculate the global extremes of the data
	if (findGlobalMinimumAndMaximum) recalculateGlobalMinimumAndMaximum();

	//	And do an inital calculation
	RecalculateDistributionOfData();
}

void SuperarcDistribution::flattenToHistogram()
{
	m_binSizes.clear();
	m_binSizes.reserve(m_numberOfBins);

	for (unsigned long bin = 0; bin < m_numberOfBins; ++bin)
	{
		unsigned long totalForThisBin = 0;

		for (unsigned long d = 0; d < m_data.size(); ++d)
		{
			totalForThisBin += m_data[d]->m_distribution[bin].size();
		}

		//	Add the bin data to the 1d array
		m_binSizes.push_back(totalForThisBin);

		//	Update the largest bin (if required)
		if (totalForThisBin > m_binMaximum) m_binMaximum = totalForThisBin;
	}
}

void SuperarcDistribution::recalculateGlobalMinimumAndMaximum()
{
	m_globalMinimum = numeric_limits<double>::max();
	m_globalMaximum = numeric_limits<double>::min();

	for (unsigned long d = 0; d < m_data.size(); ++d)
	{
		HeightField3D *heightField3d
				= m_data[d]->m_contourTree->GetHeightfield();

		if (heightField3d != nullptr)
		{
			if (heightField3d->GetMaxHeight() > m_globalMaximum)
			{
				m_globalMaximum = heightField3d->GetMaxHeight();
			}

			if (heightField3d->GetMinHeight() < m_globalMinimum)
			{
				m_globalMinimum = heightField3d->GetMinHeight();
			}
		}
	}
}

void SuperarcDistribution::AddContourTrees(vector<ContourTree*> contourTrees,
										   const std::string &yAxisLabel,
										   const bool &findGlobalMinimumAndMaximum)
{
	m_yAxisLabel = yAxisLabel;

	for (unsigned long d = 0; d < contourTrees.size(); ++d)
	{
		//	Create a record for this contour tree
		m_data.push_back(new HistogramData(contourTrees[d]));
	}

	//	Calculate the global extremes of the data
	if (findGlobalMinimumAndMaximum) recalculateGlobalMinimumAndMaximum();

	//	And do an inital calculation
	RecalculateDistributionOfData();
}

/*
void SuperarcDistribution::SuperarcDistribution(ContourTree *contourTree,
										   const bool &findGlobalMinimumAndMaximum)
{
	//	Create a record for this contour tree
	m_data.push_back(new HistogramData(contourTree));

	//	Calculate the global extremes of the data
	if (findGlobalMinimumAndMaximum) recalculateGlobalMinimumAndMaximum();

	//	And do an inital calculation
	RecalculateDistributionOfData();
}
*/

double SuperarcDistribution::valueNormalizedToGlobalScale(const double &localValue) const
{
	double result;

	double globalRange = m_globalMaximum - m_globalMinimum;

	result = m_globalMinimum + (localValue * globalRange);

	//printf("Value in: %f Value out: %f.\n", localValue, result);
	//fflush(stdout);

	return result;
}

///
/// \brief      Return the ID of supernodes with specified isovalue
/// \details    Given a specified function height value (Normalised 0-255) returns
///             the set of of supernode IDs with that function height
/// \param      isovalue: an integer (0-255) representing the height
/// \return     a vector containing all relevant supernodes
/// \since      27-11-2014
/// \author     Dean
/// \author		Dean: update to use iterators [29-09-2015]
///
SuperarcDistribution::SuperarcIDSet
SuperarcDistribution::calculateSetOfSuperarcIDsWithIsovalue(
		const unsigned long &index,
		const unsigned char &isovalue,
		const bool &ignoreBoundaries) const
{
	SuperarcIDSet result;

	ContourTree *m_contourTree = m_data[index]->m_contourTree;

	//	Normalize the isovalue as a float and then
	//	normalize to the global min-max scale
	float isovaluef = isovalue / (float)m_numberOfBins;
	isovaluef = valueNormalizedToGlobalScale(isovaluef);

	for (auto it = m_contourTree->Superarcs_cbegin();
		 it != m_contourTree->Superarcs_cend();
		 ++it)
	{
		//	Extract the superarc, and it's bounding supernodes from the iterator
		ContourTreeSuperarc* superarc = *it;
		ContourTreeSupernode *topNode = superarc->GetTopSupernode();
		ContourTreeSupernode *bottomNode = superarc->GetBottomSupernode();

		assert(topNode != nullptr);
		assert(bottomNode != nullptr);

		//if (ignoreBoundaries)
		//{
		//  Edges that start or end on the isovalue
		//  are excluded from the set
		//printf("Top: %f; Bottom: %f\n", topNode.GetNormalisedHeight(m_contourTree->GetHeightfield()->minHeight, m_contourTree->GetHeightfield()->maxHeight), bottomNode.GetNormalisedHeight(m_contourTree->GetHeightfield()->minHeight, m_contourTree->GetHeightfield()->maxHeight));
		//fflush(stdout);

		double top = topNode->GetHeight();
		double bottom = bottomNode->GetHeight();

		//double normTop = ((top - m_globalMinimum) / (m_globalMaximum - m_globalMinimum));
		//double normBottom = ((bottom - m_globalMinimum) / (m_globalMaximum - m_globalMinimum));

		if ((isovaluef > bottom)
			&& (isovaluef < top))
		{
			result.push_back(superarc->GetId());
		}
		//}
		//else
		//{
		//  Edges that start or end on the isovalue
		//  are included in the set (currently ignoring
		//  the lower bound, is this right?
		//printf("Top: %f; Bottom: %f\n", topNode.GetNormalisedHeight(m_contourTree->GetHeightfield()->minHeight, m_contourTree->GetHeightfield()->maxHeight), bottomNode.GetNormalisedHeight(m_contourTree->GetHeightfield()->minHeight, m_contourTree->GetHeightfield()->maxHeight));
		//fflush(stdout);

		//if ((isovaluef > bottomNode->GetNormalisedHeight())
		//	&& (isovaluef < topNode->GetNormalisedHeight()))
		//{
		//	result.push_back(i);
		//}

	}
	return result;
}

///
/// \brief      Return the number of nodes with specified isovalue
/// \details    Given a specified function height value (Normalised 0-255) returns
///             the number of supernodes with that function height
/// \param      isovalue: an integer (0-255) representing the height
/// \return     the number of nodes that share that height
/// \author     Dean
/// \since      27-11-2014
///
void SuperarcDistribution::RecalculateDistributionOfData()
{
	for (unsigned long d = 0; d < m_data.size(); ++d)
	{
		HistogramData *dist = m_data[d];
		dist->m_distribution.clear();

		for (unsigned long isoValue = 0; isoValue < m_numberOfBins; ++isoValue)
		{
			dist->m_distribution.push_back(
						calculateSetOfSuperarcIDsWithIsovalue(d, isoValue, false));
		}
	}

	//	Flatten the 2d surface so that it is also available as a 1d histogram
	flattenToHistogram();
}

///
/// \brief		Allows the user to modify the number of bins used to create
///				the histogram (default 256)
/// \param value
/// \since		07-07-2015
/// \author		Dean
///
void SuperarcDistribution::SetNumberOfBins(const unsigned long &value)
{
	m_numberOfBins = value;

	RecalculateDistributionOfData();
}

///
/// \brief      Return the number of nodes with specified isovalue
/// \details    Given a specified function height value (Normalised 0-255) returns
///             the number of supernodes with that function height
/// \param      isovalue: an integer (0-255) representing the height
/// \return     the number of nodes that share that height
/// \author     Dean
/// \since      02-12-2014
///
//void SuperarcDistribution::RecalculateDistribution(const float normalMinimum, const float normalMaximum)
//{
//	//	Highest value in the data
//	m_max = 0;

//	for (unsigned long isoValue = 0; isoValue < m_numberOfBins; ++isoValue)
//    {
//        m_superarcDistribution[isoValue] = calculateSetOfSuperarcIDsWithIsovalue(isoValue, normalMinimum, normalMaximum, IGNORE_BOUNDARIES);


//    }
//}

///
/// \brief      Returns the whole set of sets as a string
/// \return
/// \author     Dean
/// \since      28-11-2014
///
string SuperarcDistribution::ToString() const
{
	stringstream result;

	//  Loop through each set
	for (unsigned int isovalue = 0; isovalue < m_numberOfBins-1; ++isovalue)
	{
		result << ToString(isovalue);
		result << "\n";
	}
	result << ToString(m_numberOfBins-1);

	return result.str();
}

///
/// \brief      Returns the a single set as a string
/// \return
/// \author     Dean
/// \since      28-11-2014
///
string SuperarcDistribution::ToString(const unsigned long isovalue) const
{
	stringstream result;

	for (unsigned long d = 0; d < m_data.size(); ++d)
	{
		result << "Dataset " << d << "\n\n";
		result << "Arc set for isovalue " << isovalue;
		result << " (count = " << m_data[d]->m_distribution[isovalue].size() << " )";
		result << " = { ";

		//  Loop through all nodes, if more than 1 in the set
		if (m_data[d]->m_distribution[isovalue].size() > 1)
		{
			for (unsigned long node = 0; node < m_data[d]->m_distribution[isovalue].size()-1; ++node)
			{
				result << m_data[d]->m_distribution[isovalue][node];
				result << ", ";
			}
		}

		//  Print last node without trailing comma
		if (m_data[d]->m_distribution[isovalue].size() >= 1)
			result << m_data[d]->m_distribution[isovalue][m_data[d]->m_distribution[isovalue].size()-1];

		result << " }";
	}
	return result.str();
}

///
/// \brief		Returns the data as a 2d array of floats
/// \return
/// \since		28-09-2015
/// \author		Dean
///
SuperarcDistribution::RealArray2d SuperarcDistribution::ToArray2d() const
{
	RealArray2d result(m_numberOfBins, m_data.size());

	for (unsigned long d = 0; d < m_data.size(); ++d)
	{
		for (unsigned long bin = 0; bin < m_numberOfBins; ++bin)
		{
			result.at(bin, d) = m_data[d]->m_distribution[bin].size();
		}
	}
	return result;
}

///
/// \brief		Returns the distribution as a matlab string that can be used
///				to plot a surface of the data
/// \since		09-07-2015
/// \author		Dean
/// \return
///
string SuperarcDistribution::ToMatlabString() const
{
	stringstream result;

	result << "%%\tSurface plot\n\n";
	result << "%\tOur data\n";
	result << "A = [";

	for (unsigned long d = 0; d < m_data.size(); ++d)
	{
		for (unsigned long bin = 0; bin < m_numberOfBins; ++bin)
		{
			result << " " << m_data[d]->m_distribution[bin].size();
		}

		if (d != m_data.size()-1)
		{
			result << ";";
		}
	}
	result << "];\n\n";

	result << "%\tMaximum value per axis (normalised 0-n)\n";
	result << "xMax = " << m_numberOfBins-1 << ";\n";
	result << "yMax = " << m_data.size()-1 << ";\n";
	result << "\n";

	result << "%\tActual limits per axis\n";
	result << "x0 = " << m_globalMinimum << ";\n";
	result << "x1 = " << m_globalMaximum << ";\n";
	result << "y0 = " << 0 << ";\n";
	result << "y1 = " << m_data.size()-1 << ";\n";
	result << "\n";

	result << "%\tCompute values for each axis\n";
	result << "xAxis = x0:(x1-x0)/xMax:x1;\n";
	result << "yAxis = y0:(y1-y0)/yMax:y1;\n";
	result << "\n";

	result << "%\tCreate the surface\n";
	result << "figure;\n";
	result << "surf(xAxis, yAxis, A);\n";
	result << "xlabel('isovalue');\n";
	result << "ylabel('" << m_yAxisLabel << "');\n";
	result << "zlabel('object count');\n";

	result << "%\tModify the axis labels\n";
	result << "set(gca, 'XTick', sort([x0, x1, get(gca, 'XTick')]));\n";
	result << "set(gca, 'YTick', yAxis);\n";

	return result.str();
}

///
/// \brief		Flattens the 2d surface created by multiple binned data sets
///				to a 1D histogram bim
/// \param isovalue
/// \return
/// \author		Dean
/// \since		08-07-2015
///
unsigned long SuperarcDistribution::GetNumberOfSuperarcWithIsovalue1d(const unsigned int isovalue) const
{
	return m_binSizes[isovalue];
}

//SuperarcIDSet SuperarcDistribution::GetSuperarcIDSetWithIsovalue(const unsigned int isovalue) const
//{
//	return;
//}

///
/// \brief ContourTree::SaveArcDistributionToFile
/// \param filename
/// \param scaleToData
/// \author     Dean
/// \since      02-12-2014
///
void SuperarcDistribution::SaveCountsToFile(const char* filename,
											const bool append,
											const float dataMin,
											const float dataMax) const
{
	/*
	ofstream file;

	printf("min: %f max: %f\n", dataMin, dataMax);

	if (append)
	{
		//  Append to existing file
		file.open(filename, std::ios_base::app);
	}
	else
	{
		//  Overwrite file
		file.open(filename);
	}

	if ((file == NULL) || (!file.is_open()))
	{
		return;
	}

	for (unsigned int isovalue = 0; isovalue < m_numberOfBins; ++isovalue)
	{
		//  Format is :
		//
		//  count0  count1  count2  count3  count4  ... countN-1
		file << m_distribution[isovalue].size() << "\t";
	}

	file << "\n";

	file.close();
	*/
}
