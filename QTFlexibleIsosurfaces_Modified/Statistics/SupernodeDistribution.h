#ifndef SUPERNODEDISTRIBUTION_H
#define SUPERNODEDISTRIBUTION_H

#include <array>
#include <Graphs/ContourTreeSupernode.h>
#include <vector>
#include <string>
#include <sstream>
#include "Graphs/ContourTree.h"

using std::array;
using std::vector;
using std::string;
using std::stringstream;

typedef vector<unsigned long> SupernodeIDSet;

class SupernodeDistribution
{
public:
	//  Static constants / typedef's etc
	static const unsigned long NUMBER_OF_DISCRETE_ISOVALUES = 256;

public:
	//  Constructors / Initializers / Destructors
	SupernodeDistribution(ContourTree *contourTree);

private:
	//  Member variables
	array<SupernodeIDSet, NUMBER_OF_DISCRETE_ISOVALUES> m_supernodeDistribution;
	ContourTree *m_contourTree;
private:
	//  Member functions
	SupernodeIDSet calculateSetOfNodeIDsWithIsovalue(const unsigned char isovalue, const float normalMin, const float normalMax) const;

public:
	//  Public methods
	void RecalculateDistribution(const float normalMinimum, const float normalMaximum);
	void RecalculateDistribution();
public:
	//  Public accessors
	unsigned long GetNumberOfSupernodesWithIsovalue(const unsigned int isovalue) const { return m_supernodeDistribution[isovalue].size(); }
	SupernodeIDSet GetSupernodesIDSetWithIsovalue(const unsigned int isovalue) const { return m_supernodeDistribution[isovalue]; }
	string ToString() const;
	string ToString(const unsigned long isovalue) const;

	void SaveCountsToFile(const char*filename, const bool = false, const float = 0.0f, const float = 0.0f) const ;
};

#endif // SUPERNODEDISTRIBUTION_H
