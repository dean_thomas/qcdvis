#include "SupernodeDistribution.h"

SupernodeDistribution::SupernodeDistribution(ContourTree *contourTree)
{
    m_contourTree = contourTree;

    RecalculateDistribution();
}

///
/// \brief      Return the ID of supernodes with specified isovalue
/// \details    Given a specified function height value (Normalised 0-255) returns
///             the set of of supernode IDs with that function height
/// \param      isovalue: an integer (0-255) representing the height
/// \return     a vector containing all relevant supernodes
/// \since      27-11-2014
/// \author     Dean
/// \author		Dean: update to use iterators [01-10-2015]
///
SupernodeIDSet SupernodeDistribution::calculateSetOfNodeIDsWithIsovalue(
		const unsigned char isovalue,
		const float normalMin, const float normalMax) const
{
    SupernodeIDSet result;

	for (auto it = m_contourTree->Supernodes_cbegin();
		 it != m_contourTree->Supernodes_cbegin();
		 ++it)
    {
		auto supernode = *it;

        //printf("SN id = %ld; Height = %f\n", i, GetSupernode(i).GetNormalisedHeight());
		if (isovalue == supernode->GetNormalisedHeight())
			result.push_back(supernode->GetId());
    }
    return result;
}

///
/// \brief      Return the number of nodes with specified isovalue
/// \details    Given a specified function height value (Normalised 0-255) returns
///             the number of supernodes with that function height
/// \param      isovalue: an integer (0-255) representing the height
/// \return     the number of nodes that share that height
/// \author     Dean
/// \since      02-12-2014
///
void SupernodeDistribution::RecalculateDistribution(const float normalMinimum, const float normalMaximum)
{
    for (unsigned long isoValue = 0; isoValue < NUMBER_OF_DISCRETE_ISOVALUES; ++isoValue)
    {
        m_supernodeDistribution[isoValue] = calculateSetOfNodeIDsWithIsovalue(isoValue, normalMinimum, normalMaximum);
    }
}

//
/// \brief      Return the number of nodes with specified isovalue
/// \details    Given a specified function height value (Normalised 0-255) returns
///             the number of supernodes with that function height
/// \param      isovalue: an integer (0-255) representing the height
/// \return     the number of nodes that share that height
/// \author     Dean
/// \since      27-11-2014
///
void SupernodeDistribution::RecalculateDistribution()
{
    for (unsigned long isoValue = 0; isoValue < NUMBER_OF_DISCRETE_ISOVALUES; ++isoValue)
    {
		m_supernodeDistribution[isoValue] = calculateSetOfNodeIDsWithIsovalue(
					isoValue,
					m_contourTree->GetHeightfield()->GetMinHeight(),
					m_contourTree->GetHeightfield()->GetMaxHeight());
    }
}

///
/// \brief      Returns the whole set of sets as a string
/// \return
/// \author     Dean
/// \since      28-11-2014
///
string SupernodeDistribution::ToString() const
{
    stringstream result;

    //  Loop through each set
    for (unsigned int isovalue = 0; isovalue < NUMBER_OF_DISCRETE_ISOVALUES-1; ++isovalue)
    {
        result << ToString(isovalue);
        result << "\n";
    }
    result << ToString(NUMBER_OF_DISCRETE_ISOVALUES-1);

    return result.str();
}

///
/// \brief      Returns the a single set as a string
/// \return
/// \author     Dean
/// \since      28-11-2014
///
string SupernodeDistribution::ToString(const unsigned long isovalue) const
{
    stringstream result;

    result << "Node set for isovalue " << isovalue;
    result << " (count = " << m_supernodeDistribution[isovalue].size() << " )";
    result << " = { ";

    //  Loop through all nodes, if more than 1 in the set
    if ( m_supernodeDistribution[isovalue].size() > 1)
    {
        for (unsigned long node = 0; node < m_supernodeDistribution[isovalue].size()-1; ++node)
        {
            result << m_supernodeDistribution[isovalue][node];
            result << ", ";
        }
    }

    //  Print last node without trailing comma
    if ( m_supernodeDistribution[isovalue].size() >= 1)
        result << m_supernodeDistribution[isovalue][m_supernodeDistribution[isovalue].size()-1];

    result << " }";

    return result.str();
}

///
/// \brief ContourTree::SaveNodeDistributionToFile
/// \param filename
/// \param scaleToData
///
void SupernodeDistribution::SaveCountsToFile(const char* filename, const bool, const float, const float) const
{
    FILE *file;

    file = fopen(filename, "w");


    fclose(file);
}
