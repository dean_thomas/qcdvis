#ifndef ANNOTATION_H
#define ANNOTATION_H

#include <string>
#include "Geometry/Geometry.h"
#include "Globals/TypeDefines.h"
#include "GUI/Widgets/QtCamera.h"
#include "Trees/ContourTree/Superarc.h"

using std::string;

///
/// \brief		The Annotation struct is used to store annotations made by the
///				user in order to be able to quickly recall interesting
///				phenomena.
/// \since		12-06-2015
///	\author		Dean
///
struct Annotation
{
		//	These parameters are necessary for all annotations
		Real isovalue;
		Superarc* superarc = nullptr;
		string comment;

		//	If the user intends to pin an annotation to specific object in
		//	the 3D view these parametres can also be set.  This will allow
		//	the camera to be set to a viewing angle to witness the detail
		Point3D* targetPosition = nullptr;
		QtCamera* camera = nullptr;
};

#endif // ANNOTATION

