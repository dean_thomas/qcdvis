#ifndef ISOVALUEBOOKMARK_H
#define ISOVALUEBOOKMARK_H

#include <string>
#include <sstream>
#include <iomanip>


class IsovalueBookmark
{
	private:
		float m_isovalue = 0.0f;
		std::string m_text = "Untitled bookmark";
	public:
		bool operator ==(const IsovalueBookmark &rhs) const;
		bool operator !=(const IsovalueBookmark &rhs) const;

		explicit IsovalueBookmark(const float &isovalue);

		float GetIsovalue() const { return m_isovalue; }
		std::string GetText() const { return m_text; }
		std::string ToString() const;
};

#endif // ISOVALUEBOOKMARK_H
