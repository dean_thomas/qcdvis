#include "IsovalueBookmarkList.h"

using std::string;
using std::stringstream;
using std::endl;

IsovalueBookmarkList::IsovalueBookmarkList(const IsovalueBookmarkList &rhs)
{
	//	Copy the existing bookmarks
	m_bookmarkArray.reserve(rhs.m_bookmarkArray.size());
	for(unsigned long i = 0; i < rhs.m_bookmarkArray.size(); ++i)
	{
		m_bookmarkArray.push_back(rhs.m_bookmarkArray[i]);
	}
}

IsovalueBookmark IsovalueBookmarkList::operator [](const unsigned long &index) const
{
	assert(index < m_bookmarkArray.size());

	return m_bookmarkArray[index];
}

IsovalueBookmark& IsovalueBookmarkList::operator [](const unsigned long &index)
{
	assert(index < m_bookmarkArray.size());

	return m_bookmarkArray[index];
}

IsovalueBookmarkList& IsovalueBookmarkList::operator =(const IsovalueBookmarkList &rhs)
{
	assert (this != &rhs);

	m_bookmarkArray.clear();

	//	Copy the existing bookmarks
	m_bookmarkArray.reserve(rhs.m_bookmarkArray.size());
	for(unsigned long i = 0; i < rhs.m_bookmarkArray.size(); ++i)
	{
		m_bookmarkArray.push_back(rhs.m_bookmarkArray[i]);
	}

	return *this;
}

string IsovalueBookmarkList::GetBookmarkList() const
{
	stringstream result;

	for (auto it = m_bookmarkArray.begin(); it != m_bookmarkArray.end(); ++it)
	{
		result << "isovalue=" << it->GetIsovalue() << " ";
		result << "text=" << it->GetText() << endl;
	}

	return result.str();
}

void IsovalueBookmarkList::AddBookmark(const IsovalueBookmark &bookmark)
{
	m_bookmarkArray.push_back(bookmark);
}

void IsovalueBookmarkList::RemoveBookmark(const IsovalueBookmark &bookmark)
{
	for (unsigned i = 0; i < m_bookmarkArray.size(); ++i)
	{
		//	Found the bookmark
		if (m_bookmarkArray[i] == bookmark)
		{
			//	Remove from the list
			m_bookmarkArray.erase(m_bookmarkArray.begin() + i);

			//	Assume we are done
			return;
		}
	}
}

IsovalueBookmarkList::IsovalueBookmarkList()
{

}

