#include "IsovalueBookmark.h"

using std::string;
using std::stringstream;
using std::setprecision;
using std::fixed;

IsovalueBookmark::IsovalueBookmark(const float &isovalue)
	: m_isovalue{isovalue}
{

}

bool IsovalueBookmark::operator ==(const IsovalueBookmark &rhs) const
{
	return ((rhs.m_isovalue == m_isovalue) && (rhs.m_text == m_text));
}

bool IsovalueBookmark::operator !=(const IsovalueBookmark &rhs) const
{
	return !(operator==(rhs));
}

string IsovalueBookmark::ToString() const
{
	stringstream result;

	result << m_text << " (";
	result << fixed << setprecision(4) <<  m_isovalue << ")";

	return result.str();
}

