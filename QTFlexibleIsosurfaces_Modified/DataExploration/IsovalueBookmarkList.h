#ifndef ISOVALUEBOOKMARKLIST_H
#define ISOVALUEBOOKMARKLIST_H

#include <vector>
#include <string>
#include <sstream>
#include <cassert>

#include "IsovalueBookmark.h"

class IsovalueBookmarkList
{
	private:
		std::vector<IsovalueBookmark> m_bookmarkArray;
	public:
		IsovalueBookmarkList();
		IsovalueBookmarkList(const IsovalueBookmarkList &rhs);

		void AddBookmark(const IsovalueBookmark &bookmark);
		void RemoveBookmark(const IsovalueBookmark &bookmark);

		std::string GetBookmarkList() const;

		IsovalueBookmarkList& operator =(const IsovalueBookmarkList &rhs);
		IsovalueBookmark operator [](const unsigned long &index) const;
		IsovalueBookmark& operator [](const unsigned long &index);

		unsigned long GetBookmarkCount() const { return m_bookmarkArray.size(); }
};

#endif // ISOVALUEBOOKMARKLIST_H
