#ifndef THREADQUEUE_H
#define THREADQUEUE_H

#include "Globals/Globals.h"
#include <queue>
#include <thread>
#include <functional>
#include <vector>
#include <algorithm>
#include "Graphs/ContourTreeSuperarc.h"

using std::queue;
using std::thread;
using std::function;
using std::vector;

struct ContourJob
{
		ContourTreeSuperarc& arc;
		Real isovalue;
};

class ThreadQueue
{
		typedef function<void ()> SimpleTask;

	private:
		queue<ContourJob> m_jobQueue;
		vector<thread> m_activeThreads;
		unsigned long m_maxThreads;
	private:
		ThreadQueue();
		ThreadQueue(ThreadQueue const&) = delete;
		void operator=(ThreadQueue const&) = delete;
		void startAllJobsOnSeparateThreads();
		void queueNextJob();
	public:
		void AddJobToQueue(ContourTreeSuperarc &arc, Real isovalue);
		void SerialExecute();
		void ParallelExecute(const bool &detachThreads);

		static ThreadQueue& GetInstance();
		~ThreadQueue();
};

#endif // THREADQUEUE_H
