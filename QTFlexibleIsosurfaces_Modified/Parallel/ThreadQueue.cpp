#include "ThreadQueue.h"
#include "Graphs/ContourTreeSuperarc.h"

///
/// \brief		Queue's a job for execution later
/// \param		arc: the arc to generate the contour for
/// \param		isovalue: the target isovalue for the contour
/// \since		15-05-2015
/// \author		Dean
///
void ThreadQueue::AddJobToQueue(ContourTreeSuperarc &arc, Real isovalue)
{
	//	Queue the job
	m_jobQueue.push({arc, isovalue});
}

///
/// \brief		Accessor for a Singleton object
/// \return		a reference to the job queue
///
ThreadQueue& ThreadQueue::GetInstance()
{
	static ThreadQueue instance;

	return instance;
}

ThreadQueue::ThreadQueue()
{
	m_maxThreads = thread::hardware_concurrency();
	//m_threads.reserve(m_maxThreads);
}

ThreadQueue::~ThreadQueue()
{

}

///
/// \brief		Takes all queued jobs and starts all jobs on separate threads
/// \since		15-05-2015
/// \author		Dean
///
void ThreadQueue::startAllJobsOnSeparateThreads()
{
	while (!m_jobQueue.empty())
	{
		//	Take a job from the list
		ContourJob job = m_jobQueue.front();
		m_jobQueue.pop();

		//	Create a function for the job
		function<void ()> func =
				std::bind(&ContourTreeSuperarc::generateContourWithIsovalue, job.arc, job.isovalue);

		//	Start the thread on a separate thread
		m_activeThreads.push_back(thread(func));
	}
}

///
/// \brief		Executes all tasks on the main thread
/// \since		15-05-2015
/// \author		Dean
///
void ThreadQueue::SerialExecute()
{
	while (!m_jobQueue.empty())
	{
		//	Take a job from the list
		ContourJob job = m_jobQueue.front();
		m_jobQueue.pop();

		//	Create a function for the job
		function<void ()> func =
				std::bind(&ContourTreeSuperarc::generateContourWithIsovalue, job.arc, job.isovalue);

		//	Execute on the main thread
		func();
	}
}

///
/// \brief		Executes all tasks across multiple threads
/// \param		detachThreads: if true we will not wait for the threads to
///				finish before we exit the function
/// \since		15-05-2015
/// \author		Dean
///
void ThreadQueue::ParallelExecute(const bool &detachThreads)
{
	//	Empty the queue and start all jobs
	startAllJobsOnSeparateThreads();

	//	Loop through the active threads (some could have already terminated)
	for_each(m_activeThreads.begin(), m_activeThreads.end(),
			 [&detachThreads](thread &t) {
		if (detachThreads)
		{
			//	Tell the thread to become a daemon thread
			t.detach();
		}
		else
		{
			//	We will wait until all threads have completed
			t.join();
		}
	});
}

