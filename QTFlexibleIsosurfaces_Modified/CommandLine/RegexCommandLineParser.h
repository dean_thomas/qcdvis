#ifndef REGEX_COMMAND_LINE_PARSER_H
#define REGEX_COMMAND_LINE_PARSER_H

#include <regex>
#include <string>
#include <vector>
#include <cassert>
#include <utility>
#include <map>

class RegexCommandLineParser
{
	public:
		using FilenameString = std::string;
		using ExtString = std::string;
		using FileList = std::map<const FilenameString, ExtString>;
		struct CommandLineOptions
		{
				bool noGui;

				std::string stdOutFile;
				std::string stdErrFile;

				FileList inFiles;
				FileList outFiles;

				bool calculateStats;

				char segmentationAxis;
		};

	private:
		//	The command line string stored as a vector of strings
		std::vector<std::string> m_argumentArray;

		//	As Regexes can be expensive to setup we'll set them up once and
		//	retain them should they need re-use
		std::regex m_noGuiRegex;
		std::regex m_infileRegex;
		std::regex m_segmentationRegex;
		std::regex m_statisticsRegex;
		std::regex m_outfileRegex;
		std::regex m_stdOutFileRegex;
		std::regex m_stdErrFileRegex;

		bool checkNoGui() const;
		bool checkCalculateStats() const;
		FileList checkInFile() const;
		FileList checkOutFile() const;
		std::string checkStdOutFile() const;
		std::string checkStdErrFile() const;
	public:
		RegexCommandLineParser();

		CommandLineOptions operator()(const int argc, char* argv[]);


};

#endif // REGEXCOMMANDLINEPARSER_H
