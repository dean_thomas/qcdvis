#include "RegexCommandLineParser.h"
#include <string>
#include <iostream>

using std::string;
using std::regex;
using std::cout;
using std::endl;
using std::pair;

///
/// \brief		RegexCommandLineParser::RegexCommandLineParser
/// \since		27-10-2015
/// \author		Dean
///
RegexCommandLineParser::RegexCommandLineParser()
{
	//	Run without GUI
	m_noGuiRegex = "nogui";

	//	Output file (using windows style path)
	//		example: 'X:\some where123\file.dat'
	//		example: 'c:\\hello\\goodbye\\abc123.txt'
	m_infileRegex = R"(infile="{0,1}([A-Za-z]:(?=\\{0,1}|\/{0,1})[a-zA-Z0-9_\.\s(?=\\{0,1}|\/)]*\.([a-zA-Z0-9]{0,4}))\"{0,1})";

	//	Segment file into 3D volumes
	m_segmentationRegex = "seg3D=([x|y|z|t])";

	//	Compute statistics
	m_statisticsRegex = "stats";

	//	Output file (using windows style path)
	//		example: 'X:\some where123\file.dat'
	//		example: 'c:\\hello\\goodbye\\abc123.txt'
	m_outfileRegex = R"(outfile="{0,1}([A-Za-z]:(?=\\{0,1}|\/{0,1})[a-zA-Z0-9_\.\s(?=\\{0,1}|\/)]*\.([a-zA-Z0-9]{0,4}))\"{0,1})";

	//	Output file (using windows style path)
	//		example: 'X:\some where123\file.dat'
	//		example: 'c:\\hello\\goodbye\\abc123.txt'
	m_stdOutFileRegex = "stdout=(\\\"{0,1}[A-Za-z]:\\\\{1,2}"
						"[a-zA-Z0-9\\s\\\\]*"
						"\\."
						"([a-zA-Z0-9]{0,4})\\\"{0,1})";

	//	Output file (using windows style path)
	//		example: 'X:\some where123\file.dat'
	//		example: 'c:\\hello\\goodbye\\abc123.txt'
	m_stdErrFileRegex = "stderr=(\\\"{0,1}[A-Za-z]:\\\\{1,2}"
						"[a-zA-Z0-9\\s\\\\]*"
						"\\."
						"([a-zA-Z0-9]{0,4})\\\"{0,1})";
}

///
/// \brief		RegexCommandLineParser::operator ()
/// \param		argc
/// \param		argv
/// \return
/// \since		27-10-2015
/// \author		Dean
///
RegexCommandLineParser::CommandLineOptions RegexCommandLineParser::operator()(
		const int argc, char* argv[])
{
	assert (argc >= 0);

	//	Parse the rhar* array into a string vector
	m_argumentArray.reserve(argc);
	for (auto it = &argv[0]; it != &argv[argc]; ++it)
	{
		//cout << *it << endl;

		m_argumentArray.push_back(*it);
	}

	auto noGui = checkNoGui();
	auto stdOutFile = checkStdOutFile();
	auto stdErrFile = checkStdErrFile();
	auto inFileLise = checkInFile();
	auto outFileList = checkOutFile();
	auto calcStats = checkCalculateStats();
	auto domain = 't';

	return { noGui, stdOutFile, stdErrFile, inFileLise,
				outFileList, calcStats, domain };
}

///
/// \brief RegexCommandLineParser::checkInFile
/// \return
/// \since		27-10-2015
/// \author		Dean
///
RegexCommandLineParser::FileList RegexCommandLineParser::checkInFile() const
{
	FileList result;

	for (const auto &arg : m_argumentArray)
	{
		std::smatch regexMatches;

		if (std::regex_match(arg, regexMatches, m_infileRegex))
		{
			//	We should match four groups
			assert(regexMatches.size() == 3);

			//	Matches:
			//		0.	Whole string (including switch)
			//		1.	Full path to file
			//		2.	file extension only
			//auto fullMatch = regexMatches[0];
			auto fullPath = regexMatches[1];
			auto extension = regexMatches[2];

			result[fullPath] = extension;
		}
	}
	return result;
}

///
/// \brief RegexCommandLineParser::checkOutFile
/// \return
/// \since		27-10-2015
/// \author		Dean
///
RegexCommandLineParser::FileList RegexCommandLineParser::checkOutFile() const
{
	FileList result;

	for (const auto &arg : m_argumentArray)
	{
		std::smatch regexMatches;

		if (std::regex_match(arg, regexMatches, m_outfileRegex))
		{
			//	We should match four groups
			assert(regexMatches.size() == 3);

			//	Matches:
			//		0.	Whole string (including switch)
			//		1.	Full path to file
			//		2.	file extension only
			//auto fullMatch = regexMatches[0];
			auto fullPath = regexMatches[1];
			auto extension = regexMatches[2];

			result[fullPath] = extension;
		}
	}
	return result;
}

///
/// \brief		RegexCommandLineParser::checkNoGui
/// \return		<tt>bool</tt>: true if the 'nogui' command-line flag was
///				detected
/// \since		27-10-2015
/// \author		Dean
///
bool RegexCommandLineParser::checkNoGui() const
{
	std::smatch regexMatches;

	for (auto it = m_argumentArray.cbegin(); it != m_argumentArray.cend(); ++it)
	{
		if (std::regex_match(*it, regexMatches, m_noGuiRegex))
			return true;
	}
	return false;
}

///
/// \brief		RegexCommandLineParser::checkCalculateStats
/// \return		<tt>bool</tt>: true if the 'stats' command-line flag was
///				detected
/// \since		27-10-2015
/// \author		Dean
///
bool RegexCommandLineParser::checkCalculateStats() const
{
	std::smatch regexMatches;

	for (auto it = m_argumentArray.cbegin(); it != m_argumentArray.cend(); ++it)
	{
		if (std::regex_match(*it, regexMatches, m_statisticsRegex))
			return true;
	}
	return false;
}

///
/// \brief RegexCommandLineParser::checkStdOutFile
/// \return
/// \since		27-10-2015
/// \author		Dean
///
string RegexCommandLineParser::checkStdOutFile() const
{
	for (const auto &arg : m_argumentArray)
	{
		std::smatch regexMatches;

		if (std::regex_match(arg, regexMatches, m_stdOutFileRegex))
		{
			//	We should match four groups
			assert(regexMatches.size() == 3);

			//	Matches:
			//		0.	Whole string (including switch)
			//		1.	Full path to file
			//		2.	file extension only
			//auto fullMatch = regexMatches[0];
			auto fullPath = regexMatches[1];
			//auto extension = regexMatches[2];

			return fullPath;
		}
	}
	return "";
}

///
/// \brief		RegexCommandLineParser::checkStdErrFile
/// \return
/// \since		27-10-2015
/// \author		Dean
///
string RegexCommandLineParser::checkStdErrFile() const
{
	for (const auto &arg : m_argumentArray)
	{
		std::smatch regexMatches;

		if (std::regex_match(arg, regexMatches, m_stdErrFileRegex))
		{
			//	We should match four groups
			assert(regexMatches.size() == 3);

			//	Matches:
			//		0.	Whole string (including switch)
			//		1.	Full path to file
			//		2.	file extension only
			//auto fullMatch = regexMatches[0];
			auto fullPath = regexMatches[1];
			//auto extension = regexMatches[2];

			return fullPath;
		}
	}
	return "";
}
