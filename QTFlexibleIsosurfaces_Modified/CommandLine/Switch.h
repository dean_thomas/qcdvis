#pragma once

//#include "ArgumentParser.h"
#include <vector>
#include <string>
#include <sstream>

using std::vector;
using std::string;
using std::stringstream;

namespace CommandLine
{
	///	@brief:		Provides a generic interface for parsed command
	///				line arguments.
	///	@author:	Dean
	///	@since:		13-11-2014
	///
	///	@todo:		...
	class ISwitch
	{
	protected:
		string _identifer;

	public:
		string Identifier() { return _identifer; };

		virtual string ToString() const = 0;
	};

	///	@brief:		Provides a storage container for command line
	///				arguments without parameters.
	///	@author:	Dean
	///	@since:		13-11-2014
	///
	///	@todo:		...
	class Switch : public ISwitch
	{
	public:
		Switch(string identifer)
		{
			_identifer = identifer;
		}

		virtual string ToString() const
		{
			return _identifer;
		}
	};

	///	@brief:		Provides a storage container for command line
	///				arguments with parameters as strings.
	///	@author:	Dean
	///	@since:		13-11-2014
	///
	///	@todo:		...
	class SwitchWithParameters : public ISwitch
	{
		vector<string> _parameters;
	public:
		SwitchWithParameters(string identifier, vector<string> parameters)
		{
			_identifer = identifier;
			_parameters = parameters;
		}

		vector<string> GetParameters() const { return _parameters; }

		virtual string ToString() const
		{
			stringstream result;

			result << _identifer << " : { ";

			for (unsigned int i = 0; i < _parameters.size() - 1; ++i)
			{
				result << _parameters[i] << ", ";
			}
			result << _parameters[_parameters.size() - 1] << " }";

			return result.str();
		}
	};
	/*
	class SwitchWithIntegerParameters : public ISwitch
	{
	public:
	vector<long> _parameters;

	SwitchWithIntegerParameters(string identifier, vector<long> parameters)
	{
	_identifer = identifier;
	_parameters = parameters;
	}

	virtual string ToString() const { return "Do this later!"; }
	};

	class SwitchWithFloatParameters : public ISwitch
	{
	vector<float> _parameters;

	SwitchWithFloatParameters(string identifier, vector<float> parameters)
	{
	_identifer = identifier;
	_parameters = parameters;
	}

	virtual string ToString() const { return "Do this later!"; }
	};
	*/
}
