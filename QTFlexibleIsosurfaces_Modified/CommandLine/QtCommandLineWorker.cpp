#include "QtCommandLineWorker.h"
#include "Statistics/SuperarcDistribution.h"
#include <QFile>
#include <iostream>

using std::cout;
using std::cerr;
using std::endl;

///
/// \brief QtCommandLineWorker::QtCommandLineWorker
/// \param dataModel
/// \param options
/// \since		27-10-2015
/// \author		Dean
///
QtCommandLineWorker::QtCommandLineWorker(DataModel& dataModel,
										 const CommandLineOptions& options)
	: m_dataModel{dataModel}
{
	m_commandLineOptions = options;
}

///
/// \brief QtCommandLineWorker::DoWork
/// \return
/// \since		27-10-2015
/// \author		Dean
///
bool QtCommandLineWorker::DoWork()
{
	FileList inputFileQueue = m_commandLineOptions.inFiles;

	for (auto it = inputFileQueue.cbegin(); it != inputFileQueue.cend(); ++it)
	{
		auto filename = it->first;
		auto extension = it->second;

		cout << "Working on file: " << filename << endl;

		QFile inputFile(QString::fromStdString(filename));

		if (inputFile.exists())
		{
			//	We'll only work with 3D volumes for now...
			assert(extension == "vol1");

			auto result = process3dFile(filename);
			if (result)
			{
				cout << "Analysis of file '" << filename << "' is complete.";
				cout << endl;
			}
			else
			{
				cerr << "Analysis of file '" << filename << "' failed.";
				cerr << endl;
			}
		}
		else
		{
			cerr << "Unable to load file: " << filename << "." << endl;
		}
	}
	return true;
}

///
/// \brief QtCommandLineWorker::process3dFile
/// \param filename
/// \return
/// \since		27-10-2015
/// \author		Dean
///
bool QtCommandLineWorker::process3dFile(const std::string &filename)
{
	cout << "Processing file: " << filename << endl;

	//	Load the heighfield and compute the contour tree
	auto heightField3D = m_dataModel.LoadData3d(filename, true);
	assert (heightField3D != nullptr);

	//	Get a local copy of the contour tree
	auto contourTree = heightField3D->GetContourTree();
	assert (contourTree != nullptr);

	//	Calculate the superarc distribution
	auto superarcDistribution = contourTree->GetSuperarcDistribution();
	assert (superarcDistribution != nullptr);

	cout << superarcDistribution->ToString() << endl;

	return true;
}
