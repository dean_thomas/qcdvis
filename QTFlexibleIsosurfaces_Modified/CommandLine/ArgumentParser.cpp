#include "ArgumentParser.h"

namespace CommandLine
{
	///	@brief:		
	///	@author:	Dean
	///	@since:		17-11-2014
	///
	///	@details:	...
	///	
	///	@param:		param1...
	///	@param:		param2...
	///	
	///	@return:	...
	///
	///	@throws:	...
	///
	///	@todo:		...
	vector<string> ArgumentParser::GetSwitchParameters(const string identifier) const
	{
		for (unsigned int i = 0; i < m_processedSwitches.size(); ++i)
		{
			if (m_processedSwitches[i]->Identifier() == identifier)
			{
				//if (typeid(_processedSwitches[i]).name() == "SwitchWithParameters")
					//return (dynamic_cast<SwitchWithParameters*>(_processedSwitches[i])->GetParameters());
				SwitchWithParameters* theSwitch = dynamic_cast<SwitchWithParameters*>(m_processedSwitches[i]);

				if (theSwitch != nullptr)
				{
					return theSwitch->GetParameters();
				}
			}
		}
		return vector<string>();
	}

	///	@brief:		Returns an ordered list of switches supplied to the program
	///				without additional information such as parameters.
	///	@author:	Dean
	///	@since:		13-11-2014
	///
	///	@details:	...
	///	
	///	@param:		param1...
	///	@param:		param2...
	///	
	///	@return:	...
	///
	///	@throws:	...
	///
	///	@todo:		...
	vector<string> ArgumentParser::OrderedArgumentListWithoutParameters() const
	{
		vector<string> result;

		for (unsigned int i = 0; i < m_processedSwitches.size(); ++i)
		{
			result.push_back(m_processedSwitches[i]->Identifier());
		}
		return result;
	}

	///	@brief:		Processes the raw command line arguments in to a
	///				more convenient form.
	///	@author:	Dean
	///	@since:		13-11-2014
	///
	///	@details:	...
	///	
	///	@param:		param1...
	///	@param:		param2...
	///	
	///	@return:	...
	///
	///	@throws:	...
	///
	///	@todo:		...
	void ArgumentParser::processArguments(int argc, char* argv[])
	{
		for (int i = 1; i < argc; ++i)
		{
			//cout << argv[i] << endl;
			m_rawSwitches.push_back(argv[i]);
		}

		makeUniform();

		for (unsigned int i = 0; i < m_rawSwitches.size(); ++i)
		{
			processSwitch(m_rawSwitches[i]);
		}
	}

	///	@brief:		Strips variations from the supplied command line
	///				argument to make further processing easier.
	///	@author:	Dean
	///	@since:		13-11-2014
	///
	///	@details:	...
	///	
	///	@param:		param1...
	///	@param:		param2...
	///	
	///	@return:	...
	///
	///	@throws:	...
	///
	///	@todo:		...
	void ArgumentParser::makeUniform()
	{
		for (unsigned int i = 0; i < m_rawSwitches.size(); ++i)
		{
			//if (!isPotentialFilename(_rawSwitches[i]))
			//{
				removeLeadTokens(m_rawSwitches[i]);
				modifiyParameterSpecifiers(m_rawSwitches[i]);
			//}

			//cout << _rawSwitches[i] << endl;
		}
	}

	///	@brief:		Check if the specified switch has been set by
	///				the user
	///	@author:	Dean
	///	@since:		13-11-2014
	///
	///	@details:	...
	///	
	///	@param:		param1...
	///	@param:		param2...
	///	
	///	@return:	boolean, true if the switch appears in any form
	///				on the command line
	///
	///	@throws:	...
	///
	///	@todo:		
	bool ArgumentParser::IsSet(string identifier, const bool caseSensitive) const
	{
		for (unsigned int i = 0; i < m_rawSwitches.size(); ++i)
		{
			string testSwitch = m_processedSwitches[i]->Identifier();

			if (!caseSensitive)
			{
				//	Case doesn't matter, so test both as lower
				std::transform(identifier.begin(), identifier.end(), identifier.begin(), tolower);
				std::transform(testSwitch.begin(), testSwitch.end(), testSwitch.begin(), tolower);
			}

			if (testSwitch == identifier)
			{
				return true;
			}
		}
		return false;
	}

	///	@brief:		Removes any leading slashes or dashes from a given string
	///	@author:	Dean
	///	@since:		13-11-2014
	///
	///	@details:	...
	///	
	///	@param:		param1...
	///	@param:		param2...
	///	
	///	@return:	...
	///
	///	@throws:	...
	///
	///	@todo:		Apply fi for slashes in filenames
	///				Apply fix for negative numbers
	void ArgumentParser::removeLeadTokens(std::string& in)
	{
		char REMOVEABLE_CHARS[3] = {'-', '\\', '/'};

		unsigned int firstAlphaNumericChar = in.find_first_not_of(REMOVEABLE_CHARS, 0);

		in.erase(0, firstAlphaNumericChar);
	}

	///	@brief:		Replaces assignment tokens with equals sign
	///	@author:	Dean
	///	@since:		13-11-2014
	///
	///	@details:	...
	///	
	///	@param:		param1...
	///	@param:		param2...
	///	
	///	@return:	...
	///
	///	@throws:	...
	///
	///	@todo:		...
	void ArgumentParser::modifiyParameterSpecifiers(std::string& in)
	{
		const char* VALUE_SEPARATORS = ":=";

		//	First we make sure the first colon is are replaced with an equals
		std::size_t found = in.find_first_of(VALUE_SEPARATORS);
		if (found != std::string::npos)
		{
			in[found] = '=';
		}
	}

	///	@brief:		Parses the uniform formatted operator and add's
	///				it to the list of processed switches.
	///	@author:	Dean
	///	@since:		13-11-2014
	///
	///	@details:	...
	///	
	///	@param:		param1...
	///	@param:		param2...
	///	
	///	@return:	...
	///
	///	@throws:	...
	///
	///	@todo:		...
	void ArgumentParser::processSwitch(std::string& in)
	{
		unsigned int equalsPos = in.find_first_of("=");

		if (equalsPos == std::string::npos)
		{
			//	Just a simple on/off switch
			m_processedSwitches.push_back(new Switch(in));
		}
		else
		{
			//	Get the identifier string
			string identifier = in.substr(0, equalsPos);

			//	Parameters specified
			//	These need to be parsed
			string parameters = in.substr(equalsPos+1);
			vector<string> parameterElements = split(parameters, ',');

			//	Add multiple parameters
			m_processedSwitches.push_back(new SwitchWithParameters(identifier, parameterElements));
		}
	}

	///	@brief:		...
	///	@author:	Dean
	///	@since:		13-11-2014
	///
	///	@details:	...
	///	
	///	@param:		param1...
	///	@param:		param2...
	///	
	///	@return:	...
	///
	///	@throws:	...
	///
	///	@todo:		...
	ArgumentParser::ArgumentParser(int argc, char* argv[])
	{
		//cout << argv[0] << endl;

		m_commandLine = argv[0];

		//	Ignore the executable argument
		m_switchCount = argc - 1;

		processArguments(argc, argv);
	}


	///	@brief:		...
	///	@author:	Dean
	///	@since:		13-11-2014
	///
	///	@details:	...
	///	
	///	@param:		param1...
	///	@param:		param2...
	///	
	///	@return:	...
	///
	///	@throws:	...
	///
	///	@todo:		...
	ArgumentParser::~ArgumentParser()
	{
		//	Tidy up after ourselves
		while (!m_processedSwitches.empty())
		{
			delete m_processedSwitches.back();
			m_processedSwitches.pop_back();
		}
	}

	///	@brief:		...
	///	@author:	Dean
	///	@since:		13-11-2014
	///
	///	@details:	...
	///	
	///	@param:		param1...
	///	@param:		param2...
	///	
	///	@return:	...
	///
	///	@throws:	...
	///
	///	@todo:		...
	string ArgumentParser::ToString() const
	{
		std::stringstream ss;

		for (unsigned int i = 0; i < m_processedSwitches.size(); ++i)
		{
			ss << m_processedSwitches[i]->ToString();
			ss << "\n";
		}

		return ss.str();
	}

	///	@brief:		...
	///	@author:	Dean
	///	@since:		13-11-2014
	///
	///	@details:	...
	///	
	///	@param:		param1...
	///	@param:		param2...
	///	
	///	@return:	...
	///
	///	@throws:	...
	///
	///	@todo:		...
	vector<string> &ArgumentParser::split(const string &s, char delim, vector<string> &elems) const
	{
		std::stringstream ss(s);
		string item;
		while (std::getline(ss, item, delim))
		{
			elems.push_back(item);
		}
		return elems;
	}

	///	@brief:		...
	///	@author:	Dean
	///	@since:		13-11-2014
	///
	///	@details:	...
	///	
	///	@param:		param1...
	///	@param:		param2...
	///	
	///	@return:	...
	///
	///	@throws:	...
	///
	///	@todo:		...
	vector<string> ArgumentParser::split(const string &s, char delim) const
	{
		vector<string> elems;
		split(s, delim, elems);
		return elems;
	}
}
