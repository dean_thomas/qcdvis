#ifndef QTCOMMANDLINEWORKER_H
#define QTCOMMANDLINEWORKER_H

#include "DataModel.h"
#include "RegexCommandLineParser.h"
#include <vector>
#include <string>

class QtCommandLineWorker
{
	public:
		using CommandLineOptions = RegexCommandLineParser::CommandLineOptions;
		using FilePath = std::string;
		using FileList = RegexCommandLineParser::FileList;
	private:
		DataModel m_dataModel;
		CommandLineOptions m_commandLineOptions;
	public:
		bool process3dFile(const std::string &filename);

		QtCommandLineWorker(DataModel& dataModel, const CommandLineOptions& options);
		bool DoWork();
};

#endif // QTCOMMANDLINEWORKER_H
