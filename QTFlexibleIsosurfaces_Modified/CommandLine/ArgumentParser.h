#pragma once

#include <cstdlib>
#include <cstdio>
#include <vector>
#include <tuple>
#include <string>
#include <algorithm>
#include <sstream>
#include <fstream>

#include "Switch.h"

using std::string;
using std::vector;
using std::ifstream;

namespace CommandLine
{
	///	@file:		...
	///
	///	@brief:		...
	///	@author:	Dean
	///	@since:		13-11-2014
	///
	///	@details:	...
	///	
	///	@copyright:	....
	///
	///	@todo:		...
	class ArgumentParser
	{
	private:
		string m_commandLine;
		unsigned long m_switchCount;

		vector<string> m_rawSwitches;
		vector<ISwitch*> m_processedSwitches;

		void processArguments(int argc, char* argv[]);

		void removeLeadTokens(std::string& in);
		void makeUniform();
		void modifiyParameterSpecifiers(std::string& in);
		void processSwitch(std::string& in);

		vector<string> &split(const string &s, char delim, vector<string> &elems) const;
		vector<string> split(const string &s, char delim) const;
	public:
		string ToString() const;

		bool IsSet(string identifier, const bool caseSensitive = false) const;
		vector<string> GetSwitchParameters(const string identifier) const;

		unsigned int SwitchCount() const { return m_switchCount; }

		vector<string> OrderedArgumentListWithoutParameters() const;
		string ArgumentWithoutParameters(const unsigned int index) { return m_processedSwitches[index]->Identifier(); }

		ArgumentParser(int argc, char* argv[]);
		~ArgumentParser();
	};
}
