//	IsoSurface II
//	Hamish Carr, 2000

//	Superarc.h:		class representing superarcs in CT
//	Palatino-12; 5 n-spaces/tab
#ifndef SUPERARC_H
#define SUPERARC_H

#include <limits>

//#include "Globals/TypeDefines.h"

#include "SuperarcBase.h"

#include "Contour/Generation/ContourFactory.h"
#include "Contour/Storage//MeshCache.h"

#include <thread>

//	this class represents superarcs in the contour tree, including seed information
//	Since we know that the values at the supernodes are unique, we can assume that there is a high-valued and low-valued vertex
//	this in turn allows us to avoid using half-edge representation, and to economize on storage as a result
//	WARNING: CHANGE TO PREVIOUS FUNCTIONALITY
//	This version keeps separate lists at supernodes for up-arcs and down-arcs
//	Thus, the circular lists between pointers are only for the up- or down- arcs
//	i.e. at each supernode, nextHi/lastHi cycles around the down arcs
//	nextLo/lastLo cycles around the up arcs

class ContourTreeSupernode;
class ContourTree;

//	a Superarc in Union-Find: dual existence as superarc in JT/ST
class ContourTreeSuperarc : public SuperarcBase<ContourTreeSupernode>
{
	public:
	//	Type aliases
	using size_type = size_t;

	private:
	MeshCache *m_meshCache = nullptr;

	ContourTree *m_contourTree = nullptr;

	public:

	enum class Persistence_Measure
	{
		NODE_COUNT,
		SAMPLE_COUNT
	};

	//	HACK:	should be private! [27-05-2015]
	ContourFactory *m_isosurfaceGenerator = nullptr;
	private:
	//	Contour Mesh at current isovalue
	//	Contains a copy of the mesh at the user selected height (or null
	//	if out of range)


	//	flags for various conditions
	bool m_isHighlighted;
	//bool m_isSelected;

	bool m_isValid;

	//	Keep - for now
	bool m_isActive;

	//bool m_isRestorable;
	//bool m_isSuppressed;
	//bool m_isDirty;
	bool m_isOnBackQueue;
	bool m_wasActiveBeforePruning;


	public:
	Contour* RequestContour(const Real& isovalue, const bool addToQueue = false);
	Contour* RequestContourAtMidpoint(const bool addToQueue = false);
	Real GetIsovalueAtMidpoint() const;

	void generateContourWithIsovalue(const Real& isovalue);

	//	"pointers" for circular lists at top and bottom
	ContourTreeSuperarc* m_nextUpArc = nullptr;
	ContourTreeSuperarc* m_lastUpArc = nullptr;												//	circular list of up-arcs
	ContourTreeSuperarc* m_nextDownArc = nullptr;
	ContourTreeSuperarc* m_lastDownArc = nullptr;												//	circular list of down-arcs

	//	computed approximation of volume: node count
	size_type nodesOnArc;
	size_type nodesThisSideOfTop;	
	size_type nodesThisSideOfBottom;

	//	for counting the size of
	//	this one assumes (for now) that the samples are actually short integers (16-bit) not floats
	Real sampleSumTop, sampleSumBottom, sampleSumOnArc;

	//	for use in collapses: tracks the two arcs that merge to make a new arc
	ContourTreeSuperarc* m_collapseTopSuperarc = nullptr;
	ContourTreeSuperarc* m_collapseBottomSuperarc = nullptr;

	//	seeds for isosurface generation
	Real *seedFromHi, *seedFromLo, *seedToHi, *seedToLo;						//	seeds for generating isosurfaces

	float GetArcLength() const override;

	ContourTreeSuperarc(ContourTree* contourTree,
		 const size_type id,
		 ContourTreeSupernode *topNode,
		 ContourTreeSupernode *bottomNode);

	~ContourTreeSuperarc();

	//	Routines to set seeds
	void SetHighSeed(Real *SeedFromHi, Real *SeedToHi);					//	set high end seed
	void SetLowSeed(Real *SeedFromLo, Real *SeedToLo);						//	set low end seed

	//	computed approximation of volume: node count
	float GetApproxVolumeUp(const Persistence_Measure measure) const;
	float GetApproxVolumeDown(const Persistence_Measure measure) const;
	float GetApproxVolume(const Persistence_Measure measure) const;

	bool IsPresentAtIsovalue(const Real& value) const;

	void SetValid(const bool value);
	bool IsValid() const { return m_isValid; }

	void SetActive(const bool value);
	bool IsActive() const { return m_isActive; }

	//void SetSelected(const bool value, const bool raiseNotification);
	//bool IsSelected() const { return m_isSelected; }

	void SetHidden(const bool value) override;

	void SetOnBackQueue(const bool value) { m_isOnBackQueue = value; }
	bool IsOnBackQueue() const { return m_isOnBackQueue; }

	void SetWasActiveBeforePruning(const bool value);
	bool WasActiveBeforePruning() const { return m_wasActiveBeforePruning; }

	//ContourTreeSupernode* GetTopSupernode() const { return m_topSupernode; }
	//ContourTreeSupernode* GetBottomSupernode() const { return m_bottomSupernode; }
	//Real GetMinimumIsovalue() const { return m_bottom

	void integratePropertiesOverArc();

	float GetIntegratedSurfaceArea(const size_t sample_count);
	float GetIntegratedVolume(const size_t sample_count);

	ContourTree *GetContourTree() const { return m_contourTree; }

	std::string ToString() const;

	bool operator==(const ContourTreeSuperarc& rhs)
	{
		return this == &rhs;
	}

	bool operator<(const ContourTreeSuperarc& rhs)
	{
		return this < &rhs;
	}
}; // end of class Superarc

#endif
