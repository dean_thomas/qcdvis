#include "SplitTree.h"
#include "Graphs/ContourTree.h"


#ifdef _MSC_VER
#define __PRETTY_FUNCTION__ __FUNCSIG__
#endif

bool SplitTree::SaveToDotFile(const std::string basefilename) const
{
	return MergeTree::saveToDotFile(basefilename, true);
}

///
/// \brief		Copy constructor
/// \since		21-09-2015
/// \author		Dean
///
SplitTree::SplitTree(const SplitTree& rhs)
{
	cout << __PRETTY_FUNCTION__ << endl;

	//	From MergeTree
	m_componentArray3D = rhs.m_componentArray3D;
	m_arcLookupArray3D = rhs.m_arcLookupArray3D;
	m_rootComponent = rhs.m_rootComponent;

	//	From SplitTree
	m_joinTree = rhs.m_joinTree;
	supernodeCount = rhs.supernodeCount;
	m_superNodesInContourTree = rhs.m_superNodesInContourTree;
}

///
/// \brief		Clones a SplitTree
/// \since		21-09-2015
/// \author		Dean
///
SplitTree* SplitTree::Clone() const
{
	return new SplitTree(*this);
}

///
/// \brief      SplitTree::SplitTree
/// \param      heightfield
/// \since:     20-11-2014
/// \author:    Dean
///
SplitTree::SplitTree(HeightField3D *heightfield, JoinTree *joinTree)
	: MergeTree(heightfield)
{
	//  We need to keep a reference of the jointree for
	//  *READ-ONLY* access later on.
	m_joinTree = joinTree;

	m_superNodesInContourTree = 0;

	//  Run the construction algorithm
	createTree();

#ifdef SAVE_MERGE_TREES
	saveToDotFile("/home/dean/Desktop/split_tree.dot", true);
#endif

}

/*
///
/// \brief:     Prints out the Union-Find info for the split sweep
/// \since:     20-11-2014
/// \author:    Hamish
///
void SplitTree::printSplitComponents()
{

#ifdef ENABLE_PRINT_SPLIT_COMPONENTS
	for (unsigned long i = 0; i < _heightfield->xDim; i++)
	{
		for (unsigned long j = 0; j < _heightfield->yDim; j++)
		{
			for (unsigned long k = 0; k < _heightfield->zDim; k++)
			{
				printf("(%2ld, %2ld, %2ld): %8.5f: pointer: %8X\n", i, j, k, _heightfield->height(i, j, k), (unsigned int)componentArray3D(i, j, k));

#ifndef USE_SHORT_COMPONENTS
				if (componentArray3D(i, j, k) == NULL)
					continue;

				printf("hiEnd: ");
				PrintPointerIndices(componentArray3D(i, j, k)->hiEnd);
				printf(", ");
				printf("loEnd: ");
				PrintPointerIndices(componentArray3D(i, j, k)->loEnd);
				printf(", ");
				printf("seed: ");
				PrintPointerIndices(componentArray3D(i, j, k)->seedFrom);
				printf("\n");
				printf(", pointer: %8X, nextHi: %8X, lastHi: %8X, nextLo: %8X, lastLo: %8X\n",
					   (unsigned int)componentArray3D(i, j, k),
					   splitComponent(i, j, k)->nextHi,
					   splitComponent(i, j, k)->lastHi,
					   splitComponent(i, j, k)->nextLo,
					   splitComponent(i, j, k)->lastLo);
#endif
			} // ijk
			printf("\n");
		} // ij
		printf("\n");
	}
#endif
#undef ENABLE_PRINT_SPLIT_COMPONENTS
	//#endif
}
*/

/*
///
/// \brief:     Prints out the split tree (i.e. split components, with duplicates suppressed)
/// \since:     20-11-2014
/// \author:    Hamish
///
void SplitTree::printSplitTree()
{
	/*
	for (unsigned long i = 0; i < _heightfield->XDim(); i++)
	{
		for (unsigned long j = 0; j < _heightfield->YDim(); j++)
		{
			for (unsigned long k = 0; k < _heightfield->ZDim(); k++)
			{
				if (componentArray3D(i, j, k) == NULL)
					continue;
				if (componentArray3D(i, j, k)->loEnd != &_heightfield->height(i, j, k))
					continue;

				printf("hiEnd: ");
				printPointerIndices(componentArray3D(i, j, k)->hiEnd);
				printf(", ");

				printf("loEnd: ");
				printPointerIndices(componentArray3D(i, j, k)->loEnd);
				printf(", ");
				//				printf("seed: "); PrintPointerIndices(splitComponent(i, j, k)->seed); printf("\n");
				printf("pointer: %8X, nextHi: %8X, lastHi: %8X, nextLo: %8X, lastLo: %8X\n",  (unsigned int)componentArray3D(i, j, k),
					   (unsigned int)componentArray3D(i, j, k)->nextHi, (unsigned int)componentArray3D(i, j, k)->lastHi,
					   (unsigned int)componentArray3D(i, j, k)->nextLo, (unsigned int)componentArray3D(i, j, k)->lastLo);
			}
		}
	}

}
*/

/*
///
/// \brief:     Prints out the pointer indices
/// \param:     Vertex
/// \since:     20-11-2014
/// \author:    Hamish
///
void SplitTree::printPointerIndices(Real *Vertex)
{
	unsigned long x, y, z;

	if (Vertex == NULL)
		printf("NULL");
	else if (Vertex == &(MINUS_INFINITY))
		printf("minus infinity");
	else if (Vertex == &(PLUS_INFINITY))
		printf("plus infinity");
	else
	{
		// should be valid pointer
		m_heightfield3D->ComputeIndex(Vertex, x, y, z);
		printf("(%2ld, %2ld, %2ld): %6.2f", x, y, z, *Vertex);
	}
}
*/

///
/// \brief:     Does the up sweep to create the split tree
/// \author:    Hamish
/// \since:     21-11-2014
///
void SplitTree::createTree()
{
	//#define DEBUG_SPLIT_TREE
	//	CreateSplitTree() needs to do the following:
	//	A.	create the array holding the split components, all initialized to NULL
	//	B.	do a loop in upwards order, adding each vertex to the union-find:
	//		i.	queue up the neighbours of the vertex
	//		ii.	loop through all neighbours of the vertex:
	//			a.	if the neighbour is higher than the vertex, skip
	//			b.	if the neighbour belongs to a different component than the vertex
	//				1.	and the vertex has no component yet, add the vertex to the component
	//				2.	the vertex is a component, but is not yet a split, make it a split
	//				3.	the vertex is a split, merge the additional component
	//		iii.	if the vertex still has no (NULL) component, start a new one
	//	C.	tie off the final component to plus infinity
	size_t x, y, z;														//	coordinates of any given vertex
	unsigned long nbrX, nbrY, nbrZ;												//	coordinates of a neighbour
	size_t splitX, splitY, splitZ;											//	for computing bottom end of split arc
	unsigned long nNbrComponents;												//	# of neighbouring components
	Component *sComp;													//	local pointer to split component of vertex

#ifdef DEBUG_SPLIT_TREE
	printf("Starting computation of split tree\n");
#endif

	//	A.	create the array holding the split components, all initialized to NULL
	//gettimeofday(&thatTime, NULL);
	//timingBuffer += sprintf(timingBuffer, "Starting computation of split tree at %ld: %ld\n", thatTime.tv_sec % 1000, thatTime.tv_usec);
	//flushTimingBuffer();

	m_componentArray3D= ComponentArray3D(m_heightfield3D->GetBaseDimX(),
										 m_heightfield3D->GetBaseDimY(),
										 m_heightfield3D->GetBaseDimZ());								//	construct the array of split components

	m_arcLookupArray3D = ArcLookupArray3D(m_heightfield3D->GetBaseDimX(),
										  m_heightfield3D->GetBaseDimY(),
										  m_heightfield3D->GetBaseDimZ());

	supernodeCount = 0;												//	reset count of split supernodes

	//	B.	do a loop in upwards order, adding each vertex to the union-find:
	for (long vertexIndex = 0; vertexIndex < m_heightfield3D->GetVertexCount(); vertexIndex++)							//	walk from low end of array
	{ // i
		//		i.	queue up the neighbours of the vertex
		m_heightfield3D->ComputeIndex(m_heightfield3D->GetSortedHeight(vertexIndex), x, y, z);						//	compute the indices of the vertex

#ifdef DEBUG_SPLIT_TREE
		printf("Examining vertex (%ld, %ld, %ld)\n", x, y, z);					//	print out the number of the vertex
		fflush(stdout);

		//  Print the union-find array
		printf("%s\n", componentArrayToString(false).c_str());

		printf("################################\n");
		fflush(stdout);
#endif

		NeighbourQueue neighbourQueue;

		NeighbourFunctor18 neighbourFunctor(m_heightfield3D->GetBaseDimX(),
											m_heightfield3D->GetBaseDimY(),
											m_heightfield3D->GetBaseDimZ());

		neighbourQueue.QueueNeighbours({x, y, z}, neighbourFunctor);									//	and set up the neighbour queue
		nNbrComponents = 0;												//	reset the count of neighbouring components
		//		ii.	loop through all neighbours of the vertex:
		sComp = m_componentArray3D(x, y, z);									//	retrieve a local pointer to the split component of the vertex
		for (long neighbour = 0; neighbour < neighbourQueue.GetNeighbourCount(); neighbour++)			//	loop through neighbours
		{
			Index3d temp = neighbourQueue.GetNeighbourAt(neighbour);

			nbrX = temp.x;
			nbrY = temp.y;
			nbrZ = temp.z;

			//			a.	if the neighbour is higher than the vertex, skip
			if (compareHeight(&(m_heightfield3D->GetHeightAt(nbrX, nbrY, nbrZ)), m_heightfield3D->GetSortedHeight(vertexIndex)) > 0)
				//	if neighbour sorts higher
				continue;												//	skip to end of loop
#ifdef CHECK_INTERNAL_CONDITIONS
			if (compareHeight(&(_heightfield->GetHeightAt(nbrX, nbrY, nbrZ)), _heightfield->m_sortedHeights[vertex]) == 0)
				//	this should *NEVER* be allowed to happen: a vertex cannot be its own neighbour
			{ printf("compareHeight says that %p (%f) and %p (%f) have identical heights\n", neighbour, *neighbour, m_sortedHeights[vertex], *(m_sortedHeights[vertex])); BAILOUT; }
#endif
			Component *nbrComponent = m_componentArray3D(nbrX, nbrY, nbrZ)->component();
			//	retrieve the neighbour's component from union-find
			//			b.	if the neighbour belongs to a different component than the vertex
			if (sComp != nbrComponent)									//	compare components
			{ // B.ii.b.
				//				1.	and the vertex has no component yet, add the vertex to the component
				if (nNbrComponents == 0)									//	this is the first neighbouring component
				{ // B.ii.b.1.
					m_componentArray3D(x, y, z) = sComp = nbrComponent;			//	set the vertex to point to it
					nbrComponent->m_seedTo = (&m_heightfield3D->GetHeightAt(nbrX, nbrY, nbrZ));		//	set the seed pointer to nbr (it's adjacent to the vertex)
					nbrComponent->m_seedFrom = m_heightfield3D->GetSortedHeight(vertexIndex);				//	but set both ends . . .
					m_heightfield3D->ComputeIndex(nbrComponent->m_hiEnd, splitX, splitY, splitZ);

					m_arcLookupArray3D(splitX, splitY, splitZ) = m_heightfield3D->GetSortedHeight(vertexIndex);	//	and add the corresponding split arc to the tree

					nbrComponent->m_hiEnd = m_heightfield3D->GetSortedHeight(vertexIndex);				//	and update the hi end (for drawing purposes)
					nNbrComponents++;									//	increment the number of neighbouring components
				} // B.ii.b.1.
				//				2.	the vertex is a component, but is not yet a split, make it a split
				else if (nNbrComponents == 1)								//	this is the second neighbouring component
				{ // B.ii.b.2.
					//	create a new component
					Component *newComponent = new Component;				//	create a new component
					newComponent->m_loEnd = m_heightfield3D->GetSortedHeight(vertexIndex);				//	with the vertex at the low end
					newComponent->m_hiEnd = m_heightfield3D->GetSortedHeight(vertexIndex);				//	make the high end something predictable
					newComponent->m_nextLo = sComp;							//	set its nextLo pointer
					newComponent->m_lastLo = nbrComponent;					//	and its lastLo pointer
					//	update the neighbour's component
					nbrComponent->m_seedTo = (&m_heightfield3D->GetHeightAt(nbrX, nbrY, nbrZ));		//	set the seed pointer to nbr (it's adjacent to the vertex)
					nbrComponent->m_seedFrom = m_heightfield3D->GetSortedHeight(vertexIndex);				//	but set both ends . . .
					m_heightfield3D->ComputeIndex(nbrComponent->m_hiEnd, splitX, splitY, splitZ);

					m_arcLookupArray3D(splitX, splitY, splitZ) = m_heightfield3D->GetSortedHeight(vertexIndex);	//	and add the corresponding split arc to the tree

					//	make the vertex the high end of the neighbour's component
					nbrComponent->m_hiEnd = m_heightfield3D->GetSortedHeight(vertexIndex);
					nbrComponent->m_nextHi = newComponent;					//	set the nextHi pointer
					nbrComponent->m_lastHi = sComp;							//	and the lastHi pointer


					//	perform the merge
					nbrComponent->MergeTo(newComponent);					//	perform the actual merge
					sComp->MergeTo(newComponent);							//	for both old components
					//	update the existing pointer for the vertex' component

					//	make the vertex the high end of the component it was assigned to
					sComp->m_hiEnd = m_heightfield3D->GetSortedHeight(vertexIndex);
					sComp->m_nextHi = nbrComponent;							//	set the nextHi pointer
					sComp->m_lastHi = newComponent;							//	and the lastHi pointer
					//	and reset the split component
					m_componentArray3D(x, y, z) = sComp = newComponent;
					nNbrComponents++;									//	increment the number of neighbouring components
					//	check to see if its a join supernode
					if (m_joinTree->m_componentArray3D(x, y, z)->m_hiEnd != &m_heightfield3D->GetHeightAt(x, y, z))		//	if it isn't a join supernode
					{
						m_superNodesInContourTree++;									//	increment the number of supernodes
					}
					supernodeCount++;
				} // B.ii.b.2.
				//				3.	the vertex is a split, merge the additional component
				else // i.e. nNbrComponents > 1
				{ // B.ii.b.3
					//	update the neighbour's component
					nbrComponent->m_seedTo = (&m_heightfield3D->GetHeightAt(nbrX, nbrY, nbrZ));		//	set the seed pointer to nbr (it's adjacent to the vertex)
					nbrComponent->m_seedFrom = m_heightfield3D->GetSortedHeight(vertexIndex);				//	but set both ends . . .
					m_heightfield3D->ComputeIndex(nbrComponent->m_hiEnd, splitX, splitY, splitZ);

					m_arcLookupArray3D(splitX, splitY, splitZ) = m_heightfield3D->GetSortedHeight(vertexIndex);	//	and add the corresponding split arc to the tree

					nbrComponent->m_hiEnd = m_heightfield3D->GetSortedHeight(vertexIndex);				//	make the vertex the high end of the neighbour's component
					nbrComponent->m_nextHi = sComp;							//	set the nextHi pointer
					nbrComponent->m_lastHi = sComp->m_lastLo;					//	and the lastHi pointer (NB: sComp is upwards)
					nbrComponent->m_lastHi->m_nextHi = nbrComponent;				//	reset the nextHi pointer of the old lastLo
					//	perform the merge
					nbrComponent->MergeTo(sComp);							//	perform the actual merge
					sComp->m_lastLo = nbrComponent;							//	and finally, the lastLo pointer
				} // B.ii.b.3
			} // B.ii.b.
		} // neighbour
		//		iii.	if the vertex still has no (NULL) component, start a new one
		if (sComp == NULL)												//	no neighbours found:  must be a local maximum
		{ // B.iii.
			m_componentArray3D(x, y, z) = sComp = new Component;					//	create a new component
			sComp->m_loEnd = m_heightfield3D->GetSortedHeight(vertexIndex);								//	with the vertex at the low end
			sComp->m_hiEnd = m_heightfield3D->GetSortedHeight(vertexIndex);								//	and, just for good measure, at the high end
			sComp->m_nextLo = sComp;										//	set circular links to itself
			sComp->m_lastLo = sComp;										//	in both directions
			sComp->m_nextHi = sComp->m_lastHi = sComp;							//	tie off the upper end

			//	check to see if its a join supernode
			if (m_joinTree->m_componentArray3D(x, y, z)->m_hiEnd != &m_heightfield3D->GetHeightAt(x, y, z))
			{//	if it isn't a join supernode
				m_superNodesInContourTree++;
			}
			//	increment the number of supernodes
			supernodeCount++;
		} // B.iii.
	} // i
	//	C.	tie off the final component to plus infinity
	m_heightfield3D->ComputeIndex(sComp->m_hiEnd, splitX, splitY, splitZ);

	m_arcLookupArray3D(splitX, splitY, splitZ) = (Real*)&PLUS_INFINITY;						//	and add the corresponding split arc to the tree

	sComp->m_hiEnd = (Real*)&PLUS_INFINITY;										//	set the hiEnd to plus infinity
	sComp->m_nextHi = sComp->m_lastHi = sComp;									//	set the circular list pointers
	m_rootComponent = sComp;													//	and store a pointer to it

#ifdef DEBUG_SPLIT_TREE
	printf("%s\n", componentArrayToString(false).c_str());
	fflush(stdout);
#endif
}
