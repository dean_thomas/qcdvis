#ifndef JOINTREE_H
#define JOINTREE_H

#include <Vector3.h>
#include "../HeightField/Component.h"
#include "../HeightField/HeightField3D.h"
#include <iostream>
#include "HeightField/HeightSort.h"
#include "../HeightField/NeighbourQueue/NeighbourQueue.h"
#include "MergeTree.h"
#include "Globals/Globals.h"
#include "HeightField/NeighbourQueue/NeighbourFunctor6.h"
#include "HeightField/NeighbourQueue/NeighbourFunctor6P.h"

using std::cout;
using std::endl;

class JoinTree : public MergeTree
{
	using Index3d = HeightField3D::Index3d;

	unsigned long m_superNodesInContourTree;

	virtual void createTree();

	JoinTree(const JoinTree& rhs);

	Component* createNewComponent(const size_t &vertexIndex,
								  Component *jComp,
								  Component *nbrComponent,
								  const Index3d &neighbourIndex3d);
	void mergeComponent(Component *nbrComponent,
						const Index3d &nbrIndex3d,
						const size_t &vertexIndex1d, Component *jComp);

public:
	virtual bool SaveToDotFile(const std::string basefilename) const override;

	unsigned long supernodeCount;

	JoinTree* Clone() const;

	JoinTree();
	JoinTree(HeightField3D *heightfield);

	unsigned long SuperNodesInContourTree() const { return m_superNodesInContourTree; }
};

#endif // JOINTREE_H
