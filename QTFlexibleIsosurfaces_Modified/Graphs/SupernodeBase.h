///
///	\brief		Class that provides a common base for supernodes in a
///				topological	tree structure such as the contour tree or reeb
///				graph
///	\author		Dean
///	\since		04-02-2016
///
#ifndef SUPERNODEBASE_H
#define SUPERNODEBASE_H

#include <cstdlib>
#include <functional>
#include <sstream>
#include <tuple>
#include <set>

//template <typename T>
class SupernodeBase
{
	public:
		using size_type = size_t;
		using T = float;
		using vec_3 = std::tuple<size_t, size_t, size_t>;
	protected:
		size_type m_id;

		bool m_isSelected = false;
		bool m_isHighlighted = false;
		bool m_isHidden = false;

		vec_3 m_position;
	public:

		vec_3 GetPosition() const { return m_position; }

		virtual bool IsSelected() const { return m_isSelected; }
		virtual void SetSelected(const bool value)
		{
			m_isSelected = value;
		}

		virtual bool IsHighlighted() const { return m_isHighlighted; }
		virtual void SetHighlighted(const bool value) { m_isHighlighted = value; }

		virtual bool IsHidden() const { return m_isHidden; }
		virtual void SetHidden(const bool value) { m_isHidden = value; }


		friend std::ostream& operator<< (std::ostream& os,
										 const SupernodeBase& supernode);
		virtual operator std::string() const;

		//	TODO: it is intended that these variables will be replaced
		//	by actual lists of superarc's [03-10-2015]
		virtual size_type GetUpDegree() const = 0;
		virtual size_type GetDownDegree() const = 0;
		virtual size_type GetDegree() const { return GetUpDegree() + GetDownDegree(); }

		virtual size_type GetTotalArcsUp() const = 0;
		virtual size_type GetTotalArcsDown() const = 0;

		virtual size_type GetTotalNodesUp() const = 0;
		virtual size_type GetTotalNodesDown() const = 0;

		inline bool IsUpperLeaf() const;
		inline bool IsLowerLeaf() const;
		inline bool IsRegular() const;

		//	Derrived classes must provide some way of accesing the function
		//	height
		virtual T GetHeight() const = 0;
		virtual T GetNormalisedHeight() const = 0;

		virtual void HighlightUpperSubtree() = 0;
		virtual void HighlightLowerSubtree() = 0;

		size_type GetId() const { return m_id; }
		SupernodeBase(const size_type id)
			: m_id{id}
		{ }


		virtual ~SupernodeBase()
		{ }


		//	Default comparison via address
		bool operator <(const SupernodeBase& rhs) const
		{
			return this < &rhs;
		}

		//std::set<SupernodeBase> m_linked_super_nodes;
};

///
/// \brief		Comparison functor allowing polymorphic storage of
///				derrived supernodes in a std::map
/// \author		Dean
/// \since		04-02-2016
///
//template <typename T, typename U>
//struct SupernodeBase_less:
//		std::function<bool(const SupernodeBase*,const SupernodeBase*)>
//{
//	bool operator()(const SupernodeBase* a, const SupernodeBase* b) const
//	{
//		return &a < &b;
//	}
//};

///
/// \brief      functions to test whether a node is a leaf
/// \return
/// \since      25-11-2014
/// \author     Hamish
/// \author		Dean: begin to implement upDegree and downDegree as functions
///				[03-10-2015]
///
bool SupernodeBase::IsLowerLeaf() const
{
	return ((GetUpDegree() == 1) && (GetDownDegree() == 0));
}

///
/// \brief      functions to test whether a node is a leaf
/// \return
/// \since      25-11-2014
/// \author     Hamish
/// \author		Dean: begin to implement upDegree and downDegree as functions
///				[03-10-2015]
///
bool SupernodeBase::IsUpperLeaf() const
{
	return ((GetUpDegree() == 0) && (GetDownDegree() == 1));
}

///
/// \brief		checks if a leaf has exactly one up and one down edge
/// \return
/// \since      25-11-2014
/// \author		Hamish
/// \author		Dean: begin to implement upDegree and downDegree as functions
///				[03-10-2015]
///
bool SupernodeBase::IsRegular() const
{
	return ((GetUpDegree() == 1) && (GetDownDegree() == 1));
}

///
/// \brief		Provides a basic human readable of the class, can be overriden
///				in derived classes to give more suitable information
/// \author		Dean
/// \since		08-02-2016
///
inline SupernodeBase::operator std::string() const
{
	std::stringstream result;

	result << "SupernodeBase = { id: " << m_id << " } ";
	return result.str();
}

///
/// \brief		Allow the object to be output directly to a output stream.
///				Calls the virtual std::string operator, so the output can
///				vary if called from a derived type.
/// \param os
/// \param supernode
/// \return
/// \author		Dean
/// \since		08-02-2016
///
inline std::ostream& operator<< (std::ostream& os,
								 const SupernodeBase& supernode)
{
	//	Call operator std::string in the concrete class
	os << static_cast<std::string>(supernode);
	return os;
}

#endif // SUPERNODEBASE_H
