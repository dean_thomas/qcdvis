//	IsoSurface II
//	Hamish Carr, 2000
//	Superarc.cpp:		class representing superarcs in CT
//	Palatino-12; 5 n-spaces/tab
#include "ContourTreeSuperarc.h"
#include "ContourTreeSupernode.h"
#include "Graphs/ContourTree.h"
#include <functional>
#include "Parallel/ThreadQueue.h"

using std::cerr;
using std::cout;
using std::endl;

float ContourTreeSuperarc::GetArcLength() const
{
	return m_topSupernode->GetHeight() - m_bottomSupernode->GetHeight();
}

///
/// \brief	Set's the valid state of the superarc
/// \param	value: the new state
/// \since	14-05-2015
/// \author	Dean
/// \author	Dean: update to use assert [03-10-2015]
///
void ContourTreeSuperarc::SetValid(const bool value)
{
	assert (value != m_isValid);

	m_isValid = value;
}

///
/// \brief	Set's the active state of the superarc
/// \param	value: the new state
/// \since	14-05-2015
/// \author	Dean
/// \author	Dean: update to use assert [03-10-2015]
///
void ContourTreeSuperarc::SetActive(const bool value)
{
	assert (value != m_isActive);

	m_isActive = value;
}

///
/// \brief		Returns details of the arc in human readable form
/// \return
/// \author		Dean
/// \since		15-04-2015
///
string ContourTreeSuperarc::ToString() const
{
	stringstream result;

	result << "Superarc = { ";
	result << "ArcID = " << m_id << ", ";

	//	Bounding nodes
	result << "Top Node = { " << m_topSupernode << " }, ";
	result << "Bottom Node = { " << m_bottomSupernode << " }, ";

	//	Boolean properties
	result << "IsHighlighted = " << (m_isHighlighted ? "true" : "false")
		   << ", ";
	result << "IsSelected = " << (m_isSelected ? "true" : "false")
		   << ", ";

	//	Todo:	add lists of all mesh vertices on each heightfield boundary
	result << " }";

	return result.str();
}

///
/// \brief	Marks the arc as being selected
/// \param	value: boolean; if true, the arc is marked as selected
/// \param	raiseNotification: boolean; if true, any observers will be notified
///			of the update
/// \author	Dean
/// \since	15-04-2015
///
//void ContourTreeSuperarc::SetSelected(const bool value, const bool raiseNotification)
//{
//	m_isSelected = value;
//
//	if (raiseNotification)
//		m_contourTree->GetHeightfield()->GetDataModel()->NotifyObservers();
//}

void ContourTreeSuperarc::SetHidden(const bool value)
{
	//	Call the basic implementation
	SuperarcBase::SetHidden(value);

	//	For a contour tree arc we also need to maintain additional state
	if (m_isHidden)	m_isActive = false;
}


ContourTreeSuperarc::ContourTreeSuperarc(ContourTree *contourTree,
										 const size_type id,
										 ContourTreeSupernode *topNode,
										 ContourTreeSupernode *bottomNode)
	: SuperarcBase(id)
{
	//	By defualt we have no surface generated
	//m_currentIsosurface = nullptr;

	//	Somewhere to store our meshes
	m_meshCache = new MeshCache();

	//	What contour tree do we belong to?
	m_contourTree = contourTree;

	//	All objects are given a global ID on creation...
	//	This should tally up to the index of the arc in the global superarc
	//	list
	m_id = id;

	//	store a reference to the top and bottom nodes
	m_topSupernode = topNode;
	m_bottomSupernode = bottomNode;

	//	initialize the rest to "empty"
	m_nextUpArc = nullptr;
	m_lastUpArc = nullptr;
	m_nextDownArc = nullptr;
	m_lastDownArc = nullptr;

	//	and these
	m_collapseTopSuperarc = nullptr;
	m_collapseBottomSuperarc = nullptr;

	//	zero out the volumetric information
	nodesOnArc = 0;
	nodesThisSideOfTop = 0;
	nodesThisSideOfBottom = 0;

	//	and the flags
	m_isValid = true;
	m_isActive = false;
	m_isSelected = false;
	m_isHidden = false;
	m_isOnBackQueue = false;
	m_wasActiveBeforePruning = false;
	m_isHighlighted = false;

	//	and the seeds
	seedFromHi = nullptr;
	seedToHi = nullptr;
	seedFromLo = nullptr;
	seedToLo = nullptr;

	sampleSumTop = 0;
	sampleSumBottom = 0;
	sampleSumOnArc = 0;

	m_isosurfaceGenerator = new ContourFactory(this,
											   m_contourTree->GetHeightfield());
}

///
/// \brief      set high end seed
/// \param      SeedFromHi
/// \param      SeedToHi
/// \author     Hamish
/// \since      25-11-2014
///
void ContourTreeSuperarc::SetHighSeed(Real *SeedFromHi, Real *SeedToHi)
{
	//	copy the pointers
	seedFromHi = SeedFromHi;
	seedToHi = SeedToHi;
}

///
/// \brief      set low end seed
/// \param      SeedFromLo
/// \param      SeedToLo
/// \author     Hamish
/// \since      25-11-2014
///
void ContourTreeSuperarc::SetLowSeed(Real *SeedFromLo, Real *SeedToLo)
{
	//	copy the pointers
	seedFromLo = SeedFromLo;
	seedToLo = SeedToLo;
}

///
/// \brief      Get's the volume represented above this arc in
///             the contour tree
/// \return
/// \since      26-11-12014
/// \author     Dean
///
float ContourTreeSuperarc::GetApproxVolumeUp(const Persistence_Measure measure) const
{
	if (measure == Persistence_Measure::NODE_COUNT)
	{
		return static_cast<float>(nodesThisSideOfBottom);
	}
	else
	{
		return sampleSumBottom;
	}
}

///
/// \brief      Get's the volume represented bolow this arc in
///             the contour tree
/// \return
/// \since      26-11-12014
/// \author     Dean
///
float ContourTreeSuperarc::GetApproxVolumeDown(const Persistence_Measure measure) const
{
	if (measure == Persistence_Measure::NODE_COUNT)
	{
		return static_cast<float>(nodesThisSideOfTop);
	}
	else
	{
		return sampleSumTop;
	}
}

///
/// \brief      Get's the volume represented on this arc in
///             the contour tree
/// \return
/// \since      10-01-2017
/// \author     Dean
///
float ContourTreeSuperarc::GetApproxVolume(const Persistence_Measure measure) const
{
	if (measure == Persistence_Measure::NODE_COUNT)
	{
		return static_cast<float>(nodesOnArc);
	}
	else
	{
		return sampleSumOnArc;
	}
}

float ContourTreeSuperarc::GetIntegratedSurfaceArea(const size_t sample_count)
{
	auto result = 0.0f;

	auto minIsovalue = m_bottomSupernode->GetHeight();
	auto maxIsovalue = m_topSupernode->GetHeight();
	auto isovalueRange = maxIsovalue - minIsovalue;

	auto stepSize = isovalueRange / static_cast<float>(sample_count + 2);

	auto first_isovalue = minIsovalue + stepSize;
	auto last_isovalue = maxIsovalue - stepSize;

	for (auto isovalue = first_isovalue; isovalue < last_isovalue; isovalue += stepSize)
	{
		auto contour = RequestContour(isovalue);
		assert(contour != nullptr);

		if (contour->IsClosedSurface())
		{
			//	We can only rely on the surface area of closed contours
			auto sampled_surface_area = contour->GetSurfaceArea();
			result += sampled_surface_area;
		}
		else
		{
			//	Notify of non-manifold objects in error console
			cerr << "Contour " << m_id << " is not a closed surface at isovalue "
				 << isovalue << "." << endl;
		}
	}
	return result;// / static_cast<float>(sample_count);
}

float ContourTreeSuperarc::GetIntegratedVolume(const size_t sample_count)
{
	auto result = 0.0f;

	auto minIsovalue = m_bottomSupernode->GetHeight();
	auto maxIsovalue = m_topSupernode->GetHeight();
	auto isovalueRange = maxIsovalue - minIsovalue;

	auto stepSize = isovalueRange / static_cast<float>(sample_count + 2);

	auto first_isovalue = minIsovalue + stepSize;
	auto last_isovalue = maxIsovalue - stepSize;

	for (auto isovalue = first_isovalue; isovalue < last_isovalue; isovalue += stepSize)
	{
		auto contour = RequestContour(isovalue);
		assert(contour != nullptr);

		if (contour->IsClosedSurface())
		{
			//	We can only rely on the surface area of closed contours
			auto sampled_volume = contour->GetUnscaledVolume();
			result += sampled_volume;
		}
		else
		{
			//	Notify of non-manifold objects in error console
			cerr << "Contour " << m_id << " is not a closed surface at isovalue "
				 << isovalue << "." << endl;
		}
	}
	return result;// / static_cast<float>(sample_count);
}

void ContourTreeSuperarc::integratePropertiesOverArc()
{
	//	This should be a power of two (>0) ideally
#define STEPS 2

	Real minIsovalue = m_bottomSupernode->GetHeight();
	Real maxIsovalue = m_topSupernode->GetHeight();
	Real isovalueRange = maxIsovalue - minIsovalue;

	Real stepSize = isovalueRange / STEPS;

	cout << "Integrating over superarc " << m_id;
	cout << "." << endl;
	printf("isovalue = { min: %f, max: %f, range: %f }\n", minIsovalue,
		   maxIsovalue, isovalueRange);
	printf("Number of steps = %ld\n", STEPS);

	for (Real isovalue = minIsovalue;
		 isovalue < maxIsovalue; isovalue += stepSize)
	{
		printf("Calculating stats for isovalue %f.\n", isovalue);
	}

	fflush(stdout);
}


///
/// \brief	Checks to see if the given isovalue exists in the isovalue range
///			that the superarc represents
/// \param	value: the isovalue to be checked
/// \return bool: true if the arcs is present at the given isovalue
/// \since	11-05-2015
/// \author	Dean
///
bool ContourTreeSuperarc::IsPresentAtIsovalue(const Real& value) const
{
	return ((value > m_bottomSupernode->GetHeight())
			&& (value < m_topSupernode->GetHeight()));
}

///
/// \brief		Asks the contour factory associated with this superarc to
///				generate a contour for the specified isovalue.
/// \details	This function shouldn't be called directly but via the
///				Superarc::RequestContour function, which will call it on a
///				separate thread
/// \param		isovalue: the isovalue that the contour should represent
/// \since		11-05-2015
/// \author		Dean
///
void ContourTreeSuperarc::generateContourWithIsovalue(const Real& isovalue)
{
	try
	{
		//	Create an entry in the cache
		MeshCacheItem* cacheItem = m_meshCache->QueueMesh(isovalue);

		assert(cacheItem != nullptr);

		//	Generate the contour using flexible isosurface algorithm
		Contour* contour = m_isosurfaceGenerator->GenerateContour(isovalue);

		//	Cap holes etc.
		//if (contour->IsOnAnyBoundary())
		//	contour->PostProcess();

		//	update the cache item with the completed contour
		cacheItem->SetContour(contour);
	}
	catch (InvalidIsovalueException &ex)
	{
		fprintf(stderr, ex.what());
		fflush(stderr);
	}

}

///
/// \brief		Requests a contour for a given isovalue.  If one exists in the
///				cache it is returned; if not, a call is made to generate the
///				contour on a separate thread
/// \param		isovalue: the isovalue that the contour should represent
/// \return		Contour*: a pointer to the contour or nullptr if it is not
///				currently available.
/// \since		11-05-2015
/// \author		Dean
///
Contour* ContourTreeSuperarc::RequestContour(const Real& isovalue, const bool addToQueue)
{
	MeshCacheItem *result = m_meshCache->FindCacheItemWithIsovalue(isovalue);

	//	Mesh is already cached
	if (result != nullptr)
	{
		if (result->status == MeshCacheItem::MeshStatus::READY)
		{
			//	Mesh is constructed
			return result->contour;
		}
		else
		{
			//	Mesh is being constructed
			return nullptr;
		}
	}


#ifdef GENERATE_ALL_CONTOURS_ON_MAIN_THREAD
	//	Compute the contour *now* on the mainthread
	generateContourWithIsovalue(isovalue);

	//	The result should exist now...
	result = m_meshCache->FindCacheItemWithIsovalue(isovalue);

	//	Make sure nothing went wrong
	if (result != nullptr)
	{
		if (result->status == MeshCacheItem::MeshStatus::READY)
		{
			//	Mesh is constructed
			return result->contour;
		}
		else
		{
			//	Mesh is being constructed
			return nullptr;
		}
	}
#else
	//	Mesh needs creation
	//	Start creation on a separate thread and return nullptr immediatley
	using std::function;

	function<void ()> func =
			std::bind(&Superarc::generateContourWithIsovalue, this, isovalue);

	if (!addToQueue)
	{
		std::thread myThread(func);
		myThread.detach();
	}
	else
	{
		ThreadQueue::GetInstance().AddJobToQueue(*this, isovalue);
	}

	return nullptr;
#endif
}

///
/// \brief		Get's the isovalue at the midpoint of the arc
/// \return		Real: the actual isovalue.
/// \since		13-05-2015
/// \author		Dean
///
Real ContourTreeSuperarc::GetIsovalueAtMidpoint() const
{
	Real valMax = m_topSupernode->GetHeight();
	Real valMin = m_bottomSupernode->GetHeight();

	return (valMax + valMin) / 2.0;
}

///
/// \brief		Convenience method to generate / retreive a contour at the
///				midpoint of the contour.
/// \return		A pointer to the contour (or nullptr if it is to be created).
/// \since		13-05-2015
/// \author		Dean
///
Contour* ContourTreeSuperarc::RequestContourAtMidpoint(const bool addToQueue)
{
	Real midpoint = GetIsovalueAtMidpoint();
	return RequestContour(midpoint, addToQueue);
}

ContourTreeSuperarc::~ContourTreeSuperarc()
{
	delete m_meshCache;
	delete m_isosurfaceGenerator;
}
