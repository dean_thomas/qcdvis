#ifndef MERGE_TREE_H
#define MERGE_TREE_H

#include "../HeightField/HeightField3D.h"
#include "../HeightField/Component.h"
#include "Globals/Globals.h"
#include <string>

using std::string;

class MergeTree
{
protected:
	HeightField3D *m_heightfield3D = nullptr;

	virtual void createTree() = 0;

//	using Index3d = HeightField3D::Index3d;
	using ComponentArray3D = Storage::Vector3<Component*>;
	using ArcLookupArray3D = Storage::Vector3<Real *>;

	bool saveToDotFile(const string basefilename, const bool reverseArcs) const;
public:

	ComponentArray3D m_componentArray3D;
	ArcLookupArray3D m_arcLookupArray3D;
	Component *m_rootComponent = nullptr;

	virtual bool SaveToDotFile(const string basefilename) const = 0;
	string ToDotString(const bool reverseArcs) const;

	string pointerIndicesToString(const Real *vertex) const;
	string componentArrayToString(const bool suppressDuplicates,
								  const bool fullDetails = true) const;

	MergeTree();
	MergeTree(HeightField3D *heightfield3D);
	virtual ~MergeTree();
};

#endif // MERGE_TREE_H
