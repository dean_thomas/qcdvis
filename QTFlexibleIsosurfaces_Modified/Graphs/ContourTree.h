#ifndef CONTOURTREE_H
#define CONTOURTREE_H

#include "TreeBase.h"
#include "ContourTreeSuperarc.h"
#include <Vector3.h>
#include "ContourTreeSupernode.h"
#include "../HeightField/HeightField3D.h"
#include "Graphs/JoinTree.h"
#include "Graphs/SplitTree.h"
#include "../HeightField/PriorityIndex.h"
#include "DataModel.h"
#include <queue>
#include <cmath>
#include <string>
#include <sstream>
#include "Globals/Globals.h"
#include <queue>
#include <array>
#include <queue>
#include <array>

class SuperarcDistribution;
class SupernodeDistribution;

class ContourTree : public TreeBase<ContourTreeSupernode, ContourTreeSuperarc>
{
	public:
		using Index3d = Storage::Index3;

		///	Enums / Structs
		///	===============
		//	Used to define the priority metric when simplifying the Tree
		enum class CollapsePriority
		{
			HEIGHT,
			VOLUME,
			RIEMANN_SUM,
			HYPERVOLUME
		};

		struct MergeTreePair
		{
				JoinTree* joinTree = nullptr;
				SplitTree* splitTree = nullptr;

				MergeTreePair(JoinTree *jt, SplitTree* st)
				{
					assert (jt != nullptr);
					assert (st != nullptr);

					joinTree = jt;
					splitTree = st;
				}

				MergeTreePair()
					: joinTree{nullptr}, splitTree{nullptr} { }
		};

	public:
		///  Constructors and destructors
		///	============================
		ContourTree(HeightField3D *heightfield);
		~ContourTree();

		Real GetMinHeight() const;
		Real GetMaxHeight() const;

		JoinTree* GetJoinTree() const
		{
			return m_initialMergeTrees.joinTree;
		}

		SplitTree* GetSplitTree() const
		{
			return m_initialMergeTrees.splitTree;
		}
	private:
		///	Member variables
		///	================
		bool m_isReady;

		MergeTreePair m_initialMergeTrees;
		MergeTreePair m_augmentedMergeTrees;

		HeightField3D *m_heightfield = nullptr;

		//	Lookup for the supernode that relates to each data point
		Storage::Vector3<ContourTreeSupernode*> m_nodeLookup;

		SupernodeDistribution* m_supernodeDistribution = nullptr;
		SuperarcDistribution* m_superarcDistribution = nullptr;

		Real **m_leafQueue;
		long m_leafQueueSize;


	private:
		///	Helper functions
		///	================
		MergeTreePair augmentTrees(const JoinTree *joinTree, const SplitTree *splitTree);

		MergeTreePair constructInitialMergeTrees();

		void combineTrees(const JoinTree *joinTree, const SplitTree *splitTree);

		ContourTreeSuperarc *addSuperarc(ContourTreeSupernode *topSupernode,
							  ContourTreeSupernode *bottomSupernode,
							  Real *seedHiFrom,
							  Real *seedHiTo,
							  Real *seedLoFrom,
							  Real *seedLoTo);
		ContourTreeSuperarc *addSuperarc(Real *hiEnd,
							  Real *loEnd,
							  Real *seedHiFrom,
							  Real *seedHiTo,
							  Real *seedLoFrom,
							  Real *seedLoTo);

		ContourTreeSupernode *getSupernode(Real *theVertex);


		void addDownArc(ContourTreeSupernode *theNode, ContourTreeSuperarc *theArc);
		void addUpArc(ContourTreeSupernode* theNode, ContourTreeSuperarc *theArc);
		void removeArc(ContourTreeSuperarc *superarc);

		size_type computeSuperarcPriority(ContourTreeSuperarc* arc, const CollapsePriority &collapsePriority,
										  const bool &upSweep) const;

		//  Merged from collapsible contour tree [07-04-2015]
		ContourTreeSuperarc *collapseSupernodeToSuperarc(ContourTreeSupernode* whichSupernode);

		void collapseLeafPruning(const CollapsePriority& priority,
								 unsigned long targetSize,
								 long prunedVolumeBound);


		void collapseEpsilonEdges();
		bool removeEdgeIfEpsilonHeight(std::queue<ContourTreeSupernode *> &supernodeQueue,
									   std::vector<bool> m_wasChecked,
									   ContourTreeSuperarc* theArc);
		void transferArcLists(ContourTreeSupernode *ontoNode, ContourTreeSupernode *fromNode);

	public:
		std::string ToDOT(const bool &fullTree) const;

		///	Public Functions and Accessors
		///	==============================
		//	Simple accessors
		bool IsReady() const { return m_isReady; }

		SupernodeDistribution* GetSupernodeDistribution() const { return m_supernodeDistribution; }
		SuperarcDistribution* GetSuperarcDistribution() const { return m_superarcDistribution; }

		HeightField3D* GetHeightfield() const { return m_heightfield;}

		//	Subsets of main lists with specified properties
		SuperarcContainer GetValidSuperarcsPresentAtIsovalue(const Real &value) const;

		void ClearActive();

		//void ClearSelectedNodes();

		void SaveNodeDistributionToFile(const char* filename, const bool scaleToData = false);
		void SaveArcDistributionToFile(const char* filename, const bool scaleToData = false);

		void SaveContourTreeToDotFile(const string &basefilename,
									  const bool &fullTree) const;
};

///
/// \brief	ContourTree::GetMinHeight
/// \return
/// \author	Dean
/// \since	09-02-2015
///
inline Real ContourTree::GetMinHeight() const
{
	//	For now, drag this from the heightfield but it'd be nice to pull it
	//	from the bottom supernode directly
	assert (m_heightfield != nullptr);
	return m_heightfield->GetMinHeight();
}

///
/// \brief	ContourTree::GetMaxHeight
/// \return
/// \author	Dean
/// \since	09-02-2015
///
inline Real ContourTree::GetMaxHeight() const
{
	//	For now, drag this from the heightfield but it'd be nice to pull it
	//	from the top supernode directly
	assert (m_heightfield != nullptr);
	return m_heightfield->GetMaxHeight();
}


#endif // CONTOURTREE_H
