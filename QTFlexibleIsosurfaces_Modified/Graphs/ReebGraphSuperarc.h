#ifndef REEB_GRAPH_SUPERARC_H
#define REEB_GRAPH_SUPERARC_H

#include "SuperarcBase.h"

class ReebGraphSupernode;

class ReebGraphSuperarc : public SuperarcBase<ReebGraphSupernode>
{
	public:
		ReebGraphSuperarc()
			: SuperarcBase(0)
		{
			SetTopSupernode(nullptr);
			SetBottomSupernode(nullptr);
		}

		ReebGraphSuperarc(const size_t id,
						  ReebGraphSupernode* const top,
						  ReebGraphSupernode* const bottom);


		float GetArcLength() const override;

};

#endif // REEB_GRAPH_SUPERARC_H

