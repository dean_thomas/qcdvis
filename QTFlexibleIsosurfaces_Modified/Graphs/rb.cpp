#include "rb.h"

#include <string>
#include <fstream>
#include <cassert>
#include <regex>

using namespace std;

#define USING_STL_VECTOR
#define DISABLE_DEBUG_OUTPUT

bool RbWriter::operator()(const std::string& filename)
{
    cerr << "NOTE: This method is obsolete, use the RbFormat stream writer instead "
			"from now on." << endl;

	ofstream outFile(filename);

	if (!outFile.is_open()) return false;

	outFile << "#	Nodes" << endl;
	for (auto it = reebGraph.Supernodes_cbegin(); it != reebGraph.Supernodes_cend(); ++it)
	{
#ifdef USING_STL_VECTOR
		auto id = it - reebGraph.Supernodes_cbegin();
		auto node = *it;
#else
		auto id = it->first;
		auto node = it->second;
#endif
		outFile << id << "\t" << node->GetHeight() << endl;
	}
	outFile << endl;

	outFile << "#	Arcs" << endl;
	for (auto it = reebGraph.Superarcs_cbegin(); it != reebGraph.Superarcs_cend(); ++it)
	{
#ifdef USING_STL_VECTOR
		auto id = it - reebGraph.Superarcs_cbegin();
		auto arc = *it;
#else
		auto id = it->first;
		auto arc = it->second;
#endif
		outFile << id << "\t" << arc->GetTopSupernode()->GetId()
				<< "\t" << arc->GetBottomSupernode()->GetId() << endl;
	}

	outFile.close();

    return true;
}

bool RbReader::operator()(const std::string& filename)
{
	//	This should match the version in the Rb file formatter
	//const auto TARGET_RB_VERSION = 1.1f;


	//	(float: version)
	const auto VERSION_REGEX = R"|(#\s*version\s*([+-]?[0-9]*[.]?[0-9]*))|";
	enum { VERSION = 1};

	//	(int: id)(float: height)(float: norm height)(int: x)(int: y)(int: z)
	const auto VERTEX_REGEX = R"|((\d+)\s*([+-]?[0-9]*[.]?[0-9]*)\s*([+-]?[0-9]*[.]?[0-9]*)\s*\((\d*),\s*(\d*),\s*(\d*)\))|";
	enum { V_ID = 1, V_HEIGHT = 2, V_NORM = 3, V_X = 4, V_Y = 5, V_Z = 6 };

	//	(int: id)(int: src_id)(int: dst_id)
	const auto EDGE_REGEX = R"|((\d+)\s*(\d+)\s*(\d+))|";
	enum { E_ID = 1, E_SRC = 2, E_DST = 3 };

	//	Compile our REGEXes
	regex version_regex(VERSION_REGEX);
	regex vertex_regex(VERTEX_REGEX);
	regex edge_regex(EDGE_REGEX);

	//	temporaries
	string buffer;
	auto version_number = 0.0f;

	//	Open the file
	ifstream inFile(filename);
	if (!inFile.is_open()) return false;

	//	New files will start with a version comment
	getline(inFile, buffer);
	smatch version_matches;
	if (regex_match(buffer, version_matches, version_regex))
	{
		assert(version_matches.size() == 2);
		version_number = stof(version_matches[VERSION]);

		cout << "Target RB version is: " << TARGET_RB_VERSION << endl;
		cout << "Rb file version is: " << version_number << endl;

		//if (version_number != TARGET_RB_VERSION)
		//{
		//	cerr << "Rb file is out of date.  Recompute using the most "
		//		"recent version of the application." << endl;
		//	return false;
		//}
	}
	else
	{
		cerr << "Rb file is the old format, cannot be parsed." << endl;
		return false;
	}

	//	Read remainder of file
	while(!inFile.eof())
	{
		getline(inFile, buffer);
		smatch vertex_matches;
		smatch edge_matches;

		//	Try to match a vertex definiton
		if (regex_match(buffer, vertex_matches, vertex_regex))
		{
			assert(vertex_matches.size() == 7);
			auto id = stoul(vertex_matches[V_ID]);
			auto height = stof(vertex_matches[V_HEIGHT]);
			auto norm = stof(vertex_matches[V_NORM]);
			auto x = stoul(vertex_matches[V_X]);
			auto y = stoul(vertex_matches[V_Y]);
			auto z = stoul(vertex_matches[V_Z]);

			//	Add the node to the reeb graph (use the actually height
			//	and calculate the normalised height on demand
			reebGraph.AddNode(id, { id, height, norm, x, y, z });
			//reebGraph.AddNode(id, { id, norm, x, y, z });
		}
		else if (regex_match(buffer, edge_matches, edge_regex))
		{
			assert(edge_matches.size() == 4);
			auto id = stoi(edge_matches[E_ID]);
			auto src = stoi(edge_matches[E_SRC]);
			auto dst = stoi(edge_matches[E_DST]);

			//	Add the edge to the Reeb graph (src, dst need to be reversed
			//	to maintain height order)
			reebGraph.AddEdge(id, src, dst);
		}
	}
	inFile.close();

	return true;
}


/*	OLD FILE READER CODE
 *  removed 30-06-2016
 *
	size_t nodeCount = 0;
	size_t arcCount = 0;

	getline(inFile, buffer);	// #	Nodes
	//	Todo: Validate buffer
	while (inFile.peek() != '#')
	{
		//	Read in nodes
		size_t nodeId;
		float height;

		inFile >> nodeId >> height;
		inFile >> ws;

		++nodeCount;
		cout << nodeId << "\t" << height
			 << "\t" << "(" << nodeCount << ")"<< endl;

		//	Add the node to the reeb graph
		reebGraph.AddNode(nodeId, { nodeId, height });
	}
	//cout << "Read in " << nodeCount << " nodes." << endl;

	getline(inFile, buffer);	// #	Arcs
	//	Todo: Validate buffer
	while ((inFile.peek() != '#') && (!inFile.eof()))
	{
		//	Read in nodes
		size_t arcId;
		size_t sourceNodeId;
		size_t targetNodeId;

		inFile >> arcId >> sourceNodeId >> targetNodeId;
		inFile >> ws;

		++arcCount;
		//cout << arcId << "\t" << sourceNodeId
		//	<< " - > " << targetNodeId << endl;

		//	Add the arc to the reeb graph
		auto result = reebGraph.AddEdge(arcId, sourceNodeId, targetNodeId);
	}
	inFile.close();

	//cout << "Read in " << arcCount << " arcs." << endl;
	//cout << reebGraph << endl;
 */
