#include "ContourTree.h"

#include "Statistics/SupernodeDistribution.h"
#include "Statistics/SuperarcDistribution.h"

using namespace std;

///
/// \brief:     clears active list
/// \author:    Hamish
/// \since:     24-11-2014
/// \author		Dean: update to use iterators [29-09-2015]
///
void ContourTree::ClearActive()
{
	for (auto it = m_superarcContainer.begin(); it != m_superarcContainer.end(); ++it)
	{
		//	Set the selected arc to inactive
		(*it)->SetActive(false);
	}
}

///
/// \brief      computes priority for a given arc
/// \param      arc
/// \param      upSweep
/// \return
/// \author     Hamish
/// \since      21-11-2014
/// \author		Dean: pass collapse priority as a parameter [05-10-2015]
///
ContourTree::size_type ContourTree::computeSuperarcPriority(ContourTreeSuperarc *arc,
															const CollapsePriority& collapsePriority,
															const bool& upSweep) const
{
	size_type volume;
	double riemannSum;

	if (upSweep)
	{
		//	set priority to volume on edge
		volume = arc->nodesThisSideOfTop;
		//		printf("Arc %2d. Raw riemann: %12.1f.  Volume: %8d. Value: %12.1f. V*V: %12.1f\n", arc, superarcs[arc].sampleSumTop,
		//			volume, *(supernodes[superarcs[arc].topID].value), volume * *(supernodes[superarcs[arc].topID].value));
		riemannSum = -arc->sampleSumTop + volume * *(arc->GetTopSupernode()->GetScalarVertex());
	}
	else
	{
		volume = arc->nodesThisSideOfBottom;
		//		printf("Arc %2d. Raw riemann: %12.1f.  Volume: %8d. Value: %12.1f. V*V: %12.1f\n", arc, superarcs[arc].sampleSumBottom,
		//			volume, *(supernodes[superarcs[arc].bottomID].value), volume * *(supernodes[superarcs[arc].bottomID].value));
		riemannSum = arc->sampleSumBottom - volume * *(arc->GetBottomSupernode()->GetScalarVertex());
	}

	//printf("Volume is %ld\n", volume);

	double height = *(arc->GetTopSupernode()->GetScalarVertex()) - *(arc->GetBottomSupernode()->GetScalarVertex());
	double approxhypervolume = (((double) volume) * height);

	//	printf("Approx: %12.1f.  Riemann: %12.1f.  Ratio: %5.3f\n", approxhypervolume, riemannSum, approxhypervolume/riemannSum);

	double scaledhypervolume = ((double) 1E6) * (riemannSum / (((double) m_heightfield->GetVertexCount()) * (m_heightfield->GetMaxHeight() - m_heightfield->GetMinHeight())));

	switch(collapsePriority)
	{
		case CollapsePriority::HEIGHT:
			return (long) height;
		case CollapsePriority::VOLUME:
			return volume;
		case CollapsePriority::RIEMANN_SUM:
			return (size_type) (1E6 * approxhypervolume / (double) (m_heightfield->GetVertexCount() * (m_heightfield->GetMaxHeight() - m_heightfield->GetMinHeight())));
		case CollapsePriority::HYPERVOLUME:
		default:
			return (size_type) scaledhypervolume;
	}
}


// *************
// ** WARNING **
// *************
//
// This routine assumes that the fromNode's arcs have already had the hiID/loID and hiEnd/loEnd correctly reset to the onto node
//
///
/// \brief      transfer's arcs from one node to another
/// \param      ontoNode
/// \param      fromNode
/// \since      21-11-2014
/// \author     Hamish
/// \author		Pass supernodes as pointer instead of via indices [03-10-2015]
///
void ContourTree::transferArcLists(ContourTreeSupernode* ontoNode, ContourTreeSupernode* fromNode)
{
	//	printf("Transferring arcs from %3d to %3d\n", fromNode, ontoNode);

	//	transfer the up list
	if (ontoNode->GetUpDegree() == 0)
	{
		//	simple case: onto node is empty
		//	just copy across
		ontoNode->m_upDegree = fromNode->m_upDegree;
		ontoNode->upList = fromNode->upList;
	}
	else if (fromNode->GetUpDegree() != 0)
	{
		//	if the second one is non-empty
		auto fromFirst = fromNode->upList;
		auto ontoFirst = ontoNode->upList;

		//	pointers to the two nodes needing fixing
		auto fromLast = fromFirst->m_lastUpArc;
		auto ontoNext = ontoFirst->m_nextUpArc;

		ontoNext->m_lastUpArc = fromLast;
		fromLast->m_nextUpArc = ontoNext;
		fromFirst->m_lastUpArc = ontoFirst;
		ontoFirst->m_nextUpArc = fromFirst;

		ontoNode->m_upDegree += fromNode->m_upDegree;
	}

	//	transfer the down list
	if (ontoNode->GetDownDegree() == 0)
	{
		//	simple case: onto node is empty
		//	just copy across
		ontoNode->m_downDegree = fromNode->m_downDegree;
		ontoNode->downList = fromNode->downList;
	}
	else if (fromNode->GetDownDegree() != 0)
	{
		//	if the second one is non-empty
		auto fromFirst = fromNode->downList;
		auto ontoFirst = ontoNode->downList;

		//	pointers to the two nodes needing fixing
		auto fromLast = fromFirst->m_lastDownArc;
		auto ontoNext = ontoFirst->m_nextDownArc;

		ontoNext->m_lastDownArc = fromLast;
		fromLast->m_nextDownArc = ontoNext;
		fromFirst->m_lastDownArc = ontoFirst;
		ontoFirst->m_nextDownArc = fromFirst;
		ontoNode->m_downDegree += fromNode->m_downDegree;
	}

	//	and reset the from node
	fromNode->m_upDegree = 0;
	fromNode->upList = nullptr;
	fromNode->m_downDegree = 0;
	fromNode->downList = nullptr;
}

///
/// \brief      routine to check an arc, and remove from tree if needed
/// \param      theArc
/// \return
/// \author     Hamish
/// \since      21-11-2014
/// \author		Dean: pass superarc directly [03-10-2015]
///
bool ContourTree::removeEdgeIfEpsilonHeight(queue<ContourTreeSupernode*> &supernodeQueue,
											vector<bool> m_wasChecked,
											ContourTreeSuperarc *theArc)
{
	auto topNode = theArc->GetTopSupernode();
	auto bottomNode = theArc->GetBottomSupernode();

	if (*(topNode->GetScalarVertex()) == *(bottomNode->GetScalarVertex()))
	{ // found an epsilon-height edge
		//		printf("Found epsilon-height edge %d - %d\n", superarcs[theArc].topID, superarcs[theArc].bottomID);
		if (!m_wasChecked[topNode->GetId()])
		{
			supernodeQueue.push(topNode);
		}
		//m_supernodeQueue[m_supernodeQueueSize++] = topNode;					//	add the "high end" to the queue

		if (!m_wasChecked[bottomNode->GetId()])
		{
			supernodeQueue.push(bottomNode);
		}
		//	m_supernodeQueue[m_supernodeQueueSize++] = bottomNode;				//	or the bottom one

		removeArc(theArc);												//	and remove the arc from the contour tree
		//		contourTreeToDotFile("collapsed", dotFileNo++);
		//		PrintcontourTree();
		return true;
	} // found an epsilon-height edge
	return false;
}

///
/// \brief      Collapses the epsilon-height edges
/// \author     Hamish
/// \since      21-11-2014
/// \author		Dean: update to avoid using 'superarc count' member variables
///				[29-09-2015]
/// \author		Dean: update to use iterators and use supernodes via pointers
///				(instead of indices) [03-10-2015]
///
void ContourTree::collapseEpsilonEdges()
{
	//	Kepp track of if each superarc has been checked using a boolean vector
	//	(all initialized to false)
	vector<bool> wasChecked(m_superarcContainer.size(), false);


	queue<ContourTreeSupernode*> supernodeQueue;

	//long qNext;														//	keep track of logical next item
	ContourTreeSuperarc* upArc = nullptr;
	ContourTreeSuperarc* downArc = nullptr;
	size_type nArcs;											//	used for walking around nodes

	//	loop through all supernodes: test each to see if it has any zero-height edges

	for (auto it = m_supernodeContainer.cbegin(); it != m_supernodeContainer.cend(); ++it)
	{ // loop through supernodes
		auto node = *it;

		if (wasChecked[node->GetId()]) continue;									//	skip nodes we've already checked
		wasChecked[node->GetId()] = true;											//	and mark this one as checked

		//		printf("Checking supernode %d\n", node);
		//qNext = m_supernodeQueueSize = 0;									//	reset the queue
		//	now, walk around the uparcs at the node
		upArc = node->upList;									//	find the first ascending arc
		nArcs = node->GetUpDegree();									//	we need this, because RemoveArc() decrements degree

		for (auto i = 0; i < nArcs; i++)									//	loop through arcs at node
		{ // loop through up arcs at node
			//			printf("Checking superarc %d\n", upArc);
			auto nextUpArc = upArc->m_nextUpArc;						//	find the next arc
			removeEdgeIfEpsilonHeight(supernodeQueue, wasChecked, upArc);								//	check the edge
			upArc = nextUpArc;											//	and walk to the next one
		} // loop through up arcs at node

		//	now, walk around the downarcs at the node
		downArc = node->downList;								//	find the first descending arc
		nArcs = node->GetDownDegree();								//	we need this, because RemoveArc() decrements degree

		for (auto i = 0; i < nArcs; i++)									//	loop through arcs at node
		{ // loop through down arcs at node
			//			printf("Checking superarc %d\n", downArc);
			auto nextDownArc =downArc->m_nextDownArc;					//	find the next arc
			removeEdgeIfEpsilonHeight(supernodeQueue, wasChecked, downArc);							//	check the edge
			downArc = nextDownArc;										//	and walk to the next one
		} // loop through down arcs at node

		//	now, as long as there is stuff on the queue
		while(!supernodeQueue.empty())
		{ //  loop through nodes on the queue
			//			printf("Checking supernode %d (%d on queue)\n", supernodeQueue[qNext], qNext);

			//	grab the next node on the queue
			//	and mark it as checked
			auto nextNode = supernodeQueue.front();
			wasChecked[nextNode->GetId()] = true;

			//	now, walk around the uparcs at the node
			upArc = nextNode->upList;							//	find the first ascending arc
			nArcs = nextNode->GetUpDegree();							//	we need this, because RemoveArc() decrements degree

			for (auto i = 0; i < nArcs; i++)								//	loop through arcs at node
			{ // loop through up arcs at node
				auto nextUpArc = upArc->m_nextUpArc;					//	find the next arc
				//				printf("Checking superarc %d\n", upArc);
				if (!removeEdgeIfEpsilonHeight(supernodeQueue, wasChecked, upArc))
				{//	check the edge
					upArc->SetBottomSupernode(node);						//	attach the top end to the collapsed supernode
				}
				upArc = nextUpArc;										//	and walk to the next one
			} // loop through up arcs at node

			//	now, walk around the downarcs at the node
			downArc = nextNode->downList;						//	find the first descending arc
			nArcs = nextNode->GetDownDegree();						//	we need this, because RemoveArc() decrements degree

			for (auto i = 0; i < nArcs; i++)								//	loop through arcs at node
			{ // loop through down arcs at node
				//				printf("Checking superarc %d\n", downArc);
				auto nextDownArc = downArc->m_nextDownArc;				//	find the next arc
				if (!removeEdgeIfEpsilonHeight(supernodeQueue, wasChecked, downArc))					//	check the edge
					downArc->SetTopSupernode(node);						//	attach the top end to the collapsed supernode
				downArc = nextDownArc;									//	and walk to the next one
			} // loop through down arcs at node

			transferArcLists(node, nextNode);								//	merge nextNode's arcs onto node's

			//	remove nextNode from the valid list
			nextNode->SetValid(false);
			//			contourTreeToDotFile("collapsed", dotFileNo++);
			//			PrintcontourTree();
		} // loop through nodes on the queue

		//	now check to see if the combined node is now a regular node
		if (node->IsRegular())
		{ // its now a regular node
			//			printf("Node %d now a regular point.  Collapsing.\n", node);
			//			contourTreeToDotFile("collapsed", dotFileNo++);
			collapseSupernodeToSuperarc(node);										//	collapse the node itself
			//			contourTreeToDotFile("collapsed", dotFileNo++);
		} // its now a regular node
	} // loop through supernodes

	//	m_nNonEpsilonArcs = GetValidArcCount();											//	store the number of non-epsilon arcs
	//	printf("Nodes: \n");
	//	for (int i = 0; i < nValidNodes; i++)
	//		printf("%3d ", validNodes[i]);
	//	printf("\n");
	//	printf("Arcs:\n");
	//	for (int i = 0; i < nValidArcs; i++)
	//		printf("%3d ", valid[i]);
	//	printf("\n");
	// release temporary memory

	//	free(m_wasChecked);
	//	free(m_supernodeQueue);

	//	contourTreeToDotFile("collapsed", dotFileNo++);
	//gettimeofday(&thisTime, NULL);
	//timingBuffer += sprintf(timingBuffer, "Epsilon-collapsed tree to %ld arcs.\n", validArcCount);
	//timingBuffer += sprintf(timingBuffer, "Collapse took %8.5f seconds\n", (Real) (thisTime.tv_sec - thatTime.tv_sec) + 1E-6 * (thisTime.tv_usec - thatTime.tv_usec));
	//	CheckNodeCounts();
}

///
/// \brief      leaf-prunes until one of the bounds is reached
/// \param      targetSize
/// \param      prunedVolumeBound
/// \since      21-11-2014
/// \author     Hamish
/// \author		Dean: update to use iterator [01-10-2015]
/// \author		Dean: pass collapse priority as a parameter [05-10-2015]
///
void ContourTree::collapseLeafPruning(const CollapsePriority& priority, unsigned long targetSize, long prunedVolumeBound)
{
	/*
	//gettimeofday(&thatTime, NULL);
	priority_queue<PriorityIndex, vector<PriorityIndex>, PriorityCompare> pQueue;	//	the priority queue
	//contourTreeToDotFile("collapsed", dotFileNo++, true);

	//	load all the leaves up on the queue
	for (auto it = m_supernodeContainer.begin(); it != m_supernodeContainer.end(); ++it)
	{
		ContourTreeSupernode* supernode = *it;

		if (supernode->IsValid())
		{
			//long node = m_validNodeArray[theNode];
			//		printf("Node %d is %d with degrees %d up and %d down\n", theNode, node, supernodes[node].upDegree, supernodes[node].downDegree);

			if (supernode->IsUpperLeaf())
			{
				// upper leaf
				//	the edge to queue up is the sole down edge
				auto theArc = supernode->downList;

				//	add to priority queue
				pQueue.push(PriorityIndex(theArc->GetId(), computeSuperarcPriority(theArc, priority, false)));
				//			printf("Adding upper leaf %d to queue with priority %d\n", theArc, priority);
			}
			else if (supernode->IsLowerLeaf())
			{
				// lower leaf
				//	the edge to queue up is the sole up edge
				auto theArc = supernode->upList;

				//	add to priority queue
				pQueue.push(PriorityIndex(theArc->GetId(), computeSuperarcPriority(theArc, priority, true)));
				//			printf("Adding lower leaf %d to queue with priority %d\n", theArc, priority);
			}
		}
	}

	//	Count the number of valid arcs in the storage container
	auto validArcCount = std::count_if(m_superarcContainer.cbegin(),
									   m_superarcContainer.cend(),
									   [](Superarc* superarc)
	{
						 return superarc->IsValid();
});

	//	this is the only one we don't know
	//	m_collapseBounds[validArcCount-1] = 1;

	//	now process leaves one at a time, until we hit our bounds
	//	stop when we have the target size in # of arcs
	while((!pQueue.empty()) && (validArcCount > targetSize))
	{
		//	retrieve the first arc on the priority queue
		PriorityIndex theIndex = pQueue.top();
		//		printf("Arc %d retrieved from queue with volume %d:\n", theIndex.index, theIndex.priority);

		//	and remove it from the priority queue
		pQueue.pop();

		//		contourTreeToDotFile("collapsed", dotFileNo++, theIndex.index);

		//	if the leaf being pruned is bigger than the target
		if (theIndex.priority > prunedVolumeBound)
		{
			//			printf("Priority %d greater than volume bound %d\n", theIndex.priority, prunedVolumeBound);
			//	then we're done, since all others are bigger
			break;
		}

		//	retrieve the arc's ID
		auto theArc = m_superarcContainer[theIndex.index];

		//	this arc was collapsed in a regular point reduction
		if (!theArc->IsValid())
		{
			//	so we ignore it and continue
			//			printf("Arc is invalid.\n");
			continue;
		}

		//	grab the nodes at each end
		ContourTreeSupernode* topNode = theArc->m_topSupernode;
		ContourTreeSupernode* bottomNode = theArc->m_bottomSupernode;

		assert (topNode != nullptr);

		ContourTreeSupernode* interiorNode;

		//		printf("Pruning arc %d\n", theArc);
		if (topNode->IsUpperLeaf())
		{ // top leaf
			//			printf("upper leaf.\n");
			if (bottomNode->GetUpDegree() == 1)						//	this is the only up-leaf at that vertex
				continue;												//	then we want to leave it in the tree
			removeArc(theArc);											//	delete the superarc

			//	delete the leaf node
			topNode->SetValid(false);

			m_collapseRecord[validArcCount] = theArc->GetId();							//	store the arc
			interiorNode = bottomNode;									//	and set the interior node
		} // top leaf
		else if (bottomNode->IsLowerLeaf())
		{ // top leaf
			//			printf("lower leaf.\n");
			if (topNode->GetDownDegree() == 1)						//	this is the only down-leaf at that vertex
				continue;												//	then we want to leave it in the tree

			removeArc(theArc);											//	delete the superarc

			//	delete the leaf node
			bottomNode->SetValid(false);

			m_collapseRecord[validArcCount] = theArc->GetId();							//	store the arc
			interiorNode = topNode;										//	and set the interior node
		} // top leaf
		else
		{ // bad edge
			printf("Major error: attempt to prune edge which is not actually a leaf.\n");
			break;
		} // bad edge
		//		m_collapseBounds[validArcCount - 1] = theIndex.priority;					//	store the priority bound (after decrement)

		//		PrintCollapseRecord();

		//	now check for regular-point collapse
		if (interiorNode->IsRegular())
		{ // regular point
			//	Collapse the vertex to a new supernode
			auto theArc = collapseSupernodeToSuperarc(interiorNode);
			//m_collapseBounds[validArcCount - 1] = theIndex.priority;				//	store the priority bound (after decrement)
			//			printf("Collapsing regular point %3d to get new arc %3d\n", interiorNode, theArc);
			//			contourTreeToDotFile("collapsed", dotFileNo++, theArc);
			topNode = theArc->m_topSupernode;								//	grab the top/bottom ID's
			bottomNode = theArc->m_bottomSupernode;

			if (topNode->IsUpperLeaf())
			{ // upper leaf
				pQueue.push(PriorityIndex(theArc->GetId(), computeSuperarcPriority(theArc, priority, false)));
				//				printf("Adding upper leaf %d to queue with priority %d\n", theArc, priority);
			} // upper leaf
			else if (bottomNode->IsLowerLeaf())
			{ // lower leaf
				pQueue.push(PriorityIndex(theArc->GetId(), computeSuperarcPriority(theArc, priority, true)));		//	add to priority queue
				//				printf("Adding upper leaf %d to queue with priority %d\n", theArc, priority);
			} // lower leaf
			m_collapseRecord[validArcCount] = theArc->GetId();							//	store the arc
			//			PrintCollapseRecord();
		} // regular point
		else if (interiorNode->IsUpperLeaf())
		{ // upper leaf
			auto theArc = interiorNode->downList;					//	the edge to queue up is the sole down edge
			pQueue.push(PriorityIndex(theArc->GetId(), computeSuperarcPriority(theArc, priority, false)));		//	add to priority queue
			//			printf("Adding upper leaf %d to queue with priority %d\n", theArc, priority);
		} // upper leaf
		else if (interiorNode->IsLowerLeaf())
		{ // lower leaf
			auto theArc = interiorNode->upList;					//	the edge to queue up is the sole up edge
			pQueue.push(PriorityIndex(theArc->GetId(), computeSuperarcPriority(theArc, priority, true)));			//	add to priority queue
			//			printf("Adding lower leaf %d to queue with priority %d\n", theArc, priority);
		} // lower leaf
		//		CheckNodeCounts();
	} // loop through priority queue

	//	printf("Collapse Bounds:\n");
	//	for (long whichCollapseBound = 0; whichCollapseBound < nNonEpsilonArcs; whichCollapseBound++)
	//		printf("%2d %6d %7.5f\n", whichCollapseBound, collapseBounds[whichCollapseBound],
	//			log((Real)collapseBounds[whichCollapseBound])/log((Real)nNonEpsilonArcs));

	//	printf("Contents of priority queue:\n");
	//	while (not pQueue.empty())
	//		{ // emptying queue
	//		PriorityIndex theIndex = pQueue.top();								//	retrieve the first arc on the priority queue
	//		pQueue.pop();													//	and remove it from the priority queue
	//		printf("Arc %d retrieved from queue with volume %d:\n", theIndex.index, theIndex.priority);
	//		} // emptying queue

	//	contourTreeToDotFile("collapsed", dotFileNo++);
	//gettimeofday(&thisTime, NULL);
	//timingBuffer += sprintf(timingBuffer, "Leaf-pruned tree to %ld arcs with priority bound of %ld.\n", validArcCount, prunedVolumeBound);
	//timingBuffer += sprintf(timingBuffer, "Pruning took %8.5f seconds\n", (Real) (thisTime.tv_sec - thatTime.tv_sec) + 1E-6 * (thisTime.tv_usec - thatTime.tv_usec));
	//	CheckNodeCounts();
	//	PrintCollapseRecord();
	*/
}

///
/// \brief      Collapses a vertex: returns the new superarc
/// \param      whichSupernode
/// \since      20-11-2014
/// \return
/// \author     Hamish
/// \author		Dean: return a pointer to the new superarc instead of the ID
///				[02-10-2015
/// \author		Dean: pass the supernode directly [03-10-2015]
///
ContourTreeSuperarc* ContourTree::collapseSupernodeToSuperarc(ContourTreeSupernode *whichSupernode)
{
	assert (whichSupernode->IsRegular());

	//	grab the existing arcs & nodes
	auto topArc = whichSupernode->upList;
	auto bottomArc = whichSupernode->downList;

	//	Grab the top and bottom supernodes
	auto topNode = topArc->GetTopSupernode();
	auto bottomNode = bottomArc->GetBottomSupernode();

	//	printf("Collapsing edges %d and %d\n", topArc, bottomArc);

	//	first remove the existing arcs
	removeArc(topArc);
	removeArc(bottomArc);

	//	and remove the vertex
	whichSupernode->SetValid(false);

	//	now add the new arc
	auto newSArc = addSuperarc(topNode, bottomNode, NULL, NULL, NULL, NULL);

	//	now fill in the hierarchical fields
	newSArc->m_collapseTopSuperarc = topArc;
	newSArc->m_collapseBottomSuperarc = bottomArc;

	newSArc->nodesThisSideOfTop = topArc->nodesThisSideOfTop;
	newSArc->nodesThisSideOfBottom = bottomArc->nodesThisSideOfBottom;

	//	this one could equally be the sum of the two parts, but this value is also defensible
	newSArc->nodesOnArc = newSArc->nodesThisSideOfTop - (m_heightfield->GetVertexCount() - newSArc->nodesThisSideOfBottom);

	//	now do the same for the Riemann sum
	newSArc->sampleSumTop = topArc->sampleSumTop;
	newSArc->sampleSumBottom = bottomArc->sampleSumBottom;
	newSArc->sampleSumOnArc = newSArc->sampleSumTop + newSArc->sampleSumBottom - m_heightfield->GetSampleSum();

	//	printf("top: %8.1f, bot: %8.1f, on: %8.1f\n", superarcs[newSArc].sampleSumTop, superarcs[newSArc].sampleSumBottom, superarcs[newSArc].sampleSumOnArc);

	return newSArc;
}

///
/// \brief      Get supernode for a vertex, creating it if necessary
/// \param      theVertex
/// \return
/// \since      21-11-2014
/// \author     Hamish
/// \author		Dean: remove m_nextSupernode member variable [02-10-2015]
/// \author		Dean: update to use pointers to supernodes and superarcs
///				instead of ID's [02-10-2015]
///
ContourTreeSupernode* ContourTree::getSupernode(Real *theVertex)
{
	//	compute its indices
	auto nodeIndex3d = m_heightfield->ComputeIndex(theVertex);

	//	lookup the node
	auto theNode = m_nodeLookup.at(nodeIndex3d);

	//	return in the easy case
	if (theNode != nullptr) return theNode;

	//	if there isn't one already, make one
	//	Get the index of the new supernode
	auto supernodeID = m_supernodeContainer.size();

	//	initialize the next one
	//	and set it in the lookup table
	ContourTreeSupernode *newSupernode = new ContourTreeSupernode(this, supernodeID, theVertex);
	m_supernodeContainer.push_back(newSupernode);
	m_nodeLookup.at(nodeIndex3d) = newSupernode;
	newSupernode->SetValid(true);

	//	increment & return the ID
	return newSupernode;
}

///
/// \brief:     Find the top and bottom supernodes
/// \param:     hiEnd
/// \param:     loEnd
/// \param:     seedHiFrom
/// \param:     seedHiTo
/// \param:     seedLoFrom
/// \param:     seedLoTo
/// \return:
/// \author:    Hamish
/// \since:     21-11-2014
/// \author		Dean: update to use pointers to supernodes and superarcs
///				instead of ID's [02-10-2015]
///
ContourTreeSuperarc* ContourTree::addSuperarc(Real *hiEnd,
								   Real *loEnd,
								   Real *seedHiFrom,
								   Real *seedHiTo,
								   Real *seedLoFrom,
								   Real *seedLoTo)
{
	//	retrieve the supernode at the top end
	auto theTopNode = getSupernode(hiEnd);

	//	printf(" to ");

	//	retrieve the supernode at the bottom end
	auto theBottomNode = getSupernode(loEnd);
	//	printf("\n");

	return addSuperarc(theTopNode, theBottomNode, seedHiFrom, seedHiTo, seedLoFrom, seedLoTo);
}

ContourTree::MergeTreePair ContourTree::constructInitialMergeTrees()
{
	//  Set the references AND reserve space in supernodes
#if defined(VERBOSE_OUTPUT) && !defined(SUPRESS_OUTPUT)
	cout << "Creating join tree." << endl;
	fflush(stdout);
#endif

	//	Generate the JoinTree.  Add the number of supernodes in the JoinTree
	//	to the global supernode count.
	JoinTree *joinTree = new JoinTree(m_heightfield);

#if defined(VERBOSE_OUTPUT) && !defined(SUPRESS_OUTPUT)
	cout << "Creating split tree." << endl;
	fflush(stdout);
#endif
	//	Generate the SplitTree.  Add the number of supernodes in the SplitTree
	//	to the global supernode count.
	SplitTree *splitTree = new SplitTree(m_heightfield, joinTree);

	return { joinTree, splitTree };
}


///
/// \brief:     Constructor
/// \param:     heightfield
/// \since:     20-11-2014
/// \author:    Dean
///
ContourTree::ContourTree(HeightField3D *heightfield)
{
#define SUPRESS_OUTPUT
	m_isReady = false;

	//  Store a local reference to the heightfield data
	m_heightfield = heightfield;

	//	allocate space on leaf queue
	//	set logical size to zero
	m_leafQueue = NULL;
	m_leafQueueSize = 0;

	//	Create the initial merge trees
	m_initialMergeTrees = constructInitialMergeTrees();

	//  Now we have the JoinTree and SplitTree
	//  we can augment and combine the trees
	//  to produce the final contour tree
#if defined(VERBOSE_OUTPUT) && !defined(SUPRESS_OUTPUT)
	cout << "Augmenting join and split trees." << endl;
	fflush(stdout);
#endif

	try
	{
		m_augmentedMergeTrees = augmentTrees(m_initialMergeTrees.joinTree,
											 m_initialMergeTrees.splitTree);

		//cout << "Non-augmented join tree:" << endl;
		//cout << m_initialMergeTrees.joinTree->ToDotString(false) << endl;
		//cout << "Augmented join tree:" << endl;
		//cout << m_augmentedMergeTrees.joinTree->ToDotString(false) << endl;

		//cout << "Non-augmented split tree:" << endl;
		//cout << m_initialMergeTrees.splitTree->ToDotString(true) << endl;
		//cout << "Augmented split tree:" << endl;
		//cout << m_augmentedMergeTrees.splitTree->ToDotString(true) << endl;


		//  For now the contour tree collapser needs to exist
		//  before we combine trees.  Maybe this could be changed?

#if defined(VERBOSE_OUTPUT) && !defined(SUPRESS_OUTPUT)
		string jtDotBefore = m_joinTree->ToDotString(false);
		string stDotBefore = m_splitTree->ToDotString(true);
		string jtDotAfter = m_joinTree->ToDotString(false);
		string stDotAfter = m_splitTree->ToDotString(true);

		cout << "About to combine join and split trees." << endl;
		cout << "Join Tree (before combination):"  << endl;
		cout << jtDotBefore << endl;
		cout << "Split Tree (before combination):"  << endl;
		cout << stDotBefore << endl;
		fflush(stdout);
#endif

		//  Combines the augmented contour trees
		combineTrees(m_augmentedMergeTrees.joinTree,
					 m_augmentedMergeTrees.splitTree);

#if defined(VERBOSE_OUTPUT) && !defined(SUPRESS_OUTPUT)
		cout << "Completed combining join and split trees.\n" << endl;
		cout << "Join Tree (after combination):"  << endl;
		cout << jtDotAfter << endl;
		cout << "Split Tree (after combination):"  << endl;
		cout << stDotAfter << endl;
		cout << "Join tree is unchanged by algorithm: ";
		cout << (jtDotBefore == jtDotAfter ? "true" : "false") << endl;
		cout << "Split tree is unchanged by algorithm: ";
		cout << (stDotBefore == stDotAfter ? "true" : "false") << endl;
		fflush(stdout);
#endif

		//  Contour tree with Height bar down left side
#ifdef OUTPUT_CONTOUR_TREE_MORE
		SaveContourTreeToDotFile(CONTOUR_TREE_MORE_FILENAME, true);
#endif

		//  Contour tree showing only critical nodes
#ifdef OUTPUT_CONTOUR_TREE_LESS
		SaveContourTreeToDotFile(CONTOUR_TREE_LESS_FILENAME, false);
#endif

#ifdef CHECK_FILES_MATCH_REFERENCE_DATA
		cout << "Testing files using external tool..." << endl;
		fflush(stdout);

		system("cd debug && comp_files.bat");
#endif

#if defined(VERBOSE_OUTPUT) && !defined(SUPRESS_OUTPUT)
		cout << "Initializing collapsible contour tree." << endl;
		fflush(stdout);
#endif
		{
			//  Currently this cannot be called from the
			//  CollapsibleContourTree constructor as
			//  some of the initializer code is handled
			//  indirectly by the ContourTree::combineTrees
			//  function.
			//	collapses epsilon edges
			//  CollapseEpsilonEdges();
			//m_collapseRecord = (long *) calloc(m_nNonEpsilonArcs, sizeof(long));				//	allocate array for collapse record
			//m_collapseBounds = (long *) calloc(m_nNonEpsilonArcs, sizeof(long));				//	allocate array for collapse bounds

			//	collapsePriority = PRIORITY_HYPERVOLUME;
			//	CollapseLeafPruning(1, nVertices);


		}

#if defined(VERBOSE_OUTPUT) && !defined(SUPRESS_OUTPUT)
		cout << "Calculating distribution of nodes and arcs in contour tree.";
		cout << endl;
		fflush(stdout);
#endif

		//  Calculate and store distributions...
		m_supernodeDistribution = new SupernodeDistribution(this);
		m_superarcDistribution = new SuperarcDistribution;
		m_superarcDistribution->AddContourTree(this, true);

#if defined(VERBOSE_OUTPUT) && !defined(SUPRESS_OUTPUT)
		cout << "Setting X position of nodes." << endl;
		fflush(stdout);
#endif

		//  Set the position of the nodes in the contour tree graphic
		//SetNodePositions();

#if defined(VERBOSE_OUTPUT) && !defined(SUPRESS_OUTPUT)
		cout << "Setting selection root etc." << endl;
		fflush(stdout);
#endif

#if defined(VERBOSE_OUTPUT) && !defined(SUPRESS_OUTPUT)
		cout << "Contour tree was sucessfully created." << endl;
		fflush(stdout);
#endif

		m_isReady = true;
	}
	catch (exception &ex)
	{
		cerr << "An exception was caught whilst creating the contour tree.";
		cerr << endl;
		cerr << "The exception reads: " <<  ex.what() << endl;
		fflush(stderr);
	}
}

///
/// \brief:     Destructor
/// \author:    Dean
/// \since:     21-11-2014
///
ContourTree::~ContourTree()
{
	if (m_leafQueue != NULL)
		free(m_leafQueue);
	m_leafQueue = NULL;

	while (!m_superarcContainer.empty())
	{
		delete m_superarcContainer.back();
		m_superarcContainer.pop_back();
	}

	while (!m_supernodeContainer.empty())
	{
		delete m_supernodeContainer.back();
		m_supernodeContainer.pop_back();
	}

	if (m_supernodeDistribution != nullptr)
		delete m_supernodeDistribution;
	m_supernodeDistribution = nullptr;

	if (m_superarcDistribution != nullptr)
		delete m_superarcDistribution;
	m_superarcDistribution = nullptr;
}

///
/// \brief:     Routine to remove an arc from the tree
/// \param:     theArc
/// \since:     20-11-2014
/// \author:    Hamish
///
void ContourTree::removeArc(ContourTreeSuperarc* superarc)
{
	//	retrieve top & bottom of arc
	auto topSupernode = superarc->GetTopSupernode();
	auto bottomSupernode = superarc->GetBottomSupernode();

	auto nextDownSuperarc = superarc->m_nextDownArc;
	auto lastDownSuperarc = superarc->m_lastDownArc;

	auto nextUpSuperarc = superarc->m_nextUpArc;
	auto lastUpSuperarc = superarc->m_lastUpArc;

	//	first, remove it at the high end
	//	check to see if there are any other edges at this end
	if (nextDownSuperarc == superarc)
	{
		//	if it's the only down edge at this node
		//	make it point to NO_SUPERARC
		topSupernode->downList = nullptr;
	}
	else
	{
		//	make sure the entry pointer stays valid
		if (topSupernode->downList == superarc)
		{
			topSupernode->downList = nextDownSuperarc;
		}

		lastDownSuperarc->m_nextDownArc = nextDownSuperarc;
		nextDownSuperarc->m_lastDownArc = lastDownSuperarc;
	}

	// now do the same at the low end
	if (nextUpSuperarc == superarc)
	{
		//	if it's the only up edge at this node
		//	make it point to NO_SUPERARC
		bottomSupernode = nullptr;
	}
	else
	{
		//	make sure the entry pointer stays valid
		if (bottomSupernode->upList == superarc)
		{
			bottomSupernode->upList = nextUpSuperarc;
		}

		lastUpSuperarc->m_nextUpArc = nextUpSuperarc;
		nextUpSuperarc->m_lastUpArc = lastUpSuperarc;
	}

	topSupernode->m_downDegree--;
	bottomSupernode->m_upDegree--;

	//	and remove the arc from the list of valid arcs
	superarc->SetValid(false);
}

///
/// \brief      adds a superarc to the contour tree
/// \param      topID
/// \param      bottomID
/// \param      seedHiFrom
/// \param      seedHiTo
/// \param      seedLoFrom
/// \param      seedLoTo
/// \return
/// \since      20-11-2014
/// \author     Hamish
/// \author		Dean: store pointers to the supernodes in the arc [16-02-2015]
/// \author		Dean: remove m_nextSuperarc member variable [02-10-2015]
/// \author		Dean: update to use pointers to supernodes and superarcs
///				instead of ID's [02-10-2015]
///
ContourTreeSuperarc* ContourTree::addSuperarc(ContourTreeSupernode* topSupernode,
								   ContourTreeSupernode* bottomSupernode,
								   Real *seedHiFrom,
								   Real *seedHiTo,
								   Real *seedLoFrom,
								   Real *seedLoTo)
{
	//	What will the numeric identity be of this superarc
	auto superarcIndex = m_superarcContainer.size();

	//	initialize the new superarc and set the seeds.  We now also store a
	//	pointer to the two nodes used to define the arc.
	ContourTreeSuperarc *newSuperarc = new ContourTreeSuperarc(this,
										 superarcIndex,
										 topSupernode,
										 bottomSupernode);
	newSuperarc->SetHighSeed(seedHiFrom, seedHiTo);
	newSuperarc->SetLowSeed(seedLoFrom, seedLoTo);

	//	Add to the storage container
	m_superarcContainer.push_back(newSuperarc);

	//	now add the arcs to the nodes at each end
	//	insert at top end
	//	and at bottom end
	addDownArc(topSupernode, newSuperarc);
	addUpArc(bottomSupernode, newSuperarc);

	//	Return the new superarc
	return newSuperarc;
}

///
/// \brief      Saves the Contour Tree to a 'dot' file, listing all possible IsoValues
/// \param      dotFile
/// \param      highlightEdge
/// \since      27-11-2014
/// \author     Hamish: original implementation
/// \author     Dean: moved to separate method
///	\author		Dean: use iterators [29-09-2015]
///
string ContourTree::ToDOT(const bool &fullTree) const
{
	stringstream result;

	//	Header
	result << "digraph G {\n";

	if (fullTree)
	{
		//	put a list down the side of isovalues (255 -> 0)
		for (long i = 255; i > 0; i--)
			result << "\t" << i << " -> " << i-1 << "\n";

		for (auto it = m_superarcContainer.cbegin();
			 it != m_superarcContainer.cend(); ++it)
		{
			ContourTreeSuperarc *superarc = (*it);

			if (superarc->IsValid())
			{
				result << "\tv" << superarc->GetTopSupernode()->GetId();
				result << " -> v" << superarc->GetBottomSupernode()->GetId();
				result << " [label=\"e" << superarc->GetId();
				result << "\"]\n";
			}
		}

		for (long i = 255; i >= 0; i--)
		{
			result << "\t{ rank = same; " << i << "; ";

			for (auto it = m_supernodeContainer.cbegin();
				 it != m_supernodeContainer.cend(); ++it)
			{
				ContourTreeSupernode* supernode = (*it);
				if (supernode->IsValid())
				{
					int nodeVal = (int) (255.0 * (*(supernode->GetScalarVertex()) - m_heightfield->GetMinHeight()) / (m_heightfield->GetMaxHeight() - m_heightfield->GetMinHeight()));
					if (nodeVal == i)
						result << "v" << supernode->GetId() << "; ";
				}
			}

			result << "}\n";
		}
	}
	else
	{
		for (auto it = m_superarcContainer.cbegin();
			 it != m_superarcContainer.cend(); ++it)
		{
			ContourTreeSuperarc *superarc = (*it);

			if (superarc->IsValid())
			{
				ContourTreeSupernode* topNode = superarc->GetTopSupernode();
				ContourTreeSupernode* bottomNode = superarc->GetBottomSupernode();

				//unsigned long i = m_validArcArray[arc];
				size_t x1, y1, z1, x2, y2, z2;
				m_heightfield->ComputeIndex(topNode->GetScalarVertex(), x1, y1, z1);
				m_heightfield->ComputeIndex(bottomNode->GetScalarVertex(), x2, y2, z2);
				//			fprintf(dotFile, "\t\"(%ld,%ld,%ld)\" -> \"(%ld,%ld,%ld)\" [label=\"%ld\", taillabel = \"%ld\", headlabel=\"%ld\"];\n",
				//				x1, y1, z1, x2, y2, z2, superarcs[i].nodesOnArc, superarcs[i].nodesThisSideOfTop, superarcs[i].nodesThisSideOfBottom);

				result << "\tv" << superarc->GetTopSupernode()->GetId() << " -> ";
				result << "v" << superarc->GetBottomSupernode()->GetId() << " ";
				result << "[label=\"e" << superarc->GetId();
				result << "[" << superarc->nodesOnArc << "]";
				result << ((*(topNode->GetScalarVertex()) == *(bottomNode->GetScalarVertex())) ? "(E)" : "");
				result << "\", taillabel = \"" << superarc->nodesThisSideOfTop;
				result << "\", headlabel=\"" << superarc->nodesThisSideOfBottom;
				result << "\"]\n";
			}
		}
	}

	//	Footer
	result << "}\n";

	return result.str();
}

///
/// \brief      Prints out the contour tree, once it is fully computed
/// \param      basefilename
/// \param      serialNum
/// \param      postscriptOutput
/// \param      forceYPosition
/// \param      highlightEdge
/// \since      20-11-2014
/// \author     Hamish - original implementation
/// \author     Dean - split code to save the two different graphs [27-11-2014]
///
void ContourTree::SaveContourTreeToDotFile(const std::string &basefilename,
										   const bool &fullTree) const
{
	//  Header is the same regardless of level of detail
	FILE *dotFile = fopen(basefilename.c_str(), "w");
	if (dotFile == NULL)
	{
		fprintf(stderr,"Unable to write to dotfile %s\n", basefilename.c_str());
		fflush(stderr);
		return;
	}

	//  Switch based upon the requested level of detail
	if (fullTree)
	{
		fprintf(dotFile, "%s", ToDOT(true).c_str());
	}
	else
	{
		fprintf(dotFile, "%s", ToDOT(false).c_str());
	}

	fclose(dotFile);
}

///
/// \brief      Add a down arc at a given node
/// \param      theNode
/// \param      theArc
/// \since      20-11-2014
/// \author     Hamish
/// \author		Dean: update to use pointers to supernodes and superarcs
///				instead of ID's [02-10-2015]
///
void ContourTree::addDownArc(ContourTreeSupernode* theNode, ContourTreeSuperarc* theArc)
{
	//	grab the top of the list
	auto topOfList = theNode->downList;

	//	if there are no down arcs
	if (topOfList == nullptr)
	{
		//	make it point to itself
		//	and set the list up
		theArc->m_nextDownArc = theArc;
		theArc->m_lastDownArc = theArc;
		theNode->downList = theArc;
	}
	else
	{
		//	grab the old "last" arc
		auto oldLast = topOfList->m_lastDownArc;

		//	point this one at it
		//	and vice versa
		theArc->m_lastDownArc = oldLast;
		oldLast->m_nextDownArc = theArc;

		//	do the same for the top of the list
		topOfList->m_lastDownArc = theArc;
		theArc->m_nextDownArc = topOfList;
	}

	theNode->m_downDegree++;
}

///
/// \brief      Add an up arc at a given node
/// \param      theNode
/// \param      theArc
/// \since      20-11-2014
/// \author     Hamish
/// \author		Dean: update to use pointers to supernodes and superarcs
///				instead of ID's [02-10-2015]
///
void ContourTree::addUpArc(ContourTreeSupernode *theNode, ContourTreeSuperarc* theArc)
{
	//	grab the top of the list
	auto topOfList = theNode->upList;

	//	if there are no up arcs
	if (topOfList == nullptr)
	{
		//	make it point to itself
		//	and set the list up
		theArc->m_nextUpArc = theArc;
		theArc->m_lastUpArc = theArc;
		theNode->upList = theArc;
	}
	else
	{
		//	grab the old "last" arc
		auto oldLast = topOfList->m_lastUpArc;

		//	point this one at it
		//	and vice versa
		theArc->m_lastUpArc = oldLast;
		oldLast->m_nextUpArc = theArc;

		//	do the same for the top of the list
		topOfList->m_lastUpArc = theArc;
		theArc->m_nextUpArc = topOfList;
	}

	theNode->m_upDegree++;
}

///
/// \brief      Augments join and split trees with nodes of other
/// \details    does the following
///             A.	Allocate space for the leaf queue, contourComponent, and the contour tree
///             B.	Augment the join and split trees and construct the leaf queue:  loop through vertices from high to low
///                 i.	if a vertex is not in the split tree, but is in the join tree
///                     a.	add to the split tree
///                     b.	if the vertex is degree 1 in the join tree
///                         1.	add to leaf queue
///                 ii.	if a vertex is not in the join tree, but is in the split tree
///                     a.	add to the join tree
///                     b.	if the vertex is degree 1 in the split tree
///                         1.	add to leaf queue
/// \since      20-11-2014
/// \author     Hamish
///
ContourTree::MergeTreePair ContourTree::augmentTrees(const JoinTree *joinTree,
													 const SplitTree *splitTree)
{
#define SUPRESS_OUTPUT
	assert (joinTree != nullptr);
	assert (splitTree != nullptr);

	JoinTree *augmentedJoinTree = joinTree->Clone();
	SplitTree *augmentedSplitTree = splitTree->Clone();

	//gettimeofday(&thatTime, NULL);
	// 	timingBuffer += sprintf(timingBuffer, "Starting augmentation of trees at %ld: %ld\n", thatTime.tv_sec % 1000, thatTime.tv_usec);
	// 	flushTimingBuffer();

	//	A.	Allocate space for the leaf queue, contourComponent, and the contour tree
	//	set index to next available superarc
	//	for a tree, this is always true

	//	Calculate how many supernodes are going to be required
	auto requiredSupernodeCount = 0;
	requiredSupernodeCount += joinTree->SuperNodesInContourTree();
	requiredSupernodeCount += splitTree->SuperNodesInContourTree();

	//	we need to add one because the last one will be added twice (due to pts at inf)
	m_leafQueue = (Real **) malloc((sizeof(Real *) * (1 + requiredSupernodeCount)));

	//	no nodes on the leaf queue (yet)
	m_leafQueueSize = 0;


	try
	{

		//	B.	Augment the join and split trees and construct the leaf queue:
		//		loop through vertices from high to low
		for (long vertexIndex = m_heightfield->GetVertexCount() - 1; vertexIndex >= 0; vertexIndex--)
		{ // B.

#if defined(VERBOSE_OUTPUT) && !defined(SUPRESS_OUTPUT)
			printf("Join Tree: \n");
			joinTree->printJoinTree();
			printf("Split Tree: \n");
			splitTree->printSplitTree();
#endif
			//size_t x, y, z;														//	coordinates of vertex
			Real *theVertex;													//	pointer to vertex in data set
			Component *jComp, *sComp;											//	pointers to component in join & split trees

			theVertex = m_heightfield->GetSortedHeight(vertexIndex);										//	retrieve the vertex from the sort list
			Index3d vertexIndex3d = m_heightfield->ComputeIndex(theVertex);								//	compute its indices
			jComp = joinTree->m_componentArray3D.at(vertexIndex3d);										//	retrieve the corresponding join component
			sComp = splitTree->m_componentArray3D.at(vertexIndex3d);									//	and the corresponding split component

			//		 i.	if a vertex is not in the split tree but is in the join tree
			//			this test suffices for two purposes:	a.	augmenting the split tree	(for obvious reasons)
			//										b.	to help find upper leaves, which can be neither splits nor local minima, and are therefore not in the split tree
			if ((sComp->m_loEnd != theVertex) && (jComp->m_hiEnd == theVertex))			//	if the component did not start at this vertex, it's not in the split tree
			{ // B.i.
				//			a.	add to the split tree
				//				to do this, I splice an additional component on to the top end of the existing component: thus, the existing component is valid for future tests
				Component *splice = new Component;								//	create it

				//	this only happens at plus infinity
				if (sComp->m_nextHi == sComp)
				{
					//	set the ends of the splice
					splice->m_hiEnd = sComp->m_hiEnd;
					splice->m_loEnd = theVertex;

					//	link it to itself
					splice->m_nextHi = splice;
					splice->m_lastHi = splice;

					//	make sure that any seed is in the top component
					splice->m_seedFrom = sComp->m_seedFrom;
					splice->m_seedTo = sComp->m_seedTo;

					//	set up circular list around the vertex
					splice->m_nextLo = sComp;
					splice->m_lastLo = sComp;

					//	reset the upper end of the existing component
					sComp->m_hiEnd = theVertex;

					//	make sure that any seed is in the top component
					sComp->m_seedFrom = nullptr;
					sComp->m_seedTo = nullptr;

					//	above and below
					sComp->m_nextHi = splice;
					sComp->m_lastHi = splice;

				}
				else
				{
					// else sComp isn't highest arc
					//	set the ends of the splice
					splice->m_hiEnd = sComp->m_hiEnd;
					splice->m_loEnd = theVertex;

					//	grab the circular edge-list pointers from sComp
					splice->m_nextHi = sComp->m_nextHi;
					splice->m_lastHi = sComp->m_lastHi;

					//	this happens if the next edge around hiEnd is upwards
					if (splice->m_nextHi->m_loEnd == splice->m_hiEnd)
					{
						//	so reset the low-end circular list there
						splice->m_nextHi->m_lastLo = splice;
					}
					//	i.e. next edge around hiEnd is also downwards
					else
					{
						//	reset the high-end circular list there
						splice->m_nextHi->m_lastHi = splice;
					}

					//	basically the same as before, only in backwards direction
					if (splice->m_lastHi->m_loEnd == splice->m_hiEnd)
					{
						//	reset the lo pointer
						splice->m_lastHi->m_nextLo = splice;
					}
					else
					{
						//	reset the hi pointer
						splice->m_lastHi->m_nextHi = splice;
					}

					splice->m_seedFrom = sComp->m_seedFrom;
					splice->m_seedTo = sComp->m_seedTo;		//	make sure that any seed is in the top component
					splice->m_nextLo = sComp;
					splice->m_lastLo = sComp;							//	set up circular list around the vertex

					sComp->m_hiEnd = theVertex;									//	reset the upper end of the existing component
					sComp->m_seedFrom = nullptr;
					sComp->m_seedTo = nullptr;								//	make sure that any seed is in the top component
					sComp->m_nextHi = splice;
					sComp->m_lastHi = splice;								//	above and below

				} // else sComp isn't highest arc

				augmentedSplitTree->m_componentArray3D.at(vertexIndex3d) = splice;									//	and set up the link in the split tree so that we can retrieve the edge-list
				augmentedSplitTree->supernodeCount++;

				//			b.	if the vertex is degree 1 in the join tree
				if (jComp->m_nextHi == jComp)									//	only one edge in the circular list at the upper end
					//				1.	add to leaf queue
				{ // B. i. 1.
					//				printf("Adding leaf %2ld (%p) to leaf queue\n", leafQSize, theVertex);
					m_leafQueue[m_leafQueueSize++] = theVertex;							//	add the vertex to the leaf queue
				} // B. i. 1.
			} // B. i.
			//		 ii.	if a vertex is not in the join tree, but is in the split tree: see above for explanation
			if ((jComp->m_hiEnd != theVertex) && (sComp->m_loEnd == theVertex))			//	if the component did not start at this vertex, it's not in the join tree
			{ // B.ii.
				//			a.	add to the join tree
				//				to do this, I splice an additional component on to the top end of the existing component: thus, the existing component is valid for future tests
				Component *splice = new Component;								//	create it
				splice->m_hiEnd = jComp->m_hiEnd;	splice->m_loEnd = theVertex;				//	set the ends of the splice
				jComp->m_hiEnd = theVertex;										//	reset the upper end of the existing component

				if (jComp->m_nextHi == jComp)									//	this happens at local maxima
					splice->m_nextHi = splice->m_lastHi = splice;							//	link it to itself
				else														//	any other case
				{ // else sComp isn't highest arc
					splice->m_nextHi = jComp->m_nextHi;
					splice->m_lastHi = jComp->m_lastHi;		//	grab the circular edge-list pointers from sComp
					//	this next chunk is simpler than above, since join tree always has down-degree of 1
					splice->m_nextHi->m_lastLo = splice;								//	so reset the low-end circular list there
					splice->m_lastHi->m_nextLo = splice;								//	reset the lo pointer
				} // else sComp isn't highest arc

				splice->m_nextLo = splice->m_lastLo = jComp;							//	set up circular list around the vertex
				jComp->m_nextHi = jComp->m_lastHi = splice;							//	above and below

				size_t xx, yy, zz;												//	used for grabbing upper end

				m_heightfield->ComputeIndex(splice->m_hiEnd, xx, yy, zz);						//	compute index of upper end
				augmentedJoinTree->m_componentArray3D(xx, yy, zz) = splice;								//	and set pointer into edge-list for next stage
				augmentedJoinTree->supernodeCount++;

				//			b.	if the vertex is degree 1 in the split tree
				if (sComp->m_nextLo == sComp)									//	only one edge in the circular list at the lower end
					//				1.	add to leaf queue
				{ // B.ii.b.1.
					//				printf("Adding leaf %2ld (%p) to leaf queue\n", leafQSize, theVertex);
					m_leafQueue[m_leafQueueSize++] = theVertex;							//	add the vertex to the leaf queue
				}
			}
		}
	}
	catch (exception &ex)
	{
		fprintf(stderr,
				"An exception was caught whilst Augmenting trees.\n");
		fprintf(stderr, "The exception reads:\n%s\n", ex.what());
		fflush(stderr);

		//	Makes sure this gets flagged in the calling method
		throw runtime_error("An error prevented the Augmented Contour tree from being created.");
	}
	return { augmentedJoinTree, augmentedSplitTree };

#undef SUPRESS_OUTPUT
}

///
/// \brief      Combines join & split trees to produce contour tree
/// \details    Does the following:
///             C.	Loop through the leaves on the leaf queue
///                 i.	if upper leaf
///         			a.	add to contour tree
///                     b.	delete from join tree
///                     c.	delete from split tree
///                     d.	test other end to see if it should be added to leaf queue
///                 ii.	if lower leaf
///                     a.	add to contour tree
///                     b.	delete from split tree
///                     c.	delete from join tree
///                 d.	test other end to see if it should be added to leaf queue
///             D.	Clean up excess memory, &c.
/// \author     Hamish
/// \since      20-11-2014
///
void ContourTree::combineTrees(const JoinTree *joinTree, const SplitTree *splitTree)
{
#define SUPRESS_OUTPUT

	assert(joinTree != nullptr);
	assert(splitTree != nullptr);

	//	Create copies of the merge trees for us to work with
	JoinTree *tempJoinTree = joinTree->Clone();
	SplitTree *tempSplitTree = splitTree->Clone();

	size_t xNext, yNext, zNext;											//	for walking down the arcs

	//	For keeping track of heightfield vertices already visited in the
	//	algorithm
	Storage::Vector3<bool> isVisited(m_heightfield->GetBaseDimX(),
									 m_heightfield->GetBaseDimY(),
									 m_heightfield->GetBaseDimZ());


	//	build array of supernode ID's
	m_nodeLookup = Storage::Vector3<ContourTreeSupernode*>(m_heightfield->GetBaseDimX(),
												m_heightfield->GetBaseDimY(),
												m_heightfield->GetBaseDimZ());

#if defined(VERBOSE_OUTPUT) && !defined(SUPRESS_OUTPUT)
	cout << "About to start merge loop." << endl;
	cout << "Join Tree: " << endl;
	//_tempJoinTree->printJoinTree();
	cout << "Join Components: " << endl;
	cout << tempJoinTree->componentArrayToString(false) << endl;
	cout << "Split Tree: " << endl;
	//_m_splitTree->printSplitTree();
	cout << "Split Components: " << endl;
	cout <<  m_splitTree->componentArrayToString(false) << endl;
	//printf("Leaf Queue: \n");
	//PrintLeafQueue();
	fflush(stdout);
#endif


#if defined(VERBOSE_OUTPUT) && !defined(SUPRESS_OUTPUT)
	cout << "Initializing node lookup." << endl;
	fflush(stdout);
#endif

	//	walk through each dimension
	for (auto i = 0; i < m_heightfield->GetBaseDimX(); i++)
	{
		for (auto j = 0; j < m_heightfield->GetBaseDimY(); j++)
		{
			for (auto k = 0; k < m_heightfield->GetBaseDimZ(); k++)
			{
				//	setting the ID to predictable value
				m_nodeLookup.at(i, j, k) = nullptr;

				//	and set the "visit" flag, too
				isVisited.at(i, j, k) = false;
			}
		}
	}

#if defined(VERBOSE_OUTPUT) && !defined(SUPRESS_OUTPUT)
	cout << "Looping through leaf queue." << endl;
	fflush(stdout);
#endif

	try
	{
		//	Calculate how many supernodes are going to be required
		//	Number of superarcs will be the total number of supernodes in the
		//	two merge trees - 1.
		auto requiredSuperarcCount = 0;
		requiredSuperarcCount += joinTree->SuperNodesInContourTree();
		requiredSuperarcCount += splitTree->SuperNodesInContourTree();
		--requiredSuperarcCount;

		//	C.	Loop through the leaves on the leaf queue
		for (auto vertexIndex = 0;
			 vertexIndex < requiredSuperarcCount;
			 vertexIndex++)
		{ // C.
			//	retrieve the vertex from the leaf queue
			auto theVertex = m_leafQueue[vertexIndex];

			//	coordinates of vertex
			auto vertexPos3d = m_heightfield->ComputeIndex(theVertex);

			//	retrieve the corresponding join component
			//	and the corresponding split component
			auto jComp = tempJoinTree->m_componentArray3D.at(vertexPos3d);
			auto sComp = tempSplitTree->m_componentArray3D.at(vertexPos3d);

			//		i.	if upper leaf
			if (jComp->m_nextHi == jComp)										//	since we already know that it's a leaf, this suffices
			{ // C. i.
				//			a.	add to contour tree
				ContourTreeSuperarc* newSuperarc = nullptr;

				//	grab a pointer to the other end
				auto otherEnd = jComp->m_loEnd;

				//	if there wasn't a seed stored on the edge
				if (jComp->m_seedFrom == nullptr)
				{
					newSuperarc = addSuperarc(theVertex,
											  otherEnd,
											  theVertex,
											  m_heightfield->findDescendingNeighbour(theVertex, m_heightfield->GetUntransformedHeightArray()),
											  nullptr,
											  nullptr);
				}
				else
				{
					newSuperarc = addSuperarc(theVertex,
											  otherEnd,
											  nullptr,
											  nullptr,
											  jComp->m_seedFrom,
											  jComp->m_seedTo);
				}

				assert(newSuperarc != nullptr);

				//	now compute the sum of the nodes this side of the top end: initial -1 excludes the vertex proper
				newSuperarc->nodesThisSideOfTop = newSuperarc->nodesThisSideOfBottom = m_heightfield->GetVertexCount() - 1;
				newSuperarc->sampleSumTop = m_heightfield->GetSampleSum() - *theVertex;

				for (auto downArc = newSuperarc->m_nextDownArc->GetId();
					 downArc != newSuperarc->GetId();
					 downArc = m_superarcContainer[downArc]->m_nextDownArc->GetId())
				{ // other downarcs exist
					newSuperarc->nodesThisSideOfTop -= m_superarcContainer[downArc]->nodesThisSideOfTop;
					newSuperarc->sampleSumTop -= m_superarcContainer[downArc]->sampleSumTop;
					//				ContourTreeToDotFile("building", dotFileNo++);
				} // other downarcs exist

				//	up arcs: first we have to check if any exist
				auto upArc = newSuperarc->GetTopSupernode()->upList;
				if (upArc != nullptr)
				{ // there are uparcs
					do
					{ // walk around them
						newSuperarc->nodesThisSideOfTop -= upArc->nodesThisSideOfBottom;
						newSuperarc->sampleSumTop -= upArc->sampleSumBottom;
						upArc = upArc->m_nextUpArc;
					} // walk around them
					while (upArc != newSuperarc->GetTopSupernode()->upList);
				} // there are uparcs

				//	set flag for the vertex to mark "visited"
				isVisited.at(vertexPos3d) = true;

				for (Real *nextNode = tempJoinTree->m_arcLookupArray3D.at(vertexPos3d);
					 nextNode != otherEnd;
					 nextNode = tempJoinTree->m_arcLookupArray3D(xNext, yNext, zNext))
				{ // nextNode											//	walk along join arcs to transfer nodes
					//	find the x, y, z indices for the next step
					auto temp = m_heightfield->ComputeIndex(nextNode);
					xNext = temp.x;
					yNext = temp.y;
					zNext = temp.z;
					//				printf("Walking past (%1d, %1d, %1d): %8.5f\n", xNext, yNext, zNext, *nextNode);

					if (isVisited.at(temp) == false)					//	1 => vertex is not yet on a superarc
					{ // first time this vertex has been processed
						//					printf(": counting.");

						//	set to 0 to mark that its used, and reset flags for rendering
						isVisited.at(temp) = true;

						//	find the x, y, z indices
						auto temp2 = m_heightfield->ComputeIndex(nextNode);
						xNext = temp2.x;
						yNext = temp2.y;
						zNext = temp2.z;

						newSuperarc->nodesOnArc++;					//	increment the superarcs node count
						newSuperarc->sampleSumOnArc += m_heightfield->GetHeightAt(xNext, yNext, zNext);
					} // first time this vertex has been processed
					//				printf("\n");
				} // nextNode
				//	compute the count of nodes this side of the bottom
				newSuperarc->nodesThisSideOfBottom = newSuperarc->nodesOnArc + (m_heightfield->GetVertexCount() - newSuperarc->nodesThisSideOfTop);
				newSuperarc->sampleSumBottom = m_heightfield->GetSampleSum() - newSuperarc->sampleSumTop +newSuperarc->sampleSumOnArc;

				//			b.	delete from join tree
				//				note that otherEnd is degree 2 or higher (i.e. it is guaranteed to have a downarc, even for last edge, because of existence of -infinity
				if (jComp->m_nextLo->m_hiEnd == otherEnd)							//	if the next edge at low end is downwards
					jComp->m_nextLo->m_lastHi = jComp->m_lastLo;						//	reset its lastHi pointer
				else
					jComp->m_nextLo->m_lastLo = jComp->m_lastLo;						//	otherwise reset the lastLo pointer

				if (jComp->m_lastLo->m_hiEnd == otherEnd)							//	if the last edge at low end is downwards
					jComp->m_lastLo->m_nextHi = jComp->m_nextLo;						//	reset its nextHi pointer
				else
					jComp->m_lastLo->m_nextLo = jComp->m_nextLo;						//	otherwise reset the nextLo pointer

				delete jComp;												//	get rid of the jComp edge

				//	get rid of it in the jComponent array as well
				tempJoinTree->m_componentArray3D.at(vertexPos3d) = nullptr;
				tempJoinTree->supernodeCount--;

				//			c.	delete from split tree
				//				since we have +infinity, there will always be an up and a down arc
				//				this is true even for the last edge, since we start at an upper leaf U.  This means that there is a down-arc to the other remaining node, L.
				//				and since sComp is the departing edge travelling upwards, UL must be downwards in the split tree.
				//				all we do is collapse sComp & sComp->nextLo onto sComp->nextLo
				if (sComp->m_nextHi == sComp)									//	i.e. sComp leads to +inf
					sComp->m_nextLo->m_nextHi = sComp->m_nextLo->m_lastHi = sComp->m_nextLo;	//	set the upper arcs to itself (i.e. no higher neighbours)
				else	//	not an edge to +inf
				{ // not edge to +inf
					sComp->m_nextLo->m_nextHi = sComp->m_nextHi;						//	set the upper arcs
					sComp->m_nextLo->m_lastHi = sComp->m_lastHi;
				} //  not edge to +inf

				sComp->m_nextLo->m_hiEnd = sComp->m_hiEnd;							//	transfer the high end
				sComp->m_nextLo->m_seedFrom = sComp->m_seedFrom;						//	transfer the seed as well
				sComp->m_nextLo->m_seedTo = sComp->m_seedTo;							//	transfer the seed as well

				if (sComp->m_nextHi->m_loEnd == sComp->m_hiEnd)						//	if nextHi is an up-arc
					sComp->m_nextHi->m_lastLo = sComp->m_nextLo;						//	adjust the low end
				else
					sComp->m_nextHi->m_lastHi = sComp->m_nextLo;						//	otherwise the high end

				if (sComp->m_lastHi->m_loEnd == sComp->m_hiEnd)						//	if lastHi is an up-arc
					sComp->m_lastHi->m_nextLo = sComp->m_nextLo;						//	adjust the low end
				else
					sComp->m_lastHi->m_nextHi = sComp->m_nextLo;						//	otherwise the high end

				//	delete the now-useless edge
				delete sComp;

				//	get rid of it in the sComponent array as well
				tempSplitTree->m_componentArray3D.at(vertexPos3d) = NULL;
				tempSplitTree->supernodeCount--;

				//			d.	test other end to see if it should be added to leaf queue
				//			we have reduced the up-degree of otherEnd in the join tree, and haven't changed the degrees in the split tree
				//	compute indices
				Index3d otherPos3d = m_heightfield->ComputeIndex(otherEnd);

				//	retrieve the split component
				//	and the join component
				sComp = tempSplitTree->m_componentArray3D.at(otherPos3d);
				jComp = tempJoinTree->m_componentArray3D.at(otherPos3d);

				if (	(	(jComp->m_nextHi == jComp)								//	two ways otherEnd can be a leaf:  upper
							&& 	(sComp->m_nextLo->m_nextHi == sComp)		)				//		(jComp has no "higher end" nbr, sComp has one each way)
						|| (	(	(sComp->m_nextLo == sComp)							//	or lower
									&& 	(jComp->m_nextHi->m_nextLo == jComp)		)	)	)		//		(sComp has no "lower end" nbr, jComp has one each way)
				{
					//				printf("Adding leaf %2ld (%p) to leaf queue\n", leafQSize, otherEnd);
					m_leafQueue[m_leafQueueSize++] = otherEnd;							//	so add it already
				}
			} // C. i.
			else // i.e. a lower leaf
			{
				//	a.	add to contour tree
				ContourTreeSuperarc* newSuperarc = nullptr;

				//	grab a pointer to the other end
				auto otherEnd = sComp->m_hiEnd;

				if (sComp->m_seedFrom == nullptr)									//	if there wasn't a seed stored on the edge
					newSuperarc = addSuperarc(otherEnd,
											  theVertex,
											  otherEnd,
											  m_heightfield->findDescendingNeighbour(otherEnd, m_heightfield->GetUntransformedHeightArray()),
											  nullptr,
											  nullptr);
				else
					newSuperarc = addSuperarc(otherEnd,
											  theVertex,
											  sComp->m_seedFrom,
											  sComp->m_seedTo,
											  nullptr,
											  nullptr);

				assert(newSuperarc != nullptr);

				//	add the superarc to the contour tree
				//			if (newSuperarc == 175)
				//				ContourTreeToDotFile("building", dotFileNo++);
				//	now compute the sum of the nodes this side of the top end: initial -1 excludes the vertex proper
				newSuperarc->nodesThisSideOfTop = newSuperarc->nodesThisSideOfBottom = m_heightfield->GetVertexCount() - 1;
				newSuperarc->sampleSumBottom = m_heightfield->GetSampleSum() - *theVertex;

				//			printf("Bot: %8.5f\n", superarcs[newSuperarc].sampleSumBottom);
				for (auto upArc = newSuperarc->m_nextUpArc->GetId();
					 upArc != newSuperarc->GetId();
					 upArc = m_superarcContainer[upArc]->m_nextUpArc->GetId())
				{ // other uparcs exist
					//				printf("Up arc %d\n", upArc);
					newSuperarc->nodesThisSideOfBottom -= m_superarcContainer[upArc]->nodesThisSideOfBottom;
					newSuperarc->sampleSumBottom -= m_superarcContainer[upArc]->sampleSumBottom;
					//				printf("Bot: %8.5f \n", superarcs[newSuperarc].sampleSumBottom);
				} // other uparcs exist

				//	down arcs: first we have to check if any exist
				auto downArc = newSuperarc->GetBottomSupernode()->downList;

				if (downArc != nullptr)
				{ // there are downarcs
					do
					{ // walk around them
						newSuperarc->nodesThisSideOfBottom -= downArc->nodesThisSideOfTop;
						newSuperarc->sampleSumBottom -= downArc->sampleSumTop;
						//					printf("Bot: %8.5f\n ", superarcs[newSuperarc].sampleSumBottom);
						downArc = downArc->m_nextDownArc;
					} // walk around them
					while (downArc != newSuperarc->GetBottomSupernode()->downList);
				} // there are downarcs

				//			printf("On: %8.5f\n", superarcs[newSuperarc].sampleSumOnArc);

				//	set flag for the vertex to mark "visited"
				isVisited.at(vertexPos3d) = true;

				for (Real *nextNode = tempSplitTree->m_arcLookupArray3D.at(vertexPos3d);
					 nextNode != otherEnd;
					 nextNode = tempSplitTree->m_arcLookupArray3D(xNext, yNext, zNext))
				{ // nextNode											//	walk along split arcs to transfer nodes
					//	find the x, y, z indices
					auto temp = m_heightfield->ComputeIndex(nextNode);
					xNext = temp.x;
					yNext = temp.y;
					zNext = temp.z;

					//				printf("Walking past (%1d, %1d, %1d): %8.5f\n", xNext, yNext, zNext, *nextNode);

					if (isVisited.at(temp) == false)						//	1 => vertex is not yet on a superarc
					{
						// first time this vertex has been processed
						isVisited.at(temp) = true;					//	set to 0 to mark that its used, and reset flags for rendering

						//	find the x, y, z indices
						auto temp2 = m_heightfield->ComputeIndex(nextNode);
						xNext = temp2.x;
						yNext = temp2.y;
						zNext = temp2.z;

						//	increment the superarcs node count
						newSuperarc->nodesOnArc++;
						newSuperarc->sampleSumOnArc += m_heightfield->GetHeightAt(xNext, yNext, zNext);
						//					printf("On: %8.5f\n", superarcs[newSuperarc].sampleSumOnArc);
					} // first time this vertex has been processed
					//				printf("\n");
				} // nextNode

				//	compute the count of nodes this side of the top
				newSuperarc->nodesThisSideOfTop =
						newSuperarc->nodesOnArc
						+ (m_heightfield->GetVertexCount()
						   - newSuperarc->nodesThisSideOfBottom);

				newSuperarc->sampleSumTop =
						m_heightfield->GetSampleSum()
						- newSuperarc->sampleSumBottom
						+ newSuperarc->sampleSumOnArc;

				//			printf("Top: %8.5f\n", superarcs[newSuperarc].sampleSumTop);
				//			b.	delete from split tree
				//				note that otherEnd is degree 2 or higher (i.e. it is guaranteed to have a up arc, even for last edge, because of existence of -infinity)
				if (sComp->m_nextHi->m_loEnd == otherEnd)							//	if the next edge at high end is upwards
					sComp->m_nextHi->m_lastLo = sComp->m_lastHi;						//	reset its lastLo pointer
				else
					sComp->m_nextHi->m_lastHi = sComp->m_lastHi;						//	otherwise reset the lastHi pointer

				if (sComp->m_lastHi->m_loEnd == otherEnd)							//	if the last edge at high end is upwards
					sComp->m_lastHi->m_nextLo = sComp->m_nextHi;						//	reset its nextLo pointer
				else
					sComp->m_lastHi->m_nextHi = sComp->m_nextHi;						//	otherwise reset the nextHi pointer

				//	get rid of the sComp edge
				delete sComp;

				//	get rid of it in the sComponent array as well
				tempSplitTree->m_componentArray3D.at(vertexPos3d) = NULL;
				tempSplitTree->supernodeCount--;

				//			c.	delete from join tree
				//				since we have -infinity, there will always be an up and a down arc
				//				this is true even for the last edge, since we start at a lower leaf L.  This means that there is an up-arc to the other remaining node, U.
				//				and since jComp is the departing edge travelling downwards, LU must be upwards in the split tree.
				//				all we do is collapse jComp & jComp->nextHi onto jComp->nextHi
				if (jComp->m_nextLo == jComp)									//	i.e. jComp leads to -inf
					jComp->m_nextHi->m_nextLo = jComp->m_nextHi->m_lastLo =jComp->m_nextHi;	//	set the lower arcs to itself (i.e. no higher neighbours)
				else	//	not an edge to -inf
				{ // not edge to -inf
					jComp->m_nextHi->m_nextLo = jComp->m_nextLo;						//	set the upper arcs
					jComp->m_nextHi->m_lastLo = jComp->m_lastLo;
				} //  not edge to -inf
				jComp->m_nextHi->m_loEnd = jComp->m_loEnd;							//	transfer the low end
				jComp->m_nextHi->m_seedFrom = jComp->m_seedFrom;						//	transfer the seed
				jComp->m_nextHi->m_seedTo = jComp->m_seedTo;							//	transfer the seed

				if (jComp->m_nextLo->m_hiEnd == jComp->m_loEnd)						//	if nextLo is a down-arc
					jComp->m_nextLo->m_lastHi = jComp->m_nextHi;						//	adjust the high end
				else
					jComp->m_nextLo->m_lastLo = jComp->m_nextHi;						//	otherwise the low end

				if (jComp->m_lastLo->m_hiEnd == jComp->m_loEnd)							//	if lastLo is a down-arc
					jComp->m_lastLo->m_nextHi = jComp->m_nextHi;						//	adjust the high end
				else
					jComp->m_lastLo->m_nextLo = jComp->m_nextHi;						//	otherwise the low end

				delete jComp;												//	delete the now-useless edge

				//	get rid of it in the jComponent array as well
				tempJoinTree->m_componentArray3D.at(vertexPos3d) = NULL;
				tempJoinTree->supernodeCount--;

				//			d.	test other end to see if it should be added to leaf queue
				//			we have reduced the up-degree of otherEnd in the join tree, and haven't changed the degrees in the split tree
				//	compute indices
				Index3d otherPos3d = m_heightfield->ComputeIndex(otherEnd);

				//	retrieve the split component
				//	and the join component
				sComp = tempSplitTree->m_componentArray3D.at(otherPos3d);
				jComp = tempJoinTree->m_componentArray3D.at(otherPos3d);

				//	two ways otherEnd can be a leaf:  upper (jComp has no "higher end" nbr, sComp has one each way)
				//	or lower (sComp has no "lower end" nbr, jComp has one each way)
				if (((jComp->m_nextHi == jComp) && (sComp->m_nextLo->m_nextHi == sComp))
					|| (((sComp->m_nextLo == sComp) && (jComp->m_nextHi->m_nextLo == jComp))))
				{
					//				printf("Adding leaf %2ld (%p) to leaf queue\n", leafQSize, otherEnd);

					//	so add it already
					m_leafQueue[m_leafQueueSize++] = otherEnd;
				}
			}
		}
	}
	catch (exception &ex)
	{
		cerr << "An exception was caught whilst Combining Trees." << endl;
		cerr << "The error reads as follows: " << ex.what() << endl;
		fflush(stderr);

		//	Makes sure this gets flagged in the calling method
		throw runtime_error("An error prevented Split and Merge trees from being combined.");
	}

#if defined(VERBOSE_OUTPUT) && !defined(SUPRESS_OUTPUT)
	cout << "Done." << endl;
	fflush(stdout);
#endif

	//	delete joinRoot;														//	delete the root (remaining arc) in join tree
	//	CheckContourTree();
	//	delete splitRoot;														//	delete the root (remaining arc) in split tree

	//	and reset to something predictable
	tempJoinTree->m_rootComponent = nullptr;
	tempSplitTree->m_rootComponent = nullptr;

	//cout << "Modified jointree: " << tempJoinTree->ToDotString(false) << endl;
	//fflush(stdout);

	//	Dispose of the working copies
	delete tempJoinTree;
	delete tempSplitTree;

#undef SUPRESS_OUTPUT
}

///
/// \brief	Returns a list of Superarcs are present at the given isovalue
/// \param	value: the isovalue to be used in the lookup
/// \return a vector containing pointers to each of the present superarcs
/// \since	11-05-2015
/// \author	Dean
/// \author		Dean: update to use iterators [29-09-2015]
///
ContourTree::SuperarcContainer ContourTree::GetValidSuperarcsPresentAtIsovalue(const Real &value) const
{
	SuperarcContainer result;

	//	Check all superarcs in the contour tree
	for (auto it = m_superarcContainer.cbegin(); it != m_superarcContainer.cend(); ++it)
	{
		if ((*it)->IsValid())
		{
			//	Only add to the vector if the arc is intersected by the isovalue
			if ((*it)->IsPresentAtIsovalue(value))
			{
				result.push_back(*it);
			}
		}
	}
	return result;
}
