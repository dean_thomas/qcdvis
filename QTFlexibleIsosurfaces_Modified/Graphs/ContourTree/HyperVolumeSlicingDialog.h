#ifndef HYPERVOLUMESLICINGDIALOG_H
#define HYPERVOLUMESLICINGDIALOG_H

#include <QDialog>

namespace Ui {
	class HyperVolumeSlicingDialog;
}

class HyperVolumeSlicingDialog : public QDialog
{
		Q_OBJECT

	public:
		explicit HyperVolumeSlicingDialog(QWidget *parent = 0);
		~HyperVolumeSlicingDialog();

	private:
		Ui::HyperVolumeSlicingDialog *ui;
};

#endif // HYPERVOLUMESLICINGDIALOG_H
