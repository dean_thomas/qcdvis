#include "HyperVolumeSlicingDialog.h"
#include "ui_HyperVolumeSlicingDialog.h"

HyperVolumeSlicingDialog::HyperVolumeSlicingDialog(QWidget *parent) :
	QDialog(parent),
	ui(new Ui::HyperVolumeSlicingDialog)
{
	ui->setupUi(this);
}

HyperVolumeSlicingDialog::~HyperVolumeSlicingDialog()
{
	delete ui;
}
