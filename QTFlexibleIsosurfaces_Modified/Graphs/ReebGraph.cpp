#include "ReebGraph.h"

//#include <vtkDataArray.h>
//#include <vtkReebGraph.h>
//#include <vtkDataSetAttributes.h>
//#include <vtkReebGraphSurfaceSkeletonFilter.h>
//#include <iostream>
#include <queue>

#include "rb.h"

using namespace std;

#define USING_STL_VECTOR

ReebGraphSuperarc* ReebGraph::CollapseNodeToArc(ReebGraphSupernode* const node)
{
	assert(node->IsRegular());

	return nullptr;
}

ReebGraph::ReebGraph()
{
	m_writer = RbWriter(*this);
	m_reader = RbReader(*this);
}

bool ReebGraph::LoadFromFile(const std::string& filename)
{
	return m_reader(filename);
}

ReebGraph::~ReebGraph()
{
	/*
	for (auto& a : m_arcs)
	{
		delete a.second;
	}

	for (auto& n : m_nodes)
	{
		delete n.second;
	}
	*/
}

void ReebGraph::AddNode(const size_t id,
						const ReebGraphSupernode& node)
{
	auto newNode = new ReebGraphSupernode(node);
	m_supernodeContainer.push_back(newNode);
	//m_nodes[id] =
}

bool ReebGraph::AddEdge(const size_t id,
						const size_t top_node_id,
						const size_t bottom_node_id)
{
	//	Normal behaviour is to fail silently returning false (allowing for
	//	corrupted input).  In debug mode we'll instantly throw an assertion.
#ifdef USING_STL_VECTOR
	auto topNode = m_supernodeContainer.at(top_node_id);
	auto bottomNode = m_supernodeContainer.at(bottom_node_id);
	assert((topNode != nullptr) && (bottomNode != nullptr));

	if (topNode->GetHeight() < bottomNode->GetHeight()) std::swap(topNode, bottomNode);
	assert(topNode->GetHeight() >= bottomNode->GetHeight());

	//	Create the arc and to the internal lookup and add to the connected
	//	nodes
	auto newSuperarc = new ReebGraphSuperarc(id, topNode, bottomNode);
	m_superarcContainer.push_back(newSuperarc);
	topNode->addConnectedArc(newSuperarc);
	bottomNode->addConnectedArc(newSuperarc);
#else
	auto sourceNode = m_supernodeContainer.find(sourceNodeId);
	assert(sourceNode !=  m_supernodeContainer.cend());
	if (sourceNode ==  m_supernodeContainer.cend()) return false;

	//	Normal behaviour is to fail silently returning false (allowing for
	//	corrupted input).  In debug mode we'll instantly throw an assertion.
	auto targetNode =  m_supernodeContainer.find(targetNodeId);
	assert(targetNode !=  m_supernodeContainer.cend());
	if (targetNode ==  m_supernodeContainer.cend()) return false;

	auto topNode = sourceNode->second;
	auto bottomNode = targetNode->second;

	m_arcs[id] = new ReebGraphSuperarc(id, topNode, bottomNode);
#endif

	return true;
}

#undef USING_STL_VECTOR
