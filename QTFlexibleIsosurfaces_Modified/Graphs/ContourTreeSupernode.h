//	Simplified Flexible Isosurfaces
//	Hamish Carr, 2003
//	Supernode.h:		class representing supernodes in contour tree
#ifndef SUPERNODE_H
#define SUPERNODE_H

//	include superarc, so we can get a couple of constants from it
#include "ContourTreeSuperarc.h"
#include <cstdlib>
#include <iostream>
#include "SupernodeBase.h"

//	class representing a supernode in the contour tree
class ContourTree;

class ContourTreeSupernode : public SupernodeBase
{
	public:
		friend class ContourTree;

		//	Type aliases
		using size_type = size_t;

		//  For now, fix the precision to float...
		using T = float;


		ContourTree* m_contourTree = nullptr;


		bool m_isValid;

		//	Initializer
		ContourTreeSupernode(ContourTree* contourTree,
							 const size_type nodeID,
							 Real *heightfield_vertex);

		ContourTreeSupernode(const ContourTreeSupernode& rhs) = default;

		ContourTreeSupernode()
			: SupernodeBase(0)
		{}

		Real* GetScalarVertex() const { return m_heightfield_vertex; }

		Real GetNormalisedHeight() const;

		virtual operator std::string() const override;

		void SetValid(const bool valid);
		bool IsValid() const;

	public:
		void HighlightUpperSubtree() override;
		void HighlightLowerSubtree() override;

		//	Concrete implementations of virtual methods
		T GetHeight() const override ;
		size_type GetUpDegree() const override;
		size_type GetDownDegree() const override;

		size_type GetTotalArcsUp() const override { return 0; }
		size_type GetTotalArcsDown() const override { return 0; }

		size_type GetTotalNodesUp() const override { return 0; }
		size_type GetTotalNodesDown() const override { return 0; }

		std::set<ContourTreeSuperarc*> GetDownwardArcs() const;
		std::set<ContourTreeSuperarc*> GetUpwardArcs() const;
	private:
		//	pointer to the corresponding spatial sample
		Real* m_heightfield_vertex = nullptr;


		//	degree of node
		//	TODO: it is intended that these variables will be replaced
		//	by actual lists of superarc's [03-10-2015]
		size_type m_upDegree;
		size_type m_downDegree;

		//	pointers to lists of up- and down- arcs
		ContourTreeSuperarc* upList = nullptr;
		ContourTreeSuperarc* downList = nullptr;
		//unsigned long upListID, downListID;
};

#endif
