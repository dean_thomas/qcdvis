#include "ReebGraphSuperarc.h"
#include "ReebGraphSupernode.h"
#include <cassert>

float ReebGraphSuperarc::GetArcLength() const
{
	return m_topSupernode->GetHeight() - m_bottomSupernode->GetHeight();
}

ReebGraphSuperarc::ReebGraphSuperarc(const size_t id,
				  ReebGraphSupernode* const top,
				  ReebGraphSupernode* const bottom)
	: SuperarcBase(id)
{
	assert(top->GetHeight() >= bottom->GetHeight());

	SetTopSupernode(top);
	SetBottomSupernode(bottom);
}
