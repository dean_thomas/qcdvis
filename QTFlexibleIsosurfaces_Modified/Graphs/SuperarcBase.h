///
///	\brief		Class that provides a common base for superarcs in a
///				topological	tree structure such as the contour tree or reeb
///				graph
///	\author		Dean
///	\since		08-02-2016
///
#ifndef SUPERARC_BASE_H
#define SUPERARC_BASE_H

#include <cstdlib>
#include <cassert>
#include <string>
#include <ostream>
#include <sstream>
#include <functional>
#include <iostream>
#include <set>
//#include "SupernodeBase.h"

//class SupernodeBase;

template <typename supernode_type>
class SuperarcBase
{
	public:
		using size_type = size_t;
		using T = float;

	protected:
		size_type m_id;

		supernode_type *m_topSupernode = nullptr;
		supernode_type *m_bottomSupernode = nullptr;

		bool m_isSelected = false;
		bool m_isHidden = false;
		bool m_isHighlighted = false;
	public:
		virtual bool IsHidden() const { return m_isHidden; }
		virtual void SetHidden(const bool value)
		{
			m_isHidden = value;
		}

		virtual bool IsSelected() const { return m_isSelected; }
		virtual void SetSelected(const bool value)
		{
			//std::cout << "Selected arc: " << m_id << "." << std::endl;
			m_isSelected = value;
		}

		virtual bool IsHighlighted() const { return m_isHighlighted; }
		virtual void SetHighlighted(const bool value)
		{
			m_isHighlighted = value;
		}

		virtual float GetArcLength() const = 0;

		friend std::ostream& operator<< (std::ostream& os,
										 const SuperarcBase& superarc)
		{
			//	Call operator std::string in the concrete class
			os << static_cast<std::string>(superarc);
			return os;
		}

		virtual operator std::string() const;

		size_type GetId() const { return m_id; }

		SuperarcBase(const size_type id)
			: m_id{id} { }

		supernode_type* GetTopSupernode() const
		{
			return m_topSupernode;
		}

		void SetTopSupernode(supernode_type* const supernode)
		{
			assert(supernode != nullptr);
			m_topSupernode = supernode;
		}

		supernode_type* GetBottomSupernode() const
		{
			return m_bottomSupernode;
		}

		void SetBottomSupernode(supernode_type* const supernode)
		{
			assert(supernode != nullptr);
			m_bottomSupernode = supernode;
		}

		//	Default comparison via address
		bool operator <(const SuperarcBase& rhs) const
		{
			return this < &rhs;
		}

		template <typename U>
		bool operator <(const SuperarcBase<U>& rhs) const
		{
			return this < &rhs;
		}

		virtual ~SuperarcBase() { }
};

///
/// \brief		Provides a basic human readable of the class, can be overriden
///				in derived classes to give more suitable information
/// \author		Dean
/// \since		08-02-2016
///
template <typename supernode_type>
inline SuperarcBase<supernode_type>::operator std::string() const
{
	std::stringstream result;

	result << "SuperarcBase = { id: " << m_id << ", "
		   << "length: " << GetArcLength() << " } ";
	return result.str();
}

template <typename T, typename U>
bool operator<(const SuperarcBase<T>& lhs, const SuperarcBase<U>& rhs)
{
	return &lhs < &rhs;
}

///
/// \brief		Comparison functor allowing polymorphic storage of
///				derrived supernodes in a std::map
/// \author		Dean
/// \since		17-03-2016
///
template <typename supernode_type>
struct SuperarcBase_less:
		std::function<bool(const SuperarcBase<supernode_type>*,const SuperarcBase<supernode_type>*)>
{
	bool operator()(const SuperarcBase<supernode_type>* a, const SuperarcBase<supernode_type>* b) const
	{
		return a < b;
	}
};

///
/// \brief		Allow the object to be output directly to a output stream.
///				Calls the virtual std::string operator, so the output can
///				vary if called from a derived type.
/// \param os
/// \param supernode
/// \return
/// \author		Dean
/// \since		08-02-2016
///


#endif // SUPERARC_BASE_H

