#include "JoinTree.h"
#include "Graphs/ContourTree.h"
#include <fstream>

#ifdef _MSC_VER
#define __PRETTY_FUNCTION__ __FUNCSIG__
#endif

template <typename V>
inline std::ostream& operator<<(std::ostream& os, const Storage::Vector3<V>& vec)
{
	using namespace std;

	//os << "Map: size = " << map.size() << endl;
	for (auto x = 0ul; x < vec.GetDimX(); ++x)
	{
		for (auto y = 0ul; y < vec.GetDimY(); ++y)
		{
			for (auto z = 0ul; z < vec.GetDimZ(); ++z)
			{
				if (vec.at(x, y, z) != nullptr)
				{
					cout << "\t" << x << " " << y << " " << z
						 << ":\t" << vec.at(x, y, z) << endl;
				}

			}
		}
	}
	return os;
}

///
/// \brief		JoinTree::createNewComponent
/// \param index3d
/// \param index1d
/// \since		22-09-2015
/// \author		Hamish
/// \author		Dean: moved to separate function [24-09-2015]
/// \\//TODO:	Probably a candidate for a constructor of the Component class
///				(the first half at least)
///
Component* JoinTree::createNewComponent(const size_t& vertexIndex,
										Component* jComp,
										Component* nbrComponent,
										const Index3d& neighbourIndex3d)
{
	//	create a new component
	Component *newComponent = new Component(m_heightfield3D->GetSortedHeight(vertexIndex), jComp, nbrComponent);
	assert (newComponent != nullptr);
	assert (jComp != nullptr);
	assert (nbrComponent != nullptr);

#if defined(VERBOSE_OUTPUT) && !defined(SUPRESS_OUTPUT)
	//	printf("Join at (%3ld, %3ld, %3ld)\n", vertexX, vertexY, vertexZ);
	//	fflush(stdout);
#endif

#if defined(VERBOSE_OUTPUT) && !defined(SUPRESS_OUTPUT)
	//	printf("Neighbour joining is: (%3ld, %3ld, %3ld)\n", neighbourX, neighbourY, neighbourZ);
	//	fflush(stdout);
#endif

	//	set the seed pointer to nbr (it's adjacent to the vertex)
	//	but set both ends . . .
	nbrComponent->m_seedTo = (&m_heightfield3D->GetHeightAt(neighbourIndex3d.x,
															neighbourIndex3d.y,
															neighbourIndex3d.z));
	nbrComponent->m_seedFrom = m_heightfield3D->GetSortedHeight(vertexIndex);

	Index3d join3d = m_heightfield3D->ComputeIndex(nbrComponent->m_loEnd);

	//	and add the corresponding join arc to the tree
	m_arcLookupArray3D(join3d.x, join3d.y, join3d.z) = m_heightfield3D->GetSortedHeight(vertexIndex);

	//	make the vertex the low end of the neighbour's component
	nbrComponent->m_loEnd = m_heightfield3D->GetSortedHeight(vertexIndex);

	//	set the nextLo pointer and the lastLo pointer
	nbrComponent->m_nextLo = newComponent;
	nbrComponent->m_lastLo = jComp;

#if defined(VERBOSE_OUTPUT) && !defined(SUPRESS_OUTPUT)
	//	printf("Existing component is: (%3ld, %3ld, %3ld)\n", neighbourIndex3d.x,
	//		   neighbourIndex3d.y,
	//		   neighbourIndex3d.z);
	//	fflush(stdout);
#endif
	//	make the vertex the low end of the component it was assigned to
	jComp->m_loEnd = m_heightfield3D->GetSortedHeight(vertexIndex);

	//	set the nextLo pointer and the lastLo pointer
	jComp->m_nextLo = nbrComponent;
	jComp->m_lastLo = newComponent;

	//	perform the merge for both old components
	nbrComponent->MergeTo(newComponent);
	jComp->MergeTo(newComponent);

	//	Make sure nothing has gone wrong!
	assert (jComp != nullptr);
	assert (nbrComponent != nullptr);
	assert (newComponent != nullptr);

	return newComponent;
}

///
/// \brief JoinTree::mergeComponent
/// \param nbrComponent
/// \param nbrIndex3d
/// \param vertexIndex1d
/// \param jComp
/// \since		22-09-2015
/// \author		Hamish
/// \author		Dean: moved to separate function [22-09-2015]
///
void JoinTree::mergeComponent(Component *nbrComponent, const Index3d& nbrIndex3d,
							  const size_t& vertexIndex1d,
							  Component *jComp)
{
	cout << __PRETTY_FUNCTION__ << endl;

	//	Find the 3d position
	Index3d joinIndex3d = m_heightfield3D->ComputeIndex(nbrComponent->m_loEnd);

	//	and add the corresponding join arc to the tree
	m_arcLookupArray3D(joinIndex3d.x,
					   joinIndex3d.y,
					   joinIndex3d.z) = m_heightfield3D->GetSortedHeight(vertexIndex1d);




	//	Set the seeds for the neighbour component
	Real* seedFrom = m_heightfield3D->GetSortedHeight(vertexIndex1d);
	Real* seedTo = (&m_heightfield3D->GetHeightAt(nbrIndex3d.x,
												  nbrIndex3d.y,
												  nbrIndex3d.z));
	nbrComponent->SetSeeds(seedFrom, seedTo);

	//	make the vertex the low end of the neighbour's component
	nbrComponent->m_loEnd = m_heightfield3D->GetSortedHeight(vertexIndex1d);

	//	perform the merge
	nbrComponent->MergeDown(jComp);
}

///
/// \brief		Clones a JoinTree
/// \since		21-09-2015
/// \author		Dean
///
JoinTree* JoinTree::Clone() const
{
	return new JoinTree(*this);
}

///
/// \brief		Copy constructor
/// \since		21-09-2015
/// \author		Dean
///
JoinTree::JoinTree(const JoinTree& rhs)
{
	cout << __PRETTY_FUNCTION__ << endl;

	//	From MergeTree
	m_componentArray3D = rhs.m_componentArray3D;
	m_arcLookupArray3D = rhs.m_arcLookupArray3D;
	m_rootComponent = rhs.m_rootComponent;

	//	From JoinTree
	m_superNodesInContourTree = rhs.m_superNodesInContourTree;
	supernodeCount = rhs.supernodeCount;
}

///
/// \brief		Default constructor
/// \since		21-09-2015
/// \author		Dean
///
JoinTree::JoinTree()
	: MergeTree(nullptr)
{

}

///
/// \brief:     JoinTree::JoinTree
/// \param:     heightfield
/// \since:     20-11-2014
/// \author:    Dean
///
JoinTree::JoinTree(HeightField3D *heightfield)
	: MergeTree(heightfield)
{
	m_superNodesInContourTree = 0;

	//  Run the construction algoritm
	createTree();

#ifdef SAVE_MERGE_TREES
	saveToDotFile("/home/dean/Desktop/join_tree.dot", false);
#endif

}

/*
///
/// \brief:     Prints out the Union-Find info for the join sweep
/// \since:     20-11-2014
/// \author:    Hamish
///
void JoinTree::printJoinComponents()
{
	/*
#ifdef ENABLE_PRINT_JOIN_COMPONENTS
	for (unsigned long i = 0; i < _heightfield->xDim; i++)
	{
		for (unsigned long j = 0; j < _heightfield->yDim; j++)
		{
			for (unsigned long k = 0; k < _heightfield->zDim; k++)
			{
				printf("(%2ld, %2ld, %2ld): %8.5f: pointer: %8X\n", i, j, k, _heightfield->height(i, j, k), (unsigned int)componentArray3D(i, j, k));
#ifndef USE_SHORT_COMPONENTS
				if (componentArray3D(i, j, k) == NULL)
				{
					printf("Join component <%ld, %ld, %ld> is null.\n", i, j, k);
				}
				else
				{
					printf("hiEnd: ");
					PrintPointerIndices(componentArray3D(i, j, k)->hiEnd);
					printf(", ");
					printf("loEnd: ");
					PrintPointerIndices(componentArray3D(i, j, k)->loEnd);
					printf(", ");
					printf("seedFrom: ");
					PrintPointerIndices(componentArray3D(i, j, k)->seedFrom);
					printf("\n");
					printf("seedTo: ");
					PrintPointerIndices(componentArray3D(i, j, k)->seedTo);
					printf("\n");
					printf("nextHi: %8X, lastHi: %8X, nextLo: %8X, lastLo: %8X\n",
						   (unsigned int)componentArray3D(i, j, k)->nextHi, (unsigned int)componentArray3D(i, j, k)->lastHi,
						   (unsigned int)componentArray3D(i, j, k)->nextLo, (unsigned int)componentArray3D(i, j, k)->lastLo);
				}
#endif
			}
			printf("\n");
		}
		printf("\n");
	}
#undef ENABLE_PRINT_JOIN_COMPONENTS
#endif
}
*/

/*
///
/// \brief:     Prints out the join tree (i.e. join components, with duplicates suppressed)
/// \since:     20-11-2014
/// \author:    Hamish
void JoinTree::printJoinTree()
{
	/*
	for (unsigned long i = 0; i < _heightfield->XDim(); i++)
	{
		for (unsigned long j = 0; j < _heightfield->YDim(); j++)
		{
			for (unsigned long k = 0; k < _heightfield->ZDim(); k++)
			{
				if (componentArray3D(i, j, k) == NULL)
					continue;
				if (componentArray3D(i, j, k)->hiEnd != &(_heightfield->height(i, j, k)))
					continue;
				printf("hiEnd: ");
				PrintPointerIndices(componentArray3D(i, j, k)->hiEnd);
				printf(", ");
				printf("loEnd: ");
				PrintPointerIndices(componentArray3D(i, j, k)->loEnd);
				printf(", ");
				printf("seed: ");
				PrintPointerIndices(componentArray3D(i, j, k)->seedFrom);
				printf(", pointer: %8X, nextHi: %8X, lastHi: %8X, nextLo: %8X, lastLo: %8X\n",
					   (unsigned int)componentArray3D(i, j, k),
					(unsigned int)componentArray3D(i, j, k)->nextHi, (unsigned int)componentArray3D(i, j, k)->lastHi,
					(unsigned int)componentArray3D(i, j, k)->nextLo, (unsigned int)componentArray3D(i, j, k)->lastLo);
			}
		}
	}

}
*/

/*
///
/// \brief:     Prints out the pointer indices
/// \param:     Vertex
/// \since:     20-11-2014
/// \author:    Hamish
///
void JoinTree::pointerIndicesToString(Real *Vertex)
{
	unsigned long x, y, z;

	if (Vertex == NULL)
		printf("NULL");
	else if (Vertex == &(MINUS_INFINITY))
		printf("minus infinity");
	else if (Vertex == &(PLUS_INFINITY))
		printf("plus infinity");
	else
	{
		 // should be valid pointer
		m_heightfield3D->ComputeIndex(Vertex, x, y, z);
		printf("(%2ld, %2ld, %2ld): %6.2f", x, y, z, *Vertex);
	}
}
*/

///
/// \brief		Does the down sweep to create the join tree
/// \details	CreateJoinTree() needs to do the following:
///				A.	create the array holding the join components, all
///					initialized to NULL
///				B.	do a loop in downwards order, adding each vertex to the
///					union-find:
///					i.	queue up the neighbours of the vertex
///					ii.	loop through all neighbours of the vertex:
///						a.	if the neighbour is lower than the vertex, skip
///						b.	if the neighbour belongs to a different component
///							than the vertex
///							1.	and the vertex has no component yet, add the
///								vertex to the component
///							2.	the vertex is a component, but is not yet a
///								join, make it a join
///							3.	the vertex is a join, merge the additional
///								component
///					iii.	if the vertex still has no (NULL) component, start
///							a new one
///				C.	tie off the final component to minus_infinity
///	\since		22-09-2015
///	\author		Hamish: original code
/// \author		Dean: make a virtual method of merge tree [23-09-2015]
/// \author		Dean: make code more modular and modernise [23-09-2015]
///
void JoinTree::createTree()												//
{
#define SUPRESS_OUTPUT

	unsigned long neighbourComponentCount;													//	# of neighbouring components
	Component *jComp;														//	local pointer to join component of vertex

#if defined(VERBOSE_OUTPUT) && !defined(SUPRESS_OUTPUT)
	//	A.	create the array holding the join components, all initialized to NULL
	cout << "Starting computation of join tree." << endl;
#endif

	//	create the join component array
	m_componentArray3D = ComponentArray3D(m_heightfield3D->GetBaseDimX(),
										  m_heightfield3D->GetBaseDimY(),
										  m_heightfield3D->GetBaseDimZ());

	m_arcLookupArray3D = ArcLookupArray3D(m_heightfield3D->GetBaseDimX(),
										  m_heightfield3D->GetBaseDimY(),
										  m_heightfield3D->GetBaseDimZ());

	supernodeCount = 0;

	//	B.	do a loop in downwards order, adding each vertex to the union-find:
	for (long vertexIndex = m_heightfield3D->GetVertexCount() - 1; vertexIndex >= 0; vertexIndex--)							//	walk from high end of array
	{
		//	B.i.
		cout << "B.i." << endl;

		//		i.	queue up the neighbours of the vertex
		Index3d vertex3d = m_heightfield3D->ComputeIndex(m_heightfield3D->GetSortedHeight(vertexIndex));							//	compute the indices of the vertex

#if defined(VERBOSE_OUTPUT) && !defined(SUPRESS_OUTPUT)
		//	print out the number of the vertex
		printf("Examining vertex (%ld, %ld, %ld)\n", vertexX, vertexY, vertexZ);
		printf("%s\n", componentArrayToString(false).c_str());
		printf("################################\n\n");
		fflush(stdout);
#endif
		cout << "Current vertex is: " << vertex3d << " with index " << vertexIndex << endl;

		NeighbourQueue neighbourQueue;

		NeighbourFunctor6 neighbourFunctor(m_heightfield3D->GetBaseDimX(),
										   m_heightfield3D->GetBaseDimY(),
										   m_heightfield3D->GetBaseDimZ());

		neighbourQueue.QueueNeighbours(vertex3d, neighbourFunctor);
		neighbourComponentCount = 0;

		cout << neighbourQueue << endl;

		//	reset the count of neighbouring components
		//		ii.	loop through all neighbours of the vertex:
		jComp = m_componentArray3D(vertex3d.x, vertex3d.y, vertex3d.z);
		cout << "Retreived join component " << jComp << endl;

		//	retrieve a local pointer to the join component of the vertex
		cout << (jComp == nullptr ? "Component is nullptr" : static_cast<string>(*jComp)) << endl;

		for (long neighbour = 0; neighbour < neighbourQueue.GetNeighbourCount(); neighbour++)			//	loop through neighbours
		{
			//cout << "Component lookup: " << m_componentArray3D << endl;

			Index3d temp = neighbourQueue.GetNeighbourAt(neighbour);

			Index3d neighbourIndex3d = {temp.x, temp.y, temp.z};
			cout << "Inspecting neighbour " << neighbourIndex3d << endl;

			//			a.	if the neighbour is lower than the vertex, skip
			auto result = compareHeight(&(m_heightfield3D->GetHeightAt(neighbourIndex3d.x,
															 neighbourIndex3d.y,
															 neighbourIndex3d.z)), m_heightfield3D->GetSortedHeight(vertexIndex));

			cout << "Height sort result = " << result << endl;
			if (result < 0) continue;
														//	skip to end of loop
			cout << "Retreiving neighbour component: " << m_componentArray3D(neighbourIndex3d.x,
														 neighbourIndex3d.y,
														 neighbourIndex3d.z) << endl;
			Component *nbrComponent = m_componentArray3D(neighbourIndex3d.x,
														 neighbourIndex3d.y,
														 neighbourIndex3d.z)->component();
			cout << "Resolving path compression: " << nbrComponent << endl;

			cout << (nbrComponent == nullptr ? "Neighbour component is nullptr" : static_cast<string>(*nbrComponent)) << endl;

			//	retrieve the neighbour's component from union-find
			//			b.	if the neighbour belongs to a different component than the vertex
			if (jComp != nbrComponent)									//	compare components
			{
				cout << "Join component: " << jComp
				<< " != neighbour component " << nbrComponent << "." << endl;

				// B.ii.b.
				cout << "B.ii.b." << endl;

				//				1.	and the vertex has no component yet, add the vertex to the component
				if (neighbourComponentCount == 0)									//	this is the first neighbouring component
				{
					// B.ii.b.1.
					cout << "B.ii.b.1" << endl;
					cout << "neighbour component count is zero." << endl;

					//	components could be null when starting this step
					//assert(jComp != nullptr);
					//assert(nbrComponent != nullptr);

					jComp = nbrComponent;
					m_componentArray3D(vertex3d.x, vertex3d.y, vertex3d.z) = nbrComponent;			//	set the vertex to point to it
					cout << "Set component at " << vertex3d << " to " << *nbrComponent << endl;

					nbrComponent->m_seedTo = (&m_heightfield3D->GetHeightAt(neighbourIndex3d.x,
																			neighbourIndex3d.y,
																			neighbourIndex3d.z));		//	set the seed pointer to nbr (it's adjacent to the vertex)
					nbrComponent->m_seedFrom = m_heightfield3D->GetSortedHeight(vertexIndex);				//	but set both ends . . .

					Index3d join3d = m_heightfield3D->ComputeIndex(nbrComponent->m_loEnd);

					m_arcLookupArray3D(join3d.x, join3d.y, join3d.z) = m_heightfield3D->GetSortedHeight(vertexIndex);		//	and add the corresponding join arc to the tree
					cout << "Set arc look up for ("
					<< join3d.x << ", "<< join3d.y << ", "<< join3d.z << ") "
					<< "to " << *m_heightfield3D->GetSortedHeight(vertexIndex)
					<< endl;

					nbrComponent->m_loEnd = m_heightfield3D->GetSortedHeight(vertexIndex);				//	and update the lo end (for drawing purposes)
					cout << "Set neighbour component lo end to "
					<< m_heightfield3D->GetSortedHeight(vertexIndex) << endl;

					neighbourComponentCount++;									//	increment the number of neighbouring components
					cout << "Neighbour component count is now: "
					<< neighbourComponentCount << endl;
				} // B.ii.b.1.
				//				2.	the vertex is a component, but is not yet a join, make it a join
				else if (neighbourComponentCount == 1)								//	this is the second neighbouring component
				{
					// B.ii.b.2.
					cout << "B.ii.b.2" << endl;

					Component* newComponent = createNewComponent(vertexIndex, jComp, nbrComponent, neighbourIndex3d);

					//	and reset the join component
					jComp = newComponent;			//	reset it
					m_componentArray3D(vertex3d.x, vertex3d.y, vertex3d.z) = newComponent;
					cout << "Set component at " << vertex3d << " to " << *newComponent << endl;

					m_superNodesInContourTree++;										//	and increment the number of supernodes
					//	This will be a new supernode
					++supernodeCount;
					cout << "Supernode count is now " << supernodeCount << endl;
					neighbourComponentCount++;									//	increment the number of neighbouring components

				} // B.ii.b.2.
				// i.e. nNbrComponents > 1
				else
				{
					// B.ii.b.3
					cout << "B.ii.b.3" << endl;

					//	the vertex is a join, merge the additional component
					mergeComponent(nbrComponent, neighbourIndex3d, vertexIndex, jComp);
				} // B.ii.b.3
			} // B.ii.b.
		} // neighbour
		if (jComp == NULL)
		{
			//		iii.	if the vertex still has no (NULL) component, start a new one
			//	no neighbours found:  must be a local maximum
			Component* newComponent =
					new Component(m_heightfield3D->GetSortedHeight(vertexIndex));
			assert (newComponent != nullptr);

			m_componentArray3D(vertex3d.x, vertex3d.y, vertex3d.z) = newComponent;
			cout << "Set component at " << vertex3d << " to " << *newComponent << endl;

			//	and increment the number of supernodes
			m_superNodesInContourTree++;
			//	This will be a new supernode
			++supernodeCount;
			cout << "Supernode count is now " << supernodeCount << endl;
		}
	}

	//	C.	tie off the final component to minus_infinity
	Index3d join3d = m_heightfield3D->ComputeIndex(jComp->m_loEnd);					//	compute the low end

	m_arcLookupArray3D(join3d.x, join3d.y, join3d.z) = (Real*)&MINUS_INFINITY;							//	and add the corresponding join arc to the tree

	jComp->m_loEnd = (Real*)&MINUS_INFINITY;										//	set the loEnd to minus infinity
	jComp->m_nextLo = jComp->m_lastLo = jComp;									//	set the circular list pointers

	//	and store a pointer to it
	m_rootComponent = jComp;

	cout << "Join Tree computation completed." << endl;

#if defined(VERBOSE_OUTPUT) && !defined(SUPRESS_OUTPUT)
	printf("%s\n", componentArrayToString(false).c_str());
	fflush(stdout);
#endif

#undef SUPRESS_OUTPUT
}

bool JoinTree::SaveToDotFile(const std::string basefilename) const
{
	return MergeTree::saveToDotFile(basefilename, false);
}

