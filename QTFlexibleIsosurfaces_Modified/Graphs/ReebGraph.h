#pragma once
#ifndef REEB_GRAPH_H
#define REEB_GRAPH_H



//#include <vtkReebGraph.h>
//#include <vtkFloatArray.h>

#include <Vector3.h>
#include <map>
#include <functional>
#include <iostream>

#include "TreeBase.h"
//#include "SuperarcBase.h"

#include "ReebGraphSupernode.h"
#include "ReebGraphSuperarc.h"

class ReebGraph : public TreeBase<ReebGraphSupernode, ReebGraphSuperarc>
{
	public:
		/*
		using NodeTable = std::map<size_t, ReebGraphSupernode*>;
		using ArcTable = std::map<size_t, ReebGraphSuperarc*>;

		using NodeIterator = NodeTable::iterator;
		using ConstNodeIterator = NodeTable::const_iterator;
		using ReverseNodeIterator = NodeTable::reverse_iterator;
		using ConstReverseNodeIterator = NodeTable::const_reverse_iterator;

		using ArcIterator = ArcTable::iterator;
		using ConstArcIterator = ArcTable::const_iterator;
		using ReverseArcIterator = ArcTable::reverse_iterator;
		using ConstReverseArcIterator = ArcTable::const_reverse_iterator;
		*/
	private:
		std::function<bool(const std::string&)> m_writer;
		std::function<bool(const std::string&)> m_reader;

		//NodeTable m_nodes;
		//ArcTable m_arcs;

	public:
		/*
		NodeIterator Supernodes_begin() noexcept { return m_nodes.begin(); }
		ConstNodeIterator Supernodes_begin() const noexcept { return m_nodes.begin(); }
		ConstNodeIterator Supernodes_cbegin() const noexcept { return m_nodes.cbegin();	}
		NodeIterator Supernodes_end() noexcept { return m_nodes.end(); }
		ConstNodeIterator Supernodes_end() const noexcept { return m_nodes.end(); }
		ConstNodeIterator Supernodes_cend() const noexcept { return m_nodes.cend(); }

		ArcIterator Superarcs_begin() noexcept { return m_arcs.begin(); }
		ConstArcIterator Superarcs_begin() const noexcept { return m_arcs.begin(); }
		ConstArcIterator Superarcs_cbegin() const noexcept { return m_arcs.cbegin();	}
		ArcIterator Superarcs_end() noexcept { return m_arcs.end(); }
		ConstArcIterator Superarcs_end() const noexcept { return m_arcs.end(); }
		ConstArcIterator Superarcs_cend() const noexcept { return m_arcs.cend(); }

		//friend std::ostream& operator<<(std::ostream&, const ReebGraph&);
		*/
		ReebGraph();
		~ReebGraph();

		void AddNode(const size_t id, const ReebGraphSupernode& node);
		bool AddEdge(const size_t id,
					 const size_t sourceNodeId,
					 const size_t targetNodeId);
		ReebGraphSuperarc* CollapseNodeToArc(ReebGraphSupernode* const node);


		operator std::string() const;

		bool LoadFromFile(const std::string& filename);
};

///
///	\brief		Print the reeb graph in human readable for
/// \author		Dean
/// \since		10-02-2016
///
inline ReebGraph::operator std::string() const
{
	std::stringstream result;

	result << "ReebGraph = { ";

	result << m_supernodeContainer << ", " << m_superarcContainer;

	result << "}";

	return result.str();
}

#endif
