#ifndef REEBGRAPHSUPERNODE_H
#define REEBGRAPHSUPERNODE_H

#include "SupernodeBase.h"
#include <ostream>
#include <cassert>
#include <set>

class ReebGraphSuperarc;

class ReebGraphSupernode : public SupernodeBase
{
	public:
		using size_type = size_t;
		//	For now, ahead of template parameterization
		using T = float;

	private:
		T m_height;
		T m_normHeight;
	public:
		ReebGraphSupernode(const size_type id);
		ReebGraphSupernode(const size_type id,
						   const T height, const T norm_height,
						   const size_t x,
						   const size_t y,
						   const size_t z);

		std::set<ReebGraphSuperarc*> m_connectedArcs;
		size_t addConnectedArc(ReebGraphSuperarc* arc);
	public:
		virtual operator std::string() const override;

		bool IsValid() const { return true; }

		//	Concrete implementations of virtual functions
		virtual T GetHeight() const { return m_height; }
		virtual T GetNormalisedHeight() const { return m_normHeight; }

		size_type GetUpDegree() const override { return  getDirectUpwardArcs().size(); }
		size_type GetDownDegree() const override { return getDirectDownwardArcs().size(); }
		size_type GetDegree() const override { return m_connectedArcs.size(); }

		size_type GetTotalArcsUp() const override { return getAllUpwardArcs().size(); }
		size_type GetTotalArcsDown() const override { return getAllDownwardArcs().size(); }

		size_type GetTotalNodesUp() const override { return getAllUpwardNodes().size(); }
		size_type GetTotalNodesDown() const override { return getAllDownwardNodes().size(); }


		std::set<ReebGraphSuperarc*> getDirectUpwardArcs() const;
		std::set<ReebGraphSuperarc*> getDirectDownwardArcs() const;

		std::set<ReebGraphSupernode*> getDirectUpwardNodes() const;
		std::set<ReebGraphSupernode*> getDirectDownwardNodes() const;

		std::set<const ReebGraphSuperarc*> getAllUpwardArcs() const;
		std::set<const ReebGraphSuperarc*> getAllDownwardArcs() const;

		std::set<const ReebGraphSupernode*> getAllUpwardNodes() const;
		std::set<const ReebGraphSupernode*> getAllDownwardNodes() const;

		void HighlightUpperSubtree() override { }
		void HighlightLowerSubtree() override { }
		//std::set<ReebGraphSuperarc*> preOrderVisitUp(const ReebGraphSupernode* node) const;
};

//std::set<ReebGraphSuperarc*> preOrderVisitUp(ReebGraphSupernode* node, const std::set<ReebGraphSuperarc*>& existing);
std::set<const ReebGraphSuperarc*> recursiveVisitUpArcs(
		const ReebGraphSupernode* node,
		const bool pre_order = true,
		const std::set<const ReebGraphSuperarc*>& existing = {});
std::set<const ReebGraphSuperarc*> recursiveVisitDownArcs(
		const ReebGraphSupernode* node,
		const bool pre_order = true,
		const std::set<const ReebGraphSuperarc*>& existing = {});
std::set<const ReebGraphSupernode*> recursiveVisitUpNodes(
		const ReebGraphSupernode* node,
		const bool pre_order = true,
		const std::set<const ReebGraphSupernode*>& existing = {});
std::set<const ReebGraphSupernode*> recursiveVisitDownNodes(
		const ReebGraphSupernode* node,
		const bool pre_order = true,
		const std::set<const ReebGraphSupernode*>& existing = {});

inline std::ostream& operator<<(std::ostream& os, const ReebGraphSupernode& node)
{
	os << node.GetHeight();
	return os;
}



#endif // REEBGRAPHSUPERNODE_H
