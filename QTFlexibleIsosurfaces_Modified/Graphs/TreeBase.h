#ifndef TREE_BASE_H
#define TREE_BASE_H

#include <vector>
#include <cstdlib>
#include <string>
#include <sstream>
#include <ostream>
#include <limits>
#include <Globals/stl_ostream.h>

template <typename vertex_type, typename arc_type>
class TreeBase
{
	public:
		///	Type aliases
		///	============
		using size_type = size_t;

		using VertexType = vertex_type;
		using ArcType = arc_type;

		//	Containers used for storage
		using SuperarcContainer = std::vector<arc_type*>;
		using SupernodeContainer = std::vector<vertex_type*>;

		///	Iterator definitions
		///	====================
		//	First for superarcs...
		using SuperarcIterator = typename SuperarcContainer::iterator;
		using SuperarcConstIterator = typename SuperarcContainer::const_iterator;
		using SuperarcReverseIterator = typename SuperarcContainer::reverse_iterator;
		using SuperarcConstReverseIterator = typename SuperarcContainer::const_reverse_iterator;

		//	...and for supernodes
		using SupernodeIterator = typename SupernodeContainer::iterator;
		using SupernodeConstIterator = typename SupernodeContainer::const_iterator;
		using SupernodeReverseIterator = typename SupernodeContainer::reverse_iterator;
		using SupernodeConstReverseIterator = typename SupernodeContainer::const_reverse_iterator;

		///	Reference / pointer definitions
		///	===============================
		//	First for superarcs...
		using SuperarcReference = typename SuperarcContainer::reference;
		using SuperarcConstReference = typename SuperarcContainer::const_reference;
		using SuperarcPointer = typename SuperarcContainer::pointer;
		using SuperarcConstPointer = typename SuperarcContainer::const_pointer;

		//	...and for supernodes
		using SupernodeReference = typename SupernodeContainer::reference;
		using SupernodeConstReference = typename SupernodeContainer::const_reference;
		using SupernodePointer = typename SupernodeContainer::pointer;
		using SupernodeConstPointer = typename SupernodeContainer::const_pointer;

		///	Iterator pass through
		///	=====================
		//	First for superarcs...
		SuperarcIterator Superarcs_begin() { return m_superarcContainer.begin(); }
		SuperarcConstIterator Superarcs_begin() const { return m_superarcContainer.begin(); }
		SuperarcConstIterator Superarcs_cbegin() const noexcept { return m_superarcContainer.cbegin(); }
		SuperarcIterator Superarcs_end() { return m_superarcContainer.end(); }
		SuperarcConstIterator Superarcs_end() const { return m_superarcContainer.end(); }
		SuperarcConstIterator Superarcs_cend() const noexcept { return m_superarcContainer.cend(); }

		SuperarcReverseIterator Superarcs_rbegin() { return m_superarcContainer.rbegin(); }
		SuperarcConstReverseIterator Superarcs_rbegin() const { return m_superarcContainer.rbegin(); }
		SuperarcConstReverseIterator Superarcs_crbegin() const noexcept { return m_superarcContainer.crbegin(); }
		SuperarcReverseIterator Superarc_rend() { return m_superarcContainer.rend(); }
		SuperarcConstReverseIterator Superarc_rend() const { return m_superarcContainer.rend(); }
		SuperarcConstReverseIterator Superarc_crend() const noexcept { return m_superarcContainer.crend(); }

		//	...and for supernodes
		SupernodeIterator Supernodes_begin() { return m_supernodeContainer.begin(); }
		SupernodeConstIterator Supernodes_begin() const { return m_supernodeContainer.begin(); }
		SupernodeConstIterator Supernodes_cbegin() const noexcept { return m_supernodeContainer.cbegin(); }
		SupernodeIterator Supernodes_end() { return m_supernodeContainer.end(); }
		SupernodeConstIterator Supernodes_end() const { return m_supernodeContainer.end(); }
		SupernodeConstIterator Supernodes_cend() const noexcept { return m_supernodeContainer.cend(); }

		SupernodeReverseIterator Supernodes_rbegin() { return m_supernodeContainer.rbegin(); }
		SupernodeConstReverseIterator Supernodes_rbegin() const { return m_supernodeContainer.rbegin(); }
		SupernodeConstReverseIterator Supernodes_crbegin() const noexcept { return m_supernodeContainer.crbegin(); }
		SupernodeReverseIterator Supernode_rend() { return m_supernodeContainer.rend(); }
		SupernodeConstReverseIterator Supernode_rend() const { return m_supernodeContainer.rend(); }
		SupernodeConstReverseIterator Supernode_crend() const noexcept { return m_supernodeContainer.crend(); }

	public:
		///	Generic properties of the tree
		/// ==============================
		size_type GetVertexCount() const { return m_supernodeContainer.size(); }
		size_type GetPositiveVertexCount() const
		{
			auto result = 0;
			for (auto& v : m_supernodeContainer)
			{
				if (v->GetHeight() >= 0.0) ++result;
			}
			return result;
		}
		size_type GetNegativeVertexCount() const
		{
			auto result = 0;
			for (auto& v : m_supernodeContainer)
			{
				if (v->GetHeight() < 0.0) ++result;
			}
			return result;
		}
		size_type GetArcCount() const { return m_superarcContainer.size(); }


		///
		///	\brief		Clears the highlighted attribute of all arcs
		/// \since		10-02-2015
		/// \author		Dean
		/// \author		Dean: update to use iterators [29-09-2015]
		///
		void ClearHighlightedArcs()
		{
			for (auto& arc : m_superarcContainer)
			{
				arc->SetHighlighted(false);
			}
		}

		///
		/// \brief		Clears the selected flag in the Supernode
		/// \since		30-03-2016
		/// \author		Dean
		///
		void ClearSelectedNodes()
		{
			for (auto& node : m_supernodeContainer)
			{
				node->SetSelected(false);
			}
		}

		///
		/// \brief		Clears the selected flag in the Supernode
		/// \since		30-03-2016
		/// \author		Dean
		///
		void ClearHighlightedNodes()
		{
			for (auto& node : m_supernodeContainer)
			{
				node->SetHighlighted(false);
			}
		}

		///
		///	\brief		Clears the selected attribute of all arcs
		/// \since		10-02-2015
		/// \author		Dean
		/// \author		Dean: update to use iterators [29-09-2015]
		///
		void ClearSelectedArcs()
		{
			for (auto& arc : m_superarcContainer)
			{
				arc->SetSelected(false);
			}

			//	Tell anything that is watching us to update (redraw)
			//m_heightfield->GetDataModel()->NotifyObservers();
		}

		///
		/// \brief		Clears the selected flag in the Supernode
		/// \since		30-03-2016
		/// \author		Dean
		///
		void ClearHiddenArcs()
		{
			for (auto& arc : m_superarcContainer)
			{
				arc->SetHidden(false);
			}
		}

		///
		/// \brief		Clears the selected flag in the Supernode
		/// \since		30-03-2016
		/// \author		Dean
		///
		void ClearHiddenNodes()
		{
			for (auto& node : m_supernodeContainer)
			{
				node->SetHidden(false);
			}
		}

		virtual operator std::string() const;

		vertex_type* NodeAt(const size_t x,
						const size_t y,
						const size_t z) const
		{
			for (auto& node : m_supernodeContainer)
			{
				auto pos = node->GetPosition();
				if ((std::get<0>(pos) == x) && (std::get<1>(pos) == y) && (std::get<2>(pos) == z))
				{
					return node;
				}
			}
			return nullptr;
		}

		arc_type* ArcAt(const size_t xTop,
						const size_t yTop,
						const size_t zTop,
						const size_t xBottom,
						const size_t yBottom,
						const size_t zBottom)
		{
			for (auto& arc : m_superarcContainer)
			{
				auto posTop = arc->GetTopSupernode()->GetPosition();
				auto posBottom = arc->GetBottomSupernode()->GetPosition();

				if ((std::get<0>(posTop) == xTop) && (std::get<0>(posBottom) == xBottom)
					&& (std::get<1>(posTop) == yTop) && (std::get<1>(posBottom) == yBottom)
					&& (std::get<2>(posTop) == zTop) && (std::get<2>(posBottom) == zBottom))
				{
					return arc;
				}
			}
			return nullptr;
		}

		float GetMinHeight() const
		{
			auto min = std::numeric_limits<float>::max();

			for (auto& node : m_supernodeContainer)
			{
				auto height = node->GetHeight();
				if (height < min) min = height;
			}
			return min;
		}

		float GetMaxHeight() const
		{
			auto max = std::numeric_limits<float>::min();

			for (auto& node : m_supernodeContainer)
			{
				auto height = node->GetHeight();
				if (height > max) max = height;
			}
			return max;
		}

	protected:
		//  containers's holding all the edges and nodes of the contour tree
		SuperarcContainer m_superarcContainer;
		SupernodeContainer m_supernodeContainer;

		TreeBase() { }
};

///
///	\brief		Provides a method for printing the contents of the class in a
///				human readable form.  Virtual, so can be overriden in derrived
///				classes
/// \author		Dean
/// \since		10-02-2016
///
template <typename vertex_type, typename arc_type>
inline TreeBase<vertex_type, arc_type>::operator std::string() const
{
	std::stringstream result;

	result << "TreeBase = { ";

	result << m_supernodeContainer << ", " << m_superarcContainer;

	result << "}";

	return result.str();
}

///
/// \brief		Provide a way to print to an ostream using the overridable
///				string conversion operator
///	\author		Dean
/// \since		10-02-2016
///
template <typename vertex_type, typename arc_type>
inline std::ostream& operator<<(std::ostream& os,
								const TreeBase<vertex_type, arc_type>& tb)
{
	os << static_cast<std::string>(tb);
	return os;
}

#endif // TREE_BASE_H
