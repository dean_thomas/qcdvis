#pragma once
#ifndef RB_H
#define RB_H


#include "ReebGraph.h"
#include <string>


struct RbWriter
{
	ReebGraph reebGraph;

	RbWriter(const ReebGraph& reebGraph)
		: reebGraph { reebGraph } {	}

	bool operator()(const std::string& filename);
};

struct RbReader
{
	ReebGraph& reebGraph;

	RbReader(ReebGraph& reebGraph)
		: reebGraph { reebGraph } {	}

	bool operator()(const std::string& filename);
};


#endif
