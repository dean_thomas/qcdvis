#include "MergeTree.h"

MergeTree::MergeTree()
{

}

MergeTree::MergeTree(HeightField3D *heightfield3D)
{
	m_heightfield3D = heightfield3D;

	//	set these to something predictable
	m_rootComponent = nullptr;
}

MergeTree::~MergeTree()
{

}

bool MergeTree::saveToDotFile(const std::string basefilename, const bool reverseArcs) const
{
#define SUPRESS_OUTPUT

	FILE *dotFile = fopen(basefilename.c_str(), "w");
	if (dotFile == nullptr)
	{
		fprintf(stderr, "Unable to write to dotfile %s\n", basefilename.c_str());
		fflush(stderr);

		return false;
	}

#if defined(VERBOSE_OUTPUT) && !defined(SUPRESS_OUTPUT)
	printf("%s\n", ToDotString(reverseArcs).c_str());
	fflush(stdout);
#endif

	fprintf(dotFile, "%s", ToDotString(reverseArcs).c_str());
	fclose(dotFile);

	return true;
#undef SUPRESS_OUTPUT
}

///
/// \brief      Prints out the arcs in the tree
/// \param      basefilename: filename of the
/// \param      reverseArcs: draws the arcs in reverse (ie for split tree)
/// \since      20-11-2014
/// \author     Hamish: original code
/// \author     Dean:   modify for use in abstract class
///                     return a string instead of printing directly
///                     [11-04-2015]
///
string MergeTree::ToDotString(const bool reverseArcs) const
{
#define SUPRESS_OUTPUT
	stringstream ss;

	ss << "digraph G {\n";
	//fprintf(dotFile, "\tsize=\"6.5, 9\"\n\tratio=\"fill\"\n");

	for (unsigned long x = 0; x < m_heightfield3D->GetBaseDimX(); x++)
	{
		for (unsigned long y = 0; y < m_heightfield3D->GetBaseDimY(); y++)
		{
			for (unsigned long z = 0; z < m_heightfield3D->GetBaseDimZ(); z++)
			{
				if (m_arcLookupArray3D(x, y, z) == nullptr)
					continue;

				if (*m_arcLookupArray3D(x, y, z) == MINUS_INFINITY)
					continue;

				if (*m_arcLookupArray3D(x, y, z) == PLUS_INFINITY)
					continue;

				size_t xFar, yFar, zFar;
				m_heightfield3D->ComputeIndex(m_arcLookupArray3D(x, y, z), xFar, yFar, zFar);

				if (reverseArcs)
				{
					//  Split Tree
					ss << "\t\"(" << xFar << "," << yFar << "," << zFar;
					ss << ")\" -> \"(";
					ss << x << "," << y << "," << z;
					ss << ")\";\n";
				}
				else
				{
					//  Join Tree
					ss << "\t\"(" << x << "," << y << "," << z;
					ss << ")\" -> \"(";
					ss << xFar << "," << yFar << "," << zFar;
					ss << ")\";\n";
				}
			}
		}
	}

	ss << "}\n";

#if defined(VERBOSE_OUTPUT) && !defined(SUPRESS_OUTPUT)
	printf("%s\n",ss.str().c_str());
	fflush(stdout);
#endif

	return ss.str();
#undef SUPRESS_OUTPUT
}

///
/// \brief      Prints out the pointer indices
/// \param      Vertex
/// \since      20-11-2014
/// \author     Hamish
/// \author     Dean:   modify for use in abstract class
///                     return a string instead of printing directly
///                     [11-04-2015]
///
string MergeTree::pointerIndicesToString(const Real *vertex) const
{
	//  TODO: check that pointers are output correctly as
	//  HEX
	stringstream ss;
	size_t x, y, z;

	if (vertex == nullptr)
		ss << "NULL";
	else if (*vertex == MINUS_INFINITY)
		ss << "minus infinity";
	else if (*vertex == PLUS_INFINITY)
		ss << "plus infinity";
	else
	{
		// should be valid pointer
		m_heightfield3D->ComputeIndex(vertex, x, y, z);
		ss << "(" << x << ", " << y << ", " << z << "): ";
		ss << *vertex;
	}
	return ss.str();
}

///
/// \brief      Prints out the Union-Find info for the join/split sweeps
/// \details    Prints all the elements of the union-find component array.  Note:
///             this function combines the functionality previously found in the
///             printXtree and printXcomponents.
/// \param      suppressDuplicates: if true will give less detail for duplicate
///             components
///             fullDetails: if set to true, a full listing of connected
///             components will be returned
/// \since      20-11-2014
/// \author     Hamish
/// \author     Dean:   modify for use in abstract class
///                     return a string instead of printing directly
///                     [11-04-2015]
///
string MergeTree::componentArrayToString(const bool suppressDuplicates,
											const bool fullDetails) const
{
	//	blah, blah, blah

	//  TODO: check that pointers are output correctly as
	//  HEX
	stringstream ss;

	for (unsigned long i = 0; i < m_heightfield3D->GetBaseDimX(); i++)
	{
		for (unsigned long j = 0; j < m_heightfield3D->GetBaseDimY(); j++)
		{
			for (unsigned long k = 0; k < m_heightfield3D->GetBaseDimZ(); k++)
			{
				ss << "(" << i << ", " << j << ", " << k << "): ";
				ss << m_heightfield3D->GetHeightAt(i, j, k);
                ss << ": pointer: " << &m_componentArray3D(i, j, k);

				if (fullDetails)
				{
					if (m_componentArray3D(i, j, k) == nullptr)
						continue;

					//  Duplicate suppression in JOIN-tree
					if ((suppressDuplicates) &&
						(m_componentArray3D(i, j, k)->m_hiEnd != &m_heightfield3D->GetHeightAt(i, j, k)))
						continue;

					//  Duplicate suppression in SPLIT-tree
					if ((suppressDuplicates) &&
						(m_componentArray3D(i, j, k)->m_loEnd != &m_heightfield3D->GetHeightAt(i, j, k)))
						continue;

					ss << "hiEnd: " << pointerIndicesToString(m_componentArray3D(i, j, k)->m_hiEnd);
					ss << ", ";
					ss << "loEnd: " << pointerIndicesToString(m_componentArray3D(i, j, k)->m_loEnd);
					ss << ", ";
					ss << "seed: " << pointerIndicesToString(m_componentArray3D(i, j, k)->m_seedFrom);
					ss << "\n";
                    ss << ", pointer: " << &m_componentArray3D(i, j, k) << ", ";
					ss << "nextHi: " << m_componentArray3D(i, j, k)->m_nextHi << ", ";
					ss << "lastHi: " << m_componentArray3D(i, j, k)->m_lastHi << ", ";
					ss << "nextLo: " << m_componentArray3D(i, j, k)->m_nextLo << ", ";
					ss << "lastLo: " << m_componentArray3D(i, j, k)->m_lastLo << "\n";
				}
			}
			ss << "\n";
		}
		ss << "\n";
	}
	return ss.str();
}
