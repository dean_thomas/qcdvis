#ifndef SPLITTREE_H
#define SPLITTREE_H

#include "../HeightField/HeightField3D.h"
#include <Vector3.h>
#include "../HeightField/Component.h"
#include "../HeightField/HeightSort.h"
#include "JoinTree.h"
#include "../HeightField/NeighbourQueue/NeighbourQueue.h"
#include "MergeTree.h"

class SplitTree : public MergeTree
{
	private:
		using Index3d = HeightField3D::Index3d;
		JoinTree *m_joinTree = nullptr;

		unsigned long m_superNodesInContourTree;

		virtual void createTree();

		SplitTree(const SplitTree& rhs);
	public:
		SplitTree* Clone() const;

		unsigned long supernodeCount;

		SplitTree(HeightField3D *heightfield, JoinTree *joinTree);
		SplitTree();

		virtual bool SaveToDotFile(const std::string basefilename) const override;

		unsigned long SuperNodesInContourTree() const { return m_superNodesInContourTree; }
};

#endif // SPLITTREE_H
