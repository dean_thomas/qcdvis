#include "ReebGraphSupernode.h"
#include "ReebGraphSuperarc.h"

#include <tuple>
#include <sstream>
#include <iostream>

#define DISABLE_DEBUG_OUTPUT

using namespace std;

///
///	\brief		Returns a list of all the supernodes that have a greater
///				isovalue in the sub-tree originating from the supplied
///				supernode
/// \param		node: ReebGraphSupernode* pointer to the root node in the
///				subtree
/// \param		pre_order: boolean; if true nodes are added in pre-order
///				(default), otherwise post-ordering is used
/// \param		existing: set<ReebGraphSupernode*>; the current set of
///				supernodes to be added to (deafult to empty set)
/// \return		set<ReebGraphSupernode*>: the list of nodes with any others
///				from the current sub-tree added
///	\author		Dean
/// \since		13-07-2016
///
std::set<const ReebGraphSupernode*> recursiveVisitUpNodes(
		const ReebGraphSupernode* node,
		const bool pre_order,
		const std::set<const ReebGraphSupernode*>& existing)
{
	assert(node != nullptr);
	auto result = existing;

	//	Obtain a list of directly connected upward nodes
	for (auto& up_node : node->getDirectUpwardNodes())
	{
		//	If ordering is 'pre-order' add each to the result before recursing
		if (pre_order) result.insert(up_node);

		//	Visit each recursively
		auto temp = recursiveVisitUpNodes(up_node, pre_order, result);

		//	Merge the lists
		result.insert(temp.cbegin(), temp.cend());

		//	If ordering is 'post-order' add each to the result after recursing
		if (!pre_order) result.insert(up_node);
	}

	return result;
}

///
///	\brief		Returns a list of all the superarcs that have a higher
///				isovalue in the sub-tree originating from the supplied
///				supernode
/// \param		node: ReebGraphSupernode* pointer to the root node in the
///				subtree
/// \param		pre_order: boolean; if true arcs are added in pre-order
///				(default), otherwise post-ordering is used
/// \param		existing: set<ReebGraphSuperarc*>; the current set of
///				supernodes to be added to (deafult to empty set)
/// \return		set<ReebGraphSuperarc*>: the list of arcs with any others
///				from the current sub-tree added
///	\author		Dean
/// \since		13-07-2016
///
std::set<const ReebGraphSuperarc*> recursiveVisitUpArcs(
		const ReebGraphSupernode* node,
		const bool pre_order,
		const std::set<const ReebGraphSuperarc*>& existing)
{
	assert(node != nullptr);
	auto result = existing;

	//	Obtain a list of directly connected upward arcs
	for (auto& up_arc : node->getDirectUpwardArcs())
	{
		//	If ordering is 'pre-order' add each to the result before recursing
		if (pre_order) result.insert(up_arc);

		//	Visit each recursively
		auto temp = recursiveVisitUpArcs(up_arc->GetTopSupernode(), pre_order, result);

		//	Merge the lists
		result.insert(temp.cbegin(), temp.cend());

		//	If ordering is 'post-order' add each to the result after recursing
		if (!pre_order) result.insert(up_arc);
	}

	return result;
}

///
///	\brief		Returns a list of all the supernodes that have a lower
///				isovalue in the sub-tree originating from the supplied
///				supernode
/// \param		node: ReebGraphSupernode* pointer to the root node in the
///				subtree
/// \param		pre_order: boolean; if true nodes are added in pre-order
///				(default), otherwise post-ordering is used
/// \param		existing: set<ReebGraphSupernode*>; the current set of
///				supernodes to be added to (deafult to empty set)
/// \return		set<ReebGraphSupernode*>: the list of nodes with any others
///				from the current sub-tree added
///	\author		Dean
/// \since		13-07-2016
///
std::set<const ReebGraphSupernode*> recursiveVisitDownNodes(
		const ReebGraphSupernode* node,
		const bool pre_order,
		const std::set<const ReebGraphSupernode*>& existing)
{
	assert(node != nullptr);
	auto result = existing;

	//	Obtain a list of directly connected upward nodes
	for (auto& down_node : node->getDirectDownwardNodes())
	{
		//	If ordering is 'pre-order' add each to the result before recursing
		if (pre_order) result.insert(down_node);

		//	Visit each recursively
		auto temp = recursiveVisitDownNodes(down_node, pre_order, result);

		//	Merge the lists
		result.insert(temp.cbegin(), temp.cend());

		//	If ordering is 'post-order' add each to the result after recursing
		if (!pre_order) result.insert(down_node);
	}

	return result;
}

///
///	\brief		Returns a list of all the superarcs that have a lower
///				isovalue in the sub-tree originating from the supplied
///				supernode
/// \param		node: ReebGraphSupernode* pointer to the root node in the
///				subtree
/// \param		pre_order: boolean; if true arcs are added in pre-order
///				(default), otherwise post-ordering is used
/// \param		existing: set<ReebGraphSuperarc*>; the current set of
///				supernodes to be added to (deafult to empty set)
/// \return		set<ReebGraphSuperarc*>: the list of arcs with any others
///				from the current sub-tree added
///	\author		Dean
/// \since		13-07-2016
///
std::set<const ReebGraphSuperarc*> recursiveVisitDownArcs(
		const ReebGraphSupernode* node,
		const bool pre_order,
		const std::set<const ReebGraphSuperarc*>& existing)
{
	assert(node != nullptr);
	auto result = existing;

	//	Obtain a list of directly connected upward arcs
	for (auto& down_arc : node->getDirectDownwardArcs())
	{
		//	If ordering is 'pre-order' add each to the result before recursing
		if (pre_order) result.insert(down_arc);

		//	Visit each recursively
		auto temp = recursiveVisitDownArcs(down_arc->GetBottomSupernode(), pre_order, result);

		//	Merge the lists
		result.insert(temp.cbegin(), temp.cend());

		//	If ordering is 'post-order' add each to the result after recursing
		if (!pre_order) result.insert(down_arc);
	}

	return result;
}

ReebGraphSupernode::ReebGraphSupernode(const size_type id)
	: SupernodeBase(id)
{
	m_height = 0;
	m_normHeight = 0;
}

ReebGraphSupernode::ReebGraphSupernode(const size_type id,
									   const T height,
									   const T norm_height,
									   const size_t x,
									   const size_t y,
									   const size_t z)
	: SupernodeBase(id), m_height{height}, m_normHeight{norm_height}
{
	assert((m_normHeight >= 0.0) && (m_normHeight <= 1.0));

	m_position = std::make_tuple(x, y, z);
}

///
/// \brief	Use to maintain a list of connectivity for traversal.  This
///			requires filtering into in/out arcs
/// \return the new number of arcs in the connectivity set
/// \author	Dean
/// \since	13-07-2016
///
size_t ReebGraphSupernode::addConnectedArc(ReebGraphSuperarc* arc)
{
	m_connectedArcs.insert(arc);

#ifndef DISABLE_DEBUG_OUTPUT
	cout << "Added arc to Supernode: " << m_id << ".  Number of connected arcs"
		 << " is now: " << m_connectedArcs.size() << endl;
#endif

	return m_connectedArcs.size();
}

///
/// \brief	Filters the list of connected node to only those that are
///			directly connected downwards
/// \return set<Superarc*> a set of the directly connected arcs
/// \author	Dean
/// \since	13-07-2016
///
std::set<ReebGraphSuperarc*> ReebGraphSupernode::getDirectDownwardArcs() const
{
	std::set<ReebGraphSuperarc*> result;

	for (auto& superarc : m_connectedArcs)
	{
		assert(superarc != nullptr);
		auto topNode = superarc->GetTopSupernode();
		auto bottomNode = superarc->GetBottomSupernode();

		//	If this is the bottom node of the connected arc it cannot be
		//	 a downward connection
		if (bottomNode != this) result.insert(superarc);
	}

	return result;
}

///
/// \brief	Filters the list of connected arcs to only those that are
///			directly connected upwards
/// \return set<Superarc*> a set of the directly connected arcs
/// \author	Dean
/// \since	13-07-2016
///
std::set<ReebGraphSuperarc*> ReebGraphSupernode::getDirectUpwardArcs() const
{
	std::set<ReebGraphSuperarc*> result;

	for (auto& superarc : m_connectedArcs)
	{
		assert(superarc != nullptr);
		auto topNode = superarc->GetTopSupernode();
		auto bottomNode = superarc->GetBottomSupernode();

		//	If this is the top node of the connected arc it cannot be
		//	an upward connection
		if (topNode != this) result.insert(superarc);
	}

	return result;
}

///
/// \brief	Filters the list of connected nodes to only those that are
///			directly connected upwards
/// \return set<Supernode*> a set of the directly connected nodes
/// \author	Dean
/// \since	13-07-2016
///
std::set<ReebGraphSupernode*> ReebGraphSupernode::getDirectUpwardNodes() const
{
	std::set<ReebGraphSupernode*> result;

	//	Get a list of the up arcs from here...
	auto direct_upward_arcs = getDirectUpwardArcs();

	//	Iterate over the list
	for (auto& arc : direct_upward_arcs)
	{
		//	Extract the next supernode along the arc and add to the list
		result.insert(arc->GetTopSupernode());
	}
	return result;
}

///
/// \brief	Filters the list of connected nodes to only those that are
///			directly connected downwards
/// \return set<Supernode*> a set of the directly connected nodes
/// \author	Dean
/// \since	13-07-2016
///
std::set<ReebGraphSupernode*> ReebGraphSupernode::getDirectDownwardNodes() const
{
	std::set<ReebGraphSupernode*> result;

	//	Get a list of the down arcs from here...
	auto direct_downward_arcs = getDirectDownwardArcs();

	//	Iterate over the list
	for (auto& arc : direct_downward_arcs)
	{
		//	Extract the next supernode along the arc and add to the list
		result.insert(arc->GetBottomSupernode());
	}
	return result;
}

///
/// \brief	Recursively builds a list of nodes with a higher isovalues in the
///			subtree formed from this node
/// \return set<Supernode*> a set of the supernodes in the subtree
/// \author	Dean
/// \since	13-07-2016
///
std::set<const ReebGraphSupernode*> ReebGraphSupernode::getAllUpwardNodes() const
{
	auto result = recursiveVisitUpNodes(this);

	return result;
}

///
/// \brief	Recursively builds a list of nodes with a lower isovalue in the
///			subtree formed from this node
/// \return set<Supernode*> a set of the supernodes in the subtree
/// \author	Dean
/// \since	13-07-2016
///
std::set<const ReebGraphSupernode*> ReebGraphSupernode::getAllDownwardNodes() const
{
	auto result = recursiveVisitDownNodes(this);

	return result;
}

///
/// \brief	Recursively builds a list of arcs with a higher isovalues in the
///			subtree formed from this node
/// \return set<Superarc*> a set of the superarcs in the subtree
/// \author	Dean
/// \since	13-07-2016
///
std::set<const ReebGraphSuperarc*> ReebGraphSupernode::getAllUpwardArcs() const
{
	auto result = recursiveVisitUpArcs(this);

	return result;
}

///
/// \brief	Recursively builds a list of arcs with a lower isovalues in the
///			subtree formed from this node
/// \return set<Superarc*> a set of the superarcs in the subtree
/// \author	Dean
/// \since	13-07-2016
///
std::set<const ReebGraphSuperarc*> ReebGraphSupernode::getAllDownwardArcs() const
{
	auto result = recursiveVisitDownArcs(this);

	return result;
}

ReebGraphSupernode::operator std::string() const
{
	std::stringstream result;

	result << "ReebGraphSupernode = { ";

	result << "NodeID: " << m_id << ", ";
	result << "Normalised Isovalue: " << GetNormalisedHeight() << ", ";
	//result << "Up Degree: " << GetUpDegree() << ", ";
	//result << "Down Degree: " << GetDownDegree() << ", ";
	result << "Pos: (" << std::get<0>(m_position) << ", "
		   << std::get<1>(m_position) << ", "
		   << std::get<2>(m_position) << ")";
	result << " }";

	return result.str();
}
