//	Simplified Flexible Isosurfaces
//	Hamish Carr, 2003
//  Supernode.cpp:		class representing supernodes in contour tree
#include "ContourTreeSupernode.h"
#include "ContourTreeSuperarc.h"	// need this for the NO_SUPERARC constant
#include "Graphs/ContourTree.h"
#include <cassert>
#include <tuple>
#include <HeightField/HeightField3D.h>

using namespace std;

///
/// \brief ContourTreeSupernode::HighlightUpperSubtree
/// \since	15-07-2016
/// \author	Dean
///
void ContourTreeSupernode::HighlightUpperSubtree()
{
	SetHighlighted(true);
	auto up_arcs = GetUpwardArcs();
	cout << "number of upward arcs: " << up_arcs << endl;

	for (auto& arc : up_arcs)
	{
		arc->SetHighlighted(true);
		auto top_node = arc->GetTopSupernode();
		top_node->HighlightUpperSubtree();
	}
}

///
/// \brief ContourTreeSupernode::HighlightLowerSubtree
/// \since	15-07-2016
/// \author	Dean
///
void ContourTreeSupernode::HighlightLowerSubtree()
{
	SetHighlighted(true);
	auto down_arcs = GetDownwardArcs();
	cout << "number of downward arcs: " << down_arcs << endl;

	for (auto& arc : down_arcs)
	{
		arc->SetHighlighted(true);
		auto bottom_node = arc->GetBottomSupernode();
		bottom_node->HighlightLowerSubtree();
	}
}

///
/// \brief		Returns a list of arcs that are 'downward' from this node
/// \return
/// \author		Dean
/// \since		15-07-2016
///
std::set<ContourTreeSuperarc*> ContourTreeSupernode::GetDownwardArcs() const
{
	//	Create a set for the result, return the empty set if there are no
	//	downward nodes
	std::set<ContourTreeSuperarc*> result;
	if (downList == nullptr) return result;

	//	Select the 'first' downward arc
	auto downArc = downList;
	result.insert(downArc);

	//	Add arcs adjacent to the 'first' arc
	for (auto& arc = downArc->m_nextDownArc; arc != downArc->m_lastDownArc; ++arc)
	{
		result.insert(arc);
	}

	//	Add the 'last' adjacent arc
	result.insert(downArc->m_lastDownArc);
	assert(result.size() == m_downDegree);

	return result;
}

///
/// \brief		Returns a list of arcs that are 'upward' from this node
/// \return
/// \author		Dean
/// \since		15-07-2016
///
std::set<ContourTreeSuperarc*> ContourTreeSupernode::GetUpwardArcs() const
{
	//	Create a set for the result, return the empty set if there are no
	//	upward nodes
	std::set<ContourTreeSuperarc*> result;
	if (upList == nullptr) return result;

	//	Select the 'first' upward arc
	auto upArc = upList;
	result.insert(upArc);

	//	Add arcs adjacent to the 'first' arc
	for (auto& arc = upArc->m_nextUpArc; arc != upArc->m_lastUpArc; ++arc)
	{
		result.insert(arc);
	}

	//	Add the 'last' adjacent arc
	result.insert(upArc->m_lastUpArc);
	assert(result.size() == m_upDegree);

	return result;
}

///
/// \brief ContourTreeSupernode::GetUpDegree
/// \return
/// \author	Dean
/// \since	08-02-2016
///
ContourTreeSupernode::size_type ContourTreeSupernode::GetUpDegree() const
{
	return m_upDegree;
}

///
/// \brief ContourTreeSupernode::GetDownDegree
/// \return
/// \author	Dean
/// \since	08-02-2016
///
ContourTreeSupernode::size_type ContourTreeSupernode::GetDownDegree() const
{
	return m_downDegree;
}

///
/// \brief	Public accessor to set if a node is valid
/// \param	valid: new state of the node
/// \since	14-05-2015
/// \author	Dean
/// \author	Dean: update to use assert [03-10-2015]
///
void ContourTreeSupernode::SetValid(const bool valid)
{
	assert (m_isValid != valid);

	m_isValid = valid;
}

///
/// \brief	Public accessor to check if a node is valid
/// \return bool: true if the node is valid
/// \since	14-05-2015
/// \author	Dean
///
bool ContourTreeSupernode::IsValid() const
{
	return m_isValid;
}

///
/// \brief		Returns details of the node in human readable form
/// \return
/// \author		Dean
/// \since		16-04-2015
///
ContourTreeSupernode::operator std::string() const
{
	stringstream result;

	result << "ContourTreeSupernode = { ";

	auto lookup_sample = [&](Real* sample)
	{
		assert(m_contourTree != nullptr);
		auto heightfield = m_contourTree->GetHeightfield();
		assert(heightfield != nullptr);

		auto pos3 = heightfield->ComputeIndex(sample);

		return pos3;
	};

	result << "NodeID: " << m_id << ", ";
	result << "Isovalue: " << *m_heightfield_vertex << ", ";
	result << "Normalised Isovalue: " << GetNormalisedHeight() << ", ";
	result << "Up Degree: " << GetUpDegree() << ", ";
	result << "Down Degree: " << GetDownDegree() << ", ";
	result << "Pos: (" << lookup_sample(m_heightfield_vertex).x << ", "
		   << lookup_sample(m_heightfield_vertex).y << ", "
		   << lookup_sample(m_heightfield_vertex).z << ")";
	result << " }";

	return result.str();
}

//Supernode::Supernode(const Supernode& rhs)

ContourTreeSupernode::ContourTreeSupernode(ContourTree* contourTree,
						  const size_type nodeID,
						  Real *heightfield_vertex)
	: SupernodeBase(nodeID)
{
	assert(contourTree != nullptr);
	assert(contourTree->GetHeightfield() != nullptr);
	assert(heightfield_vertex != nullptr);

	//	What contour tree are we a member of?
	m_contourTree = contourTree;

	//	Is the node valid (after epsilon collapses etc.)
	m_isValid = false;

	//	copy the known value
	m_heightfield_vertex = heightfield_vertex;

	//	Also we'll find the actual position for storage
	auto pos = m_contourTree->GetHeightfield()->ComputeIndex(heightfield_vertex);
	m_position = make_tuple(pos.x, pos.y, pos.z);

	//	and make the rest predictable
	upList = nullptr;
	downList = nullptr;

	m_upDegree = 0;
	m_downDegree = 0;
	//xPosn = 0.5;
	//yPosn = 0.5;

}

///
/// \brief      Returns the actual height of the supernode
/// \return     The function heightat the node.
/// \author     Dean
/// \since      27-11-2014
///
ContourTreeSupernode::T ContourTreeSupernode::GetHeight() const
{
	return *m_heightfield_vertex;
}

///
/// \brief      Returns the actual height of the supernode
/// \return     The function heightat the node.
/// \since      27-11-2014
/// \author     Dean
/// \author		Dean: use lookup to cnotour tree for global min / max
///				[16-04-2015]
///
Real ContourTreeSupernode::GetNormalisedHeight() const
{
	//	Compute scale for normalization
	const auto globalMin = m_contourTree->GetHeightfield()->GetMinHeight();
	const auto globalMax = m_contourTree->GetHeightfield()->GetMaxHeight();
	const auto globalRange = globalMax - globalMin;

	if (globalRange > 0)
	{
		//	Normalize (provided there is no div by zero)
		return ((*m_heightfield_vertex - globalMin) / (globalMax - globalMin));
	}
	else
	{
		//	Could potentially happen for ground states
#ifndef NDEBUG
		cerr << "Warning!  Caught potential DivByZero - was this intended?"
			 << endl;
#endif
		return 0.0;
	}
}
