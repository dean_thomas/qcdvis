#ifndef PROCESSINFO
#define PROCESSINFO

#if defined(WIN32) || defined(__WIN32) || defined (__WIN32__)
#define WINDOWS
#include <windows.h>
#include <psapi.h>
#endif


namespace ProcessInfo
{
	//	Virtual memory
	unsigned long long GetTotalVirtualMemory();
	unsigned long long GetAvailableVirtualMemory();
	unsigned long long GetUsedVirtualMemory();
	unsigned long long GetVirtualMemoryUsedByProcess();

	//	Physical memory
	unsigned long long GetTotalRAM();
	unsigned long long GetUsedRAM();
	unsigned long long GetAvailableRAM();
	unsigned long long GetRAMUsedByProcess();

	//	Processor
}

#endif // PROCESSINFO

