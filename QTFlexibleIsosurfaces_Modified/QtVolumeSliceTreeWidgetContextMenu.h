#ifndef QTVOLUMESLICETREEWIDGETCONTEXTMENU_H
#define QTVOLUMESLICETREEWIDGETCONTEXTMENU_H

#include <QMenu>
#include <QAction>
#include <QList>
#include "HeightField4D.h"

class QtVolumeSliceTreeWidgetContextMenu : public QMenu
{
		Q_OBJECT
	private:
		QList<QAction*> m_actions;
		HeightField4D *m_heightField4d = nullptr;

	public:
		QtVolumeSliceTreeWidgetContextMenu(QWidget *parent = 0);
		void SetHeightField4d(HeightField4D *heightfield4d);

	public slots:
		void onClickPartitionByX();
		void onClickPartitionByY();
		void onClickPartitionByZ();
		void onClickPartitionByT();
};

#endif // QTVOLUMESLICETREEWIDGETCONTEXTMENU_H
