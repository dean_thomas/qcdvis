#ifndef RBH_FORMAT_H
#define RBH_FORMAT_H

#include <string>
#include <set>
#include <vector>
#include <cassert>
#include <iomanip>

struct rbh_file
{
    std::string header;
    std::string x_axis_title;
    std::string y_axis_title;

    std::vector<float> intervals;
    std::vector<std::set<ReebGraphSuperarc*>> binned_data;

    rbh_file(const std::vector<float> intervals,
             const std::vector<std::set<ReebGraphSuperarc*>>& binned_data,
             const std::string& x_axis_title,
             const std::string& y_axis_title,
             const std::string& header = "")
        : header { header },
          intervals { intervals },
          binned_data{ binned_data },
          x_axis_title{x_axis_title },
          y_axis_title{y_axis_title }
    {
        assert(intervals.size() == binned_data.size() + 1);
    }
};

std::ostream& operator<<(std::ostream& os, const rbh_file& rbh)
{
    using namespace std;
    const auto TAB = '\t';

    os << fixed << setprecision(4);
    os << "#" << TAB << "Reeb graph histogram data" << endl;
    os << "#" << TAB << rbh.header << endl;
    os << "#" << TAB << "Interval count: " << rbh.intervals.size() << endl;
    os << "#" << TAB << "Binned data count: " << rbh.binned_data.size() << endl;
    os << "#" << TAB << "x axis: " << rbh.x_axis_title << endl;
    for (auto i : rbh.intervals)
    {
        os << i << endl;
    }
    os << endl;
    os << "#" << TAB << "y axis: " << rbh.y_axis_title << endl;
    for (auto b : rbh.binned_data)
    {
        os << b.size() << endl;
    }
    os << endl;

    return os;
}

#endif // RBH_FORMAT_H
