var dir_b7263c2df3abed61e1897d5e9e534860 =
[
    [ "ArcBallWidget.cpp", "_arc_ball_widget_8cpp.html", "_arc_ball_widget_8cpp" ],
    [ "ArcBallWidget.h", "_arc_ball_widget_8h.html", [
      [ "ArcBallWidget", "class_arc_ball_widget.html", "class_arc_ball_widget" ]
    ] ],
    [ "ColourSelectionWidget.cpp", "_colour_selection_widget_8cpp.html", "_colour_selection_widget_8cpp" ],
    [ "ColourSelectionWidget.h", "_colour_selection_widget_8h.html", [
      [ "ColourSelectionWidget", "class_colour_selection_widget.html", "class_colour_selection_widget" ]
    ] ],
    [ "ContourTreeWidget.cpp", "_contour_tree_widget_8cpp.html", null ],
    [ "ContourTreeWidget.h", "_contour_tree_widget_8h.html", [
      [ "ContourTreeWidget", "class_contour_tree_widget.html", "class_contour_tree_widget" ]
    ] ],
    [ "FlexibleIsosurfaceController.cpp", "_flexible_isosurface_controller_8cpp.html", null ],
    [ "FlexibleIsosurfaceController.h", "_flexible_isosurface_controller_8h.html", [
      [ "FlexibleIsosurfaceController", "class_flexible_isosurface_controller.html", "class_flexible_isosurface_controller" ]
    ] ],
    [ "FlexibleIsosurfaceWidget.cpp", "_flexible_isosurface_widget_8cpp.html", "_flexible_isosurface_widget_8cpp" ],
    [ "FlexibleIsosurfaceWidget.h", "_flexible_isosurface_widget_8h.html", "_flexible_isosurface_widget_8h" ],
    [ "FlexibleIsosurfaceWindow.cpp", "_flexible_isosurface_window_8cpp.html", null ],
    [ "FlexibleIsosurfaceWindow.h", "_flexible_isosurface_window_8h.html", [
      [ "FlexibleIsosurfaceWindow", "class_flexible_isosurface_window.html", "class_flexible_isosurface_window" ]
    ] ],
    [ "LogCollapseWidget.cpp", "_log_collapse_widget_8cpp.html", "_log_collapse_widget_8cpp" ],
    [ "LogCollapseWidget.h", "_log_collapse_widget_8h.html", [
      [ "LogCollapseWidget", "class_log_collapse_widget.html", "class_log_collapse_widget" ]
    ] ]
];