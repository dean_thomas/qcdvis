#include <processinfo.h>

///
/// \brief ProcessInfo::GetTotalVirtualMemory
/// \return
///	\since		08-07-2015
/// \author		Dean
///
unsigned long long ProcessInfo::GetTotalVirtualMemory()
{
#ifdef WINDOWS
		MEMORYSTATUSEX memInfo;
		memInfo.dwLength = sizeof(MEMORYSTATUSEX);
		GlobalMemoryStatusEx(&memInfo);
		return memInfo.ullTotalPageFile;
#else
		return 0;
#endif
}

///
/// \brief ProcessInfo::GetAvailableVirtualMemory
/// \return
///	\since		08-07-2015
/// \author		Dean
///
unsigned long long ProcessInfo::GetUsedVirtualMemory()
{
#ifdef WINDOWS
	MEMORYSTATUSEX memInfo;
	memInfo.dwLength = sizeof(MEMORYSTATUSEX);
	GlobalMemoryStatusEx(&memInfo);
	return (memInfo.ullTotalPageFile - memInfo.ullAvailPageFile);
#else
	return 0;
#endif
}

///
/// \brief ProcessInfo::GetAvailableVirtualMemory
/// \return
///	\since		08-07-2015
/// \author		Dean
///
unsigned long long ProcessInfo::GetAvailableVirtualMemory()
{
#ifdef WINDOWS
	MEMORYSTATUSEX memInfo;
	memInfo.dwLength = sizeof(MEMORYSTATUSEX);
	GlobalMemoryStatusEx(&memInfo);
	return memInfo.ullAvailPageFile;
#else
	return 0;
#endif
}

///
/// \brief ProcessInfo::GetVirtualMemoryUsedByProcess
/// \return
///	\since		08-07-2015
/// \author		Dean
///
unsigned long long ProcessInfo::GetVirtualMemoryUsedByProcess()
{
#ifdef WINDOWS
	PROCESS_MEMORY_COUNTERS_EX pmc;
	GetProcessMemoryInfo(GetCurrentProcess(), (PROCESS_MEMORY_COUNTERS*)&pmc, sizeof(pmc));
	return pmc.PrivateUsage;
#else
	return 0;
#endif
}

///
/// \brief ProcessInfo::GetTotalRAM
/// \return
///	\since		08-07-2015
/// \author		Dean
///
unsigned long long ProcessInfo::GetTotalRAM()
{
#ifdef WINDOWS
	MEMORYSTATUSEX memInfo;
	memInfo.dwLength = sizeof(MEMORYSTATUSEX);
	GlobalMemoryStatusEx(&memInfo);
	return memInfo.ullTotalPhys;
#else
	return 0;
#endif
}

///
/// \brief ProcessInfo::GetAvailableRAM
/// \return
/// \since		08-07-2015
/// \author		Dean
///
unsigned long long ProcessInfo::GetUsedRAM()
{
#ifdef WINDOWS
	MEMORYSTATUSEX memInfo;
	memInfo.dwLength = sizeof(MEMORYSTATUSEX);
	GlobalMemoryStatusEx(&memInfo);
	return (memInfo.ullTotalPhys - memInfo.ullAvailPhys);
#else
	return 0;
#endif
}

///
/// \brief ProcessInfo::GetAvailableRAM
/// \return
/// \since		08-07-2015
/// \author		Dean
///
unsigned long long ProcessInfo::GetAvailableRAM()
{
#ifdef WINDOWS
	MEMORYSTATUSEX memInfo;
	memInfo.dwLength = sizeof(MEMORYSTATUSEX);
	GlobalMemoryStatusEx(&memInfo);
	return memInfo.ullAvailPhys;
#else
	return 0;
#endif
}

///
/// \brief ProcessInfo::GetRAMUsedByProcess
/// \return
///	\since		08-07-2015
/// \author		Dean
///
unsigned long long ProcessInfo::GetRAMUsedByProcess()
{
#ifdef WINDOWS
	PROCESS_MEMORY_COUNTERS_EX pmc;
	GetProcessMemoryInfo(GetCurrentProcess(), (PROCESS_MEMORY_COUNTERS*)&pmc, sizeof(pmc));
	return pmc.WorkingSetSize;
#else
	return 0;
#endif
}
