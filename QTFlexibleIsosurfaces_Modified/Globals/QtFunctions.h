#ifndef QT_FUNCTIONS_H
#define QT_FUNCTIONS_H

#include <cassert>

#include <QIcon>
#include <QColor>
#include <QPixmap>
#include <QSize>
#include <QPainter>
#include <QVariant>
#include <QVector4D>

//	Anonomous namespace used to hide implementation - access via templated
//	function
//
namespace {
	/// \brief		Converts a QColor to an RGB QVector4D
	/// \param		color
	/// \param		alpha: the alpha value in range 0..1
	/// \return
	/// \since		22-03-2015
	/// \author		Dean
	///
	inline QVector4D QColorToQVector4D(const QColor& color, const float alpha)
	{
		assert((alpha >= 0.0f) && (alpha <= 1.0f));

		//std::cout << __PRETTY_FUNCTION__ << std::endl;
		//std::cout << "Alpha: " << alpha << std::endl;

		return QVector4D(color.redF(),
						 color.greenF(),
						 color.blueF(),
						 alpha);
	}

	/// \brief		Converts a QColor to an RGB QVector4D
	/// \param		color
	/// \param		alpha: the alpha value in range 0..255
	/// \return
	/// \since		22-03-2015
	/// \author		Dean
	///
	inline QVector4D QColorToQVector4D(const QColor& color, const int alpha)
	{
		//std::cout << __PRETTY_FUNCTION__ << std::endl;
		assert((alpha >= 0) && (alpha <= 255));
		return QVector4D(color.redF(),
						 color.greenF(),
						 color.blueF(),
						 (alpha / 256.0f));
	}
}

///
/// \brief		Converts a QColor to an RGB QVector4D - will call the most
///				suitable implementation available using templates
/// \param		color
/// \return
/// \since		23-03-2016
/// \author		Dean
///
template <typename T>
inline QVector4D QColorToQVector4D(const QColor &color, const T alpha)
{
	static_assert(std::is_arithmetic<T>::value,
				  "Alpha channel must be a float or integer type.");

	if (std::is_floating_point<T>::value)
	{
		//	Force hamdling alpha as a floating point type
		return QColorToQVector4D(color, (float)alpha);
	}
	else if (std::is_integral<T>::value)
	{
		//	Force hamdling alpha as a integer type
		return QColorToQVector4D(color, (int)alpha);
	}
}

///
/// \brief		Converts a QColor to an RGB QVector4D
/// \param		color
/// \return
/// \since		15-07-2015
/// \author		Dean
/// \author		Dean: fix to always return floats [25-11-2015]
/// \author		Dean: add alpha channel [25-11-2015]
///
inline QVector4D QColorToQVector4D(const QColor &color)
{
	return QVector4D(color.redF(),
					 color.greenF(),
					 color.blueF(),
					 color.alphaF());
}



//inline QVector4D QColorToQVector4D(const Qt::GlobalColor color, const float alpha)
//{
//	return QColorToQVector4D(QColor(color), alpha);
//}

///
///	Template methods for converting to/from a QVariant
///
///	See: http://blog.bigpixel.ro/2010/04/storing-pointer-in-qvariant/
///	[accessed 03-11-2015]
///
/// \since		03-11-2015
/// \author		Dean
///
template <typename T>
T* toPtr(QVariant v)
{
	return (T*)v.value<void*>();
}

///
///	Template methods for converting to/from a QVariant
///
///	See: http://blog.bigpixel.ro/2010/04/storing-pointer-in-qvariant/
///	[accessed 03-11-2015]
///
/// \since		03-11-2015
/// \author		Dean
///
template <typename T>
QVariant toQVariant(T* ptr)
{
	return qVariantFromValue((void*)ptr);
}


///
/// \brief		Create a colour swatch for the item
/// \param contour
/// \return
/// \author		Dean
/// \since		08-12-2015
///
inline QIcon createSwatchIcon(const QColor& colour,
							  const QSize& size,
							  const bool withBorder)
{
	QPixmap pixmap(size);
	QPainter painter(&pixmap);

	painter.fillRect(QRect(QPoint(0,0),size), colour);

	if (withBorder)
	{
		painter.drawRect(0, 0,
						 size.width()-1,
						 size.height()-1);
	}

	QIcon result;
	result.addPixmap(pixmap);

	return result;
}

#endif // QT_FUNCTIONS_H

