///
///		\brief	Provides compatiblity with the std::ostream object for various
///				Qt types.
///		\since	02-12-2015
///		\author	Dean
///
#ifndef QT_OSTREAM
#define QT_OSTREAM

#include <ostream>
#include <QPoint>
#include <QPointF>
#include <QVector3D>
#include <QRect>
#include <QRectF>
#include <QMatrix4x4>
#include <QString>
#include <QColor>
#include <QSize>
#include <QSizeF>
#include <QGraphicsItem>


/// \author		Dean
/// \since		03-02-2016
inline std::ostream& operator<<(std::ostream& os, const QSize& size)
{
	os << "QSize = { "
	   << "w: " << size.width() << ", "
	   << "h: " << size.height() << " }";
	return os;
}

/// \author		Dean
/// \since		03-02-2016
inline std::ostream& operator<<(std::ostream& os, const QSizeF& size)
{
	os << "QSizeF = { "
	   << "w: " << size.width() << ", "
	   << "h: " << size.height() << " }";
	return os;
}

template <typename T>
inline std::ostream& operator<<(std::ostream& os, const QVector<T>& vector)
{
	os << "QVector<T> = { ";

	for (auto it = vector.cbegin(); it != vector.cend(); ++it)
	{
		if (it != vector.cbegin()) os << ", ";
		os << *it;
	}

	os << " }";

	return os;
}


inline std::ostream& operator<<(std::ostream& os, const QColor& color)
{
	os << "QColor = { "
	   << "r: " << color.red() << ", "
	   << "g: " << color.green() << ", "
	   << "b: " << color.blue() << ", "
	   << "a: " << color.alpha() << " }";
	return os;
}

inline std::ostream& operator<<(std::ostream& os, const QString& string)
{
	os << "QString = { " << string.toStdString() << " }";
	return os;
}


///
/// \brief operator <<
/// \param os
/// \param vector3
/// \return
/// \author		Dean
/// \since		02-12-2015
///
inline std::ostream& operator<<(std::ostream& os, const QVector3D& vector3)
{
	os << "QVector3D = { "
	   << "x: " << vector3.x() << ", "
	   << "y: " << vector3.y() << ", "
	   << "z: " << vector3.z() << " }";
	return os;
}

///
/// \brief operator <<
/// \param os
/// \param point
/// \return
/// \author		Dean
/// \since		02-12-2015
///
inline std::ostream& operator<<(std::ostream& os, const QPoint& point)
{
	os << "QPoint = { "
	   << "x: " << point.x() << ", "
	   << "y: " << point.y() << " }";
	return os;
}

///
/// \brief operator <<
/// \param os
/// \param point
/// \return
/// \author		Dean
/// \since		02-12-2015
///
inline std::ostream& operator<<(std::ostream& os, const QPointF& point)
{
	os << "QPointF = { "
	   << "x: " << point.x() << ", "
	   << "y: " << point.y() << " }";
	return os;
}

///
/// \brief operator <<
/// \param os
/// \param rect
/// \return
/// \author		Dean
/// \since		02-12-2015
///
inline std::ostream& operator<<(std::ostream& os, const QRect& rect)
{
	os << "QRect = { "
	   << "left: " << rect.left() << ", "
	   << "top: " << rect.top() << ", "
	   << "width: " << rect.width() << ", "
	   << "height: " << rect.height() << ", "
	   << "topLeft: [" << rect.topLeft() << "], "
	   << "topRight: [" << rect.topRight() << "], "
	   << "bottomLeft: [" << rect.bottomLeft() << "], "
	   << "bottomRight: [" << rect.bottomRight() << "] }";
	return os;
}

///
/// \brief operator <<
/// \param os
/// \param rect
/// \return
/// \author		Dean
/// \since		02-12-2015
///
inline std::ostream& operator<<(std::ostream& os, const QRectF& rect)
{
	os << "QRectF = { "
	   << "left: " << rect.left() << ", "
	   << "top: " << rect.top() << ", "
	   << "width: " << rect.width() << ", "
	   << "height: " << rect.height() << ", "
	   << "topLeft: " << rect.topLeft() << ", "
	   << "topRight: " << rect.topRight() << ", "
	   << "bottomLeft: " << rect.bottomLeft() << ", "
	   << "bottomRight: " << rect.bottomRight() << " }";
	return os;
}

///
/// \brief operator <<
/// \param os
/// \param mat4
/// \return
/// \author		Dean
/// \since		02-12-2015
///
inline std::ostream& operator<<(std::ostream&os, const QMatrix4x4& mat4)
{
	auto e = mat4.data();

	os << "QMatrix4x4 = { "
	   << "data = [ "
	   << e[ 0] << ", " << e[ 1] << ", " << e[ 2] << ", " << e[ 3] << "; "
	   << e[ 4] << ", " << e[ 5] << ", " << e[ 6] << ", " << e[ 7] << "; "
	   << e[ 8] << ", " << e[ 9] << ", " << e[10] << ", " << e[11] << "; "
	   << e[12] << ", " << e[13] << ", " << e[14] << ", " << e[15] << "; "
	   << "] }";
	return os;
}

///
/// \brief		operator <<
/// \param		os
/// \param		QGraphicsItem: the item to be printed
/// \return
/// \author		Dean
/// \since		06-03-2016
///
inline std::ostream& operator<<(std::ostream& os, const QGraphicsItem& item)
{
	os << "QGraphicsItem = { "
	   << "boundingRect: " << item.boundingRect()
	   << " }";
	return os;
}

#endif // QTOSTREAM

