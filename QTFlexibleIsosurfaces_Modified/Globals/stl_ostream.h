#pragma once
#ifndef STL_OSTREAM_H
#define STL_OSTREAM_H

#include <ostream>
#include <string>
#include <map>
#include <set>
#include <typeinfo>
#include <type_traits>
#include <utility>
#include <cstddef>

namespace
{
	////////////////////////////////////////////////////////////////////////////////
	////							Helper functions							////
	////////////////////////////////////////////////////////////////////////////////

	//	Make it (slightly) easier to read the return type
	template<typename T, typename R = void>
	using enable_if_not_pointer = typename std::enable_if<!std::is_pointer<T>::value, R>::type;

	///
	///	\brief		If the type T is not a pointer print it
	///				directly to the ostream
	///	\author		Dean
	///	\since		10-02-2016
	///
	template <typename T>
	enable_if_not_pointer<T, std::ostream&> printElem(std::ostream& os, const T& elem)
	{
		os << elem;
		return os;
	}

	///
	///	\brief		If the type T is a pointer we dereference
	///	\author		Dean
	///	\since		10-02-2016
	///
	template <typename T>
	std::ostream& printElem(std::ostream& os, T* const& elem)
	{
		if (elem != nullptr)
		{
			os << "ptr(";
			printElem(os, *elem);
			os << ")";
		}
		else
		{
			os << "nullptr";
		}
		return os;
	}

	///
	///	\brief		Specialization for handling nullptr_t directly
	///	\author		Dean
	///	\since		02-04-2016
	///
	template <>
	std::ostream& printElem(std::ostream& os, const std::nullptr_t& elem)
	{
		os << "nullptr";
		return os;
	}

}

namespace
{
	template <size_t N>
	struct print_tuple
	{
		///
		///	\brief		Prints the CONTENTS of the first element of
		///				the typelist to the ostream.  Then recursively
		///				loops over remaining elements.
		///	\since		02-04-2016
		///	\author		Dean
		///
		template <typename... T>
		static
			typename std::enable_if < (N < sizeof...(T))>::type
			print(std::ostream& os, const std::tuple<T...>& t)
		{
			//	Print the delimiter (if required)
			const std::string DELIMITER = ", ";
			if (N > 0)	os << DELIMITER;

			//	Print the element and recurse to next element
			printElem(os, std::get<N>(t));
			print_tuple<N + 1>::print(os, t);
		}

		///
		///	\brief		List terminator
		///	\since		02-04-2016
		///	\author		Dean
		///
		template <typename... T>
		static
			typename std::enable_if < !(N < sizeof...(T))>::type
			print(std::ostream&, const std::tuple<T...>&)
		{ }

		///
		///
		///	\brief		Prints the TYPE of the first element of
		///				the typelist to the ostream.  Then recursively
		///				loops over remaining elements.
		///	\since		02-04-2016
		///	\author		Dean
		///
		template <typename... T>
		static
			typename std::enable_if < (N < sizeof...(T))>::type
			printType(std::ostream& os, const std::tuple<T...>& t)
		{
			//	Print the delimiter (if required)
			const std::string DELIMITER = ", ";
			if (N > 0) os << DELIMITER;

			//	Print the element and recurse to next element
			os << typeid(std::get<N>(t)).name();
			print_tuple<N + 1>::printType(os, t);
		}

		///
		///	\brief		List terminator
		///	\since		02-04-2016
		///	\author		Dean
		///
		template <typename... T>
		static
			typename std::enable_if < !(N < sizeof...(T))>::type
			printType(std::ostream& os, const std::tuple<T...>& t)
		{ }
	};
}

////////////////////////////////////////////////////////////////////////////////
////							std::array									////
////////////////////////////////////////////////////////////////////////////////

///
///	\brief		Provide an ostream operator for std::array
///	\author		Dean
///	\since		03-04-2016
///
template <typename T, size_t N>
std::ostream& operator<<(std::ostream& os, const std::array<T, N>& arr)
{
	const std::string DELIMITER = ", ";

	//	Header (prints the vector with the template type)
	os << "std::array<" << typeid(T).name() << "> = { ";

	//	Overview of the data
	os << "size: " << arr.size();

	//	List each element (dereferencing until we reach a non-pointer
	//	as required)
	os << ", elements: [ ";
	for (auto it = arr.cbegin(); it != arr.cend(); ++it)
	{
		//	Delimiter (only after the first element)
		if (it != arr.cbegin()) os << DELIMITER;

		//	print the element
		printElem(os, *it);
	}
	os << " ]";

	//	Close output
	os << " }";

	return os;
}

////////////////////////////////////////////////////////////////////////////////
////							std::vector									////
////////////////////////////////////////////////////////////////////////////////

///
///	\brief		Provide an ostream operator for vectors
///	\author		Dean
///	\since		10-02-2016
///
template <typename T>
std::ostream& operator<<(std::ostream& os, const std::vector<T>& vec)
{
	const std::string DELIMITER = ", ";

	//	Header (prints the vector with the template type)
	os << "std::vector<" << typeid(T).name() << "> = { ";

	//	Overview of the data
	os << "size: " << vec.size();

	//	List each element (dereferencing until we reach a non-pointer
	//	as required)
	os << ", elements: [ ";
	for (auto it = vec.cbegin(); it != vec.cend(); ++it)
	{
		//	Delimiter (only after the first element)
		if (it != vec.cbegin()) os << DELIMITER;

		//	print the element
		printElem(os, *it);
	}
	os << " ]";

	//	Close output
	os << " }";

	return os;
}

////////////////////////////////////////////////////////////////////////////////
////							std::map									////
////////////////////////////////////////////////////////////////////////////////

///
///	\brief		Provide an ostream operator for maps
///	\author		Dean
///	\since		10-02-2016
///
template <typename K, typename V>
std::ostream& operator<<(std::ostream& os, const std::map<K, V>& map)
{
	const std::string DELIMITER = ", ";

	//	Header (prints the vector with the template type)
	os << "std::map<" << typeid(K).name()
		<< ", " << typeid(V).name() << "> = { ";

	//	Overview of the data
	os << "size: " << map.size();

	//	List each element (dereferencing until we reach a non-pointer
	//	as required)
	os << ", elements: [ ";
	for (auto it = map.cbegin(); it != map.cend(); ++it)
	{
		//	Delimiter (only after the first element)
		if (it != map.cbegin()) os << DELIMITER;

		//	print the key-value pair
		os << "{ ";
		printElem(os, it->first);
		os << " : ";
		printElem(os, it->second);
		os << "}";
	}
	os << " ]";

	//	Close output
	os << " }";

	return os;
}

////////////////////////////////////////////////////////////////////////////////
////							std::tuple									////
////////////////////////////////////////////////////////////////////////////////

///
///	\brief		Provide an ostream operator for empty tuples
///				Uses code from 'The C++ Programming Language (4th Edition)'
///				Section 28.6.4 (pages 817, 818).
///	\author		Dean
///	\since		02-04-2016
///
inline std::ostream& operator<<(std::ostream& os, const std::tuple<>&)
{
	//	For a tuple we wont print the normal header
	os << "std::tuple<> = { }";
	return os;
}

///
///	\brief		Provide an ostream operator for non-empty tuples
///				Uses code from 'The C++ Programming Language (4th Edition)'
///				Section 28.6.4 (pages 817, 818).
///	\author		Dean
///	\since		02-04-2016
///
template <typename T0, typename ...T>
inline std::ostream& operator<<(std::ostream& os, const std::tuple<T0, T...>& t)
{
	//	Header: use the helper function to recurse list
	os << "std::tuple<";
	print_tuple<0>::printType(os, t);
	os << "> = ";

	//	Elements: use the helper function to recurse list
	os << "{ ";
	print_tuple<0>::print(os, t);
	os << " }";
	return os;
}

////////////////////////////////////////////////////////////////////////////////
////							std::pair									////
////////////////////////////////////////////////////////////////////////////////

///
///	\brief		Provide an ostream operator for std::pair
///	\author		Dean
///	\since		03-04-2016
///
template <typename T, typename U>
std::ostream& operator<<(std::ostream& os, const std::pair<T, U>& pair)
{
	const std::string DELIMITER = ", ";

	os << "std::pair<"
		<< typeid(T).name() << DELIMITER
		<< typeid(U).name() << "> = { ";
	printElem(os, pair.first);
	os << DELIMITER;
	printElem(os, pair.second);
	os << " }";
	return os;
}

////////////////////////////////////////////////////////////////////////////////
////							std::set									////
////////////////////////////////////////////////////////////////////////////////

///
///	\brief		Provide an ostream operator for std::set
///	\author		Dean
///	\since		03-04-2016
///
template <typename K>
std::ostream& operator<<(std::ostream& os, const std::set<K>& set)
{
	const std::string DELIMITER = ", ";

	os << "std::set<"
		<< typeid(K).name() << "> = { "

		//	Overview of the data
		<< "size: " << set.size() << DELIMITER
		<< "elements: [ ";

	for (auto it = set.cbegin(); it != set.cend(); ++it)
	{
		//	Delimiter (only after the first element)
		if (it != set.cbegin()) os << DELIMITER;

		//	print the key-value pair
		printElem(os, *it);
	}
	os << " ]";

	//	Close output
	os << " }";
	return os;
}

////////////////////////////////////////////////////////////////////////////////
////							std::multiset								////
////////////////////////////////////////////////////////////////////////////////

///
///	\brief		Provide an ostream operator for std::multiset
///	\author		Dean
///	\since		03-04-2016
///
template <typename K>
std::ostream& operator<<(std::ostream& os, const std::multiset<K>& mset)
{
	const std::string DELIMITER = ", ";

	os << "std::multiset<"
		<< typeid(K).name() << "> = { "

		//	Overview of the data
		<< "size: " << mset.size() << DELIMITER
		<< "elements: [ ";

	for (auto it = mset.cbegin(); it != mset.cend(); ++it)
	{
		//	Delimiter (only after the first element)
		if (it != mset.cbegin()) os << DELIMITER;

		//	print the key-value pair
		printElem(os, *it);
	}
	os << " ]";

	//	Close output
	os << " }";
	return os;
}

#endif
