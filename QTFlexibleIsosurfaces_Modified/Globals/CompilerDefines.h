#ifndef COMPILER_DEFINES_H
#define COMPILER_DEFINES_H

#include "GUI/Widgets/Colour.h"

//	constant for empty selection
const long NO_CONTOUR_SELECTED = -1;

////////////////////////////////////////////////////////////////////////////////
///																			 ///
///								Build environment							 ///
///																			 ///
////////////////////////////////////////////////////////////////////////////////

#define LAB_PC

////////////////////////////////////////////////////////////////////////////////
///																			 ///
///								Multiple uses								 ///
///																			 ///
////////////////////////////////////////////////////////////////////////////////

//	If defined the output of stdout and stderr will be written directly to
//  files "stdout.txt" and "stderr.txt"
//#define STDOUT_TO_FILE
//#define STDOUT_TO_NULL
//#define STDERR_TO_FILE

////////////////////////////////////////////////////////////////////////////////
///																			 ///
///							Contour Tree Construction						 ///
///																			 ///
////////////////////////////////////////////////////////////////////////////////

//#define PRIORITY_HEIGHT         0
//#define PRIORITY_VOLUME         1
//#define PRIORITY_HYPERVOLUME    2
//#define PRIORITY_RIEMANN_SUM    3

#define JOIN_TREE_FILENAME "Debug\\Jointree.dot"
#define SPLIT_TREE_FILENAME "Debug\\Splittree.dot"

//  If defined, will provide detailed output of the split
//  and join tree creation processes.
//#define DEBUG_JOIN_TREE
//#define DEBUG_SPLIT_TREE

//	Performs a brute force check of algorithm output against reference data
//#define CHECK_REFERENCE_DATA_SETS

#ifdef CHECK_REFERENCE_DATA_SETS
	//#define ENABLE_PRINT_CONTOUR_TREE //- currently broken
	#define OUTPUT_CONTOUR_TREE_MORE
	#define OUTPUT_CONTOUR_TREE_LESS
	#define CONTOUR_TREE_MORE_FILENAME "Debug\\ContourTree.dot"
	#define CONTOUR_TREE_LESS_FILENAME "Debug\\ContourTree_Small.dot"
	#define CHECK_FILES_MATCH_REFERENCE_DATA
#endif

//#define ENABLE_PRINT_CONTOUR_TREE
//#define DEBUG_COMBINE_TREES

//#define DEBUG_SUPERNODE_DISTRIBUTION


////////////////////////////////////////////////////////////////////////////////
///																			 ///
///							Heightfield										 ///
///																			 ///
////////////////////////////////////////////////////////////////////////////////

//#define FILE_TYPE_ASCII         0
//#define FILE_TYPE_VOL           1
//#define FILE_TYPE_RAW           2
//#define FILE_TYPE_MHA           3
//#define FILE_TYPE_MIRG          4
///////////////////////////////////
//#define FILE_TYPE_VOL1			5	//	Scalar field (3D)
//#define FILE_TYPE_VOL3			6	//	Vector field (3D)
///////////////////////////////////
//#define FILE_TYPE_HVOL1			7	//	Scalar field (4D)

#define FILTER_STRING_TXT "Text File (*.txt)"
#define FILTER_STRING_VOL "Volume File (*.vol)"
#define FILTER_STRING_VOL1 "3D QCD Field Data (*.vol1)"
#define FILTER_STRING_OBJ "Object Files (*.obj)"
#define FILTER_STRING_DOT "Graph File (*.dot)"
#define FILTER_STRING_HVOL1 "4D QCD Field Data (*.hvol1)"
#define FILTER_STRING_HVOL1_VOL1 "QCD Field data (*.vol1 *.hvol1)"
#define FILTER_ALL "All files (*.*)"

//#define FILE_TYPE_UNKNOWN		255

//#endif // COMPILERDEFINES


////////////////////////////////////////////////////////////////////////////////
///																			 ///
///							Rendering										 ///
///																			 ///
////////////////////////////////////////////////////////////////////////////////

#define DEFAULT_HIGHLIGHT_COLOUR Colour::Integer::Greens::LIME_GREEN


#define DEFAULT_SHADER "simplegradientshading"

////////////////////////////////////////////////////////////////////////////////
///																			 ///
///							Isocontour generation							 ///
///																			 ///
////////////////////////////////////////////////////////////////////////////////

//	Contours will be computed on the mainthread, serially.  If disabled,
//	contours that require generation will detach from the mainthread and
//	be computed in parallel
#define GENERATE_ALL_CONTOURS_ON_MAIN_THREAD

//	Number of decimal places used for computation and storage of meshes
#define WORKING_PRECISION 8

//	If defined an inital set of contours will be generated at the midpoint of
//	each superarc to be use for object matching
//#define GENERATE_INITAL_CONTOURS

#endif
