#ifndef GLOBALS
#define GLOBALS

#include "Exceptions.h"
#include "TypeDefines.h"
#include "CompilerDefines.h"

#ifndef __func__
#define __func__ __FUNCTION__
#endif

//	these are to simplify processing of join tree and split tree

//#ifdef _MSC_VER
//#define MINUS_INFINITY -numeric_limits<Real>::infinity()
//#define PLUS_INFINITY numeric_limits<Real>::infinity()
//#else
static constexpr Real MINUS_INFINITY = -numeric_limits<Real>::infinity();
static constexpr Real PLUS_INFINITY = numeric_limits<Real>::infinity();
//#endif

#endif // GLOBALS

