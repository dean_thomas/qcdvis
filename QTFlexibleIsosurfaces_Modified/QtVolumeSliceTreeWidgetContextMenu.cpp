#include "QtVolumeSliceTreeWidgetContextMenu.h"

QtVolumeSliceTreeWidgetContextMenu::QtVolumeSliceTreeWidgetContextMenu(QWidget *parent)
	: QMenu(parent)
{
	QAction *actionSliceByX = new QAction("Partiton for all values of X", this);
	connect(actionSliceByX, SIGNAL(triggered(bool)),
			this, SLOT(onClickPartitionByX()));

	QAction *actionSliceByY = new QAction("Partiton for all values of Y", this);
	connect(actionSliceByY, SIGNAL(triggered(bool)),
			this, SLOT(onClickPartitionByY()));


	QAction *actionSliceByZ = new QAction("Partiton for all values of Z", this);
	connect(actionSliceByZ, SIGNAL(triggered(bool)),
			this, SLOT(onClickPartitionByZ()));

	QAction *actionSliceByT = new QAction("Partiton for all values of T", this);
	connect(actionSliceByT, SIGNAL(triggered(bool)),
			this, SLOT(onClickPartitionByT()));

	m_actions << actionSliceByX << actionSliceByY;
	m_actions << actionSliceByZ << actionSliceByT;

	addActions(m_actions);
}

void  QtVolumeSliceTreeWidgetContextMenu::onClickPartitionByX()
{
	if (m_heightField4d != nullptr)
	{
		m_heightField4d->SliceByX(true);
	}
}

void  QtVolumeSliceTreeWidgetContextMenu::onClickPartitionByY()
{
	if (m_heightField4d != nullptr)
	{
		m_heightField4d->SliceByY(true);
	}
}

void  QtVolumeSliceTreeWidgetContextMenu::onClickPartitionByZ()
{
	if (m_heightField4d != nullptr)
	{
		m_heightField4d->SliceByZ(true);
	}
}

void  QtVolumeSliceTreeWidgetContextMenu::onClickPartitionByT()
{
	if (m_heightField4d != nullptr)
	{
		m_heightField4d->SliceByT(true);
	}
}

void QtVolumeSliceTreeWidgetContextMenu::SetHeightField4d(HeightField4D *heightfield4d)
{
	m_heightField4d = heightfield4d;
}
