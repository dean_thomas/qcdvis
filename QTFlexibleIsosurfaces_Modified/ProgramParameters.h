#ifndef PROGRAMPARAMETERS_H
#define PROGRAMPARAMETERS_H

struct ProgramParameters
{
    //  Set's the filename to launch the program
    //  'file=<filename>'
    std::string filename = "";

	std::string list = "";

    //  Set's the dimensions of the heightfield
    //  'size=x,y,z'
    unsigned int dimX = 0;
    unsigned int dimY = 0;
    unsigned int dimZ = 0;

    //  Set's the resolution of the samples
    //  'res=<1/2>'
    unsigned int resolution = 0;

    //  Will keep a record of the global minima and maxima
    //  across all time/cooling slices for output later
    bool limitsFromFirstStep = true;

    bool periodicBoundaries = false;

    //  Hide's the gui (for scripting)
    bool noGUI = false;
};

#endif // PROGRAMPARAMETERS_H
