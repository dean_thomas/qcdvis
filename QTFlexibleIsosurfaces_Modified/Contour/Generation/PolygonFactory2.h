#ifndef POLYGONFACTORY2_H
#define POLYGONFACTORY2_H

#include "Globals/Globals.h"
#include "Geometry/Polygon3.h"
#include "Contour/Storage/Contour.h"
#include <HeightField/HeightField3D.h>
#include "PolylineList.h"

///
/// \brief		A wrapped used to aid construction of a polygon on a boundary
/// \since		06-05-2015
/// \author		Dean
///
struct PolygonConstruction
{
		void ReverseWinding()
		{
			polygon->ReverseWinding();

			updateProperties();
		}

		enum class PolygonType
		{
			SPANS_SINGLE_EDGE		=	0,
			SPANS_CORNER			=   1,
			SPANS_OPPOSING_EDGES	=	2,
			INVALID					=	255
		};

		bool active;

		//	The polygon under construction
		Polygon2D *polygon;

		//	Flags to tell us if the start and end vertices touch each boundary
		bool intersectionMinX;
		bool intersectionMaxX;
		bool intersectionMinY;
		bool intersectionMaxY;

		//	Limits of the heightfield data axes (for the plane)
		Real dataMinX;
		Real dataMinY;
		Real dataMaxX;
		Real dataMaxY;

		//	Limits of the heightfield data axes as points in 2D space
		Point2D dataMinXminY;
		Point2D dataMinXmaxY;
		Point2D dataMaxXminY;
		Point2D dataMaxXmaxY;

		//	Distance to other end of polygon on x and y axis
		Real distanceX;
		Real distanceY;

		//	Direction of travel along each axis (±1 or 0).
		short deltaX;
		short deltaY;

		//	*Current* classification of polygon based upon start and end
		//	vertices.
		PolygonType previousPolygonType;
		PolygonType polygonType;

		///
		/// \brief	PolygonConstruction
		/// \param	polygon: a pointer to the partially constructed polygon
		/// \param	minX: the min value of the PLANE the polygon is being
		///			constructed for.
		/// \param	maxX: the max value of the PLANE the polygon is being
		///			constructed for.
		/// \param	minY: the min value of the PLANE the polygon is being
		///			constructed for.
		/// \param	maxY: the max value of the PLANE the polygon is being
		///			constructed for.
		/// \since	06-05-2015
		/// \author	Dean
		/// \author	Dean: update to use initializers and pass in an existing
		///			partially constructed polygon [29-05-2015].
		///
		PolygonConstruction(Polygon2D* openPolygon,
							const Real minX, const Real maxX,
							const Real minY, const Real maxY)
			: dataMinX{minX}, dataMinY{minY}, dataMaxX{maxX}, dataMaxY{maxY},
			  dataMinXminY{minX, minY}, dataMinXmaxY{minX, maxY},
			  dataMaxXminY{maxY, minY}, dataMaxXmaxY{maxX, maxY},
			  active{true}
		{
			polygon = openPolygon;

			previousPolygonType = polygonType = PolygonType::INVALID;

			//	Initialize to avoid bugs >:(
			intersectionMinX = intersectionMaxX
					= intersectionMinY = intersectionMaxY = false;

			//	Calculate the classification of the polygon
			updateProperties();

			//	Log to console
			string polygonTypeString;
			switch(polygonType)
			{
				case PolygonType::SPANS_CORNER:
					polygonTypeString = "corner spanning";
					break;
				case PolygonType::SPANS_OPPOSING_EDGES:
					polygonTypeString = "spanning opposing edges";
					break;
				case PolygonType::SPANS_SINGLE_EDGE:
					polygonTypeString = "contained to single edge";
					break;
			}
			fprintf(stderr, "Current Polygon is %s.\n", polygonTypeString.c_str());
			fflush(stderr);
		}

		///
		/// \brief		Returns if the polygon straddles opposing boundaries
		/// \return		boolean: true if the polygon sits on opposing
		///				boundaries.
		/// \since		06-05-2015
		/// \author		Dean
		///
		bool IsOnOpposingBoundaries() const
		{
			return ((intersectionMinX && intersectionMaxX)
					|| (intersectionMinY && intersectionMaxY));
		}


		~PolygonConstruction()
		{
			//	Don't delete the polygon - we will transfer ownership instead
			//delete polygon;
		}

		bool classificationHasChanged()
		{
			return (previousPolygonType != polygonType);
		}

		///
		/// \brief		Calculate properties of the polygon by looking at the
		///				first and last vertices of the polygon.
		/// \since		06-05-2015
		/// \author		Dean
		///
		void updateProperties()
		{
			Point2D startPoint = polygon->GetFirstEdge().GetVertexA();
			Point2D endPoint = polygon->GetLastEdge().GetVertexB();

			//	Direction we need to move on the x axis
			if (startPoint.GetX() > endPoint.GetX())
			{
				deltaX = 1;
			}
			else if (startPoint.GetX() < endPoint.GetX())
			{
				deltaX = -1;
			}
			else deltaX = 0;

			//	Direction we need to move on the y axis
			if (startPoint.GetY() > endPoint.GetY())
			{
				deltaY = 1;
			}
			else if (startPoint.GetY() < endPoint.GetY())
			{
				deltaY = -1;
			}
			else deltaY = 0;

			if ((deltaX == 0) && (deltaY == 0))
			{
				//	This shouldn't happen?!
				fprintf(stderr, "Error!  Points are at the same position!");
				fflush(stderr);
			}

			//	Find what edges of the plane the polygon touches
			if (startPoint.EqualsX(dataMinX) || endPoint.EqualsX(dataMinX))
			{
				intersectionMinX = true;
			}

			if (startPoint.EqualsX(dataMaxX) || endPoint.EqualsX(dataMaxX))
			{
				intersectionMaxX = true;
			}

			if (startPoint.EqualsY(dataMinY) || endPoint.EqualsY(dataMinY))
			{
				intersectionMinY = true;
			}

			if (startPoint.EqualsY(dataMaxY) || endPoint.EqualsY(dataMaxY))
			{
				intersectionMaxY = true;
			}

			//	Work out what configuration we are dealing with
			//	Storing the previous so we can detect changes
			previousPolygonType = polygonType;

			if (((deltaX == 0) && (deltaY != 0))
				|| (deltaX != 0) && (deltaY == 0))
			{
				if (!IsOnOpposingBoundaries())
				{
					polygonType = PolygonType::SPANS_SINGLE_EDGE;
				}
				else
				{
					polygonType = PolygonType::SPANS_OPPOSING_EDGES;
				}
			}
			else
			{
				if (GetBoundaryIntersectionCount() == 2)
				{
					polygonType = PolygonType::SPANS_CORNER;
				}
				else
				{
					fprintf(stderr, "Unknown polygon configuration!\n");
					fflush(stderr);
				}
			}

			if ((previousPolygonType != PolygonType::INVALID)
				&& classificationHasChanged())
			{
				string polygonTypeString;
				switch(polygonType)
				{
					case PolygonType::SPANS_CORNER:
						polygonTypeString = "corner spanning";
						break;
					case PolygonType::SPANS_OPPOSING_EDGES:
						polygonTypeString = "spanning opposing edges";
						break;
					case PolygonType::SPANS_SINGLE_EDGE:
						polygonTypeString = "contained to single edge";
						break;
				}

				string previousPolygonTypeString;
				switch(previousPolygonType)
				{
					case PolygonType::SPANS_CORNER:
						previousPolygonTypeString = "corner spanning";
						break;
					case PolygonType::SPANS_OPPOSING_EDGES:
						previousPolygonTypeString = "spanning opposing edges";
						break;
					case PolygonType::SPANS_SINGLE_EDGE:
						previousPolygonTypeString = "contained to single edge";
						break;
				}
				fprintf(stderr, "**Polygon type has changed from %s to %s.\n",
						previousPolygonTypeString.c_str(),
						polygonTypeString.c_str());
				fflush(stderr);
			}
		}

		///
		/// \brief		Add's an edge to the front of the polygon
		/// \param		edge: the 2D edge to be added
		/// \since		06-05-2015
		/// \author		Dean
		/// \author		Dean: update properties after adding an edge (allowing
		///				polygon to change spanning type etc after adding an
		///				edge) [01-06-2015].
		///
		void AddEdgeToFront(const Edge2D& edge)
		{
			//	Update the actual polygon
			polygon->AddEdgeToFront(edge);

			//	Update known information of this polygon
			updateProperties();
		}

		///
		/// \brief		Add's an edge to the back of the polygon
		/// \param		edge: the 2D edge to be added
		/// \since		06-05-2015
		/// \author		Dean
		/// \author		Dean: update properties after adding an edge (allowing
		///				polygon to change spanning type etc after adding an
		///				edge) [01-06-2015].
		///
		void AddEdgeToBack(const Edge2D& edge)
		{
			//	Update the actual polygon
			polygon->AddEdgeToBack(edge);

			//	Update known information of this polygon
			updateProperties();
		}

		///
		/// \brief		Counts the number of 2D boundaries the polygon
		///				intersects (looking at first and last vertices only).
		/// \return		An unsigned integer; allowable values should be limited
		///				to 1 or 2 (0 should mean that the polygon is contained
		///				completely in the plane, thus shouldn't require
		///				closing).
		/// \since		06-05-2015
		/// \author		Dean
		/// \author		Dean: change to use separate flags for each boundary
		///				[29-05-2015].
		///
		unsigned long GetBoundaryIntersectionCount() const
		{
			unsigned long total = 0;

			if (intersectionMinX) ++total;
			if (intersectionMaxX) ++total;
			if (intersectionMinY) ++total;
			if (intersectionMaxY) ++total;

			return total;
		}

		bool AddPolygonToBack(Polygon2D *rhs)
		{
			bool result = polygon->AddPolylineToBack(rhs);

			if (result)
			{
				updateProperties();
				return result;
			}
			else
			{
				fprintf(stderr, "Failed to merge two polygons!"
								"  In function %s (file: %f, line %ld)\n",
						__func__, __FILE__, __LINE__);
				fflush(stderr);
				return result;
			}
		}
};

struct PolygonDistancePair
{
		PolygonConstruction* polygon;
		Point2D position;
		Real distance;
		unsigned long indexOfPolygon;

		PolygonDistancePair()
			: polygon{nullptr},
			  distance{std::numeric_limits<Real>::max()},
			  indexOfPolygon{std::numeric_limits<unsigned long>::max()}
		{}
};


class PolygonFactory2
{
	private:
		HeightField3D::Boundary m_targetBoundary;
		Contour* m_contour;
		HeightField3D* m_heightfield;

		//	Limits of the 2D plane
		Real m_planeMinX, m_planeMaxX;
		Real m_planeMinY, m_planeMaxY;

		//	The value of the 3rd dimension - used for filtering out 3D edges on
		//	the wrong face
		Real m_filterValue;

		//	Polylines relevant to this boundary
		vector<Edge2D> m_filteredEdges;

		//	Internal store for completed polygons
		vector<Polygon2D*> m_polygonStore;

		vector<Polygon2D *> storeSimplePolygons();

		Polygon2D* constructPolylines(vector<Edge2D> &unorderedEdges) const;

		//	Store a list of all polylines originating from and terminating at
		//	each boundary.  Each line should be a member of exactly two
		//	lists.
		PolylineList2 m_minX;
		PolylineList2 m_maxX;
		PolylineList2 m_minY;
		PolylineList2 m_maxY;

		void removePolylineFromLists(Polygon2D* thePolyline);

		void closePolygons();

		void filterEdges(const vector<Edge3D> &unfilteredEdges);
		Polygon2D *pickNextEdgeOnMinYBoundary(const Real &currentX, const DirectionOfTravel &directionOfTravel);
		void sortPolylines(vector<Polygon2D *> &unsortedPolylines);

		//void calculateCornerPolygon(PolygonConstruction* polygon);
		void process3dPolygon(Polygon3D* originalPolygon);
		void growAndMergePolygons(
				const HeightField3D::Boundary &plane, vector<Polygon2D*> &polygonList);

		void growAndMergePolygonOnEdge(
				PolygonConstruction *poly, vector<PolygonConstruction *> &polygons);
		void growAndMergePolygonOnCorner(
				PolygonConstruction *poly, vector<PolygonConstruction *> &polygons);

		double getNextIntegerLatticePoint(const double &currentPos,
										  const long &delta) const;
		PolygonDistancePair findNearestPolygonToPoint(
				vector<PolygonConstruction*> &polygons, const Point2D &point);

		vector<Polygon2D*> transferOwnershipOfPolygons();
	public:

		vector<Polygon2D *> ProcessEdges(const vector<Edge3D> &unfilteredEdges);

		PolygonFactory2(const HeightField3D::Boundary &targetBoundary,
						HeightField3D *heightfield, Contour *contour);
		~PolygonFactory2();
};

#endif // POLYGONFACTORY_H
