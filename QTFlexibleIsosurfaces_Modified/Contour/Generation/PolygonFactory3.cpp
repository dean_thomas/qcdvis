#include "PolygonFactory3.h"

PolygonFactory3::PolygonFactory3(HeightField3D *heightfield, Contour *contour)
{
	m_contour = contour;
	m_heightfield = heightfield;
}

PolygonFactory3::~PolygonFactory3()
{

}

///
/// \brief		Clears the internal vector of polygons and transfers ownership
///				amay from the factory class.
/// \return		a vector of 3D polygons
/// \since		03-06-2015
/// \author		Dean
///
vector<Polygon3D*> PolygonFactory3::transferOwnershipOfPolygons()
{
	//	Copy the list for return
	vector<Polygon3D*> result = m_3dPolygons;

	//	Clear out the internal list (but don't delete the polygons, essentially
	//	transfer ownership).
	m_3dPolygons.clear();

	//	And return them
	return result;
}

///
/// \brief		Takes an unordered set of edges (in 3D space) and turns them
///				into a skew	polygon (a non-planar polygon in 3D space)
/// \param		unorderedEdges: passed by reference, will be modified as edges
///				are removed from the list
/// \return		Polygon3D: a (not necessarily) skew polygon created from
///				the	edges provided.
/// \since		20-04-2015
/// \author		Dean
/// \author		Dean: modify to be distinct from that operating in 2D
///				[28-05-2015]
///
Polygon3D *PolygonFactory3::constructSkewPolygon(
		vector<MeshEdge3D*> &unorderedEdges) const
{
#define SUPRESS_OUTPUT

#define WASTED_LOOPS_MAX 2

	//	Convenience variables
	const Real X_MAX = m_heightfield->GetMaxX();
	const Real Y_MAX = m_heightfield->GetMaxY();
	const Real Z_MAX = m_heightfield->GetMaxZ();

	//	Create a new polygon
	Polygon3D *polygon = new Polygon3D();

	//	Take the first edge from the list and add to the polygon
	MeshEdge3D* firstEdge = unorderedEdges[0];
	unorderedEdges.erase(unorderedEdges.begin());
	polygon->AddEdgeToBack(firstEdge->ToEdge3D());

	//	Keep track of loops with no edges found
	unsigned long wastedLoops = 0;

	//	We keep looping until the polygon is completed, we run out of edges or
	//	nothing is cmopleted after several attempts
	while (!polygon->IsClosed()
		   && (unorderedEdges.size() > 0)
		   && (wastedLoops < WASTED_LOOPS_MAX))
	{
		//	Use as a safe guard - if no edges are found to add to the polygon
		//	we can increment a counter
		bool polygonHasUpdated = false;

		///	Add at the end of the current polygon
		///
		//	Check each edge in the list to see if it is incident on the last
		//	vertex added to the polygon
		for (unsigned long i = 0; i < unorderedEdges.size(); ++i)
		{

			bool currentEdgeWasAdded = false;
			Edge3D currentEdge = unorderedEdges[i]->ToEdge3D();

			//	Edge is in the correct vertex order
			if (currentEdge.GetVertexA() ==
				polygon->GetLastEdge().GetVertexB())
			{
				//	Add the edge
				polygon->AddEdgeToBack(currentEdge);

				//	And remove from the list
				unorderedEdges.erase(unorderedEdges.begin()+i);

				//	Reset the wasted loop counter and mark that the currentEdge
				//	was used
				polygonHasUpdated = true;
				currentEdgeWasAdded = true;

				//	Drop out of for loop
				//break;
			}
			//	Edge is in the reversed vertex order
			else if (currentEdge.GetVertexB() ==
					 polygon->GetLastEdge().GetVertexB())
			{
				//	Add the edge but reverse first
				polygon->AddEdgeToBack(
							currentEdge.GetReversedForm());

				//	And remove from the list
				unorderedEdges.erase(unorderedEdges.begin()+i);

				//	Reset the wasted loop counter and mark that the currentEdge
				//	was used
				polygonHasUpdated = true;
				currentEdgeWasAdded = true;

				//	Drop out of for loop
				//break;
			}
			///	Add at the start of the current polygon
			///
			else if (currentEdge.GetVertexA() ==
					 polygon->GetFirstEdge().GetVertexA())
			{
				//	Add the edge
				polygon->AddEdgeToFront(
							currentEdge.GetReversedForm());

				//	And remove from the list
				unorderedEdges.erase(unorderedEdges.begin()+i);

				//	Reset the wasted loop counter and mark that the currentEdge
				//	was used
				polygonHasUpdated = true;
				currentEdgeWasAdded = true;

				//	Drop out of for loop
				//break;
			}
			else if (currentEdge.GetVertexB() ==
					 polygon->GetFirstEdge().GetVertexA())
			{
				//	Add the edge
				polygon->AddEdgeToFront(currentEdge);

				//	And remove from the list
				unorderedEdges.erase(unorderedEdges.begin()+i);

				//	Reset the wasted loop counter and mark that the currentEdge
				//	was used
				polygonHasUpdated = true;
				currentEdgeWasAdded = true;

				//	Drop out of for loop
				//break;
			}
		}

		if (!polygonHasUpdated)
		{
#if defined(VERBOSE_OUTPUT) && !defined(SUPRESS_OUTPUT)
			//	Increment the wasted loop counter as no matcing edge was found
			printf("Incremented wasted loops.\n");
			fflush(stdout);
#endif
			++wastedLoops;
		}
		else
		{
			wastedLoops = 0;
		}
	}
	return polygon;

#undef SUPRESS_OUTPUT
}

///
/// \brief		Given a list of edges from the mesh, process them to build
///				3D skew polygons for each hole in a contour.  These are then
///				flattened to 2D polygons on each boundary.
/// \param		unorderedEdges: the unordered 3d edges from the mesh
/// \since		27-05-2015
/// \author		Dean
/// \author		Dean: return the list of polygons automatically instead of
///				waiting for the programmer to request them. [03-06-2015]
///
vector<Polygon3D*> PolygonFactory3::ProcessEdges(vector<MeshEdge3D *> &unorderedEdges)
{
	//	We shouldn't have anything to clear in reality (polygon's should have
	//	transferred ownership before now).
	m_3dPolygons.clear();

	while (!unorderedEdges.empty())
	{
		//	Create a new polygon (in 3D) from the edge list
		Polygon3D *currentPolygon = constructSkewPolygon(unorderedEdges);

		//	Add the 3D polygons to a list that can be retrieved and visualized
		m_3dPolygons.push_back(currentPolygon);
	}

	//	Return the polygons
	return transferOwnershipOfPolygons();
}
