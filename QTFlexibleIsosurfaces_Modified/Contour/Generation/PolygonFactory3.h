#ifndef POLYGONFACTORY3_H
#define POLYGONFACTORY3_H

#include "Globals/Globals.h"
#include "Geometry/Polygon3.h"
#include "Contour/Storage/Contour.h"
#include <HeightField/HeightField3D.h>

///
/// \brief		Constructs a 3D skew polygon from a set of unordered edges
/// \since		03-06-2015
/// \author		Dean: separated from previous 2D/3D polygon factory class
///				[03-06-2015]
///
class PolygonFactory3
{
	private:
		Contour* m_contour;
		HeightField3D* m_heightfield;

		vector<Polygon3D*> m_3dPolygons;
		Polygon3D *constructSkewPolygon(vector<MeshEdge3D *> &unorderedEdges) const;
		vector<Polygon3D*> transferOwnershipOfPolygons();
	public:
		vector<Polygon3D *> ProcessEdges(vector<MeshEdge3D*> &unorderedEdges);

		PolygonFactory3(HeightField3D *heightfield, Contour *contour);
		~PolygonFactory3();
};

#endif // POLYGONFACTORY3_H
