#include "PolylineList.h"

void PolylineList2::AddPolylineToOriginList(Polygon2D* polyline)
{
	m_originatingPolylines.push_back(polyline);
}

void PolylineList2::AddPolylineToTerminatingList(Polygon2D* polyline)
{
	m_terminatingPolylines.push_back(polyline);
}


void PolylineList2::RemovePolyline(Polygon2D* polyline)
{
	for (unsigned i = 0; i < m_originatingPolylines.size(); ++i)
	{
		//	Index of polygon to be checked
		unsigned long index = m_originatingPolylines.size()-1-i;

		//	Loop through in reverse
		Polygon2D* currentPolyline = m_originatingPolylines[index];

		if (polyline == currentPolyline)
			m_originatingPolylines.erase(m_originatingPolylines.begin() + index);
	}
}

bool sortingDistanceAscending(PolylineDistanceRecord &lhs, PolylineDistanceRecord &rhs)
{
	return lhs.distance < rhs.distance;
}

///
/// \brief		Given a point on the real number line, looks for the nearest
///				polyline that originates or terminates in the range:
///				[value..infinity)
/// \param value
/// \return
/// \since		05-06-2015
/// \author		Dean
///
PolygonLookup PolylineList2::findNearestXGreaterThan(const Real &value)
{
	//	Easy case... both lists are empty
	if (m_originatingPolylines.empty() && m_terminatingPolylines.empty())
		return {nullptr, false};

	printf("Carrying out search for polylines starting/ending on axis.\n");
	printf("==========================================================\n");
	fflush(stdout);

	//	Calculate distance to all polyline originating from this edge
	vector<PolylineDistanceRecord> distanceToOriginatingPolylines;
	distanceToOriginatingPolylines.reserve(m_originatingPolylines.size());
	for (unsigned long i = 0; i < m_originatingPolylines.size(); ++i)
	{
		Polygon2D* currentPolyline = m_originatingPolylines[i];
		Real distanceToVertex =
				currentPolyline->GetOriginVertex().GetX() - value;

		//	Filter out vertices that are in the wrong direction of travel
		if (distanceToVertex >= 0.0)
		{
			distanceToOriginatingPolylines.push_back({currentPolyline,distanceToVertex});
		}
		else
		{
			printf("Discarded polygon beginning at X = %f.\n",
				   currentPolyline->GetOriginVertex().GetX());
			fflush(stdout);
		}
	}

	//	Calculate distance to all polyline terminating at this edge
	vector<PolylineDistanceRecord> distanceToTerminatingPolylines;
	distanceToTerminatingPolylines.reserve(m_terminatingPolylines.size());
	for (unsigned long i = 0; i < m_terminatingPolylines.size(); ++i)
	{
		Polygon2D* currentPolyline = m_terminatingPolylines[i];
		Real distanceToVertex =
				currentPolyline->GetTerminatingVertex().GetX() - value;

		//	Filter out vertices that are in the wrong direction of travel
		if (distanceToVertex >= 0.0)
		{
			distanceToTerminatingPolylines.push_back({currentPolyline,distanceToVertex});
		}
		else
		{
			printf("Discarded polygon terminating at X = %f.\n",
				   currentPolyline->GetTerminatingVertex().GetX());
			fflush(stdout);
		}
	}

	//	Sort both vectors into order
	//	NOTE: we may need to reverse the vectors if moving in the negative
	//	direction!
	std::sort(distanceToOriginatingPolylines.begin(), distanceToOriginatingPolylines.end(), sortingDistanceAscending);
	std::sort(distanceToTerminatingPolylines.begin(), distanceToTerminatingPolylines.end(), sortingDistanceAscending);

	printf("Nearest polygons from (X = %f) moving towards infinity:\n", value);
	printf("Originating from this edge: \n");
	for (unsigned long i = 0; i < distanceToOriginatingPolylines.size(); ++i)
	{
		printf("X: %f\tDistance: %f\tPointer: %X\n", distanceToOriginatingPolylines[i].polyline->GetOriginVertex().GetX(), distanceToOriginatingPolylines[i].distance, distanceToOriginatingPolylines[i].polyline);
	}
	printf("Terminating at this edge: \n");
	for (unsigned long i = 0; i < distanceToTerminatingPolylines.size(); ++i)
	{
		printf("X: %f\tDistance: %f\tPointer: %X\n", distanceToTerminatingPolylines[i].polyline->GetTerminatingVertex().GetX(),distanceToTerminatingPolylines[i].distance, distanceToTerminatingPolylines[i].polyline);
	}
	fflush(stdout);

	printf("End.\n");
	printf("==========================================================\n");
	fflush(stdout);

	//	We may return no matches in the direction of travel
	if (distanceToOriginatingPolylines.empty() && distanceToTerminatingPolylines.empty())
		return {nullptr, false};

	//	No polylines terminate here in the direction of travel but some
	//	originate, so take the smallest of these as the result
	if (distanceToTerminatingPolylines.empty())
	{
		return {distanceToOriginatingPolylines.front().polyline, false};
	}

	//	Alternatively, no polylines originate here in the direction of travel
	//	but some terminate, so take the smallest of these as the result
	if (distanceToOriginatingPolylines.empty())
	{
		return {distanceToTerminatingPolylines.front().polyline, true};
	}

	//	Worse case scenario, is that there is some of both...so look for the
	//	nearest from either list
	if (fabs(distanceToOriginatingPolylines.front().distance) < fabs(distanceToTerminatingPolylines.front().distance))
	{
		return {distanceToOriginatingPolylines.front().polyline, false};
	}
	else
	{
		return {distanceToTerminatingPolylines.front().polyline, true};
	}
}

///
/// \brief		Given a point on the real number line, looks for the nearest
///				polyline that originates or terminates in the range:
///				[0..value)
/// \param value
/// \return
/// \since		05-06-2015
/// \author		Dean
///
PolygonLookup PolylineList2::findNearestXLessThan(const Real &value)
{
	//	Easy case... both lists are empty
	if (m_originatingPolylines.empty() && m_terminatingPolylines.empty())
		return {nullptr, false};

	printf("Carrying out search for polylines starting/ending on axis.\n");
	printf("==========================================================\n");
	fflush(stdout);

	//	Calculate distance to all polyline originating from this edge
	vector<PolylineDistanceRecord> distanceToOriginatingPolylines;
	distanceToOriginatingPolylines.reserve(m_originatingPolylines.size());
	for (unsigned long i = 0; i < m_originatingPolylines.size(); ++i)
	{
		Polygon2D* currentPolyline = m_originatingPolylines[i];
		Real distanceToVertex =
				currentPolyline->GetOriginVertex().GetX() - value;

		//	Filter out vertices that are in the wrong direction of travel
		if (distanceToVertex <= 0.0)
		{
			distanceToOriginatingPolylines.push_back({currentPolyline,distanceToVertex});
		}
		else
		{
			printf("Discarded polygon beginning at X = %f.\n",
				   currentPolyline->GetOriginVertex().GetX());
			fflush(stdout);
		}
	}

	//	Calculate distance to all polyline terminating at this edge
	vector<PolylineDistanceRecord> distanceToTerminatingPolylines;
	distanceToTerminatingPolylines.reserve(m_terminatingPolylines.size());
	for (unsigned long i = 0; i < m_terminatingPolylines.size(); ++i)
	{
		Polygon2D* currentPolyline = m_terminatingPolylines[i];
		Real distanceToVertex =
				currentPolyline->GetTerminatingVertex().GetX() - value;

		//	Filter out vertices that are in the wrong direction of travel
		if (distanceToVertex <= 0.0)
		{
			distanceToTerminatingPolylines.push_back({currentPolyline,distanceToVertex});
		}
		else
		{
			printf("Discarded polygon beginning at X = %f.\n",
				   currentPolyline->GetOriginVertex().GetX());
			fflush(stdout);
		}
	}

	//	Sort both vectors into order
	//	NOTE: we may need to reverse the vectors if moving in the negative
	//	direction!
	std::sort(distanceToOriginatingPolylines.begin(), distanceToOriginatingPolylines.end(), sortingDistanceAscending);
	std::sort(distanceToTerminatingPolylines.begin(), distanceToTerminatingPolylines.end(), sortingDistanceAscending);
	std::reverse(distanceToOriginatingPolylines.begin(), distanceToOriginatingPolylines.end());
	std::reverse(distanceToTerminatingPolylines.begin(), distanceToTerminatingPolylines.end());

	printf("Nearest polygons from (X = %f) moving towards 0:\n", value);
	printf("Originating from this edge: \n");
	for (unsigned long i = 0; i < distanceToOriginatingPolylines.size(); ++i)
	{
		printf("X: %f\tDistance: %f\tPointer: %X\n", distanceToOriginatingPolylines[i].polyline->GetOriginVertex().GetX(), distanceToOriginatingPolylines[i].distance, distanceToOriginatingPolylines[i].polyline);
	}
	printf("Terminating at this edge: \n");
	for (unsigned long i = 0; i < distanceToTerminatingPolylines.size(); ++i)
	{
		printf("X: %f\tDistance: %f\tPointer: %X\n", distanceToTerminatingPolylines[i].polyline->GetTerminatingVertex().GetX(), distanceToTerminatingPolylines[i].distance, distanceToTerminatingPolylines[i].polyline);
	}
	fflush(stdout);

	printf("End.\n");
	printf("==========================================================\n");
	fflush(stdout);

	//	We may return no matches in the direction of travel
	if (distanceToOriginatingPolylines.empty() && distanceToTerminatingPolylines.empty())
		return {nullptr, false};

	//	No polylines terminate here in the direction of travel but some
	//	originate, so take the smallest of these as the result
	if (distanceToTerminatingPolylines.empty())
	{
		return {distanceToOriginatingPolylines.front().polyline, false};
	}

	//	Alternatively, no polylines originate here in the direction of travel
	//	but some terminate, so take the smallest of these as the result
	if (distanceToOriginatingPolylines.empty())
	{
		return {distanceToTerminatingPolylines.front().polyline, true};
	}

	//	Worse case scenario, is that there is some of both...so look for the
	//	nearest from either list
	if (fabs(distanceToOriginatingPolylines.front().distance) < fabs(distanceToTerminatingPolylines.front().distance))
	{
		return {distanceToOriginatingPolylines.front().polyline, false};
	}
	else
	{
		return {distanceToTerminatingPolylines.front().polyline, true};
	}
}

///
/// \brief		Given a point on the real number line, looks for the nearest
///				polyline that originates or terminates in the range:
///				[0..value)
/// \param value
/// \return
/// \since		05-06-2015
/// \author		Dean
///
PolygonLookup PolylineList2::findNearestYLessThan(const Real &value)
{
	//	Easy case... both lists are empty
	if (m_originatingPolylines.empty() && m_terminatingPolylines.empty())
		return {nullptr, false};

	printf("Carrying out search for polylines starting/ending on axis.\n");
	printf("==========================================================\n");
	fflush(stdout);

	//	Calculate distance to all polyline originating from this edge
	vector<PolylineDistanceRecord> distanceToOriginatingPolylines;
	distanceToOriginatingPolylines.reserve(m_originatingPolylines.size());
	for (unsigned long i = 0; i < m_originatingPolylines.size(); ++i)
	{
		Polygon2D* currentPolyline = m_originatingPolylines[i];
		Real distanceToVertex =
				currentPolyline->GetOriginVertex().GetY() - value;

		//	Filter out vertices that are in the wrong direction of travel
		if (distanceToVertex <= 0.0)
		{
			distanceToOriginatingPolylines.push_back({currentPolyline,distanceToVertex});
		}
		else
		{
			printf("Discarded polygon beginning at Y = %f.\n",
				   currentPolyline->GetOriginVertex().GetY());
			fflush(stdout);
		}
	}

	//	Calculate distance to all polyline terminating at this edge
	vector<PolylineDistanceRecord> distanceToTerminatingPolylines;
	distanceToTerminatingPolylines.reserve(m_terminatingPolylines.size());
	for (unsigned long i = 0; i < m_terminatingPolylines.size(); ++i)
	{
		Polygon2D* currentPolyline = m_terminatingPolylines[i];
		Real distanceToVertex =
				currentPolyline->GetTerminatingVertex().GetY() - value;

		//	Filter out vertices that are in the wrong direction of travel
		if (distanceToVertex <= 0.0)
		{
			distanceToTerminatingPolylines.push_back({currentPolyline,distanceToVertex});
		}
		else
		{
			printf("Discarded polygon beginning at Y = %f.\n",
				   currentPolyline->GetOriginVertex().GetY());
			fflush(stdout);
		}
	}

	//	Sort both vectors into order
	//	NOTE: we may need to reverse the vectors if moving in the negative
	//	direction!
	std::sort(distanceToOriginatingPolylines.begin(), distanceToOriginatingPolylines.end(), sortingDistanceAscending);
	std::sort(distanceToTerminatingPolylines.begin(), distanceToTerminatingPolylines.end(), sortingDistanceAscending);
	std::reverse(distanceToOriginatingPolylines.begin(), distanceToOriginatingPolylines.end());
	std::reverse(distanceToTerminatingPolylines.begin(), distanceToTerminatingPolylines.end());

	printf("Nearest polygons from (Y = %f) moving towards 0:\n", value);
	printf("Originating from this edge: \n");
	for (unsigned long i = 0; i < distanceToOriginatingPolylines.size(); ++i)
	{
		printf("Y: %f\tDistance: %f\tPointer: %X\n", distanceToOriginatingPolylines[i].polyline->GetOriginVertex().GetY(), distanceToOriginatingPolylines[i].distance, distanceToOriginatingPolylines[i].polyline);
	}
	printf("Terminating at this edge: \n");
	for (unsigned long i = 0; i < distanceToTerminatingPolylines.size(); ++i)
	{
		printf("Y: %f\tDistance: %f\tPointer: %X\n", distanceToTerminatingPolylines[i].polyline->GetTerminatingVertex().GetY(), distanceToTerminatingPolylines[i].distance, distanceToTerminatingPolylines[i].polyline);
	}
	fflush(stdout);

	printf("End.\n");
	printf("==========================================================\n");
	fflush(stdout);

	//	We may return no matches in the direction of travel
	if (distanceToOriginatingPolylines.empty() && distanceToTerminatingPolylines.empty())
		return {nullptr, false};

	//	No polylines terminate here in the direction of travel but some
	//	originate, so take the smallest of these as the result
	if (distanceToTerminatingPolylines.empty())
	{
		return {distanceToOriginatingPolylines.front().polyline, false};
	}

	//	Alternatively, no polylines originate here in the direction of travel
	//	but some terminate, so take the smallest of these as the result
	if (distanceToOriginatingPolylines.empty())
	{
		return {distanceToTerminatingPolylines.front().polyline, true};
	}

	//	Worse case scenario, is that there is some of both...so look for the
	//	nearest from either list
	if (fabs(distanceToOriginatingPolylines.front().distance) < fabs(distanceToTerminatingPolylines.front().distance))
	{
		return {distanceToOriginatingPolylines.front().polyline, false};
	}
	else
	{
		return {distanceToTerminatingPolylines.front().polyline, true};
	}
}

///
/// \brief		Given a point on the real number line, looks for the nearest
///				polyline that originates or terminates in the range:
///				[value..infinity)
/// \param value
/// \return
/// \since		05-06-2015
/// \author		Dean
///
PolygonLookup PolylineList2::findNearestYGreaterThan(const Real &value)
{
	//	Easy case... both lists are empty
	if (m_originatingPolylines.empty() && m_terminatingPolylines.empty())
		return {nullptr, false};

	printf("Carrying out search for polylines starting/ending on axis.\n");
	printf("==========================================================\n");
	fflush(stdout);

	//	Calculate distance to all polyline originating from this edge
	vector<PolylineDistanceRecord> distanceToOriginatingPolylines;
	distanceToOriginatingPolylines.reserve(m_originatingPolylines.size());
	for (unsigned long i = 0; i < m_originatingPolylines.size(); ++i)
	{
		Polygon2D* currentPolyline = m_originatingPolylines[i];
		Real distanceToVertex =
				currentPolyline->GetOriginVertex().GetY() - value;

		//	Filter out vertices that are in the wrong direction of travel
		if (distanceToVertex >= 0.0)
		{
			distanceToOriginatingPolylines.push_back({currentPolyline,distanceToVertex});
		}
		else
		{
			printf("Discarded polygon beginning at Y = %f.\n",
				   currentPolyline->GetOriginVertex().GetY());
			fflush(stdout);
		}
	}

	//	Calculate distance to all polyline terminating at this edge
	vector<PolylineDistanceRecord> distanceToTerminatingPolylines;
	distanceToTerminatingPolylines.reserve(m_terminatingPolylines.size());
	for (unsigned long i = 0; i < m_terminatingPolylines.size(); ++i)
	{
		Polygon2D* currentPolyline = m_terminatingPolylines[i];
		Real distanceToVertex =
				currentPolyline->GetTerminatingVertex().GetY() - value;

		//	Filter out vertices that are in the wrong direction of travel
		if (distanceToVertex >= 0.0)
		{
			distanceToTerminatingPolylines.push_back({currentPolyline,distanceToVertex});
		}
		else
		{
			printf("Discarded polygon terminating at Y = %f.\n",
				   currentPolyline->GetTerminatingVertex().GetY());
			fflush(stdout);
		}
	}

	//	Sort both vectors into order
	//	NOTE: we may need to reverse the vectors if moving in the negative
	//	direction!
	std::sort(distanceToOriginatingPolylines.begin(), distanceToOriginatingPolylines.end(), sortingDistanceAscending);
	std::sort(distanceToTerminatingPolylines.begin(), distanceToTerminatingPolylines.end(), sortingDistanceAscending);

	printf("Nearest polygons from (Y = %f) moving towards infinity:\n", value);
	printf("Originating from this edge: \n");
	for (unsigned long i = 0; i < distanceToOriginatingPolylines.size(); ++i)
	{
		printf("Y: %f\tDistance: %f\tPointer: %X\n", distanceToOriginatingPolylines[i].polyline->GetOriginVertex().GetY(), distanceToOriginatingPolylines[i].distance, distanceToOriginatingPolylines[i].polyline);
	}
	printf("Terminating at this edge: \n");
	for (unsigned long i = 0; i < distanceToTerminatingPolylines.size(); ++i)
	{
		printf("Y: %f\tDistance: %f\tPointer: %X\n", distanceToTerminatingPolylines[i].polyline->GetTerminatingVertex().GetY(), distanceToTerminatingPolylines[i].distance, distanceToTerminatingPolylines[i].polyline);
	}
	fflush(stdout);

	printf("End.\n");
	printf("==========================================================\n");
	fflush(stdout);

	//	We may return no matches in the direction of travel
	if (distanceToOriginatingPolylines.empty() && distanceToTerminatingPolylines.empty())
		return {nullptr, false};

	//	No polylines terminate here in the direction of travel but some
	//	originate, so take the smallest of these as the result
	if (distanceToTerminatingPolylines.empty())
	{
		return {distanceToOriginatingPolylines.front().polyline, false};
	}

	//	Alternatively, no polylines originate here in the direction of travel
	//	but some terminate, so take the smallest of these as the result
	if (distanceToOriginatingPolylines.empty())
	{
		return {distanceToTerminatingPolylines.front().polyline, true};
	}

	//	Worse case scenario, is that there is some of both...so look for the
	//	nearest from either list
	if (fabs(distanceToOriginatingPolylines.front().distance) < fabs(distanceToTerminatingPolylines.front().distance))
	{
		return {distanceToOriginatingPolylines.front().polyline, false};
	}
	else
	{
		return {distanceToTerminatingPolylines.front().polyline, true};
	}
}

PolylineList2::PolylineList2()
{

}

PolylineList2::~PolylineList2()
{

}

