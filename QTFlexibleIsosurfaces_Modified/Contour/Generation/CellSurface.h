#ifndef CELLSURFACE_H
#define CELLSURFACE_H

//	a little class defining a single surface in a cell
class CellSurface
{
	public:
		unsigned long x, y, z, entryFaceEdge;

		CellSurface(long X, long Y, long Z, long EntryFaceEdge)
		{
			x = X;
			y = Y;
			z = Z;
			entryFaceEdge = EntryFaceEdge;
		}
};

#endif // CELLSURFACE_H
