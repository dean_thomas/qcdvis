#ifndef POLYLINELIST2_H
#define POLYLINELIST2_H

#include <vector>
#include "Globals/TypeDefines.h"

using std::vector;

struct PolylineDistanceRecord
{
		Polygon2D* polyline;
		Real distance;
};


struct PolygonLookup
{
		Polygon2D* polygon;
		bool needsToBeReversed;
};


class PolylineList2
{


	private:
		vector<Polygon2D*> m_originatingPolylines;
		vector<Polygon2D*> m_terminatingPolylines;
	public:
		void AddPolylineToOriginList(Polygon2D* polyline);
		void AddPolylineToTerminatingList(Polygon2D* polyline);
		void RemovePolyline(Polygon2D* polyline);

		PolygonLookup findNearestXLessThan(const Real &value);
		PolygonLookup findNearestXGreaterThan(const Real &value);

		PolygonLookup findNearestYLessThan(const Real &value);
		PolygonLookup findNearestYGreaterThan(const Real &value);

		bool HasOriginatingPolylines() const { return m_originatingPolylines.empty(); }
		bool HasTerminatingPolylines() const { return m_terminatingPolylines.empty(); }
		bool IsEmpty() {
			return m_originatingPolylines.empty() && m_terminatingPolylines.empty(); }

		PolylineList2();
		~PolylineList2();
};

#endif // POLYLINELIST2_H
