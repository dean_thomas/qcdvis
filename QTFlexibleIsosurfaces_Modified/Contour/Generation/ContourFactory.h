#ifndef CONTOUR_FACTORY_H
#define CONTOUR_FACTORY_H

#include "Globals/Globals.h"
#include <Vector3.h>
#include "Geometry/Geometry.h"
#include "Contour/Generation/FollowCubeTables.h"
#include "HeightField/NeighbourQueue/NeighbourFunctor6.h"
#include "HeightField/NeighbourQueue/NeighbourFunctor6P.h"
#include "HeightField/NeighbourQueue/NeighbourFunctor18.h"
#include "HeightField/NeighbourQueue/NeighbourFunctor18P.h"

class HeightField3D;
class ContourTreeSuperarc;
class Contour;
class Contour;
class MeshTriangle3D;
class MeshVertex3D;

#define VERBOSE_OUTPUT

struct PackedVertex
{
		Point3D position;
		Vector3D normal;
};

class ContourFactory
{
	public:
		using Array3ul = Storage::Vector3<unsigned long>;
		using Index3d = Storage::Index3;

		//	HACK:	should be private! [27-05-2015]
		void AddTriangle(Contour* contourMesh,
						 PackedVertex pvA, PackedVertex pvB, PackedVertex pvC,
						 const bool recalculateStatistics = true) const;
	private:
		HeightField3D *m_heightfield = nullptr;
		ContourTreeSuperarc *m_superArc;

		void visit(Array3ul &visitFlags,
				   long xc, long yc, long zc, long cubeContour)const;						//	sets the flag for a particular contour in a cell
		void unvisit(Array3ul& visitFlags,
					 long xc, long yc, long zc, long cubeContour)const;					//	resets the flag for a contour
		bool visited(Array3ul &visitFlags,
					 long xc, long yc, long zc, long cubeContour) const;					//	tests whether flag set for contour

		Contour *followHierarchicalPathSeed(const Real ht) const;
		Contour *followContour(Array3ul &visitFlags, const Real ht) const;
		Contour *followSurface(Array3ul &visitFlags,
							   const Real functionHeight, Real *p1,
							   Real *p2) const;
		Contour *intersectSurface(Array3ul &visitFlags,
								  Real ht,
								  unsigned long x,
								  unsigned long y,
								  unsigned long z,
								  int theEntryFaceEdge) const;
		void unflagSurface(Array3ul &visitFlags,
						   Real ht,
						   long x,
						   long y,
						   long z,
						   int theEntryFaceEdge) const;				//	resets flags, starting at specified edge
		PackedVertex calculateVertex(const Point3ul coordinates,
										   int edge,
										   Real *cubeVert,
										   Real ht) const;
		Point3D interpolateVertexPosition(const Point3ul coordinates,
										  const int edge,
										  const Real *cubeVert,
										  const Real ht) const;
		Point3D interpolatePoint(const Point3D vertex1pos,
								 const Real vertex1height,
								 const Point3D vertex2pos,
								 const Real vertex2height,
								 const Real ht) const;
		Point3D calculateNormalVector(const Point3ul position0,
									  const Real height0,
									  const Point3ul position1,
									  const Real height1,
									  const Real ht) const;
		Point3D centralDifferenceNormal(const Point3ul coordinates) const;

		Real roundDP(const Real &rhs, const unsigned long &dp) const;
	public:

		//	these flags are principally used for renderin
		//	1-D array for flags used in contour-following rendering
		//Array3D<unsigned long> visitFlags;


		ContourFactory(ContourTreeSuperarc *superarc,
					   HeightField3D *heightfield);
		~ContourFactory();

		Contour *GenerateContour(const Real isovalue) const;

};

#endif // CONTOUR_FACTORY
