#include "Contour/Generation/ContourFactory.h"
#include "Contour/Storage/Contour.h"
#include <HeightField/HeightField3D.h>
#include <HeightField/HeightSort.h>
#include "Contour/Generation/FollowCubeTables.h"
#include "CellSurface.h"
#include "Graphs/ContourTreeSuperarc.h"
#include "Graphs/ContourTreeSupernode.h"

using namespace std;

#define DISABLE_DEBUG_OUTPUT

///
/// \brief TriangleList::AddTriangle
/// \param triangle
/// \since      04-12-2014
/// \author     Dean
///
void ContourFactory::AddTriangle(Contour* contourMesh,
								 PackedVertex pvA,PackedVertex pvB,PackedVertex pvC,
								 const bool recalculateStatistics) const
{
//#define SUPRESS_OUTPUT

	//	Find the normals
	Vector3D *nA = contourMesh->lookupNormal(pvA.normal);
	Vector3D *nB = contourMesh->lookupNormal(pvB.normal);
	Vector3D *nC = contourMesh->lookupNormal(pvC.normal);

	assert(nA != nullptr);
	assert(nB != nullptr);
	assert(nC != nullptr);

	//	Find the vertices
	MeshVertex3D *vA = contourMesh->lookupVertex(pvA.position);
	MeshVertex3D *vB = contourMesh->lookupVertex(pvB.position);
	MeshVertex3D *vC = contourMesh->lookupVertex(pvC.position);

	assert(vA != nullptr);
	assert(vB != nullptr);
	assert(vC != nullptr);

	//	Find the edges
	MeshEdge3D *eAB = contourMesh->lookupEdge(vA, vB);
	MeshEdge3D *eBC = contourMesh->lookupEdge(vB, vC);
	MeshEdge3D *eAC = contourMesh->lookupEdge(vA, vC);

	assert(eAB != nullptr);
	assert(eBC != nullptr);
	assert(eAC != nullptr);

	//	Add the triangle to the list
	MeshTriangle3D *newTriangle = new MeshTriangle3D(vA, vB, vC,
													 nA, nB, nC,
													 eAB, eBC, eAC);

	assert(newTriangle != nullptr);

	contourMesh->m_triangles.push_back(newTriangle);

	//	Tell the vertices they are part of the triangle
	vA->triangles.push_back(newTriangle);
	vB->triangles.push_back(newTriangle);
	vC->triangles.push_back(newTriangle);

	//	Tell the edges they are part of the triangle
	eAB->triangles.push_back(newTriangle);
	eBC->triangles.push_back(newTriangle);
	eAC->triangles.push_back(newTriangle);

	//	Tell the vertices they are part of the edges (could move to edge
	//	lookup?)
	vA->edges.push_back(eAB);
	vA->edges.push_back(eAC);
	vB->edges.push_back(eAB);
	vB->edges.push_back(eBC);
	vC->edges.push_back(eAC);
	vC->edges.push_back(eBC);

	contourMesh->calculateBoundaries();

#ifndef DISABLE_DEBUG_OUTPUT
	cout << "Added triangle to contour with arcID: " << contourMesh->GetSuperarc()->GetId()
		  << "  Unique triangle count is now: " << contourMesh->m_triangles.size()
		  << "  Unique vertex count is now: " << contourMesh->m_vertices.size()
		   <<"  Unique edge count is now: " << contourMesh->m_edges.size()
		   <<"  Euler characteristic: " << contourMesh->m_vertices.size() - contourMesh->m_edges.size() + contourMesh->m_triangles.size()
		  << endl;
#endif

#undef SUPRESS_OUTPUT
}

///
/// \brief      linear interpolation along an edge
/// \param      vertex1pos
/// \param      vertex1height
/// \param      vertex2pos
/// \param      vertex2height
/// \param      ht
/// \return     Point3D: The midpoint of the two parameters
/// \since      04-12-2014
/// \author     Hamish
/// \author     Dean: Refactor to use Geometric structures [08-12-2014]
/// \author		Dean: move to IsosurfaceGenerator class [16-04-2015]
/// \author		Dean: round values to help avoid fp errors [26-05-2015]
///
Point3D ContourFactory::interpolatePoint(const Point3D vertex1pos,
										 const Real vertex1height,
										 const Point3D vertex2pos,
										 const Real vertex2height,
										 const Real ht) const
{
	Point3D result;

	Real delta;

	//	parameters of interpolation
	if (vertex1height == vertex2height)
	{
		delta = 0.0;
	}
	else
	{
		//	compute parameter from p1 to p2
		delta = (vertex1height - ht) / (vertex1height - vertex2height);
	}

	//	and inverse parameter
	Real oneMinusDelta = 1.0 - delta;

	//	compute resultant vertex
	Real x = (oneMinusDelta * vertex1pos.GetX()) + (delta * vertex2pos.GetX());
	Real y = (oneMinusDelta * vertex1pos.GetY()) + (delta * vertex2pos.GetY());
	Real z = (oneMinusDelta * vertex1pos.GetZ()) + (delta * vertex2pos.GetZ());

	//	Round the values to avoid potential floating point errors later on
	result.SetX(roundDP(x, WORKING_PRECISION));
	result.SetY(roundDP(y, WORKING_PRECISION));
	result.SetZ(roundDP(z, WORKING_PRECISION));

	return result;
}

///
/// \brief		Rounds the supplied number to a specified number of dp
/// \param		rhs: the float to be rounded
/// \param		dp: the number of decimal places
/// \return		the number rounded to the nearest value
/// \since		26-05-2015
/// \author		Dean
///
Real ContourFactory::roundDP(const Real &rhs, const unsigned long &dp) const
{
	//	Factor to multply and divide by
	double factor = pow(10.0, (double)dp);

	return (round(rhs * factor) / factor);
}

///
/// \brief HeightField::InterpolateVertexPosition
/// \param coordinates
/// \param edge
/// \param cubeVert
/// \param ht
/// \return
/// \since		08-12-2014
/// \author		Hamish: original code
/// \author		Dean: update to use Point classes [08-12-2014]
/// \author		Dean: move to IsosurfaceGenerator class [16-04-2015]
///
Point3D ContourFactory::interpolateVertexPosition(const Point3ul coordinates,
												  const int edge,
												  const Real *cubeVert,
												  const Real ht) const
{
	//  Find the indiices of v0 and v1
	int v0 = edgeVertices[edge][0];
	int v1 = edgeVertices[edge][1];

	//  Use lookups to calculate the positions of the two vertices
	Point3D vertex0(coordinates.GetX() + mcFollowVertexCoords[v0][0],
			coordinates.GetY() + mcFollowVertexCoords[v0][1],
			coordinates.GetZ() + mcFollowVertexCoords[v0][2]);

	Point3D vertex1(coordinates.GetX() + mcFollowVertexCoords[v1][0],
			coordinates.GetY() + mcFollowVertexCoords[v1][1],
			coordinates.GetZ() + mcFollowVertexCoords[v1][2]);

	//  Find the heights at the two vertices
	Real height0 = cubeVert[v0];
	Real height1 = cubeVert[v1];

	Point3D result = interpolatePoint(vertex0, height0, vertex1, height1, ht);

	//printf(" %s\n", result.ToString().c_str());

	return result;
}

///
/// \brief          HeightField::InterpolateVertex
/// \param x
/// \param y
/// \param z
/// \param edge
/// \param cubeVert
/// \param ht
/// \return
/// \since      04-12-2014
/// \author     Hamish: original code
/// \author     Dean: return vertex and normal as a tuple [04-12-2014]
/// \author     Dean: also return an unscaled vertex for stats [05-12-2014]
/// \author     Dean: remove rescale code and use Point classes [08-12-2014]
/// \author		Dean: move to IsosurfaceGenerator class [16-04-2015]
///
PackedVertex ContourFactory::calculateVertex(const Point3ul coordinates,
											 int edge,
											 Real *cubeVert,
											 Real ht)const
{
	int v0 = edgeVertices[edge][0];
	int v1 = edgeVertices[edge][1];

	int x0 = coordinates.GetX() + mcFollowVertexCoords[v0][0];
	int y0 = coordinates.GetY() + mcFollowVertexCoords[v0][1];
	int z0 = coordinates.GetZ() + mcFollowVertexCoords[v0][2];

	int x1 = coordinates.GetX() + mcFollowVertexCoords[v1][0];
	int y1 = coordinates.GetY() + mcFollowVertexCoords[v1][1];
	int z1 = coordinates.GetZ() + mcFollowVertexCoords[v1][2];

	//  Function heights
	Real height0 = cubeVert[v0];
	Real height1 = cubeVert[v1];

	//  Calculate the actual position  of the vertex in 3D
	Point3D unscaledVertex = interpolateVertexPosition(coordinates,edge,cubeVert,ht);
	Point3D norm0 = calculateNormalVector(Point3ul(x0,y0,z0), height0, Point3ul(x1,y1,z1), height1, ht);

	return {unscaledVertex, norm0};
}

///
/// \brief		sets the flag for a particular contour
/// \param xc
/// \param yc
/// \param zc
/// \param cubeContour
/// \since		23-02-2015
/// \author		Hamish: original code
/// \author		Dean: move from heightfield [23-02-2015]
/// \author		Dean: move to IsosurfaceGenerator class [16-04-2015]
///
void ContourFactory::visit(Array3ul& visitFlags,
						   long xc, long yc, long zc, long cubeContour) const
{
	//	do a shift to get the right bit
	visitFlags(xc, yc, zc) |= (1 << cubeContour);
}

///
/// \brief		resets the flag for a particular contour
/// \param xc
/// \param yc
/// \param zc
/// \param cubeContour
/// \since		23-02-2015
/// \author		Hamish: original code
/// \author		Dean: move from heightfield [23-02-2015]
/// \author		Dean: move to IsosurfaceGenerator class [16-04-2015]
///
void ContourFactory::unvisit(Array3ul &visitFlags,
							 long xc, long yc, long zc, long cubeContour) const
{
	//	do a shift to get the right bit
	visitFlags(xc, yc, zc) &= ~(1 << cubeContour);
}

///
/// \brief		tests whether flag set for particular contour
/// \param xc
/// \param yc
/// \param zc
/// \param cubeContour
/// \return
/// \since		23-02-2015
/// \author		Hamish: original code
/// \author		Dean: move from heightfield [23-02-2015]
/// \author		Dean: move to IsosurfaceGenerator class [16-04-2015]
///
bool ContourFactory::visited(Array3ul& visitFlags,
							 long xc, long yc, long zc, long cubeContour)const
{
	//	test the particular flag
	if (visitFlags(xc, yc, zc) & (1 << cubeContour))
		return true;
	else
		return false;
}

///
/// \brief		Encapsulates some of the code used in FlexibleIsosurfaces
///				to generate a contour at a given isovalue
/// \param		superarc: the superarc that will be used to generate
///				the contour data
/// \param		heightfield: the 3D data set that will be used to generate
///				the contour data
/// \since		16-04-2015
/// \author		Dean
///
ContourFactory::ContourFactory(ContourTreeSuperarc *superarc,
							   HeightField3D *heightfield)
{
	assert(superarc != nullptr);
	assert(heightfield != nullptr);

	m_superArc = superarc;
	m_heightfield = heightfield;
}

///
/// \brief IsosurfaceGenerator::~IsosurfaceGenerator
///	\since		16-04-2014
/// \author		Dean
///
ContourFactory::~ContourFactory()
{

}

///
/// \brief		Generates a Contour at the specified isovalue
/// \param		isovalue: the target isovalue (non-normalised)
/// \return		A new Contour object (or nullptr if out of range)
/// \since		16-04-2015
/// \author		Dean
///
Contour *ContourFactory::GenerateContour(const Real isovalue) const
{
	//	Check we are in range for the arc
	if (isovalue >= m_superArc->GetBottomSupernode()->GetHeight()
		&& isovalue < m_superArc->GetTopSupernode()->GetHeight())
	{
		//  Follow the path traced by the Isovalue stored in seedValue
		return followHierarchicalPathSeed(isovalue);
	}
	else if (isovalue == m_superArc->GetTopSupernode()->GetHeight())
	{
		//	If we calculate the mesh at the exact point where it disappears
		//	we will throw an exception.  In order to guard against this we
		//	can subtract a small amount from the extreme value.
		return followHierarchicalPathSeed(
					isovalue - std::numeric_limits<Real>::epsilon());
	}
	else
	{
		throw InvalidIsovalueException(m_superArc->GetId(),
									   isovalue,
									   m_superArc->GetBottomSupernode()->GetHeight(),
									   m_superArc->GetTopSupernode()->GetHeight(),
									   __func__, __FILE__, __LINE__);
	}
}

///
/// \brief		generate surface using hierarchical path seeds
/// \param sArc
/// \param ht
/// \return
/// \author		Hamish: original implementation
/// \author		Dean: add a return for the TriangleList making up the contour
///				[15-01-2015]
/// \author		Dean: clear out some unused code [16-02-2015]
/// \author		Dean: move to IsosurfaceGenerator class [16-04-2015]
///
Contour *ContourFactory::followHierarchicalPathSeed(const Real ht) const
{
	//Supernode topNode = m_heightfield->GetContourTree()->supernodeArray[superarc->m_topSuperNodeID];
	//Supernode bottomNode = m_heightfield->GetContourTree()->supernodeArray[
	//			superarc->m_bottomSuperNodeID];
	Storage::Vector3<unsigned long> visitFlags(m_heightfield->GetBaseDimX(),
											   m_heightfield->GetBaseDimY(),
											   m_heightfield->GetBaseDimZ());

	Contour* triangleList = followContour(visitFlags, ht);

	//	returns the contour for further processing etc
	return triangleList;
}

///
/// \brief      given a superarc spanning ht, draws the corresponding contour
/// \param      ht
/// \param      theArc
/// \return     unsigned long: the number of steps
/// \since      03-12-2014
/// \author     Hamish: original code
/// \author     Dean: return number of steps and triangles in tuple [04-12-2014]
/// \author		Dean: move to IsosurfaceGenerator class [16-04-2015]
///
Contour *ContourFactory::followContour(Array3ul& visitFlags, const Real ht) const
{
	Contour* triangles;

	//	neighbouring point
	Real *neighbour;

	//	ends of an edge that intersects contour
	Real *start, *end;

	//	remember to count the starting step
	long stepCount = 1;

	NeighbourQueue neighbourQueue;

	// 	printf("Isovalue: %8.5f\n", ht);


	if (m_superArc->seedFromLo != NULL)
	//if (false)
	{
		//	look for a seed at the bottom end
		//  =================================
		//	path starts at seedLoFrom
		//	then proceeds through seedLoTo
		start = m_superArc->seedFromLo;
		end = m_superArc->seedToLo;

		//	repeat until we straddle
		while (*end <= ht)
		{
			//	indices of point
			size_t vX, vY, vZ;

			stepCount++;

			//	move up "this end"
			start = end;

			//	retrieve the indices
			m_heightfield->ComputeIndex(start, vX, vY, vZ);

			//	queue up its neighbours (by join order)
			NeighbourFunctor6 neighbourFunctor(m_heightfield->GetBaseDimX(),
											   m_heightfield->GetBaseDimY(),
											   m_heightfield->GetBaseDimZ());

			neighbourQueue.QueueNeighbours({vX, vY, vZ}, neighbourFunctor);

			//	walk along queue
			for (int i = 0; i < neighbourQueue.GetNeighbourCount(); i++)
			{
				Index3d temp = neighbourQueue.GetNeighbourAt(i);

				neighbour = &(m_heightfield->GetHeightAt(temp.x,
														 temp.y,
														 temp.z));

				//	this *CAN* happen, when the seed is adjacent to the lo end
				if (neighbour == end)
					continue;

				//	if the neighbour is larger than the best so far
				//	grab it as the next in path
				if (compareHeight(neighbour, end) > 0)
					end = neighbour;
			}
		}
	}
	else if (m_superArc->seedFromHi != NULL)
	{
		//	look for a seed at the top end
		//  ==============================
		//	path starts at seedFromHi
		//	then proceeds through seedToHi
		start = m_superArc->seedFromHi;
		end = m_superArc->seedToHi;

		//	repeat until we straddle
		while (*end >= ht)
		{
			//	indices of point
			size_t vX, vY, vZ;

			stepCount++;

			//	move up "this end"
			start = end;

			//	retrieve the indices
			m_heightfield->ComputeIndex(start, vX, vY, vZ);

			//	queue up its neighbours in split order
			NeighbourFunctor18 mooreNeighbour(m_heightfield->GetBaseDimX(),
											  m_heightfield->GetBaseDimY(),
											  m_heightfield->GetBaseDimZ());

			neighbourQueue.QueueNeighbours({vX, vY, vZ}, mooreNeighbour);

			//	walk along queue
			for (int i = 0; i < neighbourQueue.GetNeighbourCount(); i++)
			{
				Index3d temp = neighbourQueue.GetNeighbourAt(i);

				neighbour = &(m_heightfield->GetHeightAt(temp.x,
														 temp.y,
														 temp.z));

				//	this *CAN* happen, when the seed is adjacent to the lo end
				if (neighbour == end)
					continue;

				//	if the neighbour is smaller than the best so far
				//	grab it as the next in path
				if (compareHeight(neighbour, end) < 0)
					end = neighbour;
			}
		}
	}
	else
	{
		// no seed
		printf("Major error at %s: %d: no seed available\n", __FILE__, __LINE__);
		throw 1;
	}

	//printf("Steps: %ld\n", nSteps);
	//fflush(stdout);

	//	so follow the surface (and get number of triangles)
	triangles = followSurface(visitFlags,ht, start, end);

	//  return the path length and triangle cont
	return  triangles;
}

///
/// \brief      routine to follow surface from seed edge
/// \param ht
/// \param p1
/// \param p2
/// \author     Hamish
/// \since      08-12-2014
/// \author		Dean: move to IsosurfaceGenerator class [16-04-2015]
///
Contour *ContourFactory::followSurface(Array3ul& visitFlags,
									   const Real functionHeight,
									   Real *p1,
									   Real *p2)const
{
	Contour *result;

	//	coordinates of the two points on the edge
	size_t x1, y1, z1, x2, y2, z2;

	unsigned long xCell, yCell, zCell;

	//	indices for the two vertices
	unsigned long i1, i2;

	//	used to figure out where to start
	long whichCubeEdge, theSurface, whichFaceEdge;

	//	first convert p1, p2 to (x1, y1, z1) & (x2, y2, z2)
	//	compute the indices
	m_heightfield->ComputeIndex(p1, x1, y1, z1);
	m_heightfield->ComputeIndex(p2, x2, y2, z2);

	//	find the common cell to which they both belong (make sure it's in bounds)

	//	take the minimum in each dimension
	unsigned long xMin = x1 < x2 ? x1 : x2;
	unsigned long yMin = y1 < y2 ? y1 : y2;
	unsigned long zMin = z1 < z2 ? z1 : z2;

	//	and work out which cell we are in
	xCell = xMin;
	yCell = yMin;
	zCell = zMin;

	//	make sure we stay inside a legal cell
	if (xCell == m_heightfield->GetBaseDimX() - 1)
		xCell--;

	if (yCell == m_heightfield->GetBaseDimY() - 1)
		yCell--;

	if (zCell == m_heightfield->GetBaseDimZ() - 1)
		zCell--;

	//	compute indices of vertices with respect to this cell
	i1 = ((x1 - xCell) << 2) + ((y1 - yCell) << 1) + (z1 - zCell);
	i2 = ((x2 - xCell) << 2) + ((y2 - yCell) << 1) + (z2 - zCell);

	//	compute indices
	//	first, go into the cell we have identified
	//	and find which edge we are on in the cell
	whichCubeEdge = vertex2edge[i1][i2];

	//	error-check for bad edge
	if (whichCubeEdge == -1)
	{
		printf("Major problem: %ld to %ld is not a valid seed edge\n", i1, i2);
		throw 1;
	}

	//      now figure out which case the cube uses
	//      do nothing if any index negative
	//      if ((xCell < 0) || (yCell < 0) || (zCell < 0)) return;
	if ((xCell > m_heightfield->GetBaseDimX()-2)
		|| (yCell > m_heightfield->GetBaseDimY()-2)
		|| (zCell > m_heightfield->GetBaseDimZ()-2))
	{
		//	ditto for last face worth of vertices
		//  Not sure if this may be more suited to
		//  return 0?
		throw 65535;
	}

	//	local array holding vertices of cube
	Real cubeVert[8];

	cubeVert[0] = m_heightfield->GetHeightAt(xCell, yCell, zCell);
	cubeVert[1] = m_heightfield->GetHeightAt(xCell, yCell, zCell+1);
	cubeVert[2] = m_heightfield->GetHeightAt(xCell, yCell+1, zCell);
	cubeVert[3] = m_heightfield->GetHeightAt(xCell, yCell+1, zCell+1);
	cubeVert[4] = m_heightfield->GetHeightAt(xCell+1, yCell, zCell);
	cubeVert[5] = m_heightfield->GetHeightAt(xCell+1, yCell, zCell+1);
	cubeVert[6] = m_heightfield->GetHeightAt(xCell+1, yCell+1, zCell);
	cubeVert[7] = m_heightfield->GetHeightAt(xCell+1, yCell+1, zCell+1);

	//	the marching cubes case
	long marcingCubesCase = 0;

	//	loop through corners, computing facet lookup
	for (long cellVertex = 0; cellVertex < 8; ++cellVertex)
	{
		//	if the corner is above desired height
		if (functionHeight < cubeVert[cellVertex])
		{
			//	set bit flag
			marcingCubesCase |= (1 << cellVertex);
		}
	}

	//	find the surface to which be belong
	theSurface = seedEdge2Surface[marcingCubesCase][whichCubeEdge];

	if (theSurface == -1)
	{
		//	error check for bad surface
		printf("Major problem: Edge %ld in case %ld is not a valid seed edge\n", whichCubeEdge, marcingCubesCase);
		throw 1;
	}

	//	take the first exit edge (arbitrarily)
	whichFaceEdge = surface2exitEdges[marcingCubesCase][theSurface][0];

	//	start the surface off with the given edge
	//	and clean out the flags
	result = intersectSurface(visitFlags,functionHeight, xCell, yCell, zCell, whichFaceEdge);
	unflagSurface(visitFlags,functionHeight, xCell, yCell, zCell, whichFaceEdge);

	return result;
}

///
/// \brief      starts drawing cube at specified edge
/// \param ht
/// \param x
/// \param y
/// \param z
/// \param theEntryFaceEdge
/// \return     TriangleList*: A pointer to a list of triangles making up the current
///             object
/// \since      04-12-2014
/// \author     Hamish: original code
/// \author     Dean: hacked to return the triangle list [04-12-2014]
///             Dean: return the list on the heap [05-12-2014]
/// \author		Dean: move to IsosurfaceGenerator class [16-04-2015]
///
Contour *ContourFactory::intersectSurface(Array3ul& visitFlags,
										  Real ht,
										  unsigned long x,
										  unsigned long y,
										  unsigned long z,
										  int theEntryFaceEdge)const
{
	Contour* contourMesh = new Contour(m_superArc, ht);

	//	Mark that the contour is being constructed
	contourMesh->SetIsUnderConstruction(true);

	long theCase = 0;																	//	the marching cubes case
	long theSurface;																	//	the surface the edge belongs to
	long whichExitFaceEdge;																//	index for exit edges
	long theExitFaceEdge;																//	the exit edge & entry in next cube
	Real cubeVert[8];																	//	local array holding vertices of cube

	long whichTri;																		//	index for loop through triangles

	//	queue of surfaces to be processed.
	queue<CellSurface> theQueue;

	//	cell surface being processed
	CellSurface theCellSurface = CellSurface(x, y, z, theEntryFaceEdge);

	//	push the initial cell surface on queue
	theQueue.push(theCellSurface);

	//	while the queue is non-empty
	while (!theQueue.empty())
	{
		//	grab the front item
		//	and pop to remove it
		theCellSurface = theQueue.front();
		theQueue.pop();


		unsigned long currentX = theCellSurface.x;
		unsigned long currentY = theCellSurface.y;
		unsigned long currentZ = theCellSurface.z;

		if (!((theCellSurface.x > m_heightfield->GetBaseDimX()-2)
			  || (theCellSurface.y > m_heightfield->GetBaseDimY()-2)
			  || (theCellSurface.z > m_heightfield->GetBaseDimZ()-2)))
		{

			//	copy the vertex heights
			cubeVert[0] = m_heightfield->GetHeightAt(currentX, currentY, currentZ);
			cubeVert[1] = m_heightfield->GetHeightAt(currentX, currentY, theCellSurface.z+1);
			cubeVert[2] = m_heightfield->GetHeightAt(currentX, theCellSurface.y+1, currentZ);
			cubeVert[3] = m_heightfield->GetHeightAt(currentX, theCellSurface.y+1, theCellSurface.z+1);
			cubeVert[4] = m_heightfield->GetHeightAt(theCellSurface.x+1, currentY, currentZ);
			cubeVert[5] = m_heightfield->GetHeightAt(theCellSurface.x+1, currentY, theCellSurface.z+1);
			cubeVert[6] = m_heightfield->GetHeightAt(theCellSurface.x+1, theCellSurface.y+1, currentZ);
			cubeVert[7] = m_heightfield->GetHeightAt(theCellSurface.x+1, theCellSurface.y+1, theCellSurface.z+1);

			theCase = 0;

			//	loop through corners, computing facet lookup
			for (unsigned long whichVertex = 0; whichVertex < 8; whichVertex++)
			{
				//	if the corner is above desired height
				if (ht < cubeVert[whichVertex])
				{
					//	set bit flag
					theCase |= (1 << whichVertex);
				}
			}

			//	find the surface we are on
			theSurface = entryEdge2Surface[theCase][theCellSurface.entryFaceEdge];

			//	if it's been flagged, skip it
			//	never revisit the same surface
			if (visited(visitFlags,
						theCellSurface.x, theCellSurface.y, theCellSurface.z,
						theSurface))//	Lookup for cells visited in flexible contour construction
				continue;

			visit(visitFlags,
				  theCellSurface.x, theCellSurface.y, theCellSurface.z,
				  theSurface);					//	mark the surface as "visited"

			// printf("i: %2d j: %2d k: %2d. Case: %3d Base Case: %3s.\n",
			//	theCellSurface.x, theCellSurface.y, theCellSurface.z, theCase, caseName[baseCase[theCase]]);
			//printf("%d %d %d %d\n", x, xDim, y, yDim);
			//		if ((theCellSurface.x < xDim/2) ) //&& (theCellSurface.y < yDim/2))
			//			{

			//	walk through the triangles of the surface
			for (whichTri = 0; whichTri < 3*nTriangles[theCase][theSurface]; whichTri+= 3)
			{
				Point3ul coordinates(theCellSurface.x, theCellSurface.y, theCellSurface.z);

				PackedVertex vA	= calculateVertex(coordinates, mcFollowTriangles[theCase][theSurface][whichTri + 0], cubeVert, ht);
				PackedVertex vB	= calculateVertex(coordinates, mcFollowTriangles[theCase][theSurface][whichTri + 1], cubeVert, ht);
				PackedVertex vC	= calculateVertex(coordinates, mcFollowTriangles[theCase][theSurface][whichTri + 2], cubeVert, ht);

				AddTriangle(contourMesh, vA, vB, vC, false);
				//++triangleCount;
			}
			//			}
			//	now follow the contour out each face
			for (whichExitFaceEdge = 0; whichExitFaceEdge < nExitEdges[theCase][theSurface]; whichExitFaceEdge++)
			{ // for each exit edge
				theExitFaceEdge = surface2exitEdges[theCase][theSurface][whichExitFaceEdge];				//	find the exit edge's ID

				theQueue.push(CellSurface(
								  theCellSurface.x + exitDirection[theExitFaceEdge][0], 								//	first entry in row gives delta x
							  theCellSurface.y + exitDirection[theExitFaceEdge][1],								//	second entry gives delta y
						theCellSurface.z + exitDirection[theExitFaceEdge][2], 								//	third entry gives delta z
						exit2entryEdge[theExitFaceEdge]));												//	convert from exit to entry
			} // for each exit edge
		}
	} // loop to empty queue

	//	Mark that the contour has finished being constructed
	contourMesh->SetIsUnderConstruction(false);

	return contourMesh;
}

void ContourFactory::unflagSurface(Array3ul& visitFlags,
								   Real ht,
								   long x,
								   long y,
								   long z,
								   int theEntryFaceEdge) const				//	resets flags, starting at specified edge
{ // UnFlagSurface()
	long theCase = 0;																	//	the marching cubes case
	long theSurface;																	//	the surface the edge belongs to
	long whichExitFaceEdge;																//	index for exit edges
	long theExitFaceEdge;																//	the exit edge & entry in next cube
	Real cubeVert[8];																	//	local array holding vertices of cube
	long whichVertex;																	//	index for loop through vertices

	queue<CellSurface> theQueue;															//	queue of surfaces to be processed.
	CellSurface theCellSurface = CellSurface(x, y, z, theEntryFaceEdge);							//	cell surface being processed

	theQueue.push(theCellSurface);														//	push the initial cell surface on queue

	while (!theQueue.empty())															//	while the queue is non-empty
	{ // loop to empty queue
		theCellSurface = theQueue.front();													//	grab the front item
		theQueue.pop();																//	and pop to remove it
		//if ((theCellSurface.x < 0) || (theCellSurface.y < 0) || (theCellSurface.z < 0)) continue;		//	check for out-of bounds

		if ((theCellSurface.x > m_heightfield->GetBaseDimX()-2)
			|| (theCellSurface.y > m_heightfield->GetBaseDimY()-2)
			|| (theCellSurface.z > m_heightfield->GetBaseDimZ()-2))
			continue;

		cubeVert[0] = m_heightfield->GetHeightAt(theCellSurface.x, theCellSurface.y, theCellSurface.z);				//	copy the vertex heights
		cubeVert[1] = m_heightfield->GetHeightAt(theCellSurface.x, theCellSurface.y, theCellSurface.z+1);
		cubeVert[2] = m_heightfield->GetHeightAt(theCellSurface.x, theCellSurface.y+1, theCellSurface.z);
		cubeVert[3] = m_heightfield->GetHeightAt(theCellSurface.x, theCellSurface.y+1, theCellSurface.z+1);
		cubeVert[4] = m_heightfield->GetHeightAt(theCellSurface.x+1, theCellSurface.y, theCellSurface.z);
		cubeVert[5] = m_heightfield->GetHeightAt(theCellSurface.x+1, theCellSurface.y, theCellSurface.z+1);
		cubeVert[6] = m_heightfield->GetHeightAt(theCellSurface.x+1, theCellSurface.y+1, theCellSurface.z);
		cubeVert[7] = m_heightfield->GetHeightAt(theCellSurface.x+1, theCellSurface.y+1, theCellSurface.z+1);

		theCase = 0;
		for (whichVertex = 0; whichVertex < 8; whichVertex++)									//	loop through corners, computing facet lookup
			if (ht < cubeVert[whichVertex])												//	if the corner is above desired height
				theCase |= (1 << whichVertex);											//	set bit flag

		//	find the surface we are on
		theSurface = entryEdge2Surface[theCase][theCellSurface.entryFaceEdge];

		//	if it's been unflagged, skip it
		if (!visited(visitFlags,
					 theCellSurface.x, theCellSurface.y, theCellSurface.z,
					 theSurface))
			//	never revisit the same surface
			continue;

		//	mark the surface as "visited"
		unvisit(visitFlags,
				theCellSurface.x, theCellSurface.y, theCellSurface.z,
				theSurface);

		//	now follow the contour out each face
		for (whichExitFaceEdge = 0; whichExitFaceEdge < nExitEdges[theCase][theSurface]; whichExitFaceEdge++)
		{ // for each exit edge
			theExitFaceEdge = surface2exitEdges[theCase][theSurface][whichExitFaceEdge];				//	find the exit edge's ID

			theQueue.push(CellSurface(
							  theCellSurface.x + exitDirection[theExitFaceEdge][0], 								//	first entry in row gives delta x
						  theCellSurface.y + exitDirection[theExitFaceEdge][1],								//	second entry gives delta y
					theCellSurface.z + exitDirection[theExitFaceEdge][2], 								//	third entry gives delta z
					exit2entryEdge[theExitFaceEdge]));												//	convert from exit to entry
		} // for each exit edge
	} // loop to empty queue
} // UnFlagSurface()

///
/// \brief      Calculates the normal vector between 2 points
/// \param position0
/// \param height0
/// \param position1
/// \param height1
/// \return
/// \author     Dean
/// \since      09-12-2014
/// \author		Dean: move to IsosurfaceGenerator class [16-04-2015]
///
Point3D ContourFactory::calculateNormalVector(const Point3ul position0,
											  const Real height0,
											  const Point3ul position1,
											  const Real height1,
											  const Real ht) const
{
	//  Calculate the positions of the normal vectors
	Point3D normal0pos = centralDifferenceNormal(position0);
	Point3D normal1pos = centralDifferenceNormal(position1);

	//  Calculate the actual surface normal
	Point3D normalPos = interpolatePoint(normal0pos, height0, normal1pos, height1, ht);

	//	Normalize the normal vector
	Point3D normalizedNormal = normalPos.GetUnitVector();

	return normalizedNormal;
}

///
/// \brief HeightField::CentralDifferenceNormal
/// \param coordinates
/// \return
/// \since  08-12-2014
/// \author Hamish
/// \author Dean - adapt to use Point classes (08-12-2014)
/// \author		Dean: move to IsosurfaceGenerator class [16-04-2015]
///
Point3D ContourFactory::centralDifferenceNormal(const Point3ul coordinates) const
{
	Point3D result;

	try
	{
		//  Calculate X
		if (coordinates.GetX() == 0)
		{
			result.SetX(2 * (m_heightfield->GetHeightAt(coordinates.GetX() + 1, coordinates.GetY(), coordinates.GetZ())
							 - m_heightfield->GetHeightAt(coordinates.GetX(), coordinates.GetY(), coordinates.GetZ())));
		}
		else if (coordinates.GetX() == m_heightfield->GetBaseDimX() - 1)
		{
			result.SetX(2 * (m_heightfield->GetHeightAt(coordinates.GetX(), coordinates.GetY(), coordinates.GetZ())
							 - m_heightfield->GetHeightAt(coordinates.GetX() - 1, coordinates.GetY(), coordinates.GetZ())));
		}
		else
		{
			result.SetX(m_heightfield->GetHeightAt(coordinates.GetX() + 1, coordinates.GetY(), coordinates.GetZ())
						- m_heightfield->GetHeightAt(coordinates.GetX() - 1, coordinates.GetY(), coordinates.GetZ()));
		}

		//  Calculate Y
		if (coordinates.GetY() == 0)
		{
			result.SetY(2 * (m_heightfield->GetHeightAt(coordinates.GetX(), coordinates.GetY() + 1, coordinates.GetZ())
							 - m_heightfield->GetHeightAt(coordinates.GetX(), coordinates.GetY(), coordinates.GetZ())));
		}
		else if (coordinates.GetY() == m_heightfield->GetBaseDimY() - 1)
		{
			result.SetY(2 * (m_heightfield->GetHeightAt(coordinates.GetX(), coordinates.GetY(), coordinates.GetZ())
							 - m_heightfield->GetHeightAt(coordinates.GetX(), coordinates.GetY() - 1, coordinates.GetZ())));
		}
		else
		{
			result.SetY(m_heightfield->GetHeightAt(coordinates.GetX(), coordinates.GetY() + 1, coordinates.GetZ())
						- m_heightfield->GetHeightAt(coordinates.GetX(), coordinates.GetY() - 1, coordinates.GetZ()));
		}

		//  Calculate Z
		if (coordinates.GetZ() == 0)
		{
			result.SetZ(2 * (m_heightfield->GetHeightAt(coordinates.GetX(), coordinates.GetY(), coordinates.GetZ() + 1)
							 - m_heightfield->GetHeightAt(coordinates.GetX(), coordinates.GetY(), coordinates.GetZ())));
		}
		else if (coordinates.GetZ() == m_heightfield->GetBaseDimZ() - 1)
		{
			result.SetZ(2 * (m_heightfield->GetHeightAt(coordinates.GetX(), coordinates.GetY(), coordinates.GetZ())
							 - m_heightfield->GetHeightAt(coordinates.GetX(), coordinates.GetY(), coordinates.GetZ() - 1)));
		}
		else
		{
			result.SetZ(m_heightfield->GetHeightAt(coordinates.GetX(), coordinates.GetY(), coordinates.GetZ() + 1)
						- m_heightfield->GetHeightAt(coordinates.GetX(), coordinates.GetY(), coordinates.GetZ() - 1));
		}
	}
	catch (InvalidCoordinateException &ex)
	{
		printf("There was an error: %s in %s.\n", ex.what(), __func__);
		fflush(stdout);
	}

	return result;
}
