#include "PolygonFactory2.h"

using std::cerr;
using std::cout;
using std::endl;

///
/// \brief		Returns the list of completed polygons and clear internal
///				lists containing pointers to them
/// \return
/// \since		03-06-2015
/// \author		Dean
///
vector<Polygon2D*> PolygonFactory2::transferOwnershipOfPolygons()
{
	//	Copy the list for return
	vector<Polygon2D*> result = m_polygonStore;

	//	Clear out the internal list (but don't delete the polygons, essentially
	//	transfer ownership).
	m_polygonStore.clear();

	//	And return them
	return result;
}

///
/// \brief		Works through the unordered edges and tries to construct
///				polygons.  Those that are simple (not intersecting a boundary),
///				can be filtered and stored in the completed list.  Partly
///				completed polylines are returned for additional processing.
/// \since		04-06-2015
/// \author		Dean
///
vector<Polygon2D*> PolygonFactory2::storeSimplePolygons()
{
	//	Polylines that require further processing go here
	vector<Polygon2D*> result;

	//	Construct polygons from the filtered edges
	while (!m_filteredEdges.empty())
	{
		Polygon2D* temp = constructPolylines(m_filteredEdges);

		if (temp->IsClosed())
		{
			//	No further processing required
			m_polygonStore.push_back(temp);
		}
		else
		{
			//	Add to the list of partial completed polygons
			result.push_back(temp);
		}
	}

	//	We need to do more with these
	return result;
}

Polygon2D* PolygonFactory2::pickNextEdgeOnMinYBoundary(const Real &currentX,
													   const DirectionOfTravel &directionOfTravel)
{

	return nullptr;
}

///
/// \brief		Takes a set of 3D edges, filter out those not on the target
///				boundary and then flatten them to 2D for further processing
/// \param		unfilteredEdges: the of set potentially unordered 3D edges
/// \since		03-06-2015
/// \author		Dean
///
void PolygonFactory2::filterEdges(const vector<Edge3D> &unfilteredEdges)
{
	//	Incoming edges will be from a skew polygon in 3D space
	//	so we can filter out edges that aren't on our target face
	for (unsigned long i = 0; i < unfilteredEdges.size(); ++i)
	{
		Edge3D currentEdge = unfilteredEdges[i];

		switch (m_targetBoundary)
		{
			case HeightField3D::Boundary::MIN_X:
			case HeightField3D::Boundary::MAX_X:
				//	Check that the edge exists on the target boundary
				if (currentEdge.GetVertexA().EqualsX(m_filterValue) &&
					currentEdge.GetVertexB().EqualsX(m_filterValue))
				{
					//	Flatten to 2D by removing the X coordinate
					m_filteredEdges.push_back(currentEdge.YZ());
				}
				break;
			case HeightField3D::Boundary::MIN_Y:
			case HeightField3D::Boundary::MAX_Y:
				//	Check that the edge exists on the target boundary
				if (currentEdge.GetVertexA().EqualsY(m_filterValue) &&
					currentEdge.GetVertexB().EqualsY(m_filterValue))
				{
					//	Flatten to 2D by removing the Y coordinate
					m_filteredEdges.push_back(currentEdge.XZ());
				}
				break;
			case HeightField3D::Boundary::MIN_Z:
			case HeightField3D::Boundary::MAX_Z:
				//	Check that the edge exists on the target boundary
				if (currentEdge.GetVertexA().EqualsZ(m_filterValue) &&
					currentEdge.GetVertexB().EqualsZ(m_filterValue))
				{
					//	Flatten to 2D by removing the Z coordinate
					m_filteredEdges.push_back(currentEdge.XY());
				}
				break;
		}
	}
}

///
/// \brief		Interface function of the factory.  Should take a set of
///				unordered 3D edges and construct a set of 2D polygons to cap a
///				SINGLE face of a 3D cube.
/// \param		unfilteredEdges: the of set potentially unordered 3D edges
/// \since		03-06-2015
/// \author		Dean
///
vector<Polygon2D*> PolygonFactory2::ProcessEdges(const vector<Edge3D> &unfilteredEdges)
{
	//	Sort through the incoming 3D edges and only store those on the
	//	target plane
	filterEdges(unfilteredEdges);

	//	Turns the list of unordered edges into polylines.  Those that are
	//	closed are filtered out.
	vector<Polygon2D*> openPolylines = storeSimplePolygons();

	//	Sorts the uncompleted polylines by their start and end points
	sortPolylines(openPolylines);

	closePolygons();
	//growAndMergePolygons(m_polylineStore);

	//	Return the closed polygons
	return transferOwnershipOfPolygons();
}

///
/// \brief		Removes the specified polyline from all boundary lists -
///				effectively marking it as processed
/// \param		thePolygon
/// \since		04-06-2015
/// \author		Dean
///
void PolygonFactory2::removePolylineFromLists(Polygon2D* thePolyline)
{
	if (thePolyline != nullptr)
	{
		m_minX.RemovePolyline(thePolyline);
		m_minY.RemovePolyline(thePolyline);
		m_maxX.RemovePolyline(thePolyline);
		m_maxY.RemovePolyline(thePolyline);
	}
	else
	{
		cerr << "Error!  Asked to remove a null polyline.";
		cerr << "In function " << __func__ << ": file " << __FILE__;
		cerr << ", line " << __LINE__ << "." << endl;
	}
}

void PolygonFactory2::closePolygons()
{
	//	1.	Start on the minY boundary
	//while (!m_minY.IsEmpty())
	//{
	//	If the boolean value is true, we need to reverse the polyline
	//	before adding it

	//	The polygon being constructed
	Polygon2D* workingPolygon;

	//	Find a polygon to start growing
	PolygonLookup nearestPolygon = m_maxX.findNearestYGreaterThan(12.0);

	if (nearestPolygon.polygon != nullptr)
	{
		if (!nearestPolygon.needsToBeReversed)
		{
			cout << "Best polygon begins at: ";
			cout << nearestPolygon.polygon->GetOriginVertex().GetY();
			cout << "." << endl;
		}
		else
		{
			cout << "Best polygon terminates at: ";
			cout << nearestPolygon.polygon->GetTerminatingVertex().GetY();
			cout << "." << endl;
		}
	}

	/*
	//	There may not be one suitable
	if (nearestPolygon.polygon != nullptr)
	{
		removePolylineFromLists(nearestPolygon.polygon);
		if (!(nearestPolygon.needsToBeReversed))
		{
			//	Direct copy
			workingPolygon = nearestPolygon.polygon;
		}
		else
		{
			//	Add edges in reverse
			workingPolygon = new Polygon2D(*nearestPolygon.polygon, true);

			printf("Original polygon: %s.\n", nearestPolygon.polygon->ToString().c_str());
			printf("Reversed polygon: %s.\n", workingPolygon->ToString().c_str());
			fflush(stdout);
		}

		printf("Nearest polyline: %s.\n", nearestPolygon.polygon->ToString().c_str());
		fflush(stdout);
	}
	*/

	//}

	//	2.	Next work on on the maxX boundary

	//	3.	Next work on on the maxY boundary

	//	4.	Finally work on on the minX boundary
}

///
/// \brief		Used to sort vectors of polylines by the origin/terminal vertices
/// \since		04-06-2015
/// \author		Dean
///
bool sortFuncAscendingOriginX(Polygon2D* lhs, Polygon2D* rhs)
{
	//	Sort by x-coordinate of source vertex in ascending order
	return (lhs->GetOriginVertex().GetX() < rhs->GetOriginVertex().GetX());
}

///
/// \brief		Used to sort vectors of polylines by the origin/terminal vertices
/// \since		04-06-2015
/// \author		Dean
///
bool sortFuncAscendingTerminusX(Polygon2D* lhs, Polygon2D* rhs)
{
	//	Sort by y-coordinate of source vertex in ascending order
	return (lhs->GetTerminatingVertex().GetX() < rhs->GetTerminatingVertex().GetX());
}

///
/// \brief		Used to sort vectors of polylines by the origin/terminal vertices
/// \since		04-06-2015
/// \author		Dean
///
bool sortFuncAscendingOriginY(Polygon2D* lhs, Polygon2D* rhs)
{
	//	Sort by y-coordinate of source vertex in ascending order
	return (lhs->GetOriginVertex().GetY() < rhs->GetOriginVertex().GetY());
}

///
/// \brief		Used to sort vectors of polylines by the origin/terminal vertices
/// \since		04-06-2015
/// \author		Dean
///
bool sortFuncAscendingTerminusY(Polygon2D* lhs, Polygon2D* rhs)
{
	//	Sort by y-coordinate of source vertex in ascending order
	return (lhs->GetTerminatingVertex().GetY() < rhs->GetTerminatingVertex().GetY());
}

///
/// \brief		Used to sort vectors of polylines by the origin/terminal vertices
/// \since		04-06-2015
/// \author		Dean
///
bool sortFuncDecendingOriginX(Polygon2D* lhs, Polygon2D* rhs)
{
	//	Sort by x-coordinate of source vertex in ascending order
	return (lhs->GetOriginVertex().GetX() > rhs->GetOriginVertex().GetX());
}

///
/// \brief		Used to sort vectors of polylines by the origin/terminal vertices
/// \since		04-06-2015
/// \author		Dean
///
bool sortFuncDecendingTerminusX(Polygon2D* lhs, Polygon2D* rhs)
{
	//	Sort by y-coordinate of source vertex in ascending order
	return (lhs->GetTerminatingVertex().GetX() > rhs->GetTerminatingVertex().GetX());
}

///
/// \brief		Used to sort vectors of polylines by the origin/terminal vertices
/// \since		04-06-2015
/// \author		Dean
///
bool sortFuncDecendingOriginY(Polygon2D* lhs, Polygon2D* rhs)
{
	//	Sort by y-coordinate of source vertex in ascending order
	return (lhs->GetOriginVertex().GetY() > rhs->GetOriginVertex().GetY());
}

///
/// \brief		Used to sort vectors of polylines by the origin/terminal vertices
/// \since		04-06-2015
/// \author		Dean
///
bool sortFuncDecendingTerminusY(Polygon2D* lhs, Polygon2D* rhs)
{
	//	Sort by y-coordinate of source vertex in ascending order
	return (lhs->GetTerminatingVertex().GetY() > rhs->GetTerminatingVertex().GetY());
}

///
/// \brief		Sorts all partially completed polylines into separate
///				vectors depending on their origin and terminal vertices.
///				Under normal circumstances each contour should be added
///				to one list of each type.  This condition *could* be invalidated
///				if a polyline start's or end in the corner of the plane.
/// \since		04-06-2015
/// \author		Dean
///
void PolygonFactory2::sortPolylines(vector<Polygon2D*> &unsortedPolylines)
{
	//	Nothing to do...
	if (unsortedPolylines.empty()) return;

	for (unsigned long i = 0; i < unsortedPolylines.size(); ++i)
	{
		unsigned long count = 0;

		Polygon2D* currentPolyline = unsortedPolylines[i];

		Point2D startPoint = currentPolyline->GetOriginVertex();
		Point2D endPoint = currentPolyline->GetTerminatingVertex();

		//	Lines originating from either of the the X boundaries
		if (startPoint.EqualsX(m_planeMinX))
		{
			m_minX.AddPolylineToOriginList(currentPolyline);
			++count;

			printf("Polyline %ld originates from the MinX boundary.\n", i);
			fflush(stdout);
		}
		else if (startPoint.EqualsX(m_planeMaxX))
		{
			m_maxX.AddPolylineToOriginList(currentPolyline);
			++count;

			printf("Polyline %ld originates from the MaxX boundary.\n", i);
			fflush(stdout);
		}

		//	Lines originating from either of the the Y boundaries
		if (startPoint.EqualsY(m_planeMinY))
		{
			m_minY.AddPolylineToOriginList(currentPolyline);
			++count;

			printf("Polyline %ld originates from the MinY boundary.\n", i);
			fflush(stdout);
		}
		else if (startPoint.EqualsY(m_planeMaxY))
		{
			m_maxY.AddPolylineToOriginList(currentPolyline);
			++count;

			printf("Polyline %ld originates from the MaxY boundary.\n", i);
			fflush(stdout);
		}

		//	Lines terminating at either of the the X boundaries
		if (endPoint.EqualsX(m_planeMinX))
		{
			m_minX.AddPolylineToTerminatingList(currentPolyline);
			++count;

			printf("Polyline %ld terminates at the MinX boundary.\n", i);
			fflush(stdout);
		}
		else if (endPoint.EqualsX(m_planeMaxX))
		{
			m_maxX.AddPolylineToTerminatingList(currentPolyline);
			++count;

			printf("Polyline %ld terminates at the MaxX boundary.\n", i);
			fflush(stdout);
		}

		//	Lines originating from either of the the Y boundaries
		if (endPoint.EqualsY(m_planeMinY))
		{
			m_minY.AddPolylineToTerminatingList(currentPolyline);
			++count;

			printf("Polyline %ld terminates at the MinY boundary.\n", i);
			fflush(stdout);
		}
		else if (endPoint.EqualsY(m_planeMaxY))
		{
			m_maxY.AddPolylineToTerminatingList(currentPolyline);
			++count;

			printf("Polyline %ld terminates at the MaxY boundary.\n", i);
			fflush(stdout);
		}

		if (count != 2)
		{
			//	Not sure if this could happen - so we will monitor for it
			//	manually for now
			printf("Polyline added to %ld lists.  Is this a corner vertex?\n",
				   count);
			fflush(stdout);
		}
	}

	printf("Finished sorting all Polylines for the current contour.\n");
	fflush(stdout);
}

PolygonDistancePair PolygonFactory2::findNearestPolygonToPoint(
		vector<PolygonConstruction*> &polygons,
		const Point2D &point)
{
	PolygonDistancePair result;

	for (unsigned long i = 0; i < polygons.size(); ++i)
	{
		PolygonConstruction* polygon = polygons[i];

		//	Only look for polygons marked as being 'in-progress'
		if (polygon->active)
		{
			Point2D polygonStart = polygon->polygon->GetFirstEdge().GetVertexA();
			Point2D polygonEnd = polygon->polygon->GetLastEdge().GetVertexB();

			//	Straight line distance to start and end of current test polygon
			Vector2D distA(point.GetX() - polygonStart.GetX(),
						   point.GetY() - polygonStart.GetY());
			Vector2D distB(point.GetX() - polygonEnd.GetX(),
						   point.GetY() - polygonEnd.GetY());

			Point2D nearestPoint;
			Real minDistance;

			if (distA.GetMagnitude() < distB.GetMagnitude())
			{
				minDistance = distA.GetMagnitude();
				nearestPoint = polygon->polygon->GetFirstEdge().GetVertexA();
			}
			else
			{
				minDistance = distB.GetMagnitude();
				nearestPoint = polygon->polygon->GetLastEdge().GetVertexB();
			}

			if (minDistance < result.distance)
			{
				if (!(minDistance == 0.0))
				{
					result.polygon = polygon;
					result.distance = minDistance;
					result.position = nearestPoint;
					result.indexOfPolygon = i;
				}
			}
		}
	}
	return result;
}

///
/// \brief		Calculates the next integer lattice value when moving from the
///				current point in space in the direction specified by delta
/// \param		currentPos: a floating point value representing a value on the
///				real number line.
/// \param		delta: an integer value representing the direction of movement
///				along the real number line (allowable values are 0 or ±1).
/// \return		The next integer value (as a double) on the real number line
///				when moving	in direction delta (or the input value in the case
///				of delta==0).
/// \since		02-06-2015
/// \author		Dean
///
double PolygonFactory2::getNextIntegerLatticePoint(const double &currentPos,
												   const long &delta) const
{
	//	Moving towards zero
	if (delta < 0)
	{
		return ceil(currentPos + delta);
	}
	//	Moving away from zero
	else if (delta > 0)
	{
		return floor(currentPos + delta);
	}
	//	No change
	else
	{
		return currentPos;
	}
}



///
/// \brief		Takes a group of edges and attemps to construct a closed
///				polygon with them
/// \param		unorderedEdges: a group of edges in 2D space
/// \return		a 2D polygon (not necessarily closed)
/// \since		28-05-2015
/// \author		Dean
///
Polygon2D *PolygonFactory2::constructPolylines(vector<Edge2D> &unorderedEdges) const
{
#define SUPRESS_OUTPUT

#define WASTED_LOOPS_MAX 2
	//	Convenience variables
	const Real X_MAX = m_heightfield->GetMaxX();
	const Real Y_MAX = m_heightfield->GetMaxY();
	const Real Z_MAX = m_heightfield->GetMaxZ();

	//	Create a new polygon
	Polygon2D *polygon = new Polygon2D();

	//	Count how many edges we start with
	unsigned long startCount = unorderedEdges.size();

	//	Take the first edge from the list and add to the polygon
	Edge2D firstEdge = unorderedEdges[0];
	unorderedEdges.erase(unorderedEdges.begin());
	polygon->AddEdgeToBack(firstEdge);

	//	Keep track of loops with no edges found
	unsigned long wastedLoops = 0;

	//	We keep looping until the polygon is completed, we run out of edges or
	//	nothing is cmopleted after several attempts
	while (!polygon->IsClosed()
		   && (unorderedEdges.size() > 0)
		   && (wastedLoops < WASTED_LOOPS_MAX))
	{
		//	Use as a safe guard - if no edges are found to add to the polygon
		//	we can increment a counter
		bool polygonHasUpdated = false;

		//	Add at the end of the current polygon
		//
		//	Check each edge in the list to see if it is incident on the last
		//	vertex added to the polygon
		for (unsigned long i = 0; i < unorderedEdges.size(); ++i)
		{

			bool currentEdgeWasAdded = false;
			Edge2D currentEdge = unorderedEdges[i];

			//	Edge is in the correct vertex order
			if (currentEdge.GetVertexA() ==
				polygon->GetLastEdge().GetVertexB())
			{
				//	Add the edge
				polygon->AddEdgeToBack(currentEdge);

				//	And remove from the list
				unorderedEdges.erase(unorderedEdges.begin()+i);

				//	Reset the wasted loop counter and mark that the currentEdge
				//	was used
				polygonHasUpdated = true;
				currentEdgeWasAdded = true;

				//	Drop out of for loop
				//break;
			}
			//	Edge is in the reversed vertex order
			else if (currentEdge.GetVertexB() ==
					 polygon->GetLastEdge().GetVertexB())
			{
				//	Add the edge but reverse first
				polygon->AddEdgeToBack(
							currentEdge.GetReversedForm());

				//	And remove from the list
				unorderedEdges.erase(unorderedEdges.begin()+i);

				//	Reset the wasted loop counter and mark that the currentEdge
				//	was used
				polygonHasUpdated = true;
				currentEdgeWasAdded = true;

				//	Drop out of for loop
				//break;
			}
			///	Add at the start of the current polygon
			///
			else if (currentEdge.GetVertexA() ==
					 polygon->GetFirstEdge().GetVertexA())
			{
				//	Add the edge
				polygon->AddEdgeToFront(
							currentEdge.GetReversedForm());

				//	And remove from the list
				unorderedEdges.erase(unorderedEdges.begin()+i);

				//	Reset the wasted loop counter and mark that the currentEdge
				//	was used
				polygonHasUpdated = true;
				currentEdgeWasAdded = true;

				//	Drop out of for loop
				//break;
			}
			else if (currentEdge.GetVertexB() ==
					 polygon->GetFirstEdge().GetVertexA())
			{
				//	Add the edge
				polygon->AddEdgeToFront(currentEdge);

				//	And remove from the list
				unorderedEdges.erase(unorderedEdges.begin()+i);

				//	Reset the wasted loop counter and mark that the currentEdge
				//	was used
				polygonHasUpdated = true;
				currentEdgeWasAdded = true;

				//	Drop out of for loop
				//break;
			}
		}

		if (!polygonHasUpdated)
		{
#if defined(VERBOSE_OUTPUT) && !defined(SUPRESS_OUTPUT)
			//	Increment the wasted loop counter as no matcing edge was found
			printf("Incremented wasted loops.\n");
			fflush(stdout);
#endif
			++wastedLoops;
		}
		else
		{
			wastedLoops = 0;
		}
	}

	unsigned long endCount = unorderedEdges.size();

	printf("Current polygon uses %ld edges.  Start count: %ld; End count: %ld\n",
		   startCount - endCount, startCount, endCount);
	fflush(stdout);

	return polygon;
#undef SUPRESS_OUTPUT
}

///
/// \brief		Attempts to grow and merge open contours in 3D in order to
///				close them
/// \param		polygonList
/// \since		28-05-2015
/// \author		Dean
///
void PolygonFactory2::growAndMergePolygons(const HeightField3D::Boundary &plane,
										   vector<Polygon2D*> &polygonList)
{
	//	Nothing to do...
	if (polygonList.empty()) return;

	//	Limits of the requested plane
	Real planeMinX;
	Real planeMaxX;
	Real planeMinY;
	Real planeMaxY;
	switch (plane)
	{
		case HeightField3D::Boundary::MIN_X:
		case HeightField3D::Boundary::MAX_X:
			//	YZ plane
			planeMinX = 0.0;
			planeMinY = 0.0;
			planeMaxX = m_heightfield->GetMaxY();
			planeMaxY = m_heightfield->GetMaxZ();
			break;
		case HeightField3D::Boundary::MIN_Y:
		case HeightField3D::Boundary::MAX_Y:
			//	XZ plane
			planeMinX = 0.0;
			planeMinY = 0.0;
			planeMaxX = m_heightfield->GetMaxX();
			planeMaxY = m_heightfield->GetMaxZ();
			break;
		case HeightField3D::Boundary::MIN_Z:
		case HeightField3D::Boundary::MAX_Z:
			//	XY plane
			planeMinX = 0.0;
			planeMinY = 0.0;
			planeMaxX = m_heightfield->GetMaxX();
			planeMaxY = m_heightfield->GetMaxY();
			break;
	}

	//	Create a list of polygons that require further processing
	vector<PolygonConstruction*> unclosedPolygons;
	unclosedPolygons.reserve(polygonList.size());

	//	Copy across open polygons
	for (unsigned long i = 0; i < polygonList.size(); ++i)
	{
		if (!polygonList[i]->IsClosed())
		{
			if (!polygonList[i]->GetEdges().empty())
			{
				//	Note the values used in the constructor refer to the 2D plane!
				unclosedPolygons.push_back(new PolygonConstruction(polygonList[i],
																   planeMinX, planeMaxX,
																   planeMinY, planeMaxY));
			}
		}
	}


	for (unsigned long i = 0; i < unclosedPolygons.size(); ++i)
	{
		switch (unclosedPolygons[i]->polygonType)
		{
			case PolygonConstruction::PolygonType::SPANS_SINGLE_EDGE:
				growAndMergePolygonOnEdge(unclosedPolygons[i], unclosedPolygons);
				break;
			case PolygonConstruction::PolygonType::SPANS_CORNER:
				growAndMergePolygonOnCorner(unclosedPolygons[i], unclosedPolygons);
				break;
			default:
				printf("Unknown polygon type for closing!\n");
				fflush(stdout);
				break;
		}
	}

	//	Tidy up any PolygonConstructions that haven't been deleted elsewhere
	while (!unclosedPolygons.empty())
	{
		delete unclosedPolygons.back();
		unclosedPolygons.pop_back();
	}
}

/*
inline float clamp(float x, float a, float b)
{
	return x < a ? a : (x > b ? b : x);

}
*/

///
/// \brief		Grows and merges a polygon by advancing along an edge into
///				the corner of the data
/// \param poly
/// \since		01-06-2015
/// \author		Dean
///
void PolygonFactory2::growAndMergePolygonOnCorner(PolygonConstruction *poly, vector<PolygonConstruction*> &polygons)
{
	while (!poly->polygon->IsClosed() && poly->active)
	{
		bool mergePolygons = false;
		PolygonDistancePair nearestPolygon;

		//	Existing terminating point of the poly-line
		Point2D polyLastVertex = poly->polygon->GetLastEdge().GetVertexB();

		//	Calculated positions of the next vertex
		Real nextX, nextY;

		//	Last vertex of polygon exists on one of the Y boundaries
		if (polyLastVertex.EqualsY(poly->dataMinY)
			|| polyLastVertex.EqualsY(poly->dataMaxY))
		{
			//	March along the x-axis to the relevant corner vertex;
			//	hence, fix the y-coordinate
			nextY = polyLastVertex.GetY();

			if (poly->deltaX > 0)
			{
				//	March along the X axis, placing a vertex at either the
				//	next integer lattice point or the start of the path
				//	at the corner we place an edge vertex to the top-right
				//	corner vertex (at which point we should change
				//	classification to that of a polygon on a single edge).
				Real nextLatticeX = getNextIntegerLatticePoint(
							polyLastVertex.GetX(), poly->deltaX);

				nextX = std::min(nextLatticeX, poly->dataMaxX);
			}
			else if (poly->deltaX < 0)
			{
				//	Find the nearest partial polygon to our current position
				nearestPolygon = findNearestPolygonToPoint(polygons, polyLastVertex);

				//	Find the next lattice point in the direction of travel
				Real nextLatticeX = getNextIntegerLatticePoint(
							polyLastVertex.GetX(), poly->deltaX);

				//	Determine if the next lattice point or start point of this
				//	vertex are nearer
				Real nearestVertex = std::max(nextLatticeX, poly->dataMinX);

				//	Straight line distance to nearest vertex
				Real distanceToNearestVertex =
						fabs(polyLastVertex.GetX() - nearestVertex);

				//	Check there are no polylines between the nearest vertex
				//	and the current position
				if (distanceToNearestVertex < nearestPolygon.distance)
				{
					//	Next vertex will be part of the existing mesh or a
					//	lattice point
					nextX = nearestVertex;
				}
				else
				{
					//	Next vertex will be part of an existing polyline
					//	we need to run an edge to this before adding it to the
					//	current polyline
					nextX = nearestPolygon.position.GetX();

					//	Indicate that we need to complete the merge after
					//	the edge has been added
					mergePolygons = true;
				}
			}
			else
			{
				//	Both deltaX and deltaY should be non-zero
				fprintf(stderr, "Unknown corner configuration!"
								"In function %s: file %s, line %ld\n",
						__func__, __FILE__, __LINE__);
				fflush(stderr);
				return;
			}
		}
		//	Last vertex of polygon exists on one of the X boundaries
		else if (polyLastVertex.EqualsX(poly->dataMinX)
				 || polyLastVertex.EqualsX(poly->dataMaxX))
		{
			//	March along the y-axis to the relevant corner vertex;
			//	hence, fix the x-coordinate
			nextX = polyLastVertex.GetX();

			if (poly->deltaY > 0)
			{
				//	March along the X axis, placing a vertex at either the
				//	next integer lattice point or the start of the path
				//	at the corner we place an edge vertex to the top-right
				//	corner vertex (at which point we should change
				//	classification to that of a polygon on a single edge).
				Real nextLatticeY = getNextIntegerLatticePoint(
							polyLastVertex.GetY(), poly->deltaY);
				nextY = std::min(nextLatticeY, poly->dataMaxY);
			}
			else if (poly->deltaY < 0)
			{
				//	March along the X axis, placing a vertex at either the
				//	next integer lattice point or the start of the path
				//	at the corner we place an edge vertex to the top-left
				//	corner vertex (at which point we should change
				//	classification to that of a polygon on a single edge).
				Real nextLatticeY = getNextIntegerLatticePoint(
							polyLastVertex.GetY(), poly->deltaY);
				nextY = std::max(nextLatticeY, poly->dataMinY);
			}
			else
			{
				//	Both deltaX and deltaY should be non-zero
				fprintf(stderr, "Unknown corner configuration!"
								"In function %s: file %s, line %ld\n",
						__func__, __FILE__, __LINE__);
				fflush(stderr);
				return;
			}
		}
		//	This really shouldn't happen!
		else
		{
			fprintf(stderr, "Unknown corner configuration!"
							"In function %s: file %s, line %ld\n",
					__func__, __FILE__, __LINE__);
			fflush(stderr);
			return;
		}

		//	Position of the next vertex
		Point2D nextVertex(nextX, nextY);

		//	Create the new edge
		Edge2D newEdge(polyLastVertex, nextVertex);

		//	And add to the polygon
		poly->AddEdgeToBack(newEdge);

		//	Do we need to merge a polygon?
		if (mergePolygons)
		{
			//	Attempt the merge
			bool success = poly->AddPolygonToBack(nearestPolygon.polygon->polygon);

			if (success)
			{
				//	Remove the merged polygon from the global list
				nearestPolygon.polygon->active = false;
				//delete polygons[nearestPolygon.indexOfPolygon];
				//polygons.erase(polygons.begin() + nearestPolygon.indexOfPolygon);
			}
			else
			{
				poly->active = false;
				//	Something unexpected has happened
				return;
			}
		}

		//	If we add a vertex in the corner of the data we should change
		//	classification to SPANS_SINGLE_EDGE.
		if (poly->classificationHasChanged())
		{
			switch (poly->polygonType)
			{
				case PolygonConstruction::PolygonType::SPANS_OPPOSING_EDGES:
					fprintf(stderr, "Not implemented!"
									"In function %s: file %s, line %ld\n",
							__func__, __FILE__, __LINE__);
					fflush(stderr);
					return;
				case PolygonConstruction::PolygonType::SPANS_SINGLE_EDGE:
					growAndMergePolygonOnEdge(poly,polygons);
					break;
				default:
					//	If we change to the same type or a new type
					fprintf(stderr, "Unknown new polygon calssification!"
									"In function %s: file %s, line %ld\n",
							__func__, __FILE__, __LINE__);
					fflush(stderr);
					return;
			}
		}
	}

	poly->active = false;
}

///
/// \brief		Grows and merges a polygon by advancing along a single edge
/// \param		poly: a partially constructed 2D polygon, wrapped in a helper
///				class
/// \since		29-05-2015
/// \author		Dean
///
void PolygonFactory2::growAndMergePolygonOnEdge(PolygonConstruction *poly,
												vector<PolygonConstruction*> &polygons)
{
	//	TODO: handle nested partial polygons - might require the ability to
	//	detect if a construction has changed 'type'...
	//	Maybe we could detect the relative distances the next lattice point,
	//	the start point or to any other polygon constuctions on the line and
	//	use this to determine what to do?
	while (!poly->polygon->IsClosed() && poly->active)
	{
		bool mergePolygons = false;
		PolygonDistancePair nearestPolygon;

		Point2D firstVertex = poly->polygon->GetFirstEdge().GetVertexA();
		Point2D lastVertex = poly->polygon->GetLastEdge().GetVertexB();

		Real nextX, nextY;

		if (poly->deltaY == 0)
		{
			//	Moving along the X axis
			nextY = lastVertex.GetY();

			//	March along the X axis, placing a vertex at either the
			//	next integer lattice point or the start of the path
			if (poly->deltaX < 0)
			{
				//	Find the nearest partial polygon to our current position
				nearestPolygon = findNearestPolygonToPoint(polygons, lastVertex);

				//	Find the next lattice point in the direction of travel
				Real nextLatticeX = getNextIntegerLatticePoint(
							lastVertex.GetX(), poly->deltaX);

				//	Determine if the next lattice point or start point of this
				//	vertex are nearer
				Real nearestVertex = std::max(nextLatticeX, firstVertex.GetX());

				//	Straight line distance to nearest vertex
				Real distanceToNearestVertex =
						fabs(lastVertex.GetX() - nearestVertex);

				//	Check there are no polylines between the nearest vertex
				//	and the current position
				if (distanceToNearestVertex < nearestPolygon.distance)
				{
					//	Next vertex will be part of the existing mesh or a
					//	lattice point
					nextX = nearestVertex;
				}
				else
				{
					//	Next vertex will be part of an existing polyline
					//	we need to run an edge to this before adding it to the
					//	current polyline
					nextX = nearestPolygon.position.GetX();

					//	Indicate that we need to complete the merge after
					//	the edge has been added
					mergePolygons = true;
				}
			}
			else
			{
				//	Find the nearest partial polygon to our current position
				nearestPolygon = findNearestPolygonToPoint(polygons, lastVertex);

				Real nextLatticeX = getNextIntegerLatticePoint(
							lastVertex.GetX(), poly->deltaX);
				nextX = std::min(nextLatticeX, firstVertex.GetX());
			}
		}
		else
		{
			//	Moving along the Y axis
			nextX = lastVertex.GetX();

			if (poly->deltaY < 0)
			{
				//	Find the nearest partial polygon to our current position
				nearestPolygon = findNearestPolygonToPoint(polygons, lastVertex);

				//	March along the Y axis, placing a vertex at either the
				//	next integer lattice point or the start of the path
				Real nextLatticeY = getNextIntegerLatticePoint(
							lastVertex.GetY(), poly->deltaY);

				//	Determine if the next lattice point or start point of this
				//	vertex are nearer
				Real nearestVertex = std::max(nextLatticeY, firstVertex.GetY());

				//	Straight line distance to nearest vertex
				Real distanceToNearestVertex =
						fabs(lastVertex.GetY() - nearestVertex);

				//	Check there are no polylines between the nearest vertex
				//	and the current position
				if (distanceToNearestVertex < nearestPolygon.distance)
				{
					//	Next vertex will be part of the existing mesh or a
					//	lattice point
					nextY = nearestVertex;
				}
				else
				{
					//	Next vertex will be part of an existing polyline
					//	we need to run an edge to this before adding it to the
					//	current polyline
					nextY = nearestPolygon.position.GetY();

					//	Indicate that we need to complete the merge after
					//	the edge has been added
					mergePolygons = true;
				}
			}
			else
			{
				//	March along the Y axis, placing a vertex at either the
				//	next integer lattice point or the start of the path
				Real nextLatticeY = getNextIntegerLatticePoint(
							lastVertex.GetY(), poly->deltaY);
				nextY = std::min(nextLatticeY, firstVertex.GetY());
			}
		}

		//	Position of the next vertex
		Point2D nextVertex(nextX, nextY);

		//	Create the new edge
		Edge2D newEdge(lastVertex, nextVertex);

		//	And add to the polygon
		poly->AddEdgeToBack(newEdge);

		//	Do we need to merge a polygon?
		if (mergePolygons)
		{
			//	Attempt the merge
			bool success = poly->AddPolygonToBack(nearestPolygon.polygon->polygon);

			if (success)
			{
				//	Remove the merged polygon from the global list
				nearestPolygon.polygon->active = false;
				//delete polygons[nearestPolygon.indexOfPolygon];
				//polygons.erase(polygons.begin() + nearestPolygon.indexOfPolygon);
			}
			else
			{
				//	Something unexpected has happened
				poly->active = false;

				return;
			}
		}

		//	If we add a vertex in the corner of the data we should change
		//	classification to SPANS_SINGLE_EDGE.
		if (poly->classificationHasChanged())
		{
			switch (poly->polygonType)
			{
				case PolygonConstruction::PolygonType::SPANS_OPPOSING_EDGES:
					fprintf(stderr, "Not implemented!"
									"In function %s: file %s, line %ld\n",
							__func__, __FILE__, __LINE__);
					fflush(stderr);
					return;
				case PolygonConstruction::PolygonType::SPANS_CORNER:
					growAndMergePolygonOnCorner(poly,polygons);
					break;
				default:
					//	If we change to the same type or a new type
					fprintf(stderr, "Unknown new polygon calssification!"
									"In function %s: file %s, line %ld\n",
							__func__, __FILE__, __LINE__);
					fflush(stderr);
					return;
			}
		}
	}

	poly->active = false;
	//vector<PolygonConstruction*>::iterator it
	//		= std::find(polygons.begin(), polygons.end(), poly);
	//delete it.pointer;
	//polygons.erase(it);
}

PolygonFactory2::PolygonFactory2(const HeightField3D::Boundary &targetBoundary,
								 HeightField3D *heightfield, Contour *contour)
{
	m_targetBoundary = targetBoundary;
	m_contour = contour;
	m_heightfield = heightfield;

	//	Calculate the limits of the plane we are working on
	switch (m_targetBoundary)
	{
		case HeightField3D::Boundary::MIN_X:
		case HeightField3D::Boundary::MAX_X:
			//	Working on the YZ plane
			m_planeMinX = 0.0;
			m_planeMaxX = m_heightfield->GetMaxY();
			m_planeMinY = 0.0;
			m_planeMaxY = m_heightfield->GetMaxZ();
			if (m_targetBoundary == HeightField3D::Boundary::MIN_X)
			{
				//	Filter will only extract 3D edges with X coordinate of 0.0
				m_filterValue = 0.0;
			}
			else if (m_targetBoundary == HeightField3D::Boundary::MAX_X)
			{
				//	Filter will only extract 3D edges with X coordinate of MaxX
				m_filterValue = m_heightfield->GetMaxX();
			}
			break;
		case HeightField3D::Boundary::MIN_Y:
		case HeightField3D::Boundary::MAX_Y:
			//	Working on the XZ plane
			m_planeMinX = 0.0;
			m_planeMaxX = m_heightfield->GetMaxX();
			m_planeMinY = 0.0;
			m_planeMaxY = m_heightfield->GetMaxZ();
			if (m_targetBoundary == HeightField3D::Boundary::MIN_Y)
			{
				//	Filter will only extract 3D edges with Y coordinate of 0.0
				m_filterValue = 0.0;
			}
			else if (m_targetBoundary == HeightField3D::Boundary::MAX_Y)
			{
				//	Filter will only extract 3D edges with Y coordinate of MaxY
				m_filterValue = m_heightfield->GetMaxY();
			}
			break;
		case HeightField3D::Boundary::MIN_Z:
		case HeightField3D::Boundary::MAX_Z:
			//	Working on the XY plane
			m_planeMinX = 0.0;
			m_planeMaxX = m_heightfield->GetMaxX();
			m_planeMinY = 0.0;
			m_planeMaxY = m_heightfield->GetMaxY();
			if (m_targetBoundary == HeightField3D::Boundary::MIN_Z)
			{
				//	Filter will only extract 3D edges with Z coordinate of 0.0
				m_filterValue = 0.0;
			}
			else if (m_targetBoundary == HeightField3D::Boundary::MAX_Z)
			{
				//	Filter will only extract 3D edges with Z coordinate of MaxZ
				m_filterValue = m_heightfield->GetMaxZ();
			}
			break;
	}
}

PolygonFactory2::~PolygonFactory2()
{

}

