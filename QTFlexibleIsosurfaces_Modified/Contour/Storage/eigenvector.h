#ifndef EIGENVECTOR
#define EIGENVECTOR

#include "Globals/Globals.h"

#include <Eigen/Dense>
#include <string>
#include <sstream>

using Eigen::Vector3d;
using std::string;
using std::stringstream;

struct EigenVector
{
		Real eigenValue;
		Vector3d eigenVector;

		EigenVector()
			: eigenValue {}, eigenVector {}
		{ }

		EigenVector(Real eValue, Vector3d eVector)
		{
			eigenValue = eValue;
			eigenVector = eVector;
		}

		string ToString() const
		{
			stringstream result;

			result << "EigenValue: " << eigenValue;
			result << " EigenVector: |" << eigenVector.transpose() << "|\n";

			return result.str();
		}

		bool operator < (const EigenVector& rhs) const
		{
			if (abs(eigenValue) < abs(rhs.eigenValue))
				return true;
			else if (abs(eigenValue) > abs(rhs.eigenValue))
				return false;
		}
};

#endif // EIGENVECTOR

