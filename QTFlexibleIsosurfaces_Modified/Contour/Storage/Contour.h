#ifndef CONTOUR_H
#define CONTOUR_H

///     Maintains a list of triangles that form a Contour

#define VERBOSE_OUTPUT




#include "Globals/TypeDefines.h"
#include "Contour/Storage/MeshTriangle.h"
#include <vector>
#include <string>
#include <sstream>
#include <iostream>
#include "../../HeightField/HeightField3D.h"
#include <climits>

#ifdef CALCULATE_MOMENTS
#include "eigenvector.h"
//	See:
//	http://eigen.tuxfamily.org/dox-devel/group__TopicUnalignedArrayAssert.html
//	[29-07-2015]
//#define EIGEN_DONT_VECTORIZE
//#define EIGEN_DISABLE_UNALIGNED_ARRAY_ASSERT
#define EIGEN_DONT_ALIGN_STATICALLY
#include <Eigen/Dense>
#include <Eigen/Eigenvalues>

using Eigen::Matrix3d;
#endif

//#define VERBOSE_OUTPUT


//typedef LineSegment3f Edge3D;
class ContourTreeSuperarc;
class ContourTreeSupernode;
//struct Moments
//{

//};


typedef vector<MeshEdge3D*> ClosedPath3D;

class Contour
{
		friend class ContourFactory;


		//typedef vector<Point3D> VertexList;
		//typedef vector<Edge3D> EdgeList;
		//typedef vector<Triangle3D> faceList;
	private:
		vector<MeshVertex3D*> m_vertices;
		vector<Vector3D*> m_normals;
		vector<MeshTriangle3D*> m_triangles;
	public:
		vector<MeshEdge3D*> m_edges;
#ifdef CALCULATE_MOMENTS
		EIGEN_MAKE_ALIGNED_OPERATOR_NEW_IF(true)
#endif
	private:
		void calculateBoundaries();

		ContourTreeSuperarc *m_superarc;

		//	Keep a separate list of polygons for each boundary
		vector<Polygon2D*> m_cappingPolygonsMinX;
		vector<Polygon2D*> m_cappingPolygonsMaxX;
		vector<Polygon2D*> m_cappingPolygonsMinY;
		vector<Polygon2D*> m_cappingPolygonsMaxY;
		vector<Polygon2D*> m_cappingPolygonsMinZ;
		vector<Polygon2D*> m_cappingPolygonsMaxZ;

		vector<Polygon3D*> m_3dCappingPolygons;
		//VertexList m_vertexList;
		//EdgeRecordList m_edgeList;



		Real m_totalArea = 0.0;
		Real m_totalVolume = 0.0;

		Real m_m000 = 0.0f;

		Real m_m100 = 0.0f;
		Real m_m010 = 0.0f;
		Real m_m001 = 0.0f;

		Real m_m110 = 0.0f;
		Real m_m011 = 0.0f;
		Real m_m101 = 0.0f;

		Real m_m200 = 0.0f;
		Real m_m020 = 0.0f;
		Real m_m002 = 0.0f;

		//	Actual extremes of this contour
		Real m_xMin, m_xMax;
		Real m_yMin, m_yMax;
		Real m_zMin, m_zMax;

#ifdef CALCULATE_MOMENTS
		Matrix3d m_inertiaTensor;

		EigenVector m_principleAxis[3];
#endif

		Real m_isovalue;

		long m_eulerCharacteristic;

		bool m_statisticsCalculated;
		bool m_isUnderConstruction;

		void calculateStatistics();
		void calculateInertia();

		MeshVertex3D *lookupVertex(const Point3D &vertex);
		Vector3D *lookupNormal(const Vector3D &normal);
		MeshEdge3D *lookupEdge(MeshVertex3D *v1, MeshVertex3D *v2);

		bool updateEdgeList(const Edge3D &edge);

	public:
		Real GetIsovalue() const { return m_isovalue; }

		vector<Polygon2D*> Get2dCappingPolygons(const HeightField3D::Boundary &targetBoundary) const;
		vector<Polygon3D*> Get3dCappingPolygons() const { return m_3dCappingPolygons; }

		string CappingPolygonsToString() const;

		bool StatisticsCalculated() const { return m_statisticsCalculated; }

		bool IsSelected() const;
		bool IsHiddenByUser() const;

		void SetHighlighted(const bool value);
		bool IsHighlighted() const;

		Real GetMinimumIsovalue() const;
		Real GetMaximumIsovalue() const;
		Real GetIsovalueRange() const;

		ContourTreeSuperarc *GetSuperarc() const { return m_superarc; }
		ContourTreeSupernode *GetTopSupernode() const;
		ContourTreeSupernode *GetBottomSupernode() const;
#ifdef CALCULATE_MOMENTS
		EigenVector GetPrincipleAxis() const {
			return GetPrincipleAxis(0); }
		EigenVector GetSecondaryAxis() const {
			return GetPrincipleAxis(1); }
		EigenVector GetTertiaryAxis() const {
			return GetPrincipleAxis(2); }
#endif
		bool IsClosedSurface();

		string GetOBJ() const;


		bool IsOnBoundaryMinX() const;
		bool IsOnBoundaryMaxX() const;
		bool IsOnBoundaryMinY() const;
		bool IsOnBoundaryMaxY() const;
		bool IsOnBoundaryMinZ() const;
		bool IsOnBoundaryMaxZ() const;

		bool IsOnAnyBoundary() const { return
					IsOnBoundaryMinX() || IsOnBoundaryMaxX()
					|| IsOnBoundaryMinY() || IsOnBoundaryMaxY()
					|| IsOnBoundaryMinZ() || IsOnBoundaryMaxZ();}

		Real GetMinX() const { return m_xMin; }
		Real GetMaxX() const { return m_yMax; }
		Real GetMinY() const { return m_yMin; }
		Real GetMaxY() const { return m_yMax; }
		Real GetMinZ() const { return m_zMin; }
		Real GetMaxZ() const { return m_zMax; }

		void PostProcess();

		void SetIsUnderConstruction(const bool value);
		bool IsUnderConstruction() const { return m_isUnderConstruction; }

#ifdef CALCULATE_MOMENTS
		EigenVector GetPrincipleAxis(const unsigned short i) const
		{
			if (i > 2)
				throw InvalidIndexException(i, __func__, __FILE__, __LINE__);

			return m_principleAxis[i];
		}
#endif
		Contour(ContourTreeSuperarc *arc, Real isovalue);
		~Contour();

		string EdgeListToString() const;



		vector<Point3D> GetVertexStream() const;

		MeshTriangle3D *GetMeshTriangle(const unsigned long index) const
		{
			return m_triangles[index];
		}

		unsigned long GetEdgeCount() const { return m_edges.size(); }
		unsigned long GetTriangleCount() const { return m_triangles.size(); }

		long GetEulerCharacteristic();

		Real GetSurfaceArea();
		Real GetUnscaledVolume();
		Point3f GetCentreOfMass();
		Point3f GetMomentOfInertia();

		//void triangulateBoundary(EdgeList2 &unorderedEdges, Boundary plane);
		void triangulateBoundaryPolygons(const HeightField3D::Boundary &boundary,
										 const vector<Polygon2D*> polygons);

		//PolygonConstruction *constructPolygon(EdgeList2 &unorderedEdges,
		//									 Boundary targetPlane) const;
		string ToString();
};

#endif // CONTOUR_H
