#ifndef MESHCACHE_H
#define MESHCACHE_H

#include "Contour.h"
#include <ThreadSafeVector.h>
#include "Globals/Globals.h"

//#include <vector>
#include <iostream>

#define MAX_CACHE_SIZE 100

struct MeshCacheItem
{
		enum MeshStatus
		{
			READY,
			CONSTRUCTING
		};

		Contour* contour = nullptr;
		Real isovalue;
		MeshStatus status;

		bool operator() (const Real& rhs) const
		{
			return isovalue == rhs;
		}

		MeshCacheItem(const Real isovalue)
			: contour{nullptr}, isovalue{isovalue}, status{CONSTRUCTING} {}

		void SetContour(Contour* theContour)
		{
			contour = theContour;
			status = READY;
		}
};

//using std::vector;

class MeshCache
{
	private:
		ThreadSafeVector<MeshCacheItem*> m_items;
	public:
		MeshCache();
		bool ContainsMeshWithIsovalue(const Real &isovalue) const;
		MeshCacheItem *QueueMesh(const Real &isovalue);
		MeshCacheItem *FindCacheItemWithIsovalue(const Real &isovalue) const;
		~MeshCache();
};

#endif // MESHCACHE_H
