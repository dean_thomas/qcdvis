#ifndef CONTOURLIST_H
#define CONTOURLIST_H

#include <vector>
#include <string>
#include <sstream>

#include "Globals/Globals.h"
#include "Contour.h"

using std::string;
using std::vector;
using std::stringstream;

class ContourList
{
private:
	vector<Contour*> m_contours;
    unsigned long m_maxPathLength;
    unsigned long m_totalTriangles;
	Real m_totalExtractionTime;
    unsigned int m_totalPathLength;
	Real m_averagePathLength;
public:
    ContourList();
    ContourList(const long initialSize);

	void AddContour(Contour *theContour);
	Contour *GetContour(const unsigned long index) const;
    unsigned long GetContourCount() const { return m_contours.size(); }
    void Clear();

    void CalculateStatistics();

	void UnhighlightAll();

    string ToString() const;
};

#endif // CONTOURLIST_H
