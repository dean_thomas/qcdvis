#ifndef MESHTRIANGLE_H
#define MESHTRIANGLE_H

#include "Globals/TypeDefines.h"
#include "MeshVertex.h"

//typedef Triangle3f Triangle3D;
//typedef Point3f Point3D;


class MeshTriangle3D
{
public:
	MeshVertex3D *m_vA, *m_vB, *m_vC;
	Vector3D *m_nA, *m_nB, *m_nC;
	MeshEdge3D *m_eAB, *m_eAC, *m_eBC;
private:
	Triangle3D m_unscaledTriangle;
	Triangle3D m_normals;

	Real m_m000;

	Real m_m100;
	Real m_m010;
	Real m_m001;

	Real m_m110;
	Real m_m011;
	Real m_m101;

	Real m_m111;

	Real m_m200;
	Real m_m020;
	Real m_m002;

	Real m_m300;
	Real m_m030;
	Real m_m003;

	Real m_m400;
	Real m_m040;
	Real m_m004;

	void calculateMoments();

	Real calculateM000() const;

	Real calculateM100() const;
	Real calculateM010() const;
	Real calculateM001() const;

	Real calculateM110() const;
	Real calculateM101() const;
	Real calculateM011() const;

	Real calculateM111() const;

	Real calculateM200() const;
	Real calculateM020() const;
	Real calculateM002() const;

	Real calculateM300() const;
	Real calculateM030() const;
	Real calculateM003() const;

	Real calculateM400() const;
	Real calculateM040() const;
	Real calculateM004() const;
public:
	MeshTriangle3D(MeshVertex3D *v0, MeshVertex3D *v1, MeshVertex3D *v2,
				 Vector3D *n0, Vector3D *n1, Vector3D *n2,
				 MeshEdge3D *eAB, MeshEdge3D *eAC, MeshEdge3D *eBC);

	Triangle3D GetUnscaledTriangle() const { return m_unscaledTriangle; }
	Triangle3D GetNormals() const { return m_normals; }



	Real GetM000() const { return m_m000; }

	//	First order
	Real GetM100() const { return m_m100; }
	Real GetM010() const { return m_m010; }
	Real GetM001() const { return m_m001; }

	//	Permutations of first order
	Real GetM110() const { return m_m110; }
	Real GetM101() const { return m_m101; }
	Real GetM011() const { return m_m011; }

	Real GetM200() const { return m_m200; }
	Real GetM020() const { return m_m020; }
	Real GetM002() const { return m_m002; }

	Real GetM300() const { return m_m300; }
	Real GetM030() const { return m_m030; }
	Real GetM003() const { return m_m003; }

	Real GetM400() const { return m_m400; }
	Real GetM040() const { return m_m040; }
	Real GetM004() const { return m_m004; }

	Real GetSignedVolumeOfUnscaledTetrahedronFormedWithOrigin() const
	{ return m_m000; }
};

#endif // MESHTRIANGLE_H
