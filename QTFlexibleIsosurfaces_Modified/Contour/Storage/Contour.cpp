#include "Contour.h"
#include "Graphs/ContourTreeSuperarc.h"
#include "Graphs/ContourTree.h"
#include "Graphs/ContourTreeSuperarc.h"
#include "Contour/Generation/PolygonFactory2.h"
#include "Contour/Generation/PolygonFactory3.h"

vector<Polygon2D*> Contour::Get2dCappingPolygons(const HeightField3D::Boundary &targetBoundary) const
{
	switch (targetBoundary)
	{
		case HeightField3D::Boundary::MIN_X:
			return m_cappingPolygonsMinX;
		case HeightField3D::Boundary::MAX_X:
			return m_cappingPolygonsMaxX;
		case HeightField3D::Boundary::MIN_Y:
			return m_cappingPolygonsMinY;
		case HeightField3D::Boundary::MAX_Y:
			return m_cappingPolygonsMaxY;
		case HeightField3D::Boundary::MIN_Z:
			return m_cappingPolygonsMinZ;
		case HeightField3D::Boundary::MAX_Z:
			return m_cappingPolygonsMaxZ;
		default:
			//	This shouldn't happen using strongly typed enums
			fprintf(stderr, "Unknown 3D boundary passed to function: %s,"
							" in file %s (line %ld).\n", __func__, __FILE__, __LINE__);
			fflush(stderr);
			break;
	}
}

string Contour::CappingPolygonsToString() const
{
	//	FIXME: returns blank string at present
	const string boundaryString[] = { "MIN_X", "MIN_Y", "MIN_Z",
									  "MAX_X", "MAX_Y", "MAX_Z" };

	stringstream result;

	/*
	for (unsigned long boundary = 0; boundary < 6; ++boundary)
	{
		result << "Capping polygons on " << boundaryString[boundary]
				  << " boundary (" << m_cappingPolygons[boundary].size()
				  << "):\n";

		for (unsigned long polygon = 0;
			 polygon < m_cappingPolygons[boundary].size(); ++polygon)
		{
			Polygon2D *p = m_cappingPolygons[boundary][polygon];

			result << "\t" << polygon << ":\t" << p->ToString() << "\n";
		}
	}
	*/
	return result.str();
}

///
/// \brief		Destructor
/// \since		07-05-2015
/// \author		Dean
///
Contour::~Contour()
{
	//	Clear each of the face polygon lists
	//for (unsigned long boundary = 0; boundary < 6; ++boundary)
	//{
	//	Keep deleting items (from the back) until the vector is empty
	//		while (!m_polygonsMinZ.empty())
	//		{
	//			delete m_polygonsMinZ.back();
	//			m_polygonsMinZ.pop_back();
	//		}
	//}
}

bool Contour::IsOnBoundaryMinX() const
{
	return m_xMin == (Real)0.0;
}

bool Contour::IsOnBoundaryMaxX() const
{
	return m_xMax == m_superarc->GetContourTree()->GetHeightfield()->GetMaxX();
}

bool Contour::IsOnBoundaryMinY() const
{
	return m_yMin == (Real)0.0;
}

bool Contour::IsOnBoundaryMaxY() const
{
	return m_yMax == m_superarc->GetContourTree()->GetHeightfield()->GetMaxY();
}

bool Contour::IsOnBoundaryMinZ() const
{
	return m_zMin == (Real)0.0;
}

bool Contour::IsOnBoundaryMaxZ() const
{
	return m_zMax == m_superarc->GetContourTree()->GetHeightfield()->GetMaxZ();
}

///
/// \brief		Returns the list of vertices to make up triangles of the mesh.
///				Order is as follows:
///				t0vA, t0vB, t0vC, t1vA, t1vB, ... , t(n-1)vB, t(n-1)vC
/// \return
///
vector<Point3D> Contour::GetVertexStream() const
{
	vector<Point3D> result;

	//	We know the number ot triangles we have, we also know we will have
	//	3 position vertices per triangle, so resize in advance
	result.reserve(3 * m_triangles.size());

	for (unsigned long i = 0; i < m_triangles.size(); ++i)
	{
		result.push_back(m_triangles[i]->GetUnscaledTriangle().GetVertexA());
		result.push_back(m_triangles[i]->GetUnscaledTriangle().GetVertexB());
		result.push_back(m_triangles[i]->GetUnscaledTriangle().GetVertexC());
	}
	return result;
}

Real Contour::GetMinimumIsovalue() const
{
	return m_superarc->GetBottomSupernode()->GetHeight();
}

Real Contour::GetMaximumIsovalue() const
{
	return m_superarc->GetTopSupernode()->GetHeight();
}

Real Contour::GetIsovalueRange() const
{
	return m_superarc->GetTopSupernode()->GetHeight()
			- m_superarc->GetBottomSupernode()->GetHeight();
}

ContourTreeSupernode *Contour::GetTopSupernode() const
{
	return m_superarc->GetTopSupernode();
}

ContourTreeSupernode *Contour::GetBottomSupernode() const
{
	return m_superarc->GetBottomSupernode();
}

bool Contour::IsSelected() const
{
	return m_superarc->IsSelected();
}

bool Contour::IsHiddenByUser() const
{
	return m_superarc->IsHidden();
}


void Contour::SetHighlighted(const bool value)
{
	m_superarc->SetHighlighted(value);
}

bool Contour::IsHighlighted() const
{
	return m_superarc->IsHighlighted();
}

void Contour::calculateBoundaries()
{
#define SUPRESS_OUTPUT
	//	Initialze all values to extreme values
	m_xMin = m_yMin = m_zMin = std::numeric_limits<Real>::max();
	m_xMax = m_yMax = m_zMax = std::numeric_limits<Real>::min();

	//	Loop through every triangle in the contour mesh
	for (unsigned long t = 0; t < GetTriangleCount(); ++t)
	{
		Triangle3D triangle = GetMeshTriangle(t)->GetUnscaledTriangle();

		//	Compute the minimum X, Y, Z coordinate for the current
		//	triangle
		Real minX = std::min(std::min(triangle.GetVertexA().GetX(),
									  triangle.GetVertexB().GetX()),
							 triangle.GetVertexC().GetX());
		Real minY = std::min(std::min(triangle.GetVertexA().GetY(),
									  triangle.GetVertexB().GetY()),
							 triangle.GetVertexC().GetY());
		Real minZ = std::min(std::min(triangle.GetVertexA().GetZ(),
									  triangle.GetVertexB().GetZ()),
							 triangle.GetVertexC().GetZ());

		//	Compute the maximum X, Y, Z coordinate for the current
		//	triangle
		Real maxX = std::max(std::max(triangle.GetVertexA().GetX(),
									  triangle.GetVertexB().GetX()),
							 triangle.GetVertexC().GetX());
		Real maxY = std::max(std::max(triangle.GetVertexA().GetY(),
									  triangle.GetVertexB().GetY()),
							 triangle.GetVertexC().GetY());
		Real maxZ = std::max(std::max(triangle.GetVertexA().GetZ(),
									  triangle.GetVertexB().GetZ()),
							 triangle.GetVertexC().GetZ());

		//	Update the global (as in current contour)
		//	minimum and maximum for each axis
		if (minX < m_xMin) m_xMin = minX;
		if (minY < m_yMin) m_yMin = minY;
		if (minZ < m_zMin) m_zMin = minZ;
		if (maxX > m_xMax) m_xMax = maxX;
		if (maxY > m_yMax) m_yMax = maxY;
		if (maxZ > m_zMax) m_zMax = maxZ;
	}
#if defined(VERBOSE_OUTPUT) && !defined(SUPRESS_OUTPUT)
	printf("Contour generated by arc id: %ld.\n", m_superarc->GetSuperarcIndex());
	printf("X range: %f-%f; Y range: %f-%f; Z range: %f-%f.\n", m_xMin, m_xMax,
		   m_yMin, m_yMax, m_zMin, m_zMax);
	printf("Is on x-min boundary: %s (%f | %f).\n",
		   IsOnBoundaryMinX() ? "true" : "false", m_xMin, 0.0);
	printf("Is on x-max boundary: %s (%f | %f).\n",
		   IsOnBoundaryMaxX() ? "true" : "false",
		   m_xMax,
		   m_superarc->GetContourTree()->GetHeightfield()->GetMaxX());
	printf("Is on y-min boundary: %s (%f | %f).\n",
		   IsOnBoundaryMinY() ? "true" : "false", m_yMin, 0.0);
	printf("Is on y-max boundary: %s (%f | %f).\n",
		   IsOnBoundaryMaxY() ? "true" : "false",
		   m_yMax,
		   m_superarc->GetContourTree()->GetHeightfield()->GetMaxY());
	printf("Is on z-min boundary: %s (%f | %f).\n",
		   IsOnBoundaryMinZ() ? "true" : "false", m_zMin, 0.0);
	printf("Is on z-max boundary: %s (%f | %f).\n",
		   IsOnBoundaryMaxZ() ? "true" : "false",
		   m_zMax,
		   m_superarc->GetContourTree()->GetHeightfield()->GetMaxZ());
	printf("Is on ANY boundary: %s.\n", IsOnAnyBoundary() ? "true" : "false");
	fflush(stdout);
#endif

#undef SUPRESS_OUTPUT
}

bool Contour::IsClosedSurface()
{
	//	First, check the Euler characteristic
	//	2 for spherical, 0 for torus, -2 for 2 torus etc...
	if (GetEulerCharacteristic() % 2 != 0) return false;

	//	Now, as a backup, we can check the bounding box - if it touches any
	//	edge we can assume it is not closed
	return !(IsOnAnyBoundary());
}



string Contour::GetOBJ() const
{
	stringstream result;
	stringstream vertexConfigurations;

	//	Header
	struct tm tm;
	time_t rawtime;

	time(&rawtime);
	tm = *localtime(&rawtime);

	result << "#\tQTFlexibleIsosurface contour model\n";
	result << "#\n";
	result << "#\tGenerated at: " << tm.tm_hour << ":" << tm.tm_min
		   << ":" << tm.tm_sec << "\n";
	result << "#\ton: " << tm.tm_mday << "-" << (1+tm.tm_mon)
		   << "-" << (1900+tm.tm_year) << "\n";
	result << "#\n";

	//	Indices for face look up
	unsigned long vIndex = 1;

	//	Vertex list
	for (unsigned long t = 0; t < GetTriangleCount(); ++t)
	{
		//	Access the the raw triangle data (not the ones scaled for rendering)
		MeshTriangle3D *currentMeshTriangle = GetMeshTriangle(t);
		Triangle3D rawTriangle
				= currentMeshTriangle->GetUnscaledTriangle();

		//	Add vertices to stringstream
		result << "v " << rawTriangle.GetVertexA().GetX() << " "
			   << rawTriangle.GetVertexA().GetY() << " "
			   << rawTriangle.GetVertexA().GetZ() << "\n";
		result << "v " << rawTriangle.GetVertexB().GetX() << " "
			   << rawTriangle.GetVertexB().GetY() << " "
			   << rawTriangle.GetVertexB().GetZ() << "\n";
		result << "v " << rawTriangle.GetVertexC().GetX() << " "
			   << rawTriangle.GetVertexC().GetY() << " "
			   << rawTriangle.GetVertexC().GetZ() << "\n";

		//	Store the configuration order for later
		vertexConfigurations << "f " << vIndex << " " << vIndex+1
							 << " " << vIndex+2 << "\n";

		vIndex += 3;
	}

	//	Now all the vertices are written we can write the face configurations
	result << vertexConfigurations.str();

	return result.str();
}

///
/// \brief		Checks if a vertex is already defined in the mesh
/// \param		vertex: the vertex to be checked
/// \return		boolean: true if the vertex is unique
/// \since      04-12-2014
/// \author		Dean
/// \author		Dean: update to keep a count of individual vertices as part of
///				a struct [17-04-2015]
/// \author		Dean: modified to use lookup tables [18-05-2015]
///
MeshVertex3D *Contour::lookupVertex(const Point3D &vertex)
{
	//	Check that the vertex doesn't already exist in the list
	for (unsigned long i = 0; i < m_vertices.size(); ++i)
	{
		if (m_vertices[i]->position == vertex)
		{
			//	Found the vertex is already in the list
			return m_vertices[i];
		}
	}

	//	It wasn't found so...
	//	add the vertex to the list and return that
	m_vertices.push_back(new MeshVertex3D(vertex));
	return m_vertices[m_vertices.size() - 1];
}

///
/// \brief		Checks if a vertex is already defined in the mesh
/// \param		vertex: the vertex to be checked
/// \return		boolean: true if the vertex is unique
/// \since      04-12-2014
/// \author		Dean
/// \author		Dean: update to keep a count of individual vertices as part of
///				a struct [17-04-2015]
/// \author		Dean: modified to use lookup tables [18-05-2015]
///
MeshEdge3D *Contour::lookupEdge(MeshVertex3D *v1, MeshVertex3D *v2)
{
	//	Check that the edge doesn't already exist in the list
	for (unsigned long i = 0; i < m_edges.size(); ++i)
	{
		//	Eiher orientation is allowable
		if (((m_edges[i]->vA == v1) && (m_edges[i]->vB == v2))
			|| ((m_edges[i]->vA == v2) && (m_edges[i]->vB == v1)))
		{
			//	Found the vertex is already in the list
			return m_edges[i];
		}
	}

	//	It wasn't found so...
	//	add the vertex to the list and return that
	m_edges.push_back(new MeshEdge3D(v1, v2));
	return m_edges[m_edges.size() - 1];
}

///
/// \brief		Checks if a normal is already defined in the mesh
/// \param		normal: the normal to be checked
/// \return		boolean: true if the vertex is unique
/// \since      18-05-2015
/// \author		Dean
///
Vector3D *Contour::lookupNormal(const Vector3D &normal)
{
	//	Check that the vertex doesn't already exist in the list
	for (unsigned long i = 0; i < m_normals.size(); ++i)
	{
		if (*m_normals[i] == normal)
		{
			//	Found the vertex is already in the list
			return m_normals[i];
		}
	}

	//	It wasn't found so...
	//	add the normal to the list and return that
	m_normals.push_back(new Vector3D(normal));
	return m_normals[m_normals.size() - 1];
}

///
/// \brief		Checks if an edge is already defined in the mesh
/// \param		edge: the edge to be checked
/// \return		boolean: true if the edge is unique
/// \since      04-12-2014
/// \author		Dean
/// \author		Dean: update to keep a count of individual edges as part of
///				a struct [17-04-2015]
///
bool Contour::updateEdgeList(const Edge3D &edge)
{
	return false;
	/*
	//	Check that the edge doesn't already exist in the list
	for (unsigned long i = 0; i < m_edgeList.size(); ++i)
	{
		//	Check XY and YX vertex configurations
		if (edge.IsCongruentTo(m_edgeList[i].edge))
		{
			//	Found the edge is already in the list
			++m_edgeList[i].count;

			return false;
		}
	}

	//	If not, we add to the list and set the count to 1
	m_edgeList.push_back({edge,1});
	return true;
	*/
}

long Contour::GetEulerCharacteristic()
{
	//	Check we are using upto date values
	if (!m_statisticsCalculated) calculateStatistics();

	return m_eulerCharacteristic;
}

Real Contour::GetSurfaceArea()
{
	//	Check we are using upto date values
	if (!m_statisticsCalculated) calculateStatistics();

	return m_totalArea;
}

Real Contour::GetUnscaledVolume()
{
	//	Check we are using upto date values
	if (!m_statisticsCalculated) calculateStatistics();

	return m_totalVolume;
}

Point3f Contour::GetCentreOfMass()
{
	//	Check we are using upto date values
	if (!m_statisticsCalculated) calculateStatistics();

	//printf("Calculating COM!\n");
	//fflush(stdout);

	if (fabs(m_m000) != 0.0)
	{
		return Point3f(m_m100 / m_m000,
					   m_m010 / m_m000,
					   m_m001 / m_m000);
	}
	else
	{
		fprintf(stderr, "Caught division by zero in %s!"
				"  File: %f, line: %ld.\n\n", __func__, __FILE__, __LINE__);
		fflush(stderr);
	}
}

Point3f Contour::GetMomentOfInertia()
{
	//	Check we are using upto date values
	if (!m_statisticsCalculated) calculateStatistics();

	return Point3f(m_m200,
				   m_m020,
				   m_m002);
}

///
/// \brief TriangleList::TriangleList
/// \since      04-12-2014
/// \author     Dean
///
Contour::Contour(ContourTreeSuperarc *arc, Real isovalue)
{
	//	What superarc represents this contour in the tree?
	m_superarc = arc;
	m_isovalue = isovalue;

	//	Can be used to delay statistic calculations for large meshes
	m_statisticsCalculated = false;
	m_isUnderConstruction = false;
}


void Contour::SetIsUnderConstruction(const bool value)
{
	m_isUnderConstruction = value;

	if (m_isUnderConstruction)
	{
		//	Stats will need to be recalculated
		m_statisticsCalculated = false;
	}
	else
	{
		//	If we have finished we can calculate the statistics
		calculateStatistics();
	}
}

///
/// \brief TriangleList::calculateStatistics
/// \since      04-12-2014
/// \author     Dean
///
void Contour::calculateStatistics()
{
	//	No point in calculating on a partly build model
	if (m_isUnderConstruction) return;

#define SUPRESS_OUTPUT
	Real m_secondOrderMomentsMatrix[9];

#if defined(VERBOSE_OUTPUT) && !defined(SUPRESS_OUTPUT)
	printf("Recalculating mesh statistics.\n");
	fflush(stdout);
#endif

	m_m000 = 0.0f;

	m_m100 = 0.0f;
	m_m010 = 0.0f;
	m_m001 = 0.0f;

	m_m110 = 0.0f;
	m_m011 = 0.0f;
	m_m101 = 0.0f;

	m_m200 = 0.0f;
	m_m020 = 0.0f;
	m_m002 = 0.0f;

	//	Set all elements to zero
	std::fill(m_secondOrderMomentsMatrix, m_secondOrderMomentsMatrix + 9, 0.0f);

	//	X = V - E + F
	//	TODO: fix me!
	m_eulerCharacteristic = m_vertices.size() - m_edges.size()
				+ m_triangles.size();
	cout << "Euler characteristic: " << m_eulerCharacteristic << endl;

	m_totalArea = 0.0f;
	m_totalVolume = 0.0f;

	for (unsigned int i = 0; i < m_triangles.size(); ++i)
	{
		m_totalArea += m_triangles[i]->GetUnscaledTriangle().GetArea();

		m_m000 += m_triangles[i]->GetM000();

		m_m100 += m_triangles[i]->GetM100();
		m_m010 += m_triangles[i]->GetM010();
		m_m001 += m_triangles[i]->GetM001();

		m_m110 += m_triangles[i]->GetM110();
		m_m011 += m_triangles[i]->GetM011();
		m_m101 += m_triangles[i]->GetM101();

		m_m200 += m_triangles[i]->GetM200();
		m_m020 += m_triangles[i]->GetM020();
		m_m002 += m_triangles[i]->GetM002();
	}

	m_totalVolume = fabs(m_m000);

#if defined(VERBOSE_OUTPUT) && !defined(SUPRESS_OUTPUT)
	printf("Euler characteristic: %ld.\n", m_eulerCharacteristic);
	fflush(stdout);
#endif

#ifdef CALCULATE_MOMENTS
	calculateInertia();
#endif

	//	Clear the out-of-date flag
	m_statisticsCalculated = true;

#if defined(VERBOSE_OUTPUT) && !defined(SUPRESS_OUTPUT)
	printf("Finsihed calculating mesh statistics.\n");
	printf("%s\n", EdgeListToString().c_str());
	fflush(stdout);
#endif

#undef SUPRESS_OUTPUT
}

///
/// \brief ContourMesh::EdgeListToString
/// \return
/// \since		17-04-2015
/// \author		Dean
///
string Contour::EdgeListToString() const
{
	stringstream result;

	/*
	result << "Unique edges: " << m_edgeList.size() << "\n";

	for (unsigned long i = 0; i < m_edgeList.size(); ++i)
	{
		result << i << "\t" << m_edgeList[i].edge.ToString(2) << "\t";
		result << "count: " << m_edgeList[i].count << "\n";
	}
	*/
	return result.str();
}

///
/// \brief		Creates a set of filling triangles on the target plane to close
///				the fill the pre-constructed polygon
///
void Contour::triangulateBoundaryPolygons(const HeightField3D::Boundary &boundary,
										  const vector<Polygon2D*> polygons)
{
#define SUPRESS_OUTPUT
	//	Convenience variables
	const Real X_MAX =
			m_superarc->GetContourTree()->GetHeightfield()->GetMaxX();
	const Real Y_MAX =
			m_superarc->GetContourTree()->GetHeightfield()->GetMaxY();
	const Real Z_MAX =
			m_superarc->GetContourTree()->GetHeightfield()->GetMaxZ();

	for (unsigned long p = 0;	p < polygons.size(); ++p)
	{
		//	Grab a polygon from the boundary list
		Polygon2D *currentPolygon = polygons[p];

		//	Only work with closed polygons: this method should only ever be called
		//	with a closed polygon, so if we fail this we have problems!
		if (currentPolygon->IsClosed())
		{
			vector<Triangle2D> triangles = currentPolygon->Triangulate();

			for (unsigned long t = 0; t < triangles.size(); ++t)
			{
				//	Our 2D vertices scaled to 3D on the appropriate axis
				//	and normal
				Point3D vA;
				Point3D vB;
				Point3D vC;
				Vector3D normal;

				switch (boundary)
				{
					case HeightField3D::Boundary::MIN_X:
#if defined(VERBOSE_OUTPUT) && !defined(SUPRESS_OUTPUT)
						printf("Generating triangle for MIN_X axis\n");
						fflush(stdout);
#endif
						//	Scale back up to 3D (setting x=0.0)
						vA = Point3D(0.0,
									 triangles[t].GetVertexA().GetX(),
									 triangles[t].GetVertexA().GetY());
						vB = Point3D(0.0,
									 triangles[t].GetVertexB().GetX(),
									 triangles[t].GetVertexB().GetY());
						vC = Point3D(0.0,
									 triangles[t].GetVertexC().GetX(),
									 triangles[t].GetVertexC().GetY());
						normal = Vector3D(1,0,0);
						break;
					case HeightField3D::Boundary::MIN_Y:
#if defined(VERBOSE_OUTPUT) && !defined(SUPRESS_OUTPUT)
						printf("Generating triangle for MIN_Y axis\n");
						fflush(stdout);
#endif
						//	Scale back up to 3D (setting y=0.0)
						vA = Point3D(triangles[t].GetVertexA().GetX(),
									 0.0,
									 triangles[t].GetVertexA().GetY());
						vB = Point3D(triangles[t].GetVertexB().GetX(),
									 0.0,
									 triangles[t].GetVertexB().GetY());
						vC = Point3D(triangles[t].GetVertexC().GetX(),
									 0.0,
									 triangles[t].GetVertexC().GetY());
						normal = Vector3D(0,1,0);
						break;
					case HeightField3D::Boundary::MIN_Z:
#if defined(VERBOSE_OUTPUT) && !defined(SUPRESS_OUTPUT)
						printf("Generating triangle for MIN_Z axis\n");
						fflush(stdout);
#endif
						//	Scale back up to 3D (setting z=0.0)
						vA = Point3D(triangles[t].GetVertexA().GetX(),
									 triangles[t].GetVertexA().GetY(),
									 0.0);
						vB  = Point3D(triangles[t].GetVertexB().GetX(),
									  triangles[t].GetVertexB().GetY(),
									  0.0);
						vC  = Point3D(triangles[t].GetVertexC().GetX(),
									  triangles[t].GetVertexC().GetY(),
									  0.0);
						normal = Vector3D(0,0,1);
						break;
					case HeightField3D::Boundary::MAX_X:
#if defined(VERBOSE_OUTPUT) && !defined(SUPRESS_OUTPUT)
						printf("Generating triangle for MAX_X axis\n");
						fflush(stdout);
#endif
						//	Scale back up to 3D (setting x=X_MAX)
						vA = Point3D(X_MAX,
									 triangles[t].GetVertexA().GetX(),
									 triangles[t].GetVertexA().GetY());
						vB = Point3D(X_MAX,
									 triangles[t].GetVertexB().GetX(),
									 triangles[t].GetVertexB().GetY());
						vC = Point3D(X_MAX,
									 triangles[t].GetVertexC().GetX(),
									 triangles[t].GetVertexC().GetY());
						normal = Vector3D(-1,0,0);
						break;
					case HeightField3D::Boundary::MAX_Y:
#if defined(VERBOSE_OUTPUT) && !defined(SUPRESS_OUTPUT)
						printf("Generating triangle for MAX_Y axis\n");
						fflush(stdout);
#endif
						//	Scale back up to 3D (setting y=Y_MAX)
						vA = Point3D(triangles[t].GetVertexA().GetX(),
									 Y_MAX,
									 triangles[t].GetVertexA().GetY());
						vB = Point3D(triangles[t].GetVertexB().GetX(),
									 Y_MAX,
									 triangles[t].GetVertexB().GetY());
						vC = Point3D(triangles[t].GetVertexC().GetX(),
									 Y_MAX,
									 triangles[t].GetVertexC().GetY());
						normal = Vector3D(0,-1,0);
						break;
					case HeightField3D::Boundary::MAX_Z:
#if defined(VERBOSE_OUTPUT) && !defined(SUPRESS_OUTPUT)
						printf("Generating triangle for MAX_Z axis\n");
						fflush(stdout);
#endif
						//	Scale back up to 3D (setting z=Z_MAX)
						vA = Point3D(triangles[t].GetVertexA().GetX(),
									 triangles[t].GetVertexA().GetY(),
									 Z_MAX);
						vB  = Point3D(triangles[t].GetVertexB().GetX(),
									  triangles[t].GetVertexB().GetY(),
									  Z_MAX);
						vC  = Point3D(triangles[t].GetVertexC().GetX(),
									  triangles[t].GetVertexC().GetY(),
									  Z_MAX);
						normal = Vector3D(0,0,-1);
						break;
				}

				//	Create a 'packed vertex'
				PackedVertex v0 = {vA, normal};
				PackedVertex v1 = {vB, normal};
				PackedVertex v2 = {vC, normal};

				//	Add the triangle to the mesh
				m_superarc->m_isosurfaceGenerator->AddTriangle(this, v0, v1, v2);

			}
		}
		else
		{
			fprintf(stderr, "Attempted to triangulate a non-closed polygon!");
			fprintf(stderr, "  In function: %s.\n", __func__);
			fflush(stderr);
		}
	}
#undef SUPRESS_OUTPUT
}

inline bool closeEnough(const Real a, const Real b)
{
	Real d = fabs(a - b);
	bool out = d < 0.01;

	return out;
}

///
/// \brief		Performs any post processing required on the mesh
/// \details	Post processing tasks include hole filling
/// \since		17-04-2015
/// \author		Dean
///
void Contour::PostProcess()
{
	//#define SUPRESS_OUTPUT

	//	Grab a list of all edges that bound a hole in the mesh
	vector<MeshEdge3D*> holeEdges;
	for (unsigned long e = 0; e < m_edges.size(); ++e)
	{
		MeshEdge3D *currentEdge = m_edges[e];

		//	Only add the edges marked as being bounded by a single triangle
		if (currentEdge->triangles.size() == 1)
		{
			holeEdges.push_back(currentEdge);
		}
	}

	//	Create a polygon factory to generate the skew polygon(s) for this
	//	contour
	PolygonFactory3 pf3(this->GetSuperarc()->GetContourTree()->GetHeightfield(),
						this);

	//	Process the 'hole edges' and retreive the 3D skew polygons required to
	//	cap the holes
	m_3dCappingPolygons = pf3.ProcessEdges(holeEdges);

	//	Factories to process polygons on each of the 6 faces
	PolygonFactory2 pf2MinX(HeightField3D::Boundary::MIN_X,
							this->GetSuperarc()->GetContourTree()->GetHeightfield(),
							this);
	PolygonFactory2 pf2MaxX(HeightField3D::Boundary::MAX_X,
							this->GetSuperarc()->GetContourTree()->GetHeightfield(),
							this);

	PolygonFactory2 pf2MinY(HeightField3D::Boundary::MIN_Y,
							this->GetSuperarc()->GetContourTree()->GetHeightfield(),
							this);
	PolygonFactory2 pf2MaxY(HeightField3D::Boundary::MAX_Y,
							this->GetSuperarc()->GetContourTree()->GetHeightfield(),
							this);

	PolygonFactory2 pf2MinZ(HeightField3D::Boundary::MIN_Z,
							this->GetSuperarc()->GetContourTree()->GetHeightfield(),
							this);
	PolygonFactory2 pf2MaxZ(HeightField3D::Boundary::MAX_Z,
							this->GetSuperarc()->GetContourTree()->GetHeightfield(),
							this);

	//	The current contour may be capped by any number of distinct skew
	//	polyhedra - work through each of these
	for (unsigned long i = 0; i < m_3dCappingPolygons.size(); ++i)
	{
		//	Obtain a skew polygon
		Polygon3D* polygon3 = m_3dCappingPolygons[i];

		//	Process the edges on each face
		m_cappingPolygonsMinX = pf2MinX.ProcessEdges(polygon3->GetEdges());
		m_cappingPolygonsMaxX = pf2MaxX.ProcessEdges(polygon3->GetEdges());

		m_cappingPolygonsMinY = pf2MinY.ProcessEdges(polygon3->GetEdges());
		m_cappingPolygonsMaxY = pf2MaxY.ProcessEdges(polygon3->GetEdges());

		m_cappingPolygonsMinZ = pf2MinZ.ProcessEdges(polygon3->GetEdges());
		m_cappingPolygonsMaxZ = pf2MaxZ.ProcessEdges(polygon3->GetEdges());
	}

	//	Retreive ownership of the 2D filler polygons for each face
	//	TODO: clear up existing contours in the list
	//m_cappingPolygonsMinX = pf.RetreivePolygonsMinX();
	//m_cappingPolygonsMaxX = pf.RetreivePolygonsMaxX();

	//m_cappingPolygonsMinY = pf.RetreivePolygonsMinY();
	//m_cappingPolygonsMaxY = pf.RetreivePolygonsMaxY();

	//m_cappingPolygonsMinZ = pf.RetreivePolygonsMinZ();
	//m_cappingPolygonsMaxZ = pf.RetreivePolygonsMaxZ();

	triangulateBoundaryPolygons(HeightField3D::Boundary::MIN_X,
								m_cappingPolygonsMinX);
	triangulateBoundaryPolygons(HeightField3D::Boundary::MAX_X,
								m_cappingPolygonsMaxX);
	triangulateBoundaryPolygons(HeightField3D::Boundary::MIN_Y,
								m_cappingPolygonsMinY);
	triangulateBoundaryPolygons(HeightField3D::Boundary::MAX_Y,
								m_cappingPolygonsMaxY);
	triangulateBoundaryPolygons(HeightField3D::Boundary::MIN_Z,
								m_cappingPolygonsMinZ);
	triangulateBoundaryPolygons(HeightField3D::Boundary::MAX_Z,
								m_cappingPolygonsMaxZ);
#undef SUPRESS_OUTPUT
}

#ifdef CALCULATE_MOMENTS
///
/// \brief		Construct an inertia tensor and solve the principles axis
///				of the model
/// \author		Dean
/// \sunce		18-03-2015
///
void Contour::calculateInertia()
{
#define SUPRESS_OUTPUT

	//	Be mindfull of zero volumes - ie DivByZero exceptions

	//	Fill the inertia tensor
	if (fabs(m_m000) != 0.0)
	{
		m_inertiaTensor(0,0) = (m_m200 / m_m000);
		m_inertiaTensor(1,0) = (m_m110);
		m_inertiaTensor(2,0) = (m_m101);
		m_inertiaTensor(0,1) = (m_m110);
		m_inertiaTensor(1,1) = (m_m020);
		m_inertiaTensor(2,1) = (m_m011);
		m_inertiaTensor(0,2) = (m_m101);
		m_inertiaTensor(1,2) = (m_m011);
		m_inertiaTensor(2,2) = (m_m002);
	}
	else
	{
		fprintf(stderr, "Caught division by zero in %s!\n", __func__);
		fflush(stderr);
	}

	//	Solve the eigenvalues and eigenvectors for the inertia tensor
	Eigen::EigenSolver<Matrix3d> solver;
	solver.compute(m_inertiaTensor, true);

	//	Calculate our 3 eigen vectors
	m_principleAxis[0] = EigenVector(solver.eigenvalues()(0).real(),
									 solver.eigenvectors().col(0).real());
	m_principleAxis[1] = EigenVector(solver.eigenvalues()(1).real(),
									 solver.eigenvectors().col(1).real());
	m_principleAxis[2] = EigenVector(solver.eigenvalues()(2).real(),
									 solver.eigenvectors().col(2).real());

	//	Sort the vectors by their eigenvalue magnitudes
	std::sort(m_principleAxis, m_principleAxis + 3);
	std::reverse(m_principleAxis, m_principleAxis + 3);

#if defined(VERBOSE_OUTPUT) && !defined(SUPRESS_OUTPUT)
	std::cout << "Inertia tensor: \n" << m_inertiaTensor << "\n";
	std::cout << "Eigenvector 0: \n" << m_principleAxis[0].ToString() << "\n";
	std::cout << "Eigenvector 1: \n" << m_principleAxis[1].ToString() << "\n";
	std::cout << "Eigenvector 2: \n" << m_principleAxis[2].ToString() << "\n";
#endif
#undef SUPRESS_OUTPUT
}
#endif

///
/// \brief TriangleList::ToString
/// \return
/// \since      04-12-2014
/// \author     Dean
///
string Contour::ToString()
{
	stringstream result;

	result << "TriangleList = { ";

	result << "triangle count = " << m_triangles.size();
	result << ", total area = " << GetSurfaceArea();
	result << ", ";

	//  Listing of triangles
	for (unsigned int i = 0; i < m_triangles.size(); i++)
	{
		//  Return details of each triangle
		//result << m_triangles[i].ToString();

		//  Insert comma (if needed)
		//if (i < m_triangles.size()-1)
		//    result << ", ";
	}

	//  Close brace
	result << " }";

	return result.str();
}
