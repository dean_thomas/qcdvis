#include "MeshTriangle.h"


MeshTriangle3D::MeshTriangle3D(MeshVertex3D *v0, MeshVertex3D *v1, MeshVertex3D *v2,
						   Vector3D *n0, Vector3D *n1, Vector3D *n2,
						   MeshEdge3D *eAB, MeshEdge3D *eAC, MeshEdge3D *eBC)
{
	//	Store lookup to vertices, edges and normals
	m_vA = v0;
	m_vB = v1;
	m_vC = v2;

	m_nA = n0;
	m_nB = n1;
	m_nC = n2;

	//	TODO: we should probably check this matches the vertices above!
	m_eAB = eAB;
	m_eBC = eBC;
	m_eAC = eAC;

	m_unscaledTriangle = Triangle3D(v0->position, v1->position, v2->position);
	m_normals = Triangle3D(*n0, *n1, *n2);
#ifdef CALCULATE_MOMENTS
	calculateMoments();
#endif
}


void MeshTriangle3D::calculateMoments()
{
	//	Zeroth order (volume)
	//	*** THIS MUST BE CALCULATED BEFORE ANYTHING ELSE ***
	//	It is used in all moments below
	m_m000 = calculateM000();

	//	First order (centre of mass)
	m_m100 = calculateM100();
	m_m010 = calculateM010();
	m_m001 = calculateM001();

	//	First order mixed moments
	m_m110 = calculateM110();
	m_m101 = calculateM101();
	m_m011 = calculateM011();
	m_m111 = calculateM111();

	//	Second order (moment of inertia)
	m_m200 = calculateM200();
	m_m020 = calculateM020();
	m_m002 = calculateM002();

	//	Third order (skew)
	m_m300 = calculateM300();
	m_m030 = calculateM030();
	m_m003 = calculateM003();

	//	Forth order (kurtosis)
	m_m400 = calculateM400();
	m_m040 = calculateM040();
	m_m004 = calculateM004();
}

Real MeshTriangle3D::calculateM000() const
{
	const Point3D a = m_unscaledTriangle.GetVertexA();
	const Point3D b = m_unscaledTriangle.GetVertexB();
	const Point3D c = m_unscaledTriangle.GetVertexC();

	//  Construct tetrahedron with a common origin at (0,0,0)
	return (1.0 / 6.0) * (- (a.GetX()*b.GetY()*c.GetZ())
						  + (b.GetX()*a.GetY()*c.GetZ())
						  + (a.GetX()*c.GetY()*b.GetZ())
						  - (c.GetX()*a.GetY()*b.GetZ())
						  - (b.GetX()*c.GetY()*a.GetZ())
						  + (c.GetX()*b.GetY()*a.GetZ()));
}

Real MeshTriangle3D::calculateM100() const
{
	const Real x1 = m_unscaledTriangle.GetVertexA().GetX();
	const Real x2 = m_unscaledTriangle.GetVertexB().GetX();
	const Real x3 = m_unscaledTriangle.GetVertexC().GetX();

	return 0.25 * (x1 + x2 + x3) * m_m000;
}

Real MeshTriangle3D::calculateM010() const
{
	const Real y1 = m_unscaledTriangle.GetVertexA().GetY();
	const Real y2 = m_unscaledTriangle.GetVertexB().GetY();
	const Real y3 = m_unscaledTriangle.GetVertexC().GetY();

	return (0.25 * (y1 + y2 + y3) * m_m000);
}

Real MeshTriangle3D::calculateM001() const
{
	const Real z1 = m_unscaledTriangle.GetVertexA().GetZ();
	const Real z2 = m_unscaledTriangle.GetVertexB().GetZ();
	const Real z3 = m_unscaledTriangle.GetVertexC().GetZ();

	return (0.25 * (z1 + z2 + z3) * m_m000);
}

Real MeshTriangle3D::calculateM110() const
{
	const Real x1 = m_unscaledTriangle.GetVertexA().GetX();
	const Real x2 = m_unscaledTriangle.GetVertexB().GetX();
	const Real x3 = m_unscaledTriangle.GetVertexC().GetX();

	const Real y1 = m_unscaledTriangle.GetVertexA().GetY();
	const Real y2 = m_unscaledTriangle.GetVertexB().GetY();
	const Real y3 = m_unscaledTriangle.GetVertexC().GetY();

	return ((1.0 / 20.0) * ((x1 * ((2.0 * y1) + y2 + y3))
							+ (x2 * (y1 + (2.0 * y2) + y3))
							+ (x3 * (y1 + y2 + (2.0 * y3))))
			* m_m000);
}

Real MeshTriangle3D::calculateM101() const
{
	const Real x1 = m_unscaledTriangle.GetVertexA().GetX();
	const Real x2 = m_unscaledTriangle.GetVertexB().GetX();
	const Real x3 = m_unscaledTriangle.GetVertexC().GetX();

	const Real z1 = m_unscaledTriangle.GetVertexA().GetZ();
	const Real z2 = m_unscaledTriangle.GetVertexB().GetZ();
	const Real z3 = m_unscaledTriangle.GetVertexC().GetZ();

	return ((1.0 / 20.0) * ((x1 * ((2.0 * z1) + z2 + z3))
							+ (x2 * (z1 + (2.0 * z2) + z3))
							+ (x3 * (z1 + z2 + (2.0 * z3))))
			* m_m000);
}

Real MeshTriangle3D::calculateM011() const
{
	const Real y1 = m_unscaledTriangle.GetVertexA().GetY();
	const Real y2 = m_unscaledTriangle.GetVertexB().GetY();
	const Real y3 = m_unscaledTriangle.GetVertexC().GetY();

	const Real z1 = m_unscaledTriangle.GetVertexA().GetZ();
	const Real z2 = m_unscaledTriangle.GetVertexB().GetZ();
	const Real z3 = m_unscaledTriangle.GetVertexC().GetZ();

	return ((1.0 / 20.0) * ((y1 * ((2.0 * z1) + z2 + z3))
							+ (y2 * (z1 + (2.0 * z2) + z3))
							+ (y3 * (z1 + z2 + (2.0 * z3))))
			* m_m000);
}

Real MeshTriangle3D::calculateM111() const
{
	const Real x1 = m_unscaledTriangle.GetVertexA().GetX();
	const Real x2 = m_unscaledTriangle.GetVertexB().GetX();
	const Real x3 = m_unscaledTriangle.GetVertexC().GetX();

	const Real y1 = m_unscaledTriangle.GetVertexA().GetY();
	const Real y2 = m_unscaledTriangle.GetVertexB().GetY();
	const Real y3 = m_unscaledTriangle.GetVertexC().GetY();

	const Real z1 = m_unscaledTriangle.GetVertexA().GetZ();
	const Real z2 = m_unscaledTriangle.GetVertexB().GetZ();
	const Real z3 = m_unscaledTriangle.GetVertexC().GetZ();

	const Real _2y1 = 2.0 * y1;
	const Real _2y2 = 2.0 * y2;
	const Real _2y3 = 2.0 * y3;

	const Real _2z1 = 2.0 * z1;
	const Real _2z2 = 2.0 * z2;
	const Real _2z3 = 2.0 * z3;

	const Real _3z1 = 3.0 * z1;
	const Real _3z2 = 3.0 * z2;
	const Real _3z3 = 3.0 * z3;

	return ((1.0 / 120.0) * ((x1 * (_2y1 * (_3z1 + z2 + z3)
									+ (y2 * (_2z1 + _2z2 + z3))
									+ (y3 * (_2z1 + z2 + _2z3))))
							 + (x2 * (y1 * (_2z1 + _2z2 + z3)
									  + (_2y2 * (z1 + _3z2 + z3))
									  + (y3 * (z1 + _2z2 + _2z3))))
							 + (x3 * (y1 * (_2z1 + z2 + _2z3)
									  + (y2 * (z1 + _2z2 + _2z3))
									  + (_2y3 * (z1 + z2 + _3z3)))))
			* m_m000);
}


Real MeshTriangle3D::calculateM200() const
{
	const Real x1 = m_unscaledTriangle.GetVertexA().GetX();
	const Real x2 = m_unscaledTriangle.GetVertexB().GetX();
	const Real x3 = m_unscaledTriangle.GetVertexC().GetX();

	return (0.1 * ((x1*x1) + (x2*x2) + (x3*x3) + (x1*x2) + (x2*x3) + (x1*x3))
			* m_m000);
}

Real MeshTriangle3D::calculateM020() const
{
	const Real y1 = m_unscaledTriangle.GetVertexA().GetY();
	const Real y2 = m_unscaledTriangle.GetVertexB().GetY();
	const Real y3 = m_unscaledTriangle.GetVertexC().GetY();

	return (0.1 * ((y1*y1) + (y2*y2) + (y3*y3) + (y1*y2) + (y2*y3) + (y1*y3))
			* m_m000);
}


Real MeshTriangle3D::calculateM002() const
{
	const Real z1 = m_unscaledTriangle.GetVertexA().GetZ();
	const Real z2 = m_unscaledTriangle.GetVertexB().GetZ();
	const Real z3 = m_unscaledTriangle.GetVertexC().GetZ();

	return (0.1 * ((z1*z1) + (z2*z2) + (z3*z3) + (z1*z2) + (z2*z3) + (z1*z3))
			* m_m000);
}

Real MeshTriangle3D::calculateM300() const
{
	const Real x1 = m_unscaledTriangle.GetVertexA().GetX();
	const Real x2 = m_unscaledTriangle.GetVertexB().GetX();
	const Real x3 = m_unscaledTriangle.GetVertexC().GetX();

	return (0.05 * ((x1*x1*x1) + (x2*x2*x2) + (x3*x3*x3)
					+ ((x1*x1)*(x2+x3)) + ((x2*x2)*(x1+x3))
					+ ((x3*x3)*(x1+x2)) + (x1*x2*x3)) * m_m000);
}

Real MeshTriangle3D::calculateM030() const
{
	const Real y1 = m_unscaledTriangle.GetVertexA().GetY();
	const Real y2 = m_unscaledTriangle.GetVertexB().GetY();
	const Real y3 = m_unscaledTriangle.GetVertexC().GetY();

	return (0.05 * ((y1*y1*y1) + (y2*y2*y2) + (y3*y3*y3)
					+ ((y1*y1)*(y2+y3)) + ((y2*y2)*(y1+y3))
					+ ((y3*y3)*(y1+y2)) + (y1*y2*y3)) * m_m000);
}


Real MeshTriangle3D::calculateM003() const
{
	const Real z1 = m_unscaledTriangle.GetVertexA().GetZ();
	const Real z2 = m_unscaledTriangle.GetVertexB().GetZ();
	const Real z3 = m_unscaledTriangle.GetVertexC().GetZ();

	return (0.05 * ((z1*z1*z1) + (z2*z2*z2) + (z3*z3*z3)
					+ ((z1*z1)*(z2+z3)) + ((z2*z2)*(z1+z3))
					+ ((z3*z3)*(z1+z2)) + (z1*z2*z3)) * m_m000);
}

Real MeshTriangle3D::calculateM400() const
{
	const Real x1 = m_unscaledTriangle.GetVertexA().GetX();
	const Real x2 = m_unscaledTriangle.GetVertexB().GetX();
	const Real x3 = m_unscaledTriangle.GetVertexC().GetX();

	return (0.0);
}

Real MeshTriangle3D::calculateM040() const
{
	const Real y1 = m_unscaledTriangle.GetVertexA().GetY();
	const Real y2 = m_unscaledTriangle.GetVertexB().GetY();
	const Real y3 = m_unscaledTriangle.GetVertexC().GetY();

	return (0.0);
}


Real MeshTriangle3D::calculateM004() const
{
	const Real z1 = m_unscaledTriangle.GetVertexA().GetZ();
	const Real z2 = m_unscaledTriangle.GetVertexB().GetZ();
	const Real z3 = m_unscaledTriangle.GetVertexC().GetZ();

	return (0.0);
}
