#include "MeshCache.h"

using std::cout;
using std::cerr;
using std::endl;

bool MeshCache::ContainsMeshWithIsovalue(const Real &isovalue) const
{
	MeshCacheItem* contour = FindCacheItemWithIsovalue(isovalue);

	return (contour != nullptr);
}

MeshCacheItem* MeshCache::QueueMesh(const Real& isovalue)
{
#define MESH_FLUSH_SIZE 10

	//	Check it doesn't already exist
	if (!ContainsMeshWithIsovalue(isovalue))
	{
		//	Make sure we don't exceed the cache
		if (m_items.size() < MAX_CACHE_SIZE)
		{
			MeshCacheItem *item = new MeshCacheItem(isovalue);
			m_items.push_back(item);
			return item;
		}
		else
		{
			//	Fine-tune this..
			cerr << "Exceeded mesh cache size.  In function: " << __func__;
			cerr << "; File: " << __FILE__ << " line: " << __LINE__;
			cerr << endl;

			m_items.erase(m_items.begin(), m_items.begin() + MESH_FLUSH_SIZE);

			MeshCacheItem *item = new MeshCacheItem(isovalue);
			m_items.push_back(item);
			return item;
		}
	}
	else
	{
		cerr << "Attempted to queue a mesh that already exists!" << endl;

		return nullptr;
	}
}

MeshCacheItem* MeshCache::FindCacheItemWithIsovalue(const Real &isovalue) const
{
	for (unsigned long i = 0; i < m_items.size(); ++i)
	{
		if (m_items[i]->isovalue == isovalue)
			return m_items[i];
	}
	return nullptr;
}

MeshCache::MeshCache()
{
	m_items.reserve(MAX_CACHE_SIZE);
}

MeshCache::~MeshCache()
{
	//BUG:	fix these random deallocations!
	while (!m_items.empty())
	{
		delete m_items.back();
		m_items.pop_back();
	}
}

