#ifndef MESHVERTEX_H
#define MESHVERTEX_H

#include "Globals/TypeDefines.h"

class MeshTriangle3D;
class MeshEdge3D;

struct MeshVertex3D
{
	Point3D position;
	vector<MeshTriangle3D*> triangles;
	vector<MeshEdge3D*> edges;

	MeshVertex3D(const Point3D& position)
		: position{position}
	{

	}
};

struct MeshEdge3D
{
		MeshVertex3D *vA;
		MeshVertex3D *vB;
		vector<MeshTriangle3D*> triangles;

		MeshEdge3D(MeshVertex3D *vA, MeshVertex3D *vB)
			: vA{vA}, vB{vB}
		{

		}

		Edge3D ToEdge3D() const
		{
			return Edge3D(vA->position, vB->position);
		}
};

#endif // MESHVERTEX_H
