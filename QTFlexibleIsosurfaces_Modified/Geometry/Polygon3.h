#ifndef POLYGON3_H
#define POLYGON3_H

#include <string>
#include <sstream>
#include <cmath>
#include "Vector3.h"
#include "LineSegment3.h"
#include <vector>
#include "VertexTriple.h"
#include <algorithm>

namespace Geometry
{
	template <typename T>
	class Polygon3
	{

		private:
			typedef Vector3<T> Vector3D;

			typedef Vector3<T> Point3D;
			typedef vector<Point3D> VertexList;

			typedef LineSegment3<T> Edge3D;
			typedef vector<Edge3D> EdgeList;

			VertexList m_vertices;
			EdgeList m_edges;

			T m_perimeter;

		public:
			vector<Edge3D> GetEdges() const { return m_edges; }

			Polygon3()
			{

			}

			~Polygon3()
			{

			}

			Polygon3(const Polygon3& rhs)
			{
				//	Copy vertices
				m_vertices.reserve(rhs.m_vertices.size());
				for (unsigned int v = 0; v < rhs.m_vertices.size(); ++v)
				{
					m_vertices.push_back(rhs.m_vertices[v]);
				}

				//	Copy edges
				m_edges.reserve(rhs.m_edges.size());
				for (unsigned int e = 0; e < rhs.m_edges.size(); ++e)
				{
					m_edges.push_back(rhs.m_edges[e]);
				}
			}

			unsigned long GetEdgeCount() const
			{
				return m_edges.size();
			}

			bool IsClosed() const
			{
				//	We can have a closed polygon with less than 3 sides
				if (m_edges.size() < 3)
					return false;

				if (m_edges[0].GetVertexA() ==
					m_edges[m_edges.size()-1].GetVertexB())
				{
					return true;
				}
				else
				{
					return false;
				}
			}

			void updateVertexList(const Edge3D edge)
			{

			}

			void AddEdgeToFront(const Edge3D edge)
			{
				m_edges.insert(m_edges.begin(), edge);

				updateVertexList(edge);

				Vector3D v1 = edge.GetVertexA();
				Vector3D v2 = edge.GetVertexB();
				bool v1add = true;
				bool v2add = true;

				//	See if we need to add the vertices
				for (unsigned i = 0; i < m_vertices.size(); ++i)
				{
					//	See if vertex v1 is already in the list
					if (m_vertices[i] == v1)
						v1add = false;

					//	See if vertex v2 is already in the list
					if (m_vertices[i] == v2)
						v2add = false;
				}

				//	Add the vertices, if they are already listed
				if (v1add)
					m_vertices.insert(m_vertices.begin(), v1);
				if (v2add)
					m_vertices.insert(m_vertices.begin(), v2);
			}

			void AddEdgeToBack(const Edge3D edge)
			{
				m_edges.push_back(edge);

				Vector3D v1 = edge.GetVertexA();
				Vector3D v2 = edge.GetVertexB();
				bool v1add = true;
				bool v2add = true;

				//	See if we need to add the vertices
				for (unsigned i = 0; i < m_vertices.size(); ++i)
				{
					//	See if vertex v1 is already in the list
					if (m_vertices[i] == v1)
						v1add = false;

					//	See if vertex v2 is already in the list
					if (m_vertices[i] == v2)
						v2add = false;
				}

				//	Add the vertices, if they are already listed
				if (v1add)
					m_vertices.push_back(v1);
				if (v2add)
					m_vertices.push_back(v2);
			}

			Edge3D GetFirstEdge() const
			{
				return m_edges[0];
			}

			Edge3D GetLastEdge() const
			{
				return m_edges[m_edges.size()-1];
			}

			///
			/// \brief		Removes the first edge and returns the edge
			/// \return
			/// \since		28-05-2015
			/// \author		Dean
			///
			Edge3D TakeFirstEdge()
			{
				Edge3D result = m_edges[0];
				m_edges.erase(m_edges.begin());

				return result;
			}

			string ToString() const
			{
				stringstream result;

				result << "Polygon3 = { ";

				//  Length's
				//result << "vertex count = " << m_vertices.size() << ", ";
				result << "edge count = " << m_edges.size() << ", ";
				result << "is closed = " << (IsClosed() ?"true":"false") << ", ";
				//				result << "signed area = " << GetSignedArea() << ", ";

				//  Angles
				//  ...

				//  Area
				//	...

				//  Vertex listing
				result << "edges = { ";
				for (unsigned long i = 0; i < m_edges.size(); ++i)
				{
					result << m_edges[i].ToString();

					if (i < m_edges.size() - 1)
					{
						result << ", ";
					}
				}
				result << " }";

				//  Closing brace
				result << " }";

				return result.str();
			}

			bool operator ==(const Polygon3<T> &rhs)
			{
				printf("Testing polygons for equality.\n");
				fflush(stdout);

				//	Easy tests for failure
				if (m_edges.size() != rhs.m_edges.size()) return false;
				if (m_vertices.size() != rhs.m_vertices.size()) return false;

				if (m_vertices.size() > 0)
				{
					printf("%#llx\n",m_vertices[0].GetMortonNumber());
					fflush(stdout);

				}

				//	Exact match of ordering required (but possibly reversed
				for (unsigned long e = 0; e < m_edges.size(); ++e)
				{
					//	Can exist backwards
					if (m_edges[e].IsCongruentTo(rhs.m_edges[e]))
						return false;
				}
			}

			///
			/// \brief IsCongruentTo
			/// \param rhs
			/// \return
			///
			bool IsCongruentTo(const Polygon3<T> &rhs)
			{
				//	Easy tests for failure
				if (m_edges.size() != rhs.m_edges.size()) return false;
				if (m_vertices.size() != rhs.m_vertices.size()) return false;

				//	List of hashed vertices for both polygons
				vector<int64_t> lhsMortonNumbers;
				vector<int64_t> rhsMortonNumbers;

				//	We already know how many values
				lhsMortonNumbers.reserve(m_vertices.size());
				rhsMortonNumbers.reserve(m_vertices.size());

				//	Convert each vertex and add to the two vectors
				for (unsigned i = 0; i < m_vertices.size(); ++i)
				{
					lhsMortonNumbers.push_back(m_vertices[i].GetMortonNumber());
					rhsMortonNumbers.push_back(rhs.m_vertices[i].GetMortonNumber());
				}

				//	Vertex order is not important, so sort them...
				using std::sort;
				sort(lhsMortonNumbers.begin(), lhsMortonNumbers.end());
				sort(rhsMortonNumbers.begin(), rhsMortonNumbers.end());

				//	...allowing us to do a linear search for mismatches
				for (unsigned i = 0; i < m_vertices.size(); ++i)
				{
					if (lhsMortonNumbers[i] != rhsMortonNumbers[i])
						return false;
				}
				return true;
			}
	};
}

#endif // POLYGON_H
