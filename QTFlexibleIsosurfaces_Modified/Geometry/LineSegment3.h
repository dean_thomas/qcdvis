#ifndef LINESEGMENT3_H
#define LINESEGMENT3_H

#include <string>
#include <sstream>
#include <cmath>
#include "Globals/TypeDefines.h"
#include "Vector2.h"
#include "Vector3.h"
#include <vector>
#include "VertexTriple.h"
#include "LineSegment2.h"

using std::string;
using std::stringstream;
using std::vector;

namespace Geometry
{
	template <typename T>
	class LineSegment3
	{
		private:
			typedef Vector3<T> Point3D;
			typedef Vector3<T> Vector3D;
			typedef Vector2<T> Point2D;

			Point3D m_vertexA;
			Point3D m_vertexB;

			T m_length;
		public:
			LineSegment3()
			{

			}

			LineSegment3(const Point3D vertexA, const Point3D vertexB)
			{
				m_vertexA = vertexA;
				m_vertexB = vertexB;

				calculateStats();
			}

			void calculateStats()
			{
				//Vector3D displacement = m_vertexA + m_vertexB;

				//m_length = displacement.GetMagnitude();
			}

		public:
			Point3D GetVertexA() const { return m_vertexA; }
			Point3D GetVertexB() const { return m_vertexB; }

			T GetLength() const { return m_length; }

			string ToString(const unsigned int decimalPlaces = 4) const
			{
				stringstream result;

				result << "LineSegment = { ";
				result << m_vertexA.ToString(decimalPlaces);
				result << ", ";
				result << m_vertexB.ToString(decimalPlaces);
				result << " }\n";

				return result.str();
			}

			bool operator == (const LineSegment3<T> rhs) const
			{
				return ((m_vertexA == rhs.m_vertexA)
						&& (m_vertexB == rhs.m_vertexB));
			}

			bool operator != (const LineSegment3<T> rhs) const
			{
				return !((m_vertexA == rhs.m_vertexA)
						&& (m_vertexB == rhs.m_vertexB));
			}

			///	Returns a copy of this edge but with the vertex order swapped.
			LineSegment3<T> GetReversedForm() const
			{
				return LineSegment3<T>(m_vertexB, m_vertexA);
			}

			bool IsCongruentTo(const LineSegment3<T> &rhs) const
			{
				const LineSegment3<T> lhs = *this;
				const LineSegment3<T> lhsR = lhs.GetReversedForm();

				//	Direct match
				if (lhs == rhs) return true;

				//	Reverse match
				if (lhsR == rhs) return true;

				//	Not a match
				return false;
			}

			///
			///		\brief	Check if the specified vertex is part of this edge
			///		\param vertex
			///		\return
			///		\since	20-04-2015
			///		\author	Dean
			///
			bool IsIncidentTo(const Point3D vertex) const
			{
				if ((m_vertexA == vertex) || (m_vertexB == vertex))
				{
					return true;
				}
				else
				{
					return false;
				}
			}

			///
			/// \brief		Downsize to a 2 dimensions by discarding
			///				the X coordinate of vertices
			/// \since		27-04-2015
			/// \author		Dean
			/// \return
			///
			LineSegment2<T> YZ() const
			{
				Point2D vA = m_vertexA.YZ();
				Point2D vB = m_vertexB.YZ();

				return LineSegment2<T>(vA, vB);
			}

			///
			/// \brief		Downsize to a 2 dimensions by discarding
			///				the Y coordinate of vertices
			/// \since		27-04-2015
			/// \author		Dean
			/// \return
			///
			LineSegment2<T> XZ() const
			{
				Point2D vA = m_vertexA.XZ();
				Point2D vB = m_vertexB.XZ();

				return LineSegment2<T>(vA, vB);
			}

			///
			/// \brief		Downsize to a 2 dimensions by discarding
			///				the Z coordinate of vertices
			/// \since		27-04-2015
			/// \author		Dean
			/// \return
			///
			LineSegment2<T> XY() const
			{
				Point2D vA = m_vertexA.XY();
				Point2D vB = m_vertexB.XY();

				return LineSegment2<T>(vA, vB);
			}

	};
}

#endif // LINESEGMENT_H
