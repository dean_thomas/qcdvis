#pragma once
#ifndef STL_OSTREAM_H
#define STL_OSTREAM_H

#include <ostream>
#include <string>
#include <map>
#include <typeinfo>
#include <type_traits>

////////////////////////////////////////////////////////////////////////////////
////							Helper functions							////
////////////////////////////////////////////////////////////////////////////////

//	Make it (slightly) easier to read the return type
template<typename T, typename R = void>
using enable_if_not_pointer = typename std::enable_if<!std::is_pointer<T>::value, R>::type;

///
///	\brief		If the type T is not a pointer print it
///				directly to the ostream
///	\author		Dean
///	\since		10-02-2016
///
template <typename T>
enable_if_not_pointer<T, std::ostream&> printElem(std::ostream& os, const T& elem)
{
	os << elem;
	return os;
}

///
///	\brief		If the type T is a pointer we dereference
///	\author		Dean
///	\since		10-02-2016
///
template <typename T>
std::ostream& printElem(std::ostream& os, T* const& elem)
{
	if (elem != nullptr)
	{
		os << "ptr(";
		printElem(os, *elem);
		os << ")";
	}
	else
	{
		os << "nullptr";
	}
	return os;
}

////////////////////////////////////////////////////////////////////////////////
////							std::vector									////
////////////////////////////////////////////////////////////////////////////////

///
///	\brief		Provide an ostream operator for vectors
///	\author		Dean
///	\since		10-02-2016
///
template <typename T>
std::ostream& operator<<(std::ostream& os, const std::vector<T>& vec)
{
	const std::string DELIMITER = ", ";

	//	Header (prints the vector with the template type)
	os << "std::vector<" << typeid(T).name() << "> = { ";

	//	Overview of the data
	os << "size: " << vec.size();

	//	List each element (dereferencing until we reach a non-pointer
	//	as required)
	os << ", elements: [ ";
	for (auto it = vec.cbegin(); it != vec.cend(); ++it)
	{
		//	Delimiter (only after the first element)
		if (it != vec.cbegin()) os << DELIMITER;

		//	print the element
		printElem(os, *it);
	}
	os << " ]";

	//	Close output
	os << " }";

	return os;
}

////////////////////////////////////////////////////////////////////////////////
////							std::map									////
////////////////////////////////////////////////////////////////////////////////

///
///	\brief		Provide an ostream operator for maps
///	\author		Dean
///	\since		10-02-2016
///
template <typename K, typename V>
std::ostream& operator<<(std::ostream& os, const std::map<K, V>& map)
{
	const std::string DELIMITER = ", ";

	//	Header (prints the vector with the template type)
	os << "std::map<" << typeid(K).name()
		<< ", " << typeid(V).name() << "> = { ";

	//	Overview of the data
	os << "size: " << map.size();

	//	List each element (dereferencing until we reach a non-pointer
	//	as required)
	os << ", elements: [ ";
	for (auto it = map.cbegin(); it != map.cend(); ++it)
	{
		//	Delimiter (only after the first element)
		if (it != map.cbegin()) os << DELIMITER;

		//	print the key-value pair
		os << "{ ";
		printElem(os, it->first);
		os << " : ";
		printElem(os, it->second);
		os << "}";
	}
	os << " ]";

	//	Close output
	os << " }";

	return os;
}

#endif
