#ifndef FUNCTIONS_H
#define FUNCTIONS_H

#include <cassert>
#include <iostream>
#include <fstream>

///
/// \brief		Takes an un-normalized isovalue and normalizes it
/// \param		unNormalized
/// \return		float: range 0..1
/// \since		05-08-2015
/// \author		Dean
/// \author		Dean: rewrite as generic function [26-11-2015]
///
template <typename T>
inline T normalize(const T& min, const T& max, const T& unNormalized)
{
	assert(min <= max);

	T result;
	T range = max - min;

	//	Protect againt div by zero errors
	if (range != (T)0)
	{
		result = (unNormalized - min) / range;
	}
	else
	{
		range = (T)0;
	}

	std::cout << "un-normalized value: " << unNormalized << " "
			  << "normalized: " << result << std::endl;

	assert((result >= (T)0) && (result <= (T)1));

	return result;
}

inline bool fileExists(const std::string& filename)
{
	std::ifstream file(filename);

	//	Destructor of ifstread will close the file in destructor
	return file.good();
}

#endif // FUNCTIONS

