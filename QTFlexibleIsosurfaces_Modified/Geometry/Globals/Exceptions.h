#ifndef EXCEPTIONS
#define EXCEPTIONS


#include <stdexcept>
#include <exception>
#include <string>
#include <sstream>
#include <cstring>


using std::string;
using std::stringstream;

///
/// \brief      The InvalidCoordinateException class
/// \details    Provides a way of capturing the passing of invalid parameters
///             from outside the Array3D class
/// \since      10-12-2014
/// \author     Dean
///
class NullPtrException : public std::exception
{
	private:
		string errorMessage;

	public:
		///
		/// \brief  Constructor for an exception raised by invalid x, y, z indices
		/// \param  x:  The x value passed to the generating function
		/// \param  y:  The y value passed to the generating function
		/// \param  z:  The z value passed to the generating function
		/// \author Dean
		/// \since  10-12-2014
		///
		NullPtrException(const char *function,
						 const char *file,
						 const unsigned int line)
		{
			stringstream ss;

			ss << "ERROR!  The object accessed in function ";
			ss << function << " in null (file: " << file;
			ss << " line: " << line << ").";

			errorMessage = ss.str().c_str();
		}

		virtual const char* what() const throw()
		{
			return errorMessage.c_str();
		}
};

///
/// \brief      The InvalidIndexException class
/// \details    Provides a way of capturing the passing of invalid parameters
///             from outside the Array3D class
/// \since      10-12-2014
/// \author     Dean
///
class InvalidIndexException : public std::exception
{
	private:
		string errorMessage;

	public:
		InvalidIndexException(const unsigned long index, const char* function,
							  const char* file, const unsigned int line)
		{
			stringstream ss;

			ss << "ERROR!  The index < " << index;
			ss << " is invalid!  In function: ";
			ss << function << " (file: " << file;
			ss << " line: " << line << ").";

			errorMessage = ss.str().c_str();
		}

		virtual const char* what() const throw()
		{
			return errorMessage.c_str();
		}
};

///
/// \brief      The InvalidCoordinateException class
/// \details    Provides a way of capturing the passing of invalid parameters
///             from outside the Array3D class
/// \since      10-12-2014
/// \author     Dean
///
class InvalidCoordinateException : public std::exception
{
	private:
		string errorMessage;

	public:
		///
		/// \brief  Constructor for an exception raised by invalid x, y, z indices
		/// \param  x:  The x value passed to the generating function
		/// \param  y:  The y value passed to the generating function
		/// \param  z:  The z value passed to the generating function
		/// \author Dean
		/// \since  10-12-2014
		///
		InvalidCoordinateException(const unsigned long x,
								   const unsigned long y,
								   const unsigned long z,
								   const char *function,
								   const char *file,
								   const unsigned int line)
		{
			stringstream ss;
			ss << "ERROR!  Coordinate <" << x << "," << y << "," << z << ">";
			ss << " is invalid!  In function: ";
			ss << function << " (file: " << file;
			ss << " line: " << line << ").";

			errorMessage = ss.str().c_str();
		}

		virtual const char* what() const throw()
		{
			return errorMessage.c_str();
		}
};

///
/// \brief
/// \details    Provides a way of capturing the passing of invalid isovalues to
///				a isosurface generator
/// \since      11-05-2015
/// \author     Dean
///
class InvalidIsovalueException : public std::exception
{
	private:
		string errorMessage;

	public:
		InvalidIsovalueException(const unsigned long &arcID,
								 const float &badIsovalue,
								 const float &minIsovalue,
								 const float &maxIsovalue,
								 const char* function,
								 const char* file,
								 const unsigned int line)
		{
			stringstream ss;

			ss << "ERROR!  The isovalue " << badIsovalue;
			ss << " is invalid for the superarc with ID = " << arcID << "!";
			ss << "  Allowable range is " << minIsovalue << " - ";
			ss   << maxIsovalue << ".";
			ss << "  In function: " << function << " (file: " << file;
			ss << " line: " << line << ").";

			errorMessage = ss.str();
		}

		virtual const char* what() const throw()
		{
			return errorMessage.c_str();
		}
};

#endif // EXCEPTIONS

