#ifndef SIMPLEVECTOR3_H
#define SIMPLEVECTOR3_H

#include <string>
#include <sstream>
#include <cmath>

using std::string;
using std::stringstream;

template <class T>
class SimpleVector3
{
private:
    T m_x;
    T m_y;
    T m_z;
public:
    ///
    /// \brief      Constructor
    /// \since      04-12-2014
    /// \author     Dean
    ///
    SimpleVector3(const T x, const T y, const T z)
    {
        m_x = x;
        m_y = y;
        m_z = z;
    }

    ///
    /// \brief      Constructor
    /// \since      04-12-2014
    /// \author     Dean
    ///
    SimpleVector3()
    {
        m_x = T();
        m_y = T();
        m_z = T();
    }

    T GetX() const { return m_x; }
    T GetY() const { return m_y; }
    T GetZ() const { return m_z; }

    void SetX(const T x) { m_x = x; }
    void SetY(const T y) { m_y = y; }
    void SetZ(const T z) { m_z = z; }

    ///
    /// \brief Normalize
    /// \return
    /// \author Dean
    /// \since  08-12-2014
    ///
    SimpleVector3 GetNormal() const
    {
        T factor = 1.0 / GetMagnitude();

        return SimpleVector3(m_x * factor, m_y * factor, m_z * factor);
    }

    ///
    /// \brief GetMagnitude
    /// \return
    /// \author Dean
    /// \since  08-12-2014
    ///
    T GetMagnitude() const
    {
        return sqrt((m_x * m_x) + (m_y * m_y) + (m_z * m_z));
    }

    ///
    /// \brief Point3D::ToString
    /// \return
    /// \since      04-12-2014
    /// \author     Dean
    ///
    string ToString() const
    {
        stringstream result;

        result << "SimpleVector3 = { ";
        result << "x = " << m_x;
        result << ", ";
        result << "y = " << m_y;
        result << ", ";
        result << "z = " << m_z;
        result << " }";

        return result.str();
    }

};

#endif // SIMPLEVECTOR3_H
