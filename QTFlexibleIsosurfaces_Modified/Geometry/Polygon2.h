#ifndef POLYGON2_H
#define POLYGON2_H

#include "Vector2.h"
#include "LineSegment3.h"
#include "VertexTriple.h"
#include "Triangle2.h"

#include <algorithm>
#include <string>
#include <sstream>
#include <cmath>
#include <vector>
#include <iostream>

#ifndef __func__
#define __func__ __FUNCTION__
#endif

namespace Geometry
{
	static const float EPSILON=0.0000000001f;

	template <typename T>
	class Polygon2
	{
		private:
			typedef Vector2<T> Vector2D;

			typedef Vector2<T> Point2D;
			typedef vector<Point2D> VertexList;

			typedef LineSegment2<T> Edge2D;
			typedef vector<Edge2D> EdgeList;

			VertexList m_vertices;
			EdgeList m_edges;

			T m_perimeter;

		public:
			vector<Edge2D> GetEdges() const { return m_edges; }


			Polygon2()
			{

			}

			~Polygon2()
			{

			}

			Polygon2(const Polygon2& rhs, const bool reverseWinding = false)
			{
				vector<Edge2D> edges = rhs.GetEdges();
				for (unsigned long e = 0; e < edges.size(); ++e)
				{
					if (reverseWinding)
					{
						AddEdgeToBack(edges[edges.size()-1-e].GetReversedForm());
					}
					else
					{
						AddEdgeToFront(edges[e]);
					}
				}
			}

			unsigned long GetEdgeCount() const
			{
				return m_edges.size();
			}

			bool IsClosed() const
			{
				//	We can have a closed polygon with less than 3 sides
				if (m_edges.size() < 3)
					return false;

				if (m_edges[0].GetVertexA() ==
					m_edges[m_edges.size()-1].GetVertexB())
				{
					return true;
				}
				else
				{
					return false;
				}
			}

			void updateVertexList(const Edge2D edge)
			{

			}

			void AddEdgeToFront(const Edge2D edge)
			{
				m_edges.insert(m_edges.begin(), edge);

				updateVertexList(edge);

				Vector2D v1 = edge.GetVertexA();
				Vector2D v2 = edge.GetVertexB();
				bool v1add = true;
				bool v2add = true;

				//	See if we need to add the vertices
				for (unsigned i = 0; i < m_vertices.size(); ++i)
				{
					//	See if vertex v1 is already in the list
					if (m_vertices[i] == v1)
						v1add = false;

					//	See if vertex v2 is already in the list
					if (m_vertices[i] == v2)
						v2add = false;
				}

				//	Add the vertices, if they are already listed
				if (v1add)
					m_vertices.insert(m_vertices.begin(), v1);
				if (v2add)
					m_vertices.insert(m_vertices.begin(), v2);
			}

			void AddEdgeToBack(const Edge2D &edge)
			{
				m_edges.push_back(edge);

				Vector2D v1 = edge.GetVertexA();
				Vector2D v2 = edge.GetVertexB();
				bool v1add = true;
				bool v2add = true;

				//	See if we need to add the vertices
				for (unsigned i = 0; i < m_vertices.size(); ++i)
				{
					//	See if vertex v1 is already in the list
					if (m_vertices[i] == v1)
						v1add = false;

					//	See if vertex v2 is already in the list
					if (m_vertices[i] == v2)
						v2add = false;
				}

				//	Add the vertices, if they are already listed
				if (v1add)
					m_vertices.push_back(v1);
				if (v2add)
					m_vertices.push_back(v2);
			}

			///
			/// \brief		Adds two open polygons, providing their edges line
			///				up.
			/// \param		rhs
			/// \return		boolean: true if the merge was sucessful
			/// \since		02-06-2015
			/// \author		Dean
			///
			bool AddPolylineToBack(Polygon2<T> *rhs)
			{
				using std::cout;
				using std::cerr;
				using std::endl;

				if (IsClosed() || rhs->IsClosed())
				{
					cerr << "Attempting to add closed polygons!";
					cerr <<	"  In function " << __func__ << "file: ";
					cerr << __FILE__ << ", line " << __LINE__ << ")";
					cerr << endl;

					//	Failure
					return false;
				}

				if (GetLastEdge().GetVertexB()
					== rhs->GetFirstEdge().GetVertexA())
				{
					//	Polygons are continuing paths
					for (unsigned long e = 0; e < rhs->m_edges.size(); ++e)
					{
						//	Copy each of the edges from the other polygon to
						//	this one
						AddEdgeToBack(rhs->m_edges[e]);
					}

					//	Success
					return true;
				}
				else if (GetLastEdge().GetVertexB()
						 == rhs->GetLastEdge().GetVertexB())
				{
					//	End points match - but 2nd is in reverse
					for (unsigned long e = 0; e < rhs->m_edges.size(); ++e)
					{
						//	Copy each of the edges from the other polygon to
						//	this one in reverse order and reverse orientation
						Edge2D nextEdge =
								rhs->m_edges[rhs->m_edges.size() - e].GetReversedForm();
						AddEdgeToBack(nextEdge);
					}

					//	Success
					return true;
				}
				//	Failure
				return false;
			}

			Edge2D GetFirstEdge() const
			{
				return m_edges[0];
			}

			Edge2D GetLastEdge() const
			{
				return m_edges[m_edges.size()-1];
			}

			T GetSignedArea() const
			{
				using std::cout;
				using std::cerr;
				using std::endl;

#define SUPRESS_OUTPUT
				unsigned long n = m_vertices.size();

				T result =0.0f;

				for(int p=n-1,q=0; q<n; p=q++)
				{
					result += m_vertices[p].GetX() * m_vertices[q].GetY()
							- m_vertices[q].GetX() * m_vertices[p].GetY();
				}
#if defined(VERBOSE_OUTPUT) && !defined(SUPRESS_OUTPUT)
				cout << "Signed area: " << result << "." << endl;
#endif
				return result * 0.5;
#undef SUPRESS_OUTPUT
			}

			bool isInsideTriangle(float Ax, float Ay,
								  float Bx, float By,
								  float Cx, float Cy,
								  float Px, float Py) const

			{
				float ax, ay, bx, by, cx, cy, apx, apy, bpx, bpy, cpx, cpy;
				float cCROSSap, bCROSScp, aCROSSbp;

				ax = Cx - Bx;  ay = Cy - By;
				bx = Ax - Cx;  by = Ay - Cy;
				cx = Bx - Ax;  cy = By - Ay;
				apx= Px - Ax;  apy= Py - Ay;
				bpx= Px - Bx;  bpy= Py - By;
				cpx= Px - Cx;  cpy= Py - Cy;

				aCROSSbp = ax*bpy - ay*bpx;
				cCROSSap = cx*apy - cy*apx;
				bCROSScp = bx*cpy - by*cpx;

				return ((aCROSSbp >= 0.0f) && (bCROSScp >= 0.0f) && (cCROSSap >= 0.0f));
			}

			bool canRemoveEar(const unsigned long indexV1,
							  const unsigned long indexV2,
							  const unsigned long indexV3,
							  const unsigned long remainingVertexCount,int *V) const
			{
				using std::cout;
				using std::cerr;
				using std::endl;

#define SUPRESS_OUTPUT

#if defined(VERBOSE_OUTPUT) && !defined(SUPRESS_OUTPUT)
				cout << "Testing if ear can be removed from polygon." << endl;
#endif
				unsigned long p;
				float aX, aY, bX, bY, cX, cY, Px, Py;

				aX = m_vertices[V[indexV1]].GetX();
				aY = m_vertices[V[indexV1]].GetY();

				bX = m_vertices[V[indexV2]].GetX();
				bY = m_vertices[V[indexV2]].GetY();

				cX = m_vertices[V[indexV3]].GetX();
				cY = m_vertices[V[indexV3]].GetY();

				if ( EPSILON > (((bX-aX)*(cY-aY)) - ((bY-aY)*(cX-aX))) )
					return false;

				for (p=0;p<remainingVertexCount;p++)
				{
					if( (p == indexV1) || (p == indexV2) || (p == indexV3) ) continue;
					Px = m_vertices[V[p]].GetX();
					Py = m_vertices[V[p]].GetY();
					if (isInsideTriangle(aX,aY,bX,bY,cX,cY,Px,Py)) return false;
				}
#if defined(VERBOSE_OUTPUT) && !defined(SUPRESS_OUTPUT)
				cout << "OK!" << endl;
#endif
				return true;
#undef SUPRESS_OUTPUT
			}

			vector<Triangle2<T>> Triangulate()
			{
				using std::cout;
				using std::cerr;
				using std::endl;

#define SUPRESS_OUTPUT
				vector<Triangle2<T>> result;

#if defined(VERBOSE_OUTPUT) && !defined(SUPRESS_OUTPUT)
				cout << "Triangulating polygon." << endl;
#endif

				int vertexCount = m_vertices.size();
				if (vertexCount < 3)
				{
					cerr << "Triangulation error!  Not enough vertices in polygon.";
					cerr << "In function: " << __func__ << "." << endl;

					return result;
				}

				int *vertexLookup = new int[vertexCount];

				/* we want a counter-clockwise polygon in V */
				if ( 0.0f < GetSignedArea())
				{
					//	Polygon is already CCW
					for (int v=0; v < vertexCount; v++)
						vertexLookup[v] = v;
				}
				else
				{
					//	Reverse vertices to make them CCW
					for(int v=0; v < vertexCount; v++)
						vertexLookup[v] = (vertexCount - 1) - v;
				}

				//	The number of vertices left for processing in the polygon
				unsigned long remainingVerticeCount = vertexCount;

				/*  remove nv-2 Vertices, creating 1 triangle every time */
				int errorCount = 2*remainingVerticeCount;   /* error detection */

				for(unsigned long m=0, v=remainingVerticeCount-1;
					remainingVerticeCount>2; )
				{
					using std::cout;
					using std::cerr;
					using std::endl;

					/* if we loop, it is probably a non-simple polygon */
					if (0 >= (errorCount--))
					{
						//** Triangulate: ERROR - probable bad polygon!
						cerr << "Triangulation error!  Possible bad polygon.";
						cerr << "In function: " << __func__ << "." << endl;

						return result;
					}

					/* three consecutive vertices in current polygon, <u,v,w> */
					unsigned long u = v;
					if (remainingVerticeCount <= u)
						u = 0;

					/* previous */
					v = u+1;

					/* new v */
					if (remainingVerticeCount <= v)
						v = 0;

					/* next */
					unsigned long w = v+1;
					if (remainingVerticeCount <= w)
						w = 0;

					if (canRemoveEar(u,v,w,remainingVerticeCount,vertexLookup))
					{
						unsigned long indexA,indexB,indexC,s,t;

						/* true names of the vertices */
						indexA = vertexLookup[u];
						indexB = vertexLookup[v];
						indexC = vertexLookup[w];

						/* output Triangle */
						result.push_back(Triangle2<T>(m_vertices[indexA],
													  m_vertices[indexB],
													  m_vertices[indexC]));

						m++;

						/* remove v from remaining polygon */
						for(s=v,t=v+1;t<remainingVerticeCount;s++,t++)
						{
							vertexLookup[s] = vertexLookup[t];
						}
						remainingVerticeCount--;

						/* resest error detection counter */
						errorCount = 2*remainingVerticeCount;
					}
				}

				delete vertexLookup;
#if defined(VERBOSE_OUTPUT) && !defined(SUPRESS_OUTPUT)
				cout << "Number of triangles: " << result.size();
				cout << "." << endl;
#endif
				return result;
#undef SUPRESS_OUTPUT
			}

			void ReverseWinding()
			{
				std::reverse(m_edges.begin(), m_edges.end());

				for (unsigned i = 0; i < m_edges.size(); ++i)
				{
					m_edges[i] = m_edges[i].GetReversedForm();
				}
			}

			string ToString() const
			{
				stringstream result;

				result << "Polygon = { ";

				//  Length's
				//result << "vertex count = " << m_vertices.size() << ", ";
				result << "edge count = " << m_edges.size() << ", ";
				result << "is closed = " << (IsClosed() ?"true":"false") << ", ";
				result << "signed area = " << GetSignedArea() << ", ";

				//  Angles
				//  ...

				//  Area
				//	...

				//  Vertex listing
				result << "edges = { ";
				for (unsigned long i = 0; i < m_edges.size(); ++i)
				{
					result << m_edges[i].ToString();

					if (i < m_edges.size() - 1)
					{
						result << ", ";
					}
				}
				result << " }";

				//  Closing brace
				result << " }";

				return result.str();
			}

			bool operator ==(const Polygon2<T> &rhs)
			{
				using std::cout;
				using std::cerr;
				using std::endl;

				cout << "Testing polygons for equality." << endl;

				//	Easy tests for failure
				if (m_edges.size() != rhs.m_edges.size()) return false;
				if (m_vertices.size() != rhs.m_vertices.size()) return false;

				//	Exact match of ordering required (but possibly reversed
				for (unsigned long e = 0; e < m_edges.size(); ++e)
				{
					//	Can exist backwards
					if (m_edges[e].IsCongruentTo(rhs.m_edges[e]))
						return false;
				}
			}

			///
			/// \brief IsCongruentTo
			/// \param rhs
			/// \return
			///
			bool IsCongruentTo(const Polygon2<T> &rhs)
			{
				//	Easy tests for failure
				if (m_edges.size() != rhs.m_edges.size()) return false;
				if (m_vertices.size() != rhs.m_vertices.size()) return false;

				//	List of hashed vertices for both polygons
				vector<int64_t> lhsMortonNumbers;
				vector<int64_t> rhsMortonNumbers;

				//	We already know how many values
				lhsMortonNumbers.reserve(m_vertices.size());
				rhsMortonNumbers.reserve(m_vertices.size());

				//	Convert each vertex and add to the two vectors
				for (unsigned i = 0; i < m_vertices.size(); ++i)
				{
					lhsMortonNumbers.push_back(m_vertices[i].GetMortonNumber());
					rhsMortonNumbers.push_back(rhs.m_vertices[i].GetMortonNumber());
				}

				//	Vertex order is not important, so sort them...
				using std::sort;
				sort(lhsMortonNumbers.begin(), lhsMortonNumbers.end());
				sort(rhsMortonNumbers.begin(), rhsMortonNumbers.end());

				//	...allowing us to do a linear search for mismatches
				for (unsigned i = 0; i < m_vertices.size(); ++i)
				{
					if (lhsMortonNumbers[i] != rhsMortonNumbers[i])
						return false;
				}
				return true;
			}

			///
			/// \brief		Returns the first vertex of an unclosed polygon
			/// \return		A point in 2D space
			/// \since		06-06-2015
			/// \author		Dean
			///
			Point2D GetOriginVertex() const
			{
				using std::cout;
				using std::cerr;
				using std::endl;

				if (IsClosed())
				{
					cerr << "Cannot return the origin of a closed polygon!";
					cerr << "  In function: " << __func__ << " (file: ";
					cerr << __FILE__ << ", line " << __LINE__ << "." << endl;

					return Point2D();
				}
				else
				{
					if (m_edges.empty())
					{
						cerr << "Polygon does not contain any edges!";
						cerr << "  In function: " << __func__ << " (file: ";
						cerr << __FILE__ << " line " << __LINE__ << "." << endl;

						return Point2D();
					}
					else
					{
						return m_edges[0].GetVertexA();
					}
				}
			}

			///
			/// \brief		Returns the last vertex of an unclosed polygon
			/// \return		A point in 2D space
			/// \since		06-06-2015
			/// \author		Dean
			///
			Point2D GetTerminatingVertex() const
			{
				using std::cout;
				using std::cerr;
				using std::endl;

				if (IsClosed())
				{
					cerr << "Cannot return the terminus of a closed polygon!";
					cerr << "  In function: " << __func__ << " (file: ";
					cerr << __FILE__ << ", line " << __LINE__ << "." << endl;

					return Point2D();
				}
				else
				{
					if (m_edges.empty())
					{
						cerr << "Polygon does not contain any edges!";
						cerr << "  In function: " << __func__ << " (file: ";
						cerr << __FILE__ << ", line " << __LINE__ << "." << endl;

						return Point2D();
					}
					else
					{
						return m_edges[m_edges.size()-1].GetVertexB();
					}
				}
			}
	};
}

#endif // POLYGON_H
