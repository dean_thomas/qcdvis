#ifndef LINESEGMENT2D
#define LINESEGMENT2D

#include <string>
#include <sstream>
#include <cmath>
#include "Vector3.h"
#include <vector>
#include "VertexTriple.h"

using std::string;
using std::stringstream;
using std::vector;

namespace Geometry
{
	template <typename T>
	class LineSegment2
	{
		private:
			typedef Vector2<T> Point2D;
			typedef Vector2<T> Vector2D;

			Point2D m_vertexA;
			Point2D m_vertexB;

			T m_length;
		public:
			LineSegment2()
			{

			}

			LineSegment2(const Point2D vertexA, const Point2D vertexB)
			{
				m_vertexA = vertexA;
				m_vertexB = vertexB;

				calculateStats();
			}

			void calculateStats()
			{
				//Vector3D displacement = m_vertexA + m_vertexB;

				//m_length = displacement.GetMagnitude();
			}

		public:
			Point2D GetVertexA() const { return m_vertexA; }
			Point2D GetVertexB() const { return m_vertexB; }

			T GetLength() const { return m_length; }

			string ToString(const unsigned int decimalPlaces = 4) const
			{
				stringstream result;

				result << "LineSegment = { ";
				result << m_vertexA.ToString(decimalPlaces);
				result << ", ";
				result << m_vertexB.ToString(decimalPlaces);
				result << " }";

				return result.str();
			}

			bool operator == (const LineSegment2<T> rhs) const
			{
				return ((m_vertexA == rhs.m_vertexA)
						&& (m_vertexB == rhs.m_vertexB));
			}

			bool operator != (const LineSegment2<T> rhs) const
			{
				return !((m_vertexA == rhs.m_vertexA)
						&& (m_vertexB == rhs.m_vertexB));
			}

			bool IsCongruentTo(const LineSegment2<T> &rhs) const
			{
				const LineSegment2<T> lhs = *this;
				const LineSegment2<T> lhsR = lhs.GetReversedForm();

				//	Direct match
				if (lhs == rhs) return true;

				//	Reverse match
				if (lhsR == rhs) return true;

				//	Not a match
				return false;
			}

			///	Returns a copy of this edge but with the vertex order swapped.
			LineSegment2<T> GetReversedForm() const
			{
				return LineSegment2<T>(m_vertexB, m_vertexA);
			}

			///
			///		\brief	Check if the specified vertex is part of this edge
			///		\param vertex
			///		\return
			///		\since	20-04-2015
			///		\author	Dean
			///
			bool IsIncidentTo(const Point2D vertex) const
			{
				if ((m_vertexA == vertex) || (m_vertexB == vertex))
				{
					return true;
				}
				else
				{
					return false;
				}
			}

	};
}

#endif // LINESEGMENT2D

