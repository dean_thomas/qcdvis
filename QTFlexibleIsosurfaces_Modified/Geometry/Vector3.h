#ifndef VECTOR3_H
#define VECTOR3_H

#include <string>
#include <sstream>
#include <cmath>
#include <limits>
#include <iomanip>
#include <Geometry/Vector2.h>
#include <cinttypes>

using std::string;
using std::stringstream;
using std::numeric_limits;
using std::abs;
using std::setprecision;
using std::fixed;

namespace Geometry
{
#define ALLOWABLE_ULPS_FOR_EQUALITY 1
#define DECIMAL_PRECISION 4

	template <class T>
	class Vector3
	{
		private:
			T m_x;
			T m_y;
			T m_z;
		public:
			///
			/// \brief      Constructor
			/// \since      04-12-2014
			/// \author     Dean
			///
			Vector3(const T x, const T y, const T z)
			{
				m_x = x;
				m_y = y;
				m_z = z;
			}

			///
			///	\brief		Assignment operator
			/// \since		18-05-2015
			/// \author		Dean
			///
			Vector3(const Vector3<T> &rhs)
			{
				m_x = rhs.m_x;
				m_y = rhs.m_y;
				m_z = rhs.m_z;
			}

			///
			/// \brief      Constructor
			/// \since      04-12-2014
			/// \author     Dean
			///
			Vector3()
			{
				m_x = T();
				m_y = T();
				m_z = T();
			}

			T GetX() const { return m_x; }
			T GetY() const { return m_y; }
			T GetZ() const { return m_z; }

			void SetX(const T x) { m_x = x; }
			void SetY(const T y) { m_y = y; }
			void SetZ(const T z) { m_z = z; }

			///
			/// \brief      Normalize the vector so that it has unit length (1.0)
			/// \return
			/// \author Dean
			/// \since  08-12-2014
			///
			Vector3 GetUnitVector() const
			{
				T factor = 1.0 / GetMagnitude();

				return Vector3(m_x * factor, m_y * factor, m_z * factor);
			}

			///
			/// \brief GetMagnitude
			/// \return
			/// \author Dean
			/// \since  08-12-2014
			///
			T GetMagnitude() const
			{
				return sqrt((m_x * m_x) + (m_y * m_y) + (m_z * m_z));
			}

			///
			/// \brief	Point3D::ToString
			/// \return
			/// \since      04-12-2014
			/// \author     Dean
			///
			string ToString(const unsigned int decimalPlaces = 4) const
			{
				stringstream result;

				//	We want to determine the number of decimal places
				result << fixed;
				result << "vector3 = <";
				result << setprecision(decimalPlaces) << m_x;
				result << ", ";
				result << setprecision(decimalPlaces) << m_y;
				result << ", ";
				result << setprecision(decimalPlaces) << m_z;
				result << ">";

				return result.str();
			}

			Vector3 Cross(const Vector3& rhs) const
			{
				//  See:
				//  http://math.stackexchange.com/questions/305642/how-to-find-surface-normal-of-a-triangle
				//  [12-01-2015]
				T nX = (m_y * rhs.m_z) - (m_z * rhs.m_y);
				T nY = (m_z * rhs.m_x) - (m_x * rhs.m_z);
				T nZ = (m_x * rhs.m_y) - (m_y * rhs.m_x);

				return Vector3(nX, nY, nZ);
			}

			Vector3 operator -(const Vector3& rhs) const
			{
				return (Vector3((m_x - rhs.m_x), (m_y - rhs.m_y), (m_z - rhs.m_z)));
			}

			bool almostEqual(const T& x, const T& y, const unsigned long ulp) const
			{
				// the machine epsilon has to be scaled to the magnitude of
				// the values used and multiplied by the desired precision
				// in ULPs (units in the last place) - unless the result is
				//	subnormal.
				return abs(x-y) <
						numeric_limits<T>::epsilon() * abs(x+y) * ulp
						|| abs(x-y) <numeric_limits<T>::min();
			}

			bool operator ==(const Vector3& rhs) const
			{
				if (numeric_limits<T>::is_integer)
				{
					return ((m_x == rhs.m_x)
							&& (m_y == rhs.m_y) && (m_z == rhs.m_z));
				}
				else
				{
					//	Floating point precision :(
#ifdef ALLOWABLE_ULPS_FOR_EQUALITY
					return (almostEqual(m_x, rhs.m_x, ALLOWABLE_ULPS_FOR_EQUALITY)
							&& (almostEqual(m_y, rhs.m_y, ALLOWABLE_ULPS_FOR_EQUALITY))
							&& (almostEqual(m_z, rhs.m_z, ALLOWABLE_ULPS_FOR_EQUALITY)));
#else
					return ((roundDP(m_x, PRECISION) == roundDP(rhs.m_x,PRECISION))
							&& (roundDP(m_y, PRECISION) == roundDP(rhs.m_y,PRECISION))
							&& (roundDP(m_z, PRECISION) == roundDP(rhs.m_z,PRECISION)));
#endif
				}
			}

			bool operator !=(const Vector3& rhs) const
			{
				return (!operator==(rhs));
			}

			///
			/// \brief		Downsize to a Vector2 by discarding the X coordinate
			/// \since		27-04-2015
			/// \author		Dean
			/// \return
			///
			Vector2<T> YZ() const
			{
				return Vector2<T>(m_y, m_z);
			}

			///
			/// \brief		Downsize to a Vector2 by discarding the Y coordinate
			/// \since		27-04-2015
			/// \author		Dean
			/// \return
			///
			Vector2<T> XZ() const
			{
				return Vector2<T>(m_x, m_z);
			}

			///
			/// \brief		Downsize to a Vector2 by discarding the Z coordinate
			/// \since		27-04-2015
			/// \author		Dean
			/// \return
			///
			Vector2<T> XY() const
			{
				return Vector2<T>(m_x, m_y);
			}

			int64_t spreadBits(const int64_t &in) const
			{
				int64_t result = in;
				result =	(result |(result <<20))&0x000001FFC00003FF;
				result =	(result |(result <<10))&0x0007E007C00F801F;
				result =	(result |(result <<4))&0x00786070C0E181C3;
				result =	(result |(result <<2))&0x0199219243248649;
				result =	(result |(result <<2))&0x0649249249249249;
				result =	(result |(result <<2))&0x1249249249249249;
				return result ;
			}

			///
			/// \brief		Rounds the floating point number to a specified
			///				number of decimal places
			/// \param		rhs: the number to be rounded
			/// \param		dp: the number of decimal places
			/// \return		Real: the rounded number
			/// \since		15-05-2015
			/// \author		Dean
			///
			T roundDP(const T& rhs, const unsigned long dp) const
			{
				//	Factor to multply and divide by
				double factor = pow(10.0, (double)dp);

				return (round(rhs * factor) / factor);
			}

			///
			/// \brief		Returns a Morton hash of the vector, converting it
			///				to 1D - allowing sorting etc
			/// \return		a 64-bit integer hash of the number
			/// \since		08-05-2015
			/// \author		Dean
			///
			int64_t	GetMortonNumber() const
			{
				//	Make sure we are dealing with floats that fit in 64bits
				assert(sizeof(T) <= 8);

				//	Must work in 3D, so set Z to 0
				int64_t iX = (int64_t)(roundDP(m_x, DECIMAL_PRECISION));
				int64_t iY = (int64_t)(roundDP(m_y, DECIMAL_PRECISION));
				int64_t iZ = (int64_t)(roundDP(m_z, DECIMAL_PRECISION));

				//	Shift the constant by 63 bits
				int64_t k = 1;
				k = k << 63;

				return (spreadBits(iX) |
						(spreadBits(iY)<<1) |
						(spreadBits(iZ)<<2) |
						k);
			}

			///
			///	/brief		Checks if the X element matches a specific value,
			///				taking into account floating point errors
			/// /since		27-05-2015
			/// /author		Dean
			///
			bool EqualsX(const T& rhs) const
			{
				return (almostEqual(m_x, rhs, ALLOWABLE_ULPS_FOR_EQUALITY));
			}

			///
			///	/brief		Checks if the Y element matches a specific value,
			///				taking into account floating point errors
			/// /since		27-05-2015
			/// /author		Dean
			///
			bool EqualsY(const T& rhs) const
			{
				return (almostEqual(m_y, rhs, ALLOWABLE_ULPS_FOR_EQUALITY));
			}

			///
			///	/brief		Checks if the Z element matches a specific value,
			///				taking into account floating point errors
			/// /since		27-05-2015
			/// /author		Dean
			///
			bool EqualsZ(const T& rhs) const
			{
				return (almostEqual(m_z, rhs, ALLOWABLE_ULPS_FOR_EQUALITY));
			}
	};
}
#endif // VECTOR3_H
