#ifndef TRIANGLE3_H
#define TRIANGLE3_H

#include <string>
#include <sstream>
#include <cmath>
#include "Vector3.h"
#include "LineSegment3.h"
#include <vector>
#include "VertexTriple.h"

using std::string;
using std::stringstream;
using std::vector;

namespace Geometry
{
	template <typename T>
	class Triangle3
	{
#define VERTEX_A 0
#define VERTEX_B 1
#define VERTEX_C 2

		private:
			typedef Vector3<T> Point3D;
			typedef Vector3<T> Vector3D;
			typedef vector<Point3D> VertexList;
			typedef LineSegment3<T> Edge3D;

			VertexList m_vertices;
			T m_area;
			T m_semiPerimeter;
			T m_perimeter;

			//	Used for triangulation of arbitary polygon
			//typedef vector<VertexTriple<T> > TriangleVertexList;

			T m_lengthAB;
			T m_lengthBC;
			T m_lengthAC;
		public:
			Edge3D GetEdgeAB() const { return Edge3D(m_vertices[VERTEX_A],
													 m_vertices[VERTEX_B]); }
			Edge3D GetEdgeBC() const { return Edge3D(m_vertices[VERTEX_B],
													 m_vertices[VERTEX_C]); }
			Edge3D GetEdgeAC() const { return Edge3D(m_vertices[VERTEX_A],
													 m_vertices[VERTEX_C]); }

			Triangle3()
			{
				m_vertices.push_back(Point3D());
				m_vertices.push_back(Point3D());
				m_vertices.push_back(Point3D());
			}

			Triangle3(const Point3D vertexA,
					 const Point3D vertexB,
					 const Point3D vertexC)
			{
				m_vertices.push_back(vertexA);
				m_vertices.push_back(vertexB);
				m_vertices.push_back(vertexC);

				calculateStats();
			}

			void calculateStats()
			{
				Point3D vA = m_vertices[VERTEX_A];
				Point3D vB = m_vertices[VERTEX_B];
				Point3D vC = m_vertices[VERTEX_C];

				//  Length of edges
				m_lengthAB = sqrt(pow(vB.GetX() - vA.GetX(), 2.0)
								  + pow(vB.GetY() - vA.GetY(), 2.0)
								  + pow(vB.GetZ() - vA.GetZ(), 2.0));

				m_lengthBC = sqrt(pow(vC.GetX() - vB.GetX(), 2.0)
								  + pow(vC.GetY() - vB.GetY(), 2.0)
								  + pow(vC.GetZ() - vB.GetZ(), 2.0));

				m_lengthAC = sqrt(pow(vC.GetX() - vA.GetX(), 2.0)
								  + pow(vC.GetY() - vA.GetY(), 2.0)
								  + pow(vC.GetZ() - vA.GetZ(), 2.0));

				//  Perimeter
				m_perimeter = m_lengthAB + m_lengthBC + m_lengthAC;

				//  Uses Heron's Formula to calculate area given length's of sides
				m_semiPerimeter =  m_perimeter / 2.0;

				m_area = sqrt((m_semiPerimeter * (m_semiPerimeter - m_lengthAB)
							   * (m_semiPerimeter - m_lengthBC)
							   * (m_semiPerimeter - m_lengthAC)));

				//  Angles etc...
			}

		public:
			/*
			Triangle()
			{
				m_vertices.resize(3, {});

				calculateStats();
			}

*/
			//	Most likely a candidate to put into a derived mesh specific
			//	class in the future....
			/*
			virtual TriangleVertexList TriangulateVertices() const
			{
				//printf("Triangulating triangle.\n");

				//	Only 1 set of vertices here...
				VertexTriple<T> vt;

				vt.v0 = m_vertices[0];
				vt.v1 = m_vertices[1],
				vt.v2 = m_vertices[2];

				TriangleVertexList result;
				result.reserve(1);
				result.push_back(vt);

				return result;
			}
			*/

			Point3D GetVertexA() const { return m_vertices[VERTEX_A]; }
			Point3D GetVertexB() const { return m_vertices[VERTEX_B]; }
			Point3D GetVertexC() const { return m_vertices[VERTEX_C]; }

			T GetLengthAB() const { return m_lengthAB; }
			T GetLengthBC() const { return m_lengthBC; }
			T GetLengthAC() const { return m_lengthAC; }

			T GetPerimeter() const { return m_perimeter; }
			T GetSemiPerimeter() const { return m_semiPerimeter; }
			T GetArea() const { return m_area; }

			Vector3D GetUnitNormalVector() const
			{
				return GetNormalVector().GetUnitVector();
			}

			Vector3D GetNormalVector() const
			{
				//  See:
				//  http://math.stackexchange.com/questions/305642/how-to-find-surface-normal-of-a-triangle
				//  [12-01-2015]
				Vector3D v = m_vertices[VERTEX_B] - m_vertices[VERTEX_A];
				Vector3D w = m_vertices[VERTEX_C] - m_vertices[VERTEX_A];

				return v.Cross(w);
			}

			string ToString() const
			{
				stringstream result;

				result << "Triangle = { ";

				//  Length's
				result << "lengthAB = " << m_lengthAB;
				result << ", lengthBC = " << m_lengthBC;
				result << ", lengthAC = " << m_lengthAC;
				result << ", perimeter = " << m_perimeter;

				//  Angles
				//  ...

				//  Area
				result << ", area = " << m_area;

				//  Vertex listing
				result << ", vertices = { ";
				result << m_vertices[VERTEX_A].ToString();
				result << ", ";
				result << m_vertices[VERTEX_B].ToString();
				result << ", ";
				result << m_vertices[VERTEX_C].ToString();
				result << " }";

				//  Closing brace
				result << " }";

				return result.str();
			}
	};
}

#endif // TRIANGLE_H
