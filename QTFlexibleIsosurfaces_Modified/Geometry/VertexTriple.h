#ifndef VERTEXTRIPLE_H
#define VERTEXTRIPLE_H

#include "Vector3.h"

namespace Geometry
{
	template <typename T>
	struct VertexTriple
	{
			typedef Vector3<T> Point3D;
			Point3D v0;
			Point3D v1;
			Point3D v2;
	};
}

#endif // VERTEXTRIPLE_H
