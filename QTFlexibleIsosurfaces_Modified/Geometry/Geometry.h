#ifndef GEOMETRY_H
#define GEOMETRY_H

#include "Vector2.h"
#include "Vector3.h"

#include "Triangle3.h"

#include "LineSegment2.h"
#include "LineSegment3.h"

#include "Polygon2.h"
#include "Polygon3.h"

using namespace Geometry;

typedef LineSegment2<float> LineSegment2f;
typedef LineSegment2<double> LineSegment2d;
typedef LineSegment2<long double> LineSegment2ld;
typedef LineSegment2<unsigned long> LineSegment2ul;


//  3D Geometry
//  ===========

typedef LineSegment3<float> LineSegment3f;
typedef LineSegment3<double> LineSegment3d;
typedef LineSegment3<long double> LineSegment3ld;
typedef LineSegment3<unsigned long> LineSegment3ul;

typedef Vector3<unsigned long> Vector3ul;

//  Points
//  ------
typedef Vector3<float> Point3f;
typedef Vector3<double> Point3d;
typedef Vector3<long double> Point3ld;
typedef Vector3<unsigned long> Point3ul;

//  Triangles
typedef Triangle3<float> Triangle3f;
typedef Triangle3<double> Triangle3d;
typedef Triangle3<long double> Triangle3ld;
typedef Triangle3<unsigned long> Triangle3ul;

#endif // GEOMETRY_H
