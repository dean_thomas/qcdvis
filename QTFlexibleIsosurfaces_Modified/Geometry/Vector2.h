#ifndef VECTOR2
#define VECTOR2

#include <string>
#include <sstream>
#include <cmath>
#include <limits>
#include <iomanip>
#include <cfloat>
#include <cstdint>
#include <cassert>

using std::string;
using std::stringstream;
using std::numeric_limits;
using std::abs;
using std::setprecision;
using std::fixed;

namespace Geometry
{
#define ALLOWABLE_ULPS_FOR_EQUALITY 2
#define DECIMAL_PRECISION 4

	template <class T>
	class Vector2
	{
		private:
			T m_x;
			T m_y;
		public:
			///
			/// \brief      Constructor
			/// \since      27-04-2015
			/// \author     Dean
			///
			Vector2(const T x, const T y)
			{
				m_x = x;
				m_y = y;
			}

			///
			/// \brief      Constructor
			/// \since      27-04-2015
			/// \author     Dean
			///
			Vector2()
			{
				m_x = T();
				m_y = T();
			}

			T GetX() const { return m_x; }
			T GetY() const { return m_y; }

			void SetX(const T x) { m_x = x; }
			void SetY(const T y) { m_y = y; }

			///
			/// \brief      Normalize the vector so that it has unit length (1.0)
			/// \return
			/// \author Dean
			/// \since      27-04-2015
			///
			Vector2 GetUnitVector() const
			{
				T factor = 1.0 / GetMagnitude();

				return Vector2(m_x * factor, m_y * factor);
			}

			///
			/// \brief GetMagnitude
			/// \return
			/// \author Dean
			/// \since      27-04-2015
			///
			T GetMagnitude() const
			{
				return sqrt((m_x * m_x) + (m_y * m_y));
			}

			///
			/// \brief	Point3D::ToString
			/// \return
			/// \since      04-12-2014
			/// \author     Dean
			///
			string ToString(const unsigned int decimalPlaces = 4) const
			{
				stringstream result;

				//	We want to determine the number of decimal places
				result << fixed;
				result << "vector2 = <";
				result << setprecision(decimalPlaces) << m_x;
				result << ", ";
				result << setprecision(decimalPlaces) << m_y;
				result << ">";

				return result.str();
			}

			Vector2 operator -(const Vector2& rhs) const
			{
				return (Vector2((m_x - rhs.m_x), (m_y - rhs.m_y)));
			}

			///
			///	/brief		Checks if two floating point values match,
			///				taking into account floating point errors
			/// /since		05-05-2015
			/// /author		Dean
			///
			bool almostEqual(const T& val1, const T& val2,
							 const unsigned long ulp) const
			{
				// the machine epsilon has to be scaled to the magnitude of
				// the values used and multiplied by the desired precision
				// in ULPs (units in the last place) - unless the result is
				//	subnormal.
				return abs(val1-val2) <
						numeric_limits<T>::epsilon() * abs(val1+val2) * ulp
						|| abs(val1-val2) <numeric_limits<T>::min();
			}

			bool operator ==(const Vector2& rhs) const
			{
				if (numeric_limits<T>::is_integer)
				{
					return ((m_x == rhs.m_x)
							&& (m_y == rhs.m_y));
				}
				else
				{
					//	Floating point precision :(
					return (almostEqual(m_x, rhs.m_x, ALLOWABLE_ULPS_FOR_EQUALITY)
							&& (almostEqual(m_y, rhs.m_y, ALLOWABLE_ULPS_FOR_EQUALITY)));
				}
			}

			bool operator !=(const Vector2& rhs) const
			{
				return (!operator==(rhs));
			}

			///
			///	/brief		Checks if the X element matches a specific value,
			///				taking into account floating point errors
			/// /since		05-05-2015
			/// /author		Dean
			///
			bool EqualsX(const T& rhs) const
			{
				return (almostEqual(m_x, rhs, ALLOWABLE_ULPS_FOR_EQUALITY));
			}

			///
			///	/brief		Checks if the Y element matches a specific value,
			///				taking into account floating point errors
			/// /since		05-05-2015
			/// /author		Dean
			///
			bool EqualsY(const T& rhs) const
			{
				return (almostEqual(m_y, rhs, ALLOWABLE_ULPS_FOR_EQUALITY));
			}

			int64_t spreadBits(const int64_t &in) const
			{
				int64_t result = in;
				result =	(result |(result <<20))&0x000001FFC00003FF;
				result =	(result |(result <<10))&0x0007E007C00F801F;
				result =	(result |(result <<4))&0x00786070C0E181C3;
				result =	(result |(result <<2))&0x0199219243248649;
				result =	(result |(result <<2))&0x0649249249249249;
				result =	(result |(result <<2))&0x1249249249249249;
				return result ;
			}

			///
			/// \brief		Rounds the floating point number to a specified
			///				number of decimal places
			/// \param		rhs: the number to be rounded
			/// \param		dp: the number of decimal places
			/// \return		Real: the rounded number
			/// \since		15-05-2015
			/// \author		Dean
			///
			T roundDP(const T& rhs, const unsigned long dp) const
			{
				//	Factor to multply and divide by
				double factor = pow(10.0, (double)dp);

				return (round(rhs * factor) / factor);
			}

			///
			/// \brief		Returns a Morton hash of the vector, converting it
			///				to 1D - allowing sorting etc
			/// \return		a 64-bit integer hash of the number
			/// \since		08-05-2015
			/// \author		Dean
			///
			int64_t	GetMortonNumber() const
			{
				//	Make sure we are dealing with floats that fit in 64bits
				assert(sizeof(T) <= 8);

				//	Must work in 3D, so set Z to 0
				int64_t iX = (int64_t)(roundDP(m_x, DECIMAL_PRECISION));
				int64_t iY = (int64_t)(roundDP(m_y, DECIMAL_PRECISION));
				int64_t iZ = 0;

				//	Shift the constant by 63 bits
				int64_t k = 1;
				k = k << 63;

				return (spreadBits(iX) |
						(spreadBits(iY)<<1) |
						(spreadBits(iZ)<<2) |
						k);
			}
	};
}

#endif // VECTOR2

