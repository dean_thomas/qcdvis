#include "DataModel.h"

using namespace std;

#define ADD_GHOST_CELLS false

std::string DataModel::GetCacheDir(HeightField4D* const heightField4) const
{
	auto it = m_cacheDirLookup4.find(heightField4);
	assert(it != m_cacheDirLookup4.cend());

	return it->second;
}

void DataModel::AddCacheDirLookup(HeightField4D* const heightField4,
								  const std::string& cacheDir)
{
	m_cacheDirLookup4[heightField4] = cacheDir;
}

DataModel::~DataModel()
{

}

///
/// \brief		DataModel::AddBookmark
/// \param		bookmark
/// \author		Dean
/// \since		05-08-2015
///
void DataModel::AddBookmark(const IsovalueBookmark &bookmark)
{
	try
	{
		m_bookmarkArray.AddBookmark(bookmark);

		cout << "User added new bookmark (" << bookmark.ToString();
		cout << ")." << endl;
	}
	catch (exception &ex)
	{
		cerr << "Exception!  " << ex.what() << " in func: " << __func__;
		cerr << " (file: " << __FILE__ << ", line: " << __LINE__ << ".";
		cerr << endl;
	}
}

///
/// \brief DataModel::RemoveBookmark
/// \param bookmark
/// \author		Dean
/// \since		05-08-2015
///
void DataModel::RemoveBookmark(const IsovalueBookmark &bookmark)
{
	m_bookmarkArray.RemoveBookmark(bookmark);

	cout << "Removed bookmark (" << bookmark.ToString();
	cout << ")." << endl;
}

HeightField4D *DataModel::GetHeightField4d(const unsigned long &index) const
{
	if (index >= m_heightField4dArray.size())
	{
		throw InvalidIndexException(index, __func__, __FILE__, __LINE__);
	}
	else
	{
		return m_heightField4dArray[index];
	}
}

///
/// \brief		Loads a 4D hypervolume from the specified file
/// \param filename
/// \return
/// \since		02-07-2015
///	\author		Dean
///
HeightField4D *DataModel::LoadData4d(const string &filename)
{
	//cout << "Existing loaded data will be cleared!" << endl;

	//	Load the data
	HeightField4D* heightField4D = new HeightField4D(this, filename);
	heightField4D->LoadFromFile(filename);

	//	Add to vector
	m_heightField4dArray.push_back(heightField4D);


	return heightField4D;
}



HeightField3D *DataModel::GetHeightField3d(const unsigned long &index) const
{
	if (index >= m_heightField3dArray.size())
	{
		throw InvalidIndexException(index, __func__, __FILE__, __LINE__);
	}
	else
	{
		return m_heightField3dArray[index];
	}
}

///
/// \brief		Loads a 3D data set
/// \param		filename
/// \return		HeightField3D*: a pointer to the loaded file (or nullptr)
///
HeightField3D *DataModel::LoadData3d(const string &filename,
									 const bool& computeContourTree)
{
	HeightField3D *result = new HeightField3D(this);

	if (result != nullptr)
	{
		if (result->LoadFromFile(filename, ADD_GHOST_CELLS, computeContourTree))
		{


			if (result->IsLoaded())
			{
				m_heightField3dArray.push_back(result);

				return result;
			}
		}
		else
		{
			//	Heightfield can be discarded
			delete result;

			return nullptr;
		}
	}
	else
	{
		//	Heightfield can be discarded
		delete result;

		return nullptr;
	}

}

///
/// \brief		Default constructor
/// \since		27-10-2015
/// \author		Dean
///
DataModel::DataModel()
{
	m_dataSetMaxima = std::numeric_limits<Real>::min();
	m_dataSetMinima = std::numeric_limits<Real>::max();
}

///
/// \brief      Constructor
/// \param      parameters
///	\since		?
/// \author     Dean
///	\author		Dean: modify to call default constructor [27-10-2015]
///
DataModel::DataModel(const ProgramParameters parameters)
	: DataModel()
{
	m_parameters = parameters;

	if (!parameters.list.empty())
	{
		//  User has asked to process a list of files
		//	processFilelist();
	}
	else
	{
		if (!parameters.filename.empty())
		{
			bool result = LoadData3d(parameters.filename, true);

			if (result)
			{
				cout << "File " << parameters.filename.c_str();
				cout << " loaded successfully." << endl;

				// use the specified file to load the data & compute the contour tree
				//m_heightfield = new HeightField3D(this);
			}
			else
			{
				cerr << "Could not load file " << parameters.filename.c_str();
				cerr << "." << endl;
			}
		}
	}
}

/*
///
/// \brief      Processes a list of files for background computation
/// \return
/// \since      02-12-2014
/// \author     Dean
///
bool DataModel::processFilelist()
{
	unsigned long count = 0;
	unsigned long failures = 0;

	ifstream fileList(m_parameters.list, ifstream::in);

	//  Check the file was able to be opened
	if (!fileList.good() || fileList == NULL)
		return false;

	do
	{
		string theFilename;

		//  Files to be processed
		getline(fileList, theFilename);

		//  Handle quotes in filename's better...
		//  =====================================
		//
		cout << "Processing file: " << theFilename << "." << endl;

		//	Note: construction and file loading must be done as
		//	a two step process now...
		//m_heightfield = new HeightField(theFilename.c_str(), 0, 0, 0, 0);

		//  Check the file loaded correctly
		if (m_heightfield->IsLoaded())
		{
			processSingleDataSet();
		}
		else
		{
			cerr << "Unable to process: " << theFilename << "." << endl;
			++failures;
		}

		delete m_heightfield;
		++count;
	} while (fileList.good());

	fileList.close();

	cout << "Processed " << count << " files, with ";
	cout << failures << " failures." << endl;

	return true;
}
*/

/*
///
/// \brief      Performs computations on a single file in batch list
/// \since      02-12-2014
/// \author     Dean
///
void DataModel::processSingleDataSet()
{
	if (m_heightfield->GetMaxHeight() > m_dataSetMaxima)
	{
		cout << "Found new global Maxima!  Was " << m_dataSetMaxima;
		cout << ", setting to " << m_heightfield->GetMaxHeight() << ".";
		cout << endl;

		m_dataSetMaxima = m_heightfield->GetMaxHeight();
	}

	if (m_heightfield->GetMinHeight() < m_dataSetMinima)
	{
		cout << "Found new global Mainima!  Was " << m_dataSetMinima;
		cout << ", setting to " << m_heightfield->GetMinHeight() << ".";
		cout << endl;

		m_dataSetMinima = m_heightfield->GetMinHeight();
	}

	m_heightfield->GetContourTree()->GetSuperarcDistribution()->RecalculateDistribution(m_dataSetMinima, m_dataSetMaxima);
	m_heightfield->GetContourTree()->GetSuperarcDistribution()->SaveCountsToFile("c:\\aFile.txt", true, m_dataSetMinima, m_dataSetMaxima);
}
*/

///
/// \brief Saves the current workspace (open files, bookmarks)
/// \param filename
/// \return
/// \author		Dean
/// \since		24-08-2015
///
bool DataModel::SaveWorkspace(const std::string &filename) const
{
	ofstream file(filename);

	if (!file.is_open()) return false;

	file << "#\tBookmarks" << endl;
	file << m_bookmarkArray.GetBookmarkList() << endl;

	file.close();

	return true;
}
