#include "QtJcnDialog.h"
#include "ui_QtJcnDialog.h"
#include <QFileInfo>
#include <QDebug>
#include <QFileDialog>

static constexpr float SLAB_SIZES[10] = { 8, 4, 2, 1, 0.5, 0.25, 0.125, 0.0625, 0.03125, 0.015625 };

QDir QtJcnDialog::getSlabsDirF0() const
{
	QDir f0_dir = m_output_root_dir.absolutePath() + "/f0";
	if (!f0_dir.exists())
	{
		f0_dir.mkpath(".");
	}
	return f0_dir;
}

QDir QtJcnDialog::getSlabsDirF1() const
{
	QDir f1_dir = m_output_root_dir.absolutePath() + "/f1";
	if (!f1_dir.exists())
	{
		f1_dir.mkpath(".");
	}
	return f1_dir;
}

QDir QtJcnDialog::getSlabsDirF0f1() const
{
	QDir f0f1_dir = m_output_root_dir.absolutePath() + "/f0_f1";
	if (!f0f1_dir.exists())
	{
		f0f1_dir.mkpath(".");
	}
	return f0f1_dir;
}

QStringList QtJcnDialog::getParamsF0() const
{
	QStringList result;

	auto f0_dir = getSlabsDirF0();

	//	First the input fields..
	result << "-input" << (QString("%1").arg(ui->labelFilename0->text())) << "f0" << (QString("%1").arg(m_slabsize0));

	//	Output directory...
	result << "-outdir" << (QString("%1").arg(f0_dir.absolutePath()));

	//	No need for the GUI
	result << "-nogui";

	//	We'll need to generate slab obs
	result << "-slab_obj";

	//	Periodic boundaries
	if (ui->checkBoxPeriodic->isChecked()) result << "-periodic";

	//	Print for reference
	qDebug() << result;

	return result;
}

QStringList QtJcnDialog::getParamsF1() const
{
	QStringList result;

	auto f1_dir = getSlabsDirF1();

	//	First the input fields..
	result << "-input" << (QString("%1").arg(ui->labelFilename1->text())) << "f1" << (QString("%1").arg(m_slabsize1));

	//	Output directory...
	result << "-outdir" << (QString("%1").arg(f1_dir.absolutePath()));

	//	No need for the GUI
	result << "-nogui";

	//	We'll need to generate slab obs
	result << "-slab_obj";

	//	Periodic boundaries
	if (ui->checkBoxPeriodic->isChecked()) result << "-periodic";

	//	Print for reference
	qDebug() << result;

	return result;
}

QStringList QtJcnDialog::getParamsF0F1() const
{
	QStringList result;

	auto f0f1_dir = getSlabsDirF0f1();

	//	First the input fields..
	result << "-input" << (QString("%1").arg(ui->labelFilename0->text())) << "f0" << (QString("%1").arg(m_slabsize0));
	result << "-input" << (QString("%1").arg(ui->labelFilename1->text())) << "f1" << (QString("%1").arg(m_slabsize1));

	//	Output directory...
	result << "-outdir" << (QString("%1").arg(f0f1_dir.absolutePath()));

	//	No need for the GUI
	result << "-nogui";

	//	We'll need to generate slab obs
	result << "-slab_obj";

	//	Periodic boundaries
	if (ui->checkBoxPeriodic->isChecked()) result << "-periodic";

	//	Print for reference
	qDebug() << result;

	return result;
}

///
/// \brief QtJcnDialog::QtJcnDialog
/// \param parent
/// \author		Dean
/// \since		17-12-2016
///
QtJcnDialog::QtJcnDialog(QWidget *parent) :
	QDialog(parent),
	ui(new Ui::QtJcnDialog)
{
	ui->setupUi(this);

	m_slabsize0 = SLAB_SIZES[ui->horizontalSlider0->value()];
	ui->labelSlabSize0->setText(QString("%1").arg(m_slabsize0));
	connect(ui->horizontalSlider0, &QSlider::valueChanged,
			[&](int value)
	{
		m_slabsize0 = SLAB_SIZES[value];
		ui->labelSlabSize0->setText(QString("%1").arg(m_slabsize0));
	});

	m_slabsize1 = SLAB_SIZES[ui->horizontalSlider1->value()];
	ui->labelSlabSize1->setText(QString("%1").arg(m_slabsize1));
	connect(ui->horizontalSlider1, &QSlider::valueChanged,
			[&](int value)
	{
		m_slabsize1 = SLAB_SIZES[value];
		ui->labelSlabSize1->setText(QString("%1").arg(m_slabsize1));
	});

	//	Choose an output directory dialog
	m_output_root_dir = QDir::home();
	ui->lineEditOutputDir->setText(m_output_root_dir.absolutePath());

	connect(ui->pushButtonBrowse, &QPushButton::clicked,
			[&]()
	{
		auto path = QFileDialog::getExistingDirectory(this, "Select directory for JCN output files",m_output_root_dir.absolutePath());
		auto dir = QDir(path);

		if (dir.exists())
		{
			m_output_root_dir = dir;
			ui->lineEditOutputDir->setText(m_output_root_dir.absolutePath());
		}
	});
}

QtJcnDialog::~QtJcnDialog()
{
	delete ui;
}

///
/// \brief		Stores items used to call this dialog and sets up GUI
/// \param items
/// \author		Dean
/// \since		17-12-2016
///
void QtJcnDialog::setGeneratingItems(const QVector<QtVolumeStandardItem*>& items)
{
	assert(items.size() == 2);

	qDebug() << "The following files are to be usedas inputs to the JCN: ";
	for (auto& item : items)
	{
		qDebug() << item->getHeightfieldFilename();
	}

	auto filename0 = items[0]->getHeightfieldFilename();
	auto file0info = QFileInfo(filename0);
	auto fieldName0 = items[0]->getFieldname();
	assert(file0info.exists());

	auto filename1 = items[1]->getHeightfieldFilename();
	auto file1info = QFileInfo(filename1);
	auto fieldName1 = items[1]->getFieldname();
	assert(file1info.exists());

	//	Set up UI items
	ui->labelFilename0->setText(file0info.absoluteFilePath());
	ui->labelFieldName0->setText(fieldName0);

	ui->labelFilename1->setText(file1info.absoluteFilePath());
	ui->labelFieldName1->setText(fieldName1);
}
