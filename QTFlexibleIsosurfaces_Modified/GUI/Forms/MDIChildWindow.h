#ifndef MDICHILDWINDOW_H
#define MDICHILDWINDOW_H

#include <QWidget>
#include "HeightField3D.h"
#include <QAction>
#include <QMdiSubWindow>
#include <set>
#include <map>
#include "Graphs/SupernodeBase.h"
#include "Graphs/SuperarcBase.h"
#include "GUI/Widgets/QtColourRampWidget/QtColourRampWidget.h"

namespace Ui {
	class MDIChildWindow;
}

class QtIsosurfaceWidget;
class QDockWidget;
class QtIsovalueSliderWidget;
class QtTopologyTreeController;
//class SupernodeBase;
//class QtSuperarc;

class MDIChildWindow : public QWidget
{
		Q_OBJECT

	public:
		explicit MDIChildWindow(QWidget *parent = 0);
		MDIChildWindow(HeightField3D* heightField3D,
					   const QString& reeb_graph_file,
					   QWidget *parent = 0);
		~MDIChildWindow();
		QAction *GetActiveAction() const;

		//void SetHeightfield3(HeightField3D* const heightField3D);
		HeightField3D* GetActiveHeightfield3() const { return m_heightField3D; }

		QDockWidget *GetContourTreeDockWidget() const;
		QDockWidget *GetContourPropertiesDockWidget() const;
		QtIsosurfaceWidget *GetSurfaceRenderWidget() const;
		QtColourRampWidget* GetColourRampWidget() const;
		QtIsovalueSliderWidget* GetIsovalueSliderWidget() const;

		void SetHeightfield(HeightField3D* heightField3D, const QString &reeb_graph_file="");

		void SetShowCriticalNodeGlyphs(const bool value);
		bool GetShowCriticalNodeGlyphs() const;

		void StepCameraDistance(const int delta);
		void SetMdiWrapper(QMdiSubWindow *subWindow);

		void SetIsovalueSliderEnabled(const bool value);

		void SetModelMatrix(const QMatrix4x4 &rotationMatrix);
		void SetFloatingCameraPosition(const QQuaternion &rotation, const float radius);

		void SetCamera();

		Ui::MDIChildWindow* GetUi() const { return ui; }

		bool executeReebCommand(const QString& heightField3file,
								const QString& fieldName,
								const QString& cacheDir);

		void SetCurrentIsovalue(const float value);
		void SetCurrentNormalisedIsovalue(const float value);

		QImage ObtainScreenshot(const QSize& dimensions,
								const QColor& backgroundColour) const;

		void SaveReebGraph(const QString& filename) const;
		void SaveContourTree(const QString& filename) const;

		//	Axis options
		void SetRenderAxisLines(const bool value);
		void SetRenderFarAxisLines(const bool value);
		void SetRenderAxisLabels(const bool value);

		//	Grid options
		void SetRenderOuterGridOnly(const bool value);
		void SetRenderGrid(const bool value);

		void SetAlphaBlended(const bool value);
		void SetContourTreeVisible(const bool value);
		void SetNestedContours(const bool value);

		void SetNestedContourCount(const size_t value);
		size_t GetNestedContourCount() const;

		void SetRenderContourTreeSkeleton(const bool value);
		bool GetRenderContourTreeSkeleton() const;

		void SetRenderReebGraphSkeleton(const bool value);
		bool GetRenderReebGraphSkeleton() const;

		void setHeaders();
	private:
		HeightField3D *m_heightField3D = nullptr;
		Ui::MDIChildWindow *ui;
		QMdiSubWindow *m_mdiWrapper = nullptr;

		using supernode_base_type = SupernodeBase;
		using superarc_base_type = SuperarcBase<SupernodeBase>;

		QtTopologyTreeController* m_treeController = nullptr;
	private slots:
		void isovalueChanged();
		void updateAction(Qt::WindowStates, Qt::WindowStates newState);
		void on_actionShowHide_triggered(bool checked);


		void on_pushButtonSliceNext_clicked();

		void on_pushButtonSlicePrev_clicked();

		void on_pushButtonCoolsNext_clicked();

		void on_pushButtonCoolsPrev_clicked();

	signals:
		void OnSurfaceRenderMouseScroll(int delta);

		void UserClickedNextSliceButton(MDIChildWindow* window, HeightField3D* const heightfield);
		void UserClickedPrevSliceButton(MDIChildWindow* window, HeightField3D* const heightfield);

		void UserClickedNextCoolButton(MDIChildWindow* window, HeightField3D* const heightfield);
		void UserClickedPrevCoolButton(MDIChildWindow* window, HeightField3D* const heightfield);
};

#endif // MDICHILDWINDOW_H
