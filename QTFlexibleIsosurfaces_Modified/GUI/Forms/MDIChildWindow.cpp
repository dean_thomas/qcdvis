#include "MDIChildWindow.h"
#include "ui_MDIChildWindow.h"
#include "Graphs/ReebGraph.h"
#include "GUI/Widgets/QtTopologyTreeWidget/QtTopologyTreeController.h"
#include "GUI/Widgets/QtTopologyTreeWidget/QtSuperarc.h"


#define DISABLE_DEBUG_OUTPUT

#define m_cacheDir std::string("C:\\Users\\4thFloor\\Desktop\\ReebGraphTest")

#ifdef _MSC_VER
#define __PRETTY_FUNCTION__ __FUNCSIG__
#endif

using namespace std;

void MDIChildWindow::SaveReebGraph(const QString& filename) const
{
	ui->widgetReebGraph->SaveToPdf(filename);
}

void MDIChildWindow::SaveContourTree(const QString& filename) const
{
	ui->widgetContourTree->SaveToPdf(filename);
}


///
/// \brief		Sets if glyphs in topology skeleton are rendered as an overlay
/// \param value#
/// \since		30-03-2016
/// \author		Dean
///
void MDIChildWindow::SetShowCriticalNodeGlyphs(const bool value)
{
	ui->widgetSurfaceRender->SetRenderSkeletonGlyphs(value);
}

///
/// \brief MDIChildWindow::GetShowCriticalNodeGlyphs
/// \return
/// \since		30-03-2016
/// \author		Dean
///
bool MDIChildWindow::GetShowCriticalNodeGlyphs() const
{
	return ui->widgetSurfaceRender->GetRenderSkeletonGlyphs();
}
///
/// \brief		Provides headers for extra information regarding slices
/// \since		29-03-2016
/// \author		Dean
///
void MDIChildWindow::setHeaders()
{
	auto cools = m_heightField3D->GetCoolingSlice();
	auto sliceVariable = QString::fromStdString(m_heightField3D->GetFixedVariableName());
	auto sliceValue = m_heightField3D->GetFixedVariableValue();
	auto fieldName = QString::fromStdString(m_heightField3D->GetFieldName());

	ui->labelCools->setText(QString("Cools: %1").arg(cools));
	ui->labelField->setText(fieldName);
	ui->labelSlice->setText(QString("Slice %1: %2").arg(sliceVariable).arg(sliceValue));
}

QImage MDIChildWindow::ObtainScreenshot(const QSize& dimensions,
										const QColor& backgroundColour) const
{
	return ui->widgetSurfaceRender->ObtainScreenShot(dimensions,
													 backgroundColour);
}

///
/// \brief MDIChildWindow::SetNestedContoursCount
/// \param value
/// \author		Dean
/// \since		24-03-2016
///
void MDIChildWindow::SetNestedContourCount(const size_t value)
{
	ui->widgetSurfaceRender->SetNestedContourCount(value);
}

///
/// \brief MDIChildWindow::GetNestedContourCount
/// \return
/// \author		Dean
/// \since		24-03-2016
///
size_t MDIChildWindow::GetNestedContourCount() const
{
	return ui->widgetSurfaceRender->GetNestedContourCount();
}

///
/// \brief MDIChildWindow::SetIsovalueSliderEnabled
/// \param value
/// \author		Dean
/// \since		24-03-2016
///
void MDIChildWindow::SetIsovalueSliderEnabled(const bool value)
{
	ui->widgetIsovalueSlder->setEnabled(value);
}

///
/// \brief QtIsosurfaceWidget::SetNestedContours
/// \param value
/// \author		Dean
/// \since		24-03-2016
///
void MDIChildWindow::SetNestedContours(const bool value)
{
	ui->widgetSurfaceRender->SetNestedContours(value);
	ui->widgetContourTree->SetCurrentIsovalueLineVisible(!value);
	ui->widgetReebGraph->SetCurrentIsovalueLineVisible(!value);
}

///
/// \brief QtIsosurfaceWidget::SetAlphaBlended
/// \param value
/// \author		Dean
/// \since		24-03-2016
///
void MDIChildWindow::SetAlphaBlended(const bool value)
{
	ui->widgetSurfaceRender->SetAlphaBlended(value);
}

///
/// \brief MDIChildWindow::SetRenderSkeleton
/// \param checked
/// \author		Dean
/// \since		22-03-2016
///
void MDIChildWindow::SetRenderContourTreeSkeleton(const bool value)
{
	ui->widgetSurfaceRender->SetRenderContourTreeSkeleton(value);
}


void MDIChildWindow::SetRenderReebGraphSkeleton(const bool value)
{
	ui->widgetSurfaceRender->SetRenderReebGraphSkeleton(value);
}

///
/// \brief MDIChildWindow::GetRenderSkeleton
/// \return
/// \author		Dean
/// \since		22-03-2016
///
bool MDIChildWindow::GetRenderContourTreeSkeleton() const
{
	return ui->widgetSurfaceRender->GetRenderContourTreeSkeleton();
}

QtIsovalueSliderWidget* MDIChildWindow::GetIsovalueSliderWidget() const
{
	return ui->widgetIsovalueSlder;
}

QtColourRampWidget* MDIChildWindow::GetColourRampWidget() const
{
	return ui->widgetColourRamp;
}

void MDIChildWindow::SetContourTreeVisible(const bool value)
{
	ui->dockWidgetDataTopology->setVisible(value);
}

///
/// \brief		MDIChildWindow::GetContourPropertiesDockWidget
/// \return
/// \since		22-07-2015
/// \author		Dean
///
QDockWidget *MDIChildWindow::GetContourPropertiesDockWidget() const
{
	return ui->dockWidgetContourProperties;
}

///
/// \brief		MDIChildWindow::GetContourTreeDockWidget
/// \return
/// \since		22-07-2015
/// \author		Dean
///
QDockWidget *MDIChildWindow::GetContourTreeDockWidget() const
{
	return ui->dockWidgetDataTopology;
}

///
/// \brief MDIChildWindow::GetSurfaceRenderWidget
/// \return
/// \since		21-07-2015
/// \author		Dean
///
QtIsosurfaceWidget *MDIChildWindow::GetSurfaceRenderWidget() const
{
	return ui->widgetSurfaceRender;
}

void MDIChildWindow::SetCamera()
{
	ui->widgetSurfaceRender->IncrementCamera();
}

///
/// \since		16-07-2015
/// \author		Dean
///
void MDIChildWindow::StepCameraDistance(const int delta)
{
	ui->widgetSurfaceRender->StepCameraDistance(delta);
}

void MDIChildWindow::SetCurrentNormalisedIsovalue(const float value)
{
	//ui->widgetSurfaceRender->SetTargetIsovalue(value);

	//ui->widgetIsovalueSlder->SetNormalizedIsoValue(value);
}

void MDIChildWindow::SetCurrentIsovalue(const float value)
{
	ui->widgetIsovalueSlder->SetIsovalue(value);
	//ui->widgetSurfaceRender->SetTargetIsovalue(value);
}

///
/// \brief MDIChildWindow::SetRenderOuterGridOnly
/// \param checked
/// \since		16-07-2015
/// \author		Dean
///
void MDIChildWindow::SetRenderOuterGridOnly(const bool value)
{
	ui->widgetSurfaceRender->SetRenderOuterGridOnly(value);
}

///
/// \brief MDIChildWindow::SetRenderGrid
/// \param checked
/// \since		16-07-2015
/// \author		Dean
///
void MDIChildWindow::SetRenderGrid(const bool value)
{
	ui->widgetSurfaceRender->SetRenderGrid(value);
}

///
/// \brief MDIChildWindow::SetRenderAxisLines
/// \param checked
/// \since		16-07-2015
/// \author		Dean
///
void MDIChildWindow::SetRenderAxisLines(const bool value)
{
	ui->widgetSurfaceRender->SetRenderAxisLines(value);
}

///
/// \brief MDIChildWindow::SetRenderAxisLabels
/// \param checked
/// \since		16-07-2015
/// \author		Dean
///
void MDIChildWindow::SetRenderAxisLabels(const bool value)
{
	ui->widgetSurfaceRender->SetRenderAxisLabels(value);
}

///
/// \brief		Sets if axis lines should be rendered at the 'far side' of the
///				data
/// \param checked
/// \author		Dean
/// \since		14-07-2015
///
void MDIChildWindow::SetRenderFarAxisLines(const bool value)
{
	ui->widgetSurfaceRender->SetRenderFarAxisLines(value);
}

///
/// \brief		Sets the camera position using the supplied quaternion
/// \param		rotationMatrix
/// \since		10-07-2015
/// \author		Dean
///
void MDIChildWindow::SetFloatingCameraPosition(const QQuaternion &rotation, const float radius)
{
	ui->widgetSurfaceRender->SetFloatingCameraPosition(rotation, radius);
}

///
/// \brief		Sets the model matrix using the supplied values
/// \param		rotationMatrix
/// \since		10-07-2015
/// \author		Dean
///
void MDIChildWindow::SetModelMatrix(const QMatrix4x4 &rotationMatrix)
{
	ui->widgetSurfaceRender->SetModelMatrix(rotationMatrix);
}

MDIChildWindow::MDIChildWindow(QWidget *parent) :
	QWidget(parent),
	ui(new Ui::MDIChildWindow)
{
	ui->setupUi(this);
}

QAction *MDIChildWindow::GetActiveAction() const
{
	return ui->actionShowHide;
}

///
/// \brief	Executes the "Rb.exe" command to generate a reebgraph for a given
///			heightfield
/// \param heightField3file
/// \param fieldName
/// \param cacheDir
/// \return bool: true command executed without errors
/// \author		Dean
/// \since		05-02-2015
///
bool MDIChildWindow::executeReebCommand(const QString& heightField3file,
										const QString& fieldName,
										const QString& cacheDir)
{
	const QString REEB_CMD = "C:\\Users\\4thFloor\\Documents\\Qt\\QtFlexibleIsosurfaces\\vtkReebGraphTest\\x64\\Debug\\vtkReebGraphTest.exe";

	QStringList args;
	args << "-nogui"
		 << "-input"
		 << heightField3file
		 << fieldName
		 << "i256"
		 << "-periodic"
		 << "-reebRb"
		 << "-outdir"
		 << cacheDir;
	//<< "-Simplify";

	qDebug() << REEB_CMD << args;

	auto result = QProcess::execute(REEB_CMD, args);

	if (result == 0) return true;

	QMessageBox::critical(this,
						  "Error",
						  QString("There was an error executing the ReebGraph "
								  "command.  The application exited with error "
								  " code: %1.").arg(result));
	return false;
}

MDIChildWindow::MDIChildWindow(HeightField3D* heightField3D,
							   const QString& reeb_graph_file,
							   QWidget *parent) :
	QWidget(parent),
	ui(new Ui::MDIChildWindow)
{
	ui->setupUi(this);

	//	Set the heightfield
	SetHeightfield(heightField3D, reeb_graph_file);

	connect(ui->widgetColourRamp, &QtColourRampWidget::OnPositiveColourChanged,
			[=](const QColor& colour)
	{
		ui->widgetSurfaceRender->SetIsosurfaceColourPositive(colour);
	});
	connect(ui->widgetColourRamp, &QtColourRampWidget::OnNegativeColourChanged,
			[=](const QColor& colour)
	{
		ui->widgetSurfaceRender->SetIsosurfaceColourNegative(colour);
	});
	connect(ui->widgetColourRamp, &QtColourRampWidget::OnUniversalColourChanged,
			[=](const QColor& colour)
	{
		ui->widgetSurfaceRender->SetIsosurfaceColour(colour);
	});
	connect(ui->widgetColourRamp, &QtColourRampWidget::OnColourTableChanged,
			[=](const QtColourRampWidget::ColourTable& colourTable)
	{
		ui->widgetSurfaceRender->SetColourTable(colourTable);
	});

	connect(ui->widgetColourRamp, &QtColourRampWidget::OnDisplayModeChanged,
			[=](const QtColourRampWidget::DisplayMode& displayMode)
	{
		using cr_dm = QtColourRampWidget::DisplayMode;
		using iw_cm = QtIsosurfaceWidget::ColourMode;

		//	TODO: this could probably be best achieved as a single enum
		switch (displayMode)
		{
			case cr_dm::COLOUR_RAMP:
				ui->widgetSurfaceRender->SetColourMode(iw_cm::GRADIENT);
				break;
			case cr_dm::UNIVERSAL_COLOUR:
				ui->widgetSurfaceRender->SetColourMode(iw_cm::DEFAULT);
				break;
			case cr_dm::DUAL_COLOUR:
				ui->widgetSurfaceRender->SetColourMode(iw_cm::HOT_COLD);
				break;
			default:
				ui->widgetSurfaceRender->SetColourMode(iw_cm::DEFAULT);
		}
	});

	//	Make sure any updates are visible in the surface view
	connect(m_treeController, &QtTopologyTreeController::TreesUpdated,
			[=]()
	{
		ui->widgetSurfaceRender->update();
	});

	connect(ui->widgetSurfaceRender, &QtIsosurfaceWidget::OnMouseScroll,
			[=](int delta)
	{
#ifndef DISABLE_DEBUG_OUTPUT
		cout << "User scroll on Surface Render (delta=" << delta;
		cout << "." << endl;
#endif
		emit OnSurfaceRenderMouseScroll(delta);
	});

	//	Respond to changes in isovalue
	QObject::connect(ui->widgetIsovalueSlder,
					 SIGNAL(OnIsovalueChanged()),
					 this, SLOT(isovalueChanged()));

	//	For now, we'll hide the properties window (until it is re-implemented)
	ui->dockWidgetContourProperties->hide();
}

void MDIChildWindow::SetHeightfield(HeightField3D* heightField3D, const QString& reeb_graph_file)
{
	assert(heightField3D != nullptr);
	m_heightField3D = heightField3D;

	//	Set form header using heightfield info
	setHeaders();

	//isosurfaceWidget->SetIsosurfaceColourNegative(colourDialog.selectedColor());
	//isosurfaceWidget->SetColourMode(QtIsosurfaceWidget::ColourMode::HOT_COLD);
	if (m_heightField3D != nullptr)
	{
		//	Get the current isovalue in the window
		auto current_isovalue = ui->widgetIsovalueSlder->GetIsovalue();
		cout << "Current isovalue: " << current_isovalue << endl;

		ui->widgetIsovalueSlder->SetIsovalueRange(heightField3D->GetMinHeight(),
												  heightField3D->GetMaxHeight());
		//ui->widgetHistogram->SetIsovalueRange(heightField3D->GetMinHeight(),
		//										  heightField3D->GetMaxHeight());
		ui->widgetColourRamp->SetIsovalueRange(heightField3D->GetMinHeight(),
											   heightField3D->GetMaxHeight());

		//	Set the isovalue to the previous value
		ui->widgetIsovalueSlder->SetIsovalue(current_isovalue);
		//ui->widgetTopRightSubView->SetHeightField3D(heightField3D);

		ui->widgetSurfaceRender->SetHeightField3D(heightField3D);
		ui->widgetContourTree->SetExtents(m_heightField3D->GetBaseDimX(),
										  m_heightField3D->GetBaseDimY(),
										  m_heightField3D->GetBaseDimZ());
		ui->widgetContourTree->DisplayContourTree(m_heightField3D->GetContourTree());

		auto reebGraph = m_heightField3D->GetReebGraph();

		//	If there is no reeb graph available then it will need to be computed
		if ((!reebGraph) && (reeb_graph_file != ""))
		{
			reebGraph = new ReebGraph();

			auto reebFile = reeb_graph_file;

			auto result = reebGraph->LoadFromFile(reebFile.toStdString());

			if (result)
				cout << "Load Reeb graph from file: " << reeb_graph_file.toStdString() << endl;
			else
				cerr << "Unable to load Reeb graph from file: " << reeb_graph_file.toStdString() << endl;

#ifndef DISABLE_DEBUG_OUTPUT
			//std::string reebFile(m_cacheDir + "\\" + field + "_" + fixedVar + "=" + fixedVal + ".rb");
			cout << "Will try to load reeb graph from file: "
				 << reebFile << endl;


			cout << "Reeb graph loaded: " << (result ? "yes" : "no") << endl;
#endif
			//	Hacky way to avoid loading an empty reeb graph for now...
			if (!result)
			{
				delete reebGraph;
				reebGraph = nullptr;
			}
		}

		//	Respond to the user requesting a nested set of contours for a particular object
		connect(ui->widgetContourTree, &QtTopologyTreeWidget::RequestNestedContoursForObject,
				[&](ContourTreeSuperarc* superarc)
		{
			qDebug() << "User is requesting nested contours for object" << superarc->GetId();

			//	Get hold of the contour tree
			ContourTree* contour_tree = superarc->GetContourTree();
			assert(contour_tree != nullptr);

			//	Loop over superarcs to set visibility
			for (auto it = contour_tree->Superarcs_begin();
				 it != contour_tree->Superarcs_end(); ++it)
			{
				if (*it != superarc)
				{
					//	If the iterator isn't pointing at this superarc
					//	we'll set the object to hidden
					(*it)->SetHidden(true);
				}
				else
				{
					//	Otherwise, make sure it is visible
					(*it)->SetHidden(false);
				}
			}

			//	Get the top and bottom height
			auto min = superarc->GetBottomSupernode()->GetHeight();
			auto max = superarc->GetTopSupernode()->GetHeight();

			//	Set to display nested contours across the scalar range of this object
			ui->widgetSurfaceRender->SetNestedContours(true);
			ui->widgetSurfaceRender->SetNestedContourRange(min, max);
		});

		if (reebGraph != nullptr)
		{
			assert(reebGraph != nullptr);
			m_heightField3D->SetReebGraph(reebGraph);

			ui->widgetReebGraph->SetExtents(m_heightField3D->GetBaseDimX(),
											m_heightField3D->GetBaseDimY(),
											m_heightField3D->GetBaseDimZ());

			ui->widgetReebGraph->DisplayReebGraph(reebGraph);

			auto setupTreeController = [&]()
			{
				//	This function sets up the interactive functionality between
				//	the reeb graph etc.
				m_treeController = new QtTopologyTreeController(this);

				m_treeController->setContourTree(m_heightField3D->GetContourTree(),
												 ui->widgetContourTree);
				m_treeController->setReebGraph(m_heightField3D->GetReebGraph(),
											   ui->widgetReebGraph);
			};

			setupTreeController();
		}

		if (reebGraph != nullptr)
		{
			ui->widgetReebGraph->show();

			//	Show the header
			ui->frameReebGraph->show();
			ui->labelReebGraphStats->setText(QString("v+=%1, v-=%2, a=%3")
											 .arg(reebGraph->GetPositiveVertexCount())
											 .arg(reebGraph->GetNegativeVertexCount())
											 .arg(reebGraph->GetArcCount()));
			//ui->widgetReebGraph->SetCurrentIsovalueLineVisible();
		}
		else
		{
			//	If there is no Reeb graph loaded or able to be loaded then
			//	just skip using it....
			ui->widgetReebGraph->hide();
			ui->frameReebGraph->hide();
		}

		ui->widgetHistogram->AddSuperarcDistribution(
					m_heightField3D->GetContourTree()->GetSuperarcDistribution(), Qt::black);

		ui->actionShowHide->setText(
					QString::fromStdString(m_heightField3D->GetFieldName()));

	}
}

///
/// \brief		Stores a reference to the MDI interface
/// \param		mdiWrapper
/// \since		03-07-2015
/// \author		Dean
///
void MDIChildWindow::SetMdiWrapper(QMdiSubWindow *mdiWrapper)
{
#ifdef DISABLE_DEBUG_OUTPUT
	cout << "Setting MDI wrapper." << endl;
#endif
	//	Store the MDI interface for this window
	//	Monitor this for menu feedback
	m_mdiWrapper = mdiWrapper;
	QObject::connect(m_mdiWrapper,
					 SIGNAL(windowStateChanged(Qt::WindowStates, Qt::WindowStates)),
					 this,
					 SLOT(updateAction(Qt::WindowStates, Qt::WindowStates)));
#ifndef DISABLE_DEBUG_OUTPUT
	cout << "Done." << endl;
#endif
}

///
/// \brief		Monitors the minimized/maximized state of the form and displays
///				it using a QAction
/// \param		newState
/// \since		03-07-2015
/// \author		Dean
///
void MDIChildWindow::updateAction(Qt::WindowStates, Qt::WindowStates newState)
{
#ifndef DISABLE_DEBUG_OUTPUT
	cout << "Triggered update action" << endl;
#endif
	//	Temporary disable so we don't fire an event
	ui->actionShowHide->blockSignals(true);

	//	Ticks the menu item when it is in view
	ui->actionShowHide->setChecked(!newState.testFlag(Qt::WindowMinimized));

	//	Respond to UI changes
	ui->actionShowHide->blockSignals(false);
#ifndef DISABLE_DEBUG_OUTPUT
	cout << "Done." << endl;
#endif
}


///
/// \brief		Responds to changes on the Isovalue slider
/// \since		03-07-2015
/// \author		Dean
///
void MDIChildWindow::isovalueChanged()
{
	ui->widgetSurfaceRender->SetTargetIsovalue(
				ui->widgetIsovalueSlder->GetIsovalue());
	ui->widgetHistogram->SetCurrentNormalisedIsovalue(
				ui->widgetIsovalueSlder->GetNormalizedIsovalue());
	ui->widgetColourRamp->SetCurrentNormalizedIsovalue(
				ui->widgetIsovalueSlder->GetNormalizedIsovalue());
	ui->widgetContourTree->SetCurrentNormalisedIsovalue(
				ui->widgetIsovalueSlder->GetNormalizedIsovalue());
	//	Reeb graph (don't update if it has been hidden due to no data)
	if (ui->widgetReebGraph->isVisible())
	{
		ui->widgetReebGraph->SetCurrentNormalisedIsovalue(
					ui->widgetIsovalueSlder->GetNormalizedIsovalue());
	}
}

MDIChildWindow::~MDIChildWindow()
{
#ifndef DISABLE_DEBUG_OUTPUT
	cout << __PRETTY_FUNCTION__ << endl;
#endif
	delete ui;
}

///
/// \brief		Allows the form to be minimized/maximized from the menu
/// \param		arg1
/// \since		03-07-2015
/// \author		Dean
///
void MDIChildWindow::on_actionShowHide_triggered(bool checked)
{
#ifndef DISABLE_DEBUG_OUTPUT
	cout << __func__ << endl;
#endif
	if (checked)
	{
		m_mdiWrapper->showNormal();
	}
	else
	{
		m_mdiWrapper->showMinimized();
	}
#ifndef DISABLE_DEBUG_OUTPUT
	cout << "ok." << endl;
#endif
}

///
/// \brief MDIChildWindow::on_pushButtonSliceNext_clicked
/// \since		29-03-2016
/// \author		Dean
///
void MDIChildWindow::on_pushButtonSliceNext_clicked()
{
	emit(UserClickedNextSliceButton(this, m_heightField3D));
}

///
/// \brief MDIChildWindow::on_pushButtonSlicePrev_clicked
/// \since		29-03-2016
/// \author		Dean
///
void MDIChildWindow::on_pushButtonSlicePrev_clicked()
{
	emit(UserClickedPrevSliceButton(this, m_heightField3D));
}

///
/// \brief MDIChildWindow::on_pushButtonSlicePrev_clicked
/// \since		06-04-2016
/// \author		Dean
///
void MDIChildWindow::on_pushButtonCoolsNext_clicked()
{
	emit( UserClickedNextCoolButton(this, m_heightField3D));
}

///
/// \brief MDIChildWindow::on_pushButtonSlicePrev_clicked
/// \since		06-04-2016
/// \author		Dean
///
void MDIChildWindow::on_pushButtonCoolsPrev_clicked()
{
	emit(UserClickedPrevCoolButton(this, m_heightField3D));
}
