#include "MDIParentWindow.h"
#include "ui_MDIParentWindow.h"

#include <fstream>
#include <algorithm>
#include <cassert>

#include <QInputDialog>
#include <QSettings>
#include <QColorDialog>
#include <QQuaternion>

#include "Globals/QtOstream.h"
#include "GUI/Widgets/QtIsosurfaceWidget/QtIsosurfaceWidget.h"
#include "GUI/Widgets/QtColourRampWidget/QtColourRampWidget.h"
#include "GUI/Widgets/QtTopologyTreeWidget/QtTopologyTreeWidget.h"
#include "GUI/Widgets/QtArcBallWidget/QtArcBallWidget.h"
#include "GUI/Forms/QtJointContourNetForm.h"
#include "Graphs/ReebGraph.h"
#include "Globals/Functions.h"
#include "GUI/Forms/QtJcnDialog.h"

//	Hides the small dock area in the bottom left of the window
//	used for testing new widgets
#define HIDE_DEBUG_DOCK_AREA

#define JCN_EXE "/home/dean/qcdvis/vtkReebGraphTest/build-vtkReebGraphTest-Desktop_Qt_5_6_0_GCC_64bit-Release/vtkReebGraphTest"

using std::cerr;
using std::cout;
using std::unique_ptr;
using std::remove_if;

using DisplayMode = QtColourRampWidget::DisplayMode;

#ifdef _MSC_VER
#define __PRETTY_FUNCTION__ __FUNCSIG__
#endif

///
/// \brief		Hook into the closing event
/// \param event
/// \since		03-12-2015
/// \author		Dean
///
void MDIParentWindow::closeEvent(QCloseEvent* event)
{
	cout << __PRETTY_FUNCTION__ << endl;

	saveSettings();

	event->accept();
}

///
/// \brief		Saves the existing state of the application
/// \since		03-12-2015
/// \author		Dean
///
void MDIParentWindow::saveSettings(const QString& filename) const
{
	QSettings* settings = nullptr;

	if (filename != "")
	{
		//	Force loading settings from the a specific location
		settings = new QSettings(filename, QSettings::IniFormat);
	}
	else
	{
		//	Restore the settings from the platform default location
		settings = new QSettings("Swansea University: graphics group", "QCDVis");
	}

	//	Window geometry
	settings->setValue("geometry", saveGeometry());

	//	Files etc...
	settings->setValue("recentFiles", getRecentFileStringList());
	settings->setValue("screenshotPath", m_save_to_path.absolutePath());
	settings->setValue("openDirsPath", m_open_from_dir_path.absolutePath());
	settings->setValue("settingsPath", m_session_dir.absolutePath());
	settings->setValue("colourRampsPath", m_colour_ramps_dir.absolutePath());


	//	Grid settings
	settings->setValue("gridVisible", ui->actionShowGrid->isChecked());
	settings->setValue("gridOuterOnly", ui->actionOuterCellsOnly->isChecked());

	//	Set the axis options
	settings->setValue("axisVisible", ui->actionShowAxisLines->isChecked());
	settings->setValue("axisFar", ui->actionShowFarAxisLines->isChecked());
	settings->setValue("axisLabels", ui->actionLabelAxisLines->isChecked());

	//	Set the object rendering settings
	settings->setValue("objectNested", ui->actionShowNestedContours->isChecked());
	settings->setValue("objectNestedCount", static_cast<unsigned int>(m_nested_contour_count));
	settings->setValue("objectSurface", ui->actionShowSurfaces->isChecked());
	settings->setValue("objectWireframe", ui->actionShowWireframe->isChecked());

	//	Store camera settings
	auto quat = ui->widgetArcBallCameraPosition->GetQuaternion();
	settings->setValue("cameraPositionX", quat.x());
	settings->setValue("cameraPositionY", quat.y());
	settings->setValue("cameraPositionZ", quat.z());
	settings->setValue("cameraPositionW", quat.scalar());
	settings->setValue("cameraRadius", ui->widgetArcBallCameraPosition->GetRadius());

	//	skeleton overlay
	settings->setValue("overlayContourTree", ui->actionShowCTSkeleton->isChecked());
	settings->setValue("overlayReebGraph", ui->actionShowReebGraphSkeleton->isChecked());
	settings->setValue("overlayCriticalNodes", ui->actionShowCriticalNodeGlyphs->isChecked());

	//	window (toolbars)
	settings->setValue("windowTopologyToolbar", ui->actionShowContourTreeToolbar->isChecked());

	//	open files
	auto loaded_files = ui->treeViewFieldSelection->GetLoadedFiles();
	QStringList file_list;
	for (auto& file : loaded_files)
	{
		//	Make sure the path is valid
		if (file.exists())
		{
			file_list.append(file.absoluteFilePath());
			cout << "Added file: " << file.absoluteFilePath() << " to restore list." << endl;
		}
		else
		{
			cerr << "Path '" << file.absoluteFilePath() << "' is not valid." << endl;
		}
	}

	settings->setValue("openFiles", file_list);

	//	Make sure the settings are stored before deleting the object
	settings->sync();
	delete settings;
}

///
/// \brief		Restores the previous state of the application
/// \return		QStringList: a list of files that were recently opened
/// \since		03-12-2015
/// \author		Dean
///
QStringList MDIParentWindow::loadSettings(const QString& filename, const bool restore_files)
{
	QSettings* settings = nullptr;

	if (filename != "")
	{
		//	Force loading settings from the a specific location
		settings = new QSettings(filename, QSettings::IniFormat);
	}
	else
	{
		//	Restore the settings from the platform default location
		settings = new QSettings("Swansea University: graphics group", "QCDVis");
	}


#ifndef DISABLE_RESTORE_SETTINGS
	auto geom = settings.value("geometry");
	restoreGeometry(geom.toByteArray());
#endif

	//	directory settings
	m_save_to_path = settings->value("screenshotPath", QStandardPaths::HomeLocation).toString();
	m_open_from_dir_path = settings->value("openDirsPath", QStandardPaths::HomeLocation).toString();
	m_session_dir = settings->value("settingsPath", QStandardPaths::HomeLocation).toString();
	m_colour_ramps_dir = settings->value("colourRampsPath", QStandardPaths::HomeLocation).toString();
	ui->widgetColourRamp->SetDirectory(m_colour_ramps_dir);

	//	grid settings
	ui->actionShowGrid->setChecked(settings->value("gridVisible", true).toBool());
	ui->actionOuterCellsOnly->setChecked(settings->value("gridOuterOnly", true).toBool());

	//	axis options
	ui->actionShowAxisLines->setChecked(settings->value("axisVisible", true).toBool());
	ui->actionShowFarAxisLines->setChecked(settings->value("axisFar", false).toBool());
	ui->actionLabelAxisLines->setChecked(settings->value("axisLabels", true).toBool());

	//	object rendering settings
	ui->actionShowNestedContours->setChecked(settings->value("objectNested", false).toBool());
	m_nested_contour_count = settings->value("objectNestedCount", 10).toUInt();
	ui->actionShowSurfaces->setChecked(settings->value("objectSurface", true).toBool());
	ui->actionShowWireframe->setChecked(settings->value("objectWireframe", false).toBool());

	//	camera settings
	auto cam_x = settings->value("cameraPositionX", 0.0f).toFloat();
	auto cam_y = settings->value("cameraPositionY", 0.0f).toFloat();
	auto cam_z = settings->value("cameraPositionZ", 0.0f).toFloat();
	auto cam_w = settings->value("cameraPositionW", 1.0f).toFloat();
	ui->widgetArcBallCameraPosition->SetQuaternion(QQuaternion(cam_w, cam_x, cam_y, cam_z));
	ui->widgetArcBallCameraPosition->SetRadius(settings->value("cameraRadius", 30.0f).toFloat());

	//	skeleton overlay
	ui->actionShowCTSkeleton->setChecked(settings->value("overlayContourTree", false).toBool());
	ui->actionShowReebGraphSkeleton->setChecked(settings->value("overlayReebGraph", false).toBool());
	ui->actionShowCriticalNodeGlyphs->setChecked(settings->value("overlayCriticalNodes", true).toBool());

	//	window (docks)
	ui->actionShowContourTreeToolbar->setChecked(settings->value("windowTopologyToolbar", true).toBool());

	if (restore_files)
	{
		//	open files
		auto file_list = settings->value("openFiles").toStringList();
		for (auto& file : file_list)
		{
			qDebug() << file_list << endl;

			auto file_info = QFileInfo(file);

			//	Make sure the path is valid
			if (file_info.exists())
			{
				cout << "Loading file: " << file << endl;
				LoadData(file, false);
			}
			else
			{
				cerr << "Path '" << file << "' is not valid." << endl;
			}
		}
	}

	//	We need to return a list of recent files to allow a menu to be created
	auto recentFiles = settings->value("recentFiles", getRecentFileStringList()).toStringList();
	for (auto it = recentFiles.cbegin(); it != recentFiles.cend(); ++it)
	{
		cout << *it << endl;
	}

	//	Make sure the settings are loaded before deleting the object
	settings->sync();
	delete settings;
	return recentFiles;
}

///
/// \brief		Converts the recent file action list to a QStringList for
///				storage
/// \return
/// \since		03-12-2015
/// \author		Dean
///
QStringList MDIParentWindow::getRecentFileStringList() const
{
	QStringList result;

	for (auto it = m_recentFileActions.cbegin();
		 it != m_recentFileActions.cend();
		 ++it)
	{
		//	Extract filename
		auto filename = (*it)->data().toString();

		//	Add the filename (if required)
		if (filename != "null") result.push_back(filename);
	}
	return result;
}

///
/// \brief		Trims excess data from a filepath for display in the UI
/// \param filename
/// \return
/// \since		03-12-2015
/// \author		Dean
///
QString GetShortenedFilename(const QString& filename)
{
	return QFileInfo(filename).fileName();
}

///
/// \brief		Adds a file to the recent list
/// \param newFile
/// \since		03-12-2015
/// \author		Dean
///
void MDIParentWindow::addRecentFile(const QString& newFile)
{
	//	First search to see if the file already exists in the list
	for (auto i = 0; i < MAX_RECENT_FILES; ++i)
	{
		auto currentFilename = m_recentFileActions[i]->data().toString();

		//	If we find a match...
		if (currentFilename == newFile)
		{
			//	...take the item from its existing position
			//	and place at the front
			auto item = m_recentFileActions.takeAt(i);
			m_recentFileActions.push_front(item);

			//	Exit
			return;
		}
	}

	//	If not move everything else back a step and add at the front
	for (auto i = MAX_RECENT_FILES - 1; i > 0; --i)
	{
		//	Convenince accessors for items
		auto currentAction = m_recentFileActions[i];
		auto prevAction = m_recentFileActions[i-1];

		//	Copy the text and file name from the previous item
		currentAction->setText(prevAction->text());
		currentAction->setData(prevAction->data());
		currentAction->setToolTip(prevAction->toolTip());
		currentAction->setStatusTip(prevAction->statusTip());

		//	If the item is "null" hide it, else set it as visible
		currentAction->setVisible((currentAction->text() != "null"));
	}

	//	Now at the new file at the start of the list
	auto firstAction = m_recentFileActions[0];
	firstAction->setText(GetShortenedFilename(newFile));
	firstAction->setData(newFile);
	firstAction->setToolTip(newFile);
	firstAction->setStatusTip(newFile);
}

///
/// \brief		Creates a list of actions for a recent file list, but does NOT
///				add them to the form
/// \return
/// \since		03-12-2015
/// \author		Dean
///
MDIParentWindow::RecentFileActionList MDIParentWindow::createRecentFileItems(const QStringList& recentFiles)
{
	RecentFileActionList result;

	for (auto i = 0; i < MAX_RECENT_FILES; ++i)
	{
		//	Create a new item
		auto newItem = new QAction(this);

		//	Default values
		newItem->setText("null");
		newItem->setData("null");
		newItem->setToolTip("null");
		newItem->setStatusTip("null");

		//	See if a recent file can be restored from the stored settings
		if (i < recentFiles.size())
		{
			auto filePath = recentFiles[i];
			QFileInfo fileInfo(filePath);

			//	Make sure the file is still valid
			if (fileInfo.exists())
			{
				newItem->setText(GetShortenedFilename(filePath));
				newItem->setData(filePath);
				newItem->setToolTip(filePath);
				newItem->setStatusTip(filePath);

			}
		}

		//	Hide invalid items
		newItem->setVisible(newItem->text() != "null");

		//	Use a lambda to respond to the user clicking the item
		connect(newItem, &QAction::triggered,
				[=]()
		{
			auto filename = newItem->data().toString();

			cout << "Clicked recent item " << i << endl;
			cout << "Filename: " << filename << endl;

			if (filename != "null")	LoadData(newItem->data().toString());
		});

		//	Add to the list
		result.push_back(newItem);
	}
	return result;
}

///
/// \brief		Creates a cache folder for the selected file (in it's folder)
/// \param filename
/// \return
/// \author		Dean
/// \since		28-08-2015
///
QDir MDIParentWindow::createCacheDir(const QString &filename)
{
	//	We won't use this for now...we no longer cache 3d volumes from the
	//	main exe.  We may use this to cache meshes in the future...

	return QDir();


	/*
	QFileInfo fileInfo(filename);

	//	Find the path to the file
	QDir currentPath = fileInfo.dir();

	//	Get the name of the file (no extension)
	//QString currentFilename = fileInfo.baseName();

	//cout << "Current path: " << currentPath.path().toStdString() << endl;
	//cout << "Current file: " << currentFilename.toStdString() << endl;

	//	Full path to the desired cache directory
	QString cacheDir = currentPath.path() + "/cache/";
	//cout << "Cache dir: " << cacheDir.toStdString() << endl;

	if (QDir(cacheDir).exists())
	{
		//	Directory alread exists, return it
		return QDir(cacheDir);
	}
	else
	{
		//	Directory doesn't exist, create it and return it
		bool success = currentPath.mkdir("cache");

		assert(success);

		return QDir(cacheDir);
	}
	*/
}

///
/// \brief		Tests if a file extension is recognised as a 3D format
/// \param ext
/// \return
/// \since		03-12-2015
/// \author		Dean
///
bool MDIParentWindow::is3dFileFormat(const QString& ext) const
{
	if (ext == "vol1") return true;
	if (ext == "txt") return true;

	return false;
}

///
/// \brief		Tests if a file extension is recognised as a 3D format
/// \param ext
/// \return
/// \since		03-12-2015
/// \author		Dean
///
bool MDIParentWindow::is4dFileFormat(const QString& ext) const
{
	if (ext == "hvol1") return true;

	return false;
}

///
/// \brief		Methods that attempts to load a file, regardless of if the data
///				is a 3D or 4D format
/// \param filename
/// \return
/// \since		03-12-2015
/// \author		Dean
///
bool MDIParentWindow::LoadData(const QString& filename, const bool add_to_recent_files)
{
	bool result = false;

	//	First, check the file exists and extract the extension
	QFileInfo fileInfo(filename);
	if (!fileInfo.exists()) return false;
	auto extension = fileInfo.suffix();

	cout << "File extension is: " << extension << endl;

	//	Attempt to load the file by determining the data type
	if (is3dFileFormat(extension))
	{
		result = Load3dData(filename);
	}
	else if (is4dFileFormat(extension))
	{
		result = Load4dData(filename);
	}

	//	If the file correctly loaded we can add to the recent list
	if (result)
	{
		//	If restoring from a previous session we won't do this
		if (add_to_recent_files)
			addRecentFile(filename);
	}
	return result;
}

///
/// \brief MDIParentWindow::Load3dData
/// \param filename
/// \since		03-11-2015
/// \author		Dean
///
bool MDIParentWindow::Load3dData(const QString& filename)
{
	//	Create the 3D data set
	HeightField3D *newHeightField3d =
			m_dataModel.LoadData3d(filename.toStdString(), true);

	//	Make sure it loaded correctly
	if (newHeightField3d != nullptr)
	{
		//	Add to the variable list
		ui->treeViewFieldSelection->AddHeightField3(newHeightField3d, filename);

		return true;
	}
	return false;
}

///
/// \brief		Handles loading of a 4d data set, and associated post load
///				events.
/// \param		filename
/// \author		Dean
/// \since		28-08-2015
///
bool MDIParentWindow::Load4dData(const QString &filename)
{
	//	Launch our progress dialog but return execution to this methed
	QtProgressDialog loadingDialog(this);
	loadingDialog.setModal(true);
	loadingDialog.show();

	//	Load the 4D data
	loadingDialog.SetCurrentActionText("Loading 4d data");
	HeightField4D *newHeightField4d =
			m_dataModel.LoadData4d(filename.toStdString());
	assert(newHeightField4d != nullptr);

	//	Perform the sub-volume slicing and display histograms
	//	x
	loadingDialog.SetCurrentActionText("Slicing X axis");
	newHeightField4d->GenerateVolumesYZT();
	loadingDialog.IncrementProgressBar(20);

	//	y
	loadingDialog.SetCurrentActionText("Slicing Y axis");
	newHeightField4d->GenerateVolumesXZT();
	loadingDialog.IncrementProgressBar(20);

	//	z
	loadingDialog.SetCurrentActionText("Slicing Z axis");
	newHeightField4d->GenerateVolumesXYT();
	loadingDialog.IncrementProgressBar(20);

	//	t
	loadingDialog.SetCurrentActionText("Slicing T axis");
	newHeightField4d->GenerateVolumesXYZ();
	loadingDialog.IncrementProgressBar(20);

	//	Refresh the volume selection tree view
	loadingDialog.SetCurrentActionText("Finalizing");
	ui->treeViewFieldSelection->AddHeightField4(newHeightField4d, filename);
	loadingDialog.IncrementProgressBar(10);

	return true;
}

///
/// \brief		Sets up any additional toolbars for the mainform
/// \author		Dean
/// \since		05-08-2015
///
void MDIParentWindow::setupToolbars()
{
	try
	{
#ifndef HIDE_BOOKMARK_TOOLBAR
		//	Create a bookmark menu
		m_isovalueBookmarWidget = new QtIsovalueBookmarkWidget(
					m_dataModel.GetBookmarkList(),
					ui->toolBarBookmarks);
		assert(m_isovalueBookmarWidget != nullptr);

		//	Enable the title and add the bookmark menu to the toolbar
		m_isovalueBookmarWidget->SetTitleHidden(false);
		ui->toolBarBookmarks->addWidget(m_isovalueBookmarWidget);

		//	Respond to the user interacting with the bookmark toolbar
		connect(m_isovalueBookmarWidget, &QtIsovalueBookmarkWidget::OnBookmarkAdded,
				[&](const IsovalueBookmark &bookmark)
		{
			//	Add the bookmark to the list of defined values
			m_dataModel.AddBookmark(bookmark);

			//	Update the bookmark list in the global isovalue slider
			ui->widgetIsovalueSlider->SetBookmarkList(m_dataModel.GetBookmarkList());

			//	TODO: update the bookmarklist in each of the child windows
		});


		connect(m_isovalueBookmarWidget, &QtIsovalueBookmarkWidget::OnBookmarkRemoved,
				[&](const IsovalueBookmark &bookmark)
		{
			//	Remove the bookmark
			m_dataModel.RemoveBookmark(bookmark);

			//	Update the bookmark list in the global isovalue slider
			ui->widgetIsovalueSlider->SetBookmarkList(m_dataModel.GetBookmarkList());

			//	TODO: update the bookmarklist in each of the child windows
		});

		connect(m_isovalueBookmarWidget, &QtIsovalueBookmarkWidget::OnBookmarkSelected,
				[&](const IsovalueBookmark &bookmark)
		{
			ui->widgetIsovalueSlider->SetIsovalue(bookmark.GetIsovalue());
		});
#else
		ui->toolBarBookmarks->setHidden(true);
#endif
	}
	catch (exception &ex)
	{
		cerr << "Exception!  " << ex.what() << " in func: " << __func__;
		cerr << " (file: " << __FILE__ << ", line: " << __LINE__ << ".";
		cerr << endl;
	}
}

///
/// \brief MDIParentWindow::OnArcBallCameraPositionScroll
/// \param event
/// \since		16-07-2015
/// \author		Dean
///
void MDIParentWindow::OnArcBallCameraPositionScroll()
{
	cout << "Responded to signal" << endl;
	//if (event->orientation() == Qt::Vertical)
	//{
	auto subWindows = ui->mdiArea->subWindowList();
	for (auto& window : subWindows)
	{
		//	Only send a message to windows that are of the correct
		//	isosurface widget type
		if(auto isosurfaceWindow = dynamic_cast<MDIChildWindow*>(window->widget()))
		{
			isosurfaceWindow->SetFloatingCameraPosition(
						ui->widgetArcBallCameraPosition->GetQuaternion(),
						ui->widgetArcBallCameraPosition->GetRadius());
		}
	}
	//}
	//event->accept();
}

///
/// \brief		Responds to the user double clicking a volume slice
/// \param item
/// \author		Dean
/// \since		15-07-2015
///
//void MDIParentWindow::OnUserClickedHeightField3dItem(QtHeightField3dItem *item)
//{
//	HeightField3D *heightField3d = item->GetHeightField3d();

//	//	TODO: For now just launch a new window...at some point detect if the
//	//	relevent window is already open
//	AddChildWindow(heightField3d, "");
//}

void MDIParentWindow::onCameraArcBallRotationChanged()
{
	cout << "Rotation changed" << endl;

	for (unsigned long i = 0; i < ui->mdiArea->subWindowList().size(); ++i)
	{
		QMdiSubWindow *window = ui->mdiArea->subWindowList()[i];

		//	If the window shows an isosurface, we can rotate it
		if (auto isosurfaceWindow = dynamic_cast<MDIChildWindow*>(window->widget()))
		{
			isosurfaceWindow->SetFloatingCameraPosition(
						ui->widgetArcBallCameraPosition->GetQuaternion(),
						ui->widgetArcBallCameraPosition->GetRadius());
		}
	}
}

///
/// \brief		Updates memory usage statistics etc
///	\since		08-07-2015
/// \author		Dean
///
void MDIParentWindow::updateTimerTriggered()
{
	float totVM = ProcessInfo::GetTotalVirtualMemory() / 1024.0 / 1024.0 / 1024.0;
	float availVM = ProcessInfo::GetAvailableVirtualMemory() / 1024.0 / 1024.0/ 1024.0;
	float currentVW = ProcessInfo::GetVirtualMemoryUsedByProcess() / 1024.0 / 1024.0 / 1024.0;

	float totRAM = ProcessInfo::GetTotalRAM() / 1024.0 / 1024.0 / 1024.0;
	float availRAM = ProcessInfo::GetAvailableRAM() / 1024.0 / 1024.0 / 1024.0;
	float currentRAM = ProcessInfo::GetRAMUsedByProcess() / 1024.0 / 1024.0 / 1024.0;

	m_labelPhysicalMemory->setText(QString("RAM: %1GB / %2GB / %3GB")
								   .arg(QString::number(currentRAM, 'f', 2))
								   .arg(QString::number(availRAM, 'f', 2))
								   .arg(QString::number(totRAM, 'f', 2)));
	m_labelVirtualMemory->setText(QString("VM: %1GB / %2GB / %3GB")
								  .arg(QString::number(currentVW, 'f', 2))
								  .arg(QString::number(availVM, 'f', 2))
								  .arg(QString::number(totVM, 'f', 2)));
}

///
/// \brief		Responds to the user moving the global isovalue slider
/// \author		Dean
/// \since		13-07-2015
///
void MDIParentWindow::onGlobalIsovalueSliderChanged()
{
	float isovalueNorm = ui->widgetIsovalueSlider->GetNormalizedIsovalue();
	float isovalue = ui->widgetIsovalueSlider->GetIsovalue();

	//	Update the sliders to show the current value
	ui->widgetHistogramX->SetCurrentNormalisedIsovalue(isovalueNorm);
	ui->widgetHistogramY->SetCurrentNormalisedIsovalue(isovalueNorm);
	ui->widgetHistogramZ->SetCurrentNormalisedIsovalue(isovalueNorm);
	ui->widgetHistogramT->SetCurrentNormalisedIsovalue(isovalueNorm);

	//	Update the isovalue for creation of bookmarks
	//m_isovalueBookmarWidget->SetCurrentIsovalue(isovalue);

	//	Tell each window to update with the current ABSOLUTE isovalue,
	//	normalization to each windows scale will be implemented by the windows
	for (unsigned long i = 0; i < ui->mdiArea->subWindowList().size(); ++i)
	{
		QMdiSubWindow *window = ui->mdiArea->subWindowList()[i];
		((MDIChildWindow*)window->widget())->SetCurrentIsovalue(isovalue);
	}
}

///
/// \brief		Sets up connections between the global isovalue colour ramp
///				and colour ramps in any child windows
/// \since		11-12-2015
/// \author		Dean
///
void MDIParentWindow::setupColourRampConnections()
{
	//	Set event handlers from global to local sub-views
	connect(ui->widgetColourRamp, &QtColourRampWidget::OnDisplayModeChanged,
			[=](const QtColourRampWidget::DisplayMode& mode)
	{
		auto windowList = ui->mdiArea->subWindowList();
		for (auto it = windowList.begin(); it != windowList.end(); ++it)
		{
			auto window = *it;
			auto childColourRampWidget =
					((MDIChildWindow*)window->widget())->GetColourRampWidget();
			assert(childColourRampWidget != nullptr);

			cout << "Colour ramp mode changed."<< endl;
			childColourRampWidget->SetDisplayMode(mode);
		}
	});

	connect(ui->widgetColourRamp, &QtColourRampWidget::OnPositiveColourChanged,
			[=](const QColor& colour)
	{
		auto windowList = ui->mdiArea->subWindowList();
		for (auto it = windowList.begin(); it != windowList.end(); ++it)
		{
			auto window = *it;
			auto childColourRampWidget =
					((MDIChildWindow*)window->widget())->GetColourRampWidget();
			assert(childColourRampWidget != nullptr);

			cout << "Pos colour changed to: " << colour << endl;
			childColourRampWidget->SetPositiveColour(colour);
		}
	});
	connect(ui->widgetColourRamp, &QtColourRampWidget::OnNegativeColourChanged,
			[=](const QColor& colour)
	{
		auto windowList = ui->mdiArea->subWindowList();
		for (auto it = windowList.begin(); it != windowList.end(); ++it)
		{
			auto window = *it;
			auto childColourRampWidget =
					((MDIChildWindow*)window->widget())->GetColourRampWidget();
			assert(childColourRampWidget != nullptr);

			cout << "Neg colour changed to: " << colour << endl;
			childColourRampWidget->SetNegativeColour(colour);
		}
	});
	connect(ui->widgetColourRamp, &QtColourRampWidget::OnUniversalColourChanged,
			[=](const QColor& colour)
	{
		auto windowList = ui->mdiArea->subWindowList();
		for (auto it = windowList.begin(); it != windowList.end(); ++it)
		{
			auto window = *it;
			auto childColourRampWidget =
					((MDIChildWindow*)window->widget())->GetColourRampWidget();
			assert(childColourRampWidget != nullptr);

			cout << "Universal colour changed to: " << colour << endl;
			childColourRampWidget->SetUniversalColour(colour);
		}
	});
	connect(ui->widgetColourRamp, &QtColourRampWidget::OnColourTableChanged,
			[=](const QtColourRampWidget::ColourTable& colourTable)
	{
		auto windowList = ui->mdiArea->subWindowList();
		for (auto it = windowList.begin(); it != windowList.end(); ++it)
		{
			auto window = *it;
			auto childColourRampWidget =
					((MDIChildWindow*)window->widget())->GetColourRampWidget();
			assert(childColourRampWidget != nullptr);

			//	Hacky way of normalising isovalues from child view...
			auto childHeightfield = ((MDIChildWindow*)window->widget())->GetActiveHeightfield3();
			assert(childHeightfield != nullptr);

			if (ui->treeViewFieldSelection->IsLockedToGlobalView(childHeightfield))
			{
				//	Normalize the range of the slices isovalues to the range of the
				//	global isovalue
				auto normMin = normalize(ui->widgetIsovalueSlider->GetMinimumValue(),
										 ui->widgetIsovalueSlider->GetMaximumValue(),
										 childHeightfield->GetMinHeight());
				auto normMax = normalize(ui->widgetIsovalueSlider->GetMinimumValue(),
										 ui->widgetIsovalueSlider->GetMaximumValue(),
										 childHeightfield->GetMaxHeight());

				//	We only want a truncated part of the colour table (covering
				//	the isovalues present in the child view)
				auto truncatedColourTable = ui->widgetColourRamp->GetColourTable(normMin, normMax);
				cout << "Universal colour changed to: " << truncatedColourTable << endl;

				childColourRampWidget->SetColourTable(truncatedColourTable);
			}
		}
	});
}

void MDIParentWindow::initializeForm()
{
	setupToolbars();

	setupColourRampConnections();

	//	Memory monitoring
	m_labelPhysicalMemory = new QLabel("00 / 00 / 00", this);
	m_labelPhysicalMemory->setToolTip("Current Usage / Available / Total");
	m_labelVirtualMemory = new QLabel("00 / 00 / 00", this);
	m_labelVirtualMemory->setToolTip("Current Usage / Available / Total");
	m_updateTimer = new QTimer(this);

	QObject::connect(m_updateTimer, SIGNAL(timeout()), this, SLOT(updateTimerTriggered()));

	m_updateTimer->start(500);
	ui->statusbar->addPermanentWidget(m_labelPhysicalMemory);
	ui->statusbar->addPermanentWidget(m_labelVirtualMemory);

	connect(ui->widgetArcBallCameraPosition, SIGNAL(RotationChanged()),
			this, SLOT(onCameraArcBallRotationChanged()));

	connect(ui->widgetIsovalueSlider, SIGNAL(OnIsovalueChanged()),
			this, SLOT(onGlobalIsovalueSliderChanged()));

	connect(ui->treeViewFieldSelection, &QtVolumeSelectionTreeView::OnDoubleClickedHeightField3,
			[=](QtVolumeStandardItem* item)
	{
		auto heightField = item->getHeightfield3();
		assert(heightField != nullptr);

		auto ensemble = QString::fromStdString(heightField->GetEnsembleName());
		auto conf = QString::fromStdString(heightField->GetConfigurationName());
		auto field = QString::fromStdString(heightField->GetFieldName());
		auto cool = QString("%1").arg(heightField->GetCoolingSlice());
		auto slice = QString("%1=%2").arg(QString::fromStdString(heightField->GetFixedVariableName()))
				.arg(heightField->GetFixedVariableValue());

		AddChildWindow(heightField, QString("%1/%2/%3/cool%4 [%5]")
					   .arg(ensemble).arg(conf).arg(field).arg(cool).arg(slice),
					   item->getReebGraphFilename());
	});

	//connect(ui->treeWidgetVolumeSlices, SIGNAL(OnUserDoubleClickedHeightField3dItem(QtHeightField3dItem*)),
	//		this, SLOT(OnUserClickedHeightField3dItem(QtHeightField3dItem*)));
	/*
	connect(ui->treeWidgetVolumeSlices, &QtVolumeSliceTreeWidget::OnUserLeftClickedHeightField3dItem,
			[&](QtHeightField3dItem* item)
	{
		cout << "User left clicked Heightfield" << endl;

		//	Set the range of the periodic boundaries sliders
		ui->widgetOffset->SetAxisRanges(item->GetHeightField3d()->GetBaseDimX(),
										item->GetHeightField3d()->GetBaseDimY(),
										item->GetHeightField3d()->GetBaseDimZ(),
										0);
	});
	*/
	connect(ui->widgetArcBallCameraPosition, SIGNAL(RadiusChanged()),
			this, SLOT(OnArcBallCameraPositionScroll()));

	//	Respond to changes in the offset slider window
	/*
	connect(ui->widgetOffset, &QtOriginShiftWidget::OnOffsetChange,
			[&](unsigned long x,
			unsigned long y,
			unsigned long z,
			unsigned long t)
	{
		cout << "Offset changed to: " << x << " " << y << " " << z << " " << t;
		cout << endl;


	});
	*/
}

MDIParentWindow::MDIParentWindow(QWidget *parent) :
	QMainWindow(parent),
	ui(new Ui::MDIParentWindow)
{
	ui->setupUi(this);

	initializeForm();
}

MDIParentWindow::MDIParentWindow(DataModel& dataModel, QWidget *parent) :
	QMainWindow(parent),
	ui(new Ui::MDIParentWindow),
	m_dataModel{dataModel}
{
	ui->setupUi(this);

	initializeForm();

	//	Add all loaded 3D volumes to the interface
	/*
	for (unsigned long i = 0; i < m_dataModel.GetHeightField3dCount(); ++i)
	{
		//	Make sure the volume is correctly loaded before it is displayed
		HeightField3D *temp = m_dataModel.GetHeightField3d(i);
		if (temp != nullptr)
			AddChildWindow(temp, "");
	}
	*/

	//	Hide these, until they are complete at least
	//ui->dockWidgetCameraSelection->hide();
	//ui->dockWidgetPeriodicBoundaries->hide();

	//	Load stored settings and return a list of recent file paths
	QStringList recentFileList;
#ifdef NO_RESTORE_FILES
	recentFileList = loadSettings("", false);
#else
	recentFileList = loadSettings("");
#endif
	//	Create the recent file action list and add to the File menu
	m_recentFileActions = createRecentFileItems(recentFileList);
	auto fileMenu = ui->menu_FIle;
	auto exitItem = fileMenu->actions()[fileMenu->actions().indexOf(ui->actionE_xit)];
	fileMenu->insertActions(exitItem, m_recentFileActions);
	fileMenu->insertSection(exitItem,"");

	//	Respond to the user (un)checking a heightfield in the hierarchy,
	//	adding or removing from the histogram as necessary
	connect(ui->treeViewFieldSelection,
			&QtVolumeSelectionTreeView::OnItemChangedHeightField4,
			[&](HeightField4D* heightField4d,
			const Qt::CheckState& checked,
			const QColor& colour)
	{
		SuperarcDistribution *distX = heightField4d->GetSuperarcDistribution(
					HeightField4D::SpaceTimeDomain::DomainX);
		SuperarcDistribution *distY = heightField4d->GetSuperarcDistribution(
					HeightField4D::SpaceTimeDomain::DomainY);
		SuperarcDistribution *distZ = heightField4d->GetSuperarcDistribution(
					HeightField4D::SpaceTimeDomain::DomainZ);
		SuperarcDistribution *distT = heightField4d->GetSuperarcDistribution(
					HeightField4D::SpaceTimeDomain::DomainT);
		assert (distX != nullptr);
		assert (distY != nullptr);
		assert (distZ != nullptr);
		assert (distT != nullptr);

		if (checked)
		{
			ui->widgetHistogramX->AddSuperarcDistribution(distX, colour);
			ui->widgetHistogramY->AddSuperarcDistribution(distY, colour);
			ui->widgetHistogramZ->AddSuperarcDistribution(distZ, colour);
			ui->widgetHistogramT->AddSuperarcDistribution(distT, colour);
		}
		else
		{
			ui->widgetHistogramX->RemoveSuperarcDistribution(distX);
			ui->widgetHistogramY->RemoveSuperarcDistribution(distY);
			ui->widgetHistogramZ->RemoveSuperarcDistribution(distZ);
			ui->widgetHistogramT->RemoveSuperarcDistribution(distT);
		}
	});

	//	Allow computation of the Reeb graph from the menu
	connect(ui->treeViewFieldSelection,
			&QtVolumeSelectionTreeView::OnReebGraphRequested,
			[&](QtVolumeStandardItem* item)
	{
		auto heightfield_file = item->data(ROLE_PATH_TO_HEIGHTFIELD_FILE).toString();
		auto file_info = QFileInfo(heightfield_file);
		assert(file_info.exists());
		auto out_dir = file_info.absoluteDir();

		if (executeReebCommand(heightfield_file, out_dir.absolutePath()))
		{
			auto path_to_rb_file = file_info.absolutePath() + "/" + file_info.baseName() + ".rb";
			qDebug() << "Path to computed Reeb file should be:" << path_to_rb_file;

			item->setData(path_to_rb_file, ROLE_PATH_TO_REEB_GRAPH_FILE);
		}
	});


	//	When an item is added/removed to the histogram widget it will trigger
	//	the following event (in all histograms, not just X as below).  We can
	//	use this to update the min and max of the clour ramp widget and
	//	isovalue slider
	connect(ui->widgetHistogramX, &QtIsovalueHistogramWidget::OnMinMaxUpdate,
			[&](const float& min, const float& max)
	{
		ui->widgetIsovalueSlider->SetIsovalueRange(min, max);
		ui->widgetColourRamp->SetIsovalueRange(min, max);
	});

	//	Handle the user changing the search path for colour ramp files
	connect(ui->widgetColourRamp, &QtColourRampWidget::OnColourRampDirChanged,
			[&](const QDir& new_dir)
	{
		//	Update the local reference
		m_colour_ramps_dir = new_dir;

		//	Update all open subviews
		auto windowList = ui->mdiArea->subWindowList();
		for (auto& win : windowList)
		{
			auto mdiWindow = dynamic_cast<MDIChildWindow*>(win->widget());
			if (mdiWindow != nullptr)
			{
				//	Univariate windows only, not multivariate
				auto colour_ramp_widget = mdiWindow->GetColourRampWidget();
				assert(colour_ramp_widget != nullptr);

				colour_ramp_widget->SetDirectory(m_colour_ramps_dir);
			}
		}
	});

	//	Responds to the user requesting a JCN from multiple heightfields
	connect(ui->treeViewFieldSelection, &QtVolumeSelectionTreeView::OnJcnDialogRequested,
			[&](const QVector<QtVolumeStandardItem*>& items)
	{
		qDebug() << "Respnding to JCN request";
		qDebug() << "JCN requested for" << items.size() << "items.";
		auto jcn_dialog = new QtJcnDialog(this);
		jcn_dialog->setGeneratingItems(items);
		if (jcn_dialog->exec() == QDialog::Accepted)
		{
			//	Get arguments for running the JCN on the threre fields
			auto args_f0f1 = jcn_dialog->getParamsF0F1();
			auto args_f0 = jcn_dialog->getParamsF0();
			auto args_f1 = jcn_dialog->getParamsF1();

			//	Execute the command for each discrete field
			auto result_f0f1 = QProcess::execute(JCN_EXE, args_f0f1);
			auto result_f0 = QProcess::execute(JCN_EXE, args_f0);
			auto result_f1 = QProcess::execute(JCN_EXE, args_f1);

			if ((result_f0f1 == 0) && (result_f0 == 0) && (result_f1 == 0))
			{
				auto jcnWindow = new QtJointContourNetForm(this);
				jcnWindow->SetFloatingCameraPosition(ui->widgetArcBallCameraPosition->GetQuaternion(),
													 ui->widgetArcBallCameraPosition->GetRadius());

				//	Load the slabs from the output directories
				jcnWindow->setSourceDirectoryF0(jcn_dialog->getSlabsDirF0().absolutePath());
				jcnWindow->setSourceDirectoryF1(jcn_dialog->getSlabsDirF1().absolutePath());
				jcnWindow->setSourceDirectoryF0F1(jcn_dialog->getSlabsDirF0f1().absolutePath());

				//	Set titles for the input fields
				jcnWindow->setTitleF0(QString("%1 at %2 cools (slabsize: %3)")
									  .arg(items[0]->getFieldname())
						.arg(items[0]->getHeightfield3()->GetCoolingSlice())
						.arg(jcn_dialog->getSlabSizeF0()));
				jcnWindow->setTitleF1(QString("%1 at %2 cools (slabsize: %3)")
									  .arg(items[1]->getFieldname())
						.arg(items[1]->getHeightfield3()->GetCoolingSlice())
						.arg(jcn_dialog->getSlabSizeF1()));

				connect(ui->widgetArcBallCameraPosition, &ArcBallWidget::RotationChanged,
						[=]()
				{
					jcnWindow->SetFloatingCameraPosition(
								ui->widgetArcBallCameraPosition->GetQuaternion(),
								ui->widgetArcBallCameraPosition->GetRadius());
				});
				connect(ui->widgetArcBallCameraPosition, &ArcBallWidget::RadiusChanged,
						[=]()
				{
					jcnWindow->SetFloatingCameraPosition(
								ui->widgetArcBallCameraPosition->GetQuaternion(),
								ui->widgetArcBallCameraPosition->GetRadius());
				});

				QMdiSubWindow *newSubWindow = ui->mdiArea->addSubWindow(jcnWindow);
				//newSubWindow->setWindowTitle(QString("JCN - [%1]").arg(jcnWindow->getSourceDirectory()));
				newSubWindow->show();
			}
			else
			{
				QMessageBox::critical(this,
									  "Error",
									  QString("There was an error executing the JCN command."));
			}

		}
	});

#ifdef HIDE_DEBUG_DOCK_AREA
	ui->dockWidgetDebug->hide();
#endif
}

///
/// \brief		Respond to the user scrolling the mouse in the child windows
/// \param delta
/// \author		Dean
/// \since		04-08-2015
///
void MDIParentWindow::onChildWindowSurfaceMouseWhellScroll(int delta)
{
	cout << "Child window mouse wheel scrolled (delta=" << delta << ").";
	cout << endl;

	//	Tell the arc ball to change radius
	ui->widgetArcBallCameraPosition->StepRadius(delta);
}

void MDIParentWindow::AddChildWindow(HeightField3D* heightField3D,
									 const QString& windowTitle,
									 const QString& reeb_graph_file)
{
	try
	{
		//	Create the window and store a local reference to it
		MDIChildWindow *newWindow = new MDIChildWindow(heightField3D,
													   reeb_graph_file,
													   this);
		//m_childWindows.push_back(newWindow);

		//QWidget *newWindow = new QWidget(this);

		//	Add the window (let Qt handle memory management of the new window)
		QMdiSubWindow *newSubWindow =
				ui->mdiArea->addSubWindow(newWindow);
		//newSubWindow->setAttribute(Qt::WA_DeleteOnClose);
		newWindow->SetMdiWrapper(newSubWindow);
		newSubWindow->setWindowTitle(windowTitle);

		//	Respond to user mouse interaction in the new window
		connect(newWindow,SIGNAL(OnSurfaceRenderMouseScroll(int)),
				this,SLOT(onChildWindowSurfaceMouseWhellScroll(int)));

		//	SHow or hide the topology widget
		auto contourTreeDockWidget = newWindow->GetContourTreeDockWidget();
		assert(contourTreeDockWidget != nullptr);
		contourTreeDockWidget->setVisible(ui->actionShowContourTreeToolbar->isChecked());

		//	TODO: think about delegating this to the child window (for now
		//	we'll just separate the code into a lambda for later refactoring)
		auto initializeColourRamps = [&]()
		{
			auto childColourRampWidget = newWindow->GetColourRampWidget();
			assert(childColourRampWidget != nullptr);

			//	Set the search directory
			childColourRampWidget->SetDirectory(m_colour_ramps_dir);

			//	Set properties of child view isovalue slider from the parent form
			childColourRampWidget->SetDualColours(ui->widgetColourRamp->GetDualColours());
			childColourRampWidget->SetUniversalColour(ui->widgetColourRamp->GetUniversalColour());

			if (ui->treeViewFieldSelection->IsLockedToGlobalView(heightField3D))
			{
				//	Normalize the range of the slices isovalues to the range of the
				//	global isovalue
				auto normMin = normalize(ui->widgetIsovalueSlider->GetMinimumValue(),
										 ui->widgetIsovalueSlider->GetMaximumValue(),
										 heightField3D->GetMinHeight());
				auto normMax = normalize(ui->widgetIsovalueSlider->GetMinimumValue(),
										 ui->widgetIsovalueSlider->GetMaximumValue(),
										 heightField3D->GetMaxHeight());

				//	We can then clone only the parts of the colour table in the range
				//	of the global isovalue
				childColourRampWidget->SetColourTable(ui->widgetColourRamp->GetColourTable(normMin, normMax));
			}
			else
			{
				//	Just use the whole colour table
				childColourRampWidget->SetColourTable(ui->widgetColourRamp->GetColourTable());
			}
			childColourRampWidget->SetDisplayMode(ui->widgetColourRamp->GetDisplayMode());
		};

		//	TODO: think about delegating this to the child window (for now
		//	we'll just separate the code into a lambda for later refactoring)
		auto initializeIsosurfaceWidget = [&]()
		{
			auto childIsosurfaceWidget = newWindow->GetSurfaceRenderWidget();
			assert(childIsosurfaceWidget != nullptr);

			//	Set the initial position of the camera
			childIsosurfaceWidget->SetFloatingCameraPosition(ui->widgetArcBallCameraPosition->GetQuaternion(),
															 ui->widgetArcBallCameraPosition->GetRadius());

			//	Set the grid options
			childIsosurfaceWidget->SetRenderGrid(ui->actionShowGrid->isChecked());
			childIsosurfaceWidget->SetRenderOuterGridOnly(ui->actionOuterCellsOnly->isChecked());

			//	Set the axis options
			childIsosurfaceWidget->SetRenderAxisLines(ui->actionShowAxisLines->isChecked());
			childIsosurfaceWidget->SetRenderFarAxisLines(ui->actionShowFarAxisLines->isChecked());
			childIsosurfaceWidget->SetRenderAxisLabels(ui->actionLabelAxisLines->isChecked());

			//	Set the object rendering settings
			childIsosurfaceWidget->SetNestedContours(ui->actionShowNestedContours->isChecked());
			childIsosurfaceWidget->SetNestedContourCount(m_nested_contour_count);
			childIsosurfaceWidget->SetRenderSurfaces(ui->actionShowSurfaces->isChecked());
			childIsosurfaceWidget->SetRenderWireframe(ui->actionShowWireframe->isChecked());
			childIsosurfaceWidget->SetRenderContourTreeSkeleton(ui->actionShowCTSkeleton->isChecked());
			childIsosurfaceWidget->SetRenderReebGraphSkeleton(ui->actionShowReebGraphSkeleton->isChecked());
			childIsosurfaceWidget->SetRenderSkeletonGlyphs(ui->actionShowCriticalNodeGlyphs->isChecked());
		};

		//	TODO: think about delegating this to the child window (for now
		//	we'll just separate the code into a lambda for later refactoring)
		auto initializeIsovalueSlider = [&]()
		{
			auto childIsovalueSlider = newWindow->GetIsovalueSliderWidget();
			assert(childIsovalueSlider != nullptr);

			//	Set the target isovalue from global...
			childIsovalueSlider->SetIsovalue(ui->widgetIsovalueSlider->GetIsovalue());
		};

		//	TODO: think about delegating this to the child window (for now
		//	we'll just separate the code into a lambda for later refactoring)
		auto initializeSignalsAndSlots = [&]()
		{
			connect(newWindow, &MDIChildWindow::UserClickedPrevCoolButton,
					[&](MDIChildWindow* window, HeightField3D* const currentCoolingSlice)
			{
				cout << "User is requesting the prev cooling slice in a child "
						"window."
					 << "  Current cooling slice is "
					 << currentCoolingSlice->GetCoolingSlice() << "."
					 << endl;

				auto prevSlice = ui->treeViewFieldSelection->GetPrevCoolingSlice(currentCoolingSlice);
				if (prevSlice != nullptr)
				{
					//	Extract the heightfield data from the item
					auto heightfield_data = prevSlice->data(ROLE_HEIGHT_FIELD_3);
					auto heightfield = toPtr<HeightField3D>(heightfield_data);
					assert(heightfield != nullptr);

					//	Extract the Reeb graph file (which may well be empty)
					auto reeb_file = prevSlice->data(ROLE_PATH_TO_REEB_GRAPH_FILE).toString();
					window->SetHeightfield(heightfield, reeb_file);
				}
				else
				{
					//	Unable to find the next slice - allow the user to load
					//	one
					auto result = QMessageBox::question(this,
														"The previous cooling slice could not be located",
														"The previous cooling slice could not be located"
														", would you like to load one?");

					if (result == QMessageBox::Yes)
					{
						//	Allow the user to load a data set
					}
				}
			});

			connect(newWindow, &MDIChildWindow::UserClickedNextCoolButton,
					[&](MDIChildWindow* window, HeightField3D* const currentCoolingSlice)
			{
				cout << "User is requesting the next cooling slice in a child "
						"window."
					 << "  Current cooling slice is "
					 << currentCoolingSlice->GetCoolingSlice() << "."
					 << endl;

				auto nextSlice = ui->treeViewFieldSelection->GetNextCoolingSlice(currentCoolingSlice);
				if (nextSlice != nullptr)
				{
					//	Extract the heightfield data from the item
					auto heightfield_data = nextSlice->data(ROLE_HEIGHT_FIELD_3);
					auto heightfield = toPtr<HeightField3D>(heightfield_data);
					assert(heightfield != nullptr);

					//	Extract the Reeb graph file (which may well be empty)
					auto reeb_file = nextSlice->data(ROLE_PATH_TO_REEB_GRAPH_FILE).toString();
					window->SetHeightfield(heightfield, reeb_file);
				}
				else
				{
					//	Unable to find the next slice - allow the user to load
					//	one
					auto result = QMessageBox::question(this,
														"The next cooling slice could not be located",
														"The next cooling slice could not be located"
														", would you like to load one?");

					if (result == QMessageBox::Yes)
					{
						//	Allow the user to load a data set
					}
				}
			});

			connect(newWindow, &MDIChildWindow::UserClickedPrevSliceButton,
					[&](MDIChildWindow* window, HeightField3D* const currentSlice)
			{
				cout << "User is requesting the prev slice in a child window."
					 << "  Current slice is " << currentSlice->GetFixedVariableName()
					 << " = " << currentSlice->GetFixedVariableValue() << "."
					 << endl;

				auto prevSlice = ui->treeViewFieldSelection->GetPrevSlice(currentSlice);
				if (prevSlice != nullptr)
				{
					//	Extract the heightfield data from the item
					auto heightfield_data = prevSlice->data(ROLE_HEIGHT_FIELD_3);
					auto heightfield = toPtr<HeightField3D>(heightfield_data);
					assert(heightfield != nullptr);

					//	Extract the Reeb graph file (which may well be empty)
					auto reeb_file = prevSlice->data(ROLE_PATH_TO_REEB_GRAPH_FILE).toString();
					window->SetHeightfield(heightfield, reeb_file);
				}
			});

			connect(newWindow, &MDIChildWindow::UserClickedNextSliceButton,
					[&](MDIChildWindow* window, HeightField3D* const currentSlice)
			{
				cout << "User is requesting the next slice in a child window."
					 << "  Current slice is " << currentSlice->GetFixedVariableName()
					 << " = " << currentSlice->GetFixedVariableValue() << "."
					 << endl;

				auto nextSlice = ui->treeViewFieldSelection->GetNextSlice(currentSlice);
				if (nextSlice != nullptr)
				{
					//	Extract the heightfield data from the item
					auto heightfield_data = nextSlice->data(ROLE_HEIGHT_FIELD_3);
					auto heightfield = toPtr<HeightField3D>(heightfield_data);
					assert(heightfield != nullptr);

					//	Extract the Reeb graph file (which may well be empty)
					auto reeb_file = nextSlice->data(ROLE_PATH_TO_REEB_GRAPH_FILE).toString();
					window->SetHeightfield(heightfield, reeb_file);
				}
			});
		};


		//	Do UI setup
		initializeIsosurfaceWidget();
		initializeIsovalueSlider();
		initializeColourRamps();
		initializeSignalsAndSlots();

		//	Add an entry in the active windows list
		ui->menu_Window->addAction(newWindow->GetActiveAction());

		//	Finally, we can show the window
		newSubWindow->show();
	}
	catch (exception &ex)
	{
		cerr << "Exception!  " << ex.what() << "in func: ";
		cerr << __func__ << " (file: " << __FILE__ << ", line: " << __LINE__;
		cerr << ")." << endl;
	}
}

MDIParentWindow::~MDIParentWindow()
{
	cout << __func__ << endl;

	delete m_isovalueBookmarWidget;

	delete ui;
}

///
/// \brief MDIParentWindow::updateIsovalueHistograms
/// \param hypervolume
/// \since		03-11-2015
/// \author		Dean
///
void MDIParentWindow::updateIsovalueHistograms(HeightField4D* hypervolume)
{
	//	Global Isovalue slider
	float currentMin = ui->widgetIsovalueSlider->GetMinimumValue();
	float currentMax = ui->widgetIsovalueSlider->GetMaximumValue();

	if (hypervolume->GetGlobalMaxima() > currentMax)
		currentMax = hypervolume->GetGlobalMaxima();
	if (hypervolume->GetGlobalMinima() < currentMin)
		currentMin = hypervolume->GetGlobalMinima();

	ui->widgetIsovalueSlider->SetIsovalueRange(currentMin, currentMax);
	ui->widgetColourRamp->SetIsovalueRange(currentMin, currentMax);
	//ui->widgetHistogramX->SetIsovalueRange(currentMin, currentMax);
	//ui->widgetHistogramY->SetIsovalueRange(currentMin, currentMax);
	//ui->widgetHistogramZ->SetIsovalueRange(currentMin, currentMax);
	//ui->widgetHistogramT->SetIsovalueRange(currentMin, currentMax);
}

///
/// \brief		Tiles windows in MDI interface
/// \since		03-07-2015
/// \author		Dean
///
void MDIParentWindow::on_actionTileWindows_triggered()
{
	ui->mdiArea->tileSubWindows();
}

///
/// \brief		Cascades window titles in MDI interface
/// \since		03-07-2015
/// \author		Dean
///
void MDIParentWindow::on_actionCascadeWindows_triggered()
{
	ui->mdiArea->cascadeSubWindows();
}

///
/// \brief		Tiles MDI sub windows vertically (taken from:
///				https://blog.qt.io/blog/2013/04/29/how-to-tile-widgets-in-a-multiple-document-interface-application/
///				[07-07-2015])
/// \since		07-07-2015
/// \author		Dean
/// \author		Dean: modify to only affect non-minimized windows [09-11-2015]
///
void MDIParentWindow::on_actionTileWindowsVertically_triggered()
{
	auto windowList = getMaximizedChildWindows();

	//	Nothing to do
	if (windowList.isEmpty()) return;

	//	Top-left corner of MDI area
	QPoint origin(0, 0);

	for (auto it = windowList.cbegin(); it != windowList.cend(); ++it)
	{
		auto window = *it;

		QRect rect(0,
				   0,
				   ui->mdiArea->width(),
				   ui->mdiArea->height() / windowList.size());
		window->setGeometry(rect);
		window->move(origin);
		origin.setY(origin.y() + window->height());
	}
}

///
/// \brief		Returns a list of child windows that are NOT in a minimzed
///				state
/// \return
/// \since		09-11-2015
/// \author		Dean
///
QList<QMdiSubWindow*> MDIParentWindow::getMaximizedChildWindows() const
{
	//	Get a list of all sub windows
	auto result = ui->mdiArea->subWindowList();

	//	Lambda predicate for removing minimized windows
	auto isMinimized = [&](const auto window) -> bool
	{
		return window->isMinimized();
	};

	//	Erase-remove minimized windows from the list using lambda predicate
	result.erase(remove_if(result.begin(), result.end(), isMinimized),
				 result.end());

	//	Return result
	return result;
}

///
/// \brief		Tiles MDI sub windows horizontally (taken from:
///				https://blog.qt.io/blog/2013/04/29/how-to-tile-widgets-in-a-multiple-document-interface-application/
///				[07-07-2015])
/// \since		07-07-2015
/// \author		Dean
/// \author		Dean: modify to only affect non-minimized windows [09-11-2015]
///
void MDIParentWindow::on_actionTileWindowsHorizontally_triggered()
{
	auto windowList = getMaximizedChildWindows();

	//	Nothing to do
	if (windowList.isEmpty()) return;

	//	Top-left corner of MDI area
	QPoint origin(0, 0);

	for (auto it = windowList.cbegin(); it != windowList.cend(); ++it)
	{
		auto window = *it;

		QRect rect(0,
				   0,
				   ui->mdiArea->width() / windowList.size(),
				   ui->mdiArea->height());
		window->setGeometry(rect);
		window->move(origin);
		origin.setX(origin.x() + window->width());
	}
}


///
/// \brief		Minimizes all windows currently displayed in the MDI area
/// \since		09-07-2015
/// \author
///
void MDIParentWindow::on_actionMinimizeAll_triggered()
{
	for (unsigned long i = 0; i < ui->mdiArea->subWindowList().size(); ++i)
	{
		ui->mdiArea->subWindowList()[i]->showMinimized();
	}
}

///
/// \brief MDIParentWindow::on_actionShowFarAxisLines_triggered
/// \param checked
/// \author		Dean
/// \since		16-07-2015
///
void MDIParentWindow::on_actionShowFarAxisLines_triggered(bool checked)
{
	for (unsigned long i = 0; i < ui->mdiArea->subWindowList().size(); ++i)
	{
		QMdiSubWindow *window = ui->mdiArea->subWindowList()[i];
		((MDIChildWindow*)window->widget())->SetRenderFarAxisLines(checked);
	}
}

///
/// \brief		Responds to the user triggering the 'show grid' action
/// \param checked
/// \author		Dean
/// \since		16-07-2015
///
void MDIParentWindow::on_actionShowGrid_triggered(bool checked)
{
	//	Can only change grid style if it is enabled
	ui->actionOuterCellsOnly->setEnabled(checked);

	for (unsigned long i = 0; i < ui->mdiArea->subWindowList().size(); ++i)
	{
		QMdiSubWindow *window = ui->mdiArea->subWindowList()[i];
		((MDIChildWindow*)window->widget())->SetRenderGrid(checked);
	}
}

///
/// \brief MDIParentWindow::on_actionOuterCellsOnly_triggered
/// \param checked
/// \author		Dean
/// \since		16-07-2015
///
void MDIParentWindow::on_actionOuterCellsOnly_triggered(bool checked)
{
	for (unsigned long i = 0; i < ui->mdiArea->subWindowList().size(); ++i)
	{
		QMdiSubWindow *window = ui->mdiArea->subWindowList()[i];
		((MDIChildWindow*)window->widget())->SetRenderOuterGridOnly(checked);
	}
}

///
/// \brief MDIParentWindow::on_actionShowAxisLines_triggered
/// \param checked
/// \author		Dean
/// \since		16-07-2015
///
void MDIParentWindow::on_actionShowAxisLines_triggered(bool checked)
{
	ui->actionShowFarAxisLines->setEnabled(checked);
	ui->actionLabelAxisLines->setEnabled(checked);

	for (unsigned long i = 0; i < ui->mdiArea->subWindowList().size(); ++i)
	{
		QMdiSubWindow *window = ui->mdiArea->subWindowList()[i];
		((MDIChildWindow*)window->widget())->SetRenderAxisLines(checked);
	}
}

///
/// \brief MDIParentWindow::on_actionLabelAxisLines_triggered
/// \param checked
/// \author		Dean
/// \since		16-07-2015
///
void MDIParentWindow::on_actionLabelAxisLines_triggered(bool checked)
{
	for (unsigned long i = 0; i < ui->mdiArea->subWindowList().size(); ++i)
	{
		QMdiSubWindow *window = ui->mdiArea->subWindowList()[i];
		((MDIChildWindow*)window->widget())->SetRenderAxisLabels(checked);
	}
}

void MDIParentWindow::on_actionSetCamera_v0_triggered()
{
	for (unsigned long i = 0; i < ui->mdiArea->subWindowList().size(); ++i)
	{
		QMdiSubWindow *window = ui->mdiArea->subWindowList()[i];
		((MDIChildWindow*)window->widget())->SetCamera();
	}
}


void MDIParentWindow::on_actionShowSurfaces_triggered(bool checked)
{
	for (unsigned long i = 0; i < ui->mdiArea->subWindowList().size(); ++i)
	{
		QMdiSubWindow *window = ui->mdiArea->subWindowList()[i];
		QtIsosurfaceWidget *surfaceWidget =
				((MDIChildWindow*)window->widget())->GetSurfaceRenderWidget();
		surfaceWidget->SetRenderSurfaces(checked);
	}
}

///
/// \brief		Shows/hides the hypervolume navigation toolbar
/// \param		checked
/// \author		Dean
/// \since		22-07-2015
///
void MDIParentWindow::on_actionShowHypervolumePartitioningWindow_triggered(bool checked)
{
	ui->dockWidgetHyperVolume->setVisible(checked);
}

///
/// \brief		Shows/hides the floating camera toolbar
/// \param		checked
/// \author		Dean
/// \since		22-07-2015
///
void MDIParentWindow::on_actionShowFloatingCameraToolbar_triggered(bool checked)
{
	ui->dockWidgetFloatingCamera->setVisible(checked);
}

///
/// \brief		Shows/hides the camera selection toolbar
/// \param		checked
/// \author		Dean
/// \since		22-07-2015
///
void MDIParentWindow::on_actionShowCameraSelectionToolbar_triggered(bool checked)
{
	//ui->dockWidgetCameraSelection->setVisible(checked);
}

///
/// \brief		Shows/hides the contour tree toolbar
/// \param		checked
/// \author		Dean
/// \since		22-07-2015
///
void MDIParentWindow::on_actionShowContourTreeToolbar_triggered(bool checked)
{
	for (unsigned long i = 0; i < ui->mdiArea->subWindowList().size(); ++i)
	{
		QMdiSubWindow *window = ui->mdiArea->subWindowList()[i];
		QDockWidget *contourTreeDockWidget =
				((MDIChildWindow*)window->widget())->GetContourTreeDockWidget();
		contourTreeDockWidget->setVisible(checked);
	}
}

///
/// \brief		Shows/hides the contour properties toolbar
/// \param		checked
/// \author		Dean
/// \since		22-07-2015
///
void MDIParentWindow::on_actionShowContourPropertiesToolbar_triggered(bool checked)
{
	for (unsigned long i = 0; i < ui->mdiArea->subWindowList().size(); ++i)
	{
		QMdiSubWindow *window = ui->mdiArea->subWindowList()[i];
		QDockWidget *contourPropertiesDockWidget =
				((MDIChildWindow*)window->widget())->GetContourPropertiesDockWidget();
		contourPropertiesDockWidget->setVisible(checked);
	}
}

///
/// \brief		Allows the edge connectivity of the meshes to be displayed
/// \param checked
/// \since		03-08-2015
/// \author		Dean
///
void MDIParentWindow::on_actionShowEdgeConnectivityWireframe_triggered(bool checked)
{
	for (unsigned long i = 0; i < ui->mdiArea->subWindowList().size(); ++i)
	{
		QMdiSubWindow *window = ui->mdiArea->subWindowList()[i];
		QtIsosurfaceWidget *surfaceRenderWidget
				= ((MDIChildWindow*)window->widget())->GetSurfaceRenderWidget();

		assert(surfaceRenderWidget != nullptr);

		surfaceRenderWidget->SetRenderEdgeConnectivityWireframe(checked);
	}
}

void MDIParentWindow::on_actionShowWireframe_triggered(bool checked)
{
	for (unsigned long i = 0; i < ui->mdiArea->subWindowList().size(); ++i)
	{
		QMdiSubWindow *window = ui->mdiArea->subWindowList()[i];
		QtIsosurfaceWidget *surfaceRenderWidget
				= ((MDIChildWindow*)window->widget())->GetSurfaceRenderWidget();

		assert(surfaceRenderWidget != nullptr);

		surfaceRenderWidget->SetRenderWireframe(checked);
	}
}




///
/// \brief MDIParentWindow::on_actionSetGlobalIsovalue_triggered
/// \author		Dean
/// \since		05-08-2015
///
void MDIParentWindow::on_actionSetGlobalIsovalue_triggered()
{
	//	Launch the isovalue selection dialog
	ui->widgetIsovalueSlider->ShowIsovalueSelectionDialog();
}


void MDIParentWindow::on_actionSurfacePlotTest_triggered()
{
	assert (m_dataModel.GetHeightField4d(0) != nullptr);

	try
	{
		//	Create a pointer to the new window
		//	Allow Qt to handle deletion of the window
		QtSurfacePlotWindow *spwXY = new QtSurfacePlotWindow();
		spwXY->setAttribute(Qt::WA_DeleteOnClose);
		QtSurfacePlotWindow *spwXZ = new QtSurfacePlotWindow();
		spwXZ->setAttribute(Qt::WA_DeleteOnClose);
		QtSurfacePlotWindow *spwXT = new QtSurfacePlotWindow();
		spwXT->setAttribute(Qt::WA_DeleteOnClose);
		QtSurfacePlotWindow *spwYZ = new QtSurfacePlotWindow();
		spwYZ->setAttribute(Qt::WA_DeleteOnClose);
		QtSurfacePlotWindow *spwYT = new QtSurfacePlotWindow();
		spwYT->setAttribute(Qt::WA_DeleteOnClose);
		QtSurfacePlotWindow *spwZT = new QtSurfacePlotWindow();
		spwZT->setAttribute(Qt::WA_DeleteOnClose);

		Vector2f dataXY = m_dataModel.GetHeightField4d(0)->GetSliceXY(3, 3);
		Vector2f dataXZ	= m_dataModel.GetHeightField4d(0)->GetSliceXZ(3, 3);
		Vector2f dataXT	= m_dataModel.GetHeightField4d(0)->GetSliceXT(3, 3);
		Vector2f dataYZ	= m_dataModel.GetHeightField4d(0)->GetSliceYZ(3, 3);
		Vector2f dataYT	= m_dataModel.GetHeightField4d(0)->GetSliceYT(3, 3);
		Vector2f dataZT	= m_dataModel.GetHeightField4d(0)->GetSliceZT(3, 3);

		spwXY->SetData(dataXY);
		spwXY->show();
		spwXZ->SetData(dataXZ);
		spwXZ->show();
		spwXT->SetData(dataXT);
		spwXT->show();
		spwYZ->SetData(dataYZ);
		spwYZ->show();
		spwYT->SetData(dataYT);
		spwYT->show();
		spwZT->SetData(dataZT);
		spwZT->show();

	}
	catch (std::bad_alloc &ex)
	{
		cerr << ex.what() << endl;
	}
}

void MDIParentWindow::on_actionRestoreContourTreeWidget_triggered()
{
	auto windowList = ui->mdiArea->subWindowList();

	for (auto it = windowList.cbegin(); it != windowList.cend(); ++it)
	{
		QMdiSubWindow *window = *it;
		assert(window != nullptr);

		auto childWindow = (MDIChildWindow*)window->widget();
		assert(childWindow != nullptr);

		childWindow->SetContourTreeVisible(true);
	}
}


///
/// \brief		Sets the colour used to colour isosurfaces with a positive
///				isovalue globally
/// \since		26-11-2015
///	\author		Dean
///
void MDIParentWindow::on_actionChoosePositiveColour_triggered()
{
	ui->widgetColourRamp->ShowPositiveColourSelectionDialog();
}

///
/// \brief		Sets the colour used to colour isosurfaces with a negative
///				isovalue globally
/// \since		26-11-2015
///	\author		Dean
///
void MDIParentWindow::on_actionChooseNegativeColour_triggered()
{
	ui->widgetColourRamp->ShowNegativeColourSelectionDialog();
}

///
/// \brief MDIParentWindow::on_actionUsePositiveNegativeColours_triggered
/// \since		26-11-2015
///	\author		Dean
///
void MDIParentWindow::on_actionUsePositiveNegativeColours_triggered()
{
	ui->widgetColourRamp->SetDisplayMode(DisplayMode::DUAL_COLOUR);
}

void MDIParentWindow::on_actionTestGradient_triggered()
{
	auto windowList = ui->mdiArea->subWindowList();

	for (auto it = windowList.cbegin(); it != windowList.cend(); ++it)
	{
		QMdiSubWindow *window = *it;
		assert(window != nullptr);

		auto childWindow = (MDIChildWindow*)window->widget();
		assert(childWindow != nullptr);

		auto isosurfaceWidget = childWindow->GetSurfaceRenderWidget();
		assert(isosurfaceWidget != nullptr);

		auto colourRampWidget = childWindow->GetColourRampWidget();
		assert(colourRampWidget != nullptr);

		auto colourTable = colourRampWidget->GetColourTable();
		isosurfaceWidget->SetColourTable(colourTable);
		isosurfaceWidget->SetColourMode(QtIsosurfaceWidget::ColourMode::GRADIENT);
	}
}

void MDIParentWindow::on_actionE_xit_triggered()
{
	close();
}

///
/// \brief		Sets the default colour for isosurfaces globally
/// \since		26-11-2015
///	\author		Dean
///
void MDIParentWindow::on_actionChooseUniversalColour_triggered()
{
	ui->widgetColourRamp->ShowUniversalColourSelectionDialog();
}

///
/// \brief
/// \since		11-12-2015
///	\author		Dean
///
void MDIParentWindow::on_actionUseUniversalColour_triggered()
{
	ui->widgetColourRamp->SetDisplayMode(DisplayMode::UNIVERSAL_COLOUR);
}

///
/// \brief
/// \since		11-12-2015
///	\author		Dean
///
void MDIParentWindow::on_actionChooseUniversalColourRamp_triggered()
{
	ui->widgetColourRamp->ShowColourRampLoadDialog();
}

///
/// \brief
/// \since		11-12-2015
///	\author		Dean
///
void MDIParentWindow::on_actionUseUniversalColourRamp_triggered()
{
	ui->widgetColourRamp->SetDisplayMode(DisplayMode::COLOUR_RAMP);
}

///
/// \brief
/// \since		11-12-2015
///	\author		Dean
///
void MDIParentWindow::on_actionReverseUniversalColourRamp_triggered()
{
	ui->widgetColourRamp->ReverseColourRamp();
}

void MDIParentWindow::on_actionTest_New_Contour_Tree_Widget_triggered()
{
	ui->widget->DisplayContourTree(m_dataModel.GetHeightField3d(0)->GetContourTree());
}

void MDIParentWindow::on_actionZoomIn_triggered()
{
	ui->widget->zoomIn();
}

void MDIParentWindow::on_actionZoomOut_triggered()
{
	ui->widget->zoomOut();
}

///
/// \brief	Executes the "Rb.exe" command to generate a reebgraph for a given
///			heightfield
/// \param heightField3file
/// \param fieldName
/// \param cacheDir
/// \return bool: true command executed without errors
/// \author		Dean
/// \since		05-02-2015
///
bool MDIParentWindow::executeReebCommand(const QString& heightField3file,
										 const QString& out_dir)
{
	//const QString REEB_CMD = "C:\\Users\\4thFloor\\Documents\\Qt\\QtFlexibleIsosurfaces\\vtkReebGraphTest\\x64\\Debug\\vtkReebGraphTest.exe";
	const QString REEB_CMD = "/home/dean/qcdvis/CommandLineTools/CommandLineTools/build-command_line_tools-Desktop_Qt_5_7_0_GCC_64bit-Debug/reeb/reeb";

	QStringList args;
	args << heightField3file << out_dir;

	qDebug() << REEB_CMD << args;

	auto result = QProcess::execute(REEB_CMD, args);

	if (result == 0) return true;

	QMessageBox::critical(this,
						  "Error",
						  QString("There was an error executing the ReebGraph "
								  "command.  The application exited with error "
								  " code: %1.").arg(result));
	return false;
}

void MDIParentWindow::on_actionLoadTestReebGraph_triggered()
{

	auto heightfield4 = m_dataModel.GetHeightField4d(0);
	auto heightfield3 = heightfield4->GetTSlices()[0];

	auto reebGraph = heightfield3->GetReebGraph();

	//	If there is no reeb graph available then it will need to be computed
	//	if (!reebGraph)
	//	{
	//		reebGraph = new ReebGraph();
	//		auto cacheDir = m_dataModel.GetCacheDir(heightfield4);
	//		auto field = heightfield3->GetFieldName();

	//		std::string reebFile(cacheDir + "\\" + field + ".rb");
	//		if (!reebGraph->LoadFromFile(reebFile))
	//		{
	//			auto heightfieldFile = cacheDir
	//					+ "\\" + heightfield3->GetCacheFilename();

	//			if (executeReebCommand(QString::fromStdString(heightfieldFile),
	//								   QString::fromStdString(field),
	//								   QString::fromStdString(cacheDir)))
	//			{
	//				reebGraph->LoadFromFile(reebFile);
	//			}
	//			else return;
	//		}
	//	}

	cout << *reebGraph << endl;

	assert(reebGraph != nullptr);
	//auto surplusNodes = reebGraph->ReduceGraph();
	//qDebug() << "Removed " << surplusNodes << " from reeb graph.";
	ui->widget->DisplayReebGraph(reebGraph);
}

/// \author		Dean
/// \since		03-02-2016
void MDIParentWindow::on_actionAutoScale_triggered(bool checked)
{
	ui->widget->SetAutoScale(checked);
}

void MDIParentWindow::on_actionAlphaBlending_triggered(bool checked)
{
	auto windowList = ui->mdiArea->subWindowList();
	for (auto& win : windowList)
	{
		auto mdiWindow = (MDIChildWindow*)win->widget();
		assert(mdiWindow != nullptr);
		mdiWindow->SetAlphaBlended(checked);
	}
}

void MDIParentWindow::on_actionShowNestedContours_triggered(bool checked)
{
	//	Disable the isovalue slider if we are displaying nested isovalues
	ui->widgetIsovalueSlider->setEnabled(!checked);

	auto windowList = ui->mdiArea->subWindowList();
	for (auto& win : windowList)
	{
		auto mdiWindow = dynamic_cast<MDIChildWindow*>(win->widget());
		if (mdiWindow != nullptr)
		{
			mdiWindow->SetNestedContours(checked);

			//	Disable the isovalue slider if we are displaying nested isovalues
			mdiWindow->SetIsovalueSliderEnabled(!checked);
		}
	}
}

void MDIParentWindow::on_actionSetNumberOfNestedContours_triggered()
{
	constexpr size_t MAX_CONTOURS = 500;
	constexpr size_t STEP_SIZE = 5;
	bool result;

	auto new_value = QInputDialog::getInt(this,
										  "Set number of nested contours",
										  "Number:",
										  m_nested_contour_count,
										  1,
										  MAX_CONTOURS,
										  STEP_SIZE,
										  &result);

	if (result)
	{
		auto windowList = ui->mdiArea->subWindowList();
		for (auto& win : windowList)
		{
			auto mdiWindow = dynamic_cast<MDIChildWindow*>(win->widget());
			if (mdiWindow != nullptr)
			{
				mdiWindow->SetNestedContourCount(new_value);
			}
		}

		//	Set display mode to nested contours
		on_actionShowNestedContours_triggered(true);

		//	Store for next time
		m_nested_contour_count = new_value;
	}
}

void MDIParentWindow::on_action1080Screenshot_triggered()
{
	takeScreenshot(QSize(1080, 720));
}

void MDIParentWindow::takeScreenshot(const QSize& dimensions)
{
	//const QString DEFAULT_DIR = R"(C:\Users\4thFloor\Desktop)";
	const QString IMAGE_FORMATS = "PNG file (*.png);;"
								  "Bitmap image (*.bmp);;"
								  "Jpeg Image (*.jpg)";

	//	Grab the active sub-window (if any)
	auto subWindow = ui->mdiArea->activeSubWindow();
	if (subWindow == nullptr) return;

	auto filename = QFileDialog::getSaveFileName(this,
												 "Set screenshot filename",
												 m_save_to_path.absolutePath(),
												 IMAGE_FORMATS);

	qDebug() << "Saving screenshot to:" << filename;
	if (filename != "")
	{
		//	Check that the dialog has returned an extension (if not append a PNG extension)
		auto file_info = QFileInfo(filename);
		qDebug() << "Filetype:" << file_info.suffix();
		if (file_info.suffix() == "") filename += ".png";

		//	Get hold of the flexible isosurface widget  in the last accessed window
		auto mdiWindow = (MDIChildWindow*)subWindow->widget();
		assert(mdiWindow != nullptr);

		//	Take the screenshot
		auto screenshot = mdiWindow->ObtainScreenshot(dimensions, Qt::white);
		screenshot.save(filename);

		//	Store the selected directory for the next screenshot
		m_save_to_path = file_info.absoluteDir();
	}
}

void MDIParentWindow::on_action4kScreenshot_triggered()
{
	takeScreenshot(QSize(3840, 2160));
}

void MDIParentWindow::on_actionShowCriticalNodeGlyphs_triggered(bool checked)
{
	auto windowList = ui->mdiArea->subWindowList();
	for (auto& win : windowList)
	{
		auto mdiWindow = (MDIChildWindow*)win->widget();
		assert(mdiWindow != nullptr);
		mdiWindow->SetShowCriticalNodeGlyphs(checked);
	}
}

void MDIParentWindow::on_actionJoint_Contour_Net_triggered()
{
	auto jcnWindow = new QtJointContourNetForm(this);
	jcnWindow->SetFloatingCameraPosition(ui->widgetArcBallCameraPosition->GetQuaternion(),
										 ui->widgetArcBallCameraPosition->GetRadius());
	jcnWindow->setSourceDirectoryF0("/home/dean/Desktop/tidy_me/slabs/mu0500/polyakov_cools_19");
	jcnWindow->setSourceDirectoryF1("/home/dean/Desktop/tidy_me/slabs/mu0500/polyakov_cools_20");
	jcnWindow->setTitleF0("Polyakov mu=0.5 @ 19 cools");
	jcnWindow->setSourceDirectoryF0F1("/home/dean/Desktop/tidy_me/slabs/mu0500/polyakov_cools_19_20");
	jcnWindow->setTitleF1("Polyakov mu=0.5 @ 20 cools");

	connect(ui->widgetArcBallCameraPosition, &ArcBallWidget::RotationChanged,
			[=]()
	{
		jcnWindow->SetFloatingCameraPosition(
					ui->widgetArcBallCameraPosition->GetQuaternion(),
					ui->widgetArcBallCameraPosition->GetRadius());
	});
	connect(ui->widgetArcBallCameraPosition, &ArcBallWidget::RadiusChanged,
			[=]()
	{
		jcnWindow->SetFloatingCameraPosition(
					ui->widgetArcBallCameraPosition->GetQuaternion(),
					ui->widgetArcBallCameraPosition->GetRadius());
	});

	QMdiSubWindow *newSubWindow = ui->mdiArea->addSubWindow(jcnWindow);
	newSubWindow->setWindowTitle(QString("JCN - [%1]").arg(jcnWindow->getSourceDirectory()));
	newSubWindow->show();
}

void MDIParentWindow::on_actionShowCTSkeleton_triggered(bool checked)
{
	cout << "User has clicked to " << (checked ? "show" : "hide")
		 << " the contour tree skeleton." << endl;

	auto windowList = ui->mdiArea->subWindowList();
	for (auto& win : windowList)
	{
		auto mdiWindow = dynamic_cast<MDIChildWindow*>(win->widget());
		if (mdiWindow != nullptr)
		{
			mdiWindow->SetRenderContourTreeSkeleton(checked);
		}
	}
}

void MDIParentWindow::on_actionShowReebGraphSkeleton_triggered(bool checked)
{
	cout << "User has clicked to " << (checked ? "show" : "hide")
		 << " the reeb graph skeleton." << endl;

	auto windowList = ui->mdiArea->subWindowList();
	for (auto& win : windowList)
	{
		auto mdiWindow = dynamic_cast<MDIChildWindow*>(win->widget());
		if (mdiWindow != nullptr)
		{
			//	Univariate windows only, not multivariate
			mdiWindow->SetRenderReebGraphSkeleton(checked);
		}
	}
}


///
/// \brief MDIParentWindow::on_action_Open_triggered
/// \since		?
/// \author		Dean
/// \author		Dean: allow multiple files to opened and loaded [06-11-2015]
///
void MDIParentWindow::on_action_Open_triggered()
{
	//m_openDataDialog->sele
	//const QString INITIAL_DIRECTORY = "E:\\Dean\\QCD Data\\Field data\\Pygrid";
	//const QString INITIAL_DIRECTORY = "";

	//	Our initial file extension will be ".hvol1" and ".vol1"
	QString INITIAL_EXTENSION = FILTER_STRING_HVOL1_VOL1;

	//	Combine all extensions into a format that Qt can handle
	const QString FILE_EXTENSION_LIST =
			QString::fromStdString(FILTER_STRING_TXT) + ";;"
			+ QString::fromStdString(FILTER_STRING_VOL) + ";;"
			+ QString::fromStdString(FILTER_STRING_VOL1) + ";;"
			+ QString::fromStdString(FILTER_STRING_HVOL1) + ";;"
			+ QString::fromStdString(FILTER_STRING_HVOL1_VOL1) + ";;"
			+ QString::fromStdString(FILTER_ALL);

	//	Retreive a list of files selected by the user
	auto filenames = QFileDialog::getOpenFileNames(this,
												   "Open scalar field data",
												   m_open_from_dir_path.absolutePath(),
												   FILE_EXTENSION_LIST,
												   &INITIAL_EXTENSION);

	//	Loop through all of the selected files
	for (auto it = filenames.cbegin(); it != filenames.cend(); ++it)
	{
		//	Get each filename
		auto filename = *it;

		//	Open each file
		if (filename != "")
		{
			LoadData(filename);
		}
	}
}

///
/// \brief	Allow the user to select multiple directories for loading
///			multiple cooling configurations
/// \author Dean
/// \since	09-01-2017
///
void MDIParentWindow::on_actionOpenFromDirectory_triggered()
{
	QFileDialog dialog(this);
	dialog.setOptions(QFileDialog::ShowDirsOnly);// | QFileDialog::DontUseNativeDialog);
	dialog.setFileMode(QFileDialog::DirectoryOnly);
	dialog.setDirectory(m_open_from_dir_path);
	dialog.setModal(true);

	//	We'll need to do a bit of hackery to allow us to select multiple directories
	auto file_view = dialog.findChild<QListView*>("listView");
	if (file_view != nullptr)
		file_view->setSelectionMode(QAbstractItemView::MultiSelection);
	auto f_tree_view = dialog.findChild<QTreeView*>();
	if (f_tree_view != nullptr)
		f_tree_view->setSelectionMode(QAbstractItemView::MultiSelection);

	if (dialog.exec() == QFileDialog::Accepted)
	{
		//	If the user pressed OK, we will try to open any files in the selected directories
		auto dirs = dialog.selectedFiles();
		for (auto& dir : dirs)
		{
			//	For each directory, we set up a directory iterator to iterate over the files
			QDirIterator dir_iterator(dir);
			while (dir_iterator.hasNext())
			{
				//	We will open any scalar volume or scalar hypervolume files
				auto next_file = QFileInfo(dir_iterator.next());
				if ((next_file.completeSuffix() == "vol1") || (next_file.completeSuffix() == "hvol1"))
				{
					cout << next_file.absoluteFilePath() << endl;

					//	Pass the filename to the loader to handle
					LoadData(next_file.absoluteFilePath());
				}
			}
		}

		//	Store the selected dir for next time
		m_open_from_dir_path = dialog.directory();
	}
}

void MDIParentWindow::on_actionCloseAll_triggered()
{
	//	We won't actually remove the files from the datamodel - just
	//	remove them from the slice dir.  The application will think
	//	everything is closed
	ui->treeViewFieldSelection->ClearAll();
}

void MDIParentWindow::on_actionSaveSessionData_triggered()
{
	//	Get a file to open
	auto filename = QFileDialog::getSaveFileName(this,
												 "Save session data",
												 m_session_dir.absolutePath(),
												 QString("Settings (*.settings);;All files (*.*)"));
	if (filename != "")
	{
		//	Because it irritatingly often leaves off the file extension
		auto file_info = QFileInfo(filename);
		if (file_info.suffix() == "") filename += ".settings";

		//	Save the settings file
		saveSettings(filename);
	}
}

void MDIParentWindow::on_actionOpenSessionData_triggered()
{
	//	Get a file to open
	auto filename = QFileDialog::getOpenFileName(this,
												 "Load session data",
												 m_session_dir.absolutePath(),
												 QString("Settings (*.settings);;All files (*.*)"));
	if (filename != "")
	{
		//	Load the settings file
		loadSettings(filename);
	}
}

void MDIParentWindow::on_actionSaveReebGraph_triggered()
{
	//	Grab the active sub-window (if any)
	auto subWindow = ui->mdiArea->activeSubWindow();
	if (subWindow == nullptr) return;

	auto filename = QFileDialog::getSaveFileName(this,
												 "Save Reeb graph",
												 m_save_to_path.absolutePath(),
												 QString("Pdf file (*pdf)"));

	if (filename != "")
	{
		//	Because it irritatingly often leaves off the file extension
		auto file_info = QFileInfo(filename);
		if (file_info.suffix() == "") filename += ".pdf";

		//	Get hold of the flexible isosurface widget  in the last accessed window
		auto mdiWindow = (MDIChildWindow*)subWindow->widget();
		assert(mdiWindow != nullptr);

		mdiWindow->SaveReebGraph(filename);

		//	Update the output dir
		m_save_to_path = QFileInfo(filename).absolutePath();
	}
}

void MDIParentWindow::on_actionSaveContourTree_triggered()
{
	//	Grab the active sub-window (if any)
	auto subWindow = ui->mdiArea->activeSubWindow();
	if (subWindow == nullptr) return;

	auto filename = QFileDialog::getSaveFileName(this,
												 "Save contour tree",
												 m_save_to_path.absolutePath(),
												 QString("Pdf file (*pdf)"));

	if (filename != "")
	{
		//	Because it irritatingly often leaves off the file extension
		auto file_info = QFileInfo(filename);
		if (file_info.suffix() == "") filename += ".pdf";

		//	Get hold of the flexible isosurface widget  in the last accessed window
		auto mdiWindow = (MDIChildWindow*)subWindow->widget();
		assert(mdiWindow != nullptr);

		mdiWindow->SaveContourTree(filename);

		//	Update the output dir
		m_save_to_path = QFileInfo(filename).absolutePath();
	}
}

void MDIParentWindow::on_actionResetNestedIsovalueRange_triggered()
{
	auto windowList = ui->mdiArea->subWindowList();
	for (auto& win : windowList)
	{
		auto mdiWindow = dynamic_cast<MDIChildWindow*>(win->widget());
		if (mdiWindow != nullptr)
		{
			//	Univariate windows only, not multivariate
			mdiWindow->GetSurfaceRenderWidget()->ResetNestedContourRange();
		}
	}
}

void MDIParentWindow::on_actionShowOpenEdgesOnly_triggered(bool checked)
{
	auto windowList = ui->mdiArea->subWindowList();
	for (auto& win : windowList)
	{
		auto mdiWindow = dynamic_cast<MDIChildWindow*>(win->widget());
		if (mdiWindow != nullptr)
		{
			//	Univariate windows only, not multivariate
			mdiWindow->GetSurfaceRenderWidget()->SetRenderEdgeConnectivityForHolesOnly(checked);
		}
	}
}

void MDIParentWindow::on_actionIncreaseLineThickness_triggered()
{
	const auto MAX_THICKNESS = 3.0f;

	auto windowList = ui->mdiArea->subWindowList();
	for (auto& win : windowList)
	{
		auto mdiWindow = dynamic_cast<MDIChildWindow*>(win->widget());
		if (mdiWindow != nullptr)
		{
			//	Univariate windows only, not multivariate
			auto currentThickness = mdiWindow->GetSurfaceRenderWidget()->GetGridLineThickness();
			mdiWindow->GetSurfaceRenderWidget()->SetGridLineThickness(
						currentThickness < MAX_THICKNESS
						? currentThickness += 0.1f
					: MAX_THICKNESS);
		}
	}
}

void MDIParentWindow::on_actionDecreaseLineThickness_triggered()
{
	const auto MIN_THICKNESS = 0.1f;

	auto windowList = ui->mdiArea->subWindowList();
	for (auto& win : windowList)
	{
		auto mdiWindow = dynamic_cast<MDIChildWindow*>(win->widget());
		if (mdiWindow != nullptr)
		{
			//	Univariate windows only, not multivariate
			auto currentThickness = mdiWindow->GetSurfaceRenderWidget()->GetGridLineThickness();
			mdiWindow->GetSurfaceRenderWidget()->SetGridLineThickness(
						currentThickness > MIN_THICKNESS
						? currentThickness -= 0.1f
					: MIN_THICKNESS);
		}
	}
}

void MDIParentWindow::on_actionSetLineColour_triggered()
{
	auto windowList = ui->mdiArea->subWindowList();
	for (auto& win : windowList)
	{
		auto mdiWindow = dynamic_cast<MDIChildWindow*>(win->widget());
		if (mdiWindow != nullptr)
		{
			//	Univariate windows only, not multivariate
			auto currentColour = mdiWindow->GetSurfaceRenderWidget()->GetGridLineColour();
			auto newColour = QColorDialog::getColor(currentColour,
													this,
													"Choose grid line colour");

			mdiWindow->GetSurfaceRenderWidget()->SetGridLineColour(newColour);
		}
	}

}

void MDIParentWindow::on_actionSaveJoinTree_triggered()
{
	auto filename = QFileDialog::getSaveFileName(this,
	"Save Join Tree", QDir::home().absolutePath(), "*.dot");

	if (filename != "")
	{
		auto result = m_dataModel.GetHeightField3d(0)->GetContourTree()
		->GetJoinTree()->SaveToDotFile(filename.toStdString());
		cout << "Saved join tree to " << filename.toStdString()
		<< ": " << (result ? "Ok" : "failed") << endl;

	}
}

void MDIParentWindow::on_actionSaveSplitTree_triggered()
{
	auto filename = QFileDialog::getSaveFileName(this,
	"Save Split Tree", QDir::home().absolutePath(), "*.dot");

	if (filename != "")
	{
		auto result = m_dataModel.GetHeightField3d(0)->GetContourTree()
		->GetSplitTree()->SaveToDotFile(filename.toStdString());
		cout << "Saved split tree to " << filename.toStdString() << ": "
		<< (result ? "Ok" : "failed") << endl;

	}
}

void MDIParentWindow::on_actionSaveContourTree_2_triggered()
{
	auto filename = QFileDialog::getSaveFileName(this,
	"Save Contour Tree", QDir::home().absolutePath(), "*.dot");

	if (filename != "")
	{
		auto fullTree = m_dataModel.GetHeightField3d(0)->GetContourTree()->ToDOT(true);
		auto criticalTree = m_dataModel.GetHeightField3d(0)->GetContourTree()->ToDOT(false);

		std::ofstream outstream2(filename.toStdString());
		outstream2 << criticalTree;

		std::ofstream outstream1(filename.replace(".dot", "_full.dot", Qt::CaseInsensitive).toStdString());
		outstream1 << fullTree;
	}
}
