#ifndef LOADPROGRESSDIALOG_H
#define LOADPROGRESSDIALOG_H

#include <cassert>
#include <iostream>

#include <QDialog>

namespace Ui {
	class LoadProgressDialog;
}

class QtProgressDialog : public QDialog
{
		Q_OBJECT

	private:
		QString m_currentAction;

	public:
		explicit QtProgressDialog(QWidget *parent = 0);
		~QtProgressDialog();

		void SetCurrentActionText(const QString& action);
		void IncrementProgressBar(const size_t& step);
	private:
		Ui::LoadProgressDialog *ui;
};

#endif // LOADPROGRESSDIALOG_H
