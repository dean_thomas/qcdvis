#ifndef QT_SURFACE_PLOT_WINDOW_H
#define QT_SURFACE_PLOT_WINDOW_H

#include <QWidget>
#include <QDialog>
#include <Vector2.h>

namespace Ui {
	class SurfacePlotWindow;
}

class QtSurfacePlotWindow : public QDialog
{
		Q_OBJECT

	public:
		using Vector2f = Storage::Vector2<float>;

		explicit QtSurfacePlotWindow(QWidget *parent = 0);

		void SetData(const Vector2f& data);

		~QtSurfacePlotWindow();

	private:
		Ui::SurfacePlotWindow *ui;
};

#endif // QT_SURFACE_PLOT_WINDOW_H
