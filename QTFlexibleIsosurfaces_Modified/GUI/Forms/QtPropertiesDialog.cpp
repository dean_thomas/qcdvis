#include "QtPropertiesDialog.h"
#include "ui_QtPropertiesDialog.h"
#include "Graphs/ContourTreeSuperarc.h"
#include "Graphs/ContourTreeSupernode.h"
#include <utility>

using namespace std;

#define HIDE_COMPUTED_PERSISTENCE

QtPropertiesDialog::QtPropertiesDialog(QWidget *parent) :
	QDialog(parent),
	ui(new Ui::QtPropertiesDialog)
{
	ui->setupUi(this);

	buildInitialModel();
}

void QtPropertiesDialog::buildInitialModel()
{
	m_root_node = new QTreeWidgetItem(ui->treeWidgetProperties, {"Superarc id: -1"});
	m_root_node->setExpanded(true);
	ui->treeWidgetProperties->addTopLevelItem(m_root_node);

	auto supernode_root = new QTreeWidgetItem(m_root_node, {"Supernodes"});
	supernode_root->setExpanded(true);
	m_top_node_id = new QTreeWidgetItem(supernode_root, {"Top node id: -1"});
	m_top_node_pos = new QTreeWidgetItem(m_top_node_id, {"Position: (0, 0, 0)"});
	m_top_node_value = new QTreeWidgetItem(m_top_node_id, {"Isovalue: 0.0"});

	m_bottom_node_id = new QTreeWidgetItem(supernode_root, {"Bottom node id: -1"});
	m_bottom_node_pos = new QTreeWidgetItem(m_bottom_node_id, {"Position: (0, 0, 0)"});
	m_bottom_node_value = new QTreeWidgetItem(m_bottom_node_id, {"Isovalue: 0.0"});

	auto persistence_root = new QTreeWidgetItem(m_root_node, {"Persistence"});
	persistence_root->setExpanded(true);
	m_arc_length = new QTreeWidgetItem(persistence_root, {"Superarc length: -0.0"});

	//	Approximated volume (node count)
	auto node_count_root = new QTreeWidgetItem(persistence_root, {"Approximate volume (node count)"});
	node_count_root->setExpanded(true);
	m_approx_up_volume_node_count = new QTreeWidgetItem(node_count_root, {"Upward volume: 0.0"});
	m_approx_down_volume_node_count = new QTreeWidgetItem(node_count_root, {"Downward Volume: 0.0"});
	m_approx_volume_node_count = new QTreeWidgetItem(node_count_root, {"Volume on arc: 0.0"});

	//	Approximated hypervolume (sample count)
	auto sample_count_root = new QTreeWidgetItem(persistence_root, {"Approximate hypervolume (sample sum)"});
	sample_count_root->setExpanded(true);
	m_approx_up_volume_sample_count = new QTreeWidgetItem(sample_count_root, {"Upward hypervolume: 0.0"});
	m_approx_down_volume_sample_count = new QTreeWidgetItem(sample_count_root, {"Downward hypervolume: 0.0"});
	m_approx_volume_sample_count = new QTreeWidgetItem(sample_count_root, {"Hypervolume on arc: 0.0"});

#ifndef HIDE_COMPUTED_PERSISTENCE
	//	Computed persistence
	auto computed_persistence_root = new QTreeWidgetItem(persistence_root, {"Computed persistence"});
	computed_persistence_root->setExpanded(true);
	m_surface_area = new QTreeWidgetItem(computed_persistence_root, {"Integrated surface area: -0.0 (0 samples)"});
	m_enclosed_volume = new QTreeWidgetItem(computed_persistence_root, {"Integrated volume: -0.0 (0 samples)"});
#endif
	m_root_node->addChild(supernode_root);
	supernode_root->addChildren({m_top_node_id, m_bottom_node_id});

	m_root_node->addChild(persistence_root);
	persistence_root->addChild({m_arc_length});
	persistence_root->addChildren({ node_count_root, sample_count_root });
#ifndef HIDE_COMPUTED_PERSISTENCE
	persistence_root->addChild(computed_persistence_root);
#endif
}

void QtPropertiesDialog::SetSuperarc(ContourTreeSuperarc *superarc)
{
	m_root_node->setText(0, QString("Superarc id: %1").arg(superarc->GetId()));

	m_top_node_id->setText(0, QString("Top supernode id: %1").arg(superarc->GetTopSupernode()->GetId()));
	m_top_node_pos->setText(0, QString("Position: (%1, %2, %3)")
							.arg(get<0>(superarc->GetTopSupernode()->GetPosition()))
							.arg(get<1>(superarc->GetTopSupernode()->GetPosition()))
							.arg(get<2>(superarc->GetTopSupernode()->GetPosition())));
	m_top_node_value->setText(0, QString("Isovalue: %1").arg(superarc->GetTopSupernode()->GetHeight()));

	m_bottom_node_id->setText(0, QString("Bottom supernode id: %1").arg(superarc->GetBottomSupernode()->GetId()));
	m_bottom_node_pos->setText(0, QString("Position: (%1, %2, %3)")
							.arg(get<0>(superarc->GetBottomSupernode()->GetPosition()))
							.arg(get<1>(superarc->GetBottomSupernode()->GetPosition()))
							.arg(get<2>(superarc->GetBottomSupernode()->GetPosition())));
	m_bottom_node_value->setText(0, QString("Isovalue: %1").arg(superarc->GetBottomSupernode()->GetHeight()));

	m_arc_length->setText(0, QString("Superarc length: %1").arg(superarc->GetArcLength()));
#ifndef HIDE_COMPUTED_PERSISTENCE
	m_surface_area->setText(0, QString("Integrated surface area: %1 (%2 samples)").arg(superarc->GetIntegratedSurfaceArea(m_samples)).arg(m_samples));
	m_enclosed_volume->setText(0, QString("Integrated volume: %1 (%2 samples)").arg(superarc->GetIntegratedVolume(m_samples)).arg(m_samples));
#endif
	m_approx_up_volume_node_count->setText(0, QString("Upward volume: %1")
										   .arg(superarc->GetApproxVolumeUp(ContourTreeSuperarc::Persistence_Measure::NODE_COUNT)));
	m_approx_down_volume_node_count->setText(0, QString("Downward Volume: %1")
											 .arg(superarc->GetApproxVolumeDown(ContourTreeSuperarc::Persistence_Measure::NODE_COUNT)));
	m_approx_volume_node_count->setText(0, QString("Volume on arc: %1")
										.arg(superarc->GetApproxVolume(ContourTreeSuperarc::Persistence_Measure::NODE_COUNT)));

	m_approx_up_volume_sample_count->setText(0, QString("Upward hypervolume: %1")
											 .arg(superarc->GetApproxVolumeUp(ContourTreeSuperarc::Persistence_Measure::SAMPLE_COUNT)));
	m_approx_down_volume_sample_count->setText(0, QString("Downward hypervolume: %1")
											   .arg(superarc->GetApproxVolumeDown(ContourTreeSuperarc::Persistence_Measure::SAMPLE_COUNT)));
	m_approx_volume_sample_count->setText(0, QString("Hypervolume on arc: %1")
										  .arg(superarc->GetApproxVolume(ContourTreeSuperarc::Persistence_Measure::SAMPLE_COUNT)));
}

QtPropertiesDialog::~QtPropertiesDialog()
{
	delete ui;
}
