#include "QtOriginOffsetDialog.h"
#include "ui_QtOriginOffsetDialog.h"

///
/// \brief QtOriginOffsetDialog::QtOriginOffsetDialog
/// \param parent
/// \author		Dean
/// \since		28-08-2015
///
QtOriginOffsetDialog::QtOriginOffsetDialog(QWidget *parent) :
	QDialog(parent),
	ui(new Ui::QtOriginOffsetDialog)
{
	ui->setupUi(this);
}

///
/// \brief QtOriginOffsetDialog::~QtOriginOffsetDialog
/// \author		Dean
/// \since		28-08-2015
///
QtOriginOffsetDialog::~QtOriginOffsetDialog()
{
	delete ui;
}

///
/// \brief		Sets the maximum value of each slider (the slider will be
///				disabled if set to zero)
/// \param x
/// \param y
/// \param z
/// \param t
/// \since		28-08-2015
/// \author		Dean
///	\author		Dean: add ability to disable by setting value to zero
///				[22-09-2015]
///
void QtOriginOffsetDialog::SetMaximumXYZT(const size_t &x,
										  const size_t &y,
										  const size_t &z,
										  const size_t &t)
{
	ui->widget->SetAxisRanges(x, y, z, t);
	ui->widget->SetEnabled(x != 0, y != 0, z != 0, t != 0);
}

///
/// \brief QtOriginOffsetDialog::SetValueXYZT
/// \param x
/// \param y
/// \param z
/// \param t
/// \author		Dean
/// \since		28-08-2015
///
void QtOriginOffsetDialog::SetValueXYZT(const size_t &x,
										const size_t &y,
										const size_t &z,
										const size_t &t)
{
	ui->widget->SetValues(x, y, z, t);
}

size_t QtOriginOffsetDialog::GetValueX() const
{
	return ui->widget->GetAxisOffsetX();
}

size_t QtOriginOffsetDialog::GetValueY() const
{
	return ui->widget->GetAxisOffsetY();
}

size_t QtOriginOffsetDialog::GetValueZ() const
{
	return ui->widget->GetAxisOffsetZ();
}

size_t QtOriginOffsetDialog::GetValueT() const
{
	return ui->widget->GetAxisOffsetT();
}
