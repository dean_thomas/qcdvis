#include "QtJointContourNetForm.h"
#include "ui_QtJointContourNetForm.h"
#include  <utility>

using namespace std;

void QtJointContourNetForm::setTitleF0(const QString& title)
{
	ui->labelField0->setText(title);
}

void QtJointContourNetForm::setTitleF1(const QString& title)
{
	ui->labelField1->setText(title);
}

void QtJointContourNetForm::setSourceDirectoryF0F1(const QString& dir)
{
	ui->widgetJcnF0F1->loadMeshesInDirectory(dir);
}

void QtJointContourNetForm::setSourceDirectoryF0(const QString& dir)
{
	ui->widgetJcnF0->loadMeshesInDirectory(dir);
}

void QtJointContourNetForm::setSourceDirectoryF1(const QString& dir)
{
	ui->widgetJcnF1->loadMeshesInDirectory(dir);
}

void QtJointContourNetForm::updateTransferFunctionInformation()
{
	//	Update the labels
	auto range = ui->widgetJcnF0F1->getRange();
	ui->labelMin->setText(QString::number(get<0>(range), 'f', 2));
	ui->labelMax->setText(QString::number(get<1>(range), 'f', 2));

	//	Update the colours
	ui->widgetColourRamp->SetColourTable(ui->widgetJcnF0F1->getColourTable());
}

///
/// \brief		Return the directory that the meshes are loaded from
/// \return
/// \since		21-11-2016
/// \author		Dean
///
QString QtJointContourNetForm::getSourceDirectory() const
{
	return ui->widgetJcnF0F1->GetSourceDir();
}

QtJointContourNetForm::QtJointContourNetForm(QWidget *parent) :
	QWidget(parent),
	ui(new Ui::QtJointContourNetForm)
{
	ui->setupUi(this);
	ui->widgetColourRamp->SetDisplayMode(QtColourRampWidget::DisplayMode::COLOUR_RAMP);

	connect(ui->widgetColourRamp, &QtColourRampWidget::OnColourTableChanged,
			[&](const QtColourRampWidget::ColourTable& value)
	{
		ui->widgetJcnF0F1->setColourTable(value);
		ui->widgetJcnF0->setColourTable(value);
		ui->widgetJcnF1->setColourTable(value);
	});
}

QtJointContourNetForm::~QtJointContourNetForm()
{
	delete ui;
}

///
/// \brief QtJointContourNetForm::SetFloatingCameraPosition
/// \param camera
/// \param radius
/// \since		21-11-2016
/// \author		Dean
///
void QtJointContourNetForm::SetFloatingCameraPosition(const QQuaternion &camera, const float &radius)
{
	ui->widgetJcnF0F1->SetFloatingCameraPosition(camera, radius);
	ui->widgetJcnF0->SetFloatingCameraPosition(camera, radius);
	ui->widgetJcnF1->SetFloatingCameraPosition(camera, radius);
}

///
/// \brief QtJointContourNetForm::GetFloatingCameraPosition
/// \return
/// \since		21-11-2016
/// \author		Dean
///
QVector3D QtJointContourNetForm::GetFloatingCameraPosition() const
{
	return ui->widgetJcnF0F1->GetFloatingCameraPosition();
}

///
/// \brief		Assigns transfer function using users selection
///
/// \since		21-11-2016
/// \author		Dean
///
void QtJointContourNetForm::on_applyPushButton_clicked()
{
	switch (ui->transferFunctionComboBox->currentIndex())
	{
	case 1:
		ui->widgetJcnF0->assignColourByTriangleCount(ui->checkBoxTransparency->isChecked());
		ui->widgetJcnF1->assignColourByTriangleCount(ui->checkBoxTransparency->isChecked());
		ui->widgetJcnF0F1->assignColourByTriangleCount(ui->checkBoxTransparency->isChecked());
		break;
	case 2:
		ui->widgetJcnF0->assignColourBySurfaceArea(ui->checkBoxTransparency->isChecked());
		ui->widgetJcnF1->assignColourBySurfaceArea(ui->checkBoxTransparency->isChecked());
		ui->widgetJcnF0F1->assignColourBySurfaceArea(ui->checkBoxTransparency->isChecked());
		break;
	default:
		break;

	}

	updateTransferFunctionInformation();
}
