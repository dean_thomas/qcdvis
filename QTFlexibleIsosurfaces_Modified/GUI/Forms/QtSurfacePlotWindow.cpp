#include "QtSurfacePlotWindow.h"
#include "ui_SurfacePlotWindow.h"

using std::cout;
using std::endl;

///
/// \brief QtSurfacePlotWindow::QtSurfacePlotWindow
/// \param parent
/// \author		Dean
/// \since		25-08-2015
///
QtSurfacePlotWindow::QtSurfacePlotWindow(QWidget *parent) :
	QDialog(parent),
	ui(new Ui::SurfacePlotWindow)
{
	ui->setupUi(this);

	ui->widgetSurfacePlot->SetColourRampWidget(ui->widget);
}

///
/// \brief QtSurfacePlotWindow::~QtSurfacePlotWindow
/// \author		Dean
/// \since		25-08-2015
///
QtSurfacePlotWindow::~QtSurfacePlotWindow()
{
	cout << __func__ << endl;

	delete ui;
}

///
/// \brief QtSurfacePlotWindow::SetData
/// \param data
/// \author		Dean
/// \since		27-08-2015
///
void QtSurfacePlotWindow::SetData(const Vector2f& data)
{
	ui->widgetSurfacePlot->SetData(data);
}
