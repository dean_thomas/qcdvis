#ifndef QTHISTOGRAMFORM_H
#define QTHISTOGRAMFORM_H

#include <QDialog>

namespace Ui {
	class QtHistogramForm;
}

class QtHistogramForm : public QDialog
{
		Q_OBJECT

	public:
		explicit QtHistogramForm(QWidget *parent = 0);
		~QtHistogramForm();

	private:
		Ui::QtHistogramForm *ui;
};

#endif // QTHISTOGRAMFORM_H
