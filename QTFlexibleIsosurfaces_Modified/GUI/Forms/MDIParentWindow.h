#ifndef MDIPARENTWINDOW_H
#define MDIPARENTWINDOW_H

#include "MDIChildWindow.h"

#include <vector>
#include <memory>

#include <QMainWindow>
#include <QFileDialog>
#include <QFileInfo>
#include <QMdiSubWindow>
#include <QTimer>
#include <QLabel>
#include <QList>

#include "DataModel.h"
#include "processinfo.h"
//#include "GUI/Widgets/QtVolumeSliceTreeWidget/QtHeightField3dItem.h"
#include "GUI/Widgets/QtIsosurfaceWidget/QtIsosurfaceWidget.h"
#include "GUI/Widgets/QtIsovalueBookmarkWidget/QtIsovalueBookmarkWidget.h"
#include "GUI/Forms/QtSurfacePlotWindow.h"
#include "GUI/Forms/QtProgressDialog.h"
#include "GUI/Forms/QtOriginOffsetDialog.h"


namespace Ui {
	class MDIParentWindow;
}

#define HIDE_BOOKMARK_TOOLBAR

#define MAX_RECENT_FILES 10

#define DISABLE_RESTORE_SETTINGS

class QAction;

class MDIParentWindow : public QMainWindow
{
		Q_OBJECT

		using RecentFileActionList = QList<QAction*>;
		using RecentFileList = QList<QString>;

		using Vector2f = Storage::Vector2<float>;
		//using UPtrWidget = std::unique_ptr<QWidget>;

	public:
		explicit MDIParentWindow(QWidget *parent = 0);
		explicit MDIParentWindow(DataModel& dataModel, QWidget *parent = 0);
		void AddChildWindow(HeightField3D *heightField3D,
							const QString& windowTitle,
							const QString& reeb_graph_file = "");
		~MDIParentWindow();

		RecentFileActionList m_recentFileActions;
		RecentFileList m_recentFiles;

		QMenu *m_windowMenu = nullptr;
		//std::vector<UPtrWidget> m_childWindows;

		void setupColourRampConnections();
	private :
		QDir m_save_to_path;
		QDir m_open_from_dir_path;

		QStringList getRecentFileStringList() const;

		QStringList loadSettings(const QString &filename = "", const bool restore_files = true);
		void saveSettings(const QString &filename = "") const;

		void addRecentFile(const QString& newFile);

		RecentFileActionList createRecentFileItems(const QStringList& recentFiles);
		QDir createCacheDir(const QString &filename);

		QList<QMdiSubWindow*> getMaximizedChildWindows() const;

		virtual void closeEvent(QCloseEvent* event);

	private slots:



		void on_action_Open_triggered();
		void initializeForm();
		void on_actionTileWindows_triggered();

		void on_actionCascadeWindows_triggered();
		void on_actionTileWindowsVertically_triggered();

		void on_actionTileWindowsHorizontally_triggered();

		void updateTimerTriggered();
		void on_actionMinimizeAll_triggered();

		void onCameraArcBallRotationChanged();
		void onGlobalIsovalueSliderChanged();
		void on_actionShowFarAxisLines_triggered(bool checked);

		//void OnUserClickedHeightField3dItem(QtHeightField3dItem*);
		void on_actionShowGrid_triggered(bool checked);

		void on_actionOuterCellsOnly_triggered(bool checked);

		void on_actionShowAxisLines_triggered(bool checked);

		void on_actionLabelAxisLines_triggered(bool checked);

		void OnArcBallCameraPositionScroll();
		void on_actionSetCamera_v0_triggered();


		void on_actionShowSurfaces_triggered(bool checked);

		void on_actionShowHypervolumePartitioningWindow_triggered(bool checked);

		void on_actionShowFloatingCameraToolbar_triggered(bool checked);

		void on_actionShowCameraSelectionToolbar_triggered(bool checked);

		void on_actionShowContourTreeToolbar_triggered(bool checked);

		void on_actionShowContourPropertiesToolbar_triggered(bool checked);



		void on_actionShowEdgeConnectivityWireframe_triggered(bool checked);

		void on_actionShowWireframe_triggered(bool checked);

		void onChildWindowSurfaceMouseWhellScroll(int delta);

		void on_actionSetGlobalIsovalue_triggered();

		void on_actionSurfacePlotTest_triggered();

		void on_actionRestoreContourTreeWidget_triggered();

		void on_actionTestGradient_triggered();

		void on_actionE_xit_triggered();

		void on_actionChooseUniversalColour_triggered();
		void on_actionChoosePositiveColour_triggered();
		void on_actionChooseNegativeColour_triggered();
		void on_actionChooseUniversalColourRamp_triggered();

		void on_actionUsePositiveNegativeColours_triggered();
		void on_actionUseUniversalColour_triggered();
		void on_actionUseUniversalColourRamp_triggered();

		void on_actionReverseUniversalColourRamp_triggered();

		void on_actionTest_New_Contour_Tree_Widget_triggered();

		void on_actionZoomIn_triggered();

		void on_actionZoomOut_triggered();

		void on_actionLoadTestReebGraph_triggered();

		void on_actionAutoScale_triggered(bool checked);

		void on_actionAlphaBlending_triggered(bool checked);

		void on_actionShowNestedContours_triggered(bool checked);

		void on_actionSetNumberOfNestedContours_triggered();

		void on_action1080Screenshot_triggered();

		void takeScreenshot(const QSize& dimensions);

		void on_action4kScreenshot_triggered();

		void on_actionShowCriticalNodeGlyphs_triggered(bool checked);

		void on_actionJoint_Contour_Net_triggered();


		void on_actionShowCTSkeleton_triggered(bool checked);

		void on_actionShowReebGraphSkeleton_triggered(bool checked);

		void on_actionOpenFromDirectory_triggered();

		void on_actionCloseAll_triggered();

		void on_actionSaveSessionData_triggered();

		void on_actionOpenSessionData_triggered();

		void on_actionSaveReebGraph_triggered();

		void on_actionSaveContourTree_triggered();

		void on_actionResetNestedIsovalueRange_triggered();

		void on_actionShowOpenEdgesOnly_triggered(bool checked);

		void on_actionIncreaseLineThickness_triggered();

		void on_actionDecreaseLineThickness_triggered();

		void on_actionSetLineColour_triggered();

		void on_actionSaveJoinTree_triggered();

		void on_actionSaveSplitTree_triggered();

		void on_actionSaveContourTree_2_triggered();

private:
		bool executeReebCommand(const QString& heightField3file,
								const QString& out_dir);
		void updateIsovalueHistograms(HeightField4D* hypervolume);

		bool is3dFileFormat(const QString& ext) const;
		bool is4dFileFormat(const QString& ext) const;
		bool LoadData(const QString& filename, const bool add_to_recent_files = true);
		bool Load3dData(const QString &filename);
		bool Load4dData(const QString &filename);
		//vector<MDIChildWindow*> m_childWindows;
		//QFileDialog *m_openDataDialog = nullptr;

		Ui::MDIParentWindow *ui;

		QLabel *m_labelVirtualMemory = nullptr;
		QLabel *m_labelPhysicalMemory = nullptr;

		QTimer *m_updateTimer = nullptr;
		DataModel m_dataModel;

		QDir m_session_dir;
		QDir m_colour_ramps_dir;

		QtIsovalueBookmarkWidget *m_isovalueBookmarWidget = nullptr;

		unsigned int m_nested_contour_count = 10;

		void setupToolbars();
};

#endif // MDIPARENTWINDOW_H
