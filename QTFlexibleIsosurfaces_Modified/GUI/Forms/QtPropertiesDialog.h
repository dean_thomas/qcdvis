#ifndef QTPROPERTIESDIALOG_H
#define QTPROPERTIESDIALOG_H

#include <QDialog>
#include <QTreeWidgetItem>

class ContourTreeSuperarc;

namespace Ui {
class QtPropertiesDialog;
}

class QtPropertiesDialog : public QDialog
{
	Q_OBJECT
private:
	QTreeWidgetItem* m_root_node = nullptr;

	QTreeWidgetItem* m_top_node_id = nullptr;
	QTreeWidgetItem* m_top_node_pos = nullptr;
	QTreeWidgetItem* m_top_node_value = nullptr;

	QTreeWidgetItem* m_bottom_node_id = nullptr;
	QTreeWidgetItem* m_bottom_node_pos = nullptr;
	QTreeWidgetItem* m_bottom_node_value = nullptr;

	QTreeWidgetItem* m_arc_length = nullptr;
	QTreeWidgetItem* m_surface_area = nullptr;
	QTreeWidgetItem* m_enclosed_volume = nullptr;

	QTreeWidgetItem* m_approx_up_volume_node_count = nullptr;
	QTreeWidgetItem* m_approx_down_volume_node_count = nullptr;
	QTreeWidgetItem* m_approx_volume_node_count = nullptr;

	QTreeWidgetItem* m_approx_up_volume_sample_count = nullptr;
	QTreeWidgetItem* m_approx_down_volume_sample_count = nullptr;
	QTreeWidgetItem* m_approx_volume_sample_count = nullptr;

	size_t m_samples = 8;
public:
	explicit QtPropertiesDialog(QWidget *parent = 0);
	void buildInitialModel();
	void SetSuperarc(ContourTreeSuperarc *superarc);
	~QtPropertiesDialog();

private:
	Ui::QtPropertiesDialog *ui;
};

#endif // QTPROPERTIESDIALOG_H
