#include "QtProgressDialog.h"
#include "ui_QtProgressDialog.h"

using std::endl;

///
/// \brief LoadProgressDialog::LoadProgressDialog
/// \param parent
/// \since		28-08-2015
/// \author		Dean
///
QtProgressDialog::QtProgressDialog(QWidget *parent) :
	QDialog(parent),
	ui(new Ui::LoadProgressDialog)
{
	ui->setupUi(this);

	m_currentAction = "";
}

///
/// \brief LoadProgressDialog::~LoadProgressDialog
/// \since		28-08-2015
/// \author		Dean
///
QtProgressDialog::~QtProgressDialog()
{
	delete ui;
}

///
/// \brief		Sets the current action (and pushes any existing action to
///				the cmopleted tasks list.
/// \param		action: QString - the new text to be displayed
/// \since		28-08-2015
/// \author		Dean
///
void QtProgressDialog::SetCurrentActionText(const QString &action)
{
	//	Push any existing tasks to the finished tasks list
	if (m_currentAction != "")
	{
		ui->plainTextEditCompletedTasks->insertPlainText(m_currentAction + "...done\n");
	}

	//	Set the new task
	m_currentAction = action;
	ui->labelCurrentTask->setText(m_currentAction);

	//	Make sure the UI updates
	QApplication::processEvents();
}

///
/// \brief		Steps the progress bar by the specifed amount
/// \param		step: integer (range 0..100)
/// \since		28-08-2015
/// \author		Dean
///
void QtProgressDialog::IncrementProgressBar(const size_t& step)
{
	assert (step <= 100);
	assert (ui->progressBar->value() + step <= 100);
	ui->progressBar->setValue(ui->progressBar->value() + step);

	//	Make sure the UI updates
	QApplication::processEvents();
}
