#ifndef QTORIGINOFFSETDIALOG_H
#define QTORIGINOFFSETDIALOG_H

#include <QDialog>

#include <cassert>

namespace Ui {
	class QtOriginOffsetDialog;
}

class QtOriginOffsetDialog : public QDialog
{
		Q_OBJECT

	public:
		explicit QtOriginOffsetDialog(QWidget *parent = 0);
		~QtOriginOffsetDialog();

		void SetMaximumXYZT(const size_t &x,
							const size_t &y,
							const size_t &z,
							const size_t &t);
		void SetValueXYZT(const size_t &x,
						  const size_t &y,
						  const size_t &z,
						  const size_t &t);

		size_t GetValueX() const;
		size_t GetValueY() const;
		size_t GetValueZ() const;
		size_t GetValueT() const;
	private:
		Ui::QtOriginOffsetDialog *ui;
};

#endif // QTORIGINOFFSETDIALOG_H
