#ifndef QTJOINTCONTOURNETFORM_H
#define QTJOINTCONTOURNETFORM_H

#include <QWidget>

namespace Ui {
	class QtJointContourNetForm;
}

class QtJointContourNetForm : public QWidget
{
		Q_OBJECT
private:
	void updateTransferFunctionInformation();
	public:
		QString getSourceDirectory() const;

		explicit QtJointContourNetForm(QWidget *parent = 0);

		void SetFloatingCameraPosition(const QQuaternion &camera, const float &radius);
		QVector3D GetFloatingCameraPosition() const;

		void setSourceDirectoryF0F1(const QString& dir);
		void setSourceDirectoryF0(const QString& dir);
		void setSourceDirectoryF1(const QString& dir);

		void setTitleF0(const QString& title);
		void setTitleF1(const QString& title);

		~QtJointContourNetForm();

private slots:
	void on_applyPushButton_clicked();

private:
		Ui::QtJointContourNetForm *ui;
};

#endif // QTJOINTCONTOURNETFORM_H
