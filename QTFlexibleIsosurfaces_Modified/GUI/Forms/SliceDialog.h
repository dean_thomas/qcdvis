#ifndef SLICEDIALOG_H
#define SLICEDIALOG_H

#include <QDialog>

namespace Ui {
	class SliceDialog;
}

class SliceDialog : public QDialog
{
		Q_OBJECT

	public:
		explicit SliceDialog(QWidget *parent = 0);
		~SliceDialog();

	private:
		Ui::SliceDialog *ui;
};

#endif // SLICEDIALOG_H
