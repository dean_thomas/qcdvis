#ifndef QTJCNDIALOG_H
#define QTJCNDIALOG_H

#include "GUI/Widgets/QtVolumeSelectionTreeView/QtVolumeSelectionTreeView.h"
#include <QDialog>
#include <QVector>
#include <QStringList>
#include <QDir>

namespace Ui {
class QtJcnDialog;
}

class QtJcnDialog : public QDialog
{
	Q_OBJECT
	QVector<QtVolumeStandardItem*> m_generatingItems;
	float m_slabsize0 = 0.0f;
	float m_slabsize1 = 0.0f;
	QDir m_output_root_dir;
public:
	explicit QtJcnDialog(QWidget *parent = 0);
	void setGeneratingItems(const QVector<QtVolumeStandardItem*>& items);
	~QtJcnDialog();

	QStringList getParamsF0() const;
	QStringList getParamsF1() const;
	QStringList getParamsF0F1() const;

	QDir getSlabsDirF0() const;
	QDir getSlabsDirF1() const;
	QDir getSlabsDirF0f1() const;

	float getSlabSizeF0() const { return m_slabsize0; }
	float getSlabSizeF1() const { return m_slabsize1; }
private:
	Ui::QtJcnDialog *ui;
};

#endif // QTJCNDIALOG_H
