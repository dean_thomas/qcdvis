///////////////////////////////////////////////////
//
//	Contour-tree based isosurfaces
//	Version 4.0 (QT-based)
//	copyright Hamish Carr
//	January, 2014
//
//	------------------------
//	ColourSelectionWidget.cpp
//	------------------------
//
//	A widget displaying colour choices
//
///////////////////////////////////////////////////

#include "ColourSelectionWidget.h"
#include "DataModel.h"

#define DEFAULT_COLOUR_SIZE 30

//  GL sizes don't match pixel size 1-to-1
//  so scale them for drawing
#define ACTUAL_COLOUR_SIZE (DEFAULT_COLOUR_SIZE / 3.5)
//#define ACTUAL_COLOUR_SIZE 8.5

ColourSelectionWidget::ColourSelectionWidget(DataModel *dataModel, QWidget *parent)
    : QGLWidget(parent)
{ // ColourSelectionWidget()
    // store pointer to the model
    m_dataModel = dataModel;
    // let QT know we are fixed size
    setMinimumHeight(DEFAULT_COLOUR_SIZE);
    setMaximumHeight(DEFAULT_COLOUR_SIZE);
	setMinimumWidth(DEFAULT_COLOUR_SIZE * 9);
	setMaximumWidth(DEFAULT_COLOUR_SIZE * 9);

} // ColourSelectionWidget()

ColourSelectionWidget::~ColourSelectionWidget()
{ // ~ColourSelectionWidget()
    // nothing yet
} // ~ColourSelectionWidget()

// called when OpenGL context is set up
void ColourSelectionWidget::initializeGL()
{ // ColourSelectionWidget::initializeGL()
    // this is straightforward 2D rendering, so disable stuff we don't need
    glDisable(GL_LIGHTING);
    glDisable(GL_DEPTH_TEST);
    // background is white
    glClearColor(1.0, 1.0, 1.0, 1.0);
} // ContourTreeWidget::initializeGL()

// called every time the widget is resized
void ColourSelectionWidget::resizeGL(int, int)
{
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    // set it to be wide enough for multiple colour boxes

	glOrtho(0.0, DEFAULT_COLOUR_SIZE * 9, 0.0, DEFAULT_COLOUR_SIZE, 1.0, -1.0);
}

// called every time the widget needs painting
void ColourSelectionWidget::paintGL()
{
}

void ColourSelectionWidget::mousePressEvent(QMouseEvent *event)
{ // ColourSelectionWidget::mousePressEvent()

    unsigned long colour = event->x() / DEFAULT_COLOUR_SIZE;
    //if (colour < 0)
    //    colour = 0;
    //else
	//if (colour > DrawingFunctions::SURFACE_COLOUR_COUNT)
	//    colour = DrawingFunctions::SURFACE_COLOUR_COUNT;
    emit chooseColour(colour);
} // ColourSelectionWidget::mousePressEvent()
