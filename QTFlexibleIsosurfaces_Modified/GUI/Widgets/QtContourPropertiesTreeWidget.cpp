#include "QtContourPropertiesTreeWidget.h"

///
/// \brief		Uses the observer pattern to update when notified
/// \param		observable: the object that raised the update
/// \author		Dean
/// \since		15-04-2015
///
void QtContourPropertiesTreeWidget::ReceiveUpdateNotification(const IObservable *changedObservable)
{
	RebuildTree();
}

void QtContourPropertiesTreeWidget::saveState()
{

}

unsigned long QtContourPropertiesTreeWidget::sphericalClassCount() const
{
	return m_sphereRootItem->childCount();
}

unsigned long QtContourPropertiesTreeWidget::torusClassCount() const
{
	return m_torusRootItem->childCount();
}

unsigned long QtContourPropertiesTreeWidget::twoTorusClassCount() const
{
	return m_twoTorusRootItem->childCount();
}

unsigned long QtContourPropertiesTreeWidget::nTorusClassCount() const
{
	return m_nTorusRootItem->childCount();
}

QtTreeWidgetContourItem *QtContourPropertiesTreeWidget::findRootContourItem(
		QTreeWidgetItem *item)
{
	//	Check we aren't already at the top
	if (!item->parent())
		return nullptr;

	//	Try to cast to our parent as a contour item
	QtTreeWidgetContourItem *contourItem =
			dynamic_cast<QtTreeWidgetContourItem*>(item->parent());

	//	If that fails we will recurse back to our parent
	if (contourItem != nullptr)
	{
		printf("Parent item has arcID: %ld.\n",
			   contourItem->GetGeneratingContour()->GetSuperarc()->GetId());
		fflush(stdout);

		return contourItem;
	}
	else
	{
		return findRootContourItem(item->parent());
	}
}

void QtContourPropertiesTreeWidget::mouseEnterStub(QTreeWidgetItem * item, int)
{
	//	Try to cast to our derived class for interacting with contours
	QtTreeWidgetContourItem *contourItem =
			dynamic_cast<QtTreeWidgetContourItem*>(item);

	//ContourList *contourList =
	//		m_dataModel->GetFlexibleIsosurface()->GetContourList();

	//	Start afresh...
	m_heightfield->GetContourTree()->ClearHighlightedArcs();

	if (contourItem != nullptr)
	{
#ifdef VERBOSE_OUTPUT
		//	Only if this directly relates to a contour...(nice and easy)
		printf("Mouse hovered item: %ld.\n",
			   contourItem->GetGeneratingContour().GetSuperarcIndex());
#endif
		contourItem->GetGeneratingContour()->SetHighlighted(true);
	}
	else
	{
		//	Check if we are hovering over an item below the contour item
		contourItem = findRootContourItem(item);

		if (contourItem != nullptr)
		{
#ifdef VERBOSE_OUTPUT
			printf("Mouse hovered item: %ld.\n",
				   contourItem->GetGeneratingContour().GetSuperarcIndex());
#endif
			contourItem->GetGeneratingContour()->SetHighlighted(true);
		}
		else
		{
			//	We must be hovering over an item above an actual contour
			//
			//	Maybe we could use this to highlight all contours with similar
			//	topological properties (ie those that are spherical)
		}
	}
#ifdef VERBOSE_OUTPUT
	fflush(stdout);
#endif
	emit HighlightedItemHasChanged();
}

void QtContourPropertiesTreeWidget::itemClickedStub(QTreeWidgetItem *item,
													int column)
{
	//	Try to cast to our derived class for interacting with contours
	QtTreeWidgetContourItem *contourItem =
			dynamic_cast<QtTreeWidgetContourItem*>(item);

	printf("Click!\n");

	//	Only if this relates to a contour...
	if (contourItem != nullptr)
	{
		printf("User clicked item: %s column: %i.\n", item->text(0).toStdString().c_str(), column);

		//m_dataModel->GetHeightfield()->GetCollapsibleContourTree()->AddToRestorableArcsArray(contourItem->GetGeneratingContour().GetSuperarcID());
		if (item->checkState(0) == Qt::Checked)
			contourItem->GetGeneratingContour()->GetSuperarc()->SetHidden(false);
		else
			contourItem->GetGeneratingContour()->GetSuperarc()->SetHidden(true);
	}

	fflush(stdout);
}

QColor toQColor(const Colour::Integer::RGB& rgb)
{
	return QColor(rgb.r, rgb.b, rgb.g);
}

QtContourPropertiesTreeWidget::QtContourPropertiesTreeWidget(QWidget *parent)
	: QTreeWidget(parent)
{
	//	We don't want a header
	setHeaderHidden(true);

	m_highlightColour = toQColor(DEFAULT_HIGHLIGHT_COLOUR);

	//setColumnCount(3);
	setIconSize(QSize(16,12));

	setMouseTracking(true);

#ifdef VERBOSE_OUTPUT
	printf("Connecting signal and slot.\n");
	fflush(stdout);
#endif

	//	Handle clicks on items
	QObject::connect(this, SIGNAL(itemClicked(QTreeWidgetItem*, int)),
					 this, SLOT(itemClickedStub(QTreeWidgetItem*, int)));

	//	Handle the mouse hovering over items
	QObject::connect(this, SIGNAL(itemEntered(QTreeWidgetItem*,int)),
					 this, SLOT(mouseEnterStub(QTreeWidgetItem*,int)));
}

QtContourPropertiesTreeWidget::~QtContourPropertiesTreeWidget()
{

}

void QtContourPropertiesTreeWidget::SetHeightfield(HeightField3D *heightfield)
{
	m_heightfield = heightfield;

	RebuildTree();
}

void QtContourPropertiesTreeWidget::addLevel0Nodes()
{
	//	These are top level categories (Roots)
	QTreeWidgetItem *currentIsovalueContoursItem = new QTreeWidgetItem(this);
	currentIsovalueContoursItem->setText(0, "Current Isovalue");
	currentIsovalueContoursItem->setExpanded(true);
	addLevel1Nodes(currentIsovalueContoursItem);

	addTopLevelItem(currentIsovalueContoursItem);
}

void QtContourPropertiesTreeWidget::addLevel1Nodes(QTreeWidgetItem *parent)
{
	//	These are basic closed/open contours categories
	//	Common to all Level 0 category objects

	m_closedContoursRootItem = new QTreeWidgetItem(parent);
	m_closedContoursRootItem->setText(0, "Closed contours");
	m_closedContoursRootItem->setExpanded(true);
	addLevel2aNodes(m_closedContoursRootItem);

	m_openContoursRootItem = new QTreeWidgetItem(parent);
	m_openContoursRootItem->setText(0, "Open contours");
	m_openContoursRootItem->setExpanded(true);
}

void QtContourPropertiesTreeWidget::addLevel2aNodes(QTreeWidgetItem *parent)
{
	//	These are categories only applicable to closed contours
	m_sphereRootItem = new QTreeWidgetItem(parent);
	m_sphereRootItem->setText(0, "Spherical");
	m_sphereRootItem->setExpanded(true);

	m_torusRootItem = new QTreeWidgetItem(parent);
	m_torusRootItem->setText(0, "Torus");
	m_torusRootItem->setExpanded(true);

	m_twoTorusRootItem = new QTreeWidgetItem(parent);
	m_twoTorusRootItem->setText(0, "2-Torus");
	m_twoTorusRootItem->setExpanded(true);

	m_nTorusRootItem = new QTreeWidgetItem(parent);
	m_nTorusRootItem->setText(0, "n-Torus");
	m_nTorusRootItem->setExpanded(true);
}

void QtContourPropertiesTreeWidget::addLevel2bNodes(QTreeWidgetItem *parent)
{
	//	These are categories only applicable to open contours
	QTreeWidgetItem *xMinItem = new QTreeWidgetItem(parent);
	xMinItem->setText(0, "Touching X_MIN edge");
	xMinItem->setExpanded(true);

	QTreeWidgetItem *xMaxItem = new QTreeWidgetItem(parent);
	xMaxItem->setText(0, "Touching X_MAX edge");
	xMaxItem->setExpanded(true);

	QTreeWidgetItem *yMinItem = new QTreeWidgetItem(parent);
	yMinItem->setText(0, "Touching Y_MIN edge");
	yMinItem->setExpanded(true);

	QTreeWidgetItem *yMaxItem = new QTreeWidgetItem(parent);
	yMaxItem->setText(0, "Touching Y_MAX edge");
	yMaxItem->setExpanded(true);

	QTreeWidgetItem *zMinItem = new QTreeWidgetItem(parent);
	zMinItem->setText(0, "Touching Z_MIN edge");
	zMinItem->setExpanded(true);

	QTreeWidgetItem *zMaxItem = new QTreeWidgetItem(parent);
	zMaxItem->setText(0, "Touching Z_MAX edge");
	zMinItem->setExpanded(true);
}

void QtContourPropertiesTreeWidget::addContour(Contour *contour)
{
	//	The immediate parent node
	QTreeWidgetItem* parentNode;

	//	Choose which category to put this under
	//	Open or closed contour?
	if (((contour->GetEulerCharacteristic() % 2) == 1)
		|| (contour->IsOnAnyBoundary()))
	{
		parentNode = topLevelItem(CATEGORY_CURRENT_ISOVALUE)->
				child(CATEGORY_OPEN_CONTOUR);
	}
	else
	{
		switch (contour->GetEulerCharacteristic())
		{
			case 2:
				parentNode = topLevelItem(CATEGORY_CURRENT_ISOVALUE)->
						child(CATEGORY_CLOSED_CONTOUR)->
						child(CATEGORY_SPHERE);
				break;
			case 0:
				parentNode = topLevelItem(CATEGORY_CURRENT_ISOVALUE)->
						child(CATEGORY_CLOSED_CONTOUR)->
						child(CATEGORY_TORUS);
				break;
			case -2:
				parentNode = topLevelItem(CATEGORY_CURRENT_ISOVALUE)->
						child(CATEGORY_CLOSED_CONTOUR)->
						child(CATEGORY_TWO_TORUS);
				break;
			default:
				parentNode = topLevelItem(CATEGORY_CURRENT_ISOVALUE)->
						child(CATEGORY_CLOSED_CONTOUR)->
						child(CATEGORY_N_TORUS);
				break;
		}
	}

	if (parentNode == nullptr)
	{
#ifdef VERBOSE_OUTPUT
		printf("Parent node is null?!\n");
		fflush(stdout);
#endif
	}

	//	Create a node for the new contour
	QTreeWidgetItem *newContourItem =
			 new QtTreeWidgetContourItem(parentNode, contour,
										  createSwatchIcon(contour));
}

void QtContourPropertiesTreeWidget::updateCategoryLabels()
{
	unsigned long closedContourCount = 0;

	closedContourCount += sphericalClassCount();
	closedContourCount += torusClassCount();
	closedContourCount += twoTorusClassCount();
	closedContourCount += nTorusClassCount();

	m_closedContoursRootItem->setText(0, QString("Closed contours (%1)").
									  arg(closedContourCount));
	m_sphereRootItem->setText(0, QString("Spherical (%1)").
							 arg(sphericalClassCount()));
	m_torusRootItem->setText(0, QString("Torus (%1)").
							 arg(torusClassCount()));
	m_twoTorusRootItem->setText(0, QString("2-Torus (%1)").
							 arg(twoTorusClassCount()));
	m_nTorusRootItem->setText(0, QString("n-Torus (%1)").
							 arg(nTorusClassCount()));

	m_openContoursRootItem->setText(0, QString("Open contours (%1)").
									arg(m_openContoursRootItem->childCount()));
}

void QtContourPropertiesTreeWidget::RebuildTree()
{
#ifdef VERBOSE_OUTPUT
	printf("Received rebuild request.\n");
	fflush(stdout);
#endif

	if (m_heightfield == nullptr) return;
//	if (m_heightfield->GetFlexibleIsosurface() == nullptr) return;
	if (m_heightfield->IsBusy()) return;

//	ContourList *contourList = m_heightfield->
//			GetFlexibleIsosurface()->GetContourList();

	//if (contourList == nullptr) return;

#ifdef VERBOSE_OUTPUT
	printf("Contour list: %x\n", 0);
	fflush(stdout);
#endif

	//	Rebuild the entire list each time (for now)
	clear();

	//	Recursively build the category lists
	addLevel0Nodes();

	//	Contour that are not selected
/*
	for (unsigned long i = 0; i < contourList->GetContourCount(); ++i)
	{
#ifdef VERBOSE_OUTPUT
		printf("Creating item %ld.\n", i);
		fflush(stdout);
#endif
		addContour(contourList->GetContour(i));
	}
*/
	//	Label top level categories with a count of how many items they contain
	updateCategoryLabels();
}

///
/// \brief		Create a colour swatch for a Contour
/// \param contour
/// \return
/// \author		Dean
/// \since		21-01-2015
///
QIcon QtContourPropertiesTreeWidget::createSwatchIcon(Contour *contour)
{
	QSize iconSize(16, 12);

	QPixmap pixmap(iconSize);
	QPainter *painter = new QPainter(&pixmap);

	QColor fillColour;

	if (contour->IsHighlighted())
	{
		fillColour = m_highlightColour;
	}
	else
	{
		fillColour = calculateArcColour(contour->GetSuperarc(),
										contour->GetTopSupernode(),
										contour->GetBottomSupernode());
	}

#ifdef VERBOSE_OUTPUT
	printf("Fill colour: %i %i %i.\n",
		   fillColour.r,
		   fillColour.g,
		   fillColour.b);
	fflush(stdout);
#endif

	painter->fillRect(QRect(QPoint(0,0),iconSize), fillColour);
	painter->drawRect(0, 0,
					  iconSize.width()-1,
					  iconSize.height()-1);

	delete painter;

	QIcon result;
	result.addPixmap(pixmap);

	return result;
}
