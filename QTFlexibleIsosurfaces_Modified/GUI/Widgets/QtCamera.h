#ifndef CAMERA_H
#define CAMERA_H

#include <QtOpenGL>

#include <iostream>
#include <ostream>
#include <string>

#include "../Globals/QtOstream.h"

#define TO_DEG(x) (x * (180.0 / M_PI))
#define TO_RAD(x) (x * (M_PI / 180.0))


class QtCamera
{
		//	Forward declaration of out ostream operator
		friend std::ostream& operator<<(std::ostream& os, const QtCamera& camera);

		QVector3D m_position;
		QMatrix4x4 m_viewMatrix;

		std::string m_cameraName;

	public:
		QVector3D GetPosition() const { return m_position; }

		void SetCameraName(const std::string &value) { m_cameraName = value; }
		std::string GetCameraName() const { return m_cameraName; }

		///
		/// \brief		Constructs a camera at (0,0,0)
		/// \author		Dean
		/// \since		15-07-2015
		///
		QtCamera(const std::string &cameraName = "")
		{
			m_position = QVector3D(0.0, 0.0, 0.0);
			m_cameraName = cameraName;
		}

		///
		/// \brief		Constructs a camera at the specified position in
		///				Cartesian space
		/// \author		Dean
		/// \since		17-07-2015
		///
		QtCamera(const QVector3D &position, const std::string &cameraName = "")
		{
			m_position = position;
			m_cameraName = cameraName;
		}

		///
		/// \brief		Constructs a camera at the specified position in
		///				Cartesian space and initialises the view matrix to
		///				look at the specified point in cartesian space
		/// \author		Dean
		/// \since		17-07-2015
		///
		QtCamera(const QVector3D &position, const QVector3D &lookAt,
				 const std::string &cameraName = "")
		{
			m_position = position;
			m_cameraName = cameraName;

			LookAt(lookAt);
		}

		///
		/// \brief		Set the orientation of the camera using a quaternion
		/// \param		quat
		/// \author		Dean
		/// \since		15-07-2015
		///
		void SetRotation(const QQuaternion &quat)
		{
			//	See: http://www.widecodes.com/CNVVkkqWee/quaternion-to-roll-pitch-yaw.html
			//	[10-07-2015]
			double qx = quat.x();
			double qy = quat.y();
			double qz = quat.z();
			double qw = quat.scalar();

			double rotateXa0 = 2.0*(qy*qz + qw*qx);
			double rotateXa1 = qw*qw - qx*qx - qy*qy + qz*qz;
			double rotateX = 0.0;
			if (rotateXa0 != 0.0 && rotateXa1 != 0.0)
				rotateX = atan2(rotateXa0, rotateXa1);

			double rotateYa0 = -2.0*(qx*qz - qw*qy);
			double rotateY = 0.0;
			if( rotateYa0 >= 1.0 )
				rotateY = M_PI/2.0;
			else if( rotateYa0 <= -1.0 )
				rotateY = -M_PI/2.0;
			else rotateY = asin(rotateYa0);

			double rotateZa0 = 2.0*(qx*qy + qw*qz);
			double rotateZa1 = qw*qw + qx*qx - qy*qy - qz*qz;
			double rotateZ = 0.0;
			if (rotateZa0 != 0.0 && rotateZa1 != 0.0)
				rotateZ = atan2(rotateZa0, rotateZa1);
		}

		///
		/// \brief		Sets the position of the camera on a sphere around
		///				the centre point with specified radius.  Then sets
		///				the view matrix to look at the specified position in
		///				Cartesian space
		/// \param quat
		/// \author		Dean
		/// \since		15-07-2015
		///
		void SetPosition(const QVector3D &centre,
						 const QQuaternion &quat,
						 const float &radius)
		{
			const QVector3D FORWARD_VECTOR(0, 0, -1);
			QMatrix4x4 sphericalMatrixPosition;
			sphericalMatrixPosition.rotate(quat);

			QVector3D r = sphericalMatrixPosition.transposed() * FORWARD_VECTOR;

			m_position = centre + (r * -radius);
		}

		///
		/// \brief		Sets the position of the camera on a sphere around
		///				the centre point with specified radius.  Then sets
		///				the view matrix to look at the specified position in
		///				Cartesian space
		/// \param quat
		/// \author		Dean
		/// \since		17-07-2015
		///
		void SetPosition(const QVector3D &centre,
						 const QQuaternion &quat,
						 const float &radius,
						 const QVector3D &lookAt)
		{
			SetPosition(centre, quat, radius);

			LookAt(lookAt);
		}

		///
		/// \brief		Returns the view matrix for the camera
		/// \return
		/// \since		17-07-2015
		/// \author		Dean
		///
		QMatrix4x4 GetViewMatrix() const
		{
			return m_viewMatrix;
		}

		///
		/// \brief		Calculates the view matrix required to look at the
		///				specified poitn using the current camera parameters
		/// \return		QMatrix4x4: the new view matrix
		/// \since		15-07-2015
		/// \author		Dean
		/// \author		Dean: fix gimbal look when looking straight up/down
		///				(with help from Joss) [03-12-2015]
		///
		void LookAt(const QVector3D &focusPoint)
		{
			//	Approximate up vector starts at world up
			QVector3D up(0, 1, 0);

			//	Compute the look direction
			QVector3D direction = (focusPoint - m_position).normalized();

			//	If we're looking along the y-axis
			if (fabs(direction.y()) == 1.0f)
			{
				//	Start the approximate up vector as looking along the z-axis
				up = QVector3D(0,0,1);
			}

			//	Compute the adjacent vector (perpendicular to current up
			//	and direction)
			QVector3D trueLeft = QVector3D::crossProduct(up, direction);

			//	Compute the actual up vector (perpendicular to true left
			//	and direction)
			QVector3D trueUp = QVector3D::crossProduct(trueLeft, direction);

			//	Use Qt's look at function to compute the new view matrix
			QMatrix4x4 temp;
			temp.lookAt(m_position, focusPoint, trueUp);
			m_viewMatrix = temp;
		}

		///
		/// \brief		Sets the position in space looked at by the camera
		/// \param pos
		/// \author		Dean
		/// \since		15-07-2015
		///
		//		void SetViewPoint(const QVector3D &pos)
		//		{
		//			m_focalPoint = pos;
		//		}

		///
		/// \brief GetYaw
		/// \param radians
		/// \return
		/// \author		Dean
		/// \since		15-07-2015
		///
		//		float GetYaw(const bool &radians = true)
		//		{
		//			if (radians)
		//			{
		//			return m_yawRadians;
		//			}
		//			else
		//			{
		//				return TO_DEG(m_yawRadians);
		//			}
		//		}

		///
		/// \brief SetYaw
		/// \param value
		/// \param radians
		/// \author		Dean
		/// \since		15-07-2015
		///
		//		void SetYaw(const float &value, const bool &radians = true)
		//		{
		//			if (radians)
		//			{
		//				m_yawRadians = value;
		//			}
		//			else
		//			{
		//				m_yawRadians = TO_RAD(value);
		//			}
		//		}

		///
		/// \brief GetPitch
		/// \param radians
		/// \return
		/// \author		Dean
		/// \since		15-07-2015
		///
		//		float GetPitch(const bool radians = true)
		//		{
		//			if (radians)
		//			{
		//			return m_pitchRadians;
		//			}
		//			else
		//			{
		//				return TO_DEG(m_pitchRadians);
		//			}
		//		}

		///
		/// \brief SetPitch
		/// \param value
		/// \param radians
		/// \author		Dean
		/// \since		15-07-2015
		///
		//		void SetPitch(const float &value, const bool radians = true)
		//		{
		//			if (radians)
		//			{
		//				m_pitchRadians = value;
		//			}
		//			else
		//			{
		//				m_pitchRadians = TO_RAD(value);
		//			}
		//		}

		///
		/// \brief GetRoll
		/// \param radians
		/// \return
		/// \author		Dean
		/// \since		15-07-2015
		///
		//		float GetRoll(const bool radians = true)
		//		{
		//			if (radians)
		//			{
		//			return m_rollRadians;
		//			}
		//			else
		//			{
		//				return TO_DEG(m_rollRadians);
		//			}
		//		}

		///
		/// \brief SetRoll
		/// \param value
		/// \param radians
		/// \author		Dean
		/// \since		15-07-2015
		///
		//		void SetRoll(const float &value, const bool radians = true)
		//		{
		//			if (radians)
		//			{
		//				m_rollRadians = value;
		//			}
		//			else
		//			{
		//				m_rollRadians = TO_RAD(value);
		//			}
		//		}

		///
		/// \brief		Returns the distance of the camera from the focal point
		/// \return
		/// \author		Dean
		/// \since		15-07-2015
		///
		//		float GetDistance() const
		//		{
		//			return m_distance;
		//		}

		///
		/// \brief		Sets the distance of the camera from the focal point
		/// \return
		/// \author		Dean
		/// \since		15-07-2015
		///
		//		void SetDistance(const float &value)
		//		{
		//			m_distance = value;

		//			calculatePositionVector();
		//		}
};

///
/// \brief operator <<
/// \param os
/// \param camera
/// \return
/// \since		02-12-2015
/// \author		Dean
///
inline std::ostream& operator<<(std::ostream& os, const QtCamera& camera)
{
	os << "{ QtCamera:"
	   << " name = " << camera.m_cameraName << ", "
	   << " position = " << camera.m_position << ", "
	   << " viewMatrix = " << camera.m_viewMatrix << " }";
	return os;
}

#endif // CAMERA

