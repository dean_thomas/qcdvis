#ifndef QTCONTOURPROPERTIESTREEWIDGET_H
#define QTCONTOURPROPERTIESTREEWIDGET_H

#include <QTreeWidget>

#include "DataModel.h"

#include "libDesignPatterns/DesignPatterns.h"
#include "AbstractColourMappedWidget.h"
#include <QTreeWidgetItem>
#include "QtTreeWidgetContourItem.h"

#define QCOLOR_SUPPORT
#include "Colour.h"
#include <QSettings>

#undef VERBOSE_OUTPUT
//#define VERBOSE_OUTPUT

using namespace Colour;

using DesignPatterns::IObservable;
using DesignPatterns::IObserver;

//#define VERBOSE_OUTPUT

class QtContourPropertiesTreeWidget :
		public QTreeWidget,
		public AbstractColourMappedWidget,
		public IObserver
{
		Q_OBJECT
	private:
		QSettings m_settings;

		//	Level 0 categories
		unsigned long CATEGORY_CURRENT_ISOVALUE	= 0;

		//	Level 1 categories
		unsigned long CATEGORY_CLOSED_CONTOUR	= 0;
		unsigned long CATEGORY_OPEN_CONTOUR		= 1;

		//	Level 2a categories
		unsigned long CATEGORY_SPHERE			= 0;
		unsigned long CATEGORY_TORUS			= 1;
		unsigned long CATEGORY_TWO_TORUS		= 2;
		unsigned long CATEGORY_N_TORUS			= 3;


		HeightField3D *m_heightfield = nullptr;

		QIcon createSwatchIcon(Contour *contour);

		QTreeWidgetItem *m_closedContoursRootItem;
		QTreeWidgetItem *m_openContoursRootItem;

		QTreeWidgetItem *m_sphereRootItem;
		QTreeWidgetItem *m_torusRootItem;
		QTreeWidgetItem *m_twoTorusRootItem;
		QTreeWidgetItem *m_nTorusRootItem;

		virtual void ReceiveUpdateNotification(const IObservable *changedObservable);

		void addLevel0Nodes();
		void addLevel1Nodes(QTreeWidgetItem *parent);
		void addLevel2aNodes(QTreeWidgetItem *parent);
		void addLevel2bNodes(QTreeWidgetItem *parent);

		void addContour(Contour *contour);

		QtTreeWidgetContourItem *findRootContourItem(QTreeWidgetItem * item);

		QColor m_highlightColour;

		void updateCategoryLabels();

		unsigned long sphericalClassCount() const;
		unsigned long torusClassCount() const;
		unsigned long twoTorusClassCount() const;
		unsigned long nTorusClassCount() const;

		void saveState();
	public:
		QtContourPropertiesTreeWidget(QWidget *parent = 0);

		~QtContourPropertiesTreeWidget();

		void SetHeightfield(HeightField3D *heightfield);

		void RebuildTree();
	signals:
		void HighlightedItemHasChanged() const;

	public slots:
		void itemClickedStub(QTreeWidgetItem *item, int column);
		void mouseEnterStub(QTreeWidgetItem * item, int column);
};


#endif // QTCONTOURPROPERTIESTREEWIDGET_H
