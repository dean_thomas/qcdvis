#ifndef QTMESH_H
#define QTMESH_H

#include <QtOpenGL>
#include <QVector>
#include <iostream>

#include "../Globals/QtFunctions.h"
//#include "string"

#ifndef __func__
#define __func__ __FUNCTION__
#endif

//using std::string;

#define DEFAULT_COLOUR QColor(Qt::gray)



class QtMesh
{
		friend std::ostream& operator<<(std::ostream&, const QtMesh&);

	public:
		struct VertexRecord
		{
			friend std::ostream& operator<<(std::ostream&, const VertexRecord&);
			size_t vertexId;
			size_t textureId;
			size_t normalId;
		};

		using Polygon = QVector<VertexRecord>;


	private:
		//	For now we'll only handle vertex indices defined with an absolute
		//	value
		static const size_t NO_ID = 0;


		QColor m_defaultColour = DEFAULT_COLOUR;

		QVector<Polygon> m_faces;

		QVector<QVector3D> m_normalLookup;
		QVector<QVector2D> m_textureLookup;
		QVector<QVector3D> m_vertexLookup;

		bool parseTexture(const QString &line);
		QVector3D parseNormal(const QString &line) const;
		QVector3D parseVertex(const QString &line) const;
		Polygon parseFaceString(const QString &line) const;
		VertexRecord parseVertexRecord(const QString &token) const;

		//QOpenGLBuffer m_vboVertexPosition;
		//QOpenGLBuffer m_vboVertexIndex;

		//QOpenGLVertexArrayObject *m_vao = nullptr;

		//	Storing the streams between calls gives a huge increase in
		//	performance
		mutable QVector<QVector3D> m_vertexStreamCache;
		mutable QVector<QVector4D> m_colourStreamCache;
		mutable QVector<QVector3D> m_normalStreamCache;
		void clearCache();
	public:
		size_t getNumberOfPolygons() const;
		size_t getNumberOfTriangles() const;
		double getSurfaceArea() const;
		bool isTriangulated() const;

		QColor getDefaultColour() const { return m_defaultColour; }
		void setDefaultColour(const QColor& colour, const bool resetCache = true);

		QVector<QVector3D> getVertexStream() const;
		QVector<QVector4D> getColourStream() const;
		QVector<QVector3D> getNormalStream() const;
		//QOpenGLVertexArrayObject *GetVAO() const { return m_vao; }

		QtMesh();
		QtMesh(const QString &filename);

		void Draw();

		bool LoadOBJ(const QString &filename);

		bool parseLine(const QString &line);
};

inline std::ostream& operator<<(std::ostream& os, const QtMesh::VertexRecord& rec)
{
	os << "{ " << rec.vertexId << "/" << rec.textureId << "/" << rec.normalId << " }";
	return os;
}

inline std::ostream& operator<<(std::ostream& os, const QtMesh::Polygon& poly)
{
	os << "{ ";
	for (auto& v : poly)
	{
		os << v;
	}
	os << " }";
	return os;
}

///
/// \brief		isTriangle: tests to see if the passed polygon is a polygon (defined by 3 vertices)
/// \param		polygon
/// \return
/// \author		Dean
/// \since		18-11-2016
///
inline bool isTriangle(const QtMesh::Polygon& polygon)
{
	return polygon.size() == 3;
}

#endif // QTMESH_H
