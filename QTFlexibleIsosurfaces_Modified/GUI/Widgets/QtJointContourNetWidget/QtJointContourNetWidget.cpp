#include "QtJointContourNetWidget.h"

#include <cassert>
#include <iostream>
#include <functional>
#include <QtConcurrent/QtConcurrent>
#include <climits>

#include "../Globals/Functions.h"

using namespace std;

///
/// \brief		Hacky method for changing the internal colourtable (note: will need the
///				user to re-select the transfer function to apply)
/// \param value
/// \author		Dean
/// \since		21-11-2016
///
void QtJointContourNetWidget::setColourTable(const QVector<QColor>& value)
{
	m_colourTable.setInternalArray(value);

	update();
}

///
/// \brief		Hacky method for retreiving the current internal colour table
/// \return
/// \author		Dean
/// \since		21-11-2016
///
QVector<QColor> QtJointContourNetWidget::getColourTable() const
{
	return m_colourTable.getInternalArray();
}

///
/// \brief	Calculates the upper and lower limits for the number of triangles in the
///			currently loaded set of meshes
///
/// \author		Dean
/// \since		21-11-2016
/// \return
///
std::pair<size_t, size_t> QtJointContourNetWidget::getTriangleCountLimits() const
{
	auto global_min = numeric_limits<size_t>::max();
	auto global_max = numeric_limits<size_t>::min();

	for (auto& mesh : m_boundaryMeshes)
	{
		auto triangleCount = mesh.getNumberOfTriangles();

		if (triangleCount > global_max) global_max = triangleCount;
		if (triangleCount < global_min) global_min = triangleCount;
	}
	return std::make_pair(global_min, global_max);
}

///
/// \brief	Calculates the upper and lower limits for the surface area in the
///			currently loaded set of meshes
/// \return
/// \author		Dean
/// \since		21-11-2016
///
std::pair<float, float> QtJointContourNetWidget::getSurfaceAreaLimits() const
{
	auto global_min = numeric_limits<float>::max();
	auto global_max = numeric_limits<float>::min();

	for (auto& mesh : m_boundaryMeshes)
	{
		auto surfaceArea = mesh.getSurfaceArea();

		if (surfaceArea > global_max) global_max = surfaceArea;
		if (surfaceArea < global_min) global_min = surfaceArea;
	}
	return std::make_pair(global_min, global_max);
}

///
/// \brief		Computes the range of triangle counts and then assigns
///				the colour using a transfer function within the limits
/// \author		Dean
/// \since		21-11-2016
///
void QtJointContourNetWidget::assignColourByTriangleCount(const bool assign_transparency)
{
	auto range = getTriangleCountLimits();

	for (auto& mesh : m_boundaryMeshes)
	{
		auto norm = normalize(get<0>(range), get<1>(range), mesh.getNumberOfTriangles());

		auto norm_colour = m_colourTable.at(norm);

		if (assign_transparency)
		{
			norm_colour.setAlphaF(norm);
		}

		mesh.setDefaultColour(norm_colour);
	}

	m_range = range;

	repaint();
}

///
/// \brief		Computes the range of surface areas and then assigns
///				the colour using a transfer function within the limits
/// \author		Dean
/// \since		21-11-2016
///
void QtJointContourNetWidget::assignColourBySurfaceArea(const bool assign_transparency)
{
	auto range = getSurfaceAreaLimits();

	for (auto& mesh : m_boundaryMeshes)
	{
		auto norm = normalize(get<0>(range), get<1>(range), static_cast<float>(mesh.getSurfaceArea()));

		auto norm_colour = m_colourTable.at(norm);

		if (assign_transparency)
		{
			norm_colour.setAlphaF(norm);
		}

		mesh.setDefaultColour(norm_colour);
	}

	m_range = range;

	repaint();
}

void QtJointContourNetWidget::paintEvent(QPaintEvent* event)
{
	assert(context() != nullptr);
	makeCurrent();

	//	Clear the widget to the background colour
	qglClearColor(Qt::white);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Axis3 axes = { { { 0.0f, 17.0f }, "x" },
	//			   { { 0.0f, 17.0f }, "y" },
	//			   { { 0.0f, 17.0f }, "z" } };
	//renderAxes(axes, false, 1.0f);

	//QtAbstract3dRenderedView::paintEvent(event);

	renderMeshes(m_boundaryMeshes);

	QPainter painter(this);

	//renderAxisLabels(&painter, axes);

	painter.end();
}

void QtJointContourNetWidget::renderMeshes(const QVector<QtMesh>& meshes)
{
	const QString name = "simplegradientshading";

	QString vShader = SHADER_DIR + name + ".vs.glsl";
	QString fShader = SHADER_DIR + name + ".fs.glsl";

	//	Temporary holder incase we fail
	QGLShaderProgram newShaderProgram(this);

	//	This is the main shader to be used for renderling the surfaces
	assert(newShaderProgram.addShaderFromSourceFile(QGLShader::Vertex,
													vShader));
	assert(newShaderProgram.addShaderFromSourceFile(QGLShader::Fragment,
													fShader));

	//	Make sure we link correctly
	assert(newShaderProgram.link());

	for (auto& m : meshes)
	{
		auto vertexStream = m.getVertexStream();
		auto colourStream = m.getColourStream();
		auto normalStream = m.getNormalStream();

		BufferData bufferData = { vertexStream, normalStream, colourStream };

		executeTriangleDrawingShader(&newShaderProgram, m_lightPosition, false, bufferData);
	}

	//
}


bool QtJointContourNetWidget::loadMeshesInDirectory(const QString& directory)
{
	//
	auto path = directory;

	//	If no path is specified launch a dialog to allow the user to chosoe one
	if (path == "")
	{
		//	Get the required directory from the user and collect a list of fiels
		//	to be parsed
		path = QFileDialog::getExistingDirectory(this, "Select slabs directory");
	}

	//	If the user still hasn't choosen an input path return false
	if (path == "") return false;
	auto dir = QDir(path);
	dir.setNameFilters({"*.obj"});

	auto colourId = 0;

	//	Create the future watcher for monitoring the tasks and performing
	//	the reduction
	using ReturnType = QVector<QtMesh>;
	QFutureWatcher<ReturnType> futureWatcher;

	//	Initialize the progress dialog and set the busy cursor
	QProgressDialog progressDialog("Loading meshes", "cancel", 0, dir.count(), this);
	QObject::connect(&futureWatcher, SIGNAL(finished()), &progressDialog, SLOT(reset()));
	QObject::connect(&progressDialog, SIGNAL(canceled()), &futureWatcher, SLOT(cancel()));
	QObject::connect(&futureWatcher, SIGNAL(progressRangeChanged(int,int)), &progressDialog, SLOT(setRange(int,int)));
	QObject::connect(&futureWatcher, SIGNAL(progressValueChanged(int)), &progressDialog, SLOT(setValue(int)));
	QApplication::setOverrideCursor(Qt::WaitCursor);

	//	Hacky transfer function that maps integers to qt's built in
	//	defined colours (ignoring greyscale)
	auto lookupColour = [](const size_t i)
	{
		//	Loop outside the range
		auto range = Qt::darkYellow - Qt::red;
		auto normI = Qt::red + (i % range);

		//	and return the enumeration
		assert(Qt::red <= normI && normI <= Qt::darkYellow);
		return QColor(static_cast<Qt::GlobalColor>(normI));
	};

	//	Our main worker threads take a string representing a file and return
	//	a mesh
	function<QtMesh(const QString&)> mapFunction =
			[&](const QString& filename) -> QtMesh
	{
		try
		{
			//	Load the next mesh
			auto mesh = loadMeshFromObjFile(dir.absolutePath() + "/" + filename);
			//mesh.setDefaultColour(lookupColour(processed));
			return mesh;
		}
		catch (std::invalid_argument& ex)
		{
			cerr << ex.what() << endl;
		}
	};

	//	Flatten the file list from each process into a single list for
	//	return
	function<void(ReturnType&, const QtMesh&)> reductionFunction =
			[&](ReturnType& fileList, const QtMesh& subList)
	{
		QtMesh meshCopy = subList;
		meshCopy.setDefaultColour(lookupColour(colourId), true);
		++colourId;
		fileList.push_back(meshCopy);
	};

	//	Store the source directory
	m_sourceDir = dir.absolutePath();

	//	Create a future that will produce a set of field array files for
	//	each input configuration
	auto fileList = dir.entryList();
	auto future = QtConcurrent::mappedReduced<ReturnType>(fileList,
														  mapFunction,
														  reductionFunction);

	cout << "About to load obj meshed from directory '" << path.toStdString() << "'." << endl;
	cout << "Filelist: [";
	for (auto& file : fileList)
	{
		cout << file.toStdString() << ", ";
	}
	cout << "]." << endl;

	//	Set the future and show the dialog
	futureWatcher.setFuture(future);
	progressDialog.exec();

	//	Retreive the output
	m_boundaryMeshes = futureWatcher.result();

	//	Restore the standard cursor
	QApplication::restoreOverrideCursor();

	//	Default colouring
	assignColourBySurfaceArea();

	//	Repaint widget
	update();
}

///
/// \brief QtJointContourNetWidget::QtJointContourNetWidget
/// \param parent
/// \author		Dean
/// \since		18-04-2016
///
QtJointContourNetWidget::QtJointContourNetWidget(QWidget* parent)
	: QtAbstract3dRenderedView(parent)
{
	m_tickFreqX = 1.0f;
	m_tickFreqY = 1.0f;
	m_tickFreqZ = 1.0f;

	m_colourTable.loadFromDat(R"(/home/dean/Documents/colour_ramps/yellow_red.dat)");
	cout << m_colourTable << endl;
}

///
/// \brief QtJointContourNetWidget::QtJointContourNetWidget
/// \param parent
/// \author		Dean
/// \since		19-12-2016
///
QtJointContourNetWidget::QtJointContourNetWidget(const QString& path, QWidget* parent)
	: QtAbstract3dRenderedView(parent)
{

}

QtMesh QtJointContourNetWidget::loadMeshFromObjFile(const QString& filename) const
{
	QtMesh result;

	if (result.LoadOBJ(filename))
	{
		cout << "Print loaded mesh from: '" << filename.toStdString() << "'." << endl;

		cout << "Mesh stats:" << endl
			 << "\tNumber of Polygons: " << result.getNumberOfPolygons() << endl
			 << "\tIs triangulated: " << result.isTriangulated() << endl
			 << "\tSurface area: " << result.getSurfaceArea() << endl;


		return result;
	}
	else
	{
		throw std::invalid_argument("The specified file: "
									+ filename.toStdString()
									+ " could not be loaded.");
	}
}
