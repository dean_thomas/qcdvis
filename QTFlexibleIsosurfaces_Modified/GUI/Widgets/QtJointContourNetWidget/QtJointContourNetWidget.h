#ifndef QTJOINTCONTOURNETWIDGET_H
#define QTJOINTCONTOURNETWIDGET_H

#include <utility>

#include "../QtAbstract3dRenderedView.h"
#include <QVector3D>
#include <QColor>

#include "../QtMesh.h"
#include "../QtColourLookupTable.h"

class QWidget;
class QPaintEvent;

class QtJointContourNetWidget :	public QtAbstract3dRenderedView
{
		Q_OBJECT

	private:
		QVector<QtMesh> m_boundaryMeshes;


		QtColourLookupTable m_colourTable;

		QString m_sourceDir;

//QVector<QColor>
		std::pair<size_t, size_t> getTriangleCountLimits() const;
		std::pair<float, float> getSurfaceAreaLimits() const;

		std::pair<float, float> m_range = std::make_pair(0.0f, 0.0f);

		void renderMeshes(const QVector<QtMesh>& meshes);


	public:
		QVector<QColor> getColourTable() const;
		void setColourTable(const QVector<QColor>& value);

		std::pair<float, float> getRange() const { return m_range; }

		QString GetSourceDir() const { return m_sourceDir; }

		void assignColourByTriangleCount(const bool assign_transparency = false);
		void assignColourBySurfaceArea(const bool assign_transparency = false);


		QtMesh loadMeshFromObjFile(const QString& filename) const;

		QtJointContourNetWidget(const QString &path, QWidget *parent = 0);
		explicit QtJointContourNetWidget(QWidget *parent = 0);
		bool loadMeshesInDirectory(const QString& directory = "");

		void paintEvent(QPaintEvent *event) override;

		QVector3D getLookAtPoint() const override { return { 8.0f, 8.0f, 8.0f };}
};

#endif // QTJOINTCONTOURNETWIDGET_H
