#ifndef COLOUR4_H
#define COLOUR4_H

//	Note: define this the project command line
//#define QCOLOR_SUPPORT

#ifdef QCOLOR_SUPPORT
#include <QColor>
#endif

namespace Colour
{
	namespace Integer
	{
		using Byte = unsigned char;

		struct HSL
		{
				Byte h;
				Byte s;
				Byte l;
		};

		struct HSV
		{
				Byte h;
				Byte s;
				Byte v;
		};

		///
		/// \brief		Stores a colour as a tuple of integers (0-255)
		///				representing the Red, Green and Blue channels
		///
		struct RGB
		{
				Byte r;
				Byte g;
				Byte b;

#ifdef QCOLOR_SUPPORT
				///
				/// \brief		Allows conversion to a QColor
				/// \example 	Colour::Integer::RGB foo(255, 0, 128);
				///				QColor bar = foo;
				/// \since		25-09-2015
				/// \author		Dean
				///
				operator QColor() const
				{
					return QColor(r, g, b);
				}


#endif
		};

		struct RGBA
		{
				Byte r;
				Byte g;
				Byte b;
				Byte a;
		};

		//	Definition of some colours in RGB
		//	see:	http://paulbourke.net/texture_colour/colourspace/
		//	[25-01-2015]
		namespace Whites
		{
			const RGB ANTIQUE_WHITE			= { 250, 235, 215 };
			const RGB AZURE					= { 240, 255, 255 };
			const RGB BISQUE				= { 255, 228, 196 };
			const RGB BLANCHED_ALMOND		= { 255, 235, 205 };
			const RGB CORNSILK				= { 255, 248, 220 };
			const RGB EGGSHELL				= { 252, 230, 201 };
			const RGB FLORAL_WHITE			= { 255, 250, 240 };
			const RGB GAINSBORO				= { 220, 220, 220 };
			const RGB GHOST_WHITE			= { 248, 248, 255 };
			const RGB HONEYDEW				= { 240, 255, 240 };
			const RGB IVORY					= { 255, 255, 240 };
			const RGB LAVENDER				= { 230, 230, 250 };
			const RGB LAVENDER_BLUSH		= { 255, 240, 245 };
			const RGB LEMON_CHIFFON			= { 255, 250, 205 };
			const RGB LINEN					= { 250, 240, 230 };
			const RGB MINT_CREAM			= { 245, 255, 250 };
			const RGB MISTY_ROSE			= { 255, 228, 225 };
			const RGB MOCCASIN				= { 255, 228, 181 };
			const RGB NAVAJO_WHITE			= { 255, 222, 173 };
			const RGB OLD_LACE				= { 253, 245, 230 };
			const RGB PAPAYA_WHIP			= { 255, 239, 213 };
			const RGB PEACH_PUFF			= { 255, 218, 185 };
			const RGB SEASHELL				= { 255, 245, 238 };
			const RGB SNOW					= { 255, 250, 250 };
			const RGB THISTLE				= { 216, 191, 216 };
			const RGB TITANIUM_WHITE		= { 252, 255, 240 };
			const RGB WHEAT					= { 245, 222, 179 };
			const RGB WHITE					= { 255, 255, 255 };
			const RGB WHITE_SMOKE			= { 245, 245, 245 };
			const RGB ZINC_WHITE			= { 253, 248, 255 };
		}

		namespace Greys
		{
			const RGB COLD_GREY				= { 128, 138, 135 };
			const RGB DIM_GREY				= { 105, 105, 105 };
			const RGB GREY					= { 192, 192, 192 };
			const RGB LIGHT_GREY			= { 211, 211, 211 };
			const RGB SLATE_GREY			= { 112, 128, 144 };
			const RGB SLATE_GREY_DARK		= {  47,  79,  79 };
			const RGB SLATE_GREY_LIGHT		= { 119, 136, 153 };
			const RGB WARM_GREY				= { 128, 128, 105 };
		}

		namespace Blacks
		{
			const RGB BLACK					= {   0,   0,   0};
			const RGB IVORY_BLACK			= {  41,  36,  33};
			const RGB LAMP_BLACK			= {  46,  71,  59};
		}

		namespace Reds
		{
			const RGB ALIZARIN_CRIMSON		= { 227,  38,  54};
			const RGB BRICK					= { 156, 102,  31};
			const RGB CADMIUM_RED_DEEP		= { 227,  23,  13};
			const RGB CORAL					= { 255, 127,  80};
			const RGB CORAL_LIGHT			= { 240, 128, 128};
			const RGB DEEP_PINK				= { 255,  20, 147};
			const RGB ENGLISH_RED			= { 212,  61,  26};
			const RGB FIREBRICK				= { 178,  34,  34};
			const RGB GERANIUM_LAKE			= { 227,  18,  48};
			const RGB HOT_PINK				= { 255, 105, 180};
			const RGB INDIAN_RED			= { 176,  23,  31};
			const RGB LIGHT_SALMON			= { 255, 160, 122};
			const RGB MADDER_LAKE_DEEP		= { 227,  46,  48};
			const RGB MAROON				= { 176,  48,  96};
			const RGB PINK					= { 255, 192, 203};
			const RGB PINK_LIGHT			= { 255, 182, 193};
			const RGB RASPBERRY				= { 135,  38,  87};
			const RGB RED					= { 255,   0,   0};
			const RGB ROSE_MADDER			= { 227,  54,  56};
			const RGB SALMON				= { 250, 128, 114};
			const RGB TOMATO				= { 255,  99,  71};
			const RGB VENETIAN_RED			= { 212,  26,  31};
		}

		namespace Browns
		{
			const RGB BEIGE             	= { 163, 148, 128 };
			const RGB BROWN             	= { 128,  42,  42 };
			const RGB BROWN_MADDER      	= { 219,  41,  41 };
			const RGB BROWN_OCHRE       	= { 135,  66,  31 };
			const RGB BURLYWOOD         	= { 222, 184, 135 };
			const RGB BURNT_SIENNA			= { 138,  54,  15 };
			const RGB BURNT_UMBER       	= { 138,  51,  36 };
			const RGB CHOCOLATE				= { 210, 105,  30 };
			const RGB DEEP_OCHRE			= { 115,  61,  26 };
			const RGB FLESH					= { 255, 125,  64 };
			const RGB FLESH_OCHRE			= { 255,  87,  33 };
			const RGB GOLD_OCHRE			= { 199, 120,  38 };
			const RGB GREENISH_UMBER		= { 255,  61,  13 };
			const RGB KHAKI					= { 240, 230, 140 };
			const RGB KHAKI_DARK			= { 189, 183, 107 };
			const RGB LIGHT_BEIGE			= { 245, 245, 220 };
			const RGB PERU					= { 205, 133,  63 };
			const RGB ROSY_BROWN			= { 188, 143, 143 };
			const RGB RAW_SIENNA			= { 199,  97,  20 };
			const RGB RAW_UMBER				= { 115,  74,  18 };
			const RGB SEPIA					= {  94,  38,  18 };
			const RGB SIENNA				= { 160,  82,  45 };
			const RGB SADDLE_BROWN			= { 139,  69,  19 };
			const RGB SANDY_BROWN			= { 244, 164,  96 };
			const RGB TAN					= { 210, 180, 140 };
			const RGB VAN_DYKE_BROWN		= {  94,  38,   5 };
		}

		namespace Oranges
		{
			const RGB CADMIUM_ORANGE		= { 255,  97,  3 };
			const RGB CADMIUM_RED_LIGHT		= { 255,   3, 13 };
			const RGB CARROT				= { 237, 145, 33 };
			const RGB DARK_ORANGE			= { 255, 140,  0 };
			const RGB MARS_ORANGE			= { 150,  69, 20 };
			const RGB MARS_YELLOW			= { 227, 112, 26 };
			const RGB ORANGE				= { 255, 128,  0 };
			const RGB ORANGE_RED			= { 255,  69,  0 };
			const RGB YELLOW_OCHRE			= { 227, 130, 23 };
		}

		namespace Yellows
		{
			const RGB AUREOLINE_YELLOW		= { 255, 168,  36 };
			const RGB BANANA				= { 227, 207,  87 };
			const RGB CADMIUM_LEMON			= { 255, 227,   3 };
			const RGB CADMIUM_YELLOW		= { 255, 153,  18 };
			const RGB GOLD					= { 255, 215,   0 };
			const RGB GOLDENROD				= { 218, 165,  32 };
			const RGB GOLDENROD_DARK		= { 184, 134,  11 };
			const RGB GOLDENROD_LIGHT		= { 250, 250, 210 };
			const RGB GOLDENROD_PALE		= { 238, 232, 170 };
			const RGB LIGHT_GOLDENROD		= { 238, 221, 130 };
			const RGB MELON					= { 227, 168, 105 };
			const RGB NAPLESYELLOWDEEP		= { 255, 168,  18 };
			const RGB YELLOW				= { 255, 255,   0 };
			const RGB YELLOW_LIGHT			= { 255, 255, 224 };
		}

		namespace Greens
		{
			const RGB CHARTREUSE			= { 127, 255,   0 };
			const RGB CHROMEOXIDEGREEN		= { 102, 128,  20 };
			const RGB CINNABAR_GREEN		= {  97, 179,  41 };
			const RGB COBALT_GREEN			= {  61, 145,  64 };
			const RGB EMERALD_GREEN			= {   0, 201,  87 };
			const RGB FOREST_GREEN			= {  34, 139,  34 };
			const RGB GREEN					= {   0, 255,   0 };
			const RGB GREEN_DARK			= {   0, 100,   0 };
			const RGB GREEN_PALE			= { 152, 251, 152 };
			const RGB GREEN_YELLOW			= { 173, 255,  47 };
			const RGB LAWN_GREEN			= { 124, 252,   0 };
			const RGB LIME_GREEN			= {  50, 205,  50 };
			const RGB MINT					= { 189, 252, 201 };
			const RGB OLIVE					= {  59,  94,  43 };
			const RGB OLIVE_DRAB			= { 107, 142,  35 };
			const RGB OLIVE_GREEN_DARK		= {  85, 107,  47 };
			const RGB PERMANENT_GREEN		= {  10, 201,  43 };
			const RGB SAP_GREEN				= {  48, 128,  20 };
			const RGB SEA_GREEN			    = {  46, 139,  87 };
			const RGB SEA_GREEN_DARK		= { 143, 188, 143 };
			const RGB SEA_GREEN_MEDIUM		= {  60, 179, 113 };
			const RGB SEA_GREEN_LIGHT		= {  32, 178, 170 };
			const RGB SPRING_GREEN			= {   0, 255, 127 };
			const RGB SPRING_GREENMEDIUM	= {   0, 250, 154 };
			const RGB TERRE_VERTE			= {  56,  94,  15 };
			const RGB VIRIDIAN_LIGHT		= { 110, 255, 112 };
			const RGB YELLOW_GREEN			= { 154, 205,  50 };
		}

		namespace Cyans
		{
			const RGB AQUAMARINE			= { 127, 255, 212 };
			const RGB AQUAMARINEMEDIUM		= { 102, 205, 170 };
			const RGB CYAN					= {   0, 255, 255 };
			const RGB CYAN_WHITE			= { 224, 255, 255 };
			const RGB TURQUOISE				= {  64, 224, 208 };
			const RGB TURQUOISE_DARK		= {   0, 206, 209 };
			const RGB TURQUOISE_MEDIUM		= {  72, 209, 204 };
			const RGB TURQUOISE_PALE		= { 175, 238, 238 };
		}

		namespace Blues
		{
			const RGB ALICE_BLUE			= { 240, 248, 255 };
			const RGB BLUE					= {   0,   0, 255 };
			const RGB BLUE_LIGHT			= { 173, 216, 230 };
			const RGB BLUE_MEDIUM			= {   0,   0, 205 };
			const RGB CADET					= {  95, 158, 160 };
			const RGB COBALT				= {  61,  89, 171 };
			const RGB CORNFLOWER			= { 100, 149, 237 };
			const RGB CERULEAN				= {   5, 184, 204 };
			const RGB DODGER_BLUE			= {  30, 144, 255 };
			const RGB INDIGO				= {   8,  46,  84 };
			const RGB MANGANESE_BLUE		= {   3, 168, 158 };
			const RGB MIDNIGHT_BLUE			= {  25,  25, 112 };
			const RGB NAVY					= {   0,   0, 128 };
			const RGB PEACOCK				= {  51, 161, 201 };
			const RGB POWDER_BLUE			= { 176, 224, 230 };
			const RGB ROYAL_BLUE			= {  65, 105, 225 };
			const RGB SLATE_BLUE			= { 106,  90, 205 };
			const RGB SLATE_BLUE_DARK		= {  72,  61, 139 };
			const RGB SLATE_BLUE_LIGHT		= { 132, 112, 255 };
			const RGB SLATE_BLUE_MEDIUM		= { 123, 104, 238 };
			const RGB SKY_BLUE				= { 135, 206, 235 };
			const RGB SKY_BLUE_DEEP			= {   0, 191, 255 };
			const RGB SKY_BLUE_LIGHT		= { 135, 206, 250 };
			const RGB STEEL_BLUE			= {  70, 130, 180 };
			const RGB STEEL_BLUE_LIGHT		= { 176, 196, 222 };
			const RGB TURQUOISE_BLUE		= {   0, 199, 140 };
			const RGB ULTRAMARINE			= {  18,  10, 143 };
		}

		namespace Magentas
		{
			const RGB BLUE_VIOLET			= { 138,  43, 226 };
			const RGB COBALT_VIOLETDEEP		= { 145,  33, 158 };
			const RGB MAGENTA				= { 255,   0, 255 };
			const RGB ORCHID				= { 218, 112, 214 };
			const RGB ORCHID_DARK			= { 153,  50, 204 };
			const RGB ORCHID_MEDIUM			= { 186,  85, 211 };
			const RGB PERMANENT_VIOLET		= { 219,  38,  69 };
			const RGB PLUM					= { 221, 160, 221 };
			const RGB PURPLE				= { 160,  32, 240 };
			const RGB PURPLE_MEDIUM			= { 147, 112, 219 };
			const RGB ULTRAMARINE_VIOLET	= {  92,  36, 110 };
			const RGB VIOLET				= { 143,  94, 153 };
			const RGB VIOLET_DARK			= { 148,   0, 211 };
			const RGB VIOLET_RED			= { 208,  32, 144 };
			const RGB VIOLET_REDMEDIUM		= { 199,  21, 133 };
			const RGB VIOLET_RED_PALE		= { 219, 112, 147 };
		}
	}

	namespace FloatingPoint
	{
		struct HSL
		{
				float h;
				float s;
				float l;
		};

		struct HSV
		{
				float h;
				float s;
				float v;
		};

		struct RGB
		{
				float r;
				float g;
				float b;
		};

		struct RGBA
		{
				float r;
				float g;
				float b;
				float a;
		};

		namespace Whites
		{

		}

		namespace Greys
		{
			const RGB COLD_GREY				= { 128.0f / 255.0f,
												138.0f / 255.0f,
												135.0f / 255.0f };

			const RGB DIM_GREY				= { 105.0f / 255.0f,
												105.0f / 255.0f,
												105.0f / 255.0f };

			const RGB GREY					= { 192.0f / 255.0f,
												192.0f / 255.0f,
												192.0f / 255.0f };

			const RGB LIGHT_GREY			= { 211.0f / 255.0f,
												211.0f / 255.0f,
												211.0f / 255.0f };

			const RGB SLATE_GREY			= { 112.0f / 255.0f,
												128.0f / 255.0f,
												144.0f / 255.0f };

			const RGB SLATE_GREY_DARK		= {  47.0f / 255.0f,
												 79.0f / 255.0f,
												 79.0f / 255.0f };

			const RGB SLATE_GREY_LIGHT		= { 119.0f / 255.0f,
												136.0f / 255.0f,
												153.0f / 255.0f };

			const RGB WARM_GREY				= { 128.0f / 255.0f,
												128.0f / 255.0f,
												105.0f / 255.0f };
		}

		namespace Blacks
		{
			const RGB BLACK					= {   0.0f / 255.0f,
												  0.0f / 255.0f,
												  0.0f / 255.0f};

			const RGB IVORY_BLACK			= {  41.0f / 255.0f,
												 36.0f / 255.0f,
												 33.0f / 255.0f};

			const RGB LAMP_BLACK			= {  46.0f / 255.0f,
												 71.0f / 255.0f,
												 59.0f / 255.0f};
		}

		namespace Reds
		{
			const RGB ALIZARIN_CRIMSON		= { 227.0f / 255.0f,
												38.0f / 255.0f,
												54.0f / 255.0f};

			const RGB BRICK					= { 156.0f / 255.0f,
												102.0f / 255.0f,
												31.0f / 255.0f};

			const RGB CADMIUM_RED_DEEP		= { 227.0f / 255.0f,
												23.0f / 255.0f,
												13.0f / 255.0f};

			const RGB CORAL					= { 255.0f / 255.0f,
												127.0f / 255.0f,
												80.0f / 255.0f};

			const RGB CORAL_LIGHT			= { 240.0f / 255.0f,
												128.0f / 255.0f,
												128.0f / 255.0f};

			const RGB DEEP_PINK				= { 255.0f / 255.0f,
												20.0f / 255.0f,
												147.0f / 255.0f};

			const RGB ENGLISH_RED			= { 212.0f / 255.0f,
												61.0f / 255.0f,
												26.0f / 255.0f};

			const RGB FIREBRICK				= { 178.0f / 255.0f,
												34.0f / 255.0f,
												34.0f / 255.0f};

			const RGB GERANIUM_LAKE			= { 227.0f / 255.0f,
												18.0f / 255.0f,
												48.0f / 255.0f};

			const RGB HOT_PINK				= { 255.0f / 255.0f,
												105.0f / 255.0f,
												180.0f / 255.0f};

			const RGB INDIAN_RED			= { 176.0f / 255.0f,
												23.0f / 255.0f,
												31.0f / 255.0f};

			const RGB LIGHT_SALMON			= { 255.0f / 255.0f,
												160.0f / 255.0f,
												122.0f / 255.0f};

			const RGB MADDER_LAKE_DEEP		= { 227.0f / 255.0f,
												46.0f / 255.0f,
												48.0f / 255.0f};

			const RGB MAROON				= { 176.0f / 255.0f,
												48.0f / 255.0f,
												96.0f / 255.0f};

			const RGB PINK					= { 255.0f / 255.0f,
												192.0f / 255.0f,
												203.0f / 255.0f};

			const RGB PINK_LIGHT			= { 255.0f / 255.0f,
												182.0f / 255.0f,
												193.0f / 255.0f};

			const RGB RASPBERRY				= { 135.0f / 255.0f,
												38.0f / 255.0f,
												87.0f / 255.0f};

			const RGB RED					= { 255.0f / 255.0f,
												0.0f / 255.0f,
												0.0f / 255.0f};

			const RGB ROSE_MADDER			= { 227.0f / 255.0f,
												54.0f / 255.0f,
												56.0f / 255.0f};

			const RGB SALMON				= { 250.0f / 255.0f,
												128.0f / 255.0f,
												114.0f / 255.0f};

			const RGB TOMATO				= { 255.0f / 255.0f,
												99.0f / 255.0f,
												71.0f / 255.0f};

			const RGB VENETIAN_RED			= { 212.0f / 255.0f,
												26.0f / 255.0f,
												31.0f / 255.0f};
		}

		namespace Greens
		{
			const RGB CHARTREUSE			= { 127.0f / 255.0f,
												255.0f / 255.0f,
												0.0f / 255.0f };

			const RGB CHROMEOXIDEGREEN		= { 102.0f / 255.0f,
												128.0f / 255.0f,
												20.0f / 255.0f };

			const RGB CINNABAR_GREEN		= {  97.0f / 255.0f,
												 179.0f / 255.0f,
												 41.0f / 255.0f };

			const RGB COBALT_GREEN			= {  61.0f / 255.0f,
												 145.0f / 255.0f,
												 64.0f / 255.0f };

			const RGB EMERALD_GREEN			= {   0.0f / 255.0f,
												  201.0f / 255.0f,
												  87.0f / 255.0f };

			const RGB FOREST_GREEN			= {  34.0f / 255.0f,
												 139.0f / 255.0f,
												 34.0f / 255.0f };

			const RGB GREEN					= {   0.0f / 255.0f,
												  255.0f / 255.0f,
												  0.0f / 255.0f };

			const RGB GREEN_DARK			= {   0.0f / 255.0f,
												  100.0f / 255.0f,
												  0.0f / 255.0f };

			const RGB GREEN_PALE			= { 152.0f / 255.0f,
												251.0f / 255.0f,
												152.0f / 255.0f };

			const RGB GREEN_YELLOW			= { 173.0f / 255.0f,
												255.0f / 255.0f,
												47.0f / 255.0f };

			const RGB LAWN_GREEN			= { 124.0f / 255.0f,
												252.0f / 255.0f,
												0.0f / 255.0f };

			const RGB LIME_GREEN			= {  50.0f / 255.0f,
												 205.0f / 255.0f,
												 50.0f / 255.0f };

			const RGB MINT					= { 189.0f / 255.0f,
												252.0f / 255.0f,
												201.0f / 255.0f };

			const RGB OLIVE					= {  59.0f / 255.0f,
												 94.0f / 255.0f,
												 43.0f / 255.0f };

			const RGB OLIVE_DRAB			= { 107.0f / 255.0f,
												142.0f / 255.0f,
												35.0f / 255.0f };

			const RGB OLIVE_GREEN_DARK		= {  85.0f / 255.0f,
												 107.0f / 255.0f,
												 47.0f / 255.0f };

			const RGB PERMANENT_GREEN		= {  10.0f / 255.0f,
												 201.0f / 255.0f,
												 43.0f / 255.0f };

			const RGB SAP_GREEN				= {  48.0f / 255.0f,
												 128.0f / 255.0f,
												 20.0f / 255.0f };

			const RGB SEA_GREEN			    = {  46.0f / 255.0f,
												 139.0f / 255.0f,
												 87.0f / 255.0f };

			const RGB SEA_GREEN_DARK		= { 143.0f / 255.0f,
												188.0f / 255.0f,
												143.0f / 255.0f };

			const RGB SEA_GREEN_MEDIUM		= {  60.0f / 255.0f,
												 179.0f / 255.0f,
												 113.0f / 255.0f };

			const RGB SEA_GREEN_LIGHT		= {  32.0f / 255.0f,
												 178.0f / 255.0f,
												 170.0f / 255.0f };

			const RGB SPRING_GREEN			= {   0.0f / 255.0f,
												  255.0f / 255.0f,
												  127.0f / 255.0f };

			const RGB SPRING_GREENMEDIUM	= {   0.0f / 255.0f,
												  250.0f / 255.0f,
												  154.0f / 255.0f };

			const RGB TERRE_VERTE			= {  56.0f / 255.0f,
												 94.0f / 255.0f,
												 15.0f / 255.0f };

			const RGB VIRIDIAN_LIGHT		= { 110.0f / 255.0f,
												255.0f / 255.0f,
												112.0f / 255.0f };

			const RGB YELLOW_GREEN			= { 154.0f / 255.0f,
												205.0f / 255.0f,
												50.0f / 255.0f };
		}

		namespace Blues
		{
			const RGB ALICE_BLUE			= { 240.0f / 255.0f,
												248.0f / 255.0f,
												255.0f / 255.0f };

			const RGB BLUE					= {   0.0f / 255.0f,
												  0.0f / 255.0f,
												  255.0f / 255.0f };

			const RGB BLUE_LIGHT			= { 173.0f / 255.0f,
												216.0f / 255.0f,
												230.0f / 255.0f };

			const RGB BLUE_MEDIUM			= {   0.0f / 255.0f,
												  0.0f / 255.0f,
												  205.0f / 255.0f };

			const RGB CADET					= {  95.0f / 255.0f,
												 158.0f / 255.0f,
												 160.0f / 255.0f };

			const RGB COBALT				= {  61.0f / 255.0f,
												 89.0f / 255.0f,
												 171.0f / 255.0f };

			const RGB CORNFLOWER			= { 100.0f / 255.0f,
												149.0f / 255.0f,
												237.0f / 255.0f };

			const RGB CERULEAN				= {   5.0f / 255.0f,
												  184.0f / 255.0f,
												  204.0f / 255.0f };

			const RGB DODGER_BLUE			= {  30.0f / 255.0f,
												 144.0f / 255.0f,
												 255.0f / 255.0f };

			const RGB INDIGO				= {   8.0f / 255.0f,
												  46.0f / 255.0f,
												  84.0f / 255.0f };

			const RGB MANGANESE_BLUE		= {   3.0f / 255.0f,
												  168.0f / 255.0f,
												  158.0f / 255.0f };

			const RGB MIDNIGHT_BLUE			= {  25.0f / 255.0f,
												 25.0f / 255.0f,
												 112.0f / 255.0f };

			const RGB NAVY					= {   0.0f / 255.0f,
												  0.0f / 255.0f,
												  128.0f / 255.0f };

			const RGB PEACOCK				= {  51.0f / 255.0f,
												 161.0f / 255.0f,
												 201.0f / 255.0f };

			const RGB POWDER_BLUE			= { 176.0f / 255.0f,
												224.0f / 255.0f,
												230.0f / 255.0f };

			const RGB ROYAL_BLUE			= {  65.0f / 255.0f,
												 105.0f / 255.0f,
												 225.0f / 255.0f };

			const RGB SLATE_BLUE			= { 106.0f / 255.0f,
												90.0f / 255.0f,
												205.0f / 255.0f };

			const RGB SLATE_BLUE_DARK		= {  72.0f / 255.0f,
												 61.0f / 255.0f,
												 139.0f / 255.0f };

			const RGB SLATE_BLUE_LIGHT		= { 132.0f / 255.0f,
												112.0f / 255.0f,
												255.0f / 255.0f };

			const RGB SLATE_BLUE_MEDIUM		= { 123.0f / 255.0f,
												104.0f / 255.0f,
												238.0f / 255.0f };

			const RGB SKY_BLUE				= { 135.0f / 255.0f,
												206.0f / 255.0f,
												235.0f / 255.0f };

			const RGB SKY_BLUE_DEEP			= {   0.0f / 255.0f,
												  191.0f / 255.0f,
												  255.0f / 255.0f };

			const RGB SKY_BLUE_LIGHT		= { 135.0f / 255.0f,
												206.0f / 255.0f,
												250.0f / 255.0f };

			const RGB STEEL_BLUE			= {  70.0f / 255.0f,
												 130.0f / 255.0f,
												 180.0f / 255.0f };

			const RGB STEEL_BLUE_LIGHT		= { 176.0f / 255.0f,
												196.0f / 255.0f,
												222.0f / 255.0f };

			const RGB TURQUOISE_BLUE		= {   0.0f / 255.0f,
												  199.0f / 255.0f,
												  140.0f / 255.0f };

			const RGB ULTRAMARINE			= {  18.0f / 255.0f,
												 10.0f / 255.0f,
												 143.0f / 255.0f };
		}

		struct Colour3f
		{
				float r;
				float g;
				float b;
		};

		struct Colour4f
		{
				float r;
				float g;
				float b;
				float a;

				Colour4f()
					: r{}, g{}, b{}, a{}
				{ }

				Colour4f(float red, float green, float blue, float alpha)
					: r{red}, g{green}, b{blue}, a{alpha}
				{ }

				///
				/// \brief		Convert from an integer RGB colour
				/// \param		colour: an RGB colour with 8-bit integer values
				/// \param		alpha: the desired alpha level (0.0 - 1.0)
				/// \since		10-02-2015
				///	\author		Dean
				///
				Colour4f(const Integer::RGB colour, const float alpha)
					: a{alpha}
				{
					r = colour.r / 255.0f;
					g = colour.g / 255.0f;
					b = colour.b / 255.0f;
				}

				///
				/// \brief		Convert from a Colour3f::RGB
				/// \param		colour: an RGB colour with 8-float values
				/// \param		alpha: the desired alpha level (0.0 - 1.0)
				/// \since		23-03-2015
				///	\author		Dean
				///
				Colour4f(const RGB colour, const float alpha)
					: r{colour.r}, g{colour.g}, b{colour.b}, a{alpha}
				{

				}

				Colour4f(const Colour3f colour, const float alpha)
					: r{colour.r}, g{colour.g}, b{colour.b}, a{alpha}
				{ }
		};

		const Colour3f BLACK = { 0.0, 0.0, 0.0 };

		const Colour3f GREEN = { 0.0, 1.0, 0.0 };

		const Colour4f OPAQUE_RED =		{ 1.0, 0.0, 0.0, 1.0};
		const Colour4f OPAQUE_BLACK =	{ BLACK, 1.0};
		const Colour4f OPAQUE_GREY =		{ 0.5, 0.5, 0.5, 1.0};
		const Colour4f OPAQUE_GREEN =		{ GREEN, 1.0 };


		const Colour4f SEMI_TRANSPARENT_BLACK = { 0.0, 0.0, 0.0, 0.75 };

	}


}


#endif // COLOUR4_H
