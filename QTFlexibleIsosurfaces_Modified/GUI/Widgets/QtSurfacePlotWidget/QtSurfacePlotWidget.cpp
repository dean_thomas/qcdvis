#include "QtSurfacePlotWidget.h"

using std::unique_ptr;
using std::cout;
using std::endl;
using std::minmax;

#ifdef _MSC_VER
#define __PRETTY_FUNCTION__ __FUNCSIG__
#endif

///
/// \brief QtSurfacePlotWindow::normalize
/// \param unNormalized
/// \return
/// \since		25-09-2015
/// \author		Dean
///
float QtSurfacePlotWidget::normalize(const float& unNormalized) const
{
	if (m_dataMax - m_dataMin == 0.0) return 0.0;

	float result = ((float)unNormalized - m_dataMin) / (m_dataMax - m_dataMin);

	assert((result >= 0.0) && (result <= 1.0));

	return result;
}

///
/// \brief		Asscociates the SurfacePlotWidget with a Colour Ramp
/// \param widget
/// \since		25-09-2015
/// \author		Dean
///
void QtSurfacePlotWidget::SetColourRampWidget(QtColourRampWidget* widget)
{
	assert (widget != nullptr);

	m_colourRampWidget = widget;

	repaint();
}

///
/// \brief		Gets the min and max values in the data set
/// \since		25-09-2015
/// \author		Dean
///
void QtSurfacePlotWidget::recalculateMinMax()
{
	cout << "Size: " << m_data.size() << endl;

	auto dataMinMax = std::minmax_element(m_data.begin(), m_data.end());

	m_dataMin = *(dataMinMax.first);
	m_dataMax = *(dataMinMax.second);

	cout << "Data min: " << std::setprecision(4) << m_dataMin << endl;
	cout << "Data max: " << std::setprecision(4) << m_dataMax << endl;
}

///
/// \brief		Labels the 3 axis on the graph
/// \since		25-09-2015
/// \author		Dean
///
void QtSurfacePlotWidget::renderAxisLabels(QPainter *painter)
{
	assert(context() != nullptr);

	//	Amount to offset the labels from the actual axes
	const float NUM_OFFSET = -0.5;
	const float TEXT_OFFSET = -1.0;

	//	Find the max value on the Z axis
	recalculateMinMax();

	//	Roughly the midpoint of each axis
	const float midX = (m_data.size_x()-1.0) / 2.0;
	const float midY = (m_data.size_y()-1.0) / 2.0;
	const float midZ = m_dataMax / 2.0;

	//	Project the position of the zero label to screen space
	//QPoint posZero = projectToScreenSpace(QVector3D(0 + NUM_OFFSET,
	//												0 + NUM_OFFSET,
	//												0 + NUM_OFFSET));

	//	Project the maximum point labels to screen space
	QPoint posMaxX = projectToScreenSpace(QVector3D(m_data.size_x()-1.0,
													0 + NUM_OFFSET,
													0 + NUM_OFFSET));
	QPoint posMaxY = projectToScreenSpace(QVector3D(0 + NUM_OFFSET,
													m_data.size_y()-1.0,
													0 + NUM_OFFSET));
	QPoint posMaxZ = projectToScreenSpace(QVector3D(0 + NUM_OFFSET,
													0 + NUM_OFFSET,
													m_dataMax));

	//	Project the midpoint labels to screen space
	QPoint posMidX = projectToScreenSpace(QVector3D(midX,
													0 + TEXT_OFFSET,
													0 + TEXT_OFFSET));
	QPoint posMidY = projectToScreenSpace(QVector3D(0 + TEXT_OFFSET,
													midY,
													0 + TEXT_OFFSET));
	QPoint posMidZ = projectToScreenSpace(QVector3D(0 + TEXT_OFFSET,
													0 + TEXT_OFFSET,
													midZ));

	//	Make it look nice
	painter->setRenderHint(QPainter::TextAntialiasing);

	//	Zero label
	//painter->drawText(posZero.x(), posZero.y(), "0");

	//	Maximum labels
	painter->drawText(posMaxX.x(), posMaxX.y(), QString("%1")
					  .arg(m_data.size_x()-1.0));
	painter->drawText(posMaxY.x(), posMaxY.y(), QString("%1")
					  .arg(m_data.size_y()-1.0));
	painter->drawText(posMaxZ.x(), posMaxZ.y(), QString("%1")
					  .arg(m_dataMax));

	//	Axis titles
	painter->drawText(posMidX.x(), posMidX.y(),
					  QString(QChar('x')));
	painter->drawText(posMidY.x(), posMidY.y(),
					  QString(QChar('y')));
	painter->drawText(posMidZ.x(), posMidZ.y(),
					  QString("TCD"));

	//	Interval ticks
	for (float x = 0.0; x < (float)m_data.size_x()-1.0; x += m_tickFreqX)
	{
		QPoint tickLabelPos = projectToScreenSpace(QVector3D(x,
															 0 + NUM_OFFSET,
															 0 + NUM_OFFSET));
		painter->drawText(tickLabelPos.x(), tickLabelPos.y(), QString("%1")
						  .arg(x));

	}

	for (float y = 0.0; y < (float)m_data.size_y()-1.0; y += m_tickFreqY)
	{
		QPoint tickLabelPos = projectToScreenSpace(QVector3D(0 + NUM_OFFSET,
															 y,
															 0 + NUM_OFFSET));
		painter->drawText(tickLabelPos.x(), tickLabelPos.y(), QString("%1")
						  .arg(y));

	}

	for (float z = 0.0; z < (float)m_dataMax; z += m_tickFreqZ)
	{
		QPoint tickLabelPos = projectToScreenSpace(QVector3D(0 + NUM_OFFSET,
															 0 + NUM_OFFSET,
															 z));
		painter->drawText(tickLabelPos.x(), tickLabelPos.y(), QString("%1")
						  .arg(z));

	}
}

///
/// \brief		Renders the X, Y and Z axis lines.  Additionally, can render
///				axis lines at the far edges of the data.
/// \since		25-09-2015
/// \author		Dean
///
void QtSurfacePlotWidget::renderAxes()
{
	assert(context() != nullptr);
	assert(m_basicShaderProgram.isLinked());

	//	Make sure the min/max is correct for Z
	recalculateMinMax();

	const QVector3D ORIGIN(0.0, 0.0, 0.0);

	const QVector3D MAX_X(m_data.size_x(), 0.0, 0.0);
	const QVector3D MAX_Y(0.0, m_data.size_y(), 0.0);
	const QVector3D MAX_Z(0.0, 0.0, m_dataMax);
	const QVector3D MIN_Z(0.0, 0.0, m_dataMin);

	BufferData buffer;

	//	X axis
	buffer.vertexArray.push_back(ORIGIN);
	buffer.vertexArray.push_back(MAX_X);
	buffer.colourArray.push_back(QVector3D(0.0, 0.0, 0.0));
	buffer.colourArray.push_back(QVector3D(0.0, 0.0, 0.0));

	//	Y axis
	buffer.vertexArray.push_back(ORIGIN);
	buffer.vertexArray.push_back(MAX_Y);
	buffer.colourArray.push_back(QVector3D(0.0, 0.0, 0.0));
	buffer.colourArray.push_back(QVector3D(0.0, 0.0, 0.0));

	//	Z+ axis
	if (m_dataMax > 0.0)
	{
		buffer.vertexArray.push_back(ORIGIN);
		buffer.vertexArray.push_back(MAX_Z);
		buffer.colourArray.push_back(QVector3D(0.0, 0.0, 0.0));
		buffer.colourArray.push_back(QVector3D(0.0, 0.0, 0.0));
	}

	//	Z- axis
	if (m_dataMin < 0.0)
	{
		buffer.vertexArray.push_back(ORIGIN);
		buffer.vertexArray.push_back(MIN_Z);
		buffer.colourArray.push_back(QVector3D(0.0, 0.0, 0.0));
		buffer.colourArray.push_back(QVector3D(0.0, 0.0, 0.0));
	}

	/*
	if (m_renderFarAxisLines)
	{
		//	Far axis lines (9 additional lines)
		//	#1
		buffer->vertexArray.push_back(MAX_Z);
		buffer->vertexArray.push_back(MAX_XZ);
		buffer->colourArray.push_back(QVector3D(0.0, 0.0, 0.0));
		buffer->colourArray.push_back(QVector3D(0.0, 0.0, 0.0));

		//	#2
		buffer->vertexArray.push_back(MAX_X);
		buffer->vertexArray.push_back(MAX_XZ);
		buffer->colourArray.push_back(QVector3D(0.0, 0.0, 0.0));
		buffer->colourArray.push_back(QVector3D(0.0, 0.0, 0.0));

		//	#3
		buffer->vertexArray.push_back(MAX_Y);
		buffer->vertexArray.push_back(MAX_YZ);
		buffer->colourArray.push_back(QVector3D(0.0, 0.0, 0.0));
		buffer->colourArray.push_back(QVector3D(0.0, 0.0, 0.0));

		//	#4
		buffer->vertexArray.push_back(MAX_Z);
		buffer->vertexArray.push_back(MAX_YZ);
		buffer->colourArray.push_back(QVector3D(0.0, 0.0, 0.0));
		buffer->colourArray.push_back(QVector3D(0.0, 0.0, 0.0));

		//	#5
		buffer->vertexArray.push_back(MAX_X);
		buffer->vertexArray.push_back(MAX_XY);
		buffer->colourArray.push_back(QVector3D(0.0, 0.0, 0.0));
		buffer->colourArray.push_back(QVector3D(0.0, 0.0, 0.0));

		//	#6
		buffer->vertexArray.push_back(MAX_Y);
		buffer->vertexArray.push_back(MAX_XY);
		buffer->colourArray.push_back(QVector3D(0.0, 0.0, 0.0));
		buffer->colourArray.push_back(QVector3D(0.0, 0.0, 0.0));

		//	#7
		buffer->vertexArray.push_back(MAX_XZ);
		buffer->vertexArray.push_back(MAX_XYZ);
		buffer->colourArray.push_back(QVector3D(0.0, 0.0, 0.0));
		buffer->colourArray.push_back(QVector3D(0.0, 0.0, 0.0));

		//	#8
		buffer->vertexArray.push_back(MAX_YZ);
		buffer->vertexArray.push_back(MAX_XYZ);
		buffer->colourArray.push_back(QVector3D(0.0, 0.0, 0.0));
		buffer->colourArray.push_back(QVector3D(0.0, 0.0, 0.0));

		//	#9
		buffer->vertexArray.push_back(MAX_XY);
		buffer->vertexArray.push_back(MAX_XYZ);
		buffer->colourArray.push_back(QVector3D(0.0, 0.0, 0.0));
		buffer->colourArray.push_back(QVector3D(0.0, 0.0, 0.0));
	}
	*/

	glEnable(GL_DEPTH_TEST);
	glLineWidth(m_axisThickness);

	//	Our matrices to be send to the GPU
	//QMatrix4x4 mMatrix;
	QMatrix4x4 vMatrix = m_currentCamera->GetViewMatrix();
	QMatrix4x4 mvMatrix = vMatrix * m_modelMatrix;
	QMatrix3x3 normalMatrix = mvMatrix.normalMatrix();

	//
	//QVector3D lightPos(5.0, 5.0, 5.0);

	//	Bind our shader program
	m_basicShaderProgram.bind();

	//	Pre-compute our matrices in various forms and send them as uniforms
	m_basicShaderProgram.setUniformValue("uModelViewMatrix", mvMatrix);
	m_basicShaderProgram.setUniformValue("uProjectionMatrix", m_projectionMatrix);
	m_basicShaderProgram.setUniformValue("uNormalMatrix", normalMatrix);

	//	Send the vertex array
	m_basicShaderProgram.setAttributeArray("vPosition", buffer.vertexArray.constData());
	m_basicShaderProgram.enableAttributeArray("vPosition");

	//	Send the colour array
	m_basicShaderProgram.setAttributeArray("vColor", buffer.colourArray.constData());
	m_basicShaderProgram.enableAttributeArray("vColor");

	//	If there is data to be rendered, render it...
	if (buffer.vertexArray.size() > 0)
		glDrawArrays(GL_LINES, 0, buffer.vertexArray.size());

	//	Clean up the shader program
	m_basicShaderProgram.disableAttributeArray("vPosition");
	m_basicShaderProgram.disableAttributeArray("vColor");
	m_basicShaderProgram.release();
}

///
/// \brief		Creates a custom ContextMenu for this widget
/// \since		22-09-2015
/// \author		Dean
///
void QtSurfacePlotWidget::constructContextMenu()
{
	cout << __PRETTY_FUNCTION__ << endl;

	m_contextMenu.contextMenu.reset(new QMenu);
}

///
/// \brief		Handles requests for the popup from the widget
/// \since		22-09-2015
/// \author		Dean
///
void QtSurfacePlotWidget::contextMenuEvent(QContextMenuEvent *event)
{
	cout << __PRETTY_FUNCTION__ << endl;

	assert(m_contextMenu.contextMenu != nullptr);

	//	Map to the actual position on the form, not relative to this control
	m_contextMenu.contextMenu->popup(QWidget::mapToGlobal(event->pos()));
}

///
/// \brief QtSurfacePlotWidget::SetData
/// \param data
/// \author		Dean
/// \since		27-08-2015
///
void QtSurfacePlotWidget::SetData(const Vector2f& data)
{
	m_data = data;

	auto centreX = m_data.size_x() / 2.0;
	auto centreY = m_data.size_y() / 2.0;

	cout << "Set data for surface plot." << endl;
	cout << "Centre of data: " << centreX << ", " << centreY << endl;
	m_currentCamera->LookAt(QVector3D(centreX,
									  0.0,
									  -centreY));

	update();
}

///
/// \brief QtSurfacePlotWidget::QtSurfacePlotWidget
/// \param parent
/// \since		25-08-2015
/// \author		Dean
///
QtSurfacePlotWidget::QtSurfacePlotWidget(QWidget *parent)
	: QGLWidget(QGLFormat(), parent)
{
	cout << __PRETTY_FUNCTION__ << endl;

	setAutoFillBackground(false);

	constructContextMenu();

	m_backgroundColor = Qt::white;

	//	Rotate the model so that the Z axis points upwards
	m_modelMatrix.rotate(-90.0, QVector3D(1, 0, 0));

	m_currentCamera = UPtrCamera(new QtCamera(QVector3D(10, 10, 10),
											QVector3D(2.5, 2.5, 2.5),
											"Arcball Camera"));
}

///
/// \brief QtSurfacePlotWidget::paintEvent
/// \since		25-08-2015
/// \author		Dean
///
void QtSurfacePlotWidget::paintEvent(QPaintEvent *)
{
	makeCurrent();

	render3dView();

	renderOverlay();
}

///
/// \brief QtSurfacePlotWidget::calculateSurfaceNormal
/// \param p1
/// \param p2
/// \param p3
/// \return
/// \since		24-09-2015
/// \author		Dean
///
QVector3D QtSurfacePlotWidget::calculateSurfaceNormal(const QVector3D& p1,
													  const QVector3D& p2,
													  const QVector3D& p3) const
{
	QVector3D v = p2 - p1;
	QVector3D w = p3 - p1;

	QVector3D n((v.y() * w.z()) - (v.z() * w.y()),
				(v.z() * w.x()) - (v.x() * w.z()),
				(v.x() * w.y()) - (v.y() * w.x()));

	return n.normalized();
}

///
/// \brief		Respond to the user scrolling the mouse
/// \param event
/// \since		25-09-2015
/// \author		Dean
///
void QtSurfacePlotWidget::wheelEvent(QWheelEvent *event)
{
	int delta = event->delta();

	if (event->orientation() == Qt::Vertical)
	{
		//emit OnMouseScroll(delta);
	}
}

///
/// \brief QtSurfacePlotWidget::render3dView
/// \since		25-08-2015
/// \author		Dean
///
void QtSurfacePlotWidget::render3dView()
{
	assert(context() != nullptr);

	//	We need to make sure things are correct for normalization
	recalculateMinMax();

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	QVector<QVector3D> vertexArray;
	QVector<QVector3D> colourArray;
	QVector<QVector3D> normalArray;

	vertexArray.reserve(4 * m_data.size_x()-1 * m_data.size_y()-1);
	colourArray.reserve(4 * m_data.size_x()-1 * m_data.size_y()-1);
	normalArray.reserve(4 * m_data.size_x()-1 * m_data.size_y()-1);

	for (auto x = 0; x < m_data.size_x()-1; ++x)
	{
		for (auto y = 0; y < m_data.size_y()-1; ++y)
		{
			//		v3-----v2
			//		| \		|
			//		|	\	|
			//		|	  \ |
			//		v0-----v1

			//	Function height at each vertex
			auto h0 = m_data.Get(x, y);
			auto h1 = m_data.Get(x+1, y);
			auto h2 = m_data.Get(x+1, y+1);
			auto h3 = m_data.Get(x, y+1);

			//	Actual position of vertex in 3D space
			QVector3D v0 = QVector3D((GLfloat)x,
									 (GLfloat)y,
									 h0);
			QVector3D v1 = QVector3D((GLfloat)x+1,
									 (GLfloat)y,
									 h1);
			QVector3D v2 = QVector3D((GLfloat)x+1,
									 (GLfloat)y+1,
									 h2);
			QVector3D v3 = QVector3D((GLfloat)x,
									 (GLfloat)y+1,
									 h3);

			//	For now we'll make do with surface normals on quads
			//	Calculate the surface normals for the triangles
			QVector3D n0 = calculateSurfaceNormal(v0, v1, v3);
			QVector3D n1 = calculateSurfaceNormal(v2, v1, v3);

			//	Average them for the quad
			QVector3D nA = ((n0 + n1) / 2.0).normalized();

			//	Colours for each vertex (default to green)
			QVector3D c0;//(0.0, 0.0, 0.1);
			QVector3D c1;//(0.0, 0.0, 0.1);
			QVector3D c2;//(0.0, 0.0, 0.1);
			QVector3D c3;//(0.0, 0.0, 0.1);

			//	If a colour ramp is available use it to calculate the actual
			//	colours
			if (m_colourRampWidget != nullptr)
			{
				//	Normalize the heights
				auto nh0 = normalize(h0);
				auto nh1 = normalize(h1);
				auto nh2 = normalize(h2);
				auto nh3 = normalize(h3);

				c0 = QColorToQVector3D(m_colourRampWidget->GetColorFromValue(nh0));
				c1 = QColorToQVector3D(m_colourRampWidget->GetColorFromValue(nh1));
				c2 = QColorToQVector3D(m_colourRampWidget->GetColorFromValue(nh2));
				c3 = QColorToQVector3D(m_colourRampWidget->GetColorFromValue(nh3));
			}

			//	Push the values to the streams
			vertexArray << v0 << v1 << v2 << v3;
			normalArray << nA << nA << nA << nA;
			colourArray << c0 << c1 << c2 << c3;
		}
	}

	//cout << "Vertex array size = " << vertexArray.size() << endl;

	/*
	for (auto it = colourArray.begin(); it != colourArray.end(); ++it)
	{
		cout << "(" << it->x() << ", " << it->y() << ", " << it->z() << ")";
		cout << endl;
	}
	*/

	glEnable(GL_DEPTH_TEST);

	//	Our matrices to be send to the GPU
	//QMatrix4x4 mMatrix;
	QMatrix4x4 vMatrix = m_currentCamera->GetViewMatrix();
	QMatrix4x4 mvMatrix = vMatrix * m_modelMatrix;
	QMatrix3x3 normalMatrix = mvMatrix.normalMatrix();

	//	Bind our shader program
	assert(m_shaderProgram.bind());

	//	Pre-compute our matrices in various forms and send them as uniforms
	m_shaderProgram.setUniformValue("uModelViewMatrix", mvMatrix);
	m_shaderProgram.setUniformValue("uProjectionMatrix", m_projectionMatrix);
	m_shaderProgram.setUniformValue("uNormalMatrix", normalMatrix);

	//	Send the vertex array
	m_shaderProgram.setAttributeArray("vPosition", vertexArray.constData());
	m_shaderProgram.enableAttributeArray("vPosition");

	//	Send the normal array
	m_shaderProgram.setAttributeArray("vNormal", normalArray.constData());
	m_shaderProgram.enableAttributeArray("vNormal");

	//	Send the colour array
	m_shaderProgram.setAttributeArray("vColor", colourArray.constData());
	m_shaderProgram.enableAttributeArray("vColor");

	glDrawArrays(GL_QUADS, 0, vertexArray.size());

	//	Clean up the shader program
	m_shaderProgram.disableAttributeArray("vPosition");
	m_shaderProgram.disableAttributeArray("vNormal");
	m_shaderProgram.disableAttributeArray("vColor");
	m_shaderProgram.release();

	renderAxes();
}

///
/// \brief		QtSurfacePlotWidget::renderOverlay
/// \since		25-08-2015
/// \author		Dean
///
void QtSurfacePlotWidget::renderOverlay()
{
	assert(context() != nullptr);

	QPainter painter(this);

	//	...
	painter.drawText(20,20, "Test");

	renderAxisLabels(&painter);

	painter.end();
}

///
/// \brief QtSurfacePlotWidget::sizeHint
/// \return
/// \since		25-08-2015
/// \author		Dean
///
QSize QtSurfacePlotWidget::sizeHint() const
{
	return QSize(640, 480);
}

///
/// \brief		Initializes the OpenGL context for this widget
/// \since		25-08-2015
/// \author		Dean
///
void QtSurfacePlotWidget::initializeGL()
{
	qglClearColor(m_backgroundColor);

	m_shaderProgram.addShaderFromSourceFile(QGLShader::Vertex,
											QString("C:/Users/4thFloor/Documents/Qt/qt_flexible-isosurfaces/QTFlexibleIsosurfaces_Modified/Shaders/simplegradientshading.vs.glsl"));
	m_shaderProgram.addShaderFromSourceFile(QGLShader::Fragment,
											QString("C:/Users/4thFloor/Documents/Qt/qt_flexible-isosurfaces/QTFlexibleIsosurfaces_Modified/Shaders/simplegradientshading.fs.glsl"));
	assert(m_shaderProgram.link());

	//	This is a simple program which contains no lighting properties, it can
	//	be used to render 'flat' objects such as wireframes or axes
	m_basicShaderProgram.addShaderFromSourceFile(QGLShader::Vertex,
												 QString(BASIC_SHADER_VS));
	m_basicShaderProgram.addShaderFromSourceFile(QGLShader::Fragment,
												 QString(BASIC_SHADER_FS));
	m_basicShaderProgram.link();
}

///
/// \brief		Handles resizing the OpenGL surface
/// \param w
/// \param h
/// \since		25-08-2015
/// \author		Dean
///
void QtSurfacePlotWidget::resizeGL(int w, int h)
{
	if (h == 0) h = 1;

	m_projectionMatrix.setToIdentity();
	m_projectionMatrix.perspective(60.0, (float)w / (float)h, 0.001, 1000);

	glViewport(0,0,w,h);
}

///
/// \brief		Takes a 3d point as used in the 3d render and projects to the
///				relative position in screen space
/// \param		pos
/// \return
/// \since		25-08-2015
/// \author		Dean
///
QPoint QtSurfacePlotWidget::projectToScreenSpace(const QVector3D &pos) const
{
	assert (m_currentCamera != nullptr);

	//	Adapted from:
	//	http://webglfactory.blogspot.co.uk/2011/05/how-to-convert-world-to-screen.html
	//	[14-07-2015]
	QMatrix4x4 viewProjectionMatrix = m_projectionMatrix * m_currentCamera->GetViewMatrix();

	//	Take into account the model has been rotated to Z-up
	QVector3D pos3d = viewProjectionMatrix * m_modelMatrix * pos;

	int winX = round(((pos3d.x() + 1.0) / 2.0) * (float)width());
	int winY = round(((1.0 - pos3d.y()) / 2.0) * (float)height());

	return QPoint(winX, winY);

}

///
/// \brief		Takes a 2d point as used in screen space and projects to the
///				relative position in 3d space (the z coordinate can be
///				specified)
/// \param		pos
/// \return
/// \since		25-08-2015
/// \author		Dean
///
QVector3D QtSurfacePlotWidget::projectTo3dSpace(const QPoint &pos,
												const float &z) const
{
	assert (m_currentCamera != nullptr);

	//	Adapted from:
	//	http://webglfactory.blogspot.co.uk/2011/05/how-to-convert-world-to-screen.html
	//	[14-07-2015]
	double x = 2.0 * pos.x() / ((float)width() - 1.0);
	double y = 2.0 * pos.y() / ((float)height() + 1.0);

	QMatrix4x4 viewProjectionInverse =
			(m_projectionMatrix * m_currentCamera->GetViewMatrix()).inverted();

	QVector3D pos3d = QVector3D(x, y, z);

	//	Take into account the model has been rotated to Z-up
	return viewProjectionInverse * m_modelMatrix.inverted() * pos3d;
}

///
/// \brief		Converts a QColor to an RGB QVector3D
/// \param		color
/// \return
/// \since		25-08-2015
/// \author		Dean
///
QVector3D QtSurfacePlotWidget::QColorToQVector3D(const QColor &color) const
{
	QVector3D result(color.redF(), color.greenF(), color.blueF());

	cout << result.x() << ", " << result.y() << ", " << result.z() << endl;

	return result;
}
