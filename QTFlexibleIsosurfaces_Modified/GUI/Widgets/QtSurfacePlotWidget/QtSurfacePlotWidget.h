#ifndef QT_SURFACE_PLOT_WIDGET_H
#define QT_SURFACE_PLOT_WIDGET_H

#include <Vector2.h>
#include "GUI/Widgets/QtCamera.h"
#include "GUI/Forms/QtOriginOffsetDialog.h"
#include "Globals/Globals.h"
#include "GUI/Widgets/QtColourRampWidget/QtColourRampWidget.h"
#include <memory>
#include <algorithm>
#include <utility>

#include <QGLWidget>
#include <QColor>
#include <QSize>
#include <QMatrix4x4>
#include <QVector3D>
#include <QPoint>
#include <QGLShaderProgram>
#include <QMenu>
#include <QAction>
#include <memory>

#ifdef __linux__
	#define SHADER_DIR "/home/dean/qcdvis/QTFlexibleIsosurfaces_Modified/Shaders/"
	#define BASIC_SHADER_VS "/home/dean/qcdvis/QTFlexibleIsosurfaces_Modified/Shaders/basicShader.vs.glsl"
	#define BASIC_SHADER_FS "/home/dean/qcdvis/QTFlexibleIsosurfaces_Modified/Shaders/basicShader.fs.glsl"
#else
    #define SHADER_DIR "C:/Users/4thFloor/Documents/Qt/QtFlexibleIsosurfaces/QTFlexibleIsosurfaces_Modified/Shaders/"
    #define BASIC_SHADER_VS "C:/Users/4thFloor/Documents/Qt/QtFlexibleIsosurfaces/QTFlexibleIsosurfaces_Modified/Shaders/basicShader.vs.glsl"
    #define BASIC_SHADER_FS "C:/Users/4thFloor/Documents/Qt/QtFlexibleIsosurfaces/QTFlexibleIsosurfaces_Modified/Shaders/basicShader.fs.glsl"
#endif

class QtSurfacePlotWidget : public QGLWidget
{
	private:
		struct BufferData
		{
				QVector<QVector3D> vertexArray;
				QVector<QVector3D> normalArray;
				QVector<QVector3D> colourArray;
				QVector<float> objectIdArray;
		};

		float m_axisThickness = 1.0;

		float m_dataMin = 0.0;
		float m_dataMax = 0.0;
	private:
		using UPtrQMenu = std::unique_ptr<QMenu>;
		using UPtrQAction = std::unique_ptr<QAction>;
		using UPtrCamera = std::unique_ptr<QtCamera>;
		using Vector2f = Storage::Vector2<float>;

		//	Group together pointers for the context menu
		struct ContextMenu
		{
				UPtrQMenu contextMenu = nullptr;

				//	Actions...
		} m_contextMenu;

		float m_tickFreqX = 1.0;
		float m_tickFreqY = 1.0;
		float m_tickFreqZ = 1.0;

		QGLShaderProgram m_basicShaderProgram;

		QtColourRampWidget* m_colourRampWidget = nullptr;

		Vector2f m_data;
		QColor m_backgroundColor;
		UPtrCamera m_currentCamera;

		float normalize(const float &unNormalized) const;


		void render3dView();
		void renderOverlay();

		void renderAxes();
		void renderAxisLabels(QPainter *painter);

		void constructContextMenu();

		virtual QSize sizeHint() const;
		virtual void initializeGL();
		virtual void resizeGL(int w, int h);
		virtual void wheelEvent(QWheelEvent *event);

		virtual void contextMenuEvent(QContextMenuEvent *event);

		QMatrix4x4 m_projectionMatrix;
		QMatrix4x4 m_modelMatrix;

		QVector3D QColorToQVector3D(const QColor &color) const;
		QVector3D projectTo3dSpace(const QPoint &pos, const float &z) const;
		QPoint projectToScreenSpace(const QVector3D &pos) const;

		QGLShaderProgram m_shaderProgram;

		void recalculateMinMax();

		void popupRequested();

		QVector3D calculateSurfaceNormal(const QVector3D& p1, const QVector3D& p2, const QVector3D &p3) const;
	public:
		void SetColourRampWidget(QtColourRampWidget* widget);

		explicit QtSurfacePlotWidget(QWidget *parent = 0);
		virtual void paintEvent(QPaintEvent *);

		void SetData(const Vector2f& data);
		Vector2f GetData() const { return m_data; }

	private slots:
		void showDialog();

	signals:
		//void OnMouseScroll(int delta);
};

#endif // QTSURFACEPLOTWIDGET_H
