#include "QtIsovalueSelectionDialog.h"
#include "ui_QtIsovalueSelectionDialog.h"

QtIsovalueSelectionDialog::QtIsovalueSelectionDialog(QWidget *parent) :
	QDialog(parent),
	ui(new Ui::IsovalueSelectionDialog)
{
	ui->setupUi(this);
}

///
/// \brief QtIsovalueSelectionDialog::QtIsovalueSelectionDialog
/// \param isoMin: the isovalue minima (unnormalised)
/// \param isoMax: the isovalue maxima (unnormalised)
/// \param currentValue: the current isovalue (unnormalised)
/// \param parent
///
QtIsovalueSelectionDialog::QtIsovalueSelectionDialog(const Real isoMin,
													 const Real isoMax,
													 const Real currentValue,
													 QWidget *parent):
	QtIsovalueSelectionDialog(parent)
{
	assert(isoMax >= isoMin);
	assert((currentValue >= isoMin) && (currentValue <= isoMax));

	m_isoMin = isoMin;
	m_isoMax = isoMax;

	ui->doubleSpinBoxActualIsovalue->setMinimum(isoMin);
	ui->doubleSpinBoxActualIsovalue->setMaximum(isoMax);

	ui->doubleSpinBoxActualIsovalue->setValue(currentValue);

	ui->widgetBookmark->SetCurrentIsovalue(currentValue);

	connect(ui->widgetBookmark, &QtIsovalueBookmarkWidget::OnBookmarkSelected,
			[&](const IsovalueBookmark &bookmark)
	{
		printf("User selected bookmark: %s.\n", bookmark.ToString().c_str());
		fflush(stdout);

		setUnNormalizedIsovalueSpinbox(bookmark.GetIsovalue());
		setNormalizedIsovalueSpinbox(normalize(bookmark.GetIsovalue()));
	});
}

QtIsovalueSelectionDialog::~QtIsovalueSelectionDialog()
{
	delete ui;
}

void QtIsovalueSelectionDialog::on_doubleSpinBoxNormalisedIsovalue_valueChanged(double arg1)
{
	setUnNormalizedIsovalueSpinbox(unNormalize(arg1));
	ui->widgetBookmark->SetCurrentIsovalue(unNormalize(arg1));
}

Real QtIsovalueSelectionDialog::GetUnNormalizedIsovalue() const
{
	return ui->doubleSpinBoxActualIsovalue->value();
}

Real QtIsovalueSelectionDialog::GetNormalizedIsovalue() const
{
	return ui->doubleSpinBoxNormalisedIsovalue->value();
}

void QtIsovalueSelectionDialog::on_doubleSpinBoxActualIsovalue_valueChanged(double arg1)
{
	setNormalizedIsovalueSpinbox(normalize(arg1));
	ui->widgetBookmark->SetCurrentIsovalue(arg1);
}

///
/// \brief		Takes an un-normalized isovalue and normalizes it
/// \param		unNormalized
/// \return		float: range 0..1
/// \author		Dean
/// \since		05-08-2015
///
Real QtIsovalueSelectionDialog::normalize(const float &unNormalized) const
{
	Real result = ((Real)unNormalized - m_isoMin) / (m_isoMax - m_isoMin);

	assert((result >= 0.0) && (result <= 1.0));

	return result;
}

///
/// \brief		Takes a normalized isovalue and un-normalizes it
/// \param		unNormalized: float(range 0..1)
/// \return
/// \author		Dean
/// \since		05-08-2015
///
Real QtIsovalueSelectionDialog::unNormalize(const float &normalized) const
{
	assert((normalized >= 0.0) && (normalized <= 1.0));

	return m_isoMin + (normalized * (m_isoMax - m_isoMin));
}

///
/// \brief		Sets the value in the unnormalised isovalue widget (disabling
///				and re-enabling signals in the process)
/// \param value
/// \author		Dean
/// \since		05-08-2015
///
void QtIsovalueSelectionDialog::setUnNormalizedIsovalueSpinbox(const double &value)
{
	assert((value >= m_isoMin) && (value <= m_isoMax));

	ui->doubleSpinBoxActualIsovalue->blockSignals(true);
	{
		ui->doubleSpinBoxActualIsovalue->setValue(value);
	}
	ui->doubleSpinBoxActualIsovalue->blockSignals(false);
}

///
/// \brief		Sets the value in the unnormalised isovalue widget (disabling
///				and re-enabling signals in the process)
/// \param value
/// \author		Dean
/// \since		05-08-2015
///
void QtIsovalueSelectionDialog::setNormalizedIsovalueSpinbox(const double &value)
{
	assert((value >= 0.0) && (value <= 1.0));

	ui->doubleSpinBoxNormalisedIsovalue->blockSignals(true);
	{
		ui->doubleSpinBoxNormalisedIsovalue->setValue(value);
	}
	ui->doubleSpinBoxNormalisedIsovalue->blockSignals(false);
}
