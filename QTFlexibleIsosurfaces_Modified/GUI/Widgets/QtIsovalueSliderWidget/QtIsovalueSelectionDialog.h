#ifndef QT_ISOVALUE_SELECTION_DIALOG_H
#define QT_ISOVALUE_SELECTION_DIALOG_H

#include <QDialog>
#include "Globals/TypeDefines.h"
#include "DataExploration/IsovalueBookmark.h"

namespace Ui {
	class IsovalueSelectionDialog;
}

class QtIsovalueSelectionDialog : public QDialog
{
		Q_OBJECT

	public:
		explicit QtIsovalueSelectionDialog(QWidget *parent = 0);
		QtIsovalueSelectionDialog(const Real isoMin,
								const Real isoMax,
								const Real currentValue,
								QWidget *parent = 0);
		~QtIsovalueSelectionDialog();

		Real GetUnNormalizedIsovalue() const;
		Real GetNormalizedIsovalue() const;

		Real normalize(const float &unNormalized) const;
		Real unNormalize(const float &normalized) const;
	private slots:
		void on_doubleSpinBoxNormalisedIsovalue_valueChanged(double arg1);

		void on_doubleSpinBoxActualIsovalue_valueChanged(double arg1);

		void setUnNormalizedIsovalueSpinbox(const double &value);
		void setNormalizedIsovalueSpinbox(const double &value);
	private:
		Ui::IsovalueSelectionDialog *ui;

		Real m_isoMin;
		Real m_isoMax;
};

#endif // QT_ISOVALUE_SELECTION_DIALOG_H
