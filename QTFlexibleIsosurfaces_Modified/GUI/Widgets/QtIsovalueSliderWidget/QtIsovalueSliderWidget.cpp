#include "QtIsovalueSliderWidget.h"
#include "ui_QtIsovalueSliderWidget.h"

#include <iostream>

#include "../Globals/Functions.h"

#ifdef _MSC_VER
#define __PRETTY_FUNCTION__ __FUNCSIG__
#endif

using std::cout;
using std::endl;

///
/// \brief		Updates the bookmark list, triggering a repaint of the widget
/// \param bookmarkList
/// \since		01-09-2015
/// \author		Dean
///
void QtIsovalueSliderWidget::SetBookmarkList(const IsovalueBookmarkList& bookmarkList)
{
	m_bookmarkList = bookmarkList;

	update();
}

///
/// \brief		QtIsovalueSliderWidget::paintEvent
/// \since		01-09-2015
/// \author		Dean
///
void QtIsovalueSliderWidget::paintEvent(QPaintEvent*)
{
	if (m_showBookmarks)
	{
		QPainter painter(this);
		painter.save();

		painter.setPen(Qt::black);

		//	Iterate through the bookmarks
		for (size_t i = 0; i < m_bookmarkList.GetBookmarkCount(); ++i)
		{
			auto bookmark = m_bookmarkList[i];

			//	Generate the outline for the marker and draw it
			QtMarkerOutline marker = generateMarkerOutline(bookmark.GetIsovalue());
			painter.drawPolygon(marker);

			//	Fill interior
			QPainterPath path;
			path.addPolygon(marker);
			painter.fillPath(path, QBrush(palette().color(QPalette::Shadow)));
		}

		painter.restore();
	}
}

///
/// \brief		Generate a arrow shaped polygon for marking the position of
///				bookmarks
/// \param value
/// \return
/// \since		01-09-2015
/// \author		Dean
///
QtIsovalueSliderWidget::QtMarkerOutline QtIsovalueSliderWidget::generateMarkerOutline(const float& value) const
{
	QtIsovalueSliderWidget::QtMarkerOutline result;

	const float X_POS = ui->verticalSliderIsovalue->x()+ 25.0f;
	const float Y_POS = height() - (height() * normalize(m_dataMin, m_dataMax, value));
	const float WIDTH = 10.0f;
	const float HEIGHT = 8.0f;

	const QPointF origin(X_POS, Y_POS);

	const QPointF topLeftVertex(X_POS + (WIDTH * 0.25), (Y_POS - (HEIGHT / 2.0)));
	const QPointF topRightVertex(X_POS + WIDTH, (Y_POS - (HEIGHT / 2.0)));

	const QPointF bottomRightVertex(X_POS + WIDTH, (Y_POS + (HEIGHT / 2.0)));
	const QPointF bottomLeftVertex(X_POS + (WIDTH * 0.25), (Y_POS + (HEIGHT / 2.0)));

	result << origin << topLeftVertex << topRightVertex;
	result << bottomRightVertex << bottomLeftVertex << origin;

	return result;
}

///
/// \brief		Shows the isovalue selection dialog for this widget
/// \author		Dean
/// \since		05-08-2015
///
void QtIsovalueSliderWidget::ShowIsovalueSelectionDialog()
{
	QtIsovalueSelectionDialog test(m_dataMin, m_dataMax, m_isovalue, this);
	if (test.exec() == QDialog::Accepted)
	{
		printf("User accepted changes.\n");
		fflush(stdout);

		//	Update with the new isovalue (will trigger an update in all
		//	child windows)
		SetIsovalue(test.GetUnNormalizedIsovalue());
	}
}

///
/// \brief		Creates a set of actions and context menu for this widget
/// \author		Dean
/// \since		05-08-2015
///
void QtIsovalueSliderWidget::setupContextMenu()
{
	try
	{
		//	Create the menu
		m_contextMenu = new QMenu(this);

		//	Set that we are to use a custom context menu
		//	and trigger it at the desired position via a lambda expression
		setContextMenuPolicy(Qt::CustomContextMenu);
		connect(this, &QWidget::customContextMenuRequested,
				[&](const QPoint& pos)
		{
			assert(m_contextMenu != nullptr);

			//	Map the position to screen space and create the popup
			m_contextMenu->popup(mapToGlobal(pos));
		});

		//	Create the actions
		m_actionSetIsovalue = new QAction("Set &Isovalue...", m_contextMenu);

		//	Add actions to menu
		m_contextMenu->addActions(QList<QAction*>({m_actionSetIsovalue}));

		//	Respond to actions
		assert(m_actionSetIsovalue != nullptr);
		connect(m_actionSetIsovalue, &QAction::triggered,
				[&]() { ShowIsovalueSelectionDialog(); });
	}
	catch (exception &ex)
	{
		//	We may get a bad_alloc if we are using a LOT of memory (unlikely)
		fprintf(stderr, "Exception!  %s in func: %s"
						" (file: %s, line: %ld).\n",
				ex.what(), __func__, __FILE__, __LINE__);
		fflush(stderr);
	}
}

float QtIsovalueSliderWidget::GetMinimumValue() const
{
	return m_dataMin;
}

float QtIsovalueSliderWidget::GetMaximumValue() const
{
	return m_dataMax;
}


void QtIsovalueSliderWidget::SetIsovalueRange(const float &min, const float &max)
{
	if (min > max)
	{
		fprintf(stderr, "Warning!  Min value exceeds maximum in function: %s"
						" (file: %s, line: %ld).\n", __func__, __FILE__, __LINE__);
		fflush(stderr);
	}

	m_dataMin = min;
	m_dataMax = max;

	updateMinMaxIsovalueLabels();

	update();
}

void QtIsovalueSliderWidget::updateMinMaxIsovalueLabels()
{
	QString minValue = "Min: \n" + QString::number(m_dataMin, 'f', LABEL_PRECISION);
	ui->labelMinimum->setText(minValue);

	QString maxValue = "Max: \n" + QString::number(m_dataMax, 'f', LABEL_PRECISION);
	ui->labelMaximum->setText(maxValue);
}

///
/// \brief		Allows the Isovalue to be set using a real world value.  If the
///				value exceeds the min/max of the slider it will be clamped in
///				to range
/// \param value
/// \author		Dean
/// \since		16/07-2015
///
void QtIsovalueSliderWidget::SetIsovalue(const float &value)
{
	float nValue;

	//	The global isovalue could exceed the local min/max; so we test
	//	and clamp as necessary
	if (value >= m_dataMax)
	{
		nValue = 1.0;
	}
	else if (value <= m_dataMin)
	{
		nValue = 0.0;
	}
	else
	{
		//	If not, we are in the local range
		//	Normalise the actual value to fit on this slider
		nValue = normalize(m_dataMin, m_dataMax, value);

		//	Incoming value could exceed the range for this slider
		//	so we clamp the values
		if (nValue < 0.0)
			nValue = 0.0;
		else if (nValue > 1.0)
			nValue = 1.0;
	}

	//	Set using the normalised value
	SetNormalizedIsovalue(nValue);
}

void QtIsovalueSliderWidget::SetNormalizedIsovalue(const float &value)
{
	if ((value >= 0.0) && (value <= 1.0))
	{
		ui->verticalSliderIsovalue->setValue(10000 * value);
	}
	updateCurrentIsovalueLabel();
}

///
/// \brief ContourTreeWindow::updateIsovalueSliderLabels
/// \since      09-01-2015
/// \author     Dean
/// \author		Dean: moved to separate widget class [03-07-2015]
///
void QtIsovalueSliderWidget::updateCurrentIsovalueLabel()
{
	//printf("Updating slider labels.\n");
	//fflush(stdout);

	QString currentValue = "Current: \n"
						   + QString::number(m_isovalue,'f',LABEL_PRECISION);
	ui->labelCurrent->setText(currentValue);
}

QtIsovalueSliderWidget::QtIsovalueSliderWidget(QWidget *parent) :
	QWidget(parent),
	ui(new Ui::QtIsovalueSliderWidget)
{
	ui->setupUi(this);

	m_dataMin = INITIAL_MIN;
	m_dataMax = INITIAL_MAX;

	m_showBookmarks = true;

	setupContextMenu();

	updateMinMaxIsovalueLabels();
	updateCurrentIsovalueLabel();
}

QtIsovalueSliderWidget::~QtIsovalueSliderWidget()
{
	cout << __PRETTY_FUNCTION__ << endl;

	this->disconnect();
	//ui->verticalSliderIsovalue->disconnect();
	//ui->
	//	Delete menu actions
	delete m_actionSetIsovalue;

	//	Delete the context menu
	delete m_contextMenu;

	delete ui;
}

void QtIsovalueSliderWidget::updateIsovalues()
{
	int isoSliderValue = ui->verticalSliderIsovalue->value();

	//  Calculate the position of the slider based upon
	//  the range of the dataset
	m_isovalue = m_dataMin + (isoSliderValue * (m_dataMax - m_dataMin)) / 10000;
	m_normalizedIsovalue = isoSliderValue / 10000.0;
}

void QtIsovalueSliderWidget::on_verticalSliderIsovalue_valueChanged(int)
{
	//	Update the isovalue stored
	updateIsovalues();

	//	Update the label
	updateCurrentIsovalueLabel();

	emit OnIsovalueChanged();
}
