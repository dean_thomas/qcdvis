#ifndef QTISOVALUESLIDERWIDGET_H
#define QTISOVALUESLIDERWIDGET_H

#include "../Globals/Globals.h"
#include "QtIsovalueSelectionDialog.h"
#include "../DataExploration/IsovalueBookmarkList.h"

#include <QWidget>
#include <QMenu>
#include <QAction>
#include <QPointF>
#include <QVector>
#include <QLineF>
#include <QPainter>

#include <exception>

#define INITIAL_MAX +0.00001
#define INITIAL_MIN -0.00001


using std::exception;

namespace Ui {
	class QtIsovalueSliderWidget;
}

class QtIsovalueSliderWidget : public QWidget
{
		Q_OBJECT
private:
		using QtMarkerOutline = QPolygonF;

		const unsigned long LABEL_PRECISION = 4;

		void updateMinMaxIsovalueLabels();
		void updateCurrentIsovalueLabel();
		void updateIsovalues();
		float m_dataMin;
		float m_dataMax;
		float m_normalizedIsovalue = 0.0f;
		float m_isovalue = 0.0f;

		bool m_showBookmarks;

		void setupContextMenu();

		IsovalueBookmarkList m_bookmarkList;

		QtMarkerOutline generateMarkerOutline(const float& value) const;

		virtual void paintEvent(QPaintEvent*);
	public:
		void ShowIsovalueSelectionDialog();

		explicit QtIsovalueSliderWidget(QWidget *parent = 0);
		void SetIsovalueRange(const float &min, const float &max);

		void SetNormalizedIsovalue(const float &value);
		void SetIsovalue(const float &value);

		float GetNormalizedIsovalue() const { return m_normalizedIsovalue; }
		float GetIsovalue() const { return m_isovalue; }

		float GetMinimumValue() const;
		float GetMaximumValue() const;

		void SetBookmarkList(const IsovalueBookmarkList& bookmarkList);

		~QtIsovalueSliderWidget();


	private slots:
		void on_verticalSliderIsovalue_valueChanged(int);

	private:
		Ui::QtIsovalueSliderWidget *ui;
		QMenu *m_contextMenu = nullptr;
		QAction *m_actionSetIsovalue = nullptr;
	signals:
		void OnIsovalueChanged();
};

#endif // QTISOVALUESLIDERWIDGET_H
