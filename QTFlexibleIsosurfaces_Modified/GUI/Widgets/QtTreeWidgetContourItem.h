#ifndef QTTREEWIDGETCONTOURITEM_H
#define QTTREEWIDGETCONTOURITEM_H

#include <QTreeWidgetItem>
#include <QIcon>
#include <QPixmap>
#include <QPainter>
#include <QSettings>

#include "AbstractColourMappedWidget.h"
#include "Contour/Storage/Contour.h"

//	Key to be used to store properties of various items
#define KEY_MESH "mesh"
#define KEY_ISOVALUE "isovalue"
#define KEY_BOUNDING_BOX "boundingBox"
#define KEY_CONTOUR_TREE "contourTree"
#define KEY_SUPERARC "superarc"

//	Branches in the view - default states
#define CONTOURS_EXPANDED_BY_DEFAULT true
#define MESH_SUBTREE_EXPANDED_BY_DEFAULT false
#define ISOVALUE_SUBTREE_EXPANDED_BY_DEFAULT false
#define BOUNDING_BOX_SUBTREE_EXPANDED_BY_DEFAULT false
#define CONTOUR_TREE_SUBTREE_EXPANDED_BY_DEFAULT false
#define SUPERARC_SUBTREE_EXPANDED_BY_DEFAULT false

class QtTreeWidgetContourItem
		: public QTreeWidgetItem//,
		//public AbstractColourMappedWidget
{
		//Q_OBJECT
	private:
		QSettings *m_state;

		Contour *m_contour;

		QTreeWidgetItem *m_parent;
			QTreeWidgetItem *m_meshRootItem;
			QTreeWidgetItem *m_isovalueRootItem;
			QTreeWidgetItem *m_boundingBoxRootItem;
			QTreeWidgetItem *m_contourTreeRootItem;
				QTreeWidgetItem *m_superarcRootItem;
		void addGeometryProperties();
		void addMeshProperties();
		void addIsovalueProperties();
		void addContourTreeProperties();
		void addSuperarcProperties(QTreeWidgetItem *rootItem);
		void addBoundingBoxProperties();

		void saveState();
	public:
		explicit QtTreeWidgetContourItem(QTreeWidgetItem *parent,
										 Contour *contour,
										 QIcon icon);
		virtual	~QtTreeWidgetContourItem();

		Contour *GetGeneratingContour() const { return m_contour; }
	signals:

	public slots:
		//void update();
};

#endif // QTTREEWIDGETCONTOURITEM_H
