#include "QtVolumeSelectionTreeView.h"
#include <QDebug>
#include <iostream>
#include "../Globals/QtOstream.h"
#include "../Globals/QtFunctions.h"
#include <QFileInfo>
#include <QDir>
#include <QMenu>
#include <exception>

using namespace std;

//Q_DECLARE_METATYPE(HeightField3D)
//Q_DECLARE_METATYPE(HeightField4D)

void QtVolumeSelectionTreeView::ClearAll()
{
	m_dataModel->clear();
}

bool QtVolumeSelectionTreeView::IsLockedToGlobalView(HeightField3D* const heightfield3) const
{
	//	At present heightfields can only be selected at the field variable
	//	scope.  Until each configuration can have it's own state information
	//	we will use that...
	auto fieldRootItem = findFieldVariableRoot(heightfield3);
	assert(fieldRootItem != nullptr);

	return (fieldRootItem->checkState() == Qt::Checked);
}

///
/// \brief		Finds the ensemble root entry for the specified heightfield
///				in the hierarchy
/// \param currentSlice
/// \return
/// \since		30-03-2016
/// \author		Dean
///
QStandardItem* QtVolumeSelectionTreeView::findEnsembleRoot(HeightField3D* const heightfield3) const
{
	assert(heightfield3 != nullptr);

	//	Find parent node
	auto rootNode = m_dataModel->invisibleRootItem();
	assert(rootNode != nullptr);

	//	Get the name 'key' we are looking for
	auto ensembleName = QString::fromStdString(heightfield3->GetEnsembleName());

	//	Find the intended child-node (or nullptr if it isn't there)
	return findNode(rootNode, ensembleName);
}

///
/// \brief		Finds the root entry for the configuration of the specified
///				heightfield in the hierarchy
/// \param currentSlice
/// \return
/// \since		30-03-2016
/// \author		Dean
///
QStandardItem* QtVolumeSelectionTreeView::findConfigurationRoot(HeightField3D* const heightfield3) const
{
	assert(heightfield3 != nullptr);

	//	Find parent node
	auto ensembleRoot = findEnsembleRoot(heightfield3);
	assert(ensembleRoot != nullptr);

	//	Get the name 'key' we are looking for
	auto configurationName = QString::fromStdString(heightfield3->GetConfigurationName());

	//	Find the intended child-node (or nullptr if it isn't there)
	return findNode(ensembleRoot, configurationName);
}

///
/// \brief		Finds the root entry for the number of cools of the specified
///				heightfield in the hierarchy
/// \param currentSlice
/// \return
/// \since		30-03-2016
/// \author		Dean
///
QStandardItem* QtVolumeSelectionTreeView::findCoolingRoot(HeightField3D* const heightfield3) const
{
	assert(heightfield3 != nullptr);

	//	Find parent node
	auto configurationRoot = findConfigurationRoot(heightfield3);
	assert(configurationRoot != nullptr);

	//	Get the name 'key' we are looking for
	auto coolName = QString("cool %1").arg(heightfield3->GetCoolingSlice());

	//	Find the intended child-node (or nullptr if it isn't there)
	return findNode(configurationRoot, coolName);
}

///
/// \brief		Finds the root entry for the field variable (toplogical
///				charge density etc.) of the heightfield in the hierarchy
/// \param currentSlice
/// \return
/// \since		30-03-2016
/// \author		Dean
///
QStandardItem* QtVolumeSelectionTreeView::findFieldVariableRoot(HeightField3D* const heightfield3) const
{
	assert(heightfield3 != nullptr);

	//	Find parent node
	auto coolingRoot = findCoolingRoot(heightfield3);
	assert(coolingRoot != nullptr);

	//	Get the name 'key' we are looking for
	auto fieldName = QString::fromStdString(heightfield3->GetFieldName());

	//	Find the intended child-node (or nullptr if it isn't there)
	return findNode(coolingRoot, fieldName);
}

///
/// \brief		Finds the root entry for slices similar to that of the
///				heightfield in the hierarchy
/// \param currentSlice
/// \return
/// \since		30-03-2016
/// \author		Dean
///
QStandardItem* QtVolumeSelectionTreeView::findSliceRoot(HeightField3D* const heightfield3) const
{
	assert(heightfield3 != nullptr);

	//	Find parent node
	auto fieldRoot = findFieldVariableRoot(heightfield3);
	assert(fieldRoot != nullptr);

	//	Get the name 'key' we are looking for
	//	HACK: conversion from x -> YZT etc for labels
	QString sliceName;
	auto varName = heightfield3->GetFixedVariableName();
	if ((varName == "t") || (varName == "time"))
		sliceName = QString::fromStdString("XYZ");
	else if (varName == "z")
		sliceName = QString::fromStdString("XYT");
	else if (varName == "y")
		sliceName = QString::fromStdString("XZT");
	else if (varName == "x")
		sliceName = QString::fromStdString("YZT");
	else
	{
		cerr << "Unknown slice name '" << varName << "'." << endl;
		assert(false);
	}

	//	Find the intended child-node (or nullptr if it isn't there)
	return findNode(fieldRoot, sliceName);
}

///
/// \brief		Finds the actual entry for the heightfield in the hierarchy
/// \param currentSlice
/// \return
/// \since		30-03-2016
/// \author		Dean
///
QStandardItem* QtVolumeSelectionTreeView::findSliceItem(HeightField3D* const heightfield3) const
{
	assert(heightfield3 != nullptr);

	//	Find parent node
	auto sliceRoot = findSliceRoot(heightfield3);
	assert(sliceRoot != nullptr);

	//	Make sure numbers are padded with leading zeroes
	auto formatted_number = QString::number(heightfield3->GetFixedVariableValue()).rightJustified(2, '0');
	cout << "Looking for key: " << heightfield3->GetFixedVariableName() << endl;

	//	Get the name 'key' we are looking for
	auto currentSliceLabel = QString("%1 = %2")
			.arg(QString::fromStdString(heightfield3->GetFixedVariableName()))
			.arg(formatted_number);

	//	Find the intended child-node (or nullptr if it isn't there)
	return findNode(sliceRoot, currentSliceLabel);
}

QVector<QFileInfo> QtVolumeSelectionTreeView::GetLoadedFiles() const
{
	QVector<QFileInfo> result;

	std::function<void(QStandardItem*)> process_node = [&](QStandardItem* node)
	{
		//	We only need a list of actual nodes attached to heightfields
		auto heightfield = node->data(ROLE_PATH_TO_HEIGHTFIELD_FILE).toString();
		if (!heightfield.isNull())
		{
			result.append(QFileInfo(heightfield));
		}

		//	Do we already have a label constructed?
		if (node->hasChildren())
		{
			for (auto i = 0; i < node->rowCount(); ++i)
			{
				auto child = node->child(i);
				process_node(child);
			}
		}
	};

	//	Get a list of heightfield nodes
	process_node(m_dataModel->invisibleRootItem());

	return result;
}

QStandardItem* QtVolumeSelectionTreeView::GetPrevCoolingSlice(HeightField3D* const currentSlice) const
{
	assert(currentSlice != nullptr);
	assert(currentSlice->GetCoolingSlice() > 0);

	//	Find the next slice
	auto slice = findSlice(currentSlice->GetEnsembleName(),
						   currentSlice->GetConfigurationName(),
						   currentSlice->GetFieldName(),
						   currentSlice->GetCoolingSlice() - 1,
						   currentSlice->GetFixedVariableName(),
						   currentSlice->GetFixedVariableValue());

	//	Wasn't able to locate the previous slice
	if (slice == nullptr) return nullptr;

	//	Retreive the hegihtfield data
	auto heightfield_data = slice->data(ROLE_HEIGHT_FIELD_3);
	if (!heightfield_data.isNull())
	{
		return slice;
	}
	return nullptr;
}

QStandardItem* QtVolumeSelectionTreeView::GetNextCoolingSlice(HeightField3D* const currentSlice) const
{
	assert(currentSlice != nullptr);

	//	Find the next slice
	auto slice = findSlice(currentSlice->GetEnsembleName(),
						   currentSlice->GetConfigurationName(),
						   currentSlice->GetFieldName(),
						   currentSlice->GetCoolingSlice() + 1,
						   currentSlice->GetFixedVariableName(),
						   currentSlice->GetFixedVariableValue());

	//	Wasn't able to locate the next slice
	if (slice == nullptr) return nullptr;

	//	Retreive the hegihtfield data
	auto heightfield_data = slice->data(ROLE_HEIGHT_FIELD_3);
	if (!heightfield_data.isNull())
	{
		return slice;
	}
	return nullptr;
}

QVector<QStandardItem*> get_all_nodes(QStandardItem* root)
{
	QVector<QStandardItem*> result;
	QVector<QStandardItem*> partial_result;



	for (auto& x : partial_result)
	{
		result.append(x);
	}

	return result;
}

QStandardItem* QtVolumeSelectionTreeView::findSlice(const std::string& ensemble,
													const std::string& configuration,
													const std::string& field,
													const size_t cools,
													const std::string fixed_variable_name,
													const size_t fixed_variable_value) const
{
	QVector<QStandardItem*> nodes;

	std::function<void(QStandardItem*)> process_node = [&](QStandardItem* node)
	{
		//	We only need a list of actual nodes attached to heightfields
		auto heightfield = node->data(ROLE_HEIGHT_FIELD_3);
		if (!heightfield.isNull())
		{
			nodes.append(node);
		}

		//	Do we already have a label constructed?
		if (node->hasChildren())
		{
			for (auto i = 0; i < node->rowCount(); ++i)
			{
				auto child = node->child(i);
				process_node(child);
			}
		}
	};

	//	Get a list of heightfield nodes
	process_node(m_dataModel->invisibleRootItem());

	for (auto& heightfield_node : nodes)
	{
		//	Retreive the hegihtfield data
		auto heightfield_data = heightfield_node->data(ROLE_HEIGHT_FIELD_3);
		if (!heightfield_data.isNull())
		{
			auto heightfield = toPtr<HeightField3D>(heightfield_data);

			if ((heightfield->GetEnsembleName() == ensemble)
					&& (heightfield->GetConfigurationName() == configuration)
					&& (heightfield->GetFieldName() == field)
					&& (heightfield->GetCoolingSlice() == cools)
					&& (heightfield->GetFixedVariableName() ==  fixed_variable_name)
					&& (heightfield->GetFixedVariableValue() ==  fixed_variable_value))
				return heightfield_node;
		}
	}
	return nullptr;
}

///
/// \brief		Returns the next slice of data in the 3D slice branch of the
///				tree
/// \param currentSlice
/// \return
/// \since		29-03-2016
/// \author		Dean
///
QStandardItem* QtVolumeSelectionTreeView::GetNextSlice(HeightField3D* const currentSlice) const
{
	assert(currentSlice != nullptr);

	//	Find out how many items exist at the same point in the hierarchy
	//	as the current slice
	auto sliceRoot = findSliceRoot(currentSlice);
	assert(sliceRoot != nullptr);

	//	FInd out how many slices are present, if only one it must be this so return nullptr
	auto sliceCount = sliceRoot->rowCount();
	cout << "Number of rows in root: " << sliceCount << endl;
	if (sliceCount == 1) return nullptr;

	//	Lookup the index of the current slice within its parent
	auto currentSliceItem = findSliceItem(currentSlice);
	assert(currentSliceItem != nullptr);

	auto currentSliceIndex = currentSliceItem->index().row();
	cout << "Current item has index: " << currentSliceIndex << endl;

	//	Get the next item in the list, or if we are at the end, loop back to
	//	the start
	QStandardItem* nextSliceItem = nullptr;
	if (currentSliceIndex < (sliceCount-1))
	{
		//	Get the next item in the list
		nextSliceItem = sliceRoot->child(currentSliceIndex + 1);
	}
	else
	{
		//	Get the first item in the list
		nextSliceItem = sliceRoot->child(0);
	}

	//	Now that the data has been tracked down we can convert to
	//	a heightfield and return it
	assert(nextSliceItem != nullptr);
	auto data = nextSliceItem->data(ROLE_HEIGHT_FIELD_3);

	assert(!data.isNull());
	auto nextHeightfield3 = toPtr<HeightField3D>(data);

	assert(nextHeightfield3 != nullptr);
	cout << "Next slice is: " << nextHeightfield3->GetFixedVariableName()
		 << " = " << nextHeightfield3->GetFixedVariableValue() << endl;

	return nextSliceItem;
}

///
/// \brief		Returns the prev slice of data in the 3D slice branch of the
///				tree
/// \param currentSlice
/// \return
/// \since		30-03-2016
/// \author		Dean
///
QStandardItem* QtVolumeSelectionTreeView::GetPrevSlice(HeightField3D* const currentSlice) const
{
	assert(currentSlice != nullptr);

	//	Find out how many items exist at the same point in the hierarchy
	//	as the current slice
	auto sliceRoot = findSliceRoot(currentSlice);
	assert(sliceRoot != nullptr);

	//	FInd out how many slices are present, if only one it must be this so return nullptr
	auto sliceCount = sliceRoot->rowCount();
	cout << "Number of rows in root: " << sliceCount << endl;
	if (sliceCount == 1) return nullptr;

	//	Lookup the index of the current slice within its parent
	auto currentSliceItem = findSliceItem(currentSlice);
	auto currentSliceIndex = currentSliceItem->index().row();
	cout << "Current item has index: " << currentSliceIndex << endl;

	//	Get the prev item in the list, or if we are at the start, loop back to
	//	the end
	QStandardItem* prevSliceItem = nullptr;
	if (currentSliceIndex > 0)
	{
		//	Get the previous item in the list
		prevSliceItem = sliceRoot->child(currentSliceIndex - 1);
	}
	else
	{
		//	Get the last item in the list
		prevSliceItem = sliceRoot->child(sliceCount - 1);
	}

	//	Now that the data has been tracked down we can convert to
	//	a heightfield and return it
	assert(prevSliceItem != nullptr);
	auto data = prevSliceItem->data(ROLE_HEIGHT_FIELD_3);

	assert(!data.isNull());
	auto prevHeightfield3 = toPtr<HeightField3D>(data);

	assert(prevHeightfield3 != nullptr);
	cout << "Previous slice is: " << prevHeightfield3->GetFixedVariableName()
		 << " = " << prevHeightfield3->GetFixedVariableValue() << endl;

	return prevSliceItem;
}

///
/// \brief		QtVolumeSelectionTreeView::generateDefaultColourTable
/// \return
/// \since		08-12-2015
/// \author		Dean
///
QtVolumeSelectionTreeView::ColourTable QtVolumeSelectionTreeView::generateDefaultColourTable() const
{
	ColourTable result = { Qt::red, Qt::blue, Qt::black };

	return result;
}

///
/// \brief QtVolumeSelectionTreeView::getNextColour
/// \return
/// \since		08-12-2015
/// \author		Dean
///
QColor QtVolumeSelectionTreeView::getNextDefaultColour()
{
	//	Get the current colour
	auto result = m_defaultColourTable[m_defaultColourIndex];

	//	Increment
	++m_defaultColourIndex;

	//	Roll back to beginning
	if (m_defaultColourIndex >= m_defaultColourTable.size())
		m_defaultColourIndex = 0;

	return result;
}

///
/// \brief QtVolumeSelectionTreeView::QtVolumeSelectionTreeView
/// \param parent
/// \since		03-11-2015
/// \author		Dean
///
QtVolumeSelectionTreeView::QtVolumeSelectionTreeView(QWidget *parent)
	: QTreeView(parent)
{
	const auto MIN_HEIGHTFIELDS_FOR_JCN = 2ul;
	const auto MAX_HEIGHTFIELDS_FOR_JCN = 2ul;

	m_dataModel = generateInitialModel();
	assert (m_dataModel != nullptr);

	//	Create a colour table for defaults
	m_defaultColourTable = generateDefaultColourTable();

	setHeaderHidden(true);
	setSelectionMode(MultiSelection);
	setContextMenuPolicy(Qt::CustomContextMenu);

	//	Handle double clicked events
	connect(this, &QtVolumeSelectionTreeView::doubleClicked,
			[&](const QModelIndex& modelIndex)
	{
		assert(m_dataModel != nullptr);

		auto clickedItem
				= dynamic_cast<QtVolumeStandardItem*>(m_dataModel->itemFromIndex(modelIndex));
		assert(clickedItem != nullptr);

		auto data = clickedItem->data(ROLE_HEIGHT_FIELD_3);
		if (!data.isNull())
		{
			HeightField3D* clickedHeightField3 = toPtr<HeightField3D>(data);

			if (clickedHeightField3 != nullptr)
			{
				qDebug() << QString::fromStdString(
								clickedHeightField3->GetConfigurationName());

				emit OnDoubleClickedHeightField3(clickedItem);
			}
		}
	});

	//	Handle right click events
	connect(this, &QtVolumeSelectionTreeView::customContextMenuRequested,
			[&](const QPoint &pos)
	{
		assert(m_dataModel != nullptr);

		//	Function to iterate over selected items and only return those asscoiated with a heightfield 3D
		auto get_selected_heightfield3_func = [&]()
		{
			QVector<QtVolumeStandardItem*> result;

			auto selected_indexes = selectedIndexes();
			for (auto index : selected_indexes)
			{
				auto current_item = dynamic_cast<QtVolumeStandardItem*>(m_dataModel->itemFromIndex(index));
				if (current_item != nullptr)
				{
					auto data = current_item->data(ROLE_HEIGHT_FIELD_3);
					if (!data.isNull())
					{
						//	Check the item points to a valid heightfield
						HeightField3D* heightfield3 = toPtr<HeightField3D>(data);
						if (heightfield3 != nullptr)
						{
							result.push_back(current_item);
						}
					}
				}
			}
			return result;
		};

		//	Get a list of selected 3D heightfields, we'll only support
		//	JCN creation for 1 or 2 inputs for now...
		auto selected_heightfield3_items = get_selected_heightfield3_func();

		//	Construct a popup menu
		QMenu context_menu(this);
		auto action_view_jcn = new QAction("View as JCN...", &context_menu);
		//	JCN will only be available for 1 or 2 inputs
		action_view_jcn->setEnabled((selected_heightfield3_items.size() >= MIN_HEIGHTFIELDS_FOR_JCN)
									&& (selected_heightfield3_items.size() <= MAX_HEIGHTFIELDS_FOR_JCN));

		connect(action_view_jcn, &QAction::triggered,
				[&](const bool checked)
		{
			qDebug() << "Launch JCN dialog";

			emit OnJcnDialogRequested(selected_heightfield3_items);
		});

		auto action_compute_reeb_graph = new QAction("Compute reeb graph", &context_menu);
		action_compute_reeb_graph->setEnabled(selected_heightfield3_items.size() == 1);
		connect(action_compute_reeb_graph, &QAction::triggered,
				[&](const bool checked)
		{
			auto heightfield_file = selected_heightfield3_items[0]->data(ROLE_PATH_TO_HEIGHTFIELD_FILE).toString();
			qDebug() << "User is requesting a reeb graph for file:"
					 << heightfield_file;

			if (heightfield_file != "") emit OnReebGraphRequested(selected_heightfield3_items[0]);
		});

		//	Add actions to menu

		context_menu.addActions(QList<QAction*>({action_compute_reeb_graph, action_view_jcn}));


		//	Show the popup menu
		context_menu.exec(mapToGlobal(pos));
	});

	//	Handle checkbox events
	connect(m_dataModel, &QStandardItemModel::itemChanged,
			[&](QStandardItem* item)
	{
		cout << "Item changed: " << item->text() << endl;
		auto data3 = item->data(ROLE_HEIGHT_FIELD_3);

		if (!data3.isNull())
		{
			//	First, see if we can cast to a Heightfield3D, if this succeeds
			//	we can emit a signal
			HeightField3D* heightField3 = toPtr<HeightField3D>(data3);
			if (heightField3 != nullptr)
			{
				cout << "Item is heightfield3" << endl;

				//	Extract the stored colour
				auto colour = item->data(ROLE_COLOUR).value<QColor>();

				//	Emit the event
				emit OnItemChangedHeightField3(heightField3, item->checkState(), colour);
			}
		}

		auto data4 = item->data(ROLE_HEIGHT_FIELD_4);

		if (!data4.isNull())
		{
			//	Else, see if we can cast to a Heightfield4D, if this
			//	succeeds we can emit a signal
			HeightField4D* heightField4 = toPtr<HeightField4D>(data4);
			if (heightField4 != nullptr)
			{
				cout << "Item is heightfield4" << endl;

				//	Extract the stored colour
				auto colour = item->data(ROLE_COLOUR).value<QColor>();

				//	Emit the event
				emit OnItemChangedHeightField4(heightField4, item->checkState(), colour);
			}
		}

	});



	setModel(m_dataModel);
}

///
/// \brief QtVolumeSelectionTreeView::generateInitialModel
/// \return
/// \since		03-11-2015
/// \author		Dean
///
QStandardItemModel* QtVolumeSelectionTreeView::generateInitialModel()
{
	QStandardItemModel* result = new QStandardItemModel(this);



	return result;
}

///
/// \brief		Add's the passed 3d heightfield to the tree view
/// \param heightField
/// \since		03-11-2015
/// \author		Dean
/// \author		Dean: add a category for the number of cools [06-11-2015]
///
void QtVolumeSelectionTreeView::AddHeightField3(HeightField3D* heightField,
												const QString& filepath)
{
	const QString REEB_EXT = ".rb";

	assert (heightField != nullptr);

	QStandardItem* rootNode = m_dataModel->invisibleRootItem();

	QString ensembleName = QString::fromStdString(
				heightField->GetEnsembleName());
	QString configurationName = QString::fromStdString(
				heightField->GetConfigurationName());
	QString coolName = QString("cool %1").arg(heightField->GetCoolingSlice());
	QString field_name = QString::fromStdString(
				heightField->GetFieldName());
	auto slice_axis = heightField->GetFixedVariableName()[0];
	auto slice_val = heightField->GetFixedVariableValue();

	//auto dimRoot = findOrCreateNode(rootNode, dimString);
	auto ensembleRoot = findNode(rootNode, ensembleName);
	if (ensembleRoot == nullptr)
		ensembleRoot = createNode(rootNode, ensembleName, false);
	assert(ensembleRoot != nullptr);

	auto configurationRoot = findNode(ensembleRoot, configurationName);
	if (configurationRoot == nullptr)
		configurationRoot = createNode(ensembleRoot, configurationName, false);
	assert(configurationRoot != nullptr);

	auto coolRoot = findNode(configurationRoot, coolName);
	if (coolRoot == nullptr)
		coolRoot = createNode(configurationRoot, coolName, false);
	assert(coolRoot != nullptr);

	auto heightfieldRoot = findNode(coolRoot, field_name);
	if (heightfieldRoot == nullptr)
		heightfieldRoot = createNode(coolRoot, field_name, true);
	assert(heightfieldRoot != nullptr);

	//	Check that the file and path are valid
	QFileInfo file_info(filepath);
	assert(file_info.exists() && file_info.isFile());

	QStandardItem* heightfieldItem = nullptr;
	if (field_name.contains("polyakov", Qt::CaseInsensitive))
	{
		heightfieldItem = createNode(coolRoot, "Polyakov loop", true);
		assert(heightfieldItem != nullptr);
	}
	else if (field_name.contains("Unknown variable", Qt::CaseInsensitive))
	{
		heightfieldItem = createNode(coolRoot, "unknown", true);
		assert(heightfieldItem != nullptr);
	}
	else
	{
		cout << "Looking for slice" << endl;
		heightfieldItem = AddSlice(heightfieldRoot, heightField, slice_axis, slice_val, file_info.absoluteDir());
		assert(heightfieldItem != nullptr);
	}

	cout << "Store path to file as " << filepath << endl;
	heightfieldItem->setData(filepath, ROLE_PATH_TO_HEIGHTFIELD_FILE);
//	if (heightfieldItem == nullptr)
//		heightfieldItem = createNode(coolRoot, field_name, true);
//		assert(heightfieldItem != nullptr);

//	//	Convert the pointer to the heightfield to a QVariant for storage
//	QVariant vHeightField = toQVariant<HeightField3D>(heightField);
//	heightfieldItem->setData(vHeightField, ROLE_HEIGHT_FIELD_3);

//	auto auxillary_data_storage = [&]()
//	{
//		//	Check that the file and path are valid
//		QFileInfo file_info(filepath);
//		assert(file_info.exists() && file_info.isFile());

//		//	Extract some details regarding the files location and name
//		auto absolute_path = file_info.absoluteDir();
//		auto filename_no_extension = file_info.baseName();

//		//	See if a Reeb Graph is present at the location of the data, if so
//		//	store it
//		QFileInfo reeb_file_info = QFileInfo(absolute_path, filename_no_extension + REEB_EXT);
//		cout << "Looking for Reeb graph '"
//			 << reeb_file_info.absoluteFilePath().toStdString()	<< "' : "
//			 << (reeb_file_info.exists() ? "found" : "not found") << endl;

	//	Store the paths to various files etc.
//		heightfieldItem->setData(file_info.absolutePath(),
//								 ROLE_PATH_TO_DATA_DIR);
//		heightfieldItem->setData(filepath,
//								 ROLE_PATH_TO_HEIGHTFIELD_DIR);
//		if (reeb_file_info.exists())
//		{
//			heightfieldItem->setData(reeb_file_info.absoluteFilePath(),
//									 ROLE_PATH_TO_REEB_GRAPH_FILE);
//		}

//	};

//	auxillary_data_storage();
}

QFileInfo QtVolumeSelectionTreeView::find_reeb_graph_for_slice(const QDir& parent_path,
															   const QString& field_name,
															   const QString& slice_var,
															   const size_t slice_val) const
{
	auto target_filename = QString("%1_%2=%3.rb").arg(field_name).arg(slice_var).arg(slice_val);

	auto target_file = QFileInfo(parent_path, target_filename);

	if (target_file.exists()) return target_file;

	throw std::invalid_argument("Could not find file '" + target_filename.toStdString() +"'");
}

QStandardItem* QtVolumeSelectionTreeView::AddSlice(QStandardItem* heightfieldRoot,
												   HeightField3D* height_field,
												   const char slice_axis,
												   const size_t slice_val,
												   const QDir& parent_absolute_path)
{
	QStandardItem* root_node = nullptr;

	//	Strip the '\r' character from the text that Windows 'helpfully' adds
	auto field_name = QString::fromStdString(height_field->GetFieldName()).remove('\r');

	//	Add the xYZT volumes
	auto yztRoot = findNode(heightfieldRoot, "YZT");
	if (yztRoot == nullptr)
		yztRoot = createNode(heightfieldRoot, "YZT", false);
	assert(yztRoot != nullptr);

	//	Add the XyZT volumes
	auto xztRoot = findNode(heightfieldRoot, "XZT");
	if (xztRoot == nullptr)
		xztRoot = createNode(heightfieldRoot, "XZT", false);
	assert(xztRoot != nullptr);

	//	Add the XYzT volumes
	auto xytRoot = findNode(heightfieldRoot, "XYT");
	if (xytRoot == nullptr)
		xytRoot = createNode(heightfieldRoot, "XYT", false);
	assert(xytRoot != nullptr);

	//	Add the XYZt volumes
	auto xyzRoot = findNode(heightfieldRoot, "XYZ");
	if (xyzRoot == nullptr)
		xyzRoot = createNode(heightfieldRoot, "XYZ", false);
	assert(xyzRoot != nullptr);

	switch (slice_axis)
	{
	case 'x':
		root_node = yztRoot;
		break;
	case 'y':
		root_node = xztRoot;
		break;
	case 'z':
		root_node = xytRoot;
		break;
	case 't':
		root_node = xyzRoot;
		break;
	default:
		cerr << "Unknown slice axis '" << slice_axis << "'." << endl;
		//	If the slice is unknown it could be a natively 3D volume
		//	(such as the Polyakov loop)
		//assert(false);
		break;
	}

	auto heightfieldItem = findNode(root_node, QString("%1 = %2").arg(slice_axis).arg(slice_val));
	if (heightfieldItem == nullptr)
	{
		auto formatted_number = QString::number(slice_val).rightJustified(2, '0');
		heightfieldItem = createNode(root_node, QString("%1 = %2").arg(slice_axis).arg(formatted_number), false);
	}
	assert(heightfieldItem != nullptr);


	//	Convert the pointer to the heightfield to a QVariant for storage
	QVariant vHeightField = toQVariant<HeightField3D>(height_field);
	heightfieldItem->setData(vHeightField, ROLE_HEIGHT_FIELD_3);

	try
	{
		cout << "Searching directory: '" << parent_absolute_path.absolutePath()
			 << "' for a reeb graph." <<endl;
		//	Try to locate the (externally computed) Reeb graph for
		//	this data slice.
		auto reeb_path = find_reeb_graph_for_slice(parent_absolute_path,
												   field_name,
												   QString(slice_axis), slice_val);
		if (!reeb_path.exists())
			cerr << "No reeb graph found at: '" << reeb_path.absolutePath() << "'." << endl;

		heightfieldItem->setData(reeb_path.absoluteFilePath(),
								 ROLE_PATH_TO_REEB_GRAPH_FILE);
	}
	catch (std::invalid_argument& ex)
	{
		//	We're expecting that much of the time a Reeb graph might
		//	not be present. In that case just log it for debugging
		//	and set the file location to something predictable
		heightfieldItem->setData("");
		cerr << ex.what() << endl;
	}

	root_node->sortChildren(0);

	return heightfieldItem;
}

///
/// \brief		Add's the passed 4d heightfield to the tree view
/// \param		heightField
/// \since		03-11-2015
/// \author		Dean
/// \author		Dean: add a category for the number of cools [06-11-2015]
///
void QtVolumeSelectionTreeView::AddHeightField4(HeightField4D* heightField4d,
												const QString& file_path)
{
	assert (heightField4d != nullptr);

	qDebug() << "Adding 4D heightfield from " << file_path << endl;

	QStandardItem* rootNode = m_dataModel->invisibleRootItem();

	QString ensembleName = QString::fromStdString(
				heightField4d->GetEnsembleName());
	QString configurationName = QString::fromStdString(
				heightField4d->GetConfigurationName());
	QString field_name = QString::fromStdString(
				heightField4d->GetFieldName());
	QString coolName = QString("cool %1").arg(heightField4d->GetCoolingSlice());

	//auto dimRoot = findOrCreateNode(rootNode, dimString);
	auto ensembleRoot = findNode(rootNode, ensembleName);
	if (ensembleRoot == nullptr)
		ensembleRoot = createNode(rootNode, ensembleName, false);
	assert(ensembleRoot != nullptr);

	auto configurationRoot = findNode(ensembleRoot, configurationName);
	if (configurationRoot == nullptr)
		configurationRoot = createNode(ensembleRoot, configurationName, false);
	assert(configurationRoot != nullptr);

	auto coolRoot = findNode(configurationRoot, coolName);
	if (coolRoot == nullptr)
		coolRoot = createNode(configurationRoot, coolName, false);
	assert(coolRoot != nullptr);

	auto heightfieldRoot = findNode(coolRoot, field_name);
	if (heightfieldRoot == nullptr)
		heightfieldRoot = createNode(coolRoot, field_name, true);
	assert(heightfieldRoot != nullptr);

	//	Convert the HeightField4 to a variant and store in the root
	auto vHeightField4d = toQVariant<HeightField4D>(heightField4d);
	heightfieldRoot->setData(vHeightField4d, ROLE_HEIGHT_FIELD_4);

	//	Check that the file and path are valid
	QFileInfo file_info(file_path);
	assert(file_info.exists() && file_info.isFile());

	//	Extract some details regarding the files location and name
	auto parent_absolute_path = file_info.absoluteDir();

	//	Store the path to the original 4D heightfield
	heightfieldRoot->setData(parent_absolute_path.absolutePath(),
							 ROLE_PATH_TO_DATA_DIR);

	//	Store the path to the original 4D heightfield
	heightfieldRoot->setData(file_path,
							 ROLE_PATH_TO_HEIGHTFIELD_FILE);

	for (auto x = 0; x < heightField4d->GetDimX(); ++x)
	{
		auto heightField3d = heightField4d->GetVolumeYZT(x);
		assert(heightField3d != nullptr);

		AddSlice(heightfieldRoot, heightField3d, 'x', x+1,
				 parent_absolute_path);
	}

	for (auto y = 0; y < heightField4d->GetDimY(); ++y)
	{
		auto heightField3d = heightField4d->GetVolumeXZT(y);
		assert(heightField3d != nullptr);

		AddSlice(heightfieldRoot, heightField3d, 'y', y+1,
				 parent_absolute_path);
	}

	for (auto z = 0; z < heightField4d->GetDimZ(); ++z)
	{
		auto heightField3d = heightField4d->GetVolumeXYT(z);
		assert(heightField3d != nullptr);

		AddSlice(heightfieldRoot, heightField3d, 'z', z+1,
				 parent_absolute_path);
	}

	for (auto t = 0; t < heightField4d->GetDimW(); ++t)
	{
		auto heightField3d = heightField4d->GetVolumeXYZ(t);
		assert(heightField3d != nullptr);

		AddSlice(heightfieldRoot, heightField3d, 't', t+1,
				 parent_absolute_path);
	}
}

///
/// \brief		Creates a new node in the hierarchy with the specified parent
///				and label.
/// \param parent
/// \param label
/// \return
/// \since		03-11-2015
/// \author		Dean
/// \author		Dean: split into separate search and create methods [29-03-2016]
///
QtVolumeStandardItem* QtVolumeSelectionTreeView::createNode(QStandardItem* parent,
															const QString& label,
															const bool& withCheckBox)
{
	//	No match, create the node
	QtVolumeStandardItem* newItem = new QtVolumeStandardItem(label);
	newItem->setCheckable(withCheckBox);
	//	we don't want to be able to edit the text
	newItem->setEditable(false);

	//	If we want to have a check box, we also need a colour
	//	for the histogram line, we will generate this as a swatch icon
	if (withCheckBox)
	{
		auto colour = getNextDefaultColour();

		newItem->setData(colour, ROLE_COLOUR);
		newItem->setIcon(createSwatchIcon(colour, QSize(16, 12), true));
		newItem->setCheckState(Qt::Unchecked);
	}

	parent->appendRow(newItem);

	//	And return it
	return newItem;
}

///
/// \brief		Returns the node with a particular parent and specified label.
///				If a match cannot be found nullptr returned.
/// \param parent
/// \param label
/// \return
/// \since		03-11-2015
/// \author		Dean
/// \author		Dean: split into separate search and create methods [29-03-2016]
///
QStandardItem* QtVolumeSelectionTreeView::findNode(QStandardItem* parent, const QString& label) const
{
	assert(parent != nullptr);
	cout << "Looking for node with label '" << label << "'." << endl;

	//	Do we already have a label constructed?
	if (parent->hasChildren())
	{
		for (auto i = 0; i < parent->rowCount(); ++i)
		{
			if (parent->child(i)->data(Qt::DisplayRole) == label)
			{
				//	Found a match, so return it
				return parent->child(i);
			}
		}
	}

	//	No match return nullptr
	return nullptr;
}
