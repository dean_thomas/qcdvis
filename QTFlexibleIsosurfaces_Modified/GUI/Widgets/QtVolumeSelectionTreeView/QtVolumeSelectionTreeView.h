#ifndef QTVOLUMESELECTIONTREEVIEW_H
#define QTVOLUMESELECTIONTREEVIEW_H

#include <QWidget>
#include <QTreeView>
#include <QStandardItemModel>
#include <vector>
#include <QFileInfo>
#include "HeightField3D.h"
#include "HeightField4D.h"


#include "../Globals/QtFunctions.h"

//	Use to store the user assigned colour for any graphs etc.
#define ROLE_COLOUR Qt::UserRole + 1

//	We can, if we need, store a pointer to the relevant 3D and 4D heightfields
#define ROLE_HEIGHT_FIELD_3 Qt::UserRole + 2
#define ROLE_HEIGHT_FIELD_4 Qt::UserRole + 3

//	Storage for the absolute path to where the cache data exists
#define ROLE_PATH_TO_DATA_DIR Qt::UserRole + 10

//	Storage for the absolute path to the heightfield
#define ROLE_PATH_TO_HEIGHTFIELD_DIR Qt::UserRole + 11

//	Storage for a pre-computed Reeb graph
#define ROLE_PATH_TO_REEB_GRAPH_FILE Qt::UserRole + 12

//	Storage for the actual heightfield file
#define ROLE_PATH_TO_HEIGHTFIELD_FILE Qt::UserRole + 13

class QtVolumeStandardItem : public QStandardItem
{
	public:
		QtVolumeStandardItem(const QString& label)
			: QStandardItem(label) { }

		QString getReebGraphFilename() const
		{
			auto reeb_graph_filename = data(ROLE_PATH_TO_REEB_GRAPH_FILE);

			if (!reeb_graph_filename.isNull())
				return reeb_graph_filename.toString();
			else
				return "";
		}

		HeightField3D* getHeightfield3() const
		{
			auto heightfield = data(ROLE_HEIGHT_FIELD_3);
			if (!heightfield.isNull())
			{
				HeightField3D* clickedHeightField3 = toPtr<HeightField3D>(heightfield);

				if (clickedHeightField3 != nullptr)
				{
					return clickedHeightField3;
				}
			}
			return nullptr;
		}

		///
		/// \brief getHeightfieldFilename
		/// \return
		/// \author		Dean
		/// \since		17-12-2016
		///
		QString getHeightfieldFilename() const
		{
			auto path_data = data(ROLE_PATH_TO_HEIGHTFIELD_FILE);
			if (!path_data.isNull())
			{
				return path_data.toString();
			}
			else
			{
				return "";
			}
		}

		///
		/// \brief getFieldname
		/// \return
		/// \author		Dean
		/// \since		17-12-2016
		///
		QString getFieldname() const
		{
			auto heightfield_data = data(ROLE_HEIGHT_FIELD_3);
			if (!heightfield_data.isNull())
			{
				HeightField3D* heightfield = toPtr<HeightField3D>(heightfield_data);

				if (heightfield != nullptr)
				{
					return QString::fromStdString(heightfield->GetFieldName());
				}
			}
			return "";
		}
};

class QtVolumeSelectionTreeView : public QTreeView
{
		Q_OBJECT
	public:
		using ColourTable = std::vector<QColor>;
		using size_type = size_t;
	private:
		ColourTable m_defaultColourTable;
		size_type m_defaultColourIndex = 0;
		QColor getNextDefaultColour();

		ColourTable generateDefaultColourTable() const;

		QStandardItemModel* generateInitialModel();
		QStandardItemModel* m_dataModel = nullptr;

		//	Functions used for constructing and traversing the tree
		QStandardItem* findNode(QStandardItem* parent, const QString& label) const;
		QtVolumeStandardItem* createNode(QStandardItem* parent,
										 const QString& label,
										 const bool& withCheckBox);

		//	Convenience functions for finding an entry in the hierarchy
		QStandardItem* findEnsembleRoot(HeightField3D* const heightfield3) const;
		QStandardItem* findConfigurationRoot(HeightField3D* const heightfield3) const;
		QStandardItem* findCoolingRoot(HeightField3D* const heightfield3) const;
		QStandardItem* findFieldVariableRoot(HeightField3D* const heightfield3) const;
		QStandardItem* findSliceRoot(HeightField3D* const heightfield3) const;

		QStandardItem* findSliceItem(HeightField3D* const heightfield3) const;
		QStandardItem* findCoolingSliceItem(HeightField3D* const heightfield3) const;

		QStandardItem* findSlice(const std::string& ensemble,
								 const std::string& configuration,
								 const std::string& field,
								 const size_t cools,
								 const std::string fixed_variable_name,
								 const size_t fixed_variable_value) const;

		QFileInfo find_reeb_graph_for_slice(const QDir& parent_path,
											const QString& field_name,
											const QString& slice_var,
											const size_t slice_val) const;

		QStandardItem *AddSlice(QStandardItem* heightfieldRoot, HeightField3D* height_field,
								const char slice_axis, const size_t slice_val,
								const QDir &parent_absolute_path);

	public:
		//	Convenience functions for looking up if a heightfield should be
		//	locked to the global view (ie using the parents colour schemes etc)
		bool IsLockedToGlobalView(HeightField3D* const heightfield3) const;

		QVector<QFileInfo> GetLoadedFiles() const;

		explicit QtVolumeSelectionTreeView(QWidget *parent = 0);

		void AddHeightField3(HeightField3D* heightField, const QString& filepath);
		void AddHeightField4(HeightField4D* heightField4d, const QString &file_path);

		void ClearAll();

		//	Given a 3D field slice, return the neighbouring slices
		QStandardItem *GetPrevSlice(HeightField3D* const currentSlice) const;
		QStandardItem* GetNextSlice(HeightField3D* const currentSlice) const;

		QStandardItem *GetPrevCoolingSlice(HeightField3D* const currentSlice) const;
		QStandardItem *GetNextCoolingSlice(HeightField3D* const currentSlice) const;

	signals:

	public slots:

	signals:
		void OnDoubleClickedHeightField3(QtVolumeStandardItem* heightField3D);
		void OnItemChangedHeightField4(HeightField4D* heightField4D,
									   const Qt::CheckState& state,
									   const QColor& colour);
		void OnItemChangedHeightField3(HeightField3D* heightField3D,
									   const Qt::CheckState& state,
									   const QColor& colour);
		void OnJcnDialogRequested(const QVector<QtVolumeStandardItem*> heightfieldItems);
		void OnReebGraphRequested(QtVolumeStandardItem* item);
};

#endif // QTVOLUMESELECTIONTREEVIEW_H
