#ifndef QTISOVALUEHISTOGRAMWIDGET_H
#define QTISOVALUEHISTOGRAMWIDGET_H

#include <QWidget>
#include "HeightField3D.h"
#include <QPainter>
#include "Statistics/SuperarcDistribution.h"
#include "Graphs/ContourTree.h"
#include <QColor>
#include "GUI/Forms/QtHistogramForm.h"
#include <map>

namespace Ui {
	class QtIsovalueHistogramWidget;
}

class QtIsovalueHistogramWidget : public QWidget
{
		Q_OBJECT

	public:
		using size_type = size_t;

		enum class Orientation
		{
			HORIZONTAL,
			VERTICAL
		};

		struct DrawProperties
		{
			QColor lineColour;
		};

	public:
		using HistogramArray = std::map<SuperarcDistribution*, DrawProperties>;

		explicit QtIsovalueHistogramWidget(QWidget *parent = 0);
		~QtIsovalueHistogramWidget();

		void AddSuperarcDistribution(SuperarcDistribution *superarcDistribution,
									 const QColor& color);
		void RemoveSuperarcDistribution(SuperarcDistribution *superarcDistribution);
		void SetCurrentNormalisedIsovalue(const float &value);

		virtual void paintEvent(QPaintEvent *);

		Orientation GetOrientation() const { return m_orientation; }
		void SetOrientation(const Orientation& value);
		void SwitchOrientation();

		void Clear();
	private:
		virtual void mouseDoubleClickEvent(QMouseEvent*);

		float calculateUnitWidth(const size_t &binCount,
								 const size_t &highestCount) const;

		float calculateUnitHeight(const size_t& binCount,
								  const size_t& highestCount) const;

		void drawHistogram(const SuperarcDistribution& distribution,
						   QPainter &painter,
						   QPen &pen,
						   const float& unitWidth,
						   const float& unitHeight,
						   const unsigned long &numberOfBins);

		void drawCurrentIsovalueLine(QPainter &painter);
		void drawZeroLine(QPainter &painter);
		void drawTickLines(QPainter &painter, const float &unitWidth, const float &unitHeight, const unsigned long &globalMax);
		unsigned long findGlobalMaximumBinSize() const;

		void updateMinMax();

		float m_minimumValue = -1.0f;
		float m_maximumValue = +1.0f;

		Ui::QtIsovalueHistogramWidget *ui;
		HistogramArray m_superarcDistributionArray;
		float m_currentIsovalue = 0.0f;

		float m_zeroLineThickness = 1.0f;
		QColor m_zeroLineColour = QColor(Qt::black);

		float m_currentIsovalueLineThickness = 1.0f;
		QColor m_currentIsovalueLineColor = QColor(Qt::black);

		float m_histogramLineThickness = 1.0f;

		Orientation m_orientation;

		unsigned long m_tickStep = 25;

	signals:
		void OnMinMaxUpdate(const float& min, const float& max);
};

#endif // QTISOVALUEHISTOGRAMWIDGET_H
