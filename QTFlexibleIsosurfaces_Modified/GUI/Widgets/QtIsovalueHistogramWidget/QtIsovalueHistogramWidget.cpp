#include "QtIsovalueHistogramWidget.h"
#include "ui_QtIsovalueHistogramWidget.h"

#ifdef _MSC_VER
#define __PRETTY_FUNCTION__ __FUNCSIG__
#endif

#define DISABLE_DEBUG_OUTPUT

///
/// \brief		Allows us to remove any existing histogram data
/// \author		Dean
/// \since		02-11-2015
///
void QtIsovalueHistogramWidget::Clear()
{
	m_superarcDistributionArray.clear();

	update();
}

void QtIsovalueHistogramWidget::mouseDoubleClickEvent(QMouseEvent*)
{
	//	TODO: Histogram doesn't currently display the relevant data
	QtHistogramForm *newForm = new QtHistogramForm(this);
	newForm->show();
}

///
/// \brief QtIsovalueHistogramWidget::SetOrientation
/// \param value
/// \author		Dean
/// \since		01-09-2015
///
void QtIsovalueHistogramWidget::SetOrientation(const Orientation& value)
{
	m_orientation = value;

	update();
}

///
/// \brief QtIsovalueHistogramWidget::SwitchOrientation
/// \author		Dean
/// \since		01-09-2015
///
void QtIsovalueHistogramWidget::SwitchOrientation()
{
	if (m_orientation == Orientation::HORIZONTAL)
	{
		m_orientation = Orientation::VERTICAL;
	}
	else
	{
		m_orientation = Orientation::HORIZONTAL;
	}

	update();
}

///
/// \brief		Calculates the width of each unit, depending on orientation of
///				the histogram
/// \param		binCount
/// \param		highestCount
/// \return
/// \author		Dean
/// \since		01-09-2015
///
float QtIsovalueHistogramWidget::calculateUnitWidth(const size_t& binCount,
													const size_t& highestCount) const
{
	if (m_orientation == Orientation::VERTICAL)
	{
		return ((float)width() / (float)(highestCount+1));
	}
	else
	{
		return ((float)width() / (float)binCount);
	}
}

///
/// \brief		Calculates the height of each unit, depending on orientation of
///				the histogram
/// \param binCount
/// \param highestCount
/// \return
/// \author		Dean
/// \since		01-09-2015
///
float QtIsovalueHistogramWidget::calculateUnitHeight(const size_t& binCount,
													 const size_t& highestCount) const
{
	if (m_orientation == Orientation::VERTICAL)
	{
		return ((float)height() / (float)binCount);
	}
	else
	{
		return ((float)height() / (float)(highestCount+1));
	}
}

unsigned long QtIsovalueHistogramWidget::findGlobalMaximumBinSize() const
{
	unsigned long max = std::numeric_limits<unsigned long>::min();

	for (auto it = m_superarcDistributionArray.cbegin();
		 it != m_superarcDistributionArray.cend();
		 ++it)
	{
		auto dist = it->first;
		if (dist->GetMaximumBinSize() > max) max = dist->GetMaximumBinSize();
	}
	return max;
}

///
/// \brief QtIsovalueHistogramWidget::QtIsovalueHistogramWidget
/// \param parent
/// \since		07-07-2015
/// \author		Dean
///
QtIsovalueHistogramWidget::QtIsovalueHistogramWidget(QWidget *parent) :
	QWidget(parent),
	ui(new Ui::QtIsovalueHistogramWidget)
{
	ui->setupUi(this);

	//	Default to vertical
	m_orientation = Orientation::VERTICAL;
	//m_orientation = Orientation::HORIZONTAL
}

///
/// \brief QtIsovalueHistogramWidget::~QtIsovalueHistogramWidget
///	\since		07-07-2015
/// \author		Dean
QtIsovalueHistogramWidget::~QtIsovalueHistogramWidget()
{
	delete ui;
}

///
/// \brief		Sets the 3d HeightField that will be the source of the data
/// \param heightField3d
/// \since		07-07-2015
/// \author		Dean
///
void QtIsovalueHistogramWidget::AddSuperarcDistribution(SuperarcDistribution *superarcDistribution,
														const QColor& color)
{
	m_superarcDistributionArray[superarcDistribution] = { color };

	updateMinMax();

	update();
}

///
/// \brief QtIsovalueHistogramWidget::RemoveSuperarcDistribution
/// \param superarcDistribution
/// \since		08-12-2015
/// \author		Dean
///
void QtIsovalueHistogramWidget::RemoveSuperarcDistribution(SuperarcDistribution *superarcDistribution)
{
	if (superarcDistribution == nullptr) return;

	auto elemIt = m_superarcDistributionArray.find(superarcDistribution);
	if (elemIt != m_superarcDistributionArray.cend())
		m_superarcDistributionArray.erase(elemIt);

	updateMinMax();

	update();
}

///
/// \brief		Sets the isovalue for the marker line (range 0.0 - 1.0)
/// \param value
///	\since		07-07-2015
/// \author		Dean
///
void QtIsovalueHistogramWidget::SetCurrentNormalisedIsovalue(const float &value)
{
	assert (value >= 0.0f);
	assert (value <= 1.0f);

	m_currentIsovalue = value;
	update();
}

///
/// \brief		Repaints the widget
///	\since		07-07-2015
/// \author		Dean
///
void QtIsovalueHistogramWidget::paintEvent(QPaintEvent *)
{
	if (m_superarcDistributionArray.empty()) return;


	QPainter painter(this);
	painter.save();

	//	iterator to first element
	auto firstIt = m_superarcDistributionArray.cbegin();
	auto firstDist = firstIt->first;
	unsigned long numberOfBins = firstDist->GetNumberOfBins();

	unsigned long maxBinSize = findGlobalMaximumBinSize();

	float unitWidth = calculateUnitWidth(numberOfBins, maxBinSize);
	float unitHeight = calculateUnitHeight(numberOfBins, maxBinSize);

	//	Draws lines at specified intervals across the histogram
	drawTickLines(painter, unitWidth, unitHeight, maxBinSize);

	//	Draw histograms
	for (auto it = m_superarcDistributionArray.cbegin();
		 it != m_superarcDistributionArray.cend();
		 ++it)
	{
		auto dist = it->first;
		auto drawParams = it->second;

		assert(dist != nullptr);

		QPen pen(drawParams.lineColour);
		pen.setWidthF(m_histogramLineThickness);

		//m_superarcDistributionArray[i].data->SetGlobalMinimumAndMaximum(m_minimumValue,
		//														   m_maximumValue);

		SuperarcDistribution tempCopy = *dist;
		tempCopy.SetGlobalMinimumAndMaximum(m_minimumValue,
											m_maximumValue);

		//	Draw the histogram
		drawHistogram(tempCopy,
					  painter, pen, unitWidth, unitHeight, numberOfBins);


		//pen.setColor(Qt::blue);
		//drawHistogram(m_superarcDistributionArray[i],
		//			  painter, pen, unitWidth, unitHeight, numberOfBins);
	}

	//	Draw isovalue line
	drawCurrentIsovalueLine(painter);

	//	Draw zero line (only if we cross zero)
	drawZeroLine(painter);

	painter.restore();
}

///
/// \brief		Draws a line across the histograms at the current isovalue
/// \param painter
/// \since		04-08-2015
/// \author		Dean
/// \author		Dean: add the ability to switch orientation [01-09-2015]
///
void QtIsovalueHistogramWidget::drawCurrentIsovalueLine(QPainter &painter)
{
	QPen currentIsoPen(m_currentIsovalueLineColor, m_currentIsovalueLineThickness);
	painter.setPen(currentIsoPen);

	if (m_orientation == Orientation::VERTICAL)
	{
		//	Origin at bottom of widget
		QPoint leftPointCurrent(0, height() - (height() * m_currentIsovalue));
		QPoint rightPointCurrent(width(), height() - (height() * m_currentIsovalue));
		painter.drawLine(leftPointCurrent, rightPointCurrent);
	}
	else
	{
		//	Origin at left of widget
		QPoint topPointCurrent(width() * m_currentIsovalue, 0);
		QPoint bottomPointCurrent(width() * m_currentIsovalue, height());
		painter.drawLine(topPointCurrent, bottomPointCurrent);
	}
}

///
/// \brief QtIsovalueHistogramWidget::updateMinMax
/// \since		08-12-2015
/// \author		Dean
///
void QtIsovalueHistogramWidget::updateMinMax()
{
	if (m_superarcDistributionArray.empty())
	{
		//	No data, set to a predictable scale
		m_minimumValue = -1.0f;
		m_maximumValue = + 1.0f;
	}
	else
	{
		//	Initialize to the first element
		auto itFirst = m_superarcDistributionArray.cbegin();
		auto data = itFirst->first;
		m_minimumValue = data->GetGlobalMinimum();
		m_maximumValue = data->GetGlobalMaximum();

		for (auto it = m_superarcDistributionArray.cbegin();
			 it != m_superarcDistributionArray.cend();
			 ++it)
		{
			//	Access each data set
			auto data = it->first;

			//	Update the minima and maxima for the whole dataset
			if (data->GetGlobalMaximum() > m_maximumValue)
				m_maximumValue = data->GetGlobalMaximum();
			if (data->GetGlobalMinimum() < m_minimumValue)
				m_minimumValue = data->GetGlobalMinimum();
		}
	}

	emit OnMinMaxUpdate(m_minimumValue, m_maximumValue);
}

///
/// \brief		Draws a line across the histograms at the current 'zero' point
/// \param painter
/// \since		04-08-2015
/// \author		Dean
/// \author		Dean: add the ability to switch orientation [01-09-2015]
///
void QtIsovalueHistogramWidget::drawZeroLine(QPainter &painter)
{
	if ((m_minimumValue < 0.0)
		&& (m_maximumValue > 0.0))
	{
		float range = m_maximumValue - m_minimumValue;
		float offset = m_maximumValue / range;

		QPen zeroPen(m_zeroLineColour, m_zeroLineThickness);
		painter.setPen(zeroPen);

		if (m_orientation == Orientation::VERTICAL)
		{

			//	Note: as we are using the maximum, we don't need to reverse orintation
			QPoint leftPoint(0, height() * offset);
			QPoint rightPoint(width(), height() * offset);
			painter.drawLine(leftPoint, rightPoint);
#ifndef DISABLE_DEBUG_OUTPUT
			cout << __PRETTY_FUNCTION__ << endl;
			cout << "Range: " << m_minimumValue;
			cout << "..." << m_maximumValue;
			cout << endl;
			cout << "Zero line at: " << offset << endl;
#endif
		}
		else
		{
			//	Note: as we are using the maximum, we don't need to reverse orintation
			QPoint topPoint(width() * offset, 0);
			QPoint bottomPoint(width() * offset, height());
			painter.drawLine(topPoint, bottomPoint);
		}
	}
}

///
/// \brief		Draws a histogram for the specified distribution
/// \param painter
/// \since		04-08-2015
/// \author		Dean
/// \author		Dean: add the ability to switch orientation [01-09-2015]
///
void QtIsovalueHistogramWidget::drawHistogram(const SuperarcDistribution& distribution,
											  QPainter &painter,
											  QPen &pen,
											  const float& unitWidth,
											  const float& unitHeight,
											  const unsigned long &numberOfBins)
{
	painter.setPen(pen);

	for (unsigned long i = 1; i < numberOfBins; ++i)
	{
		unsigned long binValue = distribution.GetNumberOfSuperarcWithIsovalue1d(i);
		unsigned long binValuePrev = distribution.GetNumberOfSuperarcWithIsovalue1d(i-1);

		QPoint previousPt;
		QPoint currentPt;

		if (m_orientation == Orientation::VERTICAL)
		{
			//	Use left of widget as zero
			previousPt = QPoint(binValuePrev * unitWidth,
								height() - ((i-1) * unitHeight));
			currentPt = QPoint(binValue * unitWidth,
							   height() - (i * unitHeight));
		}
		else
		{
			//	Use bottom of widget as zero
			previousPt = QPoint(width() - ((i-1) * unitWidth),
								height() - (binValuePrev * unitHeight));
			currentPt = QPoint(width() - (i * unitWidth),
							   height() - (binValue * unitHeight));
		}

		painter.drawLine(previousPt, currentPt);
	}
}

///
/// \brief		Draws tick marks at specified intervals
/// \param painter
/// \since		05-08-2015
/// \author		Dean
/// \author		Dean: add the ability to switch orientation [01-09-2015]
///
void QtIsovalueHistogramWidget::drawTickLines(QPainter &painter,
											  const float& unitWidth,
											  const float& unitHeight,
											  const unsigned long& globalMax)
{
	QPen pen(QColor(225, 225, 225));
	painter.setPen(pen);

	if (m_orientation == Orientation::VERTICAL)
	{
		//	Top to bottom
		for (unsigned int i = 0; i < globalMax; i += m_tickStep)
		{
			QPoint topPoint(i * unitWidth, 0);
			QPoint bottomPoint(i * unitWidth, height());

			painter.drawLine(topPoint, bottomPoint);
		}
	}
	else
	{
		//	Left to right
		for (unsigned int i = 0; i < globalMax; i += m_tickStep)
		{
			QPoint leftPoint(0, i * unitHeight);
			QPoint rightPoint(width(), i * unitHeight);

			painter.drawLine(leftPoint, rightPoint);
		}
	}
}
