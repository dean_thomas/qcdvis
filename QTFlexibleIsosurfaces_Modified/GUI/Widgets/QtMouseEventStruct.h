///
///		\brief		Stores a mouse event in an easier to handle struct
///		\author		Dean
///		\since		05-03-2015
///
#ifndef QTMOUSEEVENTSTRUCT
#define QTMOUSEEVENTSTRUCT

#include <QMouseEvent>

struct QtMouseEventStruct
{
		bool DoubleClick;

		bool Control, Shift;

		bool LeftButton, RightButton, MiddleButton;

		float X, Y;
		float NormalizedX, NormalizedY;

		QtMouseEventStruct(QMouseEvent *event,
						   const float hostWidth,
						   const float hostHeight)
		{
			if (event->type() == QEvent::MouseButtonDblClick)
			{
				DoubleClick = true;
			}
			else
			{
				DoubleClick = false;
			}

			//	Keyboard modifiers
			Control = event->modifiers().testFlag(Qt::ControlModifier);
			Shift = event->modifiers().testFlag(Qt::ShiftModifier);

			//	Mouse buttons
			LeftButton = event->buttons().testFlag(Qt::LeftButton);
			RightButton = event->buttons().testFlag(Qt::RightButton);
			MiddleButton = event->buttons().testFlag(Qt::MiddleButton);

			// scale both coordinates to window size
			NormalizedX = (float) event->x() / hostWidth;
			NormalizedY = (float)(hostHeight - event->y()) / hostHeight;

			//	for completeness
			X = (float)event->x();
			Y = (float)event->y();
		}
};

#endif // QTMOUSEEVENTSTRUCT

