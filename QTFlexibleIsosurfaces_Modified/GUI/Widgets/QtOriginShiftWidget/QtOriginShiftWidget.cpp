#include "QtOriginShiftWidget.h"
#include "ui_QtOriginShiftWidget.h"

///
/// \brief QtOriginShiftWidget::QtOriginShiftWidget
/// \param parent
/// \since
/// \author		Dean
/// \author		Dean: update to provide feedback on slider position numerically
///				[22-09-2015]
///
QtOriginShiftWidget::QtOriginShiftWidget(QWidget *parent) :
	QWidget(parent),
	ui(new Ui::QtOriginShiftWidget)
{
	ui->setupUi(this);

	//	Fire a common event whenever a slider is changed
	connect(ui->horizontalSliderOffsetX, &QSlider::valueChanged,
			[=]
	{
		ui->labelOffsetXCount->setText(
					QString::number(ui->horizontalSliderOffsetX->value()));
		emit OnOffsetChange( ui->horizontalSliderOffsetX->value(),
							 ui->horizontalSliderOffsetY->value(),
							 ui->horizontalSliderOffsetZ->value(),
							 ui->horizontalSliderOffsetT->value());
	});
	connect(ui->horizontalSliderOffsetY, &QSlider::valueChanged,
			[=]
	{
		ui->labelOffsetYCount->setText(
					QString::number(ui->horizontalSliderOffsetY->value()));
		emit OnOffsetChange( ui->horizontalSliderOffsetX->value(),
									   ui->horizontalSliderOffsetY->value(),
									   ui->horizontalSliderOffsetZ->value(),
									   ui->horizontalSliderOffsetT->value());
	});
	connect(ui->horizontalSliderOffsetZ, &QSlider::valueChanged,
			[=]
	{
		ui->labelOffsetZCount->setText(
					QString::number(ui->horizontalSliderOffsetZ->value()));
		emit OnOffsetChange( ui->horizontalSliderOffsetX->value(),
									   ui->horizontalSliderOffsetY->value(),
									   ui->horizontalSliderOffsetZ->value(),
									   ui->horizontalSliderOffsetT->value());
	});
	connect(ui->horizontalSliderOffsetT, &QSlider::valueChanged,
			[=]
	{
		ui->labelOffsetTCount->setText(
					QString::number(ui->horizontalSliderOffsetT->value()));
		emit OnOffsetChange( ui->horizontalSliderOffsetX->value(),
									   ui->horizontalSliderOffsetY->value(),
									   ui->horizontalSliderOffsetZ->value(),
									   ui->horizontalSliderOffsetT->value());
	});
}

QtOriginShiftWidget::~QtOriginShiftWidget()
{
	delete ui;
}

void QtOriginShiftWidget::SetAxisRanges(const unsigned long &x,
										const unsigned long &y,
										const unsigned long &z,
										const unsigned long &t)
{
	ui->horizontalSliderOffsetX->setMaximum(x);
	ui->horizontalSliderOffsetY->setMaximum(y);
	ui->horizontalSliderOffsetZ->setMaximum(z);
	ui->horizontalSliderOffsetT->setMaximum(t);

	ui->horizontalSliderOffsetX->setEnabled((x > 0) ? true : false);
	ui->horizontalSliderOffsetY->setEnabled((y > 0) ? true : false);
	ui->horizontalSliderOffsetZ->setEnabled((z > 0) ? true : false);
	ui->horizontalSliderOffsetT->setEnabled((t > 0) ? true : false);
}

void QtOriginShiftWidget::SetValues(const size_t &x,
									const size_t &y,
									const size_t &z,
									const size_t &t)
{
	assert (x <= ui->horizontalSliderOffsetX->maximum());
	ui->horizontalSliderOffsetX->setValue(x);

	assert (y <= ui->horizontalSliderOffsetY->maximum());
	ui->horizontalSliderOffsetX->setValue(y);

	assert (z <= ui->horizontalSliderOffsetZ->maximum());
	ui->horizontalSliderOffsetX->setValue(z);

	assert (t <= ui->horizontalSliderOffsetT->maximum());
	ui->horizontalSliderOffsetX->setValue(t);
}

///
/// \brief QtOriginShiftWidget::SetEnabled
/// \param x
/// \param y
/// \param z
/// \param t
/// \author		Dean
/// \since		28-08-2015
///
void QtOriginShiftWidget::SetEnabled(const bool &x,
									 const bool &y,
									 const bool &z,
									 const bool &t)
{
	ui->horizontalSliderOffsetX->setEnabled(x);
	ui->horizontalSliderOffsetY->setEnabled(y);
	ui->horizontalSliderOffsetZ->setEnabled(z);
	ui->horizontalSliderOffsetT->setEnabled(t);
}

///
/// \brief QtOriginShiftWidget::GetMaxX
/// \return
/// \author		Dean
/// \since		28-08-2015
///
size_t QtOriginShiftWidget::GetMaxX() const
{
	return ui->horizontalSliderOffsetX->maximum();
}

///
/// \brief QtOriginShiftWidget::GetMaxY
/// \return
/// \author		Dean
/// \since		28-08-2015
///
size_t QtOriginShiftWidget::GetMaxY() const
{
	return ui->horizontalSliderOffsetY->maximum();
}

///
/// \brief QtOriginShiftWidget::GetMaxZ
/// \return
/// \author		Dean
/// \since		28-08-2015
///
size_t QtOriginShiftWidget::GetMaxZ() const
{
	return ui->horizontalSliderOffsetZ->maximum();
}

///
/// \brief QtOriginShiftWidget::GetMaxT
/// \return
/// \author		Dean
/// \since		28-08-2015
///
size_t QtOriginShiftWidget::GetMaxT() const
{
	return ui->horizontalSliderOffsetT->maximum();
}

unsigned long QtOriginShiftWidget::GetAxisOffsetX() const
{
	return ui->horizontalSliderOffsetX->value();
}

unsigned long QtOriginShiftWidget::GetAxisOffsetY() const
{
	return ui->horizontalSliderOffsetY->value();
}

unsigned long QtOriginShiftWidget::GetAxisOffsetZ() const
{
	return ui->horizontalSliderOffsetZ->value();
}

unsigned long QtOriginShiftWidget::GetAxisOffsetT() const
{
	return ui->horizontalSliderOffsetT->value();
}
