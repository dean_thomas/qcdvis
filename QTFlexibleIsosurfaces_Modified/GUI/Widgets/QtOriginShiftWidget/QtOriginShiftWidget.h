#ifndef QTORIGINSHIFTWIDGET_H
#define QTORIGINSHIFTWIDGET_H

#include <QWidget>

#include <cassert>

namespace Ui {
	class QtOriginShiftWidget;
}

class QtOriginShiftWidget : public QWidget
{
		Q_OBJECT

	public:
		explicit QtOriginShiftWidget(QWidget *parent = 0);
		~QtOriginShiftWidget();

		void SetAxisRanges(const unsigned long &x,
						   const unsigned long &y,
						   const unsigned long &z,
						   const unsigned long &t);
		unsigned long GetAxisOffsetX() const;
		unsigned long GetAxisOffsetY() const;
		unsigned long GetAxisOffsetZ() const;
		unsigned long GetAxisOffsetT() const;

		void SetEnabled(const bool &x,
						const bool &y,
						const bool &z,
						const bool &t);

		void SetValues(const size_t &x,
					   const size_t &y,
					   const size_t &z,
					   const size_t &t);

		size_t GetMaxX() const;
		size_t GetMaxY() const;
		size_t GetMaxZ() const;
		size_t GetMaxT() const;

	signals:
		void OnOffsetChange(const unsigned long x,
							const unsigned long y,
							const unsigned long z,
							const unsigned long t);
	private:
		Ui::QtOriginShiftWidget *ui;
};

#endif // QTORIGINSHIFTWIDGET_H
