#include "QtTreeWidgetContourItem.h"

//void QtTreeWidgetContourItem::update()
//{
//	printf("State change in: %s.\n", text(0).toStdString().c_str());
//	fflush(stdout);
//}

void QtTreeWidgetContourItem::saveState()
{
	//	This always evaluates to false when asking the object
	//	(so force to true for now).
	m_state->setValue("expanded", CONTOURS_EXPANDED_BY_DEFAULT);

	m_state->beginGroup(KEY_MESH);
	{
		m_state->setValue("expanded", m_meshRootItem->isExpanded());
	}
	m_state->endGroup();

	m_state->beginGroup(KEY_ISOVALUE);
	{
		m_state->setValue("expanded", m_isovalueRootItem->isExpanded());
	}
	m_state->endGroup();

	m_state->beginGroup(KEY_BOUNDING_BOX);
	{
		m_state->setValue("expanded", m_boundingBoxRootItem->isExpanded());
	}
	m_state->endGroup();

	m_state->beginGroup(KEY_CONTOUR_TREE);
	{
		m_state->setValue("expanded", m_contourTreeRootItem->isExpanded());

		m_state->beginGroup(KEY_SUPERARC);
		{
			m_state->setValue("expanded", m_superarcRootItem->isExpanded());
		}
		m_state->endGroup();
	}
	m_state->endGroup();
}

void QtTreeWidgetContourItem::addGeometryProperties()
{
	//	Start to add the individual properties of the contour
	QTreeWidgetItem *newContourSurfaceAreaItem
			= new QTreeWidgetItem(this);
	newContourSurfaceAreaItem->setText(0,QString("Surface Area: %1").
									   arg((float)m_contour->GetSurfaceArea()));

	QTreeWidgetItem *newContourVolumeItem
			= new QTreeWidgetItem(this);
	newContourVolumeItem->setText(0,QString("Enclosed Volume: %1").
								  arg((float)m_contour->GetUnscaledVolume()));

	QTreeWidgetItem *newCentreOfMassItem
			= new QTreeWidgetItem(this);
	newCentreOfMassItem->setText(0, QString("Centre of mass: %1").
								 arg(m_contour->GetCentreOfMass().ToString().c_str()));

	QTreeWidgetItem *newMomentOfInertiaItem
			= new QTreeWidgetItem(this);
	newMomentOfInertiaItem->setText(0, QString("Moment of inertia: %1").
								 arg(m_contour->GetMomentOfInertia().ToString().c_str()));
}

void QtTreeWidgetContourItem::addMeshProperties()
{
	m_state->beginGroup(KEY_MESH);
	{
		//	Mesh
		m_meshRootItem = new QTreeWidgetItem(this);
		m_meshRootItem->setText(0,QString("Mesh"));
		m_meshRootItem->setExpanded(
					m_state->value("expanded",
								   MESH_SUBTREE_EXPANDED_BY_DEFAULT).value<bool>());
		{
			QTreeWidgetItem *newContourTriangleCountItem
					= new QTreeWidgetItem(m_meshRootItem);
			newContourTriangleCountItem->setText(0,QString("Triangle Count: %1").
												 arg(m_contour->GetTriangleCount()));
			//newContourTriangleCountItem->setExpanded(true);

			QTreeWidgetItem *newContourEulerCharateristicItem
					= new QTreeWidgetItem(m_meshRootItem);
			newContourEulerCharateristicItem->setText(0,QString("Euler charateristic: %1").
													  arg(m_contour->GetEulerCharacteristic()));
			//newContourEulerCharateristicItem->setExpanded(true);
		}
	}
	m_state->endGroup();
}

void QtTreeWidgetContourItem::addIsovalueProperties()
{
	m_state->beginGroup(KEY_ISOVALUE);
	{
		//ISOVALUE_SUB_TREE_EXPANDED_BY_DEFAULT
		//	Isovalue tree
		m_isovalueRootItem = new QTreeWidgetItem(this);
		m_isovalueRootItem->setText(0,QString("Isovalue"));
		m_isovalueRootItem->setExpanded(
					m_state->value("expanded",
								   ISOVALUE_SUBTREE_EXPANDED_BY_DEFAULT).value<bool>());
		{
			QTreeWidgetItem *newMinIsoValueItem
					= new QTreeWidgetItem(m_isovalueRootItem);
			newMinIsoValueItem->setText(0,QString("Minimum: %1").
										arg((float)m_contour->GetMinimumIsovalue()));

			QTreeWidgetItem *newMaxIsoValueItem
					= new QTreeWidgetItem(m_isovalueRootItem);
			newMaxIsoValueItem->setText(0,QString("Maximum: %1").
										arg((float)m_contour->GetMaximumIsovalue()));

			QTreeWidgetItem *newIsoValueRangeItem
					= new QTreeWidgetItem(m_isovalueRootItem);
			newIsoValueRangeItem->setText(0,QString("Range: %1").
										  arg((float)m_contour->GetIsovalueRange()));
		}
	}
	m_state->endGroup();
}

void QtTreeWidgetContourItem::addBoundingBoxProperties()
{
	m_state->beginGroup(KEY_BOUNDING_BOX);
	{
		//	Bounding box
		m_boundingBoxRootItem = new QTreeWidgetItem(this);
		m_boundingBoxRootItem->setText(0,QString("Bounding box"));
		m_boundingBoxRootItem->setExpanded(
					m_state->value("expanded",
								   BOUNDING_BOX_SUBTREE_EXPANDED_BY_DEFAULT).value<bool>());
		{
			QTreeWidgetItem *newBoundingBoxMinXItem
					= new QTreeWidgetItem(m_boundingBoxRootItem);
			newBoundingBoxMinXItem->setText(0,QString("X min: %1")
											.arg(m_contour->GetMinX()));

			QTreeWidgetItem *newBoundingBoxMaxXItem
					= new QTreeWidgetItem(m_boundingBoxRootItem);
			newBoundingBoxMaxXItem->setText(0,QString("X max: %1")
											.arg(m_contour->GetMaxX()));

			QTreeWidgetItem *newBoundingBoxMinYItem
					= new QTreeWidgetItem(m_boundingBoxRootItem);
			newBoundingBoxMinYItem->setText(0,QString("Y min: %1")
											.arg(m_contour->GetMinY()));

			QTreeWidgetItem *newBoundingBoxMaxYItem
					= new QTreeWidgetItem(m_boundingBoxRootItem);
			newBoundingBoxMaxYItem->setText(0,QString("Y max: %1")
											.arg(m_contour->GetMaxY()));

			QTreeWidgetItem *newBoundingBoxMinZItem
					= new QTreeWidgetItem(m_boundingBoxRootItem);
			newBoundingBoxMinZItem->setText(0,QString("Z min: %1")
											.arg(m_contour->GetMinZ()));

			QTreeWidgetItem *newBoundingBoxMaxZItem
					= new QTreeWidgetItem(m_boundingBoxRootItem);
			newBoundingBoxMaxZItem->setText(0,QString("Z max: %1")
											.arg(m_contour->GetMaxZ()));
		}
	}
	m_state->endGroup();
}

void QtTreeWidgetContourItem::addContourTreeProperties()
{
	m_state->beginGroup(KEY_CONTOUR_TREE);
	{
		//	Contour tree
		m_contourTreeRootItem = new QTreeWidgetItem(this);
		m_contourTreeRootItem->setText(0,QString("Contour tree"));
		m_contourTreeRootItem->setExpanded(
					m_state->value("expanded",
								   CONTOUR_TREE_SUBTREE_EXPANDED_BY_DEFAULT).value<bool>());
		{
			addSuperarcProperties(m_contourTreeRootItem);

			QTreeWidgetItem *newTopNodeItem
					= new QTreeWidgetItem(m_contourTreeRootItem);
			newTopNodeItem->setText(0,QString("Top Node ID: %1").
									arg(m_contour->GetTopSupernode()->GetId()));

			QTreeWidgetItem *newBottomNodeItem
					= new QTreeWidgetItem(m_contourTreeRootItem);
			newBottomNodeItem->setText(0,QString("Bottom Node ID: %1").
									   arg(m_contour->GetBottomSupernode()->GetId()));
		}
	}
	m_state->endGroup();
}


void QtTreeWidgetContourItem::addSuperarcProperties(QTreeWidgetItem *rootItem)
{
	m_state->beginGroup(KEY_SUPERARC);
	{

		//	Contour tree
		m_superarcRootItem = new QTreeWidgetItem(rootItem);
		m_superarcRootItem->setText(0,QString("Superarc"));
		m_superarcRootItem->setExpanded(
					m_state->value("expanded",
								   SUPERARC_SUBTREE_EXPANDED_BY_DEFAULT).value<bool>());
		{
			ContourTreeSuperarc *superarc = m_contour->GetSuperarc();

			QTreeWidgetItem *newArcItem
					= new QTreeWidgetItem(m_superarcRootItem);
			newArcItem->setText(0,QString("Arc ID: %1").arg(m_contour->GetSuperarc()->GetId()));

			QTreeWidgetItem *isValidItem
					= new QTreeWidgetItem(m_superarcRootItem);
			isValidItem->setText(0,QString("Valid: %1").arg(
									 superarc->IsValid() ? "true" : "false"));

			QTreeWidgetItem *isActiveItem
					= new QTreeWidgetItem(m_superarcRootItem);
			isActiveItem->setText(0,QString("Active: %1").arg(
									  superarc->IsActive() ? "true" : "false"));

			QTreeWidgetItem *isSelectedItem
					= new QTreeWidgetItem(m_superarcRootItem);
			isSelectedItem->setText(0,QString("Selected: %1").arg(
										superarc->IsSelected() ? "true" : "false"));

			QTreeWidgetItem *isHiddenItem
					= new QTreeWidgetItem(m_superarcRootItem);
			isHiddenItem->setText(0,QString("Hidden by user (Restorable): %1").arg(
									  superarc->IsHidden() ? "true" : "false"));

			QTreeWidgetItem *isOnBackQueueItem
					= new QTreeWidgetItem(m_superarcRootItem);
			isOnBackQueueItem->setText(0,QString("On back queue: %1").arg(
										   superarc->IsOnBackQueue() ? "true" : "false"));

			QTreeWidgetItem *wasActiveBeforePruningItem
					= new QTreeWidgetItem(m_superarcRootItem);
			wasActiveBeforePruningItem->setText(0,QString("Was active: %1").arg(
													superarc->WasActiveBeforePruning() ? "true" : "false"));

		}
	}
	m_state->endGroup();
}

QtTreeWidgetContourItem::~QtTreeWidgetContourItem()
{
	//	Don't forget to delete items...


	//	Update the persistance settings
	//	Deleting them will force a write to disk etc
	saveState();
	delete m_state;
}

QtTreeWidgetContourItem::QtTreeWidgetContourItem(QTreeWidgetItem *parent,
												 Contour *contour, QIcon icon)
	: QTreeWidgetItem(parent)
{
	//	Bit of a hack but works for now
	//	Write the settings for each contour to a separate ini file
	QString filenameHack =
			QString("object_%1.ini").arg(contour->GetSuperarc()->GetId());
	m_state = new QSettings(filenameHack, QSettings::IniFormat);

	m_contour = contour;
	m_parent = parent;

	setText(0, QString("Object %1").
			arg(contour->GetSuperarc()->GetId()));
	setIcon(0, icon);
	setExpanded(m_state->value("expanded",
							   CONTOURS_EXPANDED_BY_DEFAULT).value<bool>());
	setSelected(contour->IsSelected());
	setCheckState(0, contour->IsHiddenByUser() ? Qt::Unchecked
											  : Qt::Checked);

	addGeometryProperties();
	addMeshProperties();
	addIsovalueProperties();
	addContourTreeProperties();
	addBoundingBoxProperties();
}
