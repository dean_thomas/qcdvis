#include "QtMesh.h"
#include <cassert>
#include <exception>
#include <iostream>
#include "Globals/Functions.h"
#include "../Globals/QtOstream.h"

using namespace std;

///
/// \brief		Returns the number of Triangles making up the mesh (currently assuming the mesh is pre-triangulated)
/// \return
/// \author		Dean
/// \since		21-11-2016
///
size_t QtMesh::getNumberOfTriangles() const
{
	//	This will do for now, a better method would be to triangulate
	//	arbitatry polygon models
	assert(isTriangulated());

	return m_faces.count();
}

///
/// \brief		Returns the number of Polygons making up the mesh
/// \return
/// \author		Dean
/// \since		18-11-2016
///
size_t QtMesh::getNumberOfPolygons() const
{
	return m_faces.count();
}

///
/// \brief		Determines if the mesh is made up entirely of triangles or of arbitary polygons
/// \return
/// \author		Dean
/// \since		18-11-2016
///
bool QtMesh::isTriangulated() const
{
	auto face_list = m_faces;
	for (auto it = face_list.cbegin(); it != face_list.cend(); ++it)
	{
		if (!isTriangle(*it)) return false;
	}
	return true;
}

///
/// \brief		Returns the surface area of the mesh
/// \return
/// \author		Dean
/// \since		18-11-2016
///
double QtMesh::getSurfaceArea() const
{
	auto result = 0.0f;

	auto face_list = m_faces;
	for (auto it = face_list.cbegin(); it != face_list.cend(); ++it)
	{
		//	For now assume we are only dealing with triangular meshes
		assert (isTriangle(*it));

		//	Assuming we are dealing with a triangle extract the 3 vertex positions
		//	subtract one to make the indices zero based.
		auto idA = (it->cbegin()+0)->vertexId - 1;
		assert(idA < m_vertexLookup.size());
		auto idB = (it->cbegin()+1)->vertexId - 1;
		assert(idB < m_vertexLookup.size());
		auto idC = (it->cbegin()+2)->vertexId - 1;
		assert(idC < m_vertexLookup.size());

		//	Convert to cartesean coordinates
		auto vA = m_vertexLookup[idA];
		auto vB = m_vertexLookup[idB];
		auto vC = m_vertexLookup[idC];

		//	Compute the lengths of the edges
		auto eAB = vA.distanceToPoint(vB);
		assert(eAB >= 0.0f);
		auto eBC = vB.distanceToPoint(vC);
		assert(eBC >= 0.0f);
		auto eAC = vC.distanceToPoint(vA);
		assert(eAC >= 0.0f);

		//	Semi-perimiter is half the perimeter
		auto s = (eAB + eBC + eAC) / 2.0f;
		assert(s >= 0.0f);

		//	Area of this triangle
		auto area = sqrt(s*(s-eAB)*(s-eBC)*(s-eAC));

		//	Add to mesh total
		result += area;
	}
	return result;
}


///
/// \brief		Defines the colour that will be returned for the mesh at each
///				vertex if the obj file doesn't define it.
/// \param colour
/// \since		15-04-2016
/// \author		Dean
///
void QtMesh::setDefaultColour(const QColor& colour, const bool resetCache)
{
	m_defaultColour = colour;

	//	If we set the flag we can force recomputation of the stream to
	//	reflect the new colour
	if (resetCache) m_colourStreamCache.clear();
}

QtMesh::QtMesh()
{

}

QtMesh::QtMesh(const QString &filename)
{
	if (!LoadOBJ(filename))
	{
		fprintf(stderr, "Failed to load mesh %s.  In %s (file: %s, line: %ld).\n",
				filename.toStdString().c_str(), __func__, __FILE__, __LINE__);
		fflush(stderr);
	}
}

bool QtMesh::LoadOBJ(const QString &filename)
{
	//	Clear anything that might already be stored
	m_faces.clear();
	m_vertexLookup.clear();
	m_textureLookup.clear();
	m_normalLookup.clear();

	//	Also clear the cache's to force recomputation later...
	clearCache();

	QFile meshFile(filename);
	if (!meshFile.open(QIODevice::ReadOnly | QIODevice::Text)) return false;

	try
	{
		while (!meshFile.atEnd())
		{
			QString line = QString(meshFile.readLine());

			parseLine(line);
		}

		meshFile.close();
		/*
		cout << "Mesh loaded from: " << filename.toStdString()
			 << "Vertex count: " << m_vertexLookup.size() << "."
			 << "Normal count: " << m_normalLookup.size() << "."
			 << "Texture count: " << m_textureLookup.size() << "."
			 << "face count: " << m_faces.size() << "." << endl;
*/
		return true;
	}
	catch (invalid_argument& ex)
	{
		cerr << "There was a problem parsing the file: "
			 << filename.toStdString() << ": " << ex.what() << "." << endl;
		return false;
	}
}

bool QtMesh::parseLine(const QString &line)
{
	//	Blank line
	if (line == "") return true;

	//	Comment
	if (line.startsWith("#", Qt::CaseInsensitive)) return true;

	//	Texture roordinate
	//if (line.startsWith("vt", Qt::CaseInsensitive)) return parseTexture(line);

	//	Normal vector
	if (line.startsWith("vn", Qt::CaseInsensitive))
	{
		auto normal = parseNormal(line);
		m_normalLookup.push_back(normal);

		//cout << "parsed normal: " << normal << endl;
		return true;
	}

	//	Vertex position
	if (line.startsWith("v", Qt::CaseInsensitive))
	{
		auto vertex = parseVertex(line);
		m_vertexLookup.push_back(vertex);

		//cout << "parsed vertex: " << vertex << endl;
		return true;
	}

	//	Faces
	if (line.startsWith("f", Qt::CaseInsensitive))
	{
		auto face = parseFaceString(line);
		m_faces.push_back(face);

		//cout << "parsed face: " << face << endl;
		return true;
	}
}

QtMesh::Polygon QtMesh::parseFaceString(const QString &line) const
{
	Polygon result;

	//	Separate the top level tokens and discard the "f" token
	QStringList tokens = line.split(QRegExp("\\s"), QString::SkipEmptyParts);
	assert(tokens.first().toLower() == "f");
	tokens.removeFirst();

	//	for now we'll only parse triangle faces
	assert(tokens.size() == 3);

	try
	{
		for (auto i = 0; i < 3; ++i)
		{
			//	ie f v1/vt1/vn1 v2/vt2/vn2 v3/vt3/vn3
			//	Separate in indices for each triangle vertex
			result << parseVertexRecord(tokens[i]);
		}
	}
	catch (invalid_argument& ex)
	{
		cerr << "An exception was caught: " << ex.what() << endl;
		throw invalid_argument("Could not correctly parse the string: "
							   + line.toStdString());
	}

	//	We should have at least 3 records defined per polygon
	assert(result.size() >= 3);
	return result;
}

QtMesh::VertexRecord QtMesh::parseVertexRecord(const QString &token) const
{
	//	See:	https://en.wikipedia.org/wiki/Wavefront_.obj_file
	//	for a description of the .obj format [15-04-2016]
	const size_t TOKEN_V = 0;
	const size_t TOKEN_VT = 1;
	const size_t TOKEN_VN = 2;

	QStringList indexTokens = token.split("/", QString::SkipEmptyParts);
	size_t slashCount = token.count('/');

	//	Used for parsing the token
	bool resultV;
	bool resultVt;
	bool resultVn;
	unsigned long v;
	unsigned long vt;
	unsigned long vn;

	switch (indexTokens.size())
	{
		case 1:
			//	vertex

			//	Parse the vertex index
			v = indexTokens[TOKEN_V].toULong(&resultV);

			//	Return the record
			if (resultV)
				return { v, NO_ID, NO_ID };
			else
				throw invalid_argument("Couldn't parse vertex: "
									   + token.toStdString());
		case 2:
			if (slashCount == 1)
			{
				//	vertex / texture

				//	Parse the vertex index
				v = indexTokens[TOKEN_V].toULong(&resultV);
				vt = indexTokens[TOKEN_VT].toULong(&resultVt);

				//	Return the record
				if (resultV && resultVt)
					return { v, vt, NO_ID };
				else
					throw invalid_argument("Couldn't parse vertex / texture"
										   "pair: " + token.toStdString());
			}
			else if (slashCount == 2)
			{
				//	vertex / blank / normal
				//	Parse the vertex index and normal (note: the central
				//	'texture' element will be empty, so shift the 'VN'
				//	across by 1
				v = indexTokens[TOKEN_V].toULong(&resultV);
				vn = indexTokens[TOKEN_VN-1].toULong(&resultVn);

				//	Return the record
				if (resultV && resultVn)
					return { v, NO_ID, vn };
				else
					throw invalid_argument("Couldn't parse vertex / normal"
										   "pair: " + token.toStdString());
			}
		case 3:
			//	vertex / texture / normal

			//	Parse the vertex index
			v = indexTokens[TOKEN_V].toULong(&resultV);
			vt = indexTokens[TOKEN_VT].toULong(&resultVt);
			vn = indexTokens[TOKEN_VN].toULong(&resultVn);

			//	Return the record
			if (resultV && resultVn && resultVt)
				return { v, vt, vn };
			else
				throw invalid_argument("Couldn't parse "
									   "vertex / texture / normal"
									   "triple: " + token.toStdString());
		default:
			throw invalid_argument("Couldn't parse vertex record:"
								   + token.toStdString());
	}
}

bool QtMesh::parseTexture(const QString &line)
{
	QStringList tokens = line.split(QRegExp("\\s"), QString::SkipEmptyParts);

	if (tokens.size() == 3)
	{
		//	ie vt x y
		bool resultX, resultY;

		float x = tokens[1].toFloat(&resultX);
		float y = tokens[2].toFloat(&resultY);

		//	Detect conversion errors
		if (resultX && resultY)
		{
			m_textureLookup.push_back(QVector2D(x, y));
		}
		else
		{
			return false;
		}
	}
	else
	{
		fprintf(stderr, "Mesh loader is only able to parse 2D texture vectors"
						", detected vector is %ld dimensional.\n", tokens.size() - 1);
		fflush(stderr);

		return false;
	}
}

QVector3D QtMesh::parseNormal(const QString &line) const
{
	//	Split the string into the form { "vn", nx, ny, nz } and discard the
	//	"vn" token
	QStringList tokens = line.split(QRegExp("\\s"), QString::SkipEmptyParts);
	assert(tokens.first().toLower() == "vn");
	tokens.removeFirst();

	//	Should be left with 3 fields
	assert(tokens.size() == 3);

	//	ie vn x y z
	bool resultX, resultY, resultZ;

	//	Perform conversion
	float nx = tokens[0].toFloat(&resultX);
	float ny = tokens[1].toFloat(&resultY);
	float nz = tokens[2].toFloat(&resultZ);

	//	Detect conversion errors
	if (resultX && resultY && resultZ)
		return { nx, ny, nz };
	else
		throw invalid_argument("Couldn't parse normal: " + line.toStdString());
}

QVector3D QtMesh::parseVertex(const QString &line) const
{
	//	Split the string into the form { "v", vx, vy, vz } and discard the
	//	"v" token
	QStringList tokens = line.split(QRegExp("\\s"), QString::SkipEmptyParts);
	assert(tokens.first().toLower() == "v");
	tokens.removeFirst();

	//	Should be left with 3 fields
	assert(tokens.size() == 3);

	//	ie V x y z
	bool resultX, resultY, resultZ;

	//	Perform conversion
	float x = tokens[0].toFloat(&resultX);
	float y = tokens[1].toFloat(&resultY);
	float z = tokens[2].toFloat(&resultZ);

	//	Detect conversion errors
	if (resultX && resultY && resultZ)
		return { x, y, z };
	else
		throw invalid_argument("Couldn't parse vertex: " + line.toStdString());
}

///
/// \brief QtMesh::getVertexStream
/// \return
/// \since		15-04-2016
/// \author		Dean
///
QVector<QVector3D> QtMesh::getVertexStream() const
{
	//	If the cache is empty, we'll need to compute it first
	if (m_vertexStreamCache.empty())
	{
		QVector<QVector3D> tempStream;

		for (auto p = 0; p < m_faces.size(); ++p)
		{
			auto currentFace = m_faces[p];

			for (auto v = 0; v < currentFace.size(); ++v)
			{
				auto currentVertex = currentFace[v];

				auto correctedVertexId = currentVertex.vertexId - 1;
				assert(correctedVertexId <= m_vertexLookup.size());

				//	In obj format vertex / normal indices are index from 1
				tempStream << m_vertexLookup[correctedVertexId];
			}
		}
		m_vertexStreamCache = tempStream;
	}
	return m_vertexStreamCache;
}

///
/// \brief QtMesh::getColourStream
/// \return
/// \since		15-04-2016
/// \author		Dean
///
QVector<QVector4D> QtMesh::getColourStream() const
{
	if (m_colourStreamCache.empty())
	{
		QVector<QVector4D> tempStream;

		for (auto p = 0; p < m_faces.size(); ++p)
		{
			auto currentFace = m_faces[p];

			for (auto v = 0; v < currentFace.size(); ++v)
			{
				//	We don't have any way of storing vertex colours for now
				tempStream << QColorToQVector4D(m_defaultColour);
			}
		}
		m_colourStreamCache = tempStream;
	}
	return m_colourStreamCache;
}

///
/// \brief QtMesh::getNormalStream
/// \return
/// \since		15-04-2016
/// \author		Dean
///
QVector<QVector3D> QtMesh::getNormalStream() const
{
	if (m_normalStreamCache.empty())
	{
		QVector<QVector3D> tempStream;

		for (auto p = 0; p < m_faces.size(); ++p)
		{
			auto currentFace = m_faces[p];

			for (auto v = 0; v < currentFace.size(); ++v)
			{
				auto currentVertex = currentFace[v];

				auto correctedNormalId = currentVertex.normalId - 1;
				assert(correctedNormalId <= m_normalLookup.size());

				//	In obj format vertex / normal indices are index from 1
				tempStream << m_normalLookup[correctedNormalId];
			}
		}
		m_normalStreamCache = tempStream;
	}
	return m_normalStreamCache;
}

///
/// \brief QtMesh::clearCache
/// \since		18-04-2016
/// \author		Dean
///
void QtMesh::clearCache()
{
	m_vertexStreamCache.clear();
	m_normalStreamCache.clear();
	m_colourStreamCache.clear();
}
