///////////////////////////////////////////////////
//
//	Contour-tree based isosurfaces
//	Version 4.0 (QT-based)
//	copyright Hamish Carr
//	January, 2014
//
//	------------------------
//	LogCollapseWidget.cpp
//	------------------------
//	
//	A widget displaying the tree size / collapse
//	bound as a log-log plot
//	
///////////////////////////////////////////////////

#include "LogCollapseWidget.h"

LogCollapseWidget::LogCollapseWidget(QWidget *parent)
    : LogCollapseWidget(nullptr, parent)
{

}

LogCollapseWidget::LogCollapseWidget(DataModel *dataModel, QWidget *parent)
    : QGLWidget(parent)
{ // LogCollapseWidget()
    // store pointer to the model
    m_dataModel = dataModel;
} // LogCollapseWidget()

LogCollapseWidget::~LogCollapseWidget()
{ // ~LogCollapseWidget()
    // nothing yet
} // ~LogCollapseWidget()

// called when OpenGL context is set up
void LogCollapseWidget::initializeGL()
{ // LogCollapseWidget::initializeGL()
    // this is straightforward 2D rendering, so disable stuff we don't need
    glDisable(GL_LIGHTING);
    glDisable(GL_DEPTH_TEST);
    // background is white
    glClearColor(1.0, 1.0, 1.0, 1.0);
} // ContourTreeWidget::initializeGL()

// called every time the widget is resized
void LogCollapseWidget::resizeGL(int w, int h)
{ // LogCollapseWidget::resizeGL()
    // reset the viewport
    glViewport(0, 0, w, h);

    // set projection matrix to have range of 0.0-1.0 in x, y
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrtho(0.0, 1.0, 0.0, 1.0, -1.0, 1.0);
} // LogCollapseWidget::resizeGL()

// called every time the widget needs painting
void LogCollapseWidget::paintGL()
{ // LogCollapseWidget::paintGL()
    // set model view matrix based on stored translation, rotation &c.
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    // clear the buffer
    glClear(GL_COLOR_BUFFER_BIT);

    // draws the current flexible isosurface contours
    glColor3f(0.0, 0.0, 0.0);

    PlotLogCollapse();
} // LogCollapseWidget::paintGL()

///
/// \brief      Plots the log collapse curve (Rightmost window)
/// \author     Hamish
/// \since      25-11-2014
///
void LogCollapseWidget::PlotLogCollapse()
{
    if (m_dataModel == nullptr)
        return;


#define N_LOG_COLLAPSE_SAMPLES 120

    //	compute the range
//	float logNSuperarcs = log((float)m_dataModel->GetHeightField3d(0)->GetContourTree()->m_nNonEpsilonArcs);

    //	how many arcs we are looking for
    long targetNArcs;

    //	position of chosen point
    float xPosition, yPosition;

    glLineWidth(2.0);
    glBegin(GL_LINE_STRIP);
    {
        //	walk across a fixed number of times
        for (int i = 0; i <= N_LOG_COLLAPSE_SAMPLES; i++)
        {
            //	compute the ratio
            //	and compute the target # of arcs
            xPosition = (float) i / (float) N_LOG_COLLAPSE_SAMPLES;
  //          targetNArcs = (long) exp(logNSuperarcs * xPosition);

            //	compute y position
//			yPosition = log((float)m_dataModel->GetHeightField3d(0)->GetContourTree()->m_collapseBounds[targetNArcs]) / log((float)m_dataModel->GetHeightField3d(0)->GetVertexCount());
            //		printf("%2d, %6d: (%7.3f, %7.3f)\n", i, targetNArcs, xPosition, yPosition);

            //	plot the point
            glVertex2f(xPosition, yPosition);
        }
    }
    glEnd();
    glLineWidth(1.0);

    //	put a point at the intersection of the lines controlled by sliders
    glPointSize(5.0);
    glBegin(GL_POINTS);
    {
//		glVertex2f(m_dataModel->GetHeightField3d(0)->GetContourTree()->m_logTreeSize, m_dataModel->GetHeightField3d(0)->GetContourTree()->m_logPriorityBound);
    }
    glEnd();
    glPointSize(1.0);

    //	and put cross-hair lines through it
    glBegin(GL_LINES);
    {
//		glVertex2f(m_dataModel->GetHeightField3d(0)->GetContourTree()->m_logTreeSize, 0.0);
//		glVertex2f(m_dataModel->GetHeightField3d(0)->GetContourTree()->m_logTreeSize, 1.0);
//		glVertex2f(0.0, m_dataModel->GetHeightField3d(0)->GetContourTree()->m_logPriorityBound);
//		glVertex2f(1.0, m_dataModel->GetHeightField3d(0)->GetContourTree()->m_logPriorityBound);
    }
    glEnd();

}
