///////////////////////////////////////////////////
//
//	Contour-tree based isosurfaces
//	Version 4.0 (QT-based)
//	copyright Hamish Carr
//	January, 2014
//
//	------------------------
//	ColourSelectionWidget.h
//	------------------------
//	
//	A widget displaying colour choices
//	
///////////////////////////////////////////////////

#ifndef _HEADER_COLOUR_SELECTION_WIDGET_H
#define _HEADER_COLOUR_SELECTION_WIDGET_H

#include <QtOpenGL>
#include <QGLWidget>
#include <QMouseEvent>

#include "DataModel.h"

class ColourSelectionWidget : public QGLWidget
	{ // class ColourSelectionWidget
	Q_OBJECT
	public:	
	// the model - i.e. the flexible isosurface data
    DataModel *m_dataModel;

	// constructor
    ColourSelectionWidget(DataModel *dataModel, QWidget *parent);

	// destructor
	~ColourSelectionWidget();
												
	protected:
	// called when OpenGL context is set up
	void initializeGL();
	// called every time the widget is resized
    void resizeGL(int, int);
	// called every time the widget needs painting
	void paintGL();
	
	public:
	signals:
	void chooseColour(int whichColour);

	public slots:
	virtual void mousePressEvent(QMouseEvent *event);
	}; // class ColourSelectionWidget

#endif

