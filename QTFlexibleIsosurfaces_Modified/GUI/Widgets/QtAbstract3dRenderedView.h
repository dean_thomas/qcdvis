#ifndef QTABSTRACT3DRENDEREDVIEW_H
#define QTABSTRACT3DRENDEREDVIEW_H

#include <QGLWidget>
#include <QMatrix4x4>
#include <QGLShaderProgram>

class HeightField3D;
class QVector3D;
class QVector4D;
template <typename T>
class QVector;
class QtCamera;
class QGLShaderProgram;

#ifdef __linux__
	#define SHADER_DIR "/home/dean/qcdvis/QTFlexibleIsosurfaces_Modified/Shaders/"
	#define BASIC_SHADER_VS "/home/dean/qcdvis/QTFlexibleIsosurfaces_Modified/Shaders/basicShader.vs.glsl"
	#define BASIC_SHADER_FS "/home/dean/qcdvis/QTFlexibleIsosurfaces_Modified/Shaders/basicShader.fs.glsl"
#else
    #define SHADER_DIR "C:/Users/4thFloor/Documents/Qt/QtFlexibleIsosurfaces/QTFlexibleIsosurfaces_Modified/Shaders/"
    #define BASIC_SHADER_VS "C:/Users/4thFloor/Documents/Qt/QtFlexibleIsosurfaces/QTFlexibleIsosurfaces_Modified/Shaders/basicShader.vs.glsl"
    #define BASIC_SHADER_FS "C:/Users/4thFloor/Documents/Qt/QtFlexibleIsosurfaces/QTFlexibleIsosurfaces_Modified/Shaders/basicShader.fs.glsl"
#endif

class QtAbstract3dRenderedView : public QGLWidget
{
		Q_OBJECT
	public:
		struct MinMax
		{
			float min;
			float max;

			float range() const { return max - min; }
		};

		struct Axis
		{
			MinMax minMax;
			QString label;
		};

		struct Axis3
		{
			Axis xAxis;
			Axis yAxis;
			Axis zAxis;
		};

		explicit QtAbstract3dRenderedView(QWidget *parent = 0);

		void SetFloatingCameraPosition(const QQuaternion &camera, const float &radius);
		QVector3D GetFloatingCameraPosition() const;

		virtual QVector3D getLookAtPoint() const = 0;
	protected:
		struct BufferData
		{
				QVector<QVector3D> vertexArray;
				QVector<QVector3D> normalArray;
				QVector<QVector4D> colourArray;
				QVector<float> objectIdArray;
		};

		float m_tickFreqX = 5.0f;
		float m_tickFreqY = 5.0f;
		float m_tickFreqZ = 5.0f;

		QVector3D m_lightPosition = { -20.0, -20.0, -20.0 };


		QPoint projectToScreenSpace(const QVector3D &pos, const float width, const float height) const;
		QPoint projectToCustomSpace(const QVector3D &pos, const QMatrix4x4& projection_matrix) const;

		void initializeGL() override;
		void resizeGL(int w, int h) override;
		QSize sizeHint() const override;

		QMatrix4x4 m_mMatrix;
		QMatrix4x4 m_projectionMatrix;

		QGLShaderProgram m_basicShaderProgram;

		QtCamera *m_currentCamera = nullptr;
		QtCamera *m_floatingCamera = nullptr;

		void renderAxes(const Axis3& axes, const bool renderFarAxisLines, const float axisThickness);
		void renderAxisLabels(QPainter *painter, const Axis3& axes);

		void executeTriangleDrawingShader(QGLShaderProgram* const shaderProgram,
							const QVector3D& lightPosition,
							const bool alphaBlended,
							const BufferData& bufferData);

		void executeLineDrawingShader(const QVector<QVector3D>& vertexPositionArray,
									  const QVector<QVector4D>& vertexColourArray,
									  const float lineThickness = 1.0f);
		void executePointDrawingShader(const QVector<QVector3D>& vertexPositionArray,
									   const QVector<QVector4D>& vertexColourArray,
									   const float pointDiameter = 2.0f);

};

#endif // QTABSTRACT3DRENDEREDVIEW_H
