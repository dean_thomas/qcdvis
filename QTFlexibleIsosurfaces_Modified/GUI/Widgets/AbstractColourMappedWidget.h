#ifndef ABSTRACT_COLOUR_MAPPED_WIDGET_H
#define ABSTRACT_COLOUR_MAPPED_WIDGET_H

#include "Colour.h"
#include "Graphs/ContourTreeSupernode.h"
#include "Graphs/ContourTreeSuperarc.h"
#include "GUI/Widgets/QtColourRampWidget/QtColourRampWidget.h"

using Colour::Integer::RGB;

///	Provides some basic functionality by mapping values to colours.
/// This can then be shared by the ContourTreeWidget and SurfaceRender
/// widgets without code duplication using Object composition.
///
/// \author		Dean
/// \since		22-01-2015
///
class AbstractColourMappedWidget
{
	protected:
		explicit AbstractColourMappedWidget();
		QtColourRampWidget *m_negativeColourRamp = nullptr;
		QtColourRampWidget *m_positiveColourRamp = nullptr;
	public:
		QColor calculateNodeColour(ContourTreeSupernode node) const;
		QColor calculateArcColour(ContourTreeSuperarc *arc,
									ContourTreeSupernode *topNode,
									ContourTreeSupernode *bottomNode) const;

		void SetColourRamps(QtColourRampWidget *positiveColourRamp,
							QtColourRampWidget *negativeColourRamp);

};

#endif // ABSTRACTCOLOURMAPPEDWIDGET_H
