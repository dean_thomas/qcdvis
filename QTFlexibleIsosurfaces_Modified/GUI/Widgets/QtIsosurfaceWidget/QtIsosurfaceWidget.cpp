#include <GL/glew.h>

#include "QtIsosurfaceWidget.h"
#include "HeightField3D.h"
#include "Contour/Storage/ContourList.h"
#include "Contour/Storage/Contour.h"
#include "Graphs/ContourTree.h"
#include "Graphs/ReebGraph.h"
#include "../Globals/Functions.h"
#include <QGLFramebufferObject>
#include "../Globals/QtOstream.h"
#include "../Globals/QtFunctions.h"
#include <QGLFunctions>

using namespace std;

#ifdef _MSC_VER
#define __PRETTY_FUNCTION__ __FUNCSIG__
#endif

#define DISABLE_DEBUG_OUTPUT

float QtIsosurfaceWidget::calculateSubviewSize() const
{
	auto smallestDim = width() < height() ? width() : height();

	return smallestDim * ((float)m_subViewPercentage / 100.0);
}

///
/// \brief		If set to true the edge connectivity wireframe will only
///				render the red edges around a hole in the mesh.
/// \param value
///
void QtIsosurfaceWidget::SetRenderEdgeConnectivityForHolesOnly(const bool value)
{
	m_renderMeshHolesWireframeOnly = value;
	update();
}


///
/// \brief QtIsosurfaceWidget::SetRenderSkeletonGlyphs
/// \param value
/// \since		30-03-2016
/// \author		Dean
///
void QtIsosurfaceWidget::SetRenderSkeletonGlyphs(const bool value)
{
	//	Use glyph overlays to show vertices in the CT / Reeb skeleton
	m_renderSkeletonGlyphs = value;
	update();
}

///
/// \brief		Creates a screenshot at the desired resolution
/// \param value
/// \author		Dean
/// \since		24-03-2016
///
QImage QtIsosurfaceWidget::ObtainScreenShot(const QSize& dimensions,
											const QColor& backgroundColour)
{
	assert(m_heightField != nullptr);
	makeCurrent();

	auto func_capture_rendered_image = [&]()
	{
		//	Create a back-buffer for the image to be drawn on and make it the
		//	current context
		QGLFramebufferObject frameBuffer(dimensions.width(),
										 dimensions.height(),
										 QGLFramebufferObject::Depth);
		frameBuffer.bind();

		//	Setup the viewport and projection matrix to fit the desired screenshot
		//	resolution
		glViewport(0, 0, dimensions.width(), dimensions.height());
		m_projectionMatrix.setToIdentity();
		m_projectionMatrix.perspective(60.0, (float)dimensions.width() / dimensions.height(), 0.001, 1000);

		//	Render the data and the axis labels
		renderPipeline(backgroundColour);

		//	Switch bark to rendering on the main widget
		frameBuffer.release();

		//	And return as an image
		return frameBuffer.toImage();
	};

	//	First render and capture the main view
	auto image = func_capture_rendered_image();

	//	So that we can pain any required overlays on top
	QPainter painter(&image);
	if ((m_contourTreeSkeleton) && (m_renderSkeletonGlyphs))
	{
		renderSkeletonGlyphOverlay(&painter, m_heightField->GetContourTree(),
								   dimensions.width(), dimensions.height());
	}

	if ((m_reebGraphSkeleton) && (m_renderSkeletonGlyphs))
	{
		renderSkeletonGlyphOverlay(&painter, m_heightField->GetReebGraph(),
								   dimensions.width(), dimensions.height());
	}

	makeCurrent();

	//	Restore the previous viewport / projection settings
	glViewport(0,0, width(), height());
	m_projectionMatrix.setToIdentity();
	m_projectionMatrix.perspective(60.0, (float)width() / (float)height(), 0.001, 1000);

	return image;
}

///
/// \brief QtIsosurfaceWidget::SetNestedContourCount
/// \param value
/// \author		Dean
/// \since		24-03-2016
///
void QtIsosurfaceWidget::SetNestedContourCount(const size_t value)
{
	m_numberOfNestedContours = value;
	update();
}

///
/// \brief QtIsosurfaceWidget::SetNestedContours
/// \param value
/// \author		Dean
/// \since		24-03-2016
///
void QtIsosurfaceWidget::SetNestedContours(const bool value)
{
	m_nestedContours = value;
	update();
}

///
/// \brief QtIsosurfaceWidget::SetAlphaBlended
/// \param value
/// \author		Dean
/// \since		24-03-2016
///
void QtIsosurfaceWidget::SetAlphaBlended(const bool value)
{
	m_alphaBlended = value;
	update();
}

///
/// \brief		Sets contour tree / reeb graph skeleton rendering in the view
/// \param value
/// \author		Dean
/// \since		22-03-2016
///
void QtIsosurfaceWidget::SetRenderContourTreeSkeleton(const bool value)
{
	m_contourTreeSkeleton = value;
	update();
}

///
/// \brief		Creates a buffered image for drawing the primary view using
///				a separate back buffer.
/// \param width
/// \param height
/// \return
/// \author		Dean
/// \since		23-03-2016
///
QImage QtIsosurfaceWidget::renderMainview(const size_type& width,
										  const size_type& height)
{
	assert(m_heightField != nullptr);
	makeCurrent();

	//	Create a back-buffer for the image to be drawn on and make it the
	//	current context
	QGLFramebufferObject frameBuffer(width, height, QGLFramebufferObject::Depth);
	frameBuffer.bind();

	//	Render the data and the axis labels
	renderPipeline(Qt::transparent);

	//	Switch bark to rendering on the main widget
	frameBuffer.release();

	return frameBuffer.toImage();
}

///
/// \brief QtIsosurfaceWidget::renderSubview
/// \param subViewWidth
/// \param subViewHeight
/// \param cameraPreset
/// \return
/// \since		01-12-2015
/// \author		Dean
///
QImage QtIsosurfaceWidget::renderSubview(const size_type& subViewWidth,
										 const size_type& subViewHeight,
										 const CameraPreset& cameraPreset)
{
	assert(m_heightField != nullptr);
	makeCurrent();

	//	We'll want to restore this after
	QtCamera* tempCamera = m_currentCamera;

	//	Create an off screen pixmap and make it the current render context
	QGLFramebufferObject frameBuffer(subViewWidth, subViewHeight,
									 QGLFramebufferObject::Depth);
	frameBuffer.bind();

	//	We want there to be a border region around the grid that is 20% of the
	//	image width
	const auto SCALE_PROJECTION_X = 1.05f;
	const auto SCALE_PROJECTION_Y = 1.05f;

	//	Calculate the range of each axis in the volume
	auto rangeAxisX = m_heightField->GetMaxX() - 0;
	auto rangeAxisY = m_heightField->GetMaxY() - 0;
	auto rangeAxisZ = m_heightField->GetMaxZ() - 0;

	//	The actual range of our projection will run from [-range/2..+range/2]
	//	as the camera will sit in the centre
	auto projectionMinX = -1.0f;
	auto projectionMaxX = +1.0f;
	auto projectionMinY = -1.0f;
	auto projectionMaxY = +1.0f;

	//	Map the 3D volume onto a 2D plane
	switch (cameraPreset)
	{
	case CameraPreset::FACE_CAM_F2_MIN_X:
	case CameraPreset::FACE_CAM_F3_MAX_X:
		//	Looking at a YZ plane
		projectionMinX = -1.0 * (rangeAxisZ / 2.0);
		projectionMaxX = +1.0 * (rangeAxisZ / 2.0);
		projectionMinY = -1.0 * (rangeAxisY / 2.0);
		projectionMaxY = +1.0 * (rangeAxisY / 2.0);
		break;
	case CameraPreset::FACE_CAM_F1_MIN_Y:
	case CameraPreset::FACE_CAM_F4_MAX_Y:
		//	Looking at a XZ plane
		projectionMinX = -1.0 * (rangeAxisX / 2.0);
		projectionMaxX = +1.0 * (rangeAxisX / 2.0);
		projectionMinY = -1.0 * (rangeAxisZ / 2.0);
		projectionMaxY = +1.0 * (rangeAxisZ / 2.0);
		break;
	case CameraPreset::FACE_CAM_F0_MIN_Z:
	case CameraPreset::FACE_CAM_F5_MAX_Z:
		//	Looking at a XY plane
		projectionMinX = -1.0 * (rangeAxisX / 2.0);
		projectionMaxX = +1.0 * (rangeAxisX / 2.0);
		projectionMinY = -1.0 * (rangeAxisY / 2.0);
		projectionMaxY = +1.0 * (rangeAxisY / 2.0);
		break;
	}

	//	Do a rescale to give the borders
	projectionMinX *= SCALE_PROJECTION_X;
	projectionMaxX *= SCALE_PROJECTION_X;
	projectionMinY *= SCALE_PROJECTION_Y;
	projectionMaxY *= SCALE_PROJECTION_Y;

	//	Set up the viewport and projection matrix (we don't want perspective
	//	distortion).
	m_projectionMatrix.setToIdentity();
	m_projectionMatrix.ortho(projectionMinX, projectionMaxX,
							 projectionMinY, projectionMaxY,
							 0.001, 1000);
	glViewport(0, 0, subViewWidth, subViewHeight);

	//	Select the relevant camera
	auto camera = m_presetCamera.find(cameraPreset);
	assert(camera != m_presetCamera.end());
	m_currentCamera = camera->second;

	//	Render the data and the axis labels
	renderPipeline(Qt::transparent);

	//	Switch bark to rendering on the main widget
	frameBuffer.release();

	auto result = frameBuffer.toImage();

	//	Restore the previous camera, projection and viewport
	m_currentCamera = tempCamera;
	m_projectionMatrix.setToIdentity();
	m_projectionMatrix.perspective(60.0, (float)width() / (float)height(), 0.001, 1000);
	glViewport(0, 0, width(), height());

	return result;
}

void QtIsosurfaceWidget::SetColourTable(const LookupTable &value)
{
	m_colourTable = value;

	update();
}

///
/// \brief QtIsosurfaceWidget::SetIsosurfaceColour
/// \param value
/// \since		26-11-2015
///	\author		Dean
///
void QtIsosurfaceWidget::SetIsosurfaceColour(const QColor& value)
{
	m_contourDefaultColour = value;

	update();
}

///
/// \brief QtIsosurfaceWidget::SetIsosurfaceColour
/// \param value
/// \since		26-11-2015
///	\author		Dean
///
void QtIsosurfaceWidget::SetIsosurfaceColourPositive(const QColor& value)
{
	m_contourDefaultColourPositive = value;

	update();
}

///
/// \brief QtIsosurfaceWidget::SetIsosurfaceColour
/// \param value
/// \since		26-11-2015
///	\author		Dean
///
void QtIsosurfaceWidget::SetIsosurfaceColourNegative(const QColor& value)
{
	m_contourDefaultColourNegative = value;

	update();
}

///
/// \brief QtIsosurfaceWidget::SetColourMode
/// \param value
/// \since		26-11-2015
///	\author		Dean
///
void QtIsosurfaceWidget::SetColourMode(const ColourMode &value)
{
	m_colourMode = value;

	update();
}

///
/// \brief		Shows a dialog to allow the user to change the origin of
///				the data
/// \since		22-09-2015
/// \author		Dean
///
void QtIsosurfaceWidget::showDialog()
{
#ifndef DISABLE_DEBUG_OUTPUT
	cout << __PRETTY_FUNCTION__ << endl;
#endif
	QtOriginOffsetDialog offsetDialog;

	unsigned long xMax = 0, xCurrent = 0;
	unsigned long yMax = 0, yCurrent = 0;
	unsigned long zMax = 0, zCurrent = 0;
	unsigned long tMax = 0, tCurrent = 0;

	//	Use to go back from 4 values to 3 in the correct order
	std::map<char, char> axisMappings;

	//	WARNING: horrible hacky code to map the 3D height field to a 4D
	//	hypervolume to setup our offset sliders [22-09-2015]
	switch (m_heightField->GetAxisLabelX())
	{
	case 'x':
		xMax = m_heightField->GetMaxX();
		xCurrent = m_heightField->GetTranslationX();
		axisMappings['x'] = 'x';
		break;
	case 'y':
		yMax = m_heightField->GetMaxX();
		yCurrent = m_heightField->GetTranslationX();
		axisMappings['y'] = 'x';
		break;
	case 'z':
		zMax = m_heightField->GetMaxX();
		zCurrent = m_heightField->GetTranslationX();
		axisMappings['z'] = 'x';
		break;
	case 't':
		tMax = m_heightField->GetMaxX();
		tCurrent = m_heightField->GetTranslationX();
		axisMappings['t'] = 'x';
		break;
	default:
		cerr << "Unrecognized axis label in: " << __func__ << endl;
		break;
	}
	switch (m_heightField->GetAxisLabelY())
	{
	case 'x':
		xMax = m_heightField->GetMaxY();
		xCurrent = m_heightField->GetTranslationY();
		axisMappings['x'] = 'y';
		break;
	case 'y':
		yMax = m_heightField->GetMaxY();
		yCurrent = m_heightField->GetTranslationY();
		axisMappings['y'] = 'y';
		break;
	case 'z':
		zMax = m_heightField->GetMaxY();
		zCurrent = m_heightField->GetTranslationY();
		axisMappings['z'] = 'y';
		break;
	case 't':
		tMax = m_heightField->GetMaxY();
		tCurrent = m_heightField->GetTranslationY();
		axisMappings['t'] = 'y';
		break;
	default:
		cerr << "Unrecognized axis label in: " << __func__ << endl;
		break;
	}
	switch (m_heightField->GetAxisLabelZ())
	{
	case 'x':
		xMax = m_heightField->GetMaxZ();
		xCurrent = m_heightField->GetTranslationZ();
		axisMappings['x'] = 'z';
		break;
	case 'y':
		yMax = m_heightField->GetMaxZ();
		yCurrent = m_heightField->GetTranslationZ();
		axisMappings['y'] = 'z';
		break;
	case 'z':
		zMax = m_heightField->GetMaxZ();
		zCurrent = m_heightField->GetTranslationZ();
		axisMappings['z'] = 'z';
		break;
	case 't':
		tMax = m_heightField->GetMaxZ();
		tCurrent = m_heightField->GetTranslationZ();
		axisMappings['t'] = 'z';
		break;
	default:
		cerr << "Unrecognized axis label in: " << __func__ << endl;
		break;
	}
#ifndef DISABLE_DEBUG_OUTPUT
	cout << "Dimensions of 3D heightfield: (";
	cout << xMax << ", " << yMax << ", " << zMax << ", " << tMax;
	cout << ")" << endl;
	cout << "Current offset of 3D heightfield: (";
	cout << xCurrent << ", " << yCurrent << ", " << zCurrent << ", " << tCurrent;
	cout << ")" << endl;
	for (auto it = axisMappings.cbegin(); it != axisMappings.cend(); ++it)
	{
		cout << it->first << " -> " << it->second << endl;
	}
#endif
	offsetDialog.SetMaximumXYZT(xMax, yMax, zMax, tMax);
	offsetDialog.SetValueXYZT(xCurrent, yCurrent, zCurrent, tCurrent);


	if (offsetDialog.exec() == QDialog::Accepted)
	{
		//	First we need to map our 4 potential values down to their 3D
		//	equivilants
		unsigned int tX = 0;
		unsigned int tY = 0;
		unsigned int tZ = 0;

		auto mapX = axisMappings.find('x');
		auto mapY = axisMappings.find('y');
		auto mapZ = axisMappings.find('z');
		auto mapT = axisMappings.find('t');

		if (mapX != axisMappings.cend())
		{
			switch (mapX->second)
			{
			case 'x':
				tX = offsetDialog.GetValueX();
				break;
			case 'y':
				tY = offsetDialog.GetValueX();
				break;
			case 'z':
				tZ = offsetDialog.GetValueX();
				break;
			}
		}

		if (mapY != axisMappings.cend())
		{
			switch (mapY->second)
			{
			case 'x':
				tX = offsetDialog.GetValueY();
				break;
			case 'y':
				tY = offsetDialog.GetValueY();
				break;
			case 'z':
				tZ = offsetDialog.GetValueY();
				break;
			}
		}

		if (mapZ != axisMappings.cend())
		{
			switch (mapZ->second)
			{
			case 'x':
				tX = offsetDialog.GetValueZ();
				break;
			case 'y':
				tY = offsetDialog.GetValueZ();
				break;
			case 'z':
				tZ = offsetDialog.GetValueZ();
				break;
			}
		}

		if (mapT != axisMappings.cend())
		{
			switch (mapT->second)
			{
			case 'x':
				tX = offsetDialog.GetValueT();
				break;
			case 'y':
				tY = offsetDialog.GetValueT();
				break;
			case 'z':
				tZ = offsetDialog.GetValueT();
				break;
			}
		}
#ifndef DISABLE_DEBUG_OUTPUT
		cout << "Desired offset is (";
		cout << tX << ", " << tY << ", " << tZ;
		cout << ")" << endl;
#endif
		m_heightField->SetTranslation(tX, tY, tZ);
	}
}

///
/// \brief QtIsosurfaceWidget::setCamera
/// \param camera
/// \since		08-12-2015
/// \author		Dean
///
void QtIsosurfaceWidget::setCamera(QtCamera* camera)
{
	assert (camera != nullptr);

	m_currentCamera = camera;
	update();
}

///
/// \brief		Creates a custom ContextMenu for this widget
/// \since		22-09-2015
/// \author		Dean
///
void QtIsosurfaceWidget::constructContextMenu()
{
#ifndef DISABLE_DEBUG_OUTPUT
	cout << __PRETTY_FUNCTION__ << endl;
#endif
	//m_contextMenu.contextMenu.reset(new QMenu);

	auto offsetAction = new QAction("Change offset...", this);
	connect(offsetAction, SIGNAL(triggered(bool)),
			this, SLOT(showDialog()));
	addAction(offsetAction);

	//	Manually relabel axes...
	QAction *actionLabelAxis = new QAction("Re-label axis", this);
	connect(actionLabelAxis, &QAction::triggered, this,
			[actionLabelAxis, this]()
	{
		//	New label for x axis
		auto newX = QInputDialog::getText(this, "New label for x axis", "x label");
		this->m_heightField->SetAxisLabelX(newX.toStdString()[0]);

		//	New label for y axis
		auto newY = QInputDialog::getText(this, "New label for y axis", "y label");
		this->m_heightField->SetAxisLabelY(newY.toStdString()[0]);

		//	New label for z axis
		auto newZ = QInputDialog::getText(this, "New label for z axis", "z label");
		this->m_heightField->SetAxisLabelZ(newZ.toStdString()[0]);
	});
	addAction(actionLabelAxis);

	//	Add a separator for the preset cameras
	QAction *presetCameraSeparator = new QAction(this);
	presetCameraSeparator->setSeparator(true);
	addAction(presetCameraSeparator);

	//	Add an entry for each of the presets
	for (auto it = m_presetCamera.cbegin(); it != m_presetCamera.cend(); ++it)
	{
		QtCamera* currentCamera = it->second;

		auto cameraAction = new QAction(QString::fromStdString(currentCamera->GetCameraName()),
										this);
		QObject::connect(cameraAction, &QAction::triggered,
						 [=]()
		{
#ifndef DISABLE_DEBUG_OUTPUT
			cout << "User clicked camera: " << currentCamera->GetCameraName() << endl;
#endif
			//	Set the new camera
			setCamera(currentCamera);
		});

		addAction(cameraAction);
	}

	//	Add a separator for the floating cameras
	QAction *floatingCameraSeparator = new QAction(this);
	floatingCameraSeparator->setSeparator(true);
	addAction(floatingCameraSeparator);

	//	The global arc-ball camera
	auto globalCameraAction = new QAction("Global arc-ball camera", this);
	QObject::connect(globalCameraAction, &QAction::triggered,
					 [&]()
	{
#ifndef DISABLE_DEBUG_OUTPUT
		cout << "User global camera: " << m_floatingCamera << endl;
#endif
		//	Set the new camera
		setCamera(m_floatingCamera);
	});
	addAction(globalCameraAction);

	//	The local arc-ball camera
	auto localCameraAction = new QAction("Local arc-ball camera", this);
	QObject::connect(localCameraAction, &QAction::triggered,
					 [&]()
	{
#ifndef DISABLE_DEBUG_OUTPUT
		cout << "User selected local camera" << endl;
#endif
		//	Set the new camera
		//setCamera(m_floatingCamera);
	});
	addAction(localCameraAction);

	setContextMenuPolicy(Qt::ActionsContextMenu);

}

#ifdef CALCULATE_MOMENTS
void QtIsosurfaceWidget::renderMoments()
{
#define MIN_VOL 0.01

	if (m_heightField == nullptr) return;
	if (m_heightField->GetContourTree() == nullptr) return;

	//	Get a list of contours for rendering
	vector<Contour*> renderableContours;
	if (m_nestedContours)
	{
		//	Multiple isovalues in a range
		renderableContours = getRenderableContours(m_nested_contour_range_min,
												   m_nested_contour_range_max,
												   m_numberOfNestedContours);
	}
	else
	{
		//	Just a single isovalue
		renderableContours = getRenderableContours(m_targetIsovalue);
	}

	qDebug() << "Renderable contour count " << renderableContours.size();

	//	Loop over the number of visible contours and find the ones that are
	//	actual ready to be rendered.
	for (auto it = renderableContours.cbegin(); it != renderableContours.cend(); ++it)
	{
		//	Access the current contour (if ready)
		Contour* currentContour = *it;

		//	Don't count contours still being constructed
		if (currentContour != nullptr)
		{
			printf("Volume is: %f.\n", abs(currentContour->GetUnscaledVolume()));
			fflush(stdout);

			//	Ignore contours marked as hidden or where the stats are not
			//	ready
			if ((!currentContour->IsHiddenByUser())
					&& (currentContour->StatisticsCalculated()))
			{
				//	Ignore potential noise
				if (abs(currentContour->GetUnscaledVolume()) > MIN_VOL)
				{
					QVector3D com = QVector3D(currentContour->GetCentreOfMass().GetX(),
											  currentContour->GetCentreOfMass().GetY(),
											  currentContour->GetCentreOfMass().GetZ());

					qDebug() << "Object has centre of mass at " << com;

					QVector3D principle = QVector3D(currentContour->GetPrincipleAxis().eigenVector(0),
													currentContour->GetPrincipleAxis().eigenVector(1),
													currentContour->GetPrincipleAxis().eigenVector(2));
					QVector3D secondary = QVector3D(currentContour->GetSecondaryAxis().eigenVector(0),
													currentContour->GetSecondaryAxis().eigenVector(1),
													currentContour->GetSecondaryAxis().eigenVector(2));
					QVector3D tertiary = QVector3D(currentContour->GetTertiaryAxis().eigenVector(0),
												   currentContour->GetTertiaryAxis().eigenVector(1),
												   currentContour->GetTertiaryAxis().eigenVector(2));

					printf("COM: [%f %f %f].\n",com.x(),com.y(),com.z());
					printf("principle: [%f %f %f].\n",principle.x(),principle.y(),principle.z());
					printf("secondary: [%f %f %f].\n",secondary.x(),secondary.y(),secondary.z());
					printf("tertiary: [%f %f %f].\n",tertiary.x(),tertiary.y(),tertiary.z());
					fflush(stdout);

					BufferData buffer;

					//	primary axis
					buffer.vertexArray.push_back(com);
					buffer.vertexArray.push_back(com + principle);
					buffer.colourArray.push_back(QVector3D(1.0, 0.0, 0.0));
					buffer.colourArray.push_back(QVector3D(1.0, 0.0, 0.0));

					//	secondary axis
					buffer.vertexArray.push_back(com);
					buffer.vertexArray.push_back(com + secondary);
					buffer.colourArray.push_back(QVector3D(0.0, 1.0, 0.0));
					buffer.colourArray.push_back(QVector3D(0.0, 1.0, 0.0));

					//	tertiary axis
					buffer.vertexArray.push_back(com);
					buffer.vertexArray.push_back(com + tertiary);
					buffer.colourArray.push_back(QVector3D(0.0, 0.0, 1.0));
					buffer.colourArray.push_back(QVector3D(0.0, 0.0, 1.0));

					glEnable(GL_DEPTH_TEST);
					glLineWidth(m_axisThickness);

					//	Our matrices to be send to the GPU
					//QMatrix4x4 mMatrix;
					QMatrix4x4 vMatrix = m_currentCamera->GetViewMatrix();
					QMatrix4x4 mvMatrix = vMatrix * m_mMatrix;
					QMatrix3x3 normalMatrix = mvMatrix.normalMatrix();

					//
					//QVector3D lightPos(5.0, 5.0, 5.0);

					//	Bind our shader program
					m_basicShaderProgram.bind();

					//	Pre-compute our matrices in various forms and send them as uniforms
					m_basicShaderProgram.setUniformValue("uModelViewMatrix", mvMatrix);
					m_basicShaderProgram.setUniformValue("uProjectionMatrix", m_projectionMatrix);
					m_basicShaderProgram.setUniformValue("uNormalMatrix", normalMatrix);

					//	Send the vertex array
					m_basicShaderProgram.setAttributeArray("vPosition", buffer.vertexArray.constData());
					m_basicShaderProgram.enableAttributeArray("vPosition");

					//	Send the colour array
					m_basicShaderProgram.setAttributeArray("vColor", buffer.colourArray.constData());
					m_basicShaderProgram.enableAttributeArray("vColor");

					//	If there is data to be rendered, render it...
					if (buffer.vertexArray.size() > 0)
						glDrawArrays(GL_LINES, 0, buffer.vertexArray.size());

					//	Clean up the shader program
					m_basicShaderProgram.disableAttributeArray("vPosition");
					m_basicShaderProgram.disableAttributeArray("vColor");
					m_basicShaderProgram.release();
				}
			}
		}


	}
}
#endif

void QtIsosurfaceWidget::IncrementCamera()
{
	++m_presetCameraIndex;

	//if (m_presetCameraIndex >= m_presetCamera.size()) m_presetCameraIndex = 0;
	//m_currentCamera = m_presetCamera[m_presetCameraIndex];

	//printf("Camera in use is %s.\n", m_presetCamera[m_presetCameraIndex]->GetCameraName().c_str());
	//fflush(stdout);

	update();
}

QtIsosurfaceWidget::QtIsosurfaceWidget(QWidget *parent)
	: QtAbstract3dRenderedView(parent)
{
	//	HACK: for now the cameras require a heightfield to be set, so the
	//	context menu must be created once a heightfield is available.  It should
	//	execute here though
	//constructContextMenu();
}

QtIsosurfaceWidget::~QtIsosurfaceWidget()
{
	delete m_mainShaderProgram;

	delete m_floatingCamera;
}

///
/// \brief		Renders skeleton graph vertices as 2d glyphs
/// \param painter
/// \since		30-03-2016
/// \author		Dean
///
template <typename V, typename A>
void QtIsosurfaceWidget::renderSkeletonGlyphOverlay(QPainter* painter,
													const TreeBase<V, A>* tree,
													const float overlay_width,
													const float overlay_height)
{
	//	We may not have a reeb graph available
	if (tree == nullptr) return;

	const size_t NODE_RADIUS = 8;
	const QColor NODE_OUTLINE_COLOUR = QColor(Qt::black);

	const QColor NODE_HIGHLIGHTED_FILL_COLOUR = QColor(Qt::red);
	const QColor NODE_SELECTED_FILL_COLOUR = QColor(Qt::green);
	const QColor NODE_DEFAULT_FILL_COLOUR = QColor(Qt::gray);

	//	Get the contour tree
	//auto contourTree = m_heightField->GetContourTree();
	assert(tree != nullptr);

	//	Setup our drawing tools
	QPen pen(NODE_OUTLINE_COLOUR);
	painter->setPen(pen);
	QBrush brush(NODE_DEFAULT_FILL_COLOUR);
	QTextOption textOptions(Qt::AlignVCenter | Qt::AlignHCenter);

	//	Use this in a bit...
	auto supernnodeColourTransferFunction =
			[&](V* const supernode) -> QColor
	{
		assert(supernode != nullptr);

		//	1.	Is highlighted
		if (supernode->IsHighlighted()) return NODE_HIGHLIGHTED_FILL_COLOUR;

		//	2.	Is selected
		if (supernode->IsSelected()) return NODE_SELECTED_FILL_COLOUR;

		//	3.	Use default colour
		return NODE_DEFAULT_FILL_COLOUR;
	};

	//	Loop over all the edges/nodes in the contour tree
	for (auto it = tree->Supernodes_cbegin();
		 it != tree->Supernodes_cend();
		 ++it)
	{
		auto supernode = *it;
		assert(supernode != nullptr);

		//	Only draw nodes that relate to non-epsilion edges
		if (supernode->IsValid())
		{
			//	Get position in 3d space
			auto pos = supernode->GetPosition();
			auto pt = projectToScreenSpace({static_cast<float>(get<0>(pos)),
											static_cast<float>(get<1>(pos)),
											static_cast<float>(get<2>(pos))},
										   static_cast<float>(overlay_width),
										   static_cast<float>(overlay_height));

			//	Compute position in 2d space
			QRect boundingBox = QRect(QPoint(pt.x() - NODE_RADIUS, pt.y() - NODE_RADIUS),
									  QPoint(pt.x() + NODE_RADIUS, pt.y() + NODE_RADIUS));

			//	Get the colour required for the brush and set
			auto brushColour = supernnodeColourTransferFunction(supernode);
			brush.setColor(brushColour);
			painter->setBrush(brush);

			//	Do the drawing
			painter->drawEllipse(boundingBox);
			painter->drawText(boundingBox, QString("%1").arg(supernode->GetId()), textOptions);
		}
	}
}

///
/// \brief		Handles calls from the OS to redraw the widget
///				(replaces former paintGL() method).
/// \since		14-07-2015
/// \author		Dean
///
void QtIsosurfaceWidget::paintEvent(QPaintEvent *event)
{
	makeCurrent();

	//	Clear the widget to the background colour
	qglClearColor(m_backgroundColor);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//	Get the smallest dimension available for the overlay (most of the
	//	time this will be the height).
	auto subViewWidth = calculateSubviewSize();
	auto subViewHeight = calculateSubviewSize();

	//#defined SUPRESS_OUTPUT
	//glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

	//glViewport(0,0,width(), height());

	//render3dView(m_backgroundColor);

	glFrontFace(GL_CW);
	//glCullFace(GL_BACK);
	//glEnable(GL_CULL_FACE);

	//	Obtain a transparent background image of the main view
	auto mainViewImage = renderMainview(width(), height());


	//	Slice through the YZ plane
	auto xMinOrthoImage = renderSubview(subViewWidth, subViewHeight, CameraPreset::FACE_CAM_F2_MIN_X);
	ImageOverlay topRightOverlay = { xMinOrthoImage,
									 {
										 QString("%1").arg(m_heightField->GetAxisLabelZ()),
										 m_heightField->GetMaxZ() + 1,
										 1
									 },
									 { QString("%1").arg(m_heightField->GetAxisLabelY()),
									   m_heightField->GetMaxY() + 1,
									   1}
								   };

	//	Slice through XZ plane
	//	Use the x-min for now, whilst the y_min is broken
	auto yMinOrthoImage = renderSubview(subViewWidth, subViewHeight, CameraPreset::FACE_CAM_F4_MAX_Y);
	ImageOverlay bottomLeftOverlay = { yMinOrthoImage,
									   {QString("%1").arg( m_heightField->GetAxisLabelX()),
										1,
										m_heightField->GetMaxX() + 1 },
									   {
										   QString("%1").arg(m_heightField->GetAxisLabelZ()),
										   m_heightField->GetMaxZ() + 1,
										   1
									   }
									 };

	//	Slice through XY plane
	auto zMinOrthoImage = renderSubview(subViewWidth, subViewHeight, CameraPreset::FACE_CAM_F0_MIN_Z);
	ImageOverlay bottomRightOverlay = { zMinOrthoImage,
										{ QString("%1").arg(m_heightField->GetAxisLabelX()),
										  1,
										  m_heightField->GetMaxX() + 1 },
										{
											QString("%1").arg(m_heightField->GetAxisLabelY()),
											m_heightField->GetMaxY() + 1,
											1
										}
									  };

	assert(context() != nullptr);

	QPainter painter(this);
	painter.drawImage(0,0,mainViewImage);

	//	Renders text over the top of the image
	if (m_renderAxisLabels)	renderAxisLabels(&painter, getAxisData(m_heightField));
	if (m_renderAxisLegend) renderAxisLegend(&painter);

	if ((m_contourTreeSkeleton) && (m_renderSkeletonGlyphs))
	{
		renderSkeletonGlyphOverlay(&painter, m_heightField->GetContourTree(),
								   width(), height());
	}

	if ((m_reebGraphSkeleton) && (m_renderSkeletonGlyphs))
	{
		renderSkeletonGlyphOverlay(&painter, m_heightField->GetReebGraph(),
								   width(), height());
	}


	renderCornerOverlay(&painter, OverlayPosition::TOP_RIGHT, topRightOverlay);
	renderCornerOverlay(&painter, OverlayPosition::BOTTOM_LEFT, bottomLeftOverlay);
	renderCornerOverlay(&painter, OverlayPosition::BOTTOM_RIGHT, bottomRightOverlay);

	painter.end();


#undef SUPRESS_OUTPUT
}

///
/// \brief		Does all of the 3d rendering
/// \since		14-07-2015
/// \author		Dean
///
void QtIsosurfaceWidget::renderPipeline(const QColor &backgroundColour)
{
	assert(context() != nullptr);
	assert(m_heightField != nullptr);

	//	Clear the background on the current render context (should be a
	//	back-buffer image)
	qglClearColor(backgroundColour);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//	Get a list of contours for rendering
	vector<Contour*> renderableContours;
	if (m_nestedContours)
	{
		//	Multiple isovalues in a range
		renderableContours = getRenderableContours(m_nested_contour_range_min,
												   m_nested_contour_range_max,
												   m_numberOfNestedContours);
	}
	else
	{
		//	Just a single isovalue
		renderableContours = getRenderableContours(m_targetIsovalue);
	}

	//	Set up the triangle vertex buffer
	auto triangleData = generateTriangleBuffer(renderableContours);

	//	Highlighted Open Edges
	BufferData *lineData = generateLineBuffer(renderableContours, m_renderMeshHolesWireframeOnly);

	if (m_renderSurfaces)
	{
		executeTriangleDrawingShader(m_mainShaderProgram,
									 m_lightPosition,
									 m_alphaBlended,
									 triangleData);
	}

	if (m_renderWireframe)
		renderWireframe(lineData);

#ifdef CALCULATE_MOMENTS
	if (m_renderMoments) renderMoments();
#endif
	if (m_contourTreeSkeleton)
		renderReebGraphSkeleton(m_heightField->GetContourTree());
	if (m_reebGraphSkeleton)
		renderReebGraphSkeleton(m_heightField->GetReebGraph());

	if (m_render3dPolygonCaps) render3dPolygonCaps();

	if (m_render2dPolygonCapsMinX)
		render2dPolygonCaps(HeightField3D::Boundary::MIN_X);
	if (m_render2dPolygonCapsMaxX)
		render2dPolygonCaps(HeightField3D::Boundary::MAX_X);
	if (m_render2dPolygonCapsMinY)
		render2dPolygonCaps(HeightField3D::Boundary::MIN_Y);
	if (m_render2dPolygonCapsMaxY)
		render2dPolygonCaps(HeightField3D::Boundary::MAX_Y);
	if (m_render2dPolygonCapsMinZ)
		render2dPolygonCaps(HeightField3D::Boundary::MIN_Z);
	if (m_render2dPolygonCapsMaxZ)
		render2dPolygonCaps(HeightField3D::Boundary::MAX_Z);

	if (m_renderGrid)
	{
		if (m_renderGridOuterOnly)
		{
			renderOuterGrid();
		}
		else
		{
			renderFullGrid();
		}
	}

	if (m_renderAxisLines)
		renderAxes(getAxisData(m_heightField), m_renderFarAxisLines, m_axisThickness);

	//	Done with this
	delete lineData;
}


///
/// \brief		Renders the data grid on the outer cells only
/// \since		16-07-2015
/// \author		Dean
///
void QtIsosurfaceWidget::renderOuterGrid()
{
	assert(context() != nullptr);

	//#define SUPRESS_OUTPUT
	if (m_heightField == nullptr) return;

	//	-1 in each dimension (as we are drawing boxes and the last row doesn't
	//	require drawing)
	unsigned long xMax = m_heightField->GetBaseDimX() - 1;
	unsigned long yMax = m_heightField->GetBaseDimY() - 1;
	unsigned long zMax = m_heightField->GetBaseDimZ() - 1;

	//	Load the mesh data in to temporary buffers
	BufferData* bufferData = new BufferData();

	//	Front and back faces
	for (unsigned long x = 0; x < xMax; ++x)
	{
		for (unsigned long y = 0; y < yMax; ++y)
		{
			const auto vColor = QColorToQVector4D(m_gridLineColor);

			const QVector3D v0 = { static_cast<float>(x),
								   static_cast<float>(y),
								   0.0f };
			const QVector3D v1 = { static_cast<float>(x + 1),
								   static_cast<float>(y),
								   0.0f };
			const QVector3D v2 = { static_cast<float>(x),
								   static_cast<float>(y + 1),
								   0.0f };
			const QVector3D v3 = { static_cast<float>(x + 1),
								   static_cast<float>(y + 1),
								   0.0f };

			const QVector3D v4 = { static_cast<float>(x),
								   static_cast<float>(y),
								   static_cast<float>(zMax) };
			const QVector3D v5 = { static_cast<float>(x + 1),
								   static_cast<float>(y),
								   static_cast<float>(zMax) };
			const QVector3D v6 = { static_cast<float>(x),
								   static_cast<float>(y + 1),
								   static_cast<float>(zMax) };
			const QVector3D v7 = { static_cast<float>(x + 1),
								   static_cast<float>(y + 1),
								   static_cast<float>(zMax) };

			//	z = 0
			bufferData->vertexArray.push_back(v0);
			bufferData->vertexArray.push_back(v1);
			bufferData->colourArray.push_back(vColor);
			bufferData->colourArray.push_back(vColor);

			bufferData->vertexArray.push_back(v0);
			bufferData->vertexArray.push_back(v2);
			bufferData->colourArray.push_back(vColor);
			bufferData->colourArray.push_back(vColor);

			bufferData->vertexArray.push_back(v2);
			bufferData->vertexArray.push_back(v3);
			bufferData->colourArray.push_back(vColor);
			bufferData->colourArray.push_back(vColor);

			bufferData->vertexArray.push_back(v1);
			bufferData->vertexArray.push_back(v3);
			bufferData->colourArray.push_back(vColor);
			bufferData->colourArray.push_back(vColor);

			//	z = zMax
			bufferData->vertexArray.push_back(v4);
			bufferData->vertexArray.push_back(v5);
			bufferData->colourArray.push_back(vColor);
			bufferData->colourArray.push_back(vColor);

			bufferData->vertexArray.push_back(v4);
			bufferData->vertexArray.push_back(v6);
			bufferData->colourArray.push_back(vColor);
			bufferData->colourArray.push_back(vColor);

			bufferData->vertexArray.push_back(v5);
			bufferData->vertexArray.push_back(v7);
			bufferData->colourArray.push_back(vColor);
			bufferData->colourArray.push_back(vColor);

			bufferData->vertexArray.push_back(v6);
			bufferData->vertexArray.push_back(v7);
			bufferData->colourArray.push_back(vColor);
			bufferData->colourArray.push_back(vColor);
		}
	}

	//	Top and bottom faces
	for (unsigned long x = 0; x < xMax; ++x)
	{
		for (unsigned long z = 0; z < zMax; ++z)
		{
			const auto vColor = QColorToQVector4D(m_gridLineColor);

			const QVector3D v0 = { static_cast<float>(x), 0.0f, static_cast<float>(z) };
			const QVector3D v1 = { static_cast<float>(x + 1), 0.0f, static_cast<float>(z) };
			const QVector3D v2 = { static_cast<float>(x), static_cast<float>(yMax), static_cast<float>(z) };
			const QVector3D v3 = { static_cast<float>(x + 1), static_cast<float>(yMax), static_cast<float>(z) };

			const QVector3D v4 = { static_cast<float>(x), 0.0f, static_cast<float>(z + 1)};
			const QVector3D v5 = { static_cast<float>(x + 1), 0.0f, static_cast<float>(z + 1)};
			const QVector3D v6 = { static_cast<float>(x), static_cast<float>(yMax), static_cast<float>(z + 1)};
			const QVector3D v7 = { static_cast<float>(x + 1), static_cast<float>(yMax), static_cast<float>(z + 1)};

			//	z = 0
			bufferData->vertexArray.push_back(v0);
			bufferData->vertexArray.push_back(v1);
			bufferData->colourArray.push_back(vColor);
			bufferData->colourArray.push_back(vColor);

			bufferData->vertexArray.push_back(v0);
			bufferData->vertexArray.push_back(v4);
			bufferData->colourArray.push_back(vColor);
			bufferData->colourArray.push_back(vColor);

			bufferData->vertexArray.push_back(v4);
			bufferData->vertexArray.push_back(v5);
			bufferData->colourArray.push_back(vColor);
			bufferData->colourArray.push_back(vColor);

			bufferData->vertexArray.push_back(v1);
			bufferData->vertexArray.push_back(v5);
			bufferData->colourArray.push_back(vColor);
			bufferData->colourArray.push_back(vColor);

			//	z = zMax
			bufferData->vertexArray.push_back(v2);
			bufferData->vertexArray.push_back(v3);
			bufferData->colourArray.push_back(vColor);
			bufferData->colourArray.push_back(vColor);

			bufferData->vertexArray.push_back(v2);
			bufferData->vertexArray.push_back(v6);
			bufferData->colourArray.push_back(vColor);
			bufferData->colourArray.push_back(vColor);

			bufferData->vertexArray.push_back(v6);
			bufferData->vertexArray.push_back(v7);
			bufferData->colourArray.push_back(vColor);
			bufferData->colourArray.push_back(vColor);

			bufferData->vertexArray.push_back(v3);
			bufferData->vertexArray.push_back(v7);
			bufferData->colourArray.push_back(vColor);
			bufferData->colourArray.push_back(vColor);
		}
	}

	//	Left and right faces
	for (unsigned long y = 0; y < yMax; ++y)
	{
		for (unsigned long z = 0; z < zMax; ++z)
		{
			const auto vColor = QColorToQVector4D(m_gridLineColor);

			const QVector3D v0 = { (float)0, (float)y, (float)z };
			const QVector3D v1 = { (float)xMax, (float)y, (float)z };
			const QVector3D v2 = { (float)0, (float)y + 1, (float)z };
			const QVector3D v3 = { (float)xMax, (float)y + 1, (float)z };

			const QVector3D v4 = { (float)0, (float)y, (float)z + 1 };
			const QVector3D v5 = { (float)xMax, (float)y, (float)z + 1 };
			const QVector3D v6 = { (float)0, (float)y + 1, (float)z + 1 };
			const QVector3D v7 = { (float)xMax, (float)y + 1, (float)z + 1 };

			//	z = 0
			bufferData->vertexArray.push_back(v1);
			bufferData->vertexArray.push_back(v3);
			bufferData->colourArray.push_back(vColor);
			bufferData->colourArray.push_back(vColor);

			bufferData->vertexArray.push_back(v3);
			bufferData->vertexArray.push_back(v7);
			bufferData->colourArray.push_back(vColor);
			bufferData->colourArray.push_back(vColor);

			bufferData->vertexArray.push_back(v1);
			bufferData->vertexArray.push_back(v5);
			bufferData->colourArray.push_back(vColor);
			bufferData->colourArray.push_back(vColor);

			bufferData->vertexArray.push_back(v5);
			bufferData->vertexArray.push_back(v7);
			bufferData->colourArray.push_back(vColor);
			bufferData->colourArray.push_back(vColor);

			//	z = zMax
			bufferData->vertexArray.push_back(v0);
			bufferData->vertexArray.push_back(v2);
			bufferData->colourArray.push_back(vColor);
			bufferData->colourArray.push_back(vColor);

			bufferData->vertexArray.push_back(v0);
			bufferData->vertexArray.push_back(v4);
			bufferData->colourArray.push_back(vColor);
			bufferData->colourArray.push_back(vColor);

			bufferData->vertexArray.push_back(v2);
			bufferData->vertexArray.push_back(v6);
			bufferData->colourArray.push_back(vColor);
			bufferData->colourArray.push_back(vColor);

			bufferData->vertexArray.push_back(v4);
			bufferData->vertexArray.push_back(v6);
			bufferData->colourArray.push_back(vColor);
			bufferData->colourArray.push_back(vColor);
		}
	}

	//	Execute the line drawing shader
	executeLineDrawingShader(bufferData->vertexArray,
							 bufferData->colourArray,
							 m_gridLineThickness);

	//	Finsihed with this now
	delete bufferData;
}

///
/// \brief		Renders the grid at integer points
/// \since		26-05-2015
/// \author		Dean
/// \author		Dean: fixed seg fault [15-07-2015]
///
void QtIsosurfaceWidget::renderFullGrid()
{
	assert(context() != nullptr);

	//#define SUPRESS_OUTPUT
	if (m_heightField == nullptr) return;

	//	-1 in each dimension (as we are drawing boxes and the last row doesn't
	//	require drawing)
	unsigned long xMax = m_heightField->GetBaseDimX() - 1;
	unsigned long yMax = m_heightField->GetBaseDimY() - 1;
	unsigned long zMax = m_heightField->GetBaseDimZ() - 1;

	//	Count the number of vertices we are going to need to reserve in
	//	video memory
	unsigned long vertexCount = 24 * xMax * yMax * zMax;

	printf("Requesting %ld vertices for grid rendering (%ld x %ld x %ld).\n",
		   vertexCount, xMax, yMax, zMax);
	fflush(stdout);

	if (vertexCount > 0)
	{
		//	Load the mesh data in to temporary buffers
		BufferData* bufferData = new BufferData();

		//	Setup the vertices / normals / colour arrays, reserving enough
		//	space in each
		bufferData->vertexArray.reserve(vertexCount);
		bufferData->colourArray.reserve(vertexCount);

		//	Loop over the ready contours
		for (unsigned long x = 0; x < xMax; ++x)
		{
			for (unsigned long y = 0; y < yMax; ++y)
			{
				for (unsigned long z = 0; z < zMax; ++z)
				{
					const auto vColor = QColorToQVector4D(m_gridLineColor);

					const QVector3D v0 = { (float)x,		(float)y,		(float)z };
					const QVector3D v1 = { (float)x + 1,	(float)y,		(float)z };
					const QVector3D v2 = { (float)x,		(float)y + 1,	(float)z };
					const QVector3D v3 = { (float)x + 1,	(float)y + 1,	(float)z };
					const QVector3D v4 = { (float)x,		(float)y,		(float)z + 1 };
					const QVector3D v5 = { (float)x + 1,	(float)y,		(float)z + 1 };
					const QVector3D v6 = { (float)x,		(float)y + 1,	(float)z + 1 };
					const QVector3D v7 = { (float)x + 1,	(float)y + 1,	(float)z + 1 };

					//	Draw our 12 edges of the cell
					bufferData->vertexArray.push_back(v0);
					bufferData->vertexArray.push_back(v1);
					bufferData->colourArray.push_back(vColor);
					bufferData->colourArray.push_back(vColor);

					bufferData->vertexArray.push_back(v0);
					bufferData->vertexArray.push_back(v4);
					bufferData->colourArray.push_back(vColor);
					bufferData->colourArray.push_back(vColor);

					bufferData->vertexArray.push_back(v0);
					bufferData->vertexArray.push_back(v2);
					bufferData->colourArray.push_back(vColor);
					bufferData->colourArray.push_back(vColor);

					bufferData->vertexArray.push_back(v1);
					bufferData->vertexArray.push_back(v3);
					bufferData->colourArray.push_back(vColor);
					bufferData->colourArray.push_back(vColor);

					bufferData->vertexArray.push_back(v1);
					bufferData->vertexArray.push_back(v5);
					bufferData->colourArray.push_back(vColor);
					bufferData->colourArray.push_back(vColor);

					bufferData->vertexArray.push_back(v2);
					bufferData->vertexArray.push_back(v3);
					bufferData->colourArray.push_back(vColor);
					bufferData->colourArray.push_back(vColor);

					bufferData->vertexArray.push_back(v2);
					bufferData->vertexArray.push_back(v6);
					bufferData->colourArray.push_back(vColor);
					bufferData->colourArray.push_back(vColor);

					bufferData->vertexArray.push_back(v3);
					bufferData->vertexArray.push_back(v7);
					bufferData->colourArray.push_back(vColor);
					bufferData->colourArray.push_back(vColor);

					bufferData->vertexArray.push_back(v4);
					bufferData->vertexArray.push_back(v5);
					bufferData->colourArray.push_back(vColor);
					bufferData->colourArray.push_back(vColor);

					bufferData->vertexArray.push_back(v4);
					bufferData->vertexArray.push_back(v6);
					bufferData->colourArray.push_back(vColor);
					bufferData->colourArray.push_back(vColor);

					bufferData->vertexArray.push_back(v5);
					bufferData->vertexArray.push_back(v7);
					bufferData->colourArray.push_back(vColor);
					bufferData->colourArray.push_back(vColor);

					bufferData->vertexArray.push_back(v6);
					bufferData->vertexArray.push_back(v7);
					bufferData->colourArray.push_back(vColor);
					bufferData->colourArray.push_back(vColor);

				}
			}
		}
#if defined(VERBOSE_OUTPUT) && !defined(SUPRESS_OUTPUT)
		//printf("Loaded in %ld vertices.\n", vertexArray.size());
		//fflush(stdout);
#endif

		glLineWidth(1.0f);

		//	Our matrices to be send to the GPU
		//QMatrix4x4 mMatrix;
		QMatrix4x4 vMatrix =m_currentCamera->GetViewMatrix();
		QMatrix4x4 mvMatrix = vMatrix * m_mMatrix;
		QMatrix3x3 normalMatrix = mvMatrix.normalMatrix();

		//
		//QVector3D lightPos(5.0, 5.0, 5.0);

		//	Bind our shader program
		m_basicShaderProgram.bind();

		//	Pre-compute our matrices in various forms and send them as uniforms
		m_basicShaderProgram.setUniformValue("uModelViewMatrix", mvMatrix);
		m_basicShaderProgram.setUniformValue("uProjectionMatrix", m_projectionMatrix);
		m_basicShaderProgram.setUniformValue("uNormalMatrix", normalMatrix);

		//	Lighting uniforms
		//m_shaderProgram.setUniformValue("lightPos", lightPos);
		//m_shaderProgram.setUniformValue("lightDiffuse", QColor(Qt::yellow));

		//	Send the vertex array
		m_basicShaderProgram.setAttributeArray("vPosition", bufferData->vertexArray.constData());
		m_basicShaderProgram.enableAttributeArray("vPosition");

		//	Send the colour array
		m_basicShaderProgram.setAttributeArray("vColor", bufferData->colourArray.constData());
		m_basicShaderProgram.enableAttributeArray("vColor");

		//	If there is data to be rendered, render it...
		if (bufferData->vertexArray.size() > 0)
			glDrawArrays(GL_LINES, 0, bufferData->vertexArray.size());

		//	Clean up the shader program
		m_basicShaderProgram.disableAttributeArray("vPosition");
		m_basicShaderProgram.disableAttributeArray("vColor");
		m_basicShaderProgram.release();


		//	Finsihed with this now
		delete bufferData;
	}

#undef SUPRESS_OUTPUT
}

///
/// \brief		Renders the mesh, using colours to highlight open areas of the
///				mesh
/// \since		26-05-2015
/// \author		Dean
///
void QtIsosurfaceWidget::renderWireframe(BufferData *vertexBuffer)
{
	assert(context() != nullptr);
	assert(vertexBuffer != nullptr);

	QVector<QVector4D> vertexColours;
	if (m_renderEdgeConnectivityWireframe)
	{
		//	Vertices coloured according to edge connectivity
		vertexColours = vertexBuffer->colourArray;
	}
	else
	{
		//	Just colour everything black
		vertexColours.fill(QVector4D(0.8f, 0.8f, 0.8f, 0.5f),vertexBuffer->vertexArray.size());
	}

	//	Do the drawing
	executeLineDrawingShader(vertexBuffer->vertexArray,
							 vertexColours,
							 0.5f);
#undef SUPRESS_OUTPUT
}


///
/// \brief		Renders the 3d polygon caps identified for filling holes in
///				meshes
///	\since		27-05-2015
/// \author		Dean
///
void QtIsosurfaceWidget::render3dPolygonCaps()
{
	assert(context() != nullptr);

	if (m_heightField == nullptr) return;
	if (m_heightField->GetContourTree() == nullptr) return;

	//	Colour the polygons for each contour in a different hue
	const unsigned long HUE_DELTA = 60; //	Gives 360/60 = 6 unique hues
	const unsigned long SAT_DELTA = 32;
	unsigned long hue = 0;
	unsigned long sat = 64;

	//	Colour of the polygons for the first capped contour
	QColor polygonColour = QColor::fromHsv(hue, sat, 128);

	//#define SUPRESS_OUTPUT
	//	Load the mesh data in to temporary buffers
	BufferData* bufferData = new BufferData();

	//	Avoid needing to resize straight away (just a random guess...)
	bufferData->vertexArray.reserve(500);
	bufferData->colourArray.reserve(500);

	//	All arcs that should be rendered
	auto superarcs =
			m_heightField->GetContourTree()->GetValidSuperarcsPresentAtIsovalue(m_targetIsovalue);

	//	Loop over the number of visible contours and find the ones that are
	//	actual ready to be rendered.
	for (auto c = 0ul; c < superarcs.size(); ++c)
	{
		//	Access the current contour (if ready)
		Contour* currentContour = superarcs[c]->RequestContour(m_targetIsovalue);

		//	Don't count contours still being constructed
		if (currentContour != nullptr)
		{
			//	Ignore contours marked as hidden
			if (!currentContour->IsHiddenByUser())
			{
				//	Get a list of 3d polygons
				vector<Polygon3D*> cappingPolygons3d
						= currentContour->Get3dCappingPolygons();

				//	A contour may not need any capping polygons - in that case
				//	skip the next section altogether
				if (!cappingPolygons3d.empty())
				{
					//	Loop through each polygon
					for (unsigned long p = 0; p < cappingPolygons3d.size(); ++p)
					{
						Polygon3D* polygon = cappingPolygons3d[p];

						//	Get a list of edges
						vector<Edge3D> edges = polygon->GetEdges();

						//	Loop through each edge
						for (unsigned long e = 0; e < edges.size(); ++e)
						{
							Edge3D currentEdge = edges[e];

							//	2 Vertices of the edge
							bufferData->vertexArray.push_back(QVector3D(currentEdge.GetVertexA().GetX(),
																		currentEdge.GetVertexA().GetY(),
																		currentEdge.GetVertexA().GetZ()));
							bufferData->vertexArray.push_back(QVector3D(currentEdge.GetVertexB().GetX(),
																		currentEdge.GetVertexB().GetY(),
																		currentEdge.GetVertexB().GetZ()));

							//	Colour of the polygon edges
							bufferData->colourArray.push_back(QVector3D(polygonColour.redF(),
																		polygonColour.blueF(),
																		polygonColour.greenF()));
							bufferData->colourArray.push_back(QVector3D(polygonColour.redF(),
																		polygonColour.blueF(),
																		polygonColour.greenF()));
						}
					}

					//	Calculate colours for polygons on next contour
					hue += HUE_DELTA;
					if (hue >= 360)
					{
						hue = 0;
						sat += SAT_DELTA;
						if (sat > 255)
							sat = 0;
					}
					polygonColour = QColor::fromHsv(hue, sat, 128);
				}
			}
		}


	}

	glEnable(GL_DEPTH_TEST);
	glLineWidth(2.0f);

	//	Our matrices to be send to the GPU
	//QMatrix4x4 mMatrix;
	QMatrix4x4 vMatrix = m_currentCamera->GetViewMatrix();
	QMatrix4x4 mvMatrix = vMatrix * m_mMatrix;
	QMatrix3x3 normalMatrix = mvMatrix.normalMatrix();

	//
	//QVector3D lightPos(5.0, 5.0, 5.0);

	//	Bind our shader program
	m_basicShaderProgram.bind();

	//	Pre-compute our matrices in various forms and send them as uniforms
	m_basicShaderProgram.setUniformValue("uModelViewMatrix", mvMatrix);
	m_basicShaderProgram.setUniformValue("uProjectionMatrix", m_projectionMatrix);
	m_basicShaderProgram.setUniformValue("uNormalMatrix", normalMatrix);

	//	Lighting uniforms
	//m_shaderProgram.setUniformValue("lightPos", lightPos);
	//m_shaderProgram.setUniformValue("lightDiffuse", QColor(Qt::yellow));

	//	Send the vertex array
	m_basicShaderProgram.setAttributeArray("vPosition", bufferData->vertexArray.constData());
	m_basicShaderProgram.enableAttributeArray("vPosition");

	//	Send the colour array
	m_basicShaderProgram.setAttributeArray("vColor", bufferData->colourArray.constData());
	m_basicShaderProgram.enableAttributeArray("vColor");

	//	If there is data to be rendered, render it...
	if (bufferData->vertexArray.size() > 0)
		glDrawArrays(GL_LINES, 0, bufferData->vertexArray.size());

	//	Clean up the shader program
	m_basicShaderProgram.disableAttributeArray("vPosition");
	m_basicShaderProgram.disableAttributeArray("vColor");
	m_basicShaderProgram.release();

	delete bufferData;
}

///
/// \brief		Renders the 3d polygon caps identified for filling holes in
///				meshes
///	\since		27-05-2015
/// \author		Dean
///
void QtIsosurfaceWidget::render2dPolygonCaps(const HeightField3D::Boundary &targetBoundary)
{
	assert(context() != nullptr);

	if (m_heightField == nullptr) return;
	if (m_heightField->GetContourTree() == nullptr) return;

	//	Colour the polygons for each contour in a different hue
	const unsigned long HUE_DELTA = 60; //	Gives 360/60 = 6 unique hues
	const unsigned long SAT_DELTA = 32;
	unsigned long hue = 0;
	unsigned long sat = 128;

	//	Colour of the polygons for the first capped contour
	QColor polygonColour = QColor::fromHsv(hue, sat, 128);

	//#define SUPRESS_OUTPUT
	//	Load the mesh data in to temporary buffers
	BufferData* bufferData = new BufferData();

	//	Avoid needing to resize straight away (just a random guess...)
	bufferData->vertexArray.reserve(500);
	bufferData->colourArray.reserve(500);

	//	All arcs that should be rendered
	auto superarcs =
			m_heightField->GetContourTree()->GetValidSuperarcsPresentAtIsovalue(m_targetIsovalue);

	//	Loop over the number of visible contours and find the ones that are
	//	actual ready to be rendered.
	for (auto c = 0ul; c < superarcs.size(); ++c)
	{
		//	Access the current contour (if ready)
		Contour* currentContour = superarcs[c]->RequestContour(m_targetIsovalue);

		//	Don't count contours still being constructed
		if (currentContour != nullptr)
		{
			//	Ignore contours marked as hidden
			if (!currentContour->IsHiddenByUser())
			{
				//	Vector to hold our 2D polygons
				vector<Polygon2D*> cappingPolygons2d
						= currentContour->Get2dCappingPolygons(targetBoundary);

				//	A contour may not need any capping polygons - in that case
				//	skip the next section altogether
				if (!cappingPolygons2d.empty())
				{
					//	Loop through each polygon
					for (unsigned long p = 0; p < cappingPolygons2d.size(); ++p)
					{
						Polygon2D* polygon = cappingPolygons2d[p];

						//	Get a list of edges
						vector<Edge2D> edges = polygon->GetEdges();

						//	Loop through each edge
						for (unsigned long e = 0; e < edges.size(); ++e)
						{
							Edge2D currentEdge = edges[e];

							//	2 Vertices of the edge translated from 2D to 3D
							switch (targetBoundary)
							{
							case HeightField3D::Boundary::MIN_X:
								//	Set X component to zero
								bufferData->vertexArray.push_back(QVector3D(0.0,
																			currentEdge.GetVertexA().GetX(),
																			currentEdge.GetVertexA().GetY()));
								bufferData->vertexArray.push_back(QVector3D(0.0,
																			currentEdge.GetVertexB().GetX(),
																			currentEdge.GetVertexB().GetY()));
								break;
							case HeightField3D::Boundary::MAX_X:
								//	Set X component to data max
								bufferData->vertexArray.push_back(QVector3D(m_heightField->GetMaxX(),
																			currentEdge.GetVertexA().GetX(),
																			currentEdge.GetVertexA().GetY()));
								bufferData->vertexArray.push_back(QVector3D(m_heightField->GetMaxX(),
																			currentEdge.GetVertexB().GetX(),
																			currentEdge.GetVertexB().GetY()));
								break;
							case HeightField3D::Boundary::MIN_Y:
								//	Set Y component to zero
								bufferData->vertexArray.push_back(QVector3D(currentEdge.GetVertexA().GetX(),
																			0.0,
																			currentEdge.GetVertexA().GetY()));
								bufferData->vertexArray.push_back(QVector3D(currentEdge.GetVertexB().GetX(),
																			0.0,
																			currentEdge.GetVertexB().GetY()));
								break;
							case HeightField3D::Boundary::MAX_Y:
								//	Set Y component to data max
								bufferData->vertexArray.push_back(QVector3D(currentEdge.GetVertexA().GetX(),
																			m_heightField->GetMaxY(),
																			currentEdge.GetVertexA().GetY()));
								bufferData->vertexArray.push_back(QVector3D(currentEdge.GetVertexB().GetX(),
																			m_heightField->GetMaxY(),
																			currentEdge.GetVertexB().GetY()));
								break;
							case HeightField3D::Boundary::MIN_Z:
								//	Set Z component to zero
								bufferData->vertexArray.push_back(QVector3D(currentEdge.GetVertexA().GetX(),
																			currentEdge.GetVertexA().GetY(),
																			0.0));
								bufferData->vertexArray.push_back(QVector3D(currentEdge.GetVertexB().GetX(),
																			currentEdge.GetVertexB().GetY(),
																			0.0));
								break;
							case HeightField3D::Boundary::MAX_Z:
								//	Set Z component to data max
								bufferData->vertexArray.push_back(QVector3D(currentEdge.GetVertexA().GetX(),
																			currentEdge.GetVertexA().GetY(),
																			m_heightField->GetMaxZ()));
								bufferData->vertexArray.push_back(QVector3D(currentEdge.GetVertexB().GetX(),
																			currentEdge.GetVertexB().GetY(),
																			m_heightField->GetMaxZ()));
								break;
							default:
								//	This shouldn't happen using strongly typed enums
								fprintf(stderr, "Unknown 3D boundary passed to function: %s,"
												" in file %s (line %ld).\n", __func__, __FILE__, __LINE__);
								fflush(stderr);
								break;
							}

							//	Colour of the polygon edges
							bufferData->colourArray.push_back(QVector3D(polygonColour.redF(),
																		polygonColour.blueF(),
																		polygonColour.greenF()));
							bufferData->colourArray.push_back(QVector3D(polygonColour.redF(),
																		polygonColour.blueF(),
																		polygonColour.greenF()));
						}
					}
				}

				//	Calculate colours for polygons on next contour
				hue += HUE_DELTA;
				if (hue >= 360)
				{
					hue = 0;
					sat += SAT_DELTA;
					if (sat > 255)
						sat = 0;
				}
				polygonColour = QColor::fromHsv(hue, sat, 128);
			}
		}


	}

	glEnable(GL_DEPTH_TEST);
	glLineWidth(2.0f);

	//	Our matrices to be send to the GPU
	//QMatrix4x4 mMatrix;
	QMatrix4x4 vMatrix = m_currentCamera->GetViewMatrix();
	QMatrix4x4 mvMatrix = vMatrix * m_mMatrix;
	QMatrix3x3 normalMatrix = mvMatrix.normalMatrix();

	//
	//QVector3D lightPos(5.0, 5.0, 5.0);

	//	Bind our shader program
	m_basicShaderProgram.bind();

	//	Pre-compute our matrices in various forms and send them as uniforms
	m_basicShaderProgram.setUniformValue("uModelViewMatrix", mvMatrix);
	m_basicShaderProgram.setUniformValue("uProjectionMatrix", m_projectionMatrix);
	m_basicShaderProgram.setUniformValue("uNormalMatrix", normalMatrix);

	//	Lighting uniforms
	//m_shaderProgram.setUniformValue("lightPos", lightPos);
	//m_shaderProgram.setUniformValue("lightDiffuse", QColor(Qt::yellow));

	//	Send the vertex array
	m_basicShaderProgram.setAttributeArray("vPosition", bufferData->vertexArray.constData());
	m_basicShaderProgram.enableAttributeArray("vPosition");

	//	Send the colour array
	m_basicShaderProgram.setAttributeArray("vColor", bufferData->colourArray.constData());
	m_basicShaderProgram.enableAttributeArray("vColor");

	//	If there is data to be rendered, render it...
	if (bufferData->vertexArray.size() > 0)
		glDrawArrays(GL_LINES, 0, bufferData->vertexArray.size());

	//	Clean up the shader program
	m_basicShaderProgram.disableAttributeArray("vPosition");
	m_basicShaderProgram.disableAttributeArray("vColor");
	m_basicShaderProgram.release();

	delete bufferData;
}



void QtIsosurfaceWidget::renderCornerOverlay(QPainter* painter,
											 const OverlayPosition& pos,
											 const ImageOverlay& overlay)
{
	assert (context() != nullptr);
#ifndef DISABLE_DEBUG_OUTPUT
	cout << __PRETTY_FUNCTION__ << endl;
#endif
	const auto LABEL_HEIGHT = 15;
	const auto TARGET_WIDTH = calculateSubviewSize();
	const auto TARGET_HEIGHT = calculateSubviewSize();

	//	const auto X_MIN = 1;
	//	const auto X_MAX = 18;
	//	const auto X_MID = (X_MIN + (X_MAX - X_MIN)) / 2;

	//	const auto Y_MIN = 1;
	//	const auto Y_MAX = 18;
	//	const auto Y_MID = (Y_MIN + (Y_MAX - Y_MIN)) / 2;

	QPointF subViewTopLeft;
	QPointF xLabelTopLeftAnchor;
	QPointF yLabelTopLeftAnchor;
	QPointF xLabelSize(TARGET_WIDTH, LABEL_HEIGHT);
	QPointF yLabelSize(LABEL_HEIGHT, TARGET_HEIGHT);

	QTextOption textOptionLeft(Qt::AlignLeft | Qt::AlignVCenter);
	QTextOption textOptionHCentre(Qt::AlignHCenter | Qt::AlignVCenter);
	QTextOption textOptionRight(Qt::AlignRight | Qt::AlignVCenter);

	QTextOption textOptionTop(Qt::AlignHCenter | Qt::AlignTop);
	QTextOption textOptionVCentre(Qt::AlignHCenter | Qt::AlignVCenter);
	QTextOption textOptionBottom(Qt::AlignHCenter | Qt::AlignBottom);

	switch (pos)
	{
	case OverlayPosition::TOP_LEFT:
		subViewTopLeft = QPointF(0, 0);
		xLabelTopLeftAnchor = QPointF(0,
									  TARGET_HEIGHT);
		yLabelTopLeftAnchor = QPointF(TARGET_WIDTH,
									  0);
		break;
	case OverlayPosition::TOP_RIGHT:
		subViewTopLeft = QPointF(width() - TARGET_WIDTH, 0);
		xLabelTopLeftAnchor = QPointF(width() - TARGET_WIDTH,
									  TARGET_HEIGHT);
		yLabelTopLeftAnchor = QPointF(width() - TARGET_WIDTH - LABEL_HEIGHT,
									  0);
		break;
	case OverlayPosition::BOTTOM_LEFT:
		subViewTopLeft = QPointF(0, height() - TARGET_HEIGHT);
		xLabelTopLeftAnchor = QPointF(0,
									  height() - TARGET_HEIGHT - LABEL_HEIGHT);
		yLabelTopLeftAnchor = QPointF(TARGET_WIDTH,
									  height() - TARGET_HEIGHT);
		break;
	case OverlayPosition::BOTTOM_RIGHT:
		subViewTopLeft = QPointF(width() - TARGET_WIDTH, height() - TARGET_HEIGHT);
		xLabelTopLeftAnchor = QPointF(width() - TARGET_WIDTH,
									  height() - TARGET_HEIGHT - LABEL_HEIGHT);
		yLabelTopLeftAnchor = QPointF(width() - TARGET_WIDTH - LABEL_HEIGHT,
									  height() - TARGET_HEIGHT);
		break;
	}

	QRectF xTextRect(xLabelTopLeftAnchor, xLabelTopLeftAnchor + xLabelSize);
	QRectF yTextRect(yLabelTopLeftAnchor, yLabelTopLeftAnchor + yLabelSize);
	//	Render the image
	painter->drawImage(subViewTopLeft, overlay.image);

	//	Label the x axis
	auto xAxis = overlay.xAxis;
	painter->drawText(xTextRect, QString("%1").arg(xAxis.min), textOptionLeft);
	painter->drawText(xTextRect, QString("%1").arg(xAxis.label), textOptionHCentre);
	painter->drawText(xTextRect, QString("%1").arg(xAxis.max), textOptionRight);

	//	Label the y axis
	auto yAxis = overlay.yAxis;
	painter->drawText(yTextRect, QString("%1").arg(yAxis.max), textOptionTop);
	painter->drawText(yTextRect, QString("%1").arg(yAxis.label), textOptionVCentre);
	painter->drawText(yTextRect, QString("%1").arg(yAxis.min), textOptionBottom);
}

///
/// \brief		Renders any overlayed graphics / text
/// \author		Dean
/// \since		14-07-2015
///
void QtIsosurfaceWidget::renderOverlay(const ImageOverlay& topRightOverlay,
									   const ImageOverlay& bottomLeftOverlay,
									   const ImageOverlay& bottomRightOverlay)
{

}

///
/// \brief		Draws the axis legend as an overlay
/// \param painter
/// \since		17-07-2015
/// \author		Dean
///
void QtIsosurfaceWidget::renderAxisLegend(QPainter *painter)
{
	assert(context() != nullptr);

	painter->save();

	const float LEGEND_LABEL_POS = m_axisLegendLineLength + 5.0;

	//	Project a unit line for each axis into screen space
	QPoint posZero = projectToScreenSpace(QVector3D(0,
													0,
													0),
										  static_cast<float>(width()),
										  static_cast<float>(height()));
	QPoint posMaxX = projectToScreenSpace(QVector3D(1,
													0,
													0),
										  static_cast<float>(width()),
										  static_cast<float>(height()));
	QPoint posMaxY = projectToScreenSpace(QVector3D(0,
													1,
													0),
										  static_cast<float>(width()),
										  static_cast<float>(height()));
	QPoint posMaxZ = projectToScreenSpace(QVector3D(0,
													0,
													1),
										  static_cast<float>(width()),
										  static_cast<float>(height()));
#ifndef DISABLE_DEBUG_OUTPUT
	cout << "posMaxX at " << posMaxX << endl;
#endif
	//	Calculate the transform from zero to the end of the axis and normalise
	//	to be of unit length
	QVector2D xAxisTransform(posMaxX - posZero);
	xAxisTransform.normalize();
	QVector2D yAxisTransform(posMaxY - posZero);
	yAxisTransform.normalize();
	QVector2D zAxisTransform(posMaxZ - posZero);
	zAxisTransform.normalize();

	//	Scale by the desired legend size to get the position of the far end
	//	on screen
	QPoint xAxisFarPoint(m_axisLegendLineLength * xAxisTransform.x(),
						 m_axisLegendLineLength * xAxisTransform.y());
	QPoint yAxisFarPoint(m_axisLegendLineLength * yAxisTransform.x(),
						 m_axisLegendLineLength * yAxisTransform.y());
	QPoint zAxisFarPoint(m_axisLegendLineLength * zAxisTransform.x(),
						 m_axisLegendLineLength * zAxisTransform.y());

	//	Set up the pen
	QPen pen(m_axisLegendLineColor);
	pen.setWidth(m_axisLegendLineWidth);
	painter->setPen(pen);

	//	Do the draw
	painter->drawLine(m_legendCentrePoint, m_legendCentrePoint + xAxisFarPoint);
	painter->drawLine(m_legendCentrePoint, m_legendCentrePoint + yAxisFarPoint);
	painter->drawLine(m_legendCentrePoint, m_legendCentrePoint + zAxisFarPoint);

	//	Calculate the position of the text legends
	QPoint xAxisLabelPoint(LEGEND_LABEL_POS * xAxisTransform.x(),
						   LEGEND_LABEL_POS * xAxisTransform.y());
	QPoint yAxisLabelPoint(LEGEND_LABEL_POS * yAxisTransform.x(),
						   LEGEND_LABEL_POS * yAxisTransform.y());
	QPoint zAxisLabelPoint(LEGEND_LABEL_POS * zAxisTransform.x(),
						   LEGEND_LABEL_POS * zAxisTransform.y());

	//	Offset to the center of the legend
	xAxisLabelPoint += m_legendCentrePoint;
	yAxisLabelPoint += m_legendCentrePoint;
	zAxisLabelPoint += m_legendCentrePoint;

	//	Draw the labels
	painter->drawText(xAxisLabelPoint,
					  QString(QChar(m_heightField->GetAxisLabelX())));
	painter->drawText(yAxisLabelPoint,
					  QString(QChar(m_heightField->GetAxisLabelY())));
	painter->drawText(zAxisLabelPoint,
					  QString(QChar(m_heightField->GetAxisLabelZ())));

	painter->restore();
}


void QtIsosurfaceWidget::initializeGL()
{
	QtAbstract3dRenderedView::initializeGL();

	//	This is the main shader to be used for renderling the surfaces
	SetShaderProgram(DEFAULT_SHADER);
}

///
/// \brief		Sets if the grid should be rendered
/// \param value
/// \since		16-07-2015
/// \author		Dean
///
void QtIsosurfaceWidget::SetRenderGrid(const bool &value)
{
	m_renderGrid = value;

	update();
}

///
/// \brief		Sets if the grid should be rendered on outer cells only
/// \param value
/// \since		16-07-2015
/// \author		Dean
///
void QtIsosurfaceWidget::SetRenderOuterGridOnly(const bool &value)
{
	m_renderGridOuterOnly = value;

	update();
}


QVector3D QtIsosurfaceWidget::getMidpointOfData() const
{
	if (m_heightField == nullptr) return QVector3D(0,0,0);

	QVector3D result((m_heightField->GetBaseDimX()-1.0) / 2.0,
					 (m_heightField->GetBaseDimY()-1.0) / 2.0,
					 (m_heightField->GetBaseDimZ()-1.0) / 2.0);

	return result;
}

///
/// \brief		Sets the model matrix using the supplied value
/// \param modelMatrix
/// \since		10-07-2015
/// \author		Dean
///
void QtIsosurfaceWidget::SetModelMatrix(const QMatrix4x4 &modelMatrix)
{
	m_mMatrix = modelMatrix;

	update();
}

///
/// \brief		Set's the name of the main shader program (the 3D effects)
/// \param		name: the name of a shader file (without path and extensions)
/// \return		bool: true if the operation completed sucessfully
/// \since		10-06-2015
/// \author		Dean
///
bool QtIsosurfaceWidget::SetShaderProgram(const QString &name)
{
	QString vShader = SHADER_DIR + name + ".vs.glsl";
	QString fShader = SHADER_DIR + name + ".fs.glsl";

	//	Temporary holder incase we fail
	QGLShaderProgram *newShaderProgram = new QGLShaderProgram(this);

	//	This is the main shader to be used for renderling the surfaces
	newShaderProgram->addShaderFromSourceFile(QGLShader::Vertex,
											  vShader);
	newShaderProgram->addShaderFromSourceFile(QGLShader::Fragment,
											  fShader);

	//	Make sure we link correctly
	bool result = newShaderProgram->link();

	if (result)
	{
		//	Only change the shader if linkning was sucessful
		if (m_mainShaderProgram != nullptr) delete m_mainShaderProgram;
		m_mainShaderProgram = newShaderProgram;

		m_shaderFilename = name.toStdString();

		//	Redraw now
		update();

		return true;
	}
	else
	{
		return false;
	}
}

///
/// \brief QtOpenGLShaderSurfaceRender::SetRenderAxisLabels
/// \param value
/// \author		Dean
/// \since		16-07-2015
///
void QtIsosurfaceWidget::SetRenderAxisLabels(const bool &value)
{
	m_renderAxisLabels = value;
	update();
}

///
/// \brief QtOpenGLShaderSurfaceRender::SetRenderAxisLines
/// \param value
/// \author		Dean
/// \since		16-07-2015
///
void QtIsosurfaceWidget::SetRenderAxisLines(const bool &value)
{
	m_renderAxisLines = value;
	update();
}

////
/// \brief QtOpenGLShaderSurfaceRender::SetRenderFarAxisLines
/// \param value
/// \author		Dean
/// \since		16-07-2015
///
void QtIsosurfaceWidget::SetRenderFarAxisLines(const bool &value)
{
	m_renderFarAxisLines = value;
	update();
}

///
/// \brief		Allow the user to show/hide the edge connectivity diagram
/// \param		checked
/// \since		27-05-2015
/// \author		Dean
///
void QtIsosurfaceWidget::SetRenderEdgeConnectivityWireframe(const bool value)
{
	m_renderEdgeConnectivityWireframe = value;
	update();
}

///
/// \brief		Allow the user to show/hide the contour surfaces
/// \param		checked
/// \since		27-05-2015
/// \author		Dean
///
void QtIsosurfaceWidget::SetRenderSurfaces(const bool value)
{
	m_renderSurfaces = value;
	update();
}

///
/// \brief		Allow the user to show/hide the 3d polygon caps identified for
///				filling holes in meshes
///	\since		27-05-2015
/// \author		Dean
///
void QtIsosurfaceWidget::SetRender3dPolygonCaps(const bool value)
{
	m_render3dPolygonCaps = value;
	update();
}

///
/// \brief		Allow the user to show/hide the 2d polygon caps identified for
///				filling holes in meshes
///	\since		28-05-2015
/// \author		Dean
/// \author		Dean: update to accept a target boundary [01-06-2015]
///
void QtIsosurfaceWidget::SetRender2dPolygonCaps(
		const HeightField3D::Boundary &targetBoundary, const bool value)
{
	switch (targetBoundary)
	{
	case HeightField3D::Boundary::MIN_X:
		m_render2dPolygonCapsMinX = value;
		break;
	case HeightField3D::Boundary::MAX_X:
		m_render2dPolygonCapsMaxX = value;
		break;
	case HeightField3D::Boundary::MIN_Y:
		m_render2dPolygonCapsMinY = value;
		break;
	case HeightField3D::Boundary::MAX_Y:
		m_render2dPolygonCapsMaxY = value;
		break;
	case HeightField3D::Boundary::MIN_Z:
		m_render2dPolygonCapsMinZ = value;
		break;
	case HeightField3D::Boundary::MAX_Z:
		m_render2dPolygonCapsMaxZ = value;
		break;
	default:
		//	This shouldn't happen using strongly typed enums
		fprintf(stderr, "Unknown 3D boundary passed to function: %s,"
						" in file %s (line %ld).\n", __func__, __FILE__, __LINE__);
		fflush(stderr);
		break;
	}
	update();
}

///
/// \brief		Sets up cameras on the vertices, edges and faces of a cube
///				surrounding the data
/// \author		Dean
/// \since		17-07-2015
///
void QtIsosurfaceWidget::setupFixedCameras()
{
	assert(m_heightField != nullptr);

	float maxX = m_heightField->GetMaxX();
	float maxY = m_heightField->GetMaxY();
	float maxZ = m_heightField->GetMaxZ();

	float midX = m_heightField->GetMaxX() / 2.0;
	float midY = m_heightField->GetMaxY() / 2.0;
	float midZ = m_heightField->GetMaxZ() / 2.0;

	float negX = -maxX;
	float negY = -maxY;
	float negZ = -maxZ;

	float posX = 2.0 * maxX;
	float posY = 2.0 * maxY;
	float posZ = 2.0 * maxZ;

	QVector3D posV0 = QVector3D(negX, negY, negZ);
	QVector3D posV1 = QVector3D(posX, negY, negZ);
	QVector3D posV2 = QVector3D(negX, posY, negZ);
	QVector3D posV3 = QVector3D(posX, posY, negZ);
	QVector3D posV4 = QVector3D(negX, negY, posZ);
	QVector3D posV5 = QVector3D(posX, negY, posZ);
	QVector3D posV6 = QVector3D(negX, posY, posZ);
	QVector3D posV7 = QVector3D(posX, posY, posZ);

	m_presetCamera[CameraPreset::VERTEX_CAM_V0] = new QtCamera(posV0, getMidpointOfData(), "Camera v0");
	m_presetCamera[CameraPreset::VERTEX_CAM_V1] = new QtCamera(posV1, getMidpointOfData(), "Camera v1");
	m_presetCamera[CameraPreset::VERTEX_CAM_V2] = new QtCamera(posV2, getMidpointOfData(), "Camera v2");
	m_presetCamera[CameraPreset::VERTEX_CAM_V3] = new QtCamera(posV3, getMidpointOfData(), "Camera v3");
	m_presetCamera[CameraPreset::VERTEX_CAM_V4] = new QtCamera(posV4, getMidpointOfData(), "Camera v4");
	m_presetCamera[CameraPreset::VERTEX_CAM_V5] = new QtCamera(posV5, getMidpointOfData(), "Camera v5");
	m_presetCamera[CameraPreset::VERTEX_CAM_V6] = new QtCamera(posV6, getMidpointOfData(), "Camera v6");
	m_presetCamera[CameraPreset::VERTEX_CAM_V7] = new QtCamera(posV7, getMidpointOfData(), "Camera v7");

	QVector3D posE0 = QVector3D(midX, negY, negZ);
	QVector3D posE1 = QVector3D(negX, midY, negZ);
	QVector3D posE2 = QVector3D(midX, posY, negZ);
	QVector3D posE3 = QVector3D(posX, midY, negZ);
	QVector3D posE4 = QVector3D(midX, negY, posZ);
	QVector3D posE5 = QVector3D(negX, midY, posZ);
	QVector3D posE6 = QVector3D(midX, posY, posZ);
	QVector3D posE7 = QVector3D(posX, midY, posZ);
	QVector3D posE8 = QVector3D(negX, negY, midZ);
	QVector3D posE9 = QVector3D(negX, posY, midZ);
	QVector3D posE10 = QVector3D(posX, posY, midZ);
	QVector3D posE11 = QVector3D(posX, negY, midZ);

	m_presetCamera[CameraPreset::EDGE_CAM_E0] = new QtCamera(posE0, getMidpointOfData(), "Camera e0");
	m_presetCamera[CameraPreset::EDGE_CAM_E1] = new QtCamera(posE1, getMidpointOfData(), "Camera e1");
	m_presetCamera[CameraPreset::EDGE_CAM_E2] = new QtCamera(posE2, getMidpointOfData(), "Camera e2");
	m_presetCamera[CameraPreset::EDGE_CAM_E3] = new QtCamera(posE3, getMidpointOfData(), "Camera e3");
	m_presetCamera[CameraPreset::EDGE_CAM_E4] = new QtCamera(posE4, getMidpointOfData(), "Camera e4");
	m_presetCamera[CameraPreset::EDGE_CAM_E5] = new QtCamera(posE5, getMidpointOfData(), "Camera e5");
	m_presetCamera[CameraPreset::EDGE_CAM_E6] = new QtCamera(posE6, getMidpointOfData(), "Camera e6");
	m_presetCamera[CameraPreset::EDGE_CAM_E7] = new QtCamera(posE7, getMidpointOfData(), "Camera e7");
	m_presetCamera[CameraPreset::EDGE_CAM_E8] = new QtCamera(posE8, getMidpointOfData(), "Camera e8");
	m_presetCamera[CameraPreset::EDGE_CAM_E9] = new QtCamera(posE9, getMidpointOfData(), "Camera e9");
	m_presetCamera[CameraPreset::EDGE_CAM_E10] = new QtCamera(posE10, getMidpointOfData(), "Camera e10");
	m_presetCamera[CameraPreset::EDGE_CAM_E11] = new QtCamera(posE11, getMidpointOfData(), "Camera e11");

	QVector3D posF0 = QVector3D(midX, midY, negZ);
	QVector3D posF1 = QVector3D(midX, negY, midZ);
	QVector3D posF2 = QVector3D(negX, midY, midZ);
	QVector3D posF3 = QVector3D(posX, midY, midZ);
	QVector3D posF4 = QVector3D(midX, posY, midZ);
	QVector3D posF5 = QVector3D(midX, midY, posZ);

	m_presetCamera[CameraPreset::FACE_CAM_F0_MIN_Z] = new QtCamera(posF0, getMidpointOfData(), "Camera f0");
	m_presetCamera[CameraPreset::FACE_CAM_F1_MIN_Y] = new QtCamera(posF1, getMidpointOfData(), "Camera f1");
	m_presetCamera[CameraPreset::FACE_CAM_F2_MIN_X] = new QtCamera(posF2, getMidpointOfData(), "Camera f2");
	m_presetCamera[CameraPreset::FACE_CAM_F3_MAX_X] = new QtCamera(posF3, getMidpointOfData(), "Camera f3");
	m_presetCamera[CameraPreset::FACE_CAM_F4_MAX_Y] = new QtCamera(posF4, getMidpointOfData(), "Camera f4");
	m_presetCamera[CameraPreset::FACE_CAM_F5_MAX_Z] = new QtCamera(posF5, getMidpointOfData(), "Camera f5");
}

void QtIsosurfaceWidget::SetNestedContourRange(const float min, const float max)
{
	assert(min <= max);

	m_nested_contour_range_min = min;
	m_nested_contour_range_max = max;

	update();
}

void QtIsosurfaceWidget::ResetNestedContourRange()
{
	assert(m_heightField != nullptr);

	m_nested_contour_range_min = m_heightField->GetMinHeight();
	m_nested_contour_range_max = m_heightField->GetMaxHeight();

	update();
}

void QtIsosurfaceWidget::SetHeightField3D(HeightField3D* heightfield)
{
	m_heightField = heightfield;

	if (m_heightField != nullptr)
	{
		//	By default we'll render nested contours for the entire heightfield range
		SetNestedContourRange(m_heightField->GetMinHeight(), m_heightField->GetMaxHeight());


		setupFixedCameras();

		//	HACK: we need to call this at after fixed camera have been created,
		//	this currently requires a heightfield to be set.  This should be
		//	moved to the construct once a better default camera setup is in
		//	place
		constructContextMenu();
	}
}






///
/// \brief		Takes a 2d point as used in screen space and projects to the
///				relative position in 3d space (the z coordinate can be
///				specified)
/// \param		pos
/// \return
/// \since		14-07-2015
/// \author		Dean
///
QVector3D QtIsosurfaceWidget::projectTo3dSpace(const QPoint &pos,
											   const float z) const
{
	//	Adapted from:
	//	http://webglfactory.blogspot.co.uk/2011/05/how-to-convert-world-to-screen.html
	//	[14-07-2015]
	double x = 2.0 * pos.x() / ((float)width() - 1.0);
	double y = 2.0 * pos.y() / ((float)height() + 1.0);

	QMatrix4x4 viewProjectionInverse =
			(m_projectionMatrix * m_currentCamera->GetViewMatrix()).inverted();

	QVector3D pos3d = QVector3D(x, y, z);

	return viewProjectionInverse * pos3d;
}


//void QtOpenGLShaderSurfaceRender::paintGL()
//{

//}

void QtIsosurfaceWidget::mousePressEvent(QMouseEvent *event)
{
	m_lastMousePosition = event->pos();
	event->accept();
}

void QtIsosurfaceWidget::mouseMoveEvent(QMouseEvent *event)
{
	/*
	float deltaX = (event->x() - m_lastMousePosition.x()) * (M_PI / 180.0);
	float deltaY = (event->y() - m_lastMousePosition.y()) * (M_PI / 180.0);

	//	TODO: move stepping / clamping code to Camera class

	if (event->buttons())
	{
		//	Yaw
		float cameraYawRads = m_camera.GetYaw();

		cameraYawRads -= deltaX;
		while (cameraYawRads < 0)
		{
			cameraYawRads += 2 * M_PI;
		}
		while (cameraYawRads >= 2 * M_PI)
		{
			cameraYawRads -= 2 * M_PI;
		}
		m_camera.SetYaw(cameraYawRads);

		//	Pitch
		float cameraPitchRads = m_camera.GetPitch();

		cameraPitchRads -= deltaY;
		while (cameraPitchRads < -(M_PI/2.0))
		{
			cameraPitchRads = -(M_PI/2.0);
		}
		while (cameraPitchRads > (M_PI/2.0))
		{
			cameraPitchRads = (M_PI/2.0);
		}
		m_camera.SetPitch(cameraPitchRads);

		update();
		//updateGL();
	}
	m_lastMousePosition = event->pos();
	event->accept();
	*/
}

void QtIsosurfaceWidget::wheelEvent(QWheelEvent *event)
{
	int delta = event->delta();

	if (event->orientation() == Qt::Vertical)
	{
		emit OnMouseScroll(delta);
	}
}

///
/// \brief		Provides a means for the user to manually enter an isovalue
/// \since		15-06-2015
/// \author		Dean
///
void QtIsosurfaceWidget::SetTargetIsovalue(const Real &value)
{
	m_targetIsovalue = value;

	update();

}

Real QtIsosurfaceWidget::GetTargetIsovalue() const
{
	return m_targetIsovalue;
}

void QtIsosurfaceWidget::StepCameraDistance(const int &delta)
{
	//	m_camera.StepDistance(delta);

	//	update();
}

///
/// \brief QtIsosurfaceWidget::getRenderableContours
/// \param isovalueMin
/// \param isovalueMax
/// \param steps
/// \return
/// \author		Dean
/// \since		22-03-2016
///
vector<Contour *> QtIsosurfaceWidget::getRenderableContours(const Real isovalueMin,
															const Real isovalueMax,
															const size_t steps) const
{
	assert(isovalueMin >= m_heightField->GetMinHeight());
	assert(isovalueMax <= m_heightField->GetMaxHeight());
	assert(isovalueMin <= isovalueMax);

	vector<Contour*> result;

	auto isoRange = isovalueMax - isovalueMin;
	auto stepSize =  isoRange / (float)steps;

	for (auto isovalue = isovalueMin; isovalue < isovalueMax; isovalue += stepSize)
	{
		auto targetIsovalueContours = getRenderableContours(isovalue);
#ifndef DISABLE_DEBUG_OUTPUT
		cout << "Retreived " << targetIsovalueContours.size()
			 << " for rendering." << endl;
#endif
		//	Concatenate the set of contours for the target isovalue to
		//	the result
		result.insert(result.end(),
					  targetIsovalueContours.begin(), targetIsovalueContours.end());
	}

	return result;
}

///
/// \brief		Builds a list of contours ready for rendering at the requested
///				isovalue
/// \param		isovalue
/// \return
/// \author		Dean
/// \since		03-08-2015
///
vector<Contour *> QtIsosurfaceWidget::getRenderableContours(const Real &isovalue) const
{
	vector<Contour *> result;

	//	All arcs that should be rendered
	auto superarcs =
			m_heightField->GetContourTree()->GetValidSuperarcsPresentAtIsovalue(isovalue);
#ifndef DISABLE_DEBUG_OUTPUT
	cout << "Number of contours valid at isovalue " << isovalue
		 << " is " << superarcs.size() << "." << endl;
#endif
	try
	{
		//	This will be the maximum number of contours returned
		result.reserve(superarcs.size());

		//	Loop over the number of visible contours and find the ones that are
		//	actual ready to be rendered.
		for (unsigned long c = 0; c < superarcs.size(); ++c)
		{
			//	Access the current contour (if ready)
			Contour* currentContour = superarcs[c]->RequestContour(isovalue);

			//	Don't count contours still being constructed
			if (currentContour != nullptr)
			{
				//	Also, ignore contours marked as hidden
				if (!currentContour->IsHiddenByUser())
				{
					//	And add to the render list
					result.push_back(currentContour);
				}
			}
		}

		return result;
	}
	catch (exception &ex)
	{
		//	We may run out of memory (BAD_ALLOC) or similar
		fprintf(stderr, "ERROR!  Caught exception %s in func: "
						"%s (file: %s, line: %ld).\n", ex.what(),
				__func__, __FILE__, __LINE__);
		fflush(stderr);

		return result;
	}
}

///
/// \brief QtIsosurfaceWidget::getIsovalueRange
/// \return
/// \author		Dean
/// \since		23-03-2016
///
QtIsosurfaceWidget::Range QtIsosurfaceWidget::getIsovalueRange() const
{
	//	Get the data range
	auto min = m_heightField->GetMinHeight();
	auto max = m_heightField->GetMaxHeight();

	return { min, max, (max - min) };
}

///
/// \brief QtIsosurfaceWidget::alphaTransferFunction
/// \param contour
/// \param isoRange
/// \return
/// \author		Dean
/// \since		23-03-2016
///
QtIsosurfaceWidget::decimal_type QtIsosurfaceWidget::alphaTransferFunction(Contour* const contour,
																		   const Range& isoRange) const
{
	if (contour->GetSuperarc()->IsSelected())
	{
		//	Selection has presidence over normal
		//	contour colouring
		return 1.0f;
	}
	else
	{
		return 0.5f;
	}
}

///
/// \brief QtIsosurfaceWidget::colourTransferFunction
/// \param contour
/// \param isoRange
/// \return
/// \author		Dean
/// \since		23-03-2016
///
QColor QtIsosurfaceWidget::colourTransferFunction(Contour* const contour,
												  const Range& isoRange) const
{
	if (contour->GetSuperarc()->IsHighlighted())
	{
		//	Highlighting has presidence over selection and normal rendering
		return m_contourHighlightedColour;
	}

	if (contour->GetSuperarc()->IsSelected())
	{
		//	Selection has presidence over normal contour colouring
		return m_contourSelectedColour;
	}

	switch (m_colourMode)
	{
	case ColourMode::DEFAULT:
		return m_contourDefaultColour;
	case ColourMode::HOT_COLD:
		if (contour->GetIsovalue() < 0.0)
		{
			return m_contourDefaultColourNegative;
		}
		else
		{
			return m_contourDefaultColourPositive;
		}
	case ColourMode::GRADIENT:
	{
		auto nIsovalue = MAX_COLOURS * normalize(isoRange.min,
												 isoRange.max,
												 contour->GetIsovalue());
		//cout << "Normalised isovalue is: " << nIsovalue << "." << endl;
		return m_colourTable[nIsovalue];
	}
	default:
		return m_contourDefaultColour;
	}

}

///
/// \brief		Returns a buffer object containing all of the data needed to
///				render using a call to 'GL_TRIANGLES'
/// \return		A full set of arrays for positions, normals and colours.  This
///				is allocated on the heap and will need to be disposed of
///				elsewhere.
/// \param      contours: a vector containing all contours that are intended
///				to be rendered
/// \since		26-05-2015
/// \author		Dean
///
QtIsosurfaceWidget::BufferData QtIsosurfaceWidget::generateTriangleBuffer(
		const vector<Contour*> &contours) const
{
#define SUPRESS_OUTPUT
	unsigned long vertexCount = 0;

	//	Make sure we allocated correctly
	BufferData result;

	//	Pre-compute the isovalue range so that it doesn't need re-computing
	//	for every contour
	auto isoRange = getIsovalueRange();

	//	First count the number of vertices required to render all the contours
	for (unsigned long i = 0; i < contours.size(); ++i)
	{
		Contour *currentContour = contours[i];

		//	There should be no null contours in this list!
		assert(currentContour != nullptr);

		//	3 vertices per triangle
		vertexCount += (3 *currentContour->GetTriangleCount());
	}

	//	Assuming that there is something to render
	if (vertexCount == 0)
	{
		return result;
	}
	else
	{
		try
		{
			//	Setup the vertices / normals / colour arrays, reserving enough
			//	space in each
			result.vertexArray.reserve(vertexCount);
			result.normalArray.reserve(vertexCount);
			result.colourArray.reserve(vertexCount);
			result.objectIdArray.reserve(vertexCount);

			//	Loop over the ready contours
			for (unsigned long i = 0; i < contours.size(); ++i)
			{
				//	Access the next contour
				Contour* currentContour = contours[i];

				//	There should be no null contours in this list!
				assert(currentContour != nullptr);

				//cout << "Contour has Euler characteristic "
				//<< currentContour->GetEulerCharacteristic() << endl;

				//	Ignore contours marked as hidden
				//	(they should already have been filtered out)
				if (!currentContour->IsHiddenByUser())
				{
					unsigned long t = currentContour->GetTriangleCount();

					for (unsigned long v = 0; v < t; ++v)
					{
						//	Select the next triangle in the mesh
						Triangle3D tri =
								currentContour->GetMeshTriangle(v)->GetUnscaledTriangle();
						Triangle3D vNormal =
								currentContour->GetMeshTriangle(v)->GetNormals();

						//	Add the vertices to the array
						result.vertexArray.push_back(QVector3D(tri.GetVertexA().GetX(),
															   tri.GetVertexA().GetY(),
															   tri.GetVertexA().GetZ()));
						result.vertexArray.push_back(QVector3D(tri.GetVertexB().GetX(),
															   tri.GetVertexB().GetY(),
															   tri.GetVertexB().GetZ()));
						result.vertexArray.push_back(QVector3D(tri.GetVertexC().GetX(),
															   tri.GetVertexC().GetY(),
															   tri.GetVertexC().GetZ()));

						//	Add the normals to the array
						result.normalArray.push_back(QVector3D(vNormal.GetVertexA().GetX(),
															   vNormal.GetVertexA().GetY(),
															   vNormal.GetVertexA().GetZ()));
						result.normalArray.push_back(QVector3D(vNormal.GetVertexB().GetX(),
															   vNormal.GetVertexB().GetY(),
															   vNormal.GetVertexB().GetZ()));
						result.normalArray.push_back(QVector3D(vNormal.GetVertexC().GetX(),
															   vNormal.GetVertexC().GetY(),
															   vNormal.GetVertexC().GetZ()));

						//	Compute the colour and alpha component using
						//	the transfer functions
						auto colour = colourTransferFunction(currentContour, isoRange);
						auto alpha = alphaTransferFunction(currentContour, isoRange);
						auto vertexColour = QColorToQVector4D(colour, alpha);

						//	Add the vertex colours to the array
						result.colourArray.push_back(vertexColour);
						result.colourArray.push_back(vertexColour);
						result.colourArray.push_back(vertexColour);

						//	Add the object ID to the array
						result.objectIdArray.push_back((float)currentContour->GetSuperarc()->GetId());
						result.objectIdArray.push_back((float)currentContour->GetSuperarc()->GetId());
						result.objectIdArray.push_back((float)currentContour->GetSuperarc()->GetId());
					}
				}
			}

			return result;
		}
		catch (exception &ex)
		{
			//	We may run out of memory (BAD_ALLOC) or similar
			fprintf(stderr, "ERROR!  Caught exception %s in func: "
							"%s (file: %s, line: %ld).\n", ex.what(),
					__func__, __FILE__, __LINE__);
			fflush(stderr);

			return result;
		}
	}
#undef SUPRESS_OUTPUT
}

///
/// \brief		Returns a buffer object containing all of the data needed to
///				render using a call to 'GL_LINES'
/// \return		A full set of arrays for positions and colours (these are
///				coded to connectivity).  This
///				is allocated on the heap and will need to be disposed of
///				elsewhere.
/// \param      contours: a vector containing all contours that are intended
///				to be rendered
/// \since		03-08-2015
/// \author		Dean
///
QtIsosurfaceWidget::BufferData *QtIsosurfaceWidget::generateLineBuffer(const vector<Contour*> &contours,
																	   const bool hide_closed_triangles) const
{
	//#define SUPRESS_OUTPUT

	unsigned long vertexCount = 0;
	BufferData *result = new BufferData();

	const QVector3D OPEN_EDGE_COLOR = QVector3D(1.0f, 0.0f, 0.0f);
	const QVector3D CLOSED_EDGE_COLOR = QVector3D(0.8f, 0.8f, 0.8f);

	//	Make sure we allocated correctly
	assert(result != nullptr);

	//	Loop over the number of visible contours and find the ones that are
	//	actual ready to be rendered.
	for (unsigned long i = 0; i < contours.size(); ++i)
	{
		//	Access the current contour
		Contour* currentContour = contours[i];

		//	Shouldn't be nullptr at this point
		assert(currentContour != nullptr);

		//	Get the number of edges in the mesh
		//	multiply by 2 (for the bounding vertices)
		vertexCount += (2 * currentContour->GetEdgeCount());
	}

	if (vertexCount == 0)
	{
		return result;
	}
	else
	{
		try
		{
			//	Setup the vertices / normals / colour arrays, reserving enough
			//	space in each
			result->vertexArray.reserve(vertexCount);
			//result->normalArray.reserve(vertexCount);
			result->colourArray.reserve(vertexCount);

			//	Loop over the ready contours
			for (unsigned long i = 0; i < contours.size(); ++i)
			{
				//	Access the next contour
				Contour* currentContour = contours[i];

				//	Shouldn't be nullptr at this point
				assert(currentContour != nullptr);

				//	Loop through all the edges in the current contour
				unsigned long edgeCount = currentContour->GetEdgeCount();
				for (unsigned long e = 0; e < edgeCount; ++e)
				{
					//	Get the next edge
					MeshEdge3D *edge = currentContour->m_edges[e];

					//	If we want we can choose to skip triangles that are marked as
					//	closed (i.e. on the main surface of a manifold.  This can be
					//	useful if we only wish to highlight periodic boundaries.
					if (hide_closed_triangles && (edge->triangles.size() != 1)) continue;

					//	Add the vertices to the array
					//	vA -> vB
					result->vertexArray.push_back(QVector3D(edge->vA->position.GetX(),
															edge->vA->position.GetY(),
															edge->vA->position.GetZ()));
					result->vertexArray.push_back(QVector3D(edge->vB->position.GetX(),
															edge->vB->position.GetY(),
															edge->vB->position.GetZ()));


					QVector3D edgeColour;

					//	Most edges should be connected to ekactly 2 triangles
					//	those connected to 1 are the boundary of holes in the
					//	mesh
					if (edge->triangles.size() == 1)
					{
						//	Red
						edgeColour = OPEN_EDGE_COLOR;
					}
					else
					{
						//	Very light grey
						edgeColour = CLOSED_EDGE_COLOR;
					}

					//	Add the colour for both vertices
					result->colourArray.push_back(edgeColour);
					result->colourArray.push_back(edgeColour);
				}

			}

			return result;
		}
		catch (exception &ex)
		{
			//	We may run out of memory (BAD_ALLOC) or similar
			fprintf(stderr, "ERROR!  Caught exception %s in func: "
							"%s (file: %s, line: %ld).\n", ex.what(),
					__func__, __FILE__, __LINE__);
			fflush(stderr);

			return result;
		}
	}
#undef SUPRESS_OUTPUT
}

void QtIsosurfaceWidget::SetRenderWireframe(const bool &value)
{
	m_renderWireframe = value;

	update();
}

///
/// \brief		Renders a spatial skeleton of the contour tree or Reeb graph
/// \param		tree: the input tree for rendering
/// \author		Dean
/// \since		22-03-2016
/// \author		Dean: make generic [01-07-16]
///
template <typename V, typename A>
void QtIsosurfaceWidget::renderReebGraphSkeleton(const TreeBase<V, A>* tree)
{
#ifndef DISABLE_DEBUG_OUTPUT
	cout << "Rendeirng Reeb graph" << endl;
#endif
	if (tree == nullptr)
	{
		qDebug() << "No Reeb graph available for rendering.";
		return;
	}

	assert(context() != nullptr);

	//	Our data streams to be sent to the shader
	QVector<QVector3D> positionArray;
	QVector<QVector4D> arcColourArray;
	QVector<QVector4D> vertexColourArray;

	assert(tree != nullptr);

	//	We will use this later...
	auto superarcColourTransferFunction =
			[&](A* const arc) -> QColor
	{
		//	1. Is the arc highlighted?
		if (arc->IsHighlighted()) return m_contourHighlightedColour;

		//	2.	Is the arc selected?
		if (arc->IsSelected()) return m_contourSelectedColour;

		//	3.	Use the default
		return m_skeletonLineColour;
	};

	//	And this...
	auto supernodeColourTransferFunction =
			[&](V* const node) -> QColor
	{
		assert(node->GetDegree() > 0);

		switch (node->GetDegree())
		{
		case 1:
			//	Leaf node
			return Qt::green;
		case 2:
			//	Regular vertex
			//	This shouldn't happen in the contour tree
			//assert(false);
			return Qt::black;
		case 3:
			//	Most common type of node
			return Qt::yellow;
			//	Highly connected vertices
		default:
			return Qt::red;
		}
	};

	//	Loop over all the edges/nodes in the contour tree
	for (auto edgeIt = tree->Superarcs_cbegin();
		 edgeIt != tree->Superarcs_cend();
		 ++edgeIt)
	{
		//	Retreive the bounding graph vertices
		auto topTreeVertex = (*edgeIt)->GetTopSupernode();
		auto bottomTreeVertex = (*edgeIt)->GetBottomSupernode();
		assert(topTreeVertex != nullptr);
		assert(bottomTreeVertex != nullptr);

		//	Retreive a pointer into the heightfield
		auto posTop = topTreeVertex->GetPosition();
		auto posBottom = bottomTreeVertex->GetPosition();

		//	Add to the vectors
		positionArray << QVector3D(get<0>(posTop),
								   get<1>(posTop),
								   get<2>(posTop));
		positionArray << QVector3D(get<0>(posBottom),
								   get<1>(posBottom),
								   get<2>(posBottom));

		//	Choose the colour for the arc line depending on if the object
		//	has been selected by the user.  Add the colour to the stream
		auto superarcRenderColour = superarcColourTransferFunction(*edgeIt);
		arcColourArray << QColorToQVector4D(superarcRenderColour, m_skeletonLineOpacity);
		arcColourArray << QColorToQVector4D(superarcRenderColour, m_skeletonLineOpacity);

		//	Choose the correct colour for drawing the vertices and add to the
		//	stream
		auto topSupernodeRenderColour = supernodeColourTransferFunction(topTreeVertex);
		auto bottomSupernodeRenderColour = supernodeColourTransferFunction(bottomTreeVertex);
		vertexColourArray << QColorToQVector4D(topSupernodeRenderColour);
		vertexColourArray << QColorToQVector4D(bottomSupernodeRenderColour);
	}

	//	Draw the arcs and vertices
	executeLineDrawingShader(positionArray, arcColourArray, m_skeletonLineThickness);
	executePointDrawingShader(positionArray, vertexColourArray, m_skeletonVertexSize);
}

void QtIsosurfaceWidget::SetRenderReebGraphSkeleton(const bool value)
{
	m_reebGraphSkeleton = value;
	update();
}
