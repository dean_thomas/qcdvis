#ifndef QT_ISOSURFACE_WIDGET_H
#define QT_ISOSURFACE_WIDGET_H

#include <QtOpenGL>
#include "HeightField3D.h"
#include "../QtCamera.h"
#include <QtGlobal>
#include <Contour/Storage/Contour.h>
#include "GUI/Forms/QtOriginOffsetDialog.h"
#include <memory>
#include "Graphs/ContourTree.h"
#include "Graphs/TreeBase.h"
#include <array>;

#include "../QtAbstract3dRenderedView.h"

//	Colour-blind safe colours
//	see: http://jfly.iam.u-tokyo.ac.jp/color/ [31-03-2016]
#define QCOLOR_CB_SAFE_BLUISH_GREEN QColor(0, 158, 115)
#define QCOLOR_CB_SAFE_YELLOW QColor(240, 228, 66)
#define QCOLOR_CB_SAFE_SKY_BLUE QColor(86, 180, 233)
#define QCOLOR_CB_SAFE_ORANGE QColor(230, 159, 0)
#define QCOLOR_CB_SAFE_VERMILLION QColor(213, 94, 0)

class QtIsosurfaceWidget : public QtAbstract3dRenderedView
{
		Q_OBJECT
		static const int MAX_COLOURS = 256;

		using LookupTable = QVector<QColor>;
		//using SuperarcContainer = ContourTree::SuperarcContainer;
		using size_type = size_t;
		using decimal_type = float;

		//	Todo: reuse the class defined in QtAbstract3dView...
		struct AxisData
		{
				QString label;
				decimal_type min;
				decimal_type max;

		};

		//	Todo: reuse the class defined in QtAbstract3dView...
		struct ImageOverlay
		{
				QImage image;
				AxisData xAxis;
				AxisData yAxis;
		};

		struct Range
		{
				decimal_type min;
				decimal_type max;
				decimal_type range;
		};

	public:
		enum class OverlayPosition
		{
			TOP_LEFT,
			TOP_RIGHT,
			BOTTOM_LEFT,
			BOTTOM_RIGHT
		};

		enum class CameraPreset
		{
			FACE_CAM_F0_MIN_Z,
			FACE_CAM_F1_MIN_Y,
			FACE_CAM_F2_MIN_X,
			FACE_CAM_F3_MAX_X,
			FACE_CAM_F4_MAX_Y,
			FACE_CAM_F5_MAX_Z,

			EDGE_CAM_E0,
			EDGE_CAM_E1,
			EDGE_CAM_E2,
			EDGE_CAM_E3,
			EDGE_CAM_E4,
			EDGE_CAM_E5,
			EDGE_CAM_E6,
			EDGE_CAM_E7,
			EDGE_CAM_E8,
			EDGE_CAM_E9,
			EDGE_CAM_E10,
			EDGE_CAM_E11,

			VERTEX_CAM_V0,
			VERTEX_CAM_V1,
			VERTEX_CAM_V2,
			VERTEX_CAM_V3,
			VERTEX_CAM_V4,
			VERTEX_CAM_V5,
			VERTEX_CAM_V6,
			VERTEX_CAM_V7

		};

		enum class ColourMode
		{
			//	Use the same colour for all contours
			DEFAULT,
			//	Use the colour specifed by the contours superarc
			PER_CONTOUR,
			//	Contours are coloured according to superarc midpoint (+ve/-ve)
			HOT_COLD,
			//	The colour is specified by a colour ramp as a function of
			//	isovalue
			GRADIENT
		};

		//	Constructors / destructors
		QtIsosurfaceWidget(QWidget *parent = 0);
		~QtIsosurfaceWidget();

	private:
		using UPtrQMenu = std::unique_ptr<QMenu>;
		using UPtrQAction = std::unique_ptr<QAction>;

		//	Group together pointers for the context menu
		struct ContextMenu
		{
				UPtrQMenu contextMenu = nullptr;
				UPtrQAction offsetDialogAction = nullptr;
		} m_contextMenu;

		//	Range of isovalues for nested contours (allowing us to isolate a
		//	single object, for example)
		float m_nested_contour_range_min = 0.0f;
		float m_nested_contour_range_max = 0.0f;
	public:
		//	Accessors
		//	General properties
		//	==================
		void SetColourTable(const LookupTable& value);
		LookupTable GetColourTable() const { return m_colourTable; }

		void SetIsosurfaceColour(const QColor& value);
		QColor GetIsosurfaceColour() const { return m_contourDefaultColour; }

		void SetIsosurfaceColourPositive(const QColor& value);
		QColor GetIsosurfaceColourPositive() const { return m_contourDefaultColourPositive; }

		void SetIsosurfaceColourNegative(const QColor& value);
		QColor GetIsosurfaceColourNegative() const { return m_contourDefaultColourNegative; }

		void SetColourMode(const ColourMode& value);
		ColourMode GetColourMode() const { return m_colourMode; }

		//	Renders axis at the origin
		void SetRenderAxisLines(const bool &value);
		bool GetRenderAxisLines() const { return m_renderAxisLines; }

		//	Extends axis at the origin to surround the whole volume
		void SetRenderFarAxisLines(const bool &value);
		bool GetRenderFarAxisLines() const { return m_renderFarAxisLines; }

		//	Render labels on the axis lines
		void SetRenderAxisLabels(const bool &value);
		bool GetRenderAxisLabels() const { return m_renderAxisLabels; }

		//	Renders the grid of data points
		bool GetRenderGrid() const { return m_renderGrid; }
		void SetRenderGrid(const bool &value);

		//	Only renders the outer edges of the data grid
		bool GetRenderOuterCellsOnly() const { return m_renderGridOuterOnly; }
		void SetRenderOuterGridOnly(const bool &value);

		//	Sets the model matrix to the specified value
		void SetModelMatrix(const QMatrix4x4 &modelMatrix);

		//	Sets wether wireframe rendering should be enabled
		void SetRenderWireframe(const bool &value);
		bool GetRenderWireframe() const { return m_renderWireframe; }

		//	If enable the wireframe will display information about
		//	edge connectivity
		void SetRenderEdgeConnectivityWireframe(const bool value);
		bool GetRenderEdgeConnectivityWireframe() const { return m_renderEdgeConnectivityWireframe; }

		void SetRenderEdgeConnectivityForHolesOnly(const bool value);
		bool GetRenderEdgeConnectivityForHolesOnly() const { return m_renderMeshHolesWireframeOnly; }

		//	Sets the heightfield data source for the render
		void SetHeightField3D(HeightField3D* heightfield);

		//	Sets the target isovalue for the render
		void SetTargetIsovalue(const Real &value);
		Real GetTargetIsovalue() const;

		void SetRenderContourTreeSkeleton(const bool value);
		bool GetRenderContourTreeSkeleton() const { return m_contourTreeSkeleton; }

		void SetRenderReebGraphSkeleton(const bool value);
		bool GetRenderReebGraphSkeleton() const { return m_reebGraphSkeleton; }

		void SetNestedContourRange(const float min, const float max);
		void ResetNestedContourRange();

		//	Enables / disables display of contours as surfaces
		void SetRenderSurfaces(const bool value);
		bool GetRenderSurfaces() const { return m_renderSurfaces; }

		//	Enables / disables display of contour caps on boundaries
		void SetRender3dPolygonCaps(const bool value);
		bool GetRender3dPolygonCaps() const { return m_render3dPolygonCaps; }

		//	Enables / disables display of contour caps on boundaries
		void SetRender2dPolygonCaps(const HeightField3D::Boundary &targetBoundary,
									const bool value);
		bool GetRender2dPolygonCaps() const { return m_render2dPolygonCapsMinZ; }

		//	Sets the shader program to be used for rendering
		bool SetShaderProgram(const QString &name);
		string GetShaderProgram() const { return m_shaderFilename; }

		 QVector3D getLookAtPoint() const override { return getMidpointOfData(); }
		//void SetAntiAliasedLines(const bool& value) { m_antiAliasedLines = true; }
		//bool GetAntiAliasedLines() const { return m_antiAliasedLines; }

		template <typename V, typename A>
		void renderReebGraphSkeleton(const TreeBase<V, A>* tree);

		template <typename V, typename A>
		void renderSkeletonGlyphOverlay(QPainter* painter, const TreeBase<V, A>* tree,
										const float overlay_width,
										const float overlay_height);
	public:
		QImage ObtainScreenShot(const QSize& dimensions,
								const QColor& backgroundColour);

		//	General functions
		void StepCameraDistance(const int &delta);
		void IncrementCamera();

		QImage renderMainview(const size_type& width,
							  const size_type& height);
		QImage renderSubview(const size_type& width,
							 const size_type& height,
							 const CameraPreset& cameraPreset);

		void SetAlphaBlended(const bool value);
		bool GetAlphaBlended() const { return m_alphaBlended; }

		void SetNestedContourCount(const size_t value);
		size_t GetNestedContourCount() const { return m_numberOfNestedContours; }

		void SetNestedContours(const bool value);
		bool GetNestedContour() const { return m_nestedContours; }

		void SetRenderSkeletonGlyphs(const bool value);
		bool GetRenderSkeletonGlyphs() const { return m_renderSkeletonGlyphs; }

		void SetGridLineThickness(const float thickness)
		{
			m_gridLineThickness = thickness;
			std::cout << "Grid line thickness is now " << thickness
					  << std::endl;
			repaint();
		}
		float GetGridLineThickness() const
		{
			return m_gridLineThickness;
		}

		void SetGridLineColour(const QColor& color)
		{
			m_gridLineColor = color;
			repaint();
		}
		QColor GetGridLineColour() const
		{
			return m_gridLineColor;
		}

	protected:
		void initializeGL() override;

		void paintEvent(QPaintEvent *event) override;

		virtual void mousePressEvent(QMouseEvent *event);
		virtual void mouseMoveEvent(QMouseEvent *event);
		virtual void wheelEvent(QWheelEvent *event);

	private:
		Range getIsovalueRange() const;

		void renderCornerOverlay(QPainter* painter,
								 const OverlayPosition& pos,
								 const ImageOverlay& overlay);

		void constructContextMenu();

		decimal_type alphaTransferFunction(Contour* const contour,
											const Range& isoRange) const;
		QColor colourTransferFunction(Contour* const contour,
									  const Range& isoRange) const;

		LookupTable m_colourTable;

		float calculateSubviewSize() const;

		ColourMode m_colourMode = ColourMode::DEFAULT;
		QColor m_contourSelectedColour = QCOLOR_CB_SAFE_BLUISH_GREEN;
		QColor m_contourHighlightedColour = QCOLOR_CB_SAFE_VERMILLION;
		QColor m_contourDefaultColour = QColor::fromRgbF(0.0, 0.8, 0.0, 1.0);
		QColor m_contourDefaultColourPositive = QCOLOR_CB_SAFE_ORANGE;
		QColor m_contourDefaultColourNegative = QCOLOR_CB_SAFE_SKY_BLUE;

		//	Size of the sub-views relative to the overall widget size
		size_t m_subViewPercentage = 30;

		vector<Contour *> getRenderableContours(const Real isovalueMin, const Real isovalueMax, const size_t steps) const;
		vector<Contour *> getRenderableContours(const Real &isovalue) const;


		QVector3D getMidpointOfData() const;

		QVector3D projectTo3dSpace(const QPoint &pos, const float z = 0.0) const;

		size_t m_axisThickness = 2.0f;
		QFont m_axisLabelFont = QFont("Arial", 20, 1, false);

		//	Grid properties
		//	===============
		//	By default only draw the outer cells of the grid
		bool m_renderGrid = true;
		bool m_renderGridOuterOnly = true;

		//	Axis legend (top-left corner)
		bool m_renderAxisLegend = true;
		QColor m_axisLegendLineColor = Qt::black;
		float m_axisLegendLineWidth = 2.0f;
		float m_axisLegendLineLength = 25.0f;
		QPoint m_legendCentrePoint = QPoint(50,50);

		//bool m_antiAliasedLines;

		bool m_renderMoments = false;

		bool m_renderEdgeConnectivityWireframe = false;
		bool m_renderMeshHolesWireframeOnly = true;
		bool m_renderWireframe = false;

		bool m_renderSurfaces = true;

		bool m_contourTreeSkeleton = false;
		bool m_reebGraphSkeleton = false;
		bool m_renderSkeletonGlyphs = true;

		bool m_render3dPolygonCaps = false;
		bool m_render2dPolygonCapsMinX = false;
		bool m_render2dPolygonCapsMaxX = false;
		bool m_render2dPolygonCapsMinY = false;
		bool m_render2dPolygonCapsMaxY = false;
		bool m_render2dPolygonCapsMinZ = false;
		bool m_render2dPolygonCapsMaxZ = false;


		QColor m_skeletonLineColour = Qt::darkBlue;
		float m_skeletonLineOpacity = 0.5f;
		float m_skeletonLineThickness = 1.5f;
		float m_skeletonVertexSize = 5.0f;

		//	Axis properties
		//	===============
		//	By default draw all axes, including the far ones and label them
		bool m_renderAxisLines = true;
		bool m_renderFarAxisLines = true;
		bool m_renderAxisLabels = true;

		string m_shaderFilename;

		float m_gridLineThickness = 1.0f;


		void setCamera(QtCamera* camera);
		void setupFixedCameras();

		//void renderAxes();
		//void renderAxisLabels(QPainter *painter);
		void renderAxisLegend(QPainter *painter);

		void renderOuterGrid();
		void renderFullGrid();

		void renderWireframe(BufferData *vertexBuffer);

		void renderMoments();
		//void renderSurfaces(const BufferData& bufferData);
		void render3dPolygonCaps();
		void render2dPolygonCaps(const HeightField3D::Boundary &targetBoundary);

		void renderPipeline(const QColor& backgroundColour);
		void renderOverlay(const ImageOverlay& topRightOverlay,
						   const ImageOverlay& bottomLeftOverlay,
						   const ImageOverlay& bottomRightOverlay);


		BufferData generateTriangleBuffer(const vector<Contour *> &contours) const;
		BufferData *generateLineBuffer(const vector<Contour*> &contours,
									   const bool hide_closed_triangles) const;

		Real m_targetIsovalue = 0.0f;

		QColor m_backgroundColor = Qt::white;
		QColor m_gridLineColor = QColor(127, 127, 127);

		std::map<CameraPreset, QtCamera*> m_presetCamera;
		unsigned long m_presetCameraIndex = 0;

		QPoint m_lastMousePosition = { 0.0f, 0.0f };


		QGLShaderProgram *m_mainShaderProgram = nullptr;


		HeightField3D *m_heightField = nullptr;

		bool m_alphaBlended = false;
		bool m_nestedContours = false;
		size_t m_numberOfNestedContours = 50;

		//	Small helper function to break the need for the abstract base needing
		//	to know more than is needed about the heightfield
		Axis3 getAxisData(HeightField3D* const heightField) const
		{
			Axis xAxis = { { 0.0, heightField->GetMaxX() },
						   QString("%1").arg(heightField->GetAxisLabelX()) };
			Axis yAxis = { { 0.0, heightField->GetMaxY() },
						   QString("%1").arg(heightField->GetAxisLabelY()) };
			Axis zAxis = { { 0.0, heightField->GetMaxZ() },
						   QString("%1").arg(heightField->GetAxisLabelZ()) };

			return { xAxis, yAxis, zAxis };
		}
	private slots:
		void showDialog();

	signals:
		void OnMouseScroll(int delta);
};

#endif // QT_ISOSURFACE_WIDGET_H
