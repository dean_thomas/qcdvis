///////////////////////////////////////////////////
//
//	Contour-tree based isosurfaces
//	Version 4.0 (QT-based)
//	copyright Hamish Carr
//	January, 2014
//
//	------------------------
//	ArcBallWidget.cpp
//	------------------------
//
//	An arcball widget that modifies a stored quaternion
//	using Ken Shoemake's arcball code
//
///////////////////////////////////////////////////

#define ARCBALL_WIDGET_SIZE 50
#define ARCBALL_SIZE 40

#include "GUI/Widgets/QtArcBallWidget/QtArcBallWidget.h"

GLfloat sphereVert[84][3] = {	{	0,	0,	-1	},
								{	0.5,	0,	-0.86603	},
								{	0.86603,	0,	-0.5	},
								{	1,	0,	0	},
								{	0.86603,	0,	0.5	},
								{	0.5,	0,	0.86603	},
								{	0,	0,	1	},
								{	0,	0,	-1	},
								{	0.43301,	0.25,	-0.86603	},
								{	0.75,	0.43301,	-0.5	},
								{	0.86603,	0.5,	0	},
								{	0.75,	0.43301,	0.5	},
								{	0.43301,	0.25,	0.86603	},
								{	0,	0,	1	},
								{	0,	0,	-1	},
								{	0.25,	0.43301,	-0.86603	},
								{	0.43301,	0.75,	-0.5	},
								{	0.5,	0.86603,	0	},
								{	0.43301,	0.75,	0.5	},
								{	0.25,	0.43301,	0.86603	},
								{	0,	0,	1	},
								{	0,	0,	-1	},
								{	0,	0.5,	-0.86603	},
								{	0,	0.86603,	-0.5	},
								{	0,	1,	0	},
								{	0,	0.86603,	0.5	},
								{	0,	0.5,	0.86603	},
								{	0,	0,	1	},
								{	0,	0,	-1	},
								{	-0.25,	0.43301,	-0.86603	},
								{	-0.43301,	0.75,	-0.5	},
								{	-0.5,	0.86603,	0	},
								{	-0.43301,	0.75,	0.5	},
								{	-0.25,	0.43301,	0.86603	},
								{	0,	0,	1	},
								{	0,	0,	-1	},
								{	-0.43301,	0.25,	-0.86603	},
								{	-0.75,	0.43301,	-0.5	},
								{	-0.86603,	0.5,	0	},
								{	-0.75,	0.43301,	0.5	},
								{	-0.43301,	0.25,	0.86603	},
								{	0,	0,	1	},
								{	0,	0,	-1	},
								{	-0.5,	0,	-0.86603	},
								{	-0.86603,	0,	-0.5	},
								{	-1,	0,	0	},
								{	-0.86603,	0,	0.5	},
								{	-0.5,	0,	0.86603	},
								{	0,	0,	1	},
								{	0,	0,	-1	},
								{	-0.43301,	-0.25,	-0.86603	},
								{	-0.75,	-0.43301,	-0.5	},
								{	-0.86603,	-0.5,	0	},
								{	-0.75,	-0.43301,	0.5	},
								{	-0.43301,	-0.25,	0.86603	},
								{	0,	0,	1	},
								{	0,	0,	-1	},
								{	-0.25,	-0.43301,	-0.86603	},
								{	-0.43301,	-0.75,	-0.5	},
								{	-0.5,	-0.86603,	0	},
								{	-0.43301,	-0.75,	0.5	},
								{	-0.25,	-0.43301,	0.86603	},
								{	0,	0,	1	},
								{	0,	0,	-1	},
								{	0,	-0.5,	-0.86603	},
								{	0,	-0.86603,	-0.5	},
								{	0,	-1,	0	},
								{	0,	-0.86603,	0.5	},
								{	0,	-0.5,	0.86603	},
								{	0,	0,	1	},
								{	0,	0,	-1	},
								{	0.25,	-0.43301,	-0.86603	},
								{	0.43301,	-0.75,	-0.5	},
								{	0.5,	-0.86603,	0	},
								{	0.43301,	-0.75,	0.5	},
								{	0.25,	-0.43301,	0.86603	},
								{	0,	0,	1	},
								{	0,	0,	-1	},
								{	0.43301,	-0.25,	-0.86603	},
								{	0.75,	-0.43301,	-0.5	},
								{	0.86603,	-0.5,	0	},
								{	0.75,	-0.43301,	0.5	},
								{	0.43301,	-0.25,	0.86603	},
								{	0,	0,	1	}
							};

///
/// \brief		Resets angle/radius of the ball
/// \author		Dean
/// \since		28-08-2015
///
void ArcBallWidget::reset()
{
	m_radius = INITIAL_RADIUS;

	Ball_Init(&theBall);
}

///
/// \brief		Returns the vector part of the rotation quaternion
/// \return
/// \author		Dean
/// \since		10-07-2015
///
QVector3D ArcBallWidget::GetVector() const
{
	return QVector3D(theBall.qNow.x, theBall.qNow.y, theBall.qNow.z);
}

///
/// \brief		Returns the current rotation as a row major 4x4 matrix
/// \return
/// \author		Dean
/// \since		10-07-2015
///
QMatrix4x4 ArcBallWidget::GetMatrixAsRowMajorMatrix() const
{
	return QMatrix4x4(theBall.mNow[0][0],
			theBall.mNow[1][0],
			theBall.mNow[2][0],
			theBall.mNow[3][0],
			theBall.mNow[0][1],
			theBall.mNow[1][1],
			theBall.mNow[2][1],
			theBall.mNow[3][1],
			theBall.mNow[0][2],
			theBall.mNow[1][2],
			theBall.mNow[2][2],
			theBall.mNow[3][2],
			theBall.mNow[0][3],
			theBall.mNow[1][3],
			theBall.mNow[2][3],
			theBall.mNow[3][3]);
}

///
/// \brief		Returns the current rotation as a column major 4x4 matrix
/// \return
/// \author		Dean
/// \since		10-07-2015
///
QMatrix4x4 ArcBallWidget::GetMatrixAsColumnMajorMatrix() const
{
	return QMatrix4x4(theBall.mNow[0][0],
			theBall.mNow[0][1],
			theBall.mNow[0][2],
			theBall.mNow[0][3],
			theBall.mNow[1][0],
			theBall.mNow[1][1],
			theBall.mNow[1][2],
			theBall.mNow[1][3],
			theBall.mNow[2][0],
			theBall.mNow[2][1],
			theBall.mNow[2][2],
			theBall.mNow[2][3],
			theBall.mNow[3][0],
			theBall.mNow[3][1],
			theBall.mNow[3][2],
			theBall.mNow[3][3]);
}

///
/// \brief		Returns the current rotation as a quaternion
/// \return
/// \author		Dean
/// \since		10-07-2015
///
QQuaternion ArcBallWidget::GetQuaternion() const
{
	return QQuaternion(theBall.qNow.w, theBall.qNow.x, theBall.qNow.y, theBall.qNow.z);
}

void ArcBallWidget::SetQuaternion(const QQuaternion& quat)
{
	theBall.qNow.w = quat.scalar();
	theBall.qNow.x = quat.x();
	theBall.qNow.y = quat.y();
	theBall.qNow.z = quat.z();

	Ball_Update(&theBall);
	update();
}

void ArcBallWidget::SetRadius(const float radius)
{
	m_radius = radius;
	update();
}

ArcBallWidget::ArcBallWidget(QWidget *parent)
	: QGLWidget(parent)
{
	setMinimumHeight(ARCBALL_WIDGET_SIZE);

	//setMaximumHeight(ARCBALL_WIDGET_SIZE);
	setMinimumWidth(ARCBALL_WIDGET_SIZE);
	//setMaximumWidth(ARCBALL_WIDGET_SIZE);

	QSizePolicy qsp(QSizePolicy::Preferred,QSizePolicy::Preferred);
	qsp.setHeightForWidth(true);
	qsp.setWidthForHeight(true);
	setSizePolicy(qsp);

	//	Set our starting parameters (ball size, orintation etc)
	reset();

	// place it at 80% of widget size
	Ball_Place(&theBall, qOne, 0.80);

	//	Set the backgronud colour to be used by OpenGL
	m_backgorundColour = QColor(Qt::white);

	//	Respond to scroll for changing the radius attribute
	connect(this, SIGNAL(wheelEvent(QWheelEvent*)),
			this, SLOT(OnMouseScroll(QWheelEvent*)));


}


void ArcBallWidget::copyRotation(GLfloat *target)
{
	// just invoke Ken Shoemake's code
	Ball_Value(&theBall, target);
}

///
/// \brief		Initializes OpenGL for the widget
/// \author		Hamish: original code
/// \author		Dean: modify to use variable backgorund colours [28-08-2015]
///
void ArcBallWidget::initializeGL()
{
	// no lighting, but we need depth test
	glDisable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);

	// background is white
	qglClearColor(m_backgorundColour);
}


void ArcBallWidget::resizeGL(int w, int h)
{
	// reset the viewport
	glViewport(0, 0, w, h);

	// set projection matrix to have range of -1.0 - 1.0 in x, y, z
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(-1.0, 1.0, -1.0, 1.0, -1.0, 1.0);
}


void ArcBallWidget::paintGL()
{
	makeCurrent();

	// clear the buffer
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// set model view matrix based on arcball rotation
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	// draw a single white quad to block back half of arcball
	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_QUADS);
	glVertex3f(-1.0, -1.0, 0.0);
	glVertex3f(1.0, -1.0, 0.0);
	glVertex3f(1.0, 1.0, 0.0);
	glVertex3f(-1.0, 1.0, 0.0);
	glEnd();

	// set colour to black
	glColor3f(0.0, 0.0, 0.0);

	// draw a circle around the edge of the sphere
	// reusing data from some of the elements of the sphere
	glScalef(0.8, 0.8, 0.8);
	glBegin(GL_LINE_LOOP);

	for (int j = 3; j < 84; j += 7)
		glVertex3f(sphereVert[j][0], sphereVert[j][1], 0.5);
	glEnd();

	// retrieve rotation from arcball & apply
	GLfloat mNow[16];
	Ball_Value(&theBall, mNow);
	glMultMatrixf(mNow);

	for (int i = 0; i < 12; i++)
	{
		// loop drawing verticals
		glBegin(GL_LINE_STRIP);
		for (int j = i*7; j < 7+i*7; j++)
			glVertex3fv(sphereVert[j]);
		glEnd();
	}

	for (int i = 1; i < 6; i++)
	{
		// loop for horizontals
		glBegin(GL_LINE_LOOP);
		for (int j = i; j < 84; j += 7)
			glVertex3fv(sphereVert[j]);
		glEnd();
	}
}

///
/// \brief		Responds to the user clicking the mouse
/// \param		event
/// \author		Hamish: original code
/// \author		Dean: define behaviour for double click events [28-08-2015]
///
void ArcBallWidget::mousePressEvent(QMouseEvent *event)
{
	//	Put into an easir to handle helper class
	QtMouseEventStruct mousePress = QtMouseEventStruct(event, width(), height());

	if (mousePress.LeftButton)
	{
		//	Double click to reset
		if (mousePress.DoubleClick)
		{
			reset();

			update();
		}
		else
		{
			//	Single click to drag
			// normalise the range & call
			BeginDrag(	(2.0 * event->x() - width()) / width(),
						(height() - 2.0 * event->y() ) / height()
						);
		}
	}
}

void ArcBallWidget::mouseMoveEvent(QMouseEvent *event)
{ // ArcBallWidget::mouseMoveEvent()
	// normalise the range & call
	ContinueDrag(	(2.0 * event->x() - width()) / width(),
					(height() - 2.0 * event->y() ) / height()
					);
} // ArcBallWidget::mouseMoveEvent()

void ArcBallWidget::mouseReleaseEvent(QMouseEvent *event)
{ // ArcBallWidget::mouseReleaseEvent()
	event->accept();

	EndDrag();
} // ArcBallWidget::mouseReleaseEvent()

// routines called to allow synchronised rotation with other widget
// coordinates are assumed to be in range of [-1..1] in x,y
// if outside that range, will be clamped
void ArcBallWidget::BeginDrag(float x, float y)
{ // ArcBallWidget::BeginDrag()
	// use the ArcBall's vector type
	HVect vNow;
	// compute scaled position
	vNow.x = x;
	vNow.y = y;
	// pass it to the arcball code
	Ball_Mouse(&theBall, vNow);
	// start dragging
	Ball_BeginDrag(&theBall);
	// update the widget
	updateGL();
	// and signal to indicate changed rotation matrix
	emit RotationChanged();
} // ArcBallWidget::BeginDrag()

void ArcBallWidget::ContinueDrag(float x, float y)
{ // ArcBallWidget::ContinueDrag()
	// use the ArcBall's vector type
	HVect vNow;
	// compute scaled position
	vNow.x = x;
	vNow.y = y;
	// pass it to the arcball code
	Ball_Mouse(&theBall, vNow);
	// start dragging
	Ball_Update(&theBall);
	// update the widget
	updateGL();
	// and signal to indicate changed rotation matrix
	emit RotationChanged();
} // ArcBallWidget::ContinueDrag()

void ArcBallWidget::EndDrag()
{ // ArcBallWidget::EndDrag()
	// end the drag
	Ball_EndDrag(&theBall);
	// update the widget
	updateGL();
	// and signal to indicate changed rotation matrix
	emit RotationChanged();
} // ArcBallWidget::EndDrag()

///
/// \brief		Steps the radius of the arc ball according to value delta
/// \param		delta: signed int
///	\since		04-08-2015
/// \author		Dean
///
void ArcBallWidget::StepRadius(const int &delta)
{
	if (delta > 0)
	{
		m_radius *= 0.9;

		printf("Scrolled radius is now: %f\n", m_radius);
		fflush(stdout);
	}
	else
	{
		m_radius *= 1.1;

		printf("Scrolled radius is now: %f\n", m_radius);
		fflush(stdout);
	}
	emit RadiusChanged();
}

///
/// \brief MDIParentWindow::OnArcBallCameraPositionScroll
/// \param event
/// \since		16-07-2015
/// \author		Dean
///
void ArcBallWidget::OnMouseScroll(QWheelEvent* event)
{
	if (event->orientation() == Qt::Vertical)
	{
		StepRadius(event->delta());
	}
}
