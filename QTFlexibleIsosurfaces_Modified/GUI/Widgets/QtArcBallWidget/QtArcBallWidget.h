///////////////////////////////////////////////////
//
//	Contour-tree based isosurfaces
//	Version 4.0 (QT-based)
//	copyright Hamish Carr
//	January, 2014
//
//	------------------------
//	ArcBallWidget.h
//	------------------------
//
//	An arcball widget that modifies a stored quaternion
//	using Ken Shoemake's arcball code
//
///////////////////////////////////////////////////

#ifndef _HEADER_ARCBALL_WIDGET_H
#define _HEADER_ARCBALL_WIDGET_H

//	Project headers
#include "Ball.h"
#include "../QtMouseEventStruct.h"

//	Stdlib headers
#include <array>

//	QtHeaders
#include <QtOpenGL>
#include <QGLWidget>
#include <QMouseEvent>
#include <QMatrix4x4>
#include <QVector3D>
#include <QSizePolicy>
#include <QColor>

using std::array;

class ArcBallWidget : public QGLWidget
{ // class ArcBallWidget
		Q_OBJECT
	private:
		static constexpr float INITIAL_RADIUS = 30.0f;

	public:
		QColor m_backgorundColour;

		BallData theBall;

		//	constructor
		ArcBallWidget(QWidget *parent);

		// routine to copy rotation matrix
		void copyRotation(GLfloat *target);

		QMatrix4x4 GetMatrixAsRowMajorMatrix() const;
		QMatrix4x4 GetMatrixAsColumnMajorMatrix() const;

		QQuaternion GetQuaternion() const;
		void SetQuaternion(const QQuaternion& quat);

		QVector3D GetVector() const;

		float GetRadius() const { return m_radius; }
		void SetRadius(const float radius);

		void StepRadius(const int &delta);

		void reset();
	protected:
		float m_radius;

		// called when OpenGL context is set up
		void initializeGL();
		// called every time the widget is resized
		void resizeGL(int w, int h);
		// called every time the widget needs painting
		void paintGL();


		// mouse-handling
		virtual void mousePressEvent(QMouseEvent *event);
		virtual void mouseMoveEvent(QMouseEvent *event);
		virtual void mouseReleaseEvent(QMouseEvent *event);

		virtual int heightForWidth(int w) const { return w;}
	private slots:
		void OnMouseScroll(QWheelEvent *event);

		//virtual void wheelEvent(QWheelEvent *event);
	public slots:
		// routines called to allow synchronised rotation with other widget
		// coordinates are assumed to be in range of [-1..1] in x,y
		// if outside that range, will be clamped
		void BeginDrag(float x, float y);
		void ContinueDrag(float x, float y);
		void EndDrag();


	public:
	signals:
		// slot for calling controller when rotation changes
		void RotationChanged();


		//	Passthrough from QWidget
		void wheelEvent(QWheelEvent *event);

		void RadiusChanged();
}; // class ArcBallWidget

#endif
