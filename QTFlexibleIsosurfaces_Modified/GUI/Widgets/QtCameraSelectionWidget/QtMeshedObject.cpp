#include "QtMeshedObject.h"

///
/// \brief		Creates an object linked to the specified mesh object
/// \param		mesh: the mesh representing the object
/// \param		translation: the offset from the world origin of the object
/// \param		scale: the scale of the object in x, y and z
/// \since		21-07-2015
/// \author		Dean
///
QtMeshedObject::QtMeshedObject(QtMesh *mesh, const QVector3D &translation, const QVector3D &scale)
	: QtMeshedObject(mesh, translation)
{
	SetScale(scale);
}

///
/// \brief		Creates an object linked to the specified mesh object
/// \param		mesh: the mesh representing the object
/// \param		translation: the offset from the world origin of the object
/// \since		21-07-2015
/// \author		Dean
///
QtMeshedObject::QtMeshedObject(QtMesh *mesh, const QVector3D &translation)
	: QtMeshedObject(mesh)
{
	SetTranslation(translation);
}

///
/// \brief		Creates an object linked to the specified mesh object
/// \param		mesh: the mesh representing the object
/// \param		translation: the offset from the world origin of the object
/// \param		scale: the scale of the object in x, y and z
/// \param		rotation: the rotation on the x, y and z axis
/// \param		radians: if true, the values given are in radians instead of
///				derees
/// \since		22-07-2015
/// \author		Dean
///
QtMeshedObject::QtMeshedObject(QtMesh *mesh,
							   const QVector3D &translation,
							   const QVector3D &scale,
							   const QVector3D &rotation,
							   const bool &radians)
	: QtMeshedObject(mesh, translation, scale)
{
	SetRotation(rotation, radians);
}

///
/// \brief		Recalculates the model matrix of the mesh
/// \return		QMatrix4x4: the new model matrix
/// \since		21-07-2015
/// \author		Dean
///
QMatrix4x4 QtMeshedObject::calculateModelMatrix() const
{
	QMatrix4x4 result;

	result.translate(m_translation);
	result.scale(m_scale);
	result *= m_rotationMatrix;

	return result;
}

///
/// \brief	QtMeshedObject::QtMeshedObject
/// \since	21-07-2015
/// \author	Dean
///
QtMeshedObject::QtMeshedObject()
{
	m_scale = QVector3D(1.0, 1.0, 1.0);
	m_translation = QVector3D(0.0, 0.0, 0.0);
}

///
/// \brief		Creates an object linked to the specified mesh object
/// \param		mesh: the mesh representing the object
/// \since		21-07-2015
/// \author		Dean
///
QtMeshedObject::QtMeshedObject(QtMesh *mesh)
	: QtMeshedObject()
{
	m_mesh = mesh;
}

///
/// \brief		Sets the objects position in world space
/// \param		translation: the offset from the world origin of the object
/// \since		21-07-2015
/// \author		Dean
///
void QtMeshedObject::SetTranslation(const QVector3D &translation)
{
	m_translation = translation;

	m_modelMatrix = calculateModelMatrix();
}

///
/// \brief		Sets the scaling factor of the object
/// \param		scale: the scale of the object in x, y and z
/// \since		21-07-2015
/// \author		Dean
///
void QtMeshedObject::SetScale(const QVector3D &scale)
{
	m_scale = scale;

	m_modelMatrix = calculateModelMatrix();
}

///
/// \brief		Sets the objects rotation using Euler angles
/// \param		rotation: the rotation on the x, y and z axis
/// \param		radians: if true, the values given are in radians instead of
/// \since		22-07-2015
/// \author		Dean
///
void QtMeshedObject::SetRotation(const QVector3D &rotation, const bool &radians)
{
	float alpha = rotation.x();
	float beta = rotation.y();
	float gamma = rotation.z();

	//	If we are in degrees, convert to radians
	if (!radians)
	{
		alpha *= (PI / 180.0);
		beta *= (PI / 180.0);
		gamma *= (PI / 180.0);

	}

	//	Create rotation matrices for each axis
	QMatrix4x4 rotX = QMatrix4x4(1,				0,				0,				0,
								 0,				cos(alpha),		-sin(alpha),	0,
								 0,				sin(alpha),		cos(alpha),		0,
								 0,				0,				0,				1);

	QMatrix4x4 rotY = QMatrix4x4(cos(beta),		0,				-sin(beta),		0,
								 0,				1,				0,				0,
								 sin(beta),		0,				cos(beta),		0,
								 0,				0,				0,				1);

	QMatrix4x4 rotZ = QMatrix4x4(cos(gamma),	-sin(gamma),	0,				0,
								 sin(gamma),	cos(gamma),		0,				0,
								 0,				0,				1 ,				0,
								 0 ,			0,				0,				1);

	//	Create the final rotation matrix
	m_rotationMatrix = rotZ * rotY * rotX;

	//	Recmpute the view matrix
	m_modelMatrix = calculateModelMatrix();
}
