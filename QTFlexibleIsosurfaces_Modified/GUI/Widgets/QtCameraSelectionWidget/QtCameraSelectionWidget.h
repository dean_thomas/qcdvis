#ifndef QT_CAMERA_SELECTION_WIDGET_H
#define QT_CAMERA_SELECTION_WIDGET_H

#include <QWidget>
#include <QPaintEvent>
#include <QPainter>
#include <QGLWidget>
#include <QVector3D>
#include <QVector>
#include <QtOpenGL>

#include <cassert>
#include "../QtMesh.h"
#include "GUI/Widgets/QtCamera.h"
#include "Globals/CompilerDefines.h"
#include "QtMeshedObject.h"

#define PI 3.14159265359

#define VERTEX_BUTTON_FRAC (1.0 / 6.0)
#define FACE_BUTTON_FRAC (1.0 - (VERTEX_BUTTON_FRAC))

#define SHADER_DIR "C:/Users/4thFloor/Documents/Qt/QtFlexibleIsosurfaces/QTFlexibleIsosurfaces_Modified/Shaders/"

#define BASIC_SHADER_VS "C:/Users/4thFloor/Documents/Qt/QtFlexibleIsosurfaces/QTFlexibleIsosurfaces_Modified/Shaders/basicShader.vs.glsl"
#define BASIC_SHADER_FS "C:/Users/4thFloor/Documents/Qt/QtFlexibleIsosurfaces/QTFlexibleIsosurfaces_Modified/Shaders/basicShader.fs.glsl"


namespace Ui {
	class CameraSelectionWidget;
}

class QtCameraSelectionWidget : public QGLWidget
{
		Q_OBJECT
	public:
		enum class CameraPreset
		{
			V_0,
			V_1,
			V_2,
			V_3,
			V_4,
			V_5,
			V_6,
			V_7,
			E_0,
			E_1,
			E_2,
			E_3,
			E_4,
			E_5,
			E_6,
			E_7,
			E_8,
			E_9,
			E_10,
			E_11,
			F_0,
			F_1,
			F_2,
			F_3,
			F_4,
			F_5
		};

	public:
		explicit QtCameraSelectionWidget(QWidget *parent = 0);
		~QtCameraSelectionWidget();



		//QVector<QVector3D> m_points;

	private:
		QtMesh *m_cubeVertexMesh = nullptr;
		QtMesh *m_cubeEdgeMesh = nullptr;
		QtMesh *m_cubeFaceMesh = nullptr;

		QVector<QtMeshedObject*> m_vertexObject;
		QVector<QtMeshedObject*> m_edgeObject;
		QVector<QtMeshedObject*> m_faceObject;

		Ui::CameraSelectionWidget *ui;
		virtual void paintEvent(QPaintEvent *);
		virtual QSize sizeHint() const;
		virtual void initializeGL();
		virtual void resizeGL(int w, int h);

		QColor m_backgroundColor;
		QMatrix4x4 m_projectionMatrix;
		QMatrix4x4 m_modelMatrix;

		QGLShaderProgram m_shaderProgram;

		QtCamera m_camera;

		bool setShaderProgram(const QString &name);

		void render3dView();
		void renderOverlay();

		void renderVertexButtons();
		void renderFaceButtons();

		void generateSphere(const unsigned long &resolutionLongitude,
							const unsigned long &resolutionLatitude);

		void setupVertexButtons();
		void setupFaceButtons();
};

#endif // QT_CAMERA_SELECTION_WIDGET_H
