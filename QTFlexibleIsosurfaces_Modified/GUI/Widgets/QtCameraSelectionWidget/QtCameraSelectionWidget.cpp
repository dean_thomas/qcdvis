#include "GUI/Widgets/QtCameraSelectionWidget/QtCameraSelectionWidget.h"
#include "ui_CameraSelectionWidget.h"

///
/// \brief		Creates the buttons to be used to select vertex cameras
/// \since		22-07-2015
/// \author		Dean
///
void QtCameraSelectionWidget::setupVertexButtons()
{
	const QVector3D V_SCALE = QVector3D(VERTEX_BUTTON_FRAC,
										VERTEX_BUTTON_FRAC,
										VERTEX_BUTTON_FRAC);

	//	Create our 8 vertex 'buttons' using a common mesh and use a translation
	//	and scale to position and orintate correctly on the cube
	m_cubeVertexMesh = new QtMesh("OBJ\\Vertex.obj");

	//	v0
	m_vertexObject.push_back(new QtMeshedObject(m_cubeVertexMesh,
												QVector3D(0.5, 0.5, 0.5),
												V_SCALE,
												QVector3D(0, 0, 0),
												false));

	//	v1
	m_vertexObject.push_back(new QtMeshedObject(m_cubeVertexMesh,
												QVector3D(0.5, 0.5, -0.5),
												V_SCALE,
												QVector3D(0, 270, 0),
												false));

	//	v2
	m_vertexObject.push_back(new QtMeshedObject(m_cubeVertexMesh,
												QVector3D(-0.5, 0.5, -0.5),
												V_SCALE,
												QVector3D(0, 180, 0),
												false));

	//	v3
	m_vertexObject.push_back(new QtMeshedObject(m_cubeVertexMesh,
												QVector3D(-0.5, 0.5, 0.5),
												V_SCALE,
												QVector3D(0, 90, 0),
												false));

	//	v4
	m_vertexObject.push_back(new QtMeshedObject(m_cubeVertexMesh,
												QVector3D(0.5, -0.5, 0.5),
												V_SCALE,
												QVector3D(90, 0, 0),
												false));

	//	v5
	m_vertexObject.push_back(new QtMeshedObject(m_cubeVertexMesh,
												QVector3D(0.5, -0.5, -0.5),
												V_SCALE,
												QVector3D(90, 270, 0),
												false));
	//	v6
	m_vertexObject.push_back(new QtMeshedObject(m_cubeVertexMesh,
												QVector3D(-0.5, -0.5, -0.5),
												V_SCALE,
												QVector3D(90, 180, 0),
												false));
	//	v7
	m_vertexObject.push_back(new QtMeshedObject(m_cubeVertexMesh,
												QVector3D(-0.5, -0.5, 0.5),
												V_SCALE,
												QVector3D(90, 90, 0),
												false));

}

///
/// \brief		Creates the buttons to be used to select face cameras
/// \since		22-07-2015
/// \author		Dean
///
void QtCameraSelectionWidget::setupFaceButtons()
{
	const QVector3D V_SCALE = QVector3D(FACE_BUTTON_FRAC,
										FACE_BUTTON_FRAC,
										FACE_BUTTON_FRAC);

	m_cubeFaceMesh = new QtMesh("OBJ\\Face.obj");

	//	f0
	m_faceObject.push_back(new QtMeshedObject(m_cubeFaceMesh,
											  QVector3D(0.0, 0.0, 0.5),
											  V_SCALE,
											  QVector3D(0, 0, 0),
											  false));

	//	f1
	m_faceObject.push_back(new QtMeshedObject(m_cubeFaceMesh,
											  QVector3D(0.0, 0.0, -0.5),
											  V_SCALE,
											  QVector3D(0, 180, 0),
											  false));

	//	f2
	m_faceObject.push_back(new QtMeshedObject(m_cubeFaceMesh,
											  QVector3D(-0.5, 0.0, 0.0),
											  V_SCALE,
											  QVector3D(0, 90, 0),
											  false));

	//	f3
	m_faceObject.push_back(new QtMeshedObject(m_cubeFaceMesh,
											  QVector3D(0.5, 0.0, 0.0),
											  V_SCALE,
											  QVector3D(0, 270, 0),
											  false));

	//	f4
	m_faceObject.push_back(new QtMeshedObject(m_cubeFaceMesh,
											  QVector3D(0.0, 0.5, 0.0),
											  V_SCALE,
											  QVector3D(90, 0, 0),
											  false));

	//	f5
	m_faceObject.push_back(new QtMeshedObject(m_cubeFaceMesh,
											  QVector3D(0.0, -0.5, 0.0),
											  V_SCALE,
											  QVector3D(90, 180, 0),
											  false));
}

QtCameraSelectionWidget::QtCameraSelectionWidget(QWidget *parent) :
	QGLWidget(QGLFormat(), parent),
	ui(new Ui::CameraSelectionWidget)
{

	setAutoFillBackground(false);

	ui->setupUi(this);

	//generateSphere(10, 10);
	setupVertexButtons();
	setupFaceButtons();

	m_cubeEdgeMesh = new QtMesh("OBJ\\Edge.obj");


	//m_edgeObject.push_back(new QtMeshedObject(m_cubeEdgeMesh,
	//											QVector3D(-0.5, -0.5, -0.5),
	//											QVector3D(-V_SCALE, -V_SCALE, -V_SCALE)));

	m_camera = QtCamera(QVector3D(1.5, 1.5, 1.5), QVector3D(0.0, 0.0, 0.0));

	m_backgroundColor = Qt::white;

	update();
}

void QtCameraSelectionWidget::renderOverlay()
{
	QPainter painter(this);


	painter.end();
}

QtCameraSelectionWidget::~QtCameraSelectionWidget()
{
	//	Remove objects
	while (!m_vertexObject.empty())
	{
		delete m_vertexObject.back();
		m_vertexObject.pop_back();
	}

	//	Remove meshes
	delete m_cubeVertexMesh;
	delete m_cubeEdgeMesh;
	delete m_cubeFaceMesh;

	delete ui;
}

void QtCameraSelectionWidget::paintEvent(QPaintEvent *)
{
	makeCurrent();

	render3dView();

	renderOverlay();
}

void QtCameraSelectionWidget::render3dView()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glEnable(GL_DEPTH_TEST);

	renderVertexButtons();
	renderFaceButtons();

	printf("Finished repaint\n");
	fflush(stdout);
}

///
/// \brief		Renders the buttons to be used to select the vertex cameras
/// \since		22-07-2015
/// \author		Dean
///
void QtCameraSelectionWidget::renderVertexButtons()
{
	m_shaderProgram.bind();

	m_shaderProgram.enableAttributeArray("position");
	m_shaderProgram.enableAttributeArray("color");

	QMatrix4x4 viewMatrix = m_camera.GetViewMatrix();

	m_shaderProgram.setUniformValue("projectionMatrix", m_projectionMatrix);

	for (unsigned long i = 0; i < m_vertexObject.size(); ++i)
	{
		QtMeshedObject *currentObject = m_vertexObject[i];

		QMatrix4x4 modelMatrix = currentObject->GetModelMatrix();

		QMatrix4x4 modelViewMatrix = viewMatrix * modelMatrix;
		m_shaderProgram.setUniformValue("modelViewMatrix", modelViewMatrix);

		QMatrix3x3 normalMatrix = modelViewMatrix.normalMatrix();
		m_shaderProgram.setUniformValue("normalMatrix", normalMatrix);

		//for (unsigned long i = 0; i < m_cubeEdgeMesh.GetVertices().size());

		//	Lighting uniforms
		//m_shaderProgram.setUniformValue("objectCount",1);


		//	Render the edge 'buttons'
		//m_shaderProgram.setAttributeArray("position", m_cubeEdgeMesh.GetVertices().constData());
		//m_shaderProgram.setAttributeArray("color", m_cubeEdgeMesh.GetVertices().constData());
		//glDrawArrays(GL_TRIANGLES, 0, m_cubeEdgeMesh.GetVertices().size());

		//	Render the vertex 'buttons'
		//m_shaderProgram.setAttributeArray("position", currentObject->GetMesh()->GetVertices().constData());
		//m_shaderProgram.setAttributeArray("color", currentObject->GetMesh()->GetVertices().constData());
		//glDrawArrays(GL_TRIANGLES, 0, currentObject->GetMesh()->GetVertices().size());

		//	Render the face 'buttons'
		//m_shaderProgram.setAttributeArray("position", m_cubeFaceMesh.GetVertices().constData());
		//m_shaderProgram.setAttributeArray("color", m_cubeFaceMesh.GetVertices().constData());
		//glDrawArrays(GL_TRIANGLES, 0, m_cubeFaceMesh.GetVertices().size());
	}

	m_shaderProgram.disableAttributeArray("position");
	m_shaderProgram.disableAttributeArray("normal");
	m_shaderProgram.disableAttributeArray("color");
	m_shaderProgram.release();

}

///
/// \brief		Renders the buttons to be used to select the face cameras
/// \since		22-07-2015
/// \author		Dean
///
void QtCameraSelectionWidget::renderFaceButtons()
{
	assert(context() != nullptr);

	m_shaderProgram.bind();

	m_shaderProgram.enableAttributeArray("position");
	m_shaderProgram.enableAttributeArray("color");

	QMatrix4x4 viewMatrix = m_camera.GetViewMatrix();

	m_shaderProgram.setUniformValue("projectionMatrix", m_projectionMatrix);

	for (unsigned long i = 0; i < m_faceObject.size(); ++i)
	{
		QtMeshedObject *currentObject = m_faceObject[i];

		QMatrix4x4 modelMatrix = currentObject->GetModelMatrix();

		QMatrix4x4 modelViewMatrix = viewMatrix * modelMatrix;
		m_shaderProgram.setUniformValue("modelViewMatrix", modelViewMatrix);

		QMatrix3x3 normalMatrix = modelViewMatrix.normalMatrix();
		m_shaderProgram.setUniformValue("normalMatrix", normalMatrix);

		//for (unsigned long i = 0; i < m_cubeEdgeMesh.GetVertices().size());

		//	Lighting uniforms
		//m_shaderProgram.setUniformValue("objectCount",1);


		//	Render the edge 'buttons'
		//m_shaderProgram.setAttributeArray("position", m_cubeEdgeMesh.GetVertices().constData());
		//m_shaderProgram.setAttributeArray("color", m_cubeEdgeMesh.GetVertices().constData());
		//glDrawArrays(GL_TRIANGLES, 0, m_cubeEdgeMesh.GetVertices().size());

		//	Render the vertex 'buttons'
		//m_shaderProgram.setAttributeArray("position", currentObject->GetMesh()->GetVertices().constData());
		//m_shaderProgram.setAttributeArray("color", currentObject->GetMesh()->GetVertices().constData());
		//glDrawArrays(GL_TRIANGLES, 0, currentObject->GetMesh()->GetVertices().size());

		//	Render the face 'buttons'
		//m_shaderProgram.setAttributeArray("position", m_cubeFaceMesh.GetVertices().constData());
		//m_shaderProgram.setAttributeArray("color", m_cubeFaceMesh.GetVertices().constData());
		//glDrawArrays(GL_TRIANGLES, 0, m_cubeFaceMesh.GetVertices().size());
	}

	m_shaderProgram.disableAttributeArray("position");
	m_shaderProgram.disableAttributeArray("normal");
	m_shaderProgram.disableAttributeArray("color");
	m_shaderProgram.release();

}

bool QtCameraSelectionWidget::setShaderProgram(const QString &name)
{
	QString vShader = SHADER_DIR + name + ".vs.glsl";
	QString fShader = SHADER_DIR + name + ".fs.glsl";

	//	This is the main shader to be used for renderling the surfaces
	printf("%s.\n", vShader.toStdString().c_str());
	printf("%s.\n", fShader.toStdString().c_str());
	fflush(stdout);

	m_shaderProgram.addShaderFromSourceFile(QGLShader::Vertex,
											vShader);
	m_shaderProgram.addShaderFromSourceFile(QGLShader::Fragment,
											fShader);

	//	Make sure we link correctly
	bool result = m_shaderProgram.link();

	return result;
}

void QtCameraSelectionWidget::generateSphere(const unsigned long &resolutionLongitude,
											 const unsigned long &resolutionLatitude)
{
	/*
	m_points.clear();

	const float TAU = 2.0 * PI;

	for (unsigned long m = 0; m < resolutionLatitude; ++m)
	{
		for (unsigned long n = 0; n < resolutionLongitude; ++n)
		{
			float x = sin(PI * ((float)m / (float)resolutionLatitude)) * cos(TAU * ((float)n / (float)resolutionLongitude));
			float y = sin(PI * ((float)m / (float)resolutionLatitude)) * sin(TAU * ((float)n / (float)resolutionLongitude));
			float z = cos(PI * ((float)m / (float)resolutionLatitude));

			QVector3D point = QVector3D(x, y, z);

			m_points.push_back(point);
		}
	}
	*/
}

QSize QtCameraSelectionWidget::sizeHint() const
{
	return QSize(640, 480);
}

void QtCameraSelectionWidget::initializeGL()
{
	glEnable(GL_DEPTH_TEST);

	qglClearColor(m_backgroundColor);

	bool result = setShaderProgram("basicShader");

	printf("Loaded shaders: %s.\n", result ? "true" : "false");
	fflush(stdout);

}

void QtCameraSelectionWidget::resizeGL(int w, int h)
{
	if (h == 0) h == 1;

	m_projectionMatrix.setToIdentity();
	m_projectionMatrix.perspective(60.0, (float)w / (float)h, 0.001, 1000);

	glViewport(0,0,w,h);

	printf("Resized opengl window to %f x %f.\n", (float)w, (float)h);
	fflush(stdout);

}
