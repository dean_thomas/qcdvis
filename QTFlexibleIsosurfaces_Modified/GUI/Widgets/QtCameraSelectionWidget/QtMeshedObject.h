#ifndef QTMESHEDOBJECT_H
#define QTMESHEDOBJECT_H

#include "../QtMesh.h"

#define PI 3.14159265359

class QtMeshedObject
{
	private:
		QtMesh *m_mesh = nullptr;

		QMatrix4x4 m_modelMatrix;

		QVector3D m_translation;
		QVector3D m_scale;
		QMatrix4x4 m_rotationMatrix;

		QMatrix4x4 calculateModelMatrix() const;
	public:
		QtMeshedObject();
		QtMeshedObject(QtMesh *mesh);
		QtMeshedObject(QtMesh *mesh, const QVector3D &translation);
		QtMeshedObject(QtMesh *mesh, const QVector3D &translation, const QVector3D &scale);
		QtMeshedObject(QtMesh *mesh, const QVector3D &translation, const QVector3D &scale, const QVector3D &rotation, const bool &radians = true);
		QMatrix4x4 GetModelMatrix() const { return m_modelMatrix; }
		QtMesh *GetMesh() const { return m_mesh; }

		void SetTranslation(const QVector3D &translation);
		void SetScale(const QVector3D &scale);
		void SetRotation(const QVector3D &rotation, const bool &radians = true);
};

#endif // QTMESHEDOBJECT_H
