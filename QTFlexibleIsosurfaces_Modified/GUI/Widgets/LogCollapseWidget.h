///////////////////////////////////////////////////
//
//	Contour-tree based isosurfaces
//	Version 4.0 (QT-based)
//	copyright Hamish Carr
//	January, 2014
//
//	------------------------
//	LogCollapseWidget.h
//	------------------------
//	
//	A widget displaying the tree size / collapse
//	bound as a log-log plot
//	
///////////////////////////////////////////////////

#ifndef _HEADER_LOG_COLLAPSE_WIDGET_H
#define _HEADER_LOG_COLLAPSE_WIDGET_H

#include <QtOpenGL>
#include <QGLWidget>

#include "DataModel.h"

class LogCollapseWidget : public QGLWidget
{ // class LogCollapseWidget
public:
    // the model - i.e. the flexible isosurface data
    DataModel *m_dataModel = nullptr;

    LogCollapseWidget(QWidget *parent);

    // constructor
    LogCollapseWidget(DataModel *dataModel, QWidget *parent);

    // destructor
    ~LogCollapseWidget();

    void PlotLogCollapse();
protected:
    // called when OpenGL context is set up
    virtual void initializeGL();
    // called every time the widget is resized
    virtual void resizeGL(int w, int h);
    // called every time the widget needs painting
    virtual void paintGL();

}; // class LogCollapseWidget

#endif
