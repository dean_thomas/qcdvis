#include "AbstractColourMappedWidget.h"

AbstractColourMappedWidget::AbstractColourMappedWidget()
{
}

void AbstractColourMappedWidget::SetColourRamps(QtColourRampWidget* positiveColourRamp,
									   QtColourRampWidget* negativeColourRamp)
{
	m_positiveColourRamp = positiveColourRamp;
	m_negativeColourRamp = negativeColourRamp;
}

QColor AbstractColourMappedWidget::calculateArcColour(ContourTreeSuperarc *arc,
												   ContourTreeSupernode *topNode,
												   ContourTreeSupernode *bottomNode) const
{
	QColor result(Qt::black);

	if ((m_positiveColourRamp != nullptr) && (m_negativeColourRamp != nullptr))
	{
		//	Calculate the mid-point of the arc as a function height
		float midHeight =
				(bottomNode->GetHeight() + topNode->GetHeight()) / 2.0;

		//	And calculate the midpoint of the arc in graph space
		//float midPointX = (topNode->xPosn + bottomNode->xPosn) / 2.0;
		float midPointX = 0.5;

		if (midHeight < 0)
		{
			//	Use the bottom ramp for -ve values
			result =  m_negativeColourRamp->GetColorFromValue(255 * midPointX);
		}
		else
		{
			//	Use the top ramp for +ve values (and zero)
			result =  m_positiveColourRamp->GetColorFromValue(255 * midPointX);
		}
	}
	return result;
}

QColor AbstractColourMappedWidget::calculateNodeColour(ContourTreeSupernode node) const
{
	QColor result(Qt::black);

	if ((m_positiveColourRamp != nullptr) && (m_negativeColourRamp != nullptr))
	{
		if (node.GetHeight() < 0)
		{
			//	Use the bottom ramp for -ve values
			result =  m_negativeColourRamp->GetColorFromValue(0.5f);
		}
		else
		{
			//	Use the top ramp for +ve values (and zero)
			result =  m_positiveColourRamp->GetColorFromValue(0.5f);
		}
	}
	return result;
}
