#include <GL/glew.h>

#include "QtAbstract3dRenderedView.h"
#include "HeightField3D.h"
#include "../Globals/QtFunctions.h"
#include "QtCamera.h"

#include <cassert>
#include <iostream>

#include <QVector3D>
#include <QMatrix3x3>
#include <QMatrix4x4>
#include <QtOpenGL>
#include <QGLFunctions>

using namespace std;

#define DISABLE_DEBUG_OUTPUT

QtAbstract3dRenderedView::QtAbstract3dRenderedView(QWidget *parent)
	: QGLWidget(QGLFormat(), parent)
{

	setAutoFillBackground(false);

	//	Setup the camera
	m_floatingCamera = new QtCamera("Arcball camera");
	m_currentCamera = m_floatingCamera;
}

QSize QtAbstract3dRenderedView::sizeHint() const
{
	return QSize(640, 480);
}

void QtAbstract3dRenderedView::resizeGL(int w, int h)
{
	if (h == 0) h = 1;

	m_projectionMatrix.setToIdentity();
	m_projectionMatrix.perspective(60.0, (float)w / (float)h, 0.001, 1000);

	glViewport(0,0,w,h);
}

void QtAbstract3dRenderedView::initializeGL()
{
	GLenum err = glewInit();
	//if(err != GLEW_OK)
	//{
	//	cerr << glewGetErrorString(err) << endl;
	//}
	assert(err == GLEW_OK);
#ifndef DISABLE_DEBUG_OUTPUT
	cout << "Initialized opengl " << glGetString(GL_VERSION) << endl;
	cout << "Using glew version: " << glewGetString(GLEW_VERSION) << endl;
#endif
	//glEnable(GL_CULL_FACE);

	//	Our surfaces are generated with Clockwise Triangulations
	//glFrontFace(GL_CW);

	qglClearColor(Qt::white);
	//qglClearColor(backgroundColour);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glEnable(GL_DEPTH_TEST);

	//	This is a simple program which contains no lighting properties, it can
	//	be used to render 'flat' objects such as wireframes or axes
	assert(m_basicShaderProgram.addShaderFromSourceFile(QGLShader::Vertex,
														QString(BASIC_SHADER_VS)));
	assert(m_basicShaderProgram.addShaderFromSourceFile(QGLShader::Fragment,
														QString(BASIC_SHADER_FS)));
	assert(m_basicShaderProgram.link());
}

///
/// \brief		Sets the camera yaw, pitch and roll by computing values from
///				the supllied quaternion rotation
/// \param camera
/// \since		10-07-2015
/// \author		Dean
///
void QtAbstract3dRenderedView::SetFloatingCameraPosition(const QQuaternion &camera,
														 const float &radius)
{
	//m_camera.SetRotation(camera);
	m_floatingCamera->SetPosition(getLookAtPoint(),
								  camera, radius,
								  getLookAtPoint());

	update();
}

QVector3D QtAbstract3dRenderedView::GetFloatingCameraPosition() const
{
	return m_floatingCamera->GetPosition();
}

///
/// \brief		Renders the data in it's standard form
/// \since		26-05-2015
/// \author		Dean
///
void QtAbstract3dRenderedView::executeTriangleDrawingShader(QGLShaderProgram* const shaderProgram,
															const QVector3D& lightPosition, const bool alphaBlended, const BufferData& bufferData)
{
	//#define SUPRESS_OUTPUT

	assert(context() != nullptr);
	assert(shaderProgram != nullptr);

	//	Nothing to be rendered
	if (bufferData.vertexArray.empty()) return;

	//	Check that the number of vertices provided matches the number of
	//	colours and number of normals (if provided).
	assert(bufferData.vertexArray.size() == bufferData.colourArray.size());
	if (!bufferData.normalArray.empty())
		assert(bufferData.vertexArray.size() == bufferData.normalArray.size());

	glEnable(GL_DEPTH_TEST);

	if (alphaBlended)
	{
		glEnable(GL_BLEND);

		QGLFunctions resolver;
		resolver.initializeGLFunctions();
		resolver.glBlendEquation(GL_FUNC_ADD);
		//if (!QGLFunctions::OpenGLFeatures::testFlag(QGLFunctions::BlendEquation))
		//{
		//	cerr << "glBlendEquation is not available." << endl;
		//}
		//	glBlendEquation( GL_FUNC_ADD );
		glDepthMask(false);
		//glBlendFunc(GL_ONE,GL_DST_COLOR);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		//glLightModelf(GL_LIGHT_MODEL_TWO_SIDE,1.0);
	}

	//	Our matrices to be send to the GPU
	//QMatrix4x4 mMatrix;
	//cout << "Rendering with camera: " << *m_currentCamera << endl;

	QMatrix4x4 vMatrix = m_currentCamera->GetViewMatrix();
	QMatrix4x4 mvMatrix = vMatrix * m_mMatrix;
	QMatrix3x3 normalMatrix = mvMatrix.normalMatrix();

	//
	//QVector3D lightPos(5.0, 5.0, 5.0);

	//	Bind our shader program
	shaderProgram->bind();

	//	Pre-compute our matrices in various forms and send them as uniforms
	shaderProgram->setUniformValue("uModelViewMatrix", mvMatrix);
	shaderProgram->setUniformValue("uProjectionMatrix", m_projectionMatrix);
	shaderProgram->setUniformValue("uNormalMatrix", normalMatrix);

	//	Lighting uniforms
	//m_mainShaderProgram->setUniformValue("objectCount",
	//									 (float)m_heightField->GetContourTree()->GetArcCount());
	shaderProgram->setUniformValue("uLightPosition", lightPosition);
	//m_shaderProgram.setUniformValue("lightDiffuse", QColor(Qt::yellow));

	//	Send the vertex array
	shaderProgram->setAttributeArray("vPosition", bufferData.vertexArray.constData());
	shaderProgram->enableAttributeArray("vPosition");

	//	Send the normal array
	shaderProgram->setAttributeArray("vNormal", bufferData.normalArray.constData());
	shaderProgram->enableAttributeArray("vNormal");

	//	Send the colour array
	shaderProgram->setAttributeArray("vColor", bufferData.colourArray.constData());
	shaderProgram->enableAttributeArray("vColor");

	shaderProgram->setAttributeArray("vObjectID", bufferData.objectIdArray.constData(), 1);
	shaderProgram->enableAttributeArray("vObjectID");


	//	If there is data to be rendered, render it...
	glDrawArrays(GL_TRIANGLES, 0, bufferData.vertexArray.size());

	//	Clean up the shader program
	shaderProgram->disableAttributeArray("vPosition");
	shaderProgram->disableAttributeArray("vNormal");
	shaderProgram->disableAttributeArray("vColor");
	shaderProgram->release();

	glDisable(GL_BLEND);
	//glUseProgram(0);

#undef SUPRESS_OUTPUT
}

///
/// \brief		Executes the point drawing shader
/// \param		A stream of QVector3 objects representing the spatial points
/// \param		A stream of QVector4 objects representing the colour at each
///				point
/// \param lineThickness
/// \author		Dean
/// \since		23-03-2016
///
void QtAbstract3dRenderedView::executePointDrawingShader(const QVector<QVector3D>& vertexPositionArray,
														 const QVector<QVector4D>& vertexColourArray,
														 const float pointDiameter)
{
	assert(vertexPositionArray.size() == vertexColourArray.size());

	//	Nothing to be drawn, so no point in setting up
	if (vertexPositionArray.empty()) return;

	glEnable(GL_DEPTH_TEST);
	glDepthMask(true);
	glPointSize(pointDiameter);

	//	Our matrices to be send to the GPU
	//QMatrix4x4 mMatrix;
	QMatrix4x4 vMatrix = m_currentCamera->GetViewMatrix();
	QMatrix4x4 mvMatrix = vMatrix * m_mMatrix;
	QMatrix3x3 normalMatrix = mvMatrix.normalMatrix();

	//	Bind our shader program
	m_basicShaderProgram.bind();

	//	Pre-compute our matrices in various forms and send them as uniforms
	m_basicShaderProgram.setUniformValue("uModelViewMatrix", mvMatrix);
	m_basicShaderProgram.setUniformValue("uProjectionMatrix", m_projectionMatrix);
	m_basicShaderProgram.setUniformValue("uNormalMatrix", normalMatrix);

	//	Send the vertex array
	m_basicShaderProgram.setAttributeArray("vPosition", vertexPositionArray.constData());
	m_basicShaderProgram.enableAttributeArray("vPosition");

	//	Send the colour array
	m_basicShaderProgram.setAttributeArray("vColor", vertexColourArray.constData());
	m_basicShaderProgram.enableAttributeArray("vColor");

	//	Render it...
	glDrawArrays(GL_POINTS, 0, vertexPositionArray.size());

	//	Clean up the shader program
	m_basicShaderProgram.disableAttributeArray("vPosition");
	m_basicShaderProgram.disableAttributeArray("vColor");
	m_basicShaderProgram.release();
}


///
/// \brief		Executes the line drawing shader
/// \param		A stream of QVector3 objects representing the spatial points
/// \param		A stream of QVector4 objects representing the colour at each
///				point
/// \param lineThickness
/// \author		Dean
/// \since		23-03-2016
///
void QtAbstract3dRenderedView::executeLineDrawingShader(const QVector<QVector3D>& vertexPositionArray,
														const QVector<QVector4D>& vertexColourArray,
														const float lineThickness)
{
	assert(vertexPositionArray.size() == vertexColourArray.size());

	//	Nothing to be drawn, so no point in setting up
	if (vertexPositionArray.empty()) return;
#ifndef DISABLE_DEBUG_OUTPUT
	cout << "Executing line drawing shader" << endl;
#endif
	glEnable(GL_DEPTH_TEST);
	glDepthMask(true);
	glLineWidth(lineThickness);

	//	Our matrices to be send to the GPU
	//QMatrix4x4 mMatrix;
	QMatrix4x4 vMatrix = m_currentCamera->GetViewMatrix();
	QMatrix4x4 mvMatrix = vMatrix * m_mMatrix;
	QMatrix3x3 normalMatrix = mvMatrix.normalMatrix();

	//	Bind our shader program
	assert(m_basicShaderProgram.bind());

	//	Pre-compute our matrices in various forms and send them as uniforms
	m_basicShaderProgram.setUniformValue("uModelViewMatrix", mvMatrix);
	m_basicShaderProgram.setUniformValue("uProjectionMatrix", m_projectionMatrix);
	m_basicShaderProgram.setUniformValue("uNormalMatrix", normalMatrix);

	//	Send the vertex array
	m_basicShaderProgram.setAttributeArray("vPosition", vertexPositionArray.constData());
	m_basicShaderProgram.enableAttributeArray("vPosition");

	//	Send the colour array
	m_basicShaderProgram.setAttributeArray("vColor", vertexColourArray.constData());
	m_basicShaderProgram.enableAttributeArray("vColor");

	//	Render it...
	glDrawArrays(GL_LINES, 0, vertexPositionArray.size());

	//	Clean up the shader program
	m_basicShaderProgram.disableAttributeArray("vPosition");
	m_basicShaderProgram.disableAttributeArray("vColor");
	m_basicShaderProgram.release();
}

///
/// \brief		Renders the X, Y and Z axis lines.  Additionally, can render
///				axis lines at the far edges of the data.
/// \since		25-05-2015
/// \author		Dean
///
void QtAbstract3dRenderedView::renderAxes(const Axis3& axes,
										  const bool renderFarAxisLines,
										  const float axisThickness)
{
	assert(context() != nullptr);
	makeCurrent();
#ifndef DISABLE_DEBUG_OUTPUT
	cout << "Rendeirng axes" << endl;
#endif
	const QVector3D ORIGIN(0.0, 0.0, 0.0);

	const QVector3D MAX_X(axes.xAxis.minMax.max, 0.0, 0.0);
	const QVector3D MAX_Y(0.0, axes.yAxis.minMax.max, 0.0);
	const QVector3D MAX_Z(0.0, 0.0, axes.zAxis.minMax.max);

	const QVector3D MAX_XY(axes.xAxis.minMax.max,
						   axes.yAxis.minMax.max
						   , 0.0);

	const QVector3D MAX_YZ(0.0,
						   axes.yAxis.minMax.max,
						   axes.zAxis.minMax.max);

	const QVector3D MAX_XZ(axes.xAxis.minMax.max,
						   0.0,
						   axes.zAxis.minMax.max);

	const QVector3D MAX_XYZ(axes.xAxis.minMax.max,
							axes.yAxis.minMax.max,
							axes.zAxis.minMax.max);


	const QVector4D LINE_COLOUR = QColorToQVector4D(QColor(Qt::black));

	QVector<QVector3D> vertexArray;
	QVector<QVector4D> colourArray;

	//	X axis
	vertexArray.push_back(ORIGIN);
	vertexArray.push_back(MAX_X);
	colourArray.push_back(LINE_COLOUR);
	colourArray.push_back(LINE_COLOUR);

	//	Y axis
	vertexArray.push_back(ORIGIN);
	vertexArray.push_back(MAX_Y);
	colourArray.push_back(LINE_COLOUR);
	colourArray.push_back(LINE_COLOUR);

	//	Z axis
	vertexArray.push_back(ORIGIN);
	vertexArray.push_back(MAX_Z);
	colourArray.push_back(LINE_COLOUR);
	colourArray.push_back(LINE_COLOUR);

	if (renderFarAxisLines)
	{
		//	Far axis lines (9 additional lines)
		//	#1
		vertexArray.push_back(MAX_Z);
		vertexArray.push_back(MAX_XZ);
		colourArray.push_back(LINE_COLOUR);
		colourArray.push_back(LINE_COLOUR);

		//	#2
		vertexArray.push_back(MAX_X);
		vertexArray.push_back(MAX_XZ);
		colourArray.push_back(LINE_COLOUR);
		colourArray.push_back(LINE_COLOUR);

		//	#3
		vertexArray.push_back(MAX_Y);
		vertexArray.push_back(MAX_YZ);
		colourArray.push_back(LINE_COLOUR);
		colourArray.push_back(LINE_COLOUR);

		//	#4
		vertexArray.push_back(MAX_Z);
		vertexArray.push_back(MAX_YZ);
		colourArray.push_back(LINE_COLOUR);
		colourArray.push_back(LINE_COLOUR);

		//	#5
		vertexArray.push_back(MAX_X);
		vertexArray.push_back(MAX_XY);
		colourArray.push_back(LINE_COLOUR);
		colourArray.push_back(LINE_COLOUR);

		//	#6
		vertexArray.push_back(MAX_Y);
		vertexArray.push_back(MAX_XY);
		colourArray.push_back(LINE_COLOUR);
		colourArray.push_back(LINE_COLOUR);

		//	#7
		vertexArray.push_back(MAX_XZ);
		vertexArray.push_back(MAX_XYZ);
		colourArray.push_back(LINE_COLOUR);
		colourArray.push_back(LINE_COLOUR);

		//	#8
		vertexArray.push_back(MAX_YZ);
		vertexArray.push_back(MAX_XYZ);
		colourArray.push_back(LINE_COLOUR);
		colourArray.push_back(LINE_COLOUR);

		//	#9
		vertexArray.push_back(MAX_XY);
		vertexArray.push_back(MAX_XYZ);
		colourArray.push_back(LINE_COLOUR);
		colourArray.push_back(LINE_COLOUR);
	}

	//	Draw the buffered data
	executeLineDrawingShader(vertexArray, colourArray, axisThickness);
}

///
/// \brief		Labels the 3 axis on the graph
/// \since		26-05-2015
/// \author		Dean
/// \author		Dean: allow axis to be labels to be offset from zero
///				[06-11-2015]
///
void QtAbstract3dRenderedView::renderAxisLabels(QPainter *painter, const Axis3& axes)
{
	assert(context() != nullptr);

	const float AXIS_LOWER_LIMIT_X = 1.0f;
	const float AXIS_LOWER_LIMIT_Y = 1.0f;
	const float AXIS_LOWER_LIMIT_Z = 1.0f;

	//	Amount to offset the labels from the actual axes
	const float NUM_OFFSET = -0.5;
	const float TEXT_OFFSET = -1.0;

	//	Roughly the midpoint of each axis
	const float midX = axes.xAxis.minMax.max / 2.0;
	const float midY = axes.yAxis.minMax.max / 2.0;
	const float midZ = axes.zAxis.minMax.max / 2.0;

	//	Aliases to the maxima for each axis
	const float maxX = axes.xAxis.minMax.max;
	const float maxY = axes.yAxis.minMax.max;
	const float maxZ = axes.zAxis.minMax.max;

	//	Aliases to the label for each axis
	const QString labelX = axes.xAxis.label;
	const QString labelY = axes.yAxis.label;
	const QString labelZ = axes.zAxis.label;

	//	Make it look nice
	painter->setRenderHint(QPainter::TextAntialiasing);

	//	Project the position of the zero label to screen space (if we use
	//	a 'unified' origin
	//QPoint posOrigin = projectToScreenSpace(QVector3D(0 + NUM_OFFSET - AXIS_LOWER_LIMIT_X,
	//												0 + NUM_OFFSET - AXIS_LOWER_LIMIT_Y,
	//												0 + NUM_OFFSET - AXIS_LOWER_LIMIT_Z));


	//	Project the maximum point labels to screen space (we will need to draw
	//	these always, as the ticks may not fall on the maximum) and draw on
	//	screen
	{
		auto labelWorldPosXMax = QVector3D(1 + maxX - AXIS_LOWER_LIMIT_X,
										   0 + NUM_OFFSET,
										   0 + NUM_OFFSET);
		QPoint posMaxX = projectToScreenSpace(labelWorldPosXMax,
											  static_cast<float>(width()),
											  static_cast<float>(height()));

		//	Draw the maximum for the x axis
		painter->drawText(posMaxX.x(), posMaxX.y(), QString("%1")
						  .arg(AXIS_LOWER_LIMIT_X + maxX));

		auto labelWorldPosYMax = QVector3D(0 + NUM_OFFSET,
										   1 + maxY - AXIS_LOWER_LIMIT_Y,
										   0 + NUM_OFFSET);
		QPoint posMaxY = projectToScreenSpace(labelWorldPosYMax,
											  static_cast<float>(width()),
											  static_cast<float>(height()));

		painter->drawText(posMaxY.x(), posMaxY.y(), QString("%1")
						  .arg(AXIS_LOWER_LIMIT_Y + maxY));

		//	Draw the maximum for the y axis
		auto labelWorldPosZMax = QVector3D(0 + NUM_OFFSET,
										   0 + NUM_OFFSET,
										   1 + maxZ - AXIS_LOWER_LIMIT_Z);
		QPoint posMaxZ = projectToScreenSpace(labelWorldPosZMax,
											  static_cast<float>(width()),
											  static_cast<float>(height()));

		//	Draw the maximum for the z axis
		painter->drawText(posMaxZ.x(), posMaxZ.y(), QString("%1")
						  .arg(AXIS_LOWER_LIMIT_Z + maxZ));
	}

	//	Project the midpoint labels to screen space and draw on screen
	{
		QPoint posMidX = projectToScreenSpace(QVector3D(midX,
														0 + TEXT_OFFSET,
														0 + TEXT_OFFSET),
											  static_cast<float>(width()),
											  static_cast<float>(height()));
		painter->drawText(posMidX.x(), posMidX.y(),labelX);

		QPoint posMidY = projectToScreenSpace(QVector3D(0 + TEXT_OFFSET,
														midY,
														0 + TEXT_OFFSET),
											  static_cast<float>(width()),
											  static_cast<float>(height()));
		painter->drawText(posMidY.x(), posMidY.y(), labelY);

		QPoint posMidZ = projectToScreenSpace(QVector3D(0 + TEXT_OFFSET,
														0 + TEXT_OFFSET,
														midZ),
											  static_cast<float>(width()),
											  static_cast<float>(height()));
		painter->drawText(posMidZ.x(), posMidZ.y(),labelZ);
	}


	//	Zero label
	//painter->drawText(posZero.x(), posZero.y(), "0");

	//	Interval ticks
	for (auto x = AXIS_LOWER_LIMIT_X;
		 x <= AXIS_LOWER_LIMIT_X + maxX;
		 x += m_tickFreqX)
	{
		auto labelWorldPos = QVector3D(x - AXIS_LOWER_LIMIT_X,
									   0 + NUM_OFFSET,
									   0 + NUM_OFFSET);

		auto tickLabelPos = projectToScreenSpace(labelWorldPos,
												 static_cast<float>(width()),
												 static_cast<float>(height()));

		painter->drawText(tickLabelPos.x(), tickLabelPos.y(), QString("%1")
						  .arg(x));

	}

	for (auto y = AXIS_LOWER_LIMIT_Y;
		 y <= AXIS_LOWER_LIMIT_Y + maxY;
		 y += m_tickFreqY)
	{
		auto labelWorldPos = QVector3D(0 + NUM_OFFSET,
									   y - AXIS_LOWER_LIMIT_Y,
									   0 + NUM_OFFSET);

		auto tickLabelPos = projectToScreenSpace(labelWorldPos,
												 static_cast<float>(width()),
												 static_cast<float>(height()));

		painter->drawText(tickLabelPos.x(), tickLabelPos.y(), QString("%1")
						  .arg(y));

	}

	for (auto z = AXIS_LOWER_LIMIT_Z;
		 z <= AXIS_LOWER_LIMIT_Z + maxZ;
		 z += m_tickFreqZ)
	{
		auto labelWorldPos = QVector3D(0 + NUM_OFFSET,
									   0 + NUM_OFFSET,
									   z - AXIS_LOWER_LIMIT_Z);

		auto tickLabelPos = projectToScreenSpace(labelWorldPos,
												 static_cast<float>(width()),
												 static_cast<float>(height()));

		painter->drawText(tickLabelPos.x(), tickLabelPos.y(), QString("%1")
						  .arg(z));

	}
}


///
/// \brief		Takes a 3d point as used in the 3d render and projects to the
///				relative position in screen space
/// \param		pos
/// \return
/// \since		14-07-2015
/// \author		Dean
///
QPoint QtAbstract3dRenderedView::projectToScreenSpace(const QVector3D &pos, const float width, const float height) const
{
	//	Adapted from:
	//	http://webglfactory.blogspot.co.uk/2011/05/how-to-convert-world-to-screen.html
	//	[14-07-2015]
	QMatrix4x4 viewProjectionMatrix = m_projectionMatrix * m_currentCamera->GetViewMatrix();

	QVector3D pos3d = viewProjectionMatrix * pos;

	int winX = round(((pos3d.x() + 1.0) / 2.0) * width);
	int winY = round(((1.0 - pos3d.y()) / 2.0) * height);

	return QPoint(winX, winY);

}
