#ifndef VERTICALLABEL_H
#define VERTICALLABEL_H

#include <QLabel>

class QtVerticalLabel : public QLabel
{
	Q_OBJECT

public:
	explicit QtVerticalLabel(QWidget *parent=0);
	explicit QtVerticalLabel(const QString &text, QWidget *parent=0);

protected:
	void paintEvent(QPaintEvent*);
	QSize sizeHint() const ;
	QSize minimumSizeHint() const;
};

#endif // VERTICALLABEL_H
