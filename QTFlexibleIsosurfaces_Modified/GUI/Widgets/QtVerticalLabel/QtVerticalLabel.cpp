#include "QtVerticalLabel.h"

#include <QPainter>

QtVerticalLabel::QtVerticalLabel(QWidget *parent)
	: QLabel(parent)
{

}

QtVerticalLabel::QtVerticalLabel(const QString &text, QWidget *parent)
: QLabel(text, parent)
{
}

void QtVerticalLabel::paintEvent(QPaintEvent*)
{
	QPainter painter(this);
	painter.setPen(Qt::black);
	painter.setBrush(Qt::Dense1Pattern);

	painter.rotate(90);

	painter.drawText(0,0, text());
}

QSize QtVerticalLabel::minimumSizeHint() const
{
	QSize s = QLabel::minimumSizeHint();
	return QSize(s.height(), s.width());
}

QSize QtVerticalLabel::sizeHint() const
{
	QSize s = QLabel::sizeHint();
	return QSize(s.height(), s.width());
}
