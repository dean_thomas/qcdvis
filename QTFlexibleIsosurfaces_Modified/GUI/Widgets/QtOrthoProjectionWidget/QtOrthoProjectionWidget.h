#ifndef QTORTHOPROJECTIONWIDGET_H
#define QTORTHOPROJECTIONWIDGET_H

#include <QWidget>

namespace Ui {
	class QtOrthoProjectionWidget;
}

class QtOrthoProjectionWidget : public QWidget
{
		Q_OBJECT

	public:
		explicit QtOrthoProjectionWidget(QWidget *parent = 0);
		~QtOrthoProjectionWidget();

	private:
		void createContextMenu();

		Ui::QtOrthoProjectionWidget *ui;
};

#endif // QTORTHOPROJECTIONWIDGET_H
