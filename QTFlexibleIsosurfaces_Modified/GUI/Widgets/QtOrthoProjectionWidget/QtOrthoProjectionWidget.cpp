#include "QtOrthoProjectionWidget.h"
#include "ui_QtOrthoProjectionWidget.h"

void QtOrthoProjectionWidget::createContextMenu()
{
	QAction* actionHide = new QAction("Hide", this);
	connect(actionHide, &QAction::triggered,
			[&]()
	{
		setHidden(true);
	});
	addAction(actionHide);


	//	Use the QActions for the context menu
	setContextMenuPolicy(Qt::ActionsContextMenu);
}

QtOrthoProjectionWidget::QtOrthoProjectionWidget(QWidget *parent) :
	QWidget(parent),
	ui(new Ui::QtOrthoProjectionWidget)
{
	ui->setupUi(this);

	ui->labelMinX->setText("0.0");
	ui->labelMidX->setText("X");
	ui->labelMaxX->setText("1.0");

	ui->labelMinY->setText("0.0");
	ui->labelMidY->setText("Y");
	ui->labelMaxY->setText("1.0");

	createContextMenu();
}

QtOrthoProjectionWidget::~QtOrthoProjectionWidget()
{
	delete ui;
}
