#ifndef QTISOVALUEBOOKMARKWIDGET_H
#define QTISOVALUEBOOKMARKWIDGET_H

#include <DataExploration/IsovalueBookmark.h>
#include <QWidget>
#include <vector>
#include <exception>
#include <cassert>

#include "DataExploration/IsovalueBookmarkList.h"

using std::exception;
using std::vector;

namespace Ui {
	class QtIsovalueBookmarkWidget;
}

class QtIsovalueBookmarkWidget : public QWidget
{
		Q_OBJECT

	public:
		explicit QtIsovalueBookmarkWidget(QWidget *parent = 0);
		QtIsovalueBookmarkWidget(const IsovalueBookmarkList &bookmarkArray,
								 QWidget *parent = 0);
		~QtIsovalueBookmarkWidget();

		void SetCurrentIsovalue(const float &isovalue);

		bool GetTitleHidden() const;
		void SetTitleHidden(const bool &value);
	private slots:
		void on_pushButtonAdd_clicked();

		void on_pushButtonRemove_clicked();

		void on_comboBoxBookmarks_currentIndexChanged(int index);

	private:
		bool addBookmark();
		bool removeBookmark();

		Ui::QtIsovalueBookmarkWidget *ui;
		IsovalueBookmarkList m_bookmarkArray;
		float m_isovalue = 0.0f;

	signals:
		void OnBookmarkSelected(const IsovalueBookmark &bookmark);
		void OnBookmarkAdded(const IsovalueBookmark &bookmark);
		void OnBookmarkRemoved(const IsovalueBookmark &bookmark);
};

#endif // QTISOVALUEBOOKMARKWIDGET_H
