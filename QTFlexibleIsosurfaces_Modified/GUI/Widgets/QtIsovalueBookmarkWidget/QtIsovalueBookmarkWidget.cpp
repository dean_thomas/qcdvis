#include "QtIsovalueBookmarkWidget.h"
#include "ui_QtIsovalueBookmarkWidget.h"

///
/// \brief QtIsovalueBookmarkWidget::GetTitleHidden
/// \return
/// \author		Dean
/// \since		05-08-2015
///
bool QtIsovalueBookmarkWidget::GetTitleHidden() const
{
	return ui->label->isVisible();
}

///
/// \brief		Allows the title to be hidden
/// \param value
/// \author		Dean
/// \since		05-08-2015
///
void QtIsovalueBookmarkWidget::SetTitleHidden(const bool &value)
{
	ui->label->setHidden(value);
}

///
/// \brief QtIsovalueBookmarkWidget::QtIsovalueBookmarkWidget
/// \param parent
/// \author		Dean
/// \since		05-08-2015
///
QtIsovalueBookmarkWidget::QtIsovalueBookmarkWidget(QWidget *parent) :
	QWidget(parent),
	ui(new Ui::QtIsovalueBookmarkWidget)
{
	ui->setupUi(this);

	//	By default we will hide the title
	ui->label->setHidden(true);
}

///
/// \brief QtIsovalueBookmarkWidget::QtIsovalueBookmarkWidget
/// \param bookmarkArray
/// \param parent
/// \author		Dean
/// \since		05-08-2015
///
QtIsovalueBookmarkWidget::QtIsovalueBookmarkWidget(const IsovalueBookmarkList &bookmarkArray,
		QWidget *parent)
	: QtIsovalueBookmarkWidget(parent)
{
	try
	{
		m_bookmarkArray = IsovalueBookmarkList(bookmarkArray);

		for (unsigned long i = 0; i < m_bookmarkArray.GetBookmarkCount(); ++i)
		{
			ui->comboBoxBookmarks->addItem(QString::fromStdString(m_bookmarkArray[i].ToString()));
		}
	}
	catch (exception &ex)
	{
		fprintf(stderr,"Exception!  %s in func: %s (file: %s, line: %ld).\n",
				ex.what(), __func__, __FILE__, __LINE__);
		fflush(stderr);
	}
}

///
/// \brief		Add's a new bookmark at the current isovalue
/// \return
/// \author		Dean
/// \since		05-08-2015
///
bool QtIsovalueBookmarkWidget::addBookmark()
{
	try
	{
		//	Create the new bookmark item
		IsovalueBookmark newBookmark(m_isovalue);
		m_bookmarkArray.AddBookmark(newBookmark);

		//	Add to the combo box
		ui->comboBoxBookmarks->addItem(
					QString::fromStdString(newBookmark.ToString()));

		//	Select our new item
		ui->comboBoxBookmarks->setCurrentIndex(ui->comboBoxBookmarks->count()-1);

		//	Raise notification
		emit(OnBookmarkAdded(newBookmark));

		return true;
	}
	catch (exception &ex)
	{
		fprintf(stderr,"Exception!  %s in func: %s (file: %s, line: %ld).\n",
				ex.what(), __func__, __FILE__, __LINE__);
		fflush(stderr);
	}
	return false;
}

///
/// \brief		Removes the currently selected bookmark
/// \return
/// \author		Dean
/// \since		05-08-2015
///
bool QtIsovalueBookmarkWidget::removeBookmark()
{
	//	Check that the models (probably) match
	assert(m_bookmarkArray.GetBookmarkCount() == ui->comboBoxBookmarks->count());

	const int selectedIndex = ui->comboBoxBookmarks->currentIndex();

	//	Nothing selected
	if (selectedIndex == -1) return false;

	//	Make sure we have a valid selection
	assert(selectedIndex < m_bookmarkArray.GetBookmarkCount());

	try
	{
		//	Retreive the bookmark
		IsovalueBookmark bookmark = m_bookmarkArray[selectedIndex];

		//	Erase from the list and the UI
		m_bookmarkArray.RemoveBookmark(bookmark);
		ui->comboBoxBookmarks->removeItem(selectedIndex);

		//	Raise notification
		emit(OnBookmarkRemoved(bookmark));

		return true;
	}
	catch (exception &ex)
	{
		fprintf(stderr,"Exception!  %s in func: %s (file: %s, line: %ld).\n",
				ex.what(), __func__, __FILE__, __LINE__);
		fflush(stderr);
	}
	return false;
}

///
/// \brief		Sets the isovalue that bookmarks will be created at
/// \param isovalue
/// \author		Dean
/// \since		05-08-2015
///
void QtIsovalueBookmarkWidget::SetCurrentIsovalue(const float &isovalue)
{
	m_isovalue = isovalue;
}

QtIsovalueBookmarkWidget::~QtIsovalueBookmarkWidget()
{
	delete ui;
}

///
/// \brief		Responds to the user pressing the add bookmark button
/// \author		Dean
/// \since		05-08-2015
///
void QtIsovalueBookmarkWidget::on_pushButtonAdd_clicked()
{
	addBookmark();
}

///	\brief		Responds to the user pressing the remove bookmark button
/// \author		Dean
/// \since		05-08-2015
///
void QtIsovalueBookmarkWidget::on_pushButtonRemove_clicked()
{
	removeBookmark();
}

///
/// \brief		Responds to the user selecting a new bookmark from the combobox
/// \param index
/// \author		Dean
/// \since		05-08-2015
///
void QtIsovalueBookmarkWidget::on_comboBoxBookmarks_currentIndexChanged(int index)
{
	//	Check that the models (probably) match
	assert(m_bookmarkArray.GetBookmarkCount() == ui->comboBoxBookmarks->count());

	//	Nothing selected
	if (index == -1) return;

	//	Make sure we have a valid selection
	assert(index < m_bookmarkArray.GetBookmarkCount());

	printf("User selected bookmark with index: %ld.\n", index);
	fflush(stdout);

	//	Raise notification
	emit OnBookmarkSelected(m_bookmarkArray[index]);
}
