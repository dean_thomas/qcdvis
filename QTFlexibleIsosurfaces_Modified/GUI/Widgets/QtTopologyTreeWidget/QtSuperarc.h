#ifndef QT_SUPERARC_H
#define QT_SUPERARC_H

#include <QGraphicsLineItem>
#include <QPen>
#include <QVariant>
#include "Globals/QtFunctions.h"
#include <QDebug>
#include <Graphs/ContourTreeSuperarc.h>
#include <Graphs/ReebGraphSuperarc.h>

class SupernodeBase;

class QtSupernode;

//	Colour-blind safe colours
//	see: http://jfly.iam.u-tokyo.ac.jp/color/ [31-03-2016]
#define QCOLOR_CB_SAFE_BLUISH_GREEN QColor(0, 158, 115)
#define QCOLOR_CB_SAFE_YELLOW QColor(240, 228, 66)
#define QCOLOR_CB_SAFE_VERMILLION QColor(213, 94, 0)

class QtSuperarc : public QGraphicsLineItem
{
	public:
		enum class DataType { CT_ARC, RB_ARC };

	private:
		QVariant m_data;

		QtSupernode* m_sourceNode = nullptr;
		QtSupernode* m_targetNode = nullptr;

		QPen m_normalPen;
		QPen m_selectedPen;
		QPen m_highlightedPen;

		DataType m_dataType;
		//QVariant GetData() const { return m_data; }
		QString generateTooltip() const;
	public:

		QtSuperarc(const QVariant& data,
					QtSupernode* const sourceNode,
				   QtSupernode* const targetNode,
				   const DataType dataType);
		~QtSuperarc();

		QtSupernode* GetTopSupernode() const { return m_sourceNode; }
		QtSupernode* GetBottomSupernode() const { return m_targetNode; }

		virtual void paint(QPainter *painter,
						   const QStyleOptionGraphicsItem *option,
						   QWidget *widget);

		//virtual QRect boun

		float GetLength() const;
		/*
		template <typename T>
		T* GetData() const
		{
			qDebug() << "Can convert: " << m_data.canConvert<T>();

			auto arcBase = toPtr<T>(m_data);
			assert(arcBase != nullptr);
			return arcBase;
		}
		*/
		ContourTreeSuperarc* toContourTreeSuperarc() const
		{
			if (m_dataType == DataType::CT_ARC)
			{
				return toPtr<ContourTreeSuperarc>(m_data);
			}
			else return nullptr;
		}

		ReebGraphSuperarc* toReebGraphSuperarc() const
		{
			if (m_dataType == DataType::RB_ARC)
			{
				return toPtr<ReebGraphSuperarc>(m_data);
			}
			else return nullptr;
		}

		template <typename T>
		T* toGenericPointer() const
		{
			return toPtr<T>(m_data);
		}

		size_t GetId() const;

		void SetHighlighted(const bool value);
		bool GetHighlighted() const;

		void SetSelected(const bool value);
		bool GetSelected() const;

		void SetHidden(const bool value);
		bool GetHidden() const;

		void trackNodes();
};


#endif // QT_SUPERARC_H
