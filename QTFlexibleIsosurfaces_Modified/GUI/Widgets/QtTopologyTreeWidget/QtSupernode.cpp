#include "QtSupernode.h"
#include <cstdlib>
#include <iostream>
#include <sstream>

#include <QDebug>
#include <QRectF>
#include <QPainter>
#include <QApplication>
#include <QStyleOptionGraphicsItem>
#include <QGraphicsScene>

#include "QtSuperarc.h"
#include "Graphs/ContourTreeSupernode.h"
#include "Graphs/ReebGraphSupernode.h"
#include "Graphs/ReebGraph.h"

using namespace std;

#define DISABLE_DEBUG_OUTPUT

///
/// \brief	Returns the up degree from the underlying data structure
/// \return
/// \since	13-07-2016
/// \author	Dean
///
size_t QtSupernode::getUpDegree() const
{
	auto nodeBase = toPtr<SupernodeBase>(m_data);
	assert(nodeBase != nullptr);
	return nodeBase->GetUpDegree();
}

///
/// \brief	Returns the down degree from the underlying data structure
/// \return
/// \since	13-07-2016
/// \author	Dean
///
size_t QtSupernode::getDownDegree() const
{
	auto nodeBase = toPtr<SupernodeBase>(m_data);
	assert(nodeBase != nullptr);
	return nodeBase->GetDownDegree();
}

size_t QtSupernode::getTotalUpwardNodes() const
{
	auto nodeBase = toPtr<SupernodeBase>(m_data);
	assert(nodeBase != nullptr);
	return nodeBase->GetTotalNodesUp();
}

size_t QtSupernode::getTotalDownwardNodes() const
{
	auto nodeBase = toPtr<SupernodeBase>(m_data);
	assert(nodeBase != nullptr);
	return nodeBase->GetTotalNodesDown();
}

size_t QtSupernode::getTotalUpwardArcs() const
{
	auto nodeBase = toPtr<SupernodeBase>(m_data);
	assert(nodeBase != nullptr);
	return nodeBase->GetTotalArcsUp();
}

size_t QtSupernode::getTotalDownwardArcs() const
{
	auto nodeBase = toPtr<SupernodeBase>(m_data);
	assert(nodeBase != nullptr);
	return nodeBase->GetTotalArcsDown();
}

///
/// \brief	Generates a tool tip for the supernode
/// \return
/// \since	13-07-2016
/// \author	Dean
///
QString QtSupernode::generateTooltip() const
{
	const string ENDL = "<br/>";
	stringstream tooltip;
	tooltip << "<b>supernode</b>" << ENDL
			<< "id: " << GetId() << ENDL << ENDL
			<< "height: " << getHeight() << ENDL
			<< "up degree: " << getUpDegree() << ENDL
			<< "down degree: " << getDownDegree() << ENDL << ENDL
			<< "total up nodes: " << getTotalUpwardNodes() << ENDL
			<< "total down nodes: " << getTotalDownwardNodes() << ENDL << ENDL
			<< "total up arcs: " << getTotalUpwardArcs() << ENDL
			<< "total down arcs: " << getTotalDownwardArcs() << ENDL;
	return QString::fromStdString(tooltip.str());
}

///
/// \brief QtSupernode::HighlightUpperSubTree
/// \since	15-07-2016
/// \author	Dean
///
void QtSupernode::HighlightUpperSubTree()
{
	auto nodeBase = toPtr<SupernodeBase>(m_data);
	assert(nodeBase != nullptr);
	nodeBase->HighlightUpperSubtree();
}

///
/// \brief QtSupernode::HighlightLowerSubTree
/// \since	15-07-2016
/// \author	Dean
///
void QtSupernode::HighlightLowerSubTree()
{
	auto nodeBase = toPtr<SupernodeBase>(m_data);
	assert(nodeBase != nullptr);
	nodeBase->HighlightLowerSubtree();
}

void QtSupernode::SetHighlighted(const bool value)
{
	auto nodeBase = toPtr<SupernodeBase>(m_data);
	assert(nodeBase != nullptr);
	nodeBase->SetHighlighted(value);
}

bool QtSupernode::GetHighlighted() const
{
	auto nodeBase = toPtr<SupernodeBase>(m_data);
	assert(nodeBase != nullptr);
	return nodeBase->IsHighlighted();
}

void QtSupernode::SetSelected(const bool value)
{
	auto nodeBase = toPtr<SupernodeBase>(m_data);
	assert(nodeBase != nullptr);
	nodeBase->SetSelected(value);
}

bool QtSupernode::GetSelected() const
{
	auto nodeBase = toPtr<SupernodeBase>(m_data);
	assert(nodeBase != nullptr);
	return nodeBase->IsSelected();
}

void QtSupernode::SetHidden(const bool value)
{
	auto nodeBase = toPtr<SupernodeBase>(m_data);
	assert(nodeBase != nullptr);
	nodeBase->SetHidden(value);
}

bool QtSupernode::GetHidden() const
{
	auto nodeBase = toPtr<SupernodeBase>(m_data);
	assert(nodeBase != nullptr);
	return nodeBase->IsHidden();
}

void QtSupernode::hoverEnterEvent(QGraphicsSceneHoverEvent* event)
{
	m_isUnderMouse = true;

	update();
}

void QtSupernode::hoverLeaveEvent(QGraphicsSceneHoverEvent* event)
{
	m_isUnderMouse = false;

	update();
}

void QtSupernode::hoverMoveEvent(QGraphicsSceneHoverEvent* event)
{
	//this->
}

///
/// \brief QtContourTreeNode::addLink
/// \param arc
///	\since		08-02-2016
/// \author		Dean
///
void QtSupernode::addLink(QtSuperarc* arc)
{
	m_links.insert(arc);
}

///
/// \brief QtContourTreeNode::removeLink
/// \param arc
///	\since		08-02-2016
/// \author		Dean
///
void QtSupernode::removeLink(QtSuperarc* arc)
{
	//	Calling this seems to cause a segfault - presumably
	//	qt is deleting the items for us?
	//if (!m_links.empty())
	//{
		//if (m_links.contains(arc)) m_links.remove(arc);
	//}
}

///
/// \brief QtContourTreeNode::itemChange
/// \param change
/// \param value
/// \return
///	\since		08-02-2016
/// \author		Dean
///
QVariant QtSupernode::itemChange(GraphicsItemChange change, const QVariant &value)
{
	if (change == ItemPositionChange && scene())
	{
		// value is the requested position.
		QPointF newPos = value.toPointF();

		if (newPos.y() != ((1.0f - getNormalisedHeight()) * 1000.0))
		{
			// Fix the y position
			newPos.setY((1.0f - getNormalisedHeight()) * 1000.0);

			notifyLinksOfPositionChange();

			return newPos;
		}

		notifyLinksOfPositionChange();
	}
	return QGraphicsItem::itemChange(change, value);
}

///
/// \brief QtContourTreeNode::notifyLinksOfPositionChange
///	\since		08-02-2016
/// \author		Dean
///
void QtSupernode::notifyLinksOfPositionChange()
{
	for (auto& link : m_links)
	{
		link->trackNodes();
	}
}

///
/// \brief QtContourTreeNode::paint
/// \param painter
/// \param option
/// \param widget
///	\since		08-02-2016
/// \author		Dean
///
void QtSupernode::paint(QPainter *painter,
						const QStyleOptionGraphicsItem *option,
						QWidget *widget)
{
	painter->setClipRect(option->exposedRect);
	auto supernode = toPtr<SupernodeBase>(m_data);
	assert(supernode);

#ifndef DISABLE_DEBUG_OUTPUT
	cout << "Repainting supernode " << supernode->GetId() << "." << endl;
	cout << "\tHighlighted: " << supernode->IsHighlighted() << "." << endl;
	cout << "\tSelected: " << supernode->IsSelected() << "." << endl;
	//cout << "\tHidden: " << supernode->IsHidden() << "." << endl;
#endif

	//qDebug() << "Painted node";
	if (supernode->IsHighlighted())
	{
		painter->setBrush(m_highlightedBrush);
	}
	else
	{
		if (supernode->IsSelected())
		{
			painter->setBrush(m_selectedBrush);
		}
		else
		{
			painter->setBrush(m_defaultBrush);
		}
	}
	//painter->setPen(m_redPen);
	//painter->drawRect(m_boundingRect);

	painter->setPen(m_outlinePen);
	painter->drawPath(m_shape);
	painter->drawText(m_boundingRect,
					  Qt::AlignCenter,
					  m_text);

	setToolTip(generateTooltip());
}

///
/// \brief QtContourTreeNode::boundingRect
/// \return
///	\since		08-02-2016
/// \author		Dean
///
QRectF QtSupernode::boundingRect() const
{
	return m_boundingRect;
}

///
/// \brief QtContourTreeNode::~QtContourTreeNode
///	\since		08-02-2016
/// \author		Dean
///
QtSupernode::~QtSupernode()
{
	//	Calling this seems to cause a segfault - presumably
	//	qt is deleting the items for us?
	//for (auto& link : m_links)
	//{
	//	if (link != nullptr)
	//	{
	//		delete link;
	//	}
	//}
}

///
/// \brief QtContourTreeNode::QtContourTreeNode
/// \param supernode
///	\since		08-02-2016
/// \author		Dean
///
QtSupernode::QtSupernode(const QVariant& data,
						 const float initialPosX,
						 const DataType dataType)
	: m_data{data}, m_initialPosX{initialPosX}, m_dataType{dataType}
{
	//cout << "Created ContourTree node" << endl;

	m_outlinePen = QPen(Qt::black);

	m_defaultBrush = QBrush(Qt::gray);
	m_selectedBrush = QBrush(Qt::green);
	m_selectedBrush.setColor(QCOLOR_CB_SAFE_BLUISH_GREEN);

	m_highlightedBrush = QBrush(Qt::red);
	m_highlightedBrush.setColor(QCOLOR_CB_SAFE_VERMILLION);

	m_blackPen = QPen(Qt::black);
	//m_redPen = QPen(Qt::red);

	m_boundingRect = QRectF(0, 0, DIAMETER, DIAMETER);
	m_boundingRect.translate(-m_boundingRect.center());

	//	Pre-draw the ellipse (adjusting for the pen size)
	m_shape.addEllipse(m_boundingRect.adjusted(+(0.5 * m_outlinePen.widthF()),
											   +(0.5 * m_outlinePen.widthF()),
											   -(0.5 * m_outlinePen.widthF()),
											   -(0.5 * m_outlinePen.widthF())));

	//setCacheMode(QGraphicsItem::DeviceCoordinateCache);
	setFlags(QGraphicsItem::ItemIsMovable
			 | QGraphicsItem::ItemIsSelectable
			 | QGraphicsItem::ItemSendsGeometryChanges);
	setAcceptHoverEvents(true);
	//	Set initial position
	auto y = 1.0f - getNormalisedHeight();
	setPos(1000 * m_initialPosX, 1000 * y);

	//	Set node text (and if we are in debug mode, a string representation of
	//	the underlying supernode).
	m_text = QString("%1").arg(GetId());
}

///
/// \brief QtContourTreeNode::getNormalisedHeight
/// \return
///	\since		08-02-2016
/// \author		Dean
///
float QtSupernode::getNormalisedHeight() const
{
	auto supernode = toPtr<SupernodeBase>(m_data);
	assert(supernode != nullptr);

	auto nHeight = supernode->GetNormalisedHeight();

#ifndef NDEBUG
	cout << nHeight << endl;
#endif
	assert((nHeight >= 0.0) && (nHeight <= 1.0));

	return nHeight;
}

///
/// \brief QtContourTreeNode::getNormalisedHeight
/// \return
///	\since		13-07-2016
/// \author		Dean
///
float QtSupernode::getHeight() const
{
	auto supernode = toPtr<SupernodeBase>(m_data);
	assert(supernode != nullptr);

	auto height = supernode->GetHeight();

	return height;
}

///
/// \brief QtContourTreeNode::getUniqueId
/// \return
///	\since		08-02-2016
/// \author		Dean
///
size_t QtSupernode::GetId() const
{
	auto supernode = toPtr<SupernodeBase>(m_data);
	assert(supernode != nullptr);

	return supernode->GetId();
}
