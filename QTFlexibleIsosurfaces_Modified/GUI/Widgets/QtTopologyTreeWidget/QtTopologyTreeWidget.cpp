#include "QtTopologyTreeWidget.h"
#include "ui_QtTopologyTreeWidget.h"

#include "QtSupernode.h"
#include "QtSuperarc.h"
#include "QtZeroLine.h"
#include "GUI/Forms/QtPropertiesDialog.h"

#include "QtCurrentIsovalueLine.h"
#include "Globals/QtFunctions.h"
#include "Globals/QtOstream.h"

#include "Graphs/ReebGraph.h"
#include <QRectF>
#include <QSize>

#include <QDebug>
#include <QWheelEvent>
#include <QMenu>
#include <QAction>
#include <QtPrintSupport/QPrinter>

using namespace std;

#define DISABLE_DEBUG_OUTPUT

void QtTopologyTreeWidget::SetCurrentIsovalueLineVisible(const bool value)
{
	m_currentIsovalueLine->setVisible(value);
}

void QtTopologyTreeWidget::SaveToPdf(const QString& filename) const
{
	QPrinter pdf_printer;
	pdf_printer.setOutputFormat(QPrinter::PdfFormat);
	//	Set the height slightly larger so that the top and bottom nodes
	//	aren't cut off
	pdf_printer.setPaperSize(QSize(m_graphicsScene->sceneRect().width(),
								   m_graphicsScene->sceneRect().height()),
							 QPrinter::Point);
	pdf_printer.setFullPage(true);
	pdf_printer.setOutputFileName(filename);

	QPainter pdf_painter;
	pdf_painter.begin(&pdf_printer);

	auto node_radius = QtSupernode::DIAMETER / 2.0;

	//	Render the image slightly small than the page so that the top and
	//	bottom nodes are not cut off...
	m_graphicsScene->render(&pdf_painter,
							QRectF(0, 2 * node_radius, pdf_printer.width(),
								   pdf_printer.height() - (4 * node_radius)));

	pdf_painter.end();
}

///
/// \brief		Shows a pop-up menu to allow us to interact with the
///				tree as a whole
/// \since		12-12-2016
/// \author		Dean
///
void QtTopologyTreeWidget::generic_popup(const QPoint& pos)
{
	//	Create the menu
	QMenu context_menu(this);

	QAction action_save_to_pdf("Save as PDF...", this);
	connect(&action_save_to_pdf, &QAction::triggered, [&]()
	{
		//	We probably should show a file dialog, but this will do
		//	for now...
		SaveToPdf("/home/dean/Desktop/reeb_graph.pdf");
	});
	context_menu.addAction(&action_save_to_pdf);

	//	And show the menu
	context_menu.exec(mapToGlobal(pos));
}

void QtTopologyTreeWidget::SetExtents(const size_t x_lim,
									  const size_t y_lim,
									  const size_t z_lim)
{
	m_domain_limits = make_tuple(x_lim, y_lim, z_lim);
}

///
/// \brief		Returns a list of only the superarcs present in the view
/// \return
/// \since		29-03-2016
/// \author		Dean
///
std::vector<QtSuperarc*> QtTopologyTreeWidget::getListOfSuperarcs() const
{
	vector<QtSuperarc*> result;

	//	Iterate through all items in the scene, extracting only the arcs
	auto allItems = m_graphicsScene->items();
	for (auto& graphicsItem : allItems)
	{
		//	Is the item a superarc graphical item
		if (auto superarc = dynamic_cast<QtSuperarc*>(graphicsItem))
		{
			result.push_back(superarc);
		}
	}
	return result;
}

std::vector<QtSupernode*> QtTopologyTreeWidget::getListOfSupernodes() const
{
	vector<QtSupernode*> result;

	//	Iterate through all items in the scene, extracting only the arcs
	auto allItems = m_graphicsScene->items();
	for (auto& graphicsItem : allItems)
	{
		//	Is the item a superarc graphical item
		if (auto supernode= dynamic_cast<QtSupernode*>(graphicsItem))
		{
			result.push_back(supernode);
		}
	}

	return result;
}

///
/// \brief		Clears the highlighted flag for all superarc items in the view
/// \since		29-03-2016
/// \author		Dean
///
void QtTopologyTreeWidget::clearHighlightedArcs()
{
	auto arcList = getListOfSuperarcs();

	for (auto& arc : arcList)
	{
		auto arcBase = arc->toGenericPointer<superarc_base_type>();
		assert(arcBase != nullptr);

		arcBase->SetHighlighted(false);
	}
}

void QtTopologyTreeWidget::clearHighlightedNodes()
{
	auto nodeList = getListOfSupernodes();

	for (auto& node : nodeList)
	{
		auto nodeBase = node->toGenericPointer<supernode_base_type>();
		assert(nodeBase != nullptr);

		nodeBase->SetHighlighted(false);
	}
}

///
/// \brief		Iterates through all items selected in the widget and returns
///				a list of selcted nodes and arcs
/// \return		A pair of lists: selected nodes and selected arcs
/// \author		Dean
/// \since		29-03-2016
///
QtTopologyTreeWidget::SelectionList QtTopologyTreeWidget::getListsOfSelectedItems() const
{
	//	Filter the selected items into the original data model items they wrap
	BaseSupernodeList baseSelectedNodes;
	BaseSuperarcList baseSelectedArcs;
	QtSupernodeList qtSelectedNodes;
	QtSuperarcList qtSelectedArcs;


	auto selectedItems = m_graphicsScene->selectedItems();
	for (auto it = selectedItems.cbegin(); it != selectedItems.cend(); ++it)
	{
		auto graphicsItem = *it;
#ifndef DISABLE_DEBUG_OUTPUT
		cout << *graphicsItem << endl;
#endif
		//	Test if the selected item is a supernode glyph
		if (auto node = dynamic_cast<QtSupernode*>(graphicsItem))
		{
			// if so map back to the original tree node and add to the
			//	selection list
			auto nodeBase = node->toGenericPointer<supernode_base_type>();
			assert(nodeBase != nullptr);
			baseSelectedNodes.push_back(nodeBase);
			qtSelectedNodes.push_back(node);
		}
		//	Test if the selected item is a superarc 'glyph'
		//	don't forget to add lookup items...
		else if (auto arc = dynamic_cast<QtSuperarc*>(graphicsItem))
		{
			auto arcBase = arc->toGenericPointer<superarc_base_type>();

			baseSelectedArcs.push_back(arcBase);
			qtSelectedArcs.push_back(arc);
		}
		//	else Something is probably wrong...
	}

	return { baseSelectedNodes, qtSelectedNodes,
				baseSelectedArcs, qtSelectedArcs };
}

///
/// \brief		Displays a popup menu for a group of arcs / nodes.  Objects
///				acted upon are those currently marked selceted in the widget
/// \param pos
/// \author		Dean
/// \since		29-03-2016
///
void QtTopologyTreeWidget::groupPopupMenu(const QPoint& pos, const SelectionList& selected_items)
{
	assert(!selected_items.empty());

	//	Create the menu
	QMenu contextMenu(this);

	//	Create some global commands
	auto showHideArcsFunc = [&](const bool hidden)
	{
#ifndef DISABLE_DEBUG_OUTPUT
		cout << "Setting the state of " << selectedItems.selectedBaseArcs.size()
			 << "superarcs to "
			 << (hidden ? "hidden" : "visible") << "." << endl;
#endif
		for (auto& arc : selected_items.selectedBaseArcs)
		{
			arc->SetHidden(hidden);
		}

		//	Make sure the tree view is up-to-date
		m_graphicsScene->update();

		//	And send a signal to anything else monitoring state
		emit(HiddenSuperarcsChanged(selected_items.selectedQtArcs));
	};
	auto hideAllArcsFunc = std::bind(showHideArcsFunc, true);
	auto showAllArcsFunc = std::bind(showHideArcsFunc, false);

	//	First add actions relating to superarcs
	//	=======================================
	//	Create each action and how it responds to user input
	QAction showAllAction("Show all", this);
	connect(&showAllAction, &QAction::triggered, showAllArcsFunc);
	QAction hideAllAction("Hide all", this);
	connect(&hideAllAction, &QAction::triggered, hideAllArcsFunc);


	//	And now supernodes
	//	==================
	//	...


	//	Add the actions
	contextMenu.addAction(&showAllAction);
	contextMenu.addAction(&hideAllAction);

	//	Allow the user to revert to autoscale
	QAction autoScaleAction("Auto scale", this);
	connect(&autoScaleAction, &QAction::triggered,
			[&]()
	{
		DoAutoScale();
	});
	contextMenu.addAction(&autoScaleAction);

	//	And show the menu
	contextMenu.exec(mapToGlobal(pos));



}

///
/// \brief		Creates and executes a popup menu for interacting with a
///				particular supernode in the tree
/// \param pos
/// \param supernode
/// \author		Dean
/// \since		29-03-2016
///
void QtTopologyTreeWidget::supernodePopupMenu(const QPoint& pos,
											  QtSupernode* const supernode)
{
	assert(supernode != nullptr);

	//	Create the menu
	QMenu contextMenu(this);

	//	Nothing to do for supernodes yet...
	QAction autoScaleAction("Auto scale", this);
	connect(&autoScaleAction, &QAction::triggered,
			[&]()
	{
		DoAutoScale();
	});
	contextMenu.addAction(&autoScaleAction);

	//	And show the menu
	contextMenu.exec(mapToGlobal(pos));
}

///
/// \brief		Creates and executes a popup menu for interacting with a
///				particular superarc in the tree
/// \param		pos
/// \param		superarc
/// \author		Dean
/// \since		29-03-2016
///
void QtTopologyTreeWidget::superarcPopupMenu(const QPoint& pos,
											 QtSuperarc* const superarc)
{
	assert(superarc != nullptr);

	//	Create the menu
	QMenu contextMenu(this);

	//	Create each action and how it responds to user input
	QAction visibleAction("Visible", this);
	visibleAction.setCheckable(true);
	visibleAction.setChecked(!superarc->GetHidden());
	connect(&visibleAction, &QAction::toggled,
			[&](const bool checked)
	{
#ifndef DISABLE_DEBUG_OUTPUT
		cout << "Setting superarc: " << superarc->GetId() << " to "
			 << (checked ? "visible" : "hidden") << "." << endl;
#endif
		superarc->SetHidden(!checked);

		//	Make sure the tree view is up-to-date
		m_graphicsScene->update();

		//	And send a signal to anything else monitoring state
		emit(HiddenSuperarcsChanged({superarc}));
	});

	auto func_show_properties_dialog = [&](ContourTreeSuperarc* superarc)
	{
		QtPropertiesDialog properties_dialog(this);
		properties_dialog.setModal(true);
		properties_dialog.SetSuperarc(superarc);
		properties_dialog.exec();
	};

	//	Show the object as an isolated set of nested isosurfaces
	auto func_isolate_object = [&](ContourTreeSuperarc* superarc)
	{
		//	emit a signal for the controller to pickup
		emit RequestNestedContoursForObject(superarc);
	};

	//	map back to the original tree arc and add to the
	auto arc = superarc->toContourTreeSuperarc();

	//	Add the actions
	contextMenu.addAction(&visibleAction);

	QAction action_isolate_object("Isolate object", this);
	connect(&action_isolate_object, &QAction::triggered, std::bind(func_isolate_object, arc));
	action_isolate_object.setEnabled(arc != nullptr);
	contextMenu.addAction(&action_isolate_object);

	QAction action_show_properties("Object properties...", this);
	connect(&action_show_properties, &QAction::triggered, std::bind(func_show_properties_dialog, arc));
	action_show_properties.setEnabled(arc != nullptr);
	contextMenu.addAction(&action_show_properties);

	QAction autoScaleAction("Auto scale", this);
	connect(&autoScaleAction, &QAction::triggered,
			[&]()
	{
		DoAutoScale();
	});
	contextMenu.addAction(&autoScaleAction);

	//	And show the menu
	contextMenu.exec(mapToGlobal(pos));
}

void QtTopologyTreeWidget::SetCurrentNormalisedIsovalue(const T value)
{
	assert(m_currentIsovalueLine != nullptr);
	assert((value >= 0.0) && (value <= 1.0));

	m_currentIsovalueLine->SetCurrentNormalisedIsovalue(value);
}

QtTopologyTreeWidget::T QtTopologyTreeWidget::GetCurrentNormalisedIsovalue() const
{
	assert(m_currentIsovalueLine != nullptr);

	return m_currentIsovalueLine->GetCurrentNormalisedIsovalue();
}


///
/// \brief		Sets the autoscale property and rescales if required
/// \param value
/// \author		Dean
/// \since		03-02-2016
///
void QtTopologyTreeWidget::SetAutoScale(const bool& value)
{
	m_autoScale = value;

	if (m_autoScale) DoAutoScale();
}

///
/// \brief		Handles selections in the tree widget.  At present only
///				collects a list of selected superarcs / nodes - this is then
///				forwarded to an external event handler.
/// \since		16-03-2016
/// \author		Dean
/// \author		Dean: modify to call private function for return a pair of
///				lists [20-03-2016]
///
void QtTopologyTreeWidget::selectionChangedHandler()
{
	qDebug() << "Selection has changed";

	//	Call a function to return the list of nodes and arcs
	auto selectedItems = getListsOfSelectedItems();

#ifdef DISABLE_EXTENDED_SELECT
	//	Temporarily disable the signal to avoid spamming the event system
	disconnect(m_graphicsScene.get(), SIGNAL(selectionChanged()),
			   this, SLOT(selectionChangedHandler()));

	//	Use a temporay store to avoid propogating too far through the tree
	BaseSuperarcList tempBaseArcs;
	QtSuperarcList tempQtArcs;

	//	Add the connected superarcs
	for (auto& item : selectedItems.selectedQtNodes)
	{
		for (auto& arc : item->getLinks())
		{
			auto arcBase = arc->GetData<superarc_base_type>();
			assert(arcBase != nullptr);

			arcBase->SetSelected(true);
			arc->setSelected(true);

			tempQtArcs.push_back(arc);
			tempBaseArcs.push_back(arcBase);
		}
	}

	//	Add the connected supernodes
	for (auto& item : selectedItems.selectedQtArcs)
	{
		auto topNode = item->getTopNode();
		auto topNodeBase = topNode->GetData<supernode_base_type>();
		assert(topNodeBase != nullptr);

		topNodeBase->SetSelected(true);
		topNode->setSelected(true);
		selectedItems.selectedQtNodes.push_back(topNode);
		selectedItems.selectedBaseNodes.push_back(topNodeBase);

		auto bottomNode = item->getBottomNode();
		auto bottomNodeBase = bottomNode->GetData<supernode_base_type>();
		assert(bottomNodeBase != nullptr);

		bottomNodeBase->SetSelected(true);
		bottomNode->setSelected(true);
		selectedItems.selectedQtNodes.push_back(bottomNode);
		selectedItems.selectedBaseNodes.push_back(bottomNodeBase);
	}

	//	Merge the temp lists into the main lists
	for (auto& item : tempBaseArcs)
	{
		selectedItems.selectedBaseArcs.push_back(item);
	}
	for (auto& item : tempQtArcs)
	{
		selectedItems.selectedQtArcs.push_back(item);
	}

	//	Re-enable the signal
	connect(m_graphicsScene.get(), SIGNAL(selectionChanged()),
			this, SLOT(selectionChangedHandler()));
#endif
	//	Raise external event
	emit (SelectedSupernodesChanged(selectedItems.selectedQtNodes));
	emit (SelectedSuperarcsChanged(selectedItems.selectedQtArcs));
}

///
/// \author		Dean
/// \since		02-02-2016
///
QtTopologyTreeWidget::QtTopologyTreeWidget(QWidget *parent) :
	QGraphicsView(parent),
	ui(new Ui::QtTopologyTreeWidget)
{
	ui->setupUi(this);

	m_graphicsScene = QGraphicsScene_uPtr(new QGraphicsScene(0,0,1000, 1000));
	setScene(m_graphicsScene.get());
	setDragMode(QGraphicsView::RubberBandDrag);
	setRenderHints(QPainter::Antialiasing | QPainter::TextAntialiasing);

	connect(m_graphicsScene.get(), SIGNAL(selectionChanged()),
			this, SLOT(selectionChangedHandler()));

	setContextMenuPolicy(Qt::CustomContextMenu);
	connect(this, &QtTopologyTreeWidget::customContextMenuRequested,
			[&](const QPoint& pos)
	{
		qDebug() << "Requested context menu";

		auto item = itemAt(pos);
		if (item != nullptr)
		{
			//	Show the popup-menu for a single item
			qDebug() << item->toolTip();

			if (auto superarcItem = dynamic_cast<QtSuperarc*>(item))
			{
				superarcPopupMenu(pos, superarcItem);
			}
			else if (auto supernodeItem = dynamic_cast<QtSupernode*>(item))
			{
				supernodePopupMenu(pos, supernodeItem);
			}
		}
		else
		{
			//	Find the selected items
			auto selectedItems = getListsOfSelectedItems();

			if (!selectedItems.empty())
			{
				//	Show the popup-menu for a group of items
				groupPopupMenu(pos, selectedItems);
			}
			else
			{
				//	Show a generic pop-up menu allowing us to save to PDF etc
				generic_popup(pos);
			}
		}
	});

	update();
}

///
/// \brief QtTopologyTreeWidget::~QtTopologyTreeWidget
/// \author		Dean
/// \since		02-02-2016
///
QtTopologyTreeWidget::~QtTopologyTreeWidget()
{
	delete ui;
}

///
/// \brief		Handles auto scaling (if required)
/// \param event
/// \author		Dean
/// \since		02-02-2016
///
void QtTopologyTreeWidget::resizeEvent(QResizeEvent* event)
{
	if (m_autoScale) DoAutoScale();

	QGraphicsView::resizeEvent(event);
}

void QtTopologyTreeWidget::wheelEvent(QWheelEvent* event)
{
	if (event->delta() > 0)
	{
		zoomIn();
	}
	else if (event->delta() < 0)
	{
		zoomOut();
	}

	event->accept();
}

///
/// \brief		If the user moves the mouse over a node or arc, raise an
///				event so that it can be highlighted in the isosurface view
/// \param		event: not used (passed to base function)
/// \since		17-03-2016
/// \author		Dean
/// \author		Dean: cache the highlighted object so that events don't
///				need to be triggered when there is no actually state change
///				[29-03-2016]
///
void QtTopologyTreeWidget::mouseMoveEvent(QMouseEvent* event)
{
	QGraphicsView::mouseMoveEvent(event);

	auto item = itemAt(event->pos());
	if (!item)
	{
		//	The easy case is that there is nothing highlighted, so handle that
		//	first...

		//	Clear any checked arcs and remove from the cache
		clearHighlightedArcs();
		clearHighlightedNodes();

		m_previouslyHighlightedSuperarc = nullptr;
		m_previouslyHighlightedSupernode = nullptr;

		//	Issue a repaint and exit immediately
		m_graphicsScene->update();
		emit(HighlightedSupernodesChanged({}));
		emit(HighlightedSuperarcsChanged({}));

		return;
	}

	if (auto node = dynamic_cast<QtSupernode*>(item))
	{
		//	Is the selected item a supernode graphics?
		//
		//	if so, map back to the original tree node and add to the
		//	selection list
		auto nodeBase = node->toGenericPointer<supernode_base_type>();
		assert(nodeBase != nullptr);

		if (node != m_previouslyHighlightedSupernode)
		{
			//	So that we don't issue any un-needed repaints throughout the
			//	system we'll cache the current node for the next call
			m_previouslyHighlightedSupernode = node;

			//	Clear selection / select new node etc...
			clearHighlightedNodes();
			clearHighlightedArcs();

			if (event->modifiers().testFlag(Qt::ShiftModifier))
			{
				if (event->modifiers().testFlag(Qt::AltModifier))
				{
					node->HighlightLowerSubTree();
				}
				else
				{
					node->HighlightUpperSubTree();
				}
			}
			else
			{
				//	Add the node to the list
				//nodes.push_back(node);
				nodeBase->SetHighlighted(true);

				//	And the superarcs
				for (auto& arc : node->getLinks())
				{
					auto arcBase = arc->toGenericPointer<superarc_base_type>();
					assert(arcBase != nullptr);

					//arcs.push_back(arc);
					arcBase->SetHighlighted(true);
				}
			}

			//	Issue a repaint, and raise a notification elsewhere
			m_graphicsScene->update();

			QtSuperarcList highlighted_arcs;
			auto all_arcs = getListOfSuperarcs();
			copy_if(all_arcs.cbegin(), all_arcs.cend(),
					back_inserter(highlighted_arcs),
					[&](QtSuperarc* arc)
			{
				return arc->GetHighlighted();
			});

			QtSupernodeList highlighted_nodes;
			auto all_nodes = getListOfSupernodes();
			copy_if(all_nodes.cbegin(), all_nodes.cend(),
					back_inserter(highlighted_nodes),
					[&](QtSupernode* node)
			{
				return node->GetHighlighted();
			});


			//	Raise an event
			emit(HighlightedSuperarcsChanged(highlighted_arcs));
			emit(HighlightedSupernodesChanged(highlighted_nodes));
		}
	}
	else if (auto arc = dynamic_cast<QtSuperarc*>(item))
	{
		//	Is it a superarc graphics?
		//
		//	if so, get the superarc in the data model
		auto arcBase = arc->toGenericPointer<superarc_base_type>();
		assert(arcBase != nullptr);

		if (arc != m_previouslyHighlightedSuperarc)
		{
			//	If an arc is highlighted, we will highlight it and it's
			//	two associated supernodes
			QtSuperarcList arcs;
			QtSupernodeList nodes;

			//	So that we don't issue any un-needed repaints throughout the
			//	system we'll cache the current arc for the next call
			m_previouslyHighlightedSuperarc = arc;

			//	As the arc has changed revert to a base state and set the flag
			//	the relevent superarc
			clearHighlightedArcs();
			clearHighlightedNodes();

			//	Add the arc to the list
			arcs.push_back(arc);
			arcBase->SetHighlighted(true);

			//	And the two supernodes
			nodes.push_back(arc->GetTopSupernode());
			arcBase->GetTopSupernode()->SetHighlighted(true);
			nodes.push_back(arc->GetBottomSupernode());
			arcBase->GetBottomSupernode()->SetHighlighted(true);

			//	Issue a repaint, and raise a notification elsewhere
			m_graphicsScene->update();

			//	Raise an event
			emit(HighlightedSuperarcsChanged(arcs));
			emit(HighlightedSupernodesChanged(nodes));
		}
	}
}

///
/// \brief		Scales the scene so that it fits onto the widgets drawing
///				area
/// \author		Dean
/// \since		03-02-2016
///
void QtTopologyTreeWidget::DoAutoScale()
{
	setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
	setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);

	//	Target size is the current size of the widget (minus the border)
	auto targetW = width() - (frameWidth() * 2);
	auto targetH = height() - (frameWidth() * 2);

	//	Ratio required to reach the targets
	auto scaleW = (float)targetW / (float)m_graphicsScene->width();
	auto scaleH = (float)targetH / (float)m_graphicsScene->height();

	//	Find the smallest so it can be applied to both axes
	auto smallest = qMin(scaleW, scaleH);

	//	Reset the transform matrix and set the scale directly
	resetTransform();
	m_scale = smallest;
	scale(scaleH, scaleH);
	//qDebug() << smallest;

	m_autoScale = true;
}

//void QtTopologyTreeWidget::paintEvent(QPaintEvent* event)
//{
//	Just pass through to the base function for now
//QGraphicsView::paintEvent(event);

//qDebug() << "Repainting widget";
//}

#define USING_STL_VECTOR

///
/// \brief		Displays the given Reeb graph
/// \param reebGraph
/// \author		Dean
/// \since		03-02-2016
///
void QtTopologyTreeWidget::DisplayReebGraph(ReebGraph* const reebGraph)
{
	assert(reebGraph != nullptr);

	m_graphicsScene->clear();

#ifndef DISABLE_DEBUG_OUTPUT
	if (typeid(ReebGraph::SuperarcContainer) == typeid(std::vector<ReebGraph::ArcType>))
	{
		cout << "Underlying container is vector." << endl;
	}
	else
	{
		cout << "Underlying container is map." << endl;
	}
#endif

	//	Mappings from model to view (and back) when constructing the
	//	Superarc items
	NodeMapFromBase m_nodesFromBase;
	NodeMapToBase m_nodesToBase;

	//	Used to calculate the initial position of the node in the x-axis
	auto transferFunctionPosX =
			[&](SupernodeBase* const node, vec_3 domain_limits) -> float
	{
		//	Position of the node in space
		auto x = get<0>(node->GetPosition());
		auto y = get<1>(node->GetPosition());
		auto z = get<2>(node->GetPosition());

		//	Dimensions offield
		auto dimX = get<0>(domain_limits);
		auto dimY = get<1>(domain_limits);
		auto dimZ = get<2>(domain_limits);

		assert(dimX + dimY + dimZ != 0);

		//	This is basically the same scheme used by Hamish originally
		return (float)(x + y + z) / (dimX + dimY + dimZ);
	};

	for (auto it = reebGraph->Supernodes_cbegin(); it != reebGraph->Supernodes_cend(); ++it)
	{
#ifdef USING_STL_VECTOR
		auto node = *it;
#else
		auto node = it->second;
#endif
		auto newNode = new QtSupernode(toQVariant(node),
									   transferFunctionPosX(node, m_domain_limits),
									   QtSupernode::DataType::RB_NODE);

		//	Add to the scene
		m_graphicsScene->addItem(newNode);

		//	Add a map item to transform between the glyph representation
		//	and original data (and back again)
		m_nodesFromBase[node] = newNode;
		m_nodesToBase[newNode] = node;
	}

	for (auto it = reebGraph->Superarcs_cbegin(); it != reebGraph->Superarcs_cend(); ++it)
	{
#ifdef USING_STL_VECTOR
		auto superarc = *it;
#else
		auto superarc = it->second;
#endif
		auto source = m_nodesFromBase.find(superarc->GetTopSupernode());
		assert(source != m_nodesFromBase.cend());

		auto target = m_nodesFromBase.find(superarc->GetBottomSupernode());
		assert(target != m_nodesFromBase.cend());

		auto newArc = new QtSuperarc(toQVariant(superarc),
									 source->second,
									 target->second,
									 QtSuperarc::DataType::RB_ARC);

		m_graphicsScene->addItem(newArc);

		//		m_arcsFromBase[superarc] = newArc;
		//m_arcsToBase[newArc] = superarc;
	}

	m_graphicsScene->addItem(new QtZeroLine(reebGraph->GetMinHeight(),
											reebGraph->GetMaxHeight()));

	m_currentIsovalueLine = new QtCurrentIsovalueLine;
	m_graphicsScene->addItem(m_currentIsovalueLine);

	DoAutoScale();

	update();
}

///
/// \brief	Displays the given ContourTree
/// \param contourTree
/// \author		Dean
/// \since		02-02-2016
///
void QtTopologyTreeWidget::DisplayContourTree(ContourTree* const contourTree)
{
	assert (contourTree != nullptr);
	m_graphicsScene->clear();

	//	Mappings from model to view (and back) when constructing the
	//	Superarc items
	NodeMapFromBase m_nodesFromBase;
	NodeMapToBase m_nodesToBase;

	//	Used to calculate the initial position of the node in the x-axis
	auto transferFunctionPosX =
			[&](SupernodeBase* const node, vec_3 domain_limits) -> float
	{
		//	Position of the node in space
		auto x = get<0>(node->GetPosition());
		auto y = get<1>(node->GetPosition());
		auto z = get<2>(node->GetPosition());

		//	Dimensions offield
		auto dimX = get<0>(domain_limits);
		auto dimY = get<1>(domain_limits);
		auto dimZ = get<2>(domain_limits);

		assert(dimX + dimY + dimZ != 0);

		//	This is basically the same scheme used by Hamish originally
		return (float)(x + y + z) / (dimX + dimY + dimZ);
	};

	for (auto it = contourTree->Supernodes_cbegin(); it != contourTree->Supernodes_cend(); ++it)
	{
#ifdef USING_STL_VECTOR
		auto node = *it;
#else
		auto node = it->second;
#endif

		//	Calculate the relative position of the node in the heightfield
		auto heightfield3 = contourTree->GetHeightfield();
		assert(heightfield3 != nullptr);
		auto posX = transferFunctionPosX(node, m_domain_limits);

		//	Create the node
		auto newNode = new QtSupernode(toQVariant(node), posX, QtSupernode::DataType::CT_NODE);

		//	Add to the scene
		m_graphicsScene->addItem(newNode);

		//	Add a map item to transform between the glyph representation
		//	and original data (and back again)
		m_nodesFromBase[node] = newNode;
		m_nodesToBase[newNode] = node;
	}

	for (auto it = contourTree->Superarcs_cbegin(); it != contourTree->Superarcs_cend(); ++it)
	{
#ifdef USING_STL_VECTOR
		auto superarc = *it;
#else
		auto superarc = it->second;
#endif

		auto source = m_nodesFromBase.find(superarc->GetTopSupernode());
		assert(source != m_nodesFromBase.cend());

		auto target = m_nodesFromBase.find(superarc->GetBottomSupernode());
		assert(target != m_nodesFromBase.cend());

		auto newArc = new QtSuperarc(toQVariant(superarc),
									 source->second,
									 target->second,
									 QtSuperarc::DataType::CT_ARC);

		m_graphicsScene->addItem(newArc);

		//		m_arcsFromBase[superarc] = newArc;
		//		m_arcsToBase[newArc] = superarc;
	}

	m_graphicsScene->addItem(new QtZeroLine(contourTree->GetMinHeight(),
											contourTree->GetMaxHeight()));


	m_currentIsovalueLine = new QtCurrentIsovalueLine;
	m_graphicsScene->addItem(m_currentIsovalueLine);

	DoAutoScale();

	update();
}

#undef USING_STL_VECTOR

///
/// \brief		Convenience method for multiplying the scale factor by a
///				given amount on both axes
/// \param		factor: > 1.0f to zoom in, < 1.0f to zoom out
/// \author		Dean
/// \since		02-02-2016
///
void QtTopologyTreeWidget::scaleBy(const float factor)
{
	m_scale *= factor;

	scale(factor, factor);
}

///
/// \brief		Zooms the view by the delta constant, also clears the auto
///				scale flag
/// \author		Dean
/// \since		02-02-2016
///
void QtTopologyTreeWidget::zoomIn()
{
	qDebug() << "Current scale: " << m_scale;

	if (m_scale < SCALE_MAX)
		scaleBy(1.0 + SCALE_DELTA);

	m_autoScale = false;

	setHorizontalScrollBarPolicy(Qt::ScrollBarAsNeeded);
	setVerticalScrollBarPolicy(Qt::ScrollBarAsNeeded);
}

///
/// \brief		Zooms the view by the delta constant, also clears the auto
///				scale flag
/// \author		Dean
/// \since		02-02-2016
///
void QtTopologyTreeWidget::zoomOut()
{
	qDebug() << "Current scale: " << m_scale;

	if (m_scale > SCALE_MIN)
		scaleBy(1.0f - SCALE_DELTA);

	m_autoScale = false;

	setHorizontalScrollBarPolicy(Qt::ScrollBarAsNeeded);
	setVerticalScrollBarPolicy(Qt::ScrollBarAsNeeded);
}

void QtTopologyTreeWidget::paintEvent(QPaintEvent *event)
{
#ifndef DISABLE_DEBUG_OUTPUT
	cout << "############# Redrawing everything #########" << endl;
	cout << this->accessibleName() << endl;
#endif


	viewport()->update();
	//m_graphicsScene->update(sceneRect());

	QGraphicsView::paintEvent(event);
#ifndef DISABLE_DEBUG_OUTPUT
	cout << "############################################" << endl;
#endif

}
