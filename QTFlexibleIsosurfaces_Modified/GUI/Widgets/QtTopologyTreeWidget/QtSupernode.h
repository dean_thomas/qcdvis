#ifndef QT_SUPERNODE_H
#define QT_SUPERNODE_H

#include <QGraphicsItem>
#include <QVariant>
#include <QSet>
#include <QPen>
#include <QBrush>
#include <QString>
#include "Globals/QtFunctions.h"
#include "Graphs/SupernodeBase.h"
#include "Graphs/ContourTreeSupernode.h"
#include "Graphs/ReebGraphSupernode.h"
//class SupernodeBase;
class QtSuperarc;

//	Colour-blind safe colours
//	see: http://jfly.iam.u-tokyo.ac.jp/color/ [31-03-2016]
#define QCOLOR_CB_SAFE_BLUISH_GREEN QColor(0, 158, 115)
#define QCOLOR_CB_SAFE_YELLOW QColor(240, 228, 66)

class QtSupernode : public QGraphicsItem
{
	public:
		const static size_t DIAMETER = 25;
		enum class DataType { CT_NODE, RB_NODE };

		DataType m_dataType;

		void HighlightLowerSubTree();
		void HighlightUpperSubTree();
	protected:
		using QtLinkList = QSet<QtSuperarc*>;
		QVariant m_data;

		float m_initialPosX;

		QPen m_outlinePen;

		QBrush m_highlightedBrush;
		QBrush m_defaultBrush;
		QBrush m_selectedBrush;

		QPen m_blackPen;
		//QPen m_redPen;

		QPainterPath m_shape;
		QRectF m_boundingRect;

		bool m_isUnderMouse = false;

		QString m_text;

		float m_simulatedHeight;

		//SupernodeBase* m_supernode = nullptr;

		virtual QVariant itemChange(GraphicsItemChange change, const QVariant &value);
		virtual QRectF boundingRect() const;
		virtual void hoverEnterEvent(QGraphicsSceneHoverEvent* event);
		virtual void hoverLeaveEvent(QGraphicsSceneHoverEvent* event);
		virtual void hoverMoveEvent(QGraphicsSceneHoverEvent* event);

		void notifyLinksOfPositionChange();

		QtLinkList m_links;

		QString generateTooltip() const;
	public:
		void addLink(QtSuperarc* arc);
		void removeLink(QtSuperarc* arc);

		SupernodeBase::vec_3 GetPosition() const;

		QtLinkList getLinks() const { return m_links; }

		virtual void paint(QPainter *painter,
						   const QStyleOptionGraphicsItem *option,
						   QWidget *widget);
		QtSupernode(const QVariant& data, const float initialPosX, const DataType dataType);

		virtual ~QtSupernode();

		size_t getUpDegree() const;
		size_t getDownDegree() const;

		size_t getTotalUpwardNodes() const;
		size_t getTotalDownwardNodes() const;

		size_t getTotalUpwardArcs() const;
		size_t getTotalDownwardArcs() const;

		float getHeight() const;
		float getNormalisedHeight() const;
		size_t GetId() const;

		void SetHighlighted(const bool value);
		bool GetHighlighted() const;

		void SetSelected(const bool value);
		bool GetSelected() const;

		void SetHidden(const bool value);
		bool GetHidden() const;

		ContourTreeSupernode* toContourTreeSupernode() const
		{
			if (m_dataType == DataType::CT_NODE)
			{
				return toPtr<ContourTreeSupernode>(m_data);
			}
			else return nullptr;
		}

		ReebGraphSupernode* toReebGraphSupernode() const
		{
			if (m_dataType == DataType::RB_NODE)
			{
				return toPtr<ReebGraphSupernode>(m_data);
			}
			else return nullptr;
		}

		template <typename T>
		T* toGenericPointer() const
		{
			return toPtr<T>(m_data);
		}
};

#endif // QCONTOURTREENODE_H
