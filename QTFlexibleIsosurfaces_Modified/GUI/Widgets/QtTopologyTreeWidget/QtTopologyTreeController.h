#ifndef QTTOPOLOGYTREECONTROLLER_H
#define QTTOPOLOGYTREECONTROLLER_H

#include <QObject>
#include "GUI/Widgets/QtTopologyTreeWidget/QtTopologyTreeWidget.h"

#include <set>
#include <map>

/////////////////////////
///	Conversion tables ///
/////////////////////////
class ContourTreeSupernode;
class ContourTreeSuperarc;
class ReebGraphSupernode;
class ReebGraphSuperarc;

class ReebGraph;
class ContourTree;

class QtTopologyTreeWidget;

class QtTopologyTreeController : public QObject
{
		Q_OBJECT
	private:
		QtTopologyTreeWidget* m_contourTreeWidget = nullptr;
		QtTopologyTreeWidget* m_reebGraphWidget = nullptr;

		ReebGraph* m_reebGraph = nullptr;
		ContourTree* m_contourTree = nullptr;

		struct TreeConversion
		{
			std::map<ContourTreeSupernode*, std::set<ReebGraphSupernode*>> ct_to_rb_nodes;
			std::map<ContourTreeSuperarc*, std::set<ReebGraphSuperarc*>> ct_to_rb_arcs;

			std::map<ReebGraphSupernode*, std::set<ContourTreeSupernode*>> rb_to_ct_nodes;
			std::map<ReebGraphSuperarc*, std::set<ContourTreeSuperarc*>> rb_to_ct_arcs;

			void addNode(ContourTreeSupernode* ct,
						   ReebGraphSupernode* rb)
			{
				ct_to_rb_nodes[ct].insert(rb);
				rb_to_ct_nodes[rb].insert(ct);
			}

			void addArc(ContourTreeSuperarc * ct,
						   ReebGraphSuperarc * rb)
			{
				ct_to_rb_arcs[ct].insert(rb);
				rb_to_ct_arcs[rb].insert(ct);
			}

			std::set<ContourTreeSupernode*> lookupNode(ReebGraphSupernode* rb)
			{
				auto it = rb_to_ct_nodes.find(rb);
				if (it != rb_to_ct_nodes.cend())
					return it->second;
				else
					return {};
			}

			std::set<ReebGraphSupernode*> lookupNode(ContourTreeSupernode* ct)
			{
				auto it = ct_to_rb_nodes.find(ct);
				if(it != ct_to_rb_nodes.cend())
					return it->second;
				else
					return {};
			}

			std::set<ContourTreeSuperarc*> lookupArc(ReebGraphSuperarc* rb)
			{
				auto it = rb_to_ct_arcs.find(rb);
				if (it != rb_to_ct_arcs.cend())
					return it->second;
				else
					return {};
			}

			std::set<ReebGraphSuperarc*> lookupArc(ContourTreeSuperarc* ct)
			{
				auto it = ct_to_rb_arcs.find(ct);
				if(it != ct_to_rb_arcs.cend())
					return it->second;
				else
					return {};
			}
		} m_tree_conversion;

		void setupLookupTable();
		void setupSignalsAndSlots();
	public:
		explicit QtTopologyTreeController(QObject* parent = 0);

		enum class Property
		{
			HIGHLIGHT,
			SELECT,
			VISIBILITY
		};

		void setReebGraph(ReebGraph* reebGraph,
						  QtTopologyTreeWidget* reebGraphWidget);
		void setContourTree(ContourTree* contourTree,
							 QtTopologyTreeWidget* contourTreeWidget);

	public slots:
		void UpdateSuperarcs(const QtTopologyTreeWidget::QtSuperarcList& superarcs,
							 const Property property,
							 const bool value = true);
		void UpdateSupernodes(const QtTopologyTreeWidget::QtSupernodeList& supernodes,
							 const Property property,
							  const bool value = true);

	signals:
		void TreesUpdated();
};

#endif // QTTOPOLOGYTREECONTROLLER_H
