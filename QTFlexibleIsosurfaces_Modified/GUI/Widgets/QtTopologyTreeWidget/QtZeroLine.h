#ifndef QTZEROLINE_H
#define QTZEROLINE_H

#include <QGraphicsLineItem>

class QPainter;
class QStyleOptionGraphicsItem;
class QWidget;

class QtZeroLine  : public QGraphicsLineItem
{
	public:
		using T = float;

		virtual void paint(QPainter *painter,
						   const QStyleOptionGraphicsItem *option,
						   QWidget *widget);

		QtZeroLine(const T dataMin, T dataMax);
	private:
		T m_dataMin;
		T m_dataMax;
		T m_dataRange;
};

#endif // QTZEROLINE_H
