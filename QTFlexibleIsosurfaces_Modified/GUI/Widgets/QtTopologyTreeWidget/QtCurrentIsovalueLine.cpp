#include "QtCurrentIsovalueLine.h"
#include <cassert>
#include <QGraphicsScene>

QtCurrentIsovalueLine::QtCurrentIsovalueLine()
{
	m_normalisedIsovalue = 0.0;

	setPen(QPen(Qt::black));
	setZValue(-1);
}

void QtCurrentIsovalueLine::SetCurrentNormalisedIsovalue(const T value)
{
	assert(scene() != nullptr);
	assert((value >= 0.0) && (value <= 1.0));

	m_normalisedIsovalue = value;

	const auto width = scene()->width();
	const auto height = scene()->height();

	setLine(0, height - (m_normalisedIsovalue * height),
			width, height - (m_normalisedIsovalue * height));
}

void QtCurrentIsovalueLine::paint(QPainter *painter,
				  const QStyleOptionGraphicsItem *option,
				  QWidget *widget)
{
	QGraphicsLineItem::paint(painter, option, widget);
}
