#ifndef QTCURRENTISOVALUELINE_H
#define QTCURRENTISOVALUELINE_H

#include <QGraphicsLineItem>

class QPainter;
class QStyleOptionGraphicsItem;
class QWidget;

class QtCurrentIsovalueLine: public QGraphicsLineItem
{
	public:
		using T = float;

		virtual void paint(QPainter *painter,
						   const QStyleOptionGraphicsItem *option,
						   QWidget *widget);

		QtCurrentIsovalueLine();

		T GetCurrentNormalisedIsovalue() const { return m_normalisedIsovalue; }
		void SetCurrentNormalisedIsovalue(const T value);
	private:
		T m_normalisedIsovalue;
};

#endif // QTCURRENTISOVALUELINE_H
