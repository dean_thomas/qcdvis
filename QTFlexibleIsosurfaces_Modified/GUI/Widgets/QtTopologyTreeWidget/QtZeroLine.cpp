#include "QtZeroLine.h"
#include <cassert>
#include <QGraphicsScene>

QtZeroLine::QtZeroLine(const T dataMin, T dataMax)
	: m_dataMin{dataMin}, m_dataMax{dataMax}
{
	assert(dataMax >= dataMin);

	m_dataRange = dataMax - dataMin;

	setPen(QPen(Qt::red));
	setZValue(-1);
}

void QtZeroLine::paint(QPainter *painter,
				  const QStyleOptionGraphicsItem *option,
				  QWidget *widget)
{
	assert(scene() != nullptr);

	auto height = scene()->height();
	auto width = scene()->width();

	auto y = (height * (m_dataMax / m_dataRange));
	setLine(0, y, width, y);

	QGraphicsLineItem::paint(painter, option, widget);
}
