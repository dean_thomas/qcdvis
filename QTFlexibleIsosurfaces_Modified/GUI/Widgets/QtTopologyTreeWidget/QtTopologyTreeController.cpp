#include "QtTopologyTreeController.h"
#include <cassert>
#include <HeightField3D.h>
#include <Graphs/ContourTree.h>
#include <Graphs/ReebGraph.h>
#include "GUI/Widgets/QtTopologyTreeWidget/QtSuperarc.h"
#include <tuple>
#include <iostream>
#include <iterator>
#include <algorithm>

using namespace std;

//#ifndef __PRETTY_FUNCTION__
//#define __PRETTY_FUNCTION__ __FUNCSIG__
//#endif

QtTopologyTreeController::QtTopologyTreeController(QObject* parent)
	: QObject(parent)
{

}

void QtTopologyTreeController::setupSignalsAndSlots()
{
	assert((m_contourTree != nullptr) && (m_contourTreeWidget != nullptr));
	assert((m_reebGraph != nullptr) && (m_reebGraphWidget != nullptr));

	//	Highlighting
	//	Superarcs
	connect(m_contourTreeWidget, &QtTopologyTreeWidget::HighlightedSuperarcsChanged,
			[=](const QtTopologyTreeWidget::QtSuperarcList& superarcs)
	{
		//cout << "Event in Contour tree" << endl;

		UpdateSuperarcs(superarcs, QtTopologyTreeController::Property::HIGHLIGHT);

		emit(TreesUpdated());
	});
	connect(m_reebGraphWidget, &QtTopologyTreeWidget::HighlightedSuperarcsChanged,
			[=](const QtTopologyTreeWidget::QtSuperarcList& superarcs)
	{
		//cout << "Event in Reeb Graph" << endl;

		UpdateSuperarcs(superarcs, QtTopologyTreeController::Property::HIGHLIGHT);

		emit(TreesUpdated());
	});
	//	Highlighting
	//	Supernodes
	connect(m_contourTreeWidget, &QtTopologyTreeWidget::HighlightedSupernodesChanged,
			[=](const QtTopologyTreeWidget::QtSupernodeList& supernodes)
	{
		//cout << "Event in Contour tree" << endl;

		UpdateSupernodes(supernodes, QtTopologyTreeController::Property::HIGHLIGHT);

		emit(TreesUpdated());
	});
	connect(m_reebGraphWidget, &QtTopologyTreeWidget::HighlightedSupernodesChanged,
			[=](const QtTopologyTreeWidget::QtSupernodeList& supernodes)
	{
		//cout << "Event in Reeb Graph" << endl;

		UpdateSupernodes(supernodes, QtTopologyTreeController::Property::HIGHLIGHT);

		emit(TreesUpdated());
	});
	//	Selecting
	//	Superarcs
	connect(m_contourTreeWidget, &QtTopologyTreeWidget::SelectedSuperarcsChanged,
			[=](const QtTopologyTreeWidget::QtSuperarcList& superarcs)
	{
		//cout << "Event in Contour tree" << endl;

		UpdateSuperarcs(superarcs, QtTopologyTreeController::Property::SELECT);

		emit(TreesUpdated());
	});
	connect(m_reebGraphWidget, &QtTopologyTreeWidget::SelectedSuperarcsChanged,
			[=](const QtTopologyTreeWidget::QtSuperarcList& superarcs)
	{
		//cout << "Event in Reeb Graph" << endl;

		UpdateSuperarcs(superarcs, QtTopologyTreeController::Property::SELECT);

		emit(TreesUpdated());
	});
	//	Selecting
	//	Supernodes
	connect(m_contourTreeWidget, &QtTopologyTreeWidget::SelectedSupernodesChanged,
			[=](const QtTopologyTreeWidget::QtSupernodeList& supernodes)
	{
		//cout << "Event in Contour tree" << endl;

		UpdateSupernodes(supernodes, QtTopologyTreeController::Property::SELECT);

		emit(TreesUpdated());
	});
	connect(m_reebGraphWidget, &QtTopologyTreeWidget::SelectedSupernodesChanged,
			[=](const QtTopologyTreeWidget::QtSupernodeList& supernodes)
	{
		//cout << "Event in Reeb Graph" << endl;

		UpdateSupernodes(supernodes, QtTopologyTreeController::Property::SELECT);

		emit(TreesUpdated());
	});
	//	Hiding
	//	Superarcs
	connect(m_contourTreeWidget, &QtTopologyTreeWidget::HiddenSuperarcsChanged,
			[=](const QtTopologyTreeWidget::QtSuperarcList& superarcs)
	{
		//cout << "Event in Contour tree" << endl;

		UpdateSuperarcs(superarcs, QtTopologyTreeController::Property::VISIBILITY);

		emit(TreesUpdated());
	});
	connect(m_reebGraphWidget, &QtTopologyTreeWidget::HiddenSuperarcsChanged,
			[=](const QtTopologyTreeWidget::QtSuperarcList& superarcs)
	{
		//cout << "Event in Reeb Graph" << endl;

		UpdateSuperarcs(superarcs, QtTopologyTreeController::Property::VISIBILITY);

		emit(TreesUpdated());
	});
	//	Hiding
	//	Supernodes
	connect(m_contourTreeWidget, &QtTopologyTreeWidget::HiddenSupernodesChanged,
			[=](const QtTopologyTreeWidget::QtSupernodeList& supernodes)
	{
		//cout << "Event in Contour tree" << endl;

		UpdateSupernodes(supernodes, QtTopologyTreeController::Property::VISIBILITY);

		emit(TreesUpdated());
	});
	connect(m_reebGraphWidget, &QtTopologyTreeWidget::HiddenSupernodesChanged,
			[=](const QtTopologyTreeWidget::QtSupernodeList& supernodes)
	{
		//cout << "Event in Reeb Graph" << endl;

		UpdateSupernodes(supernodes, QtTopologyTreeController::Property::VISIBILITY);

		emit(TreesUpdated());
	});
}

void QtTopologyTreeController::setReebGraph(ReebGraph* reebGraph,
											QtTopologyTreeWidget* reebGraphWidget)
{
	m_reebGraph = reebGraph;
	m_reebGraphWidget = reebGraphWidget;

	if ((m_reebGraph != nullptr) && (m_contourTree != nullptr)
		&& (m_reebGraphWidget != nullptr) && (m_contourTreeWidget != nullptr))
	{
//		setupLookupTable();

		setupSignalsAndSlots();
	}
}

void QtTopologyTreeController::setContourTree(ContourTree* contourTree,
											  QtTopologyTreeWidget* contourTreeWidget)
{
	m_contourTree = contourTree;
	m_contourTreeWidget = contourTreeWidget;

	if ((m_reebGraph != nullptr) && (m_contourTree != nullptr)
		&& (m_reebGraphWidget != nullptr) && (m_contourTreeWidget != nullptr))
	{
//		setupLookupTable();

		setupSignalsAndSlots();
	}
}

void QtTopologyTreeController::setupLookupTable()
{
#ifndef DISABLE_DEBUG_OUTPUT
	auto print_debug_output = [](const set<ContourTreeSuperarc*>& ct_arcs,
							  const set<ContourTreeSupernode*>& ct_nodes)
	{
		cout << "Unmatched superarcs in contour tree is now: "
			 << ct_arcs.size() << "." << endl;
		cout << "Unmatched supernodes in contour tree is now: "
			 << ct_arcs.size() << "." << endl << endl;
	};
#endif

	//auto m_heightField3D = contourTreeWidget->
	assert((m_contourTree != nullptr) && (m_contourTreeWidget != nullptr));
	assert((m_reebGraph != nullptr) && (m_reebGraphWidget != nullptr));

	set<ContourTreeSuperarc*> unmatched_ct_arcs(m_contourTree->Superarcs_cbegin(),
												m_contourTree->Superarcs_cend());
	set<ContourTreeSupernode*> unmatched_ct_nodes(m_contourTree->Supernodes_cbegin(),
												  m_contourTree->Supernodes_cend());

	auto setup_one_to_one_arc_links = [&](set<ContourTreeSuperarc*>& unprocessed_arcs)
	{
		//	Setup links for Reeb Graph supernodes that appear directly in
		//	the Contour Tree (and vice-versa).
		for (auto& ct_arc : unprocessed_arcs)
		{
			//auto ct_arc = *it;
			assert(ct_arc != nullptr);

			auto ct_top_pos = ct_arc->GetTopSupernode()->GetPosition();
			auto ct_bottom_pos = ct_arc->GetBottomSupernode()->GetPosition();

			auto x0 = get<0>(ct_top_pos);
			auto y0 = get<1>(ct_top_pos);
			auto z0 = get<2>(ct_top_pos);

			auto x1 = get<0>(ct_bottom_pos);
			auto y1 = get<1>(ct_bottom_pos);
			auto z1 = get<2>(ct_bottom_pos);


			ReebGraphSuperarc* rb_arc = nullptr;
			if (rb_arc = m_reebGraph->ArcAt(x0, y0, z0, x1, y1, z1))
			{
				cout << "Added arc lookup for arc between ("
					 << x0 << ", " << y0 << ", " << z0 << ") and ("
					 << x1 << ", " << y1 << ", " << z1 << ")." << endl;
				m_tree_conversion.addArc(ct_arc,rb_arc);

				unprocessed_arcs.erase(ct_arc);
			}
			else if (rb_arc = m_reebGraph->ArcAt(x1, y1, z1, x0, y0, z0))
			{
				cout << "Added arc lookup for arc between ("
					 << x0 << ", " << y0 << ", " << z0 << ") and ("
					 << x1 << ", " << y1 << ", " << z1 << ")." << endl;
				m_tree_conversion.addArc(ct_arc,rb_arc);

				unprocessed_arcs.erase(ct_arc);
			}
		}
	};

	auto setup_one_to_one_node_links = [&](set<ContourTreeSupernode*>& unprocessed_nodes)
	{
		//	Setup links for Reeb Graph supernodes that appear directly in
		//	the Contour Tree (and vice-versa).
		for (auto& ct_node : unprocessed_nodes)
		{
			auto x = get<0>(ct_node->GetPosition());
			auto y = get<1>(ct_node->GetPosition());
			auto z = get<2>(ct_node->GetPosition());

			auto rb_node = m_reebGraph->NodeAt(x, y, z);

			if (rb_node != nullptr)
			{
				cout << "Added symbolic link between contour tree and "
					 <<	"reeb graph nodes at ("
					 << x << ", " << y << ", " << z << ")" << endl;

				m_tree_conversion.addNode(ct_node,rb_node);
				//m_linked_nodes[rb_node].insert(ct_node);

				unprocessed_nodes.erase(ct_node);
			}
		}
	};

	auto setup_upward_leaf_node_links =
			[&](set<ContourTreeSupernode*>& unprocessed_nodes,
			set<ContourTreeSuperarc*>& unprocessed_arcs)
	{
		//	First we'll filter the list to just the upper leaf nodes
		set<ContourTreeSupernode*> upper_leaf_nodes;
		copy_if(unprocessed_nodes.begin(),
				unprocessed_nodes.end(),
				inserter(upper_leaf_nodes, upper_leaf_nodes.begin()),
				[&](ContourTreeSupernode* node)
		{
			return node->IsUpperLeaf();
		});

		cout << "Upper supernodes: " << endl;
		for (auto& supernode : upper_leaf_nodes)
		{
			//	Get the list of down arcs from this node.  If we are a leaf,
			//	this should be the only node in the set.
			auto downward_arcs = supernode->GetDownwardArcs();
			assert(downward_arcs.size() == 1);
			auto downward_arc = *downward_arcs.cbegin();

			cout << *supernode << endl;
			cout << "\t" << *downward_arc << endl;

			//	Move down the arc to the next supernode
			auto next_downward_node = downward_arc->GetBottomSupernode();

			//	Get a list of upward nodes from this node
			set<ContourTreeSuperarc*> filtered_upward_arcs;
			auto upward_arcs = next_downward_node->GetUpwardArcs();
			copy_if(upward_arcs.cbegin(),
					upward_arcs.cend(),
					inserter(filtered_upward_arcs, filtered_upward_arcs.begin()),
					[&](ContourTreeSuperarc* superarc)
			{
				return superarc != downward_arc;
			});

			cout << "Supernode below this has " << filtered_upward_arcs.size()
				 << " up arcs (not including this one." << endl;
			for (auto& up_arc : filtered_upward_arcs)
			{
				cout << "\t" << *up_arc << endl;
			}

			//	Move back up the adjacent superarc
			//auto adjacent
			cout << endl;
		}
	};

	auto setup_downward_leaf_node_links =
			[&](set<ContourTreeSupernode*>& unprocessed_nodes,
			set<ContourTreeSuperarc*>& unprocessed_arcs)
	{
		//	First we'll filter the list to just the lower leaf nodes
		set<ContourTreeSupernode*> lower_leaf_nodes;
		copy_if(unprocessed_nodes.begin(),
				unprocessed_nodes.end(),
				inserter(lower_leaf_nodes, lower_leaf_nodes.begin()),
				[&](ContourTreeSupernode* node)
		{
			return node->IsLowerLeaf();
		});

		cout << "Lower supernodes: " << endl;
		for (auto& supernode : lower_leaf_nodes)
		{
			//	Get the list of uparcs from this node.  If we are a leaf, this
			//	should be the only node in the set.
			auto upward_arcs = supernode->GetUpwardArcs();
			assert(upward_arcs.size() == 1);
			auto upward_arc = *upward_arcs.cbegin();

			cout << *supernode << endl;
			cout << "\t" << *upward_arc << endl;

			//	Move up the arc to the next supernode
			auto next_upward_node = upward_arc->GetTopSupernode();

			//	Get a list of downward nodes from this node
			set<ContourTreeSuperarc*> filtered_downward_arcs;
			auto downward_arcs = next_upward_node->GetDownwardArcs();
			copy_if(downward_arcs.cbegin(),
					downward_arcs.cend(),
					inserter(filtered_downward_arcs, filtered_downward_arcs.begin()),
					[&](ContourTreeSuperarc* superarc)
			{
				return superarc != upward_arc;
			});

			cout << "Supernode above this has " << filtered_downward_arcs.size()
				 << " up arcs (not including this one." << endl;
			for (auto& down_arc : filtered_downward_arcs)
			{
				cout << "\t" << *down_arc << endl;
			}

			//	Move back up the adjacent superarc
			//auto adjacent
			cout << endl;
		}
	};
#ifndef DISABLE_DEBUG_OUTPUT
	cout << "Initialising lookup table." << endl;
	print_debug_output(unmatched_ct_arcs, unmatched_ct_nodes);

#endif

	setup_one_to_one_node_links(unmatched_ct_nodes);
	setup_one_to_one_arc_links(unmatched_ct_arcs);

#ifndef DISABLE_DEBUG_OUTPUT
	cout << "Matching 1-to-1 arcs." << endl;
	print_debug_output(unmatched_ct_arcs, unmatched_ct_nodes);
#endif

	setup_upward_leaf_node_links(unmatched_ct_nodes, unmatched_ct_arcs);
	setup_downward_leaf_node_links(unmatched_ct_nodes, unmatched_ct_arcs);

#ifndef DISABLE_DEBUG_OUTPUT
	cout << "Matching 1-to-1 arcs." << endl;
	print_debug_output(unmatched_ct_arcs, unmatched_ct_nodes);
#endif

}

void QtTopologyTreeController::UpdateSupernodes(const QtTopologyTreeWidget::QtSupernodeList& supernodes,
												const Property property, const bool value)
{
	assert(m_contourTree != nullptr);
	assert(m_reebGraph != nullptr);

	switch (property)
	{
		case Property::HIGHLIGHT:
			m_contourTree->ClearHighlightedNodes();
			m_reebGraph->ClearHighlightedNodes();
			break;
		case Property::SELECT:
			m_contourTree->ClearSelectedNodes();
			m_reebGraph->ClearSelectedNodes();
			break;
		case Property::VISIBILITY:
			m_contourTree->ClearHiddenNodes();
			m_reebGraph->ClearHiddenNodes();
			break;
	}


	for (auto& node : supernodes)
	{
		switch (property)
		{
			case Property::HIGHLIGHT:
				node->SetHighlighted(value);
				break;
			case Property::SELECT:
				node->SetSelected(value);
				break;
			case Property::VISIBILITY:
				node->SetHighlighted(value);
				break;
		}

		ContourTreeSupernode* ct_node = nullptr;
		ReebGraphSupernode* rb_node = nullptr;

		if(ct_node = node->toContourTreeSupernode())
		{
			cout << "Node is a contour tree node" << endl;
			assert(ct_node != nullptr);

			auto node_lookup = m_tree_conversion.lookupNode(ct_node);
			for (auto& linked_node : node_lookup)
			{
				switch (property)
				{
					case Property::HIGHLIGHT:
						linked_node->SetHighlighted(value);
						break;
					case Property::SELECT:
						linked_node->SetSelected(value);
						break;
					case Property::VISIBILITY:
						linked_node->SetHidden(value);
						break;
				}
			}
		}
		else if (rb_node = node->toReebGraphSupernode())
		{
			cout << "Node is a contour tree node" << endl;
			assert(rb_node != nullptr);

			auto node_lookup = m_tree_conversion.lookupNode(rb_node);
			for (auto& linked_node : node_lookup)
			{
				switch (property)
				{
					case Property::HIGHLIGHT:
						linked_node->SetHighlighted(value);
						break;
					case Property::SELECT:
						linked_node->SetSelected(value);
						break;
					case Property::VISIBILITY:
						linked_node->SetHidden(value);
						break;
				}
			}
		}
	}

	m_reebGraphWidget->update();
	m_contourTreeWidget->update();
}

void QtTopologyTreeController::UpdateSuperarcs(const QtTopologyTreeWidget::QtSuperarcList& superarcs,
											   const Property property, const bool value)
{
#ifndef DISABLE_DEBUG_OUTPUT
	cout << __PRETTY_FUNCTION__ << endl;
#endif
	assert(m_reebGraph != nullptr);
	assert(m_contourTree != nullptr);

	switch (property)
	{
		case Property::HIGHLIGHT:
			m_reebGraph->ClearHighlightedArcs();
			m_contourTree->ClearHighlightedArcs();
			break;
		case Property::SELECT:
			m_reebGraph->ClearSelectedArcs();
			m_contourTree->ClearSelectedArcs();
			break;
		case Property::VISIBILITY:
			m_reebGraph->ClearHiddenNodes();
			m_contourTree->ClearHiddenArcs();
			break;
	}
	for (auto& arc : superarcs)
	{
		switch (property)
		{
			case Property::HIGHLIGHT:
				arc->SetHighlighted(value);
				break;
			case Property::SELECT:
				arc->SetSelected(value);
				break;
			case Property::VISIBILITY:
				arc->SetHidden(value);
				break;
		}

		ContourTreeSuperarc* ct_arc = nullptr;
		ReebGraphSuperarc* rb_arc = nullptr;

		if(ct_arc = arc->toContourTreeSuperarc())
		{
			cout << "Arc is a contour tree arc" << endl;
			assert(ct_arc != nullptr);

			auto arc_lookup = m_tree_conversion.lookupArc(ct_arc);
			for (auto& linked_arc : arc_lookup)
			{
				switch (property)
				{
					case Property::HIGHLIGHT:
						linked_arc->SetHighlighted(value);
						break;
					case Property::SELECT:
						linked_arc->SetSelected(value);
						break;
					case Property::VISIBILITY:
						linked_arc->SetHidden(value);
						break;
				}
			}
		}
		else if (rb_arc = arc->toReebGraphSuperarc())
		{
			cout << "Arc is a reeb graph arc" << endl;
			assert(rb_arc != nullptr);

			auto arc_lookup = m_tree_conversion.lookupArc(rb_arc);
			for (auto& linked_arc : arc_lookup)
			{
				switch (property)
				{
					case Property::HIGHLIGHT:
						linked_arc->SetHighlighted(value);
						break;
					case Property::SELECT:
						linked_arc->SetSelected(value);
						break;
					case Property::VISIBILITY:
						linked_arc->SetHidden(value);
						break;
				}
			}
		}
	}

	//ui->widgetSurfaceRender->update();
	m_contourTreeWidget->update();
	m_reebGraphWidget->update();
}
