#include "QtSuperarc.h"
#include "QtSupernode.h"
#include "Graphs/ReebGraphSuperarc.h"
#include "Graphs/ContourTreeSuperarc.h"
#include "Graphs/SuperarcBase.h"
#include <QDebug>
#include <QStyleOptionGraphicsItem>

float QtSuperarc::GetLength() const
{
	auto arcBase = toPtr<SuperarcBase<SupernodeBase>>(m_data);
	assert(arcBase != nullptr);
	return arcBase->GetArcLength();
}

size_t QtSuperarc::GetId() const
{
	auto arcBase = toPtr<SuperarcBase<SupernodeBase>>(m_data);
	assert(arcBase != nullptr);
	return arcBase->GetId();
}

void QtSuperarc::SetHighlighted(const bool value)
{
	auto arcBase = toPtr<SuperarcBase<SupernodeBase>>(m_data);
	assert(arcBase != nullptr);
	arcBase->SetHighlighted(value);
}

bool QtSuperarc::GetHighlighted() const
{
	auto arcBase = toPtr<SuperarcBase<SupernodeBase>>(m_data);
	assert(arcBase != nullptr);
	return arcBase->IsHighlighted();
}

void QtSuperarc::SetSelected(const bool value)
{
	auto arcBase = toPtr<SuperarcBase<SupernodeBase>>(m_data);
	assert(arcBase != nullptr);
	arcBase->SetSelected(value);
}

bool QtSuperarc::GetSelected() const
{
	auto arcBase = toPtr<SuperarcBase<SupernodeBase>>(m_data);
	assert(arcBase != nullptr);
	return arcBase->IsSelected();
}

void QtSuperarc::SetHidden(const bool value)
{
	auto arcBase = toPtr<SuperarcBase<SupernodeBase>>(m_data);
	assert(arcBase != nullptr);
	arcBase->SetHidden(value);
}

bool QtSuperarc::GetHidden() const
{
	auto arcBase = toPtr<SuperarcBase<SupernodeBase>>(m_data);
	assert(arcBase != nullptr);
	return arcBase->IsHidden();
}

///
/// \brief QtSuperarc::QtSuperarc
/// \param sourceNode
/// \param targetNode
///	\since		09-02-16
///	\author		Dean
///
QtSuperarc::QtSuperarc(const QVariant& data,
					   QtSupernode* const sourceNode,
					   QtSupernode* const targetNode,
					   const DataType dataType)
	: m_data{data}, m_dataType{dataType}
{
	m_sourceNode = sourceNode;
	m_targetNode = targetNode;

	m_sourceNode->addLink(this);
	m_targetNode->addLink(this);

	m_selectedPen = QPen(Qt::green);
	m_selectedPen.setColor(QCOLOR_CB_SAFE_BLUISH_GREEN);
	m_selectedPen.setWidth(3);

	m_normalPen = QPen(Qt::black);
	m_highlightedPen = QPen(Qt::red);
	m_highlightedPen.setColor(QCOLOR_CB_SAFE_VERMILLION);
	m_highlightedPen.setWidth(3);

	setFlag(QGraphicsItem::ItemIsSelectable);
	setAcceptHoverEvents(true);
	setZValue(-1);

	trackNodes();

	setToolTip(generateTooltip());
}

QString QtSuperarc::generateTooltip() const
{
	const string ENDL = "<br/>";
	stringstream tooltip;
	tooltip << "<b>superarc</b>" << ENDL
			<< "id: " << GetId() << ENDL << ENDL
			<< "top height: " << GetTopSupernode()->getHeight() << ENDL
			<< "bottom height: " << GetBottomSupernode()->getHeight() << ENDL
			<< "length: " << GetLength() << ENDL;

	return QString::fromStdString(tooltip.str());
}

///
/// \brief		Changes the current orientation to match the end points
///	\since		09-02-16
///	\author		Dean
///
void QtSuperarc::trackNodes()
{
	auto sourcePos = m_sourceNode->scenePos();
	auto targetPos = m_targetNode->scenePos();

	//qDebug() << "Source at: " << sourcePos;
	//qDebug() << "Target at: " << targetPos;

	setLine(QLineF(sourcePos, targetPos));
}

///
/// \brief QtSuperarc::paint
/// \param painter
/// \param option
/// \param widget
///	\since		09-02-16
///	\author		Dean
///
void QtSuperarc::paint(QPainter *painter,
					   const QStyleOptionGraphicsItem *option,
					   QWidget *widget)
{
	auto arcBase = toPtr<SuperarcBase<SupernodeBase>>(m_data);
	assert(arcBase != nullptr);

	QPen currentPen;

	if (arcBase->IsHighlighted())
	{
		currentPen = m_highlightedPen;
	}
	else
	{
		if (arcBase->IsSelected())
		{
			currentPen = m_selectedPen;
		}
		else
		{
			currentPen = m_normalPen;
		}
	}

	//	If the superarc is hidden we will 'dash' the line
	if (arcBase->IsHidden()) currentPen.setStyle(Qt::DashLine);

	//	Set the pen and do the draw
	setPen(currentPen);

	//	We don't want to render the dashed outline as we have our own method
	//	of showing selection.  So de-select the option befor calling the
	//	base paint method...
	QStyleOptionGraphicsItem* optionModified = const_cast<QStyleOptionGraphicsItem*>(option);
	optionModified->state &= ~QStyle::State_Selected;
	QGraphicsLineItem::paint(painter,optionModified,widget);
}

///
/// \brief QtSuperarc::~QtSuperarc
///	\since		09-02-16
///	\author		Dean
///
QtSuperarc::~QtSuperarc()
{
	m_sourceNode->removeLink(this);
	m_targetNode->removeLink(this);
}
