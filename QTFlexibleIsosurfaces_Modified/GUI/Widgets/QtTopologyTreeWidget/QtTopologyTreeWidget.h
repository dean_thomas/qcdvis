#ifndef QTTOPOLOGYTREEWIDGET_H
#define QTTOPOLOGYTREEWIDGET_H

#include <QWidget>
#include <QGraphicsView>

#include <map>
#include <memory>
#include <iterator>
#include <vector>

#include "Graphs/ContourTree.h"
#include "Graphs/ContourTreeSuperarc.h"
#include "Graphs/SupernodeBase.h"
#include "Graphs/SuperarcBase.h"

#include <QGraphicsScene>
#include "QtSupernode.h"
#include <tuple>
namespace Ui {
	class QtTopologyTreeWidget;
}

class QtCurrentIsovalueLine;

class QtTopologyTreeWidget : public QGraphicsView
{
		Q_OBJECT

	public:
		//	Alises that point to the basic implementation in the tree data
		//	structures
		using superarc_base_type = SuperarcBase<SupernodeBase>;
		using supernode_base_type = SupernodeBase;
		using vec_3 = std::tuple<size_t, size_t, size_t>;

		using QtSuperarcList = std::vector<QtSuperarc*>;
		using QtSupernodeList = std::vector<QtSupernode*>;
		using BaseSuperarcList = std::vector<superarc_base_type*>;
		using BaseSupernodeList = std::vector<supernode_base_type*>;

	private:
		struct SelectionList
		{
				BaseSupernodeList selectedBaseNodes;
				QtSupernodeList selectedQtNodes;

				BaseSuperarcList selectedBaseArcs;
				QtSuperarcList selectedQtArcs;

				bool empty() const
				{
					return selectedBaseNodes.empty() && selectedBaseArcs.empty();
				}
		};

		using T = float;

		//	Used to store the x, y, z extents of the heightfield;
		//	used for normalising the x position of nodes in the tree
		vec_3 m_domain_limits;

		SelectionList getListsOfSelectedItems() const;

		const float SCALE_MIN = 0.1f;
		const float SCALE_MAX = 10.0f;
		const float SCALE_DELTA = 0.05f;

		//	Store a map between the input nodes and the
		//	visual representation (because the key is a polymorphic type a
		//	custom comparator is required for ordering
		using NodeMapFromBase = std::map<SupernodeBase*, QtSupernode*>;
		using NodeMapToBase = std::map<QtSupernode*, SupernodeBase*>;

//		using SuperarcType = SuperarcBase<SupernodeBase*>;

		//	Store a map between the input arcs and the
		//	visual representation (because the key is a polymorphic type a
		//	custom comparator is required for ordering
		//using ArcMapFromBase = std::map<SuperarcType*, QtSuperarc*, SuperarcBase_less<SupernodeBase*>>;
//		using ArcMapToBase = std::map<QtSuperarc*, SuperarcType*>;
	public:
		using QGraphicsScene_uPtr = std::unique_ptr<QGraphicsScene>;
		explicit QtTopologyTreeWidget(QWidget *parent = 0);

		void SaveToPdf(const QString& filename) const;


		~QtTopologyTreeWidget();
		virtual void paintEvent(QPaintEvent* event) override;

		void zoomIn();
		void zoomOut();

		void RedrawEverything() const;

		vec_3 GetExtents() const { return m_domain_limits; }
		void SetExtents(const size_t x_lim,
						const size_t y_lim,
						const size_t z_lim);

		void DoAutoScale();

		void DisplayContourTree(ContourTree* const contourTree);
		void DisplayReebGraph(ReebGraph* const reebGraph);

		void SetCurrentNormalisedIsovalue(const T value);
		T GetCurrentNormalisedIsovalue() const;

		virtual void resizeEvent(QResizeEvent* event);

		void SetAutoScale(const bool& value);
		bool GetAutoScale() const { return m_autoScale; }

		std::vector<QtSuperarc*> getListOfSuperarcs() const;
		std::vector<QtSupernode*> getListOfSupernodes() const;

		void SetCurrentIsovalueLineVisible(const bool value);
	private slots:
		//	Internal handler that translates the selected model items to the
		//	data components they wrap.
		void selectionChangedHandler();

	signals:
		//	Signals for responding to changes in other UI components
		void SelectedSupernodesChanged(QtSupernodeList) const;
		void SelectedSuperarcsChanged(QtSuperarcList) const;

		void HighlightedSupernodesChanged(QtSupernodeList) const;
		void HighlightedSuperarcsChanged(QtSuperarcList) const;

		//	Signals emitted when the user interacts with a node/arc via it's
		//	graphical representation
		//	TODO: fix this - always reset when handled by controller
		void HiddenSuperarcsChanged(QtSuperarcList) const;
		void HiddenSupernodesChanged(QtSupernodeList) const;

		void RequestNestedContoursForObject(ContourTreeSuperarc*) const;
	private:

		void clearHighlightedArcs();
		void clearHighlightedNodes();

		void generic_popup(const QPoint& pos);

		void groupPopupMenu(const QPoint& pos, const SelectionList &selected_items);
		void superarcPopupMenu(const QPoint& pos, QtSuperarc* const superarc);
		void supernodePopupMenu(const QPoint& pos, QtSupernode* const supernode);

		void scaleBy(const float factor);
		float m_scale = 1.0f;
		bool m_autoScale = true;

		Ui::QtTopologyTreeWidget *ui;

		QGraphicsScene_uPtr m_graphicsScene;

		QtCurrentIsovalueLine* m_currentIsovalueLine = nullptr;



		QtSupernode* m_previouslyHighlightedSupernode = nullptr;
		QtSuperarc* m_previouslyHighlightedSuperarc = nullptr;

//		ArcMapFromBase m_arcsFromBase;
//		ArcMapToBase m_arcsToBase;
	protected:
		void wheelEvent(QWheelEvent* event) override;
		void mouseMoveEvent(QMouseEvent* event) override;
};

#endif // QTTOPOLOGYTREEWIDGET_H
