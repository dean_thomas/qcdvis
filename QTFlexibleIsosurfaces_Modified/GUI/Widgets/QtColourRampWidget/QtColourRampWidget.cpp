#include "QtColourRampWidget.h"
#include <QMetaType>
#include <QColorDialog>

using std::ifstream;
using std::vector;
using std::string;
using std::stringstream;
using Colour::Integer::RGB;
using Colour::Integer::Byte;
using std::cout;
using std::endl;

#ifdef _MSC_VER
#define __PRETTY_FUNCTION__ __FUNCSIG__
#endif


///
/// \brief		Returns the entire colour table
/// \return
/// \since		30-03-2016
/// \author		Dean
///
QtColourRampWidget::ColourTable QtColourRampWidget::GetColourTable() const
{
	return m_colourTable;
}

///
/// \brief		Gets a truncated version of the colour table between two values
/// \param		min: the normalised minimum of the truncated table (0..max)
/// \param		max: the normalised maximum of the truncated table (max..1)
/// \return
/// \since		30-03-2016
/// \author		Dean
///
QtColourRampWidget::ColourTable QtColourRampWidget::GetColourTable(const float normalisedMin,
																   const float normalisedMax) const
{
	//	Both numbers should be pre-normalised into the 0..1 range
	assert(0.0 <= normalisedMin && normalisedMin <= 1.0);
	assert(0.0 <= normalisedMax && normalisedMax <= 1.0);
	assert(normalisedMin <= normalisedMax);

	ColourTable result;
	result.reserve(MAX_COLOURS);

	//	Work out the size of each entry
	auto range = normalisedMax - normalisedMin;
	auto step = range / (float)MAX_COLOURS;

	//	Loop through the current table in the required range
	for (auto f = normalisedMin; f < normalisedMax; f += step)
	{
		result.push_back(GetColorFromValue(f));
	}
	return result;
}


///
/// \brief		Provide a method for serializing the DisplayMode enum class
///				(currently not used)
/// \param out
/// \param val
/// \return
/// \since		07-12-2015
/// \author		Dean
///
QDataStream& operator<<(QDataStream& out, const QtColourRampWidget::DisplayMode& val)
{
	//	Get the underlying type of the enum (int, uint etc)
	using EnumType = std::underlying_type<QtColourRampWidget::DisplayMode>::type;

	//	Cast to the underlying type
	out << static_cast<EnumType>(val);

	return out;
}

///
/// \brief		Provide a method for serializing the DisplayMode enum class
///				(currently not used)
/// \param in
/// \param val
/// \return
/// \since		07-12-2015
/// \author		Dean
///
QDataStream& operator>>(QDataStream& in, QtColourRampWidget::DisplayMode& val)
{
	//	Get the underlying type of the enum (int, uint etc)
	using EnumType = std::underlying_type<QtColourRampWidget::DisplayMode>::type;
	EnumType temp;

	//	Read the underlying value into a temp variable
	in >> temp;

	//	Cast to the enum type
	val = static_cast<QtColourRampWidget::DisplayMode>(temp);

	return in;
}

///
/// \brief		Provide a method for serializing the QtColourRampWidget enum
///				class (currently not used)
/// \param out
/// \param val
/// \return
/// \since		07-12-2015
/// \author		Dean
///
QDataStream& operator<<(QDataStream& out, const QtColourRampWidget& widget)
{
	out << widget.m_isHorizontal
		   //	Border
		<< widget.m_hasBorder
		<< widget.m_borderColour
		   //	Opacity value
		<< widget.m_opacity
		   //	Current value line
		<< widget.m_currentIsovalueLineColor
		<< widget.m_currentIsovalueLineThickness
		<< widget.m_hasCurrentValueLine
		   //	Zero line
		<< widget.m_zeroLineColour
		<< widget.m_zeroLineThickness
		<< widget.m_hasZeroLine
		   //	Colour table
		<< widget.m_colourTable
		   //	Universal colour
		<< widget.m_universalColour
		   //	Dual colours
		<< widget.m_dualColours.positive
		<< widget.m_dualColours.negative
		   //	Display mode
		<< widget.m_displayMode;

	return out;
}

///
/// \brief		Provide a method for serializing the QtColourRampWidget enum
///				class (currently not used)
/// \param	in
/// \param val
/// \return
/// \since		07-12-2015
/// \author		Dean
///
QDataStream& operator>>(QDataStream& in, QtColourRampWidget& widget)
{
	in >> widget.m_isHorizontal
		   //	Border
		>> widget.m_hasBorder
		>> widget.m_borderColour
		   //	Opacity value
		>> widget.m_opacity
		   //	Current value line
		>> widget.m_currentIsovalueLineColor
		>> widget.m_currentIsovalueLineThickness
		>> widget.m_hasCurrentValueLine
		   //	Zero line
		>> widget.m_zeroLineColour
		>> widget.m_zeroLineThickness
		>> widget.m_hasZeroLine
		   //	Colour table
		>> widget.m_colourTable
		   //	Universal colour
		>> widget.m_universalColour
		   //	Dual colours
		>> widget.m_dualColours.positive
		>> widget.m_dualColours.negative
		   //	Display mode
		>> widget.m_displayMode;

	return in;
}

///
/// \brief QtColourRampWidget::SetColourTable
/// \param value
/// \since		11-12-2015
/// \author		Dean
///
void QtColourRampWidget::SetColourTable(const ColourTable& value)
{
	m_colourTable = value;

	update();

	emit OnColourTableChanged(value);
}


///
/// \brief QtColourRampWidget::SetNegativeColour
/// \param colour
///	\since		07-12-2015
/// \author		Dean
///
void QtColourRampWidget::SetNegativeColour(const QColor& value)
{
	m_dualColours.negative = value;

	update();

	//	Trigger an external event
	emit OnNegativeColourChanged(m_dualColours.negative);
}

///
/// \brief QtColourRampWidget::SetPositiveColour
/// \param colour
///	\since		07-12-2015
/// \author		Dean
///
void QtColourRampWidget::SetPositiveColour(const QColor& value)
{
	m_dualColours.positive = value;

	update();

	//	Trigger an external event
	emit OnPositiveColourChanged(m_dualColours.positive);
}

///
/// \brief		Draws a line across the histograms at the current isovalue
/// \param painter
/// \since		26-11-2015
/// \author		Dean
/// \author		Dean: add the ability to switch orientation [01-09-2015]
///
void QtColourRampWidget::drawCurrentValueLine(QPainter &painter)
{
	QPen currentIsoPen(m_currentIsovalueLineColor, m_currentIsovalueLineThickness);
	painter.setPen(currentIsoPen);

	if (m_isHorizontal)
	{
		//	Origin at left of widget
		QPoint topPointCurrent(width() * m_currentIsovalue, 0);
		QPoint bottomPointCurrent(width() * m_currentIsovalue, height());
		painter.drawLine(topPointCurrent, bottomPointCurrent);
	}
	else
	{
		//	Origin at bottom of widget
		QPoint leftPointCurrent(0, height() - (height() * m_currentIsovalue));
		QPoint rightPointCurrent(width(), height() - (height() * m_currentIsovalue));
		painter.drawLine(leftPointCurrent, rightPointCurrent);
	}
}

///
/// \brief QtColourRampWidget::SetCurrentNormalizedIsovalue
/// \param value
/// \since		26-11-2015
/// \author		Dean
///
void QtColourRampWidget::SetCurrentNormalizedIsovalue(const float& value)
{
	m_currentIsovalue = value;

	update();
}

///
/// \brief QtColourRampWidget::SetIsovalueRange
/// \param min
/// \param max
/// \since		26-11-2015
/// \author		Dean
///
void QtColourRampWidget::SetIsovalueRange(const float& min, const float& max)
{
	assert((max > min) || ((max == 0.0) && (min == 0.0)));

	m_minimumValue = min;
	m_maximumValue = max;

	update();
}

///
/// \brief		Draws a line across the histograms at the current 'zero' point
/// \param painter
/// \since		26-11-2015
/// \author		Dean
/// \author		Dean: add the ability to switch orientation [01-09-2015]
///
void QtColourRampWidget::drawZeroLine(QPainter &painter)
{
	if ((m_minimumValue < 0.0)
		&& (m_maximumValue > 0.0))
	{
		float range = m_maximumValue - m_minimumValue;
		float offset = m_maximumValue / range;

		QPen zeroPen(m_zeroLineColour, m_zeroLineThickness);
		painter.setPen(zeroPen);

		if (m_isHorizontal)
		{
			//	Note: as we are using the maximum, we don't need to reverse orintation
			QPoint topPoint(width() * offset, 0);
			QPoint bottomPoint(width() * offset, height());
			painter.drawLine(topPoint, bottomPoint);
		}
		else
		{
			//	Note: as we are using the maximum, we don't need to reverse orintation
			QPoint leftPoint(0, height() * offset);
			QPoint rightPoint(width(), height() * offset);
			painter.drawLine(leftPoint, rightPoint);

			//cout << __PRETTY_FUNCTION__ << endl;
			//cout << "Range: " << m_minimumValue;
			//cout << "..." << m_maximumValue;
			//cout << endl;
			//cout << "Zero line at: " << offset << endl;
		}
	}
}

///
/// \brief QtColourRampWidget::drawBorder
/// \param painter
/// \since		26-11-2015
/// \author		Dean
///
void QtColourRampWidget::drawBorder(QPainter& painter)
{
	painter.setPen(m_borderColour);
	painter.drawRect(0,0, width()-1, height()-1);
}

///
/// \brief QtColourRampWidget::SetSolidColour
/// \param value
/// \since		26-11-2015
/// \author		Dean
///
void QtColourRampWidget::SetUniversalColour(const QColor& value)
{
	m_universalColour = value;

	update();

	emit OnUniversalColourChanged(value);
}

///
/// \brief QtColourRampWidget::SetSecondaryColour
/// \param value
/// \since		26-11-2015
/// \author		Dean
///
void QtColourRampWidget::SetDualColours(const QColorPair& value)
{
	m_dualColours = value;

	update();

	//	Trigger an external event
	emit OnPositiveColourChanged(m_dualColours.positive);
	emit OnNegativeColourChanged(m_dualColours.negative);
}

///
/// \brief QtColourRampWidget::drawSingleColour
/// \since		26-11-2015
/// \author		Dean
///
void  QtColourRampWidget::drawSingleColour(QPainter& painter)
{
	painter.fillRect(0, 0, width(), height(), m_universalColour);
}

///
/// \brief QtColourRampWidget::drawDualColour
/// \since		26-11-2015
/// \author		Dean
///
void  QtColourRampWidget::drawDualColour(QPainter& painter)
{
	float range = m_maximumValue - m_minimumValue;

	float offset = 0.5;
	if (range != 0.0)
	{
		offset = m_maximumValue / range;
	}

	if (m_isHorizontal)
	{
		//	x < 0
		painter.fillRect(0, 0,
						 width() * offset, height(),
						 m_dualColours.negative);

		//	x > 0
		painter.fillRect(height() * offset, 0,
						 width() - (width() * offset), height(),
						 m_dualColours.positive);
	}
	else
	{
		//	x < 0
		painter.fillRect(0, height() * offset,
						 width(), height() - (height() * offset),
						 m_dualColours.negative);
		//	x > 0
		painter.fillRect(0, 0,
						 width(), height() * offset,
						 m_dualColours.positive);
	}
}

///
/// \brief QtColourRampWidget::drawColourRamp
/// \since		26-11-2015
/// \author		Dean
///
void QtColourRampWidget::drawColourRamp(QPainter& painter)
{
	for (unsigned int i = 0; i < MAX_COLOURS; ++i)
	{
#ifdef VERBOSE_OUTPUT
		printf("Colour index: %i; r=%i g=%i b=%i.\n", i,
			   m_colours[i].r, m_colours[i].g, m_colours[i].b);
		fflush(stdout);
#endif
		//	Draw the box in the required colour
		QColor colour = m_colourTable[i];

		if (m_isHorizontal)
		{
			auto segmentWidth = (float)width() / (float)MAX_COLOURS;
			auto segmentHeight = (float)height();

			painter.fillRect(i * segmentWidth, 0,
							 segmentWidth+1, segmentHeight,
							 colour);
		}
		else
		{
			auto segmentWidth = (float)width();
			auto segmentHeight = (float)height() / (float)MAX_COLOURS;

			QRectF rect(0.0,
						(float)height() - ((float)i * segmentHeight),
						segmentWidth,
						segmentHeight);

			painter.fillRect(rect, QBrush(colour));
		}
	}
}



///
/// \brief QtColourRamp::QColorToString
/// \param colour
/// \return
/// \since
/// \author		Dean
///
string QtColourRampWidget::QColorToString(const QColor& colour) const
{
	stringstream result;

	result << "r = " << colour.red() << " ";
	result << "g = " << colour.green() << " ";
	result << "b = " << colour.blue() << " ";
	result << "a = " << colour.alpha() << " ";

	return result.str();
}

///
/// \brief QtColourRamp::ToString
/// \return
///	\since		28-09-2015
/// \author		Dean
///
string QtColourRampWidget::ToString() const
{
	stringstream result;

	for (auto it = m_colourTable.cbegin(); it != m_colourTable.cend(); ++it)
	{
		result << QColorToString(*it) << endl;
	}
	return result.str();
}

///
/// \brief		Maps a number [0..1] to a QColor
/// \param		value
/// \return
/// \since		25-09-2015
/// \author		Dean
///
QColor QtColourRampWidget::GetColorFromValue(const float &value) const
{
	assert ((value >= 0.0) && (value <= 1.0));

	//	Convert to integer
	Byte lookup = 255 * value;

	return m_colourTable[lookup];
}

///
/// \brief		Maps a number [0..255] to a QColor
/// \param value
/// \return
/// \since		25-09-2015
/// \author		Dean
///
QColor QtColourRampWidget::GetColorFromValue(const unsigned char &value) const
{
	return m_colourTable[value];
}

///
/// \brief		Reverses the sequence of colours used in the colour ramp
/// \since		25-09-2015
/// \author		Dean
/// \author		Dean: update to use new 'SetColourTable' method, which
///				will in turn raise an event [11-12-2015]
///
void QtColourRampWidget::ReverseColourRamp()
{
	auto reversed = m_colourTable;
	std::reverse(reversed.begin(), reversed.end());

	cout << ToString() << endl;

	SetColourTable(reversed);
}

QtColourRampWidget::~QtColourRampWidget()
{
	cout << __PRETTY_FUNCTION__ << endl;

	disconnect();
}

///
/// \brief QtColourRampWidget::SetDisplayMode
/// \param value
/// \since		09-12-2015
/// \author		Dean
///
void QtColourRampWidget::SetDisplayMode(const DisplayMode &value)
{
	m_displayMode = value;

	update();

	emit OnDisplayModeChanged(value);
}

///
/// \brief QtColourRampWidget::ShowUniversalColourSelectionDialog
/// \since		09-12-2015
/// \author		Dean
///
void QtColourRampWidget::ShowUniversalColourSelectionDialog()
{
	QColorDialog colourDialog;
	colourDialog.setCurrentColor(m_universalColour);

	if (colourDialog.exec() == QDialog::Accepted)
	{
		//	Set the new colour, will also trigger a repaint
		SetUniversalColour(colourDialog.selectedColor());

		//	Update the display mode, will also trigger a repaint
		SetDisplayMode(DisplayMode::UNIVERSAL_COLOUR);
	}
}

///
/// \brief QtColourRampWidget::ShowUniversalColourSelectionDialog
/// \since		09-12-2015
/// \author		Dean
///
void QtColourRampWidget::ShowNegativeColourSelectionDialog()
{
	QColorDialog colourDialog;
	colourDialog.setCurrentColor(m_dualColours.negative);

	if (colourDialog.exec() == QDialog::Accepted)
	{
		//	Set the new colour, will also trigger a repaint
		SetNegativeColour(colourDialog.selectedColor());

		//	Update the display mode, will also trigger a repaint
		SetDisplayMode(DisplayMode::DUAL_COLOUR);
	}
}

///
/// \brief QtColourRampWidget::ShowUniversalColourSelectionDialog
/// \since		09-12-2015
/// \author		Dean
///
void QtColourRampWidget::ShowPositiveColourSelectionDialog()
{
	QColorDialog colourDialog;
	colourDialog.setCurrentColor(m_dualColours.positive);

	if (colourDialog.exec() == QDialog::Accepted)
	{
		//	Set the new colour, will also trigger a repaint
		SetPositiveColour(colourDialog.selectedColor());

		//	Update the display mode, will also trigger a repaint
		SetDisplayMode(DisplayMode::DUAL_COLOUR);
	}
}


void QtColourRampWidget::generatePopupMenu()
{
	//	Reverse the colour ramp
	QAction* reverseAction = new QAction("Reverse ramp", this);
	connect(reverseAction, SIGNAL(triggered()),
					 this, SLOT(ReverseColourRamp()));
	addAction(reverseAction);

	//	Load a colour ramp file
	QAction* loadAction = new QAction("Load colour ramp", this);
	connect(loadAction, SIGNAL(triggered()),
					 this, SLOT(ShowColourRampLoadDialog()));
	addAction(loadAction);

	QAction* sep1 = new QAction(this);
	sep1->setSeparator(true);
	addAction(sep1);

	QAction* actionChoosePos = new QAction("Choose positive colour...", this);
	addAction(actionChoosePos);
	connect(actionChoosePos, SIGNAL(triggered(bool)),
			this, SLOT(ShowPositiveColourSelectionDialog()));

	QAction* actionChooseNeg = new QAction("Choose negative colour...", this);
	addAction(actionChooseNeg);
	connect(actionChooseNeg, SIGNAL(triggered(bool)),
			this, SLOT(ShowNegativeColourSelectionDialog()));

	QAction* actionSetDual = new QAction("Set dual colours", this);
	addAction(actionSetDual);
	connect(actionSetDual, &QAction::triggered,
			[=]()
	{
		SetDisplayMode(DisplayMode::DUAL_COLOUR);
	});

	QAction* sep2 = new QAction(this);
	sep2->setSeparator(true);
	addAction(sep2);

	QAction* actionChooseUnivesal = new QAction("Choose universal colour...", this);
	addAction(actionChooseUnivesal);
	connect(actionChooseUnivesal, SIGNAL(triggered(bool)),
			this, SLOT(ShowUniversalColourSelectionDialog()));

	QAction* actionSetUniversal = new QAction("Set universal colour", this);
	addAction(actionSetUniversal);
	connect(actionSetUniversal, &QAction::triggered,
			[=]()
	{
		SetDisplayMode(DisplayMode::UNIVERSAL_COLOUR);
	});

	setContextMenuPolicy(Qt::ActionsContextMenu);
}

void QtColourRampWidget::GenerateDefaultColourRamp()
{
	//	Create a greyscale ramp
	for (unsigned int i = 0; i < MAX_COLOURS; ++i)
	{
		m_colourTable[i] = QColor(i, i, i);
	}

	cout << ToString() << endl;
}

vector<string> &QtColourRampWidget::split(const string &s, char delim,
										  vector<string> &elems)
{
	stringstream ss(s);
	string item;

	while (getline(ss, item, delim))
	{
		elems.push_back(item);
	}
	return elems;
}

vector<string> QtColourRampWidget::split(const string &s, char delim)
{
	vector<string> elems;
	return split(s, delim, elems);
}

///
/// \brief		Reads a line from a colour ramp DAT file and sets the value
///				in the colour ramp array
/// \param line
/// \return
///
bool QtColourRampWidget::processlineDAT(const std::string line)
{
	// Split string
	vector<string> tokens = split(line, ' ');

	//	Remove non-null tokens on the line (working backwards)
	if (!tokens.empty())
	{
		for (int i = tokens.size()-1; i > -1; --i)
		{
			//printf("%i\n", i);
			//fflush(stdout);

			//	We aren't interested in empty strings
			if (tokens[i] == "")
				tokens.erase(tokens.begin()+i);
		}

		//printf("Non-null tokens = %i\n", tokens.size());

		//	We expect 4 groups of 4 values per line in the form:
		//	index r g b	index r g b index r g b	index r g b
		if (!tokens.size() == 16)
		{
			return false;
		}

		//	Now we loop through each of the values on the line and
		//	create the entry in the colour table.
		for (unsigned int i = 0; i < 16; i+= 4)
		{
			//	parse to integers
			int index = atoi(tokens[i].c_str());
			int red = atoi(tokens[i+1].c_str());
			int green = atoi(tokens[i+2].c_str());
			int blue = atoi(tokens[i+3].c_str());

			//	Check the values were valid
			if ((index > MAX_COLOURS) || (red > 256)
				|| (green > 256) || (blue > 256))
				return false;

			//	And set the values
			m_colourTable[index] = QColor(red, green, blue);
		}
	}

	return true;
}

///
/// \brief		Loads a colour ramp DAT file from an embedded resource
/// \param file
/// \return
///
bool QtColourRampWidget::LoadDAT(QFile &file)
{
	if (!file.open(QIODevice::ReadOnly | QIODevice::Text)) return false;

	unsigned long line = 0;

	while (!file.atEnd())
	{
		QString currentLine = file.readLine();

		bool result = processlineDAT(currentLine.toStdString());

		if (!result)
		{
			fprintf(stderr,
					"Error detected in DAT file loading at line %ld.",
					line);
			fprintf(stderr, "  Ignoring and continuing...\n");
			fflush(stdout);
		}

		++line;
	}

	file.close();

	cout << ToString() << endl;

	return true;
}

///
/// \brief		Loads a colour ramp from an external file
/// \param filename
/// \return
///
bool QtColourRampWidget::LoadDAT(const string filename)
{
	ifstream file;

	file.open(filename);

	if (!file.is_open()) return false;
	unsigned long line = 0;

	while (!file.eof())
	{
		string currentLine;
		getline(file, currentLine);

		bool result = processlineDAT(currentLine);

		if (!result)
		{
			fprintf(stderr,
					"Error detected in DAT file loading at line %ld.",
					line);
			fprintf(stderr, "  Ignoring and continuing...\n");
			fflush(stderr);
		}

		++line;
	}

	file.close();

	cout << ToString() << endl;

	return true;
}


void QtColourRampWidget::ShowColourRampLoadDialog()
{
	QString DAT_FILTER = "Dat File (*.dat)";

	QString filterList = DAT_FILTER;

	QFileDialog openDialog(this, "Open colour ramp", m_colour_ramp_dir.absolutePath(), filterList);

	if (openDialog.exec())
	{
		QFileInfo fileInfo(openDialog.selectedFiles()[0]);

#ifdef VERBOSE_OUTPUT
		printf("Fileformat = %s\n", fileInfo.suffix().toStdString().c_str());
		fflush(stdout);
#endif
		if (!fileInfo.exists()) return;

		bool result = false;

		if (fileInfo.suffix() == "dat")
		{
			result = LoadDAT(openDialog.selectedFiles()[0].toStdString());

			cout << "Colour ramp file loaded correctly: " << (result ? "true" : "false");
		}

		//	Correctly loaded so draw the new map
		//	TODO: this should be handled in a much more modular fashion
		if (result)
		{
			SetDisplayMode(DisplayMode::COLOUR_RAMP);

			update();

			//	Persist the directory
			m_colour_ramp_dir = fileInfo.absolutePath();

			emit OnColourRampDirChanged(m_colour_ramp_dir);
			emit OnColourTableChanged(m_colourTable);
		}
	}
}

void QtColourRampWidget::mouseDoubleClickEvent(QMouseEvent *event)
{
	if (event->button() == Qt::LeftButton)
	{
		ShowColourRampLoadDialog();
	}
}


QtColourRampWidget::QtColourRampWidget(QWidget *parent) :
	QWidget(parent)
{
	//	Opacity not working for now
	m_opacity = 127;
	m_isHorizontal = false;

	generatePopupMenu();

	//	Generate a default colour ramp lookup table
	m_colourTable.resize(MAX_COLOURS);
	GenerateDefaultColourRamp();

	//	Should we draw an outline border
	m_hasBorder = true;
	m_borderColour = Qt::black;

	//	If set to solid colour mode we can assign two colours for the entire
	//	widget
	m_universalColour = QCOLOUR_CB_SAFE_BLUISH_GREEN;
	m_dualColours = { QCOLOUR_CB_SAFE_SKY_BLUE, QCOLOUR_CB_SAFE_ORANGE };

	//	Default to use a colour ramp
	m_displayMode = DisplayMode::DUAL_COLOUR;

	//	For drawing a zero line
	m_minimumValue = -1.0f;
	m_maximumValue = +1.0f;
}

void QtColourRampWidget::paintEvent(QPaintEvent *)
{
	QPainter painter(this);
	painter.save();
	/*
	QColor backgroundColor = palette().light().color();
	backgroundColor.setAlpha(127);
	painter.fillRect(rect(),backgroundColor);
*/

	//printf("Segment width = %f.\n", m_segmentWidth);
	switch (m_displayMode)
	{
		case DisplayMode::UNIVERSAL_COLOUR:
			drawSingleColour(painter);
			break;
		case DisplayMode::DUAL_COLOUR:
			drawDualColour(painter);
			break;
		case DisplayMode::COLOUR_RAMP:
			drawColourRamp(painter);
			break;
		default:
			drawColourRamp(painter);
	}

	if (m_hasCurrentValueLine)
		drawCurrentValueLine(painter);

	if (m_hasZeroLine)
		drawZeroLine(painter);

	if (m_hasBorder)
		drawBorder(painter);

	painter.restore();

	fflush(stdout);
}
