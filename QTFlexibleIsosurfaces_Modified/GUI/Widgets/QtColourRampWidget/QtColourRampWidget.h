#ifndef QTCOLOURRAMP_H
#define QTCOLOURRAMP_H

#include <QWidget>
#include <QPainter>
#include <QFileDialog>
#include <fstream>
#include <cstdlib>
#include <ios>
#include <vector>
#include <string>
#include <sstream>
#include <QMenu>
#include <QMouseEvent>
#include <QDebug>
#include <QVector>
#include <QStandardPaths>

#include "GUI/Widgets/Colour.h"
#include "Globals/Globals.h"

//#define VERBOSE_OUTPUT

//	Colour-blind safe colours
//	see: http://jfly.iam.u-tokyo.ac.jp/color/ [31-03-2016]
#define QCOLOUR_CB_SAFE_SKY_BLUE QColor(86, 180, 233)
#define QCOLOUR_CB_SAFE_ORANGE QColor(230, 159, 0)
#define QCOLOUR_CB_SAFE_BLUISH_GREEN QColor(0, 158, 115)

class QtColourRampWidget : public QWidget
{
		Q_OBJECT
	public:
		friend QDataStream& operator<<(QDataStream& out, const QtColourRampWidget& widget);
		friend QDataStream& operator>>(QDataStream& in, QtColourRampWidget& widget);

		static const int MAX_COLOURS = 256;
		using ColourTable = QVector<QColor>;
		using Byte = Colour::Integer::Byte;

		struct QColorPair
		{
				QColor negative;
				QColor positive;
		};

		enum class DisplayMode
		{
			UNIVERSAL_COLOUR = 0,
			DUAL_COLOUR = 1,
			COLOUR_RAMP = 2
		};
	protected:

		vector<string> &split(const string &s,
							  char delim,
							  vector<string> &elems);
		vector<string> split(const string &s, char delim);

	private:
		DisplayMode m_displayMode;

		ColourTable m_colourTable;
		QColorPair m_dualColours;
		QColor m_universalColour;
		//float m_segmentWidth;
		//float m_segmentHeight;
		Byte m_opacity;
		bool m_isHorizontal;
		bool m_hasBorder;

		void generatePopupMenu();



		QColor m_borderColour;

		bool m_hasCurrentValueLine = true;

		bool m_hasZeroLine = true;
		float m_zeroLineThickness = 1.0f;
		QColor m_zeroLineColour = QColor(Qt::black);

		float m_currentIsovalueLineThickness = 1.0f;
		QColor m_currentIsovalueLineColor = QColor(Qt::black);

		float m_currentIsovalue = 0.0f;

		void drawColourRamp(QPainter& painter);
		void drawSingleColour(QPainter& painter);
		void drawDualColour(QPainter& painter);

		void drawBorder(QPainter& painter);
		void drawZeroLine(QPainter &painter);
		void drawCurrentValueLine(QPainter &painter);

		QDir m_colour_ramp_dir = QDir::homePath();
	public:
		void SetDirectory(const QDir& dir) { m_colour_ramp_dir = dir; }
		QDir GetDirectory() const { return m_colour_ramp_dir; }

		std::string ToString() const;

		ColourTable GetColourTable() const;
		ColourTable GetColourTable(const float normalisedMin, const float normalisedMax) const;

		void GenerateDefaultColourRamp();

		void SetIsovalueRange(const float& min, const float& max);
		void SetCurrentNormalizedIsovalue(const float& value);

		bool LoadDAT(QFile &file);
		bool LoadDAT(const std::string filename);
		bool processlineDAT(const std::string line);

		//explicit QtColourRampWidget(const QtColourRampWidget&) = default;
		explicit QtColourRampWidget(QWidget *parent = 0);

		//QtColourRampWidget& operator=(const QtColourRampWidget&) = default;

		virtual void paintEvent(QPaintEvent *);
		virtual void mouseDoubleClickEvent(QMouseEvent *event);

		virtual QColor GetColorFromValue(const float &value) const;
		virtual QColor GetColorFromValue(const unsigned char& value) const;

		virtual ~QtColourRampWidget();

		std::string QColorToString(const QColor& colour) const;

		QColor GetUniversalColour() const { return m_universalColour; }

		QColor GetNegativeColour() const { return m_dualColours.negative; }
		QColor GetPositiveColour() const { return m_dualColours.positive; }

		void SetDualColours(const QColorPair& value);
		QColorPair GetDualColours() const { return m_dualColours; }

		DisplayMode GetDisplayMode() const { return m_displayMode; }

		float m_minimumValue;
		float m_maximumValue;

	signals:
		void OnDisplayModeChanged(const DisplayMode& value);

		void OnUniversalColourChanged(const QColor& value);
		void OnNegativeColourChanged(const QColor& value);
		void OnPositiveColourChanged(const QColor& value);

		void OnColourTableChanged(const ColourTable& value);

		void OnColourRampDirChanged(const QDir& dir);
	public slots:
		void SetUniversalColour(const QColor& value);
		void SetNegativeColour(const QColor& value);
		void SetPositiveColour(const QColor& value);
		void SetColourTable(const ColourTable& value);

		void ShowColourRampLoadDialog();
		void ReverseColourRamp();

		void SetDisplayMode(const DisplayMode &value);
		void ShowUniversalColourSelectionDialog();
		void ShowPositiveColourSelectionDialog();
		void ShowNegativeColourSelectionDialog();
};


#endif // QTCOLOURRAMP_H
