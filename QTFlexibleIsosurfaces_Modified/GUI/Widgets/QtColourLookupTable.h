#ifndef QTCOLOURLOOKUPTABLE_H
#define QTCOLOURLOOKUPTABLE_H

#include <QRegularExpression>
#include <sstream>
#include <fstream>
#include <cassert>

class QtColourLookupTable
{
private:
	const static size_t MAX_COLOURS = 256;

	using Storage = QVector<QColor>;

	Storage m_table;

	std::vector<std::string> &split(const std::string &s,
						  char delim,
						  std::vector<std::string> &elems)
	{
		std::stringstream ss(s);
		std::string item;

		while (std::getline(ss, item, delim))
		{
			elems.push_back(item);
		}
		return elems;
	}

	std::vector<std::string> split(const std::string &s, char delim)
	{
			std::vector<std::string> elems;
			return split(s, delim, elems);
	}

	bool processlineDAT(const std::string& line)
	{
		m_table.resize(MAX_COLOURS);

		// Split string
		std::vector<std::string> tokens = split(line, ' ');

		//	Remove non-null tokens on the line (working backwards)
		if (!tokens.empty())
		{
			for (int i = tokens.size()-1; i > -1; --i)
			{
				//printf("%i\n", i);
				//fflush(stdout);

				//	We aren't interested in empty strings
				if (tokens[i] == "")
					tokens.erase(tokens.begin()+i);
			}

			//printf("Non-null tokens = %i\n", tokens.size());

			//	We expect 4 groups of 4 values per line in the form:
			//	index r g b	index r g b index r g b	index r g b
			if (!tokens.size() == 16)
			{
				return false;
			}

			//	Now we loop through each of the values on the line and
			//	create the entry in the colour table.
			for (unsigned int i = 0; i < 16; i+= 4)
			{
				//	parse to integers
				int index = atoi(tokens[i].c_str());
				int red = atoi(tokens[i+1].c_str());
				int green = atoi(tokens[i+2].c_str());
				int blue = atoi(tokens[i+3].c_str());

				//	Check the values were valid
				if ((index > MAX_COLOURS) || (red > 256)
					|| (green > 256) || (blue > 256))
					return false;

				//	And set the values
				m_table[index] = QColor(red, green, blue);
			}
		}

		return true;
	}

public:
	Storage getInternalArray() const
	{
		return m_table;
	}

	void setInternalArray(const Storage& value)
	{
		m_table = value;
	}

	bool loadFromDat(const std::string& filename)
	{
		std::ifstream file;

		file.open(filename);

		if (!file.is_open()) return false;
		unsigned long line = 0;

		while (!file.eof())
		{
			std::string currentLine;
			std::getline(file, currentLine);

			bool result = processlineDAT(currentLine);

			if (!result)
			{
				std::cerr << "Error detected in DAT file loading at line " << line
				 << ".  Ignoring and continuing...\n";
			}

			++line;
		}

		file.close();
		return true;
	}

	QtColourLookupTable()
	{
		m_table.push_back(Qt::black);
		m_table.push_back(Qt::white);
	}

	QColor& operator[](const size_t i) { return m_table[i]; }
	QColor operator[](const size_t i) const { return m_table[i]; }

	QColor at(const float norm) const
	{
		assert((norm >= 0.0f) && (norm <= 1.0f));

		auto index = static_cast<size_t>(norm * (m_table.size()-1));

		return m_table[index];
	}

	size_t size() const { return m_table.size(); }


};

inline std::ostream& operator<<(std::ostream& os, const QtColourLookupTable& table)
{
	for (auto i = 0ul; i < table.size(); ++i)
	{
		QColor color = table[i];
		os << "(i=" << i
		   << ", r=" << color.red()
		   << ", g=" << color.green()
		   << ", b=" << color.blue() << ")";
	}
	return os;
}


#endif // QTCOLOURLOOKUPTABLE_H
