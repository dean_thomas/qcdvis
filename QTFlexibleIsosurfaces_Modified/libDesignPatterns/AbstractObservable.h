///	@file		...
///
/// @brief		...
///	@since		...
///	@author		Dean
///
///	@details	...
///
///	@copyright	...
///
///	@todo		...

#ifndef ABSTRACT_OBSERVABLE_H
#define ABSTRACT_OBSERVABLE_H

#include "IObservable.h"
#include "IObserver.h"
#include <vector>

using std::vector;

namespace DesignPatterns
{
	class AbstractObservable :
		public IObservable
	{
	public:
		typedef vector<IObserver*> ObserverList;

		virtual ~AbstractObservable();

		virtual void AddObserver(IObserver *observer);
		virtual void RemoveObserver(IObserver *observer);
		virtual void NotifyObservers() const;
	protected:
		AbstractObservable();
		ObserverList m_observers;

	};
}

#endif