///	@file		...
///
/// @brief		...
///	@since		...
///	@author		Dean
///
///	@details	...
///
///	@copyright	...
///
///	@todo		...

#ifndef IOBSERVER_H
#define IOBSERVER_H

namespace DesignPatterns
{
	class IObservable;

	class IObserver
	{
	public:
		virtual ~IObserver() { }

		virtual void ReceiveUpdateNotification(const IObservable *changedObservable) = 0;
	};
}

#endif