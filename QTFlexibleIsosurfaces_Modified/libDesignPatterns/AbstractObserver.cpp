#include "AbstractObserver.h"

namespace DesignPatterns
{
	AbstractObserver::AbstractObserver()
	{
	}


	AbstractObserver::~AbstractObserver()
	{
	}

	void AbstractObserver::ReceiveUpdateNotification(const IObservable *changedObservable)
	{
		printf("Received update notification.\n");
	}
}