///	@file		...
///
/// @brief		...
///	@since		...
///	@author		Dean
///
///	@details	...
///
///	@copyright	...
///
///	@todo		...

#ifndef IOBSERVABLE_H
#define IOBSERVABLE_H

#include "IObserver.h"

namespace DesignPatterns
{
	class IObservable
	{
	public:
		virtual ~IObservable() { }

		virtual void AddObserver(IObserver *observer) = 0;
		virtual void RemoveObserver(IObserver *observer) = 0;
		virtual void NotifyObservers() const = 0;

	protected:
		IObservable() { }
	};
}

#endif