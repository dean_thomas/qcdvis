///	@file		...
///
/// @brief		...
///	@since		...
///	@author		Dean
///
///	@details	...
///
///	@copyright	...
///
///	@todo		...

//	Stdlib includes
#include <stack>

//	Observer pattern
#include "IObserver.h"
#include "IObservable.h"
#include "AbstractObserver.h"
#include "AbstractObservable.h"

//	Command Pattern
#include "ICommand.h"
#include "MacroCommand.h"

//	Typedefs
namespace DesignPatterns
{
	typedef std::stack<ICommand*> CommandStack;
}
