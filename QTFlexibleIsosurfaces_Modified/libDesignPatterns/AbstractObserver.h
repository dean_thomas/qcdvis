///	@file		...
///
/// @brief		...
///	@since		...
///	@author		Dean
///
///	@details	...
///
///	@copyright	...
///
///	@todo		...
#ifndef ABSTRACT_OBSERVER_H
#define ABSTRACT_OBSERVER_H

#include <cstdio>
#include "IObserver.h"

namespace DesignPatterns
{
	class AbstractObserver :
		public IObserver
	{
	public:
		AbstractObserver();
		virtual ~AbstractObserver();
		virtual void ReceiveUpdateNotification(const IObservable *changedObservable);
	};
}

#endif