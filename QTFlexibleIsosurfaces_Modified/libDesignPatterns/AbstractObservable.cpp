///	@file		...
///
/// @brief		...
///	@since		...
///	@author		Dean
///
///	@details	...
///
///	@copyright	...
///
///	@todo		...

#include "AbstractObservable.h"

namespace DesignPatterns
{
	AbstractObservable::AbstractObservable()
	{

	}

	AbstractObservable::~AbstractObservable()
	{
	}

	void AbstractObservable::AddObserver(IObserver *observer)
	{
		for (long i = m_observers.size() - 1; i > -1; --i)
		{
			//	Make sure it isn't already in the list
			if (m_observers[i] == observer)
				return;
		}

		//	Add to the list
		m_observers.push_back(observer);
	}

	void AbstractObservable::RemoveObserver(IObserver *observer)
	{
		for (long i = m_observers.size() - 1; i > -1; --i)
		{
			//	Make sure it is in the list
			if (m_observers[i] == observer)
			{
				//	Erase the observer
				m_observers.erase(m_observers.begin() + i);
			}
		}
	}

	void AbstractObservable::NotifyObservers() const
	{
		for (unsigned long int i = 0; i < m_observers.size(); i++)
		{
			m_observers[i]->ReceiveUpdateNotification(this);
		}
	}
}