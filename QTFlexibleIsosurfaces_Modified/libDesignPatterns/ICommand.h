///	@file		ICommand.h
///
/// @brief		...
///	@since		19-01-2015
///	@author		Dean
///
///	@details	...
///
///	@copyright	...
///
///	@todo		...

#ifndef ICOMMAND_H
#define ICOMMAND_H

namespace DesignPatterns
{
	class ICommand
	{
	public:
		virtual ~ICommand() { }

		virtual void Execute() = 0;
		virtual void Undo() = 0;
	protected:
		ICommand() { }
	};
}

#endif