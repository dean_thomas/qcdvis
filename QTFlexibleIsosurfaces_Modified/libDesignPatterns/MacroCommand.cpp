#include "MacroCommand.h"

namespace DesignPatterns
{
	MacroCommand::MacroCommand()
	{

	}

	MacroCommand::~MacroCommand()
	{

	}

	void MacroCommand::Execute()
	{
		for (unsigned long i = 0; i < m_commandList.size(); ++i)
		{
			m_commandList[i]->Execute();
		}
	}
	
	void MacroCommand::Undo()
	{
		for (long i = m_commandList.size() - 1; i > -1; --i)
		{
			m_commandList[i]->Undo();
		}
	}

	void MacroCommand::AddCommand(ICommand *command)
	{
		m_commandList.push_back(command);
	}

	void MacroCommand::RemoveCommand(ICommand *command)
	{
		for (long i = m_commandList.size() - 1; i > -1; --i)
		{
			if (m_commandList[i] == command)
			{
				m_commandList.erase(m_commandList.begin() + i);
			}
		}
	}

	unsigned long MacroCommand::GetCommandCount() const
	{
		return m_commandList.size();
	}

}