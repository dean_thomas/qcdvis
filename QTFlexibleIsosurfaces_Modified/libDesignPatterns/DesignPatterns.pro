#-------------------------------------------------
#
# Project created by QtCreator 2015-01-19T14:02:45
#
#-------------------------------------------------

QT       -= core gui

TARGET = DesignPatterns
TEMPLATE = lib
CONFIG += staticlib

SOURCES += \
    AbstractObservable.cpp \
    AbstractObserver.cpp \
    MacroCommand.cpp

HEADERS += \
    AbstractObservable.h \
    AbstractObserver.h \
    DesignPatterns.h \
    ICommand.h \
    IObservable.h \
    IObserver.h \
    MacroCommand.h

unix { \
    target.path = /usr/lib
    INSTALLS += target
}
