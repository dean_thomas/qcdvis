///	@file		MacroCommand.h
///
/// @brief		...
///	@since		19-01-2015
///	@author		Dean
///
///	@details	...
///
///	@copyright	...
///
///	@todo		...

#ifndef MACRO_COMMAND_H
#define MACRO_COMMAND_H

#include "ICommand.h"
#include <vector>

using std::vector;

namespace DesignPatterns
{
	class MacroCommand
	{
	public:
		MacroCommand();
		virtual ~MacroCommand();

		virtual void Execute();
		virtual void Undo();

		void AddCommand(ICommand *command);
		void RemoveCommand(ICommand *command);

		unsigned long GetCommandCount() const;
	protected:
		vector<ICommand*> m_commandList;
	};
}

#endif