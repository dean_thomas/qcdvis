///////////////////////////////////////////////////
//
//	Contour-tree based isosurfaces
//	Version 4.0 (QT-based)
//	copyright Hamish Carr
//	January, 2014
//
//	------------------------
//	main.cpp
//	------------------------
//
///////////////////////////////////////////////////

#include <QApplication>

#include "DataModel.h"
#include "CommandLine/CommandLine.h"
#include "CommandLine/RegexCommandLineParser.h"
#include "ProgramParameters.h"
#include <stdio.h>
#include "GUI/Forms/MDIParentWindow.h"
#include "CommandLine/QtCommandLineWorker.h"

using CommandLine::ArgumentParser;

///
/// \brief      Parses the switches passed to the program by the user
/// \param      argc
/// \param      argv
/// \return
/// \author     Dean
/// \since      01-12-2014
///
ProgramParameters parseCommandLine(int argc, char* argv[])
{
	ProgramParameters result;

	printf("Parsing command line parameters.\n");

	/*
	//  Get the input file
	if (argParser.IsSet("file"))
	{
		result.filename = argParser.GetSwitchParameters("file")[0];

		printf("Filename is :%s\n", result.filename.c_str());
		fflush(stdout);
	}
	else if (argParser.IsSet("list"))
	{
		result.list = argParser.GetSwitchParameters("list")[0];

		printf("Filelist is :%s\n", result.list.c_str());
		fflush(stdout);
	}
	//else
	//{
	//    printf("Must either specify a single file or list for processing!\n");
	//    fflush(stdout);
	//    exit(1);
	//}

	//  Set dimensions for heightfield (if specified)
	if (argParser.IsSet("size"))
	{
		result.dimX = atoi(argParser.GetSwitchParameters("size")[0].c_str());
		result.dimY = atoi(argParser.GetSwitchParameters("size")[1].c_str());
		result.dimZ = atoi(argParser.GetSwitchParameters("size")[2].c_str());
	}

	//  Set sample resolution if specified
	if (argParser.IsSet("res"))
	{
		result.resolution = atoi(argParser.GetSwitchParameters("res")[0].c_str());
	}

	//  Limits from first step of the data
	result.limitsFromFirstStep = argParser.IsSet("limits");

	//  Periodic boundaries
	result.periodicBoundaries = argParser.IsSet("pb");

	//  No GUI
	result.noGUI = argParser.IsSet("nogui");
*/
	return result;
}

///
/// \brief		Close the StdOut file at program exit, if it has been
///				redirected
/// \since		27-10-2015
/// \author		Dean
///
void closeStdOut()
{
	fclose(stdout);
}

///
/// \brief		Close the StdErr file at program exit, if it has been
///				redirected
/// \since		27-10-2015
/// \author		Dean
///
void closeStdErr()
{
	fclose(stderr);
}

///
/// \brief		setOutputOptions
/// \param		disableBuffers <tt>bool</tt>: if true Qt's buffered output
///				behaviour will be disabled.
/// \param		stdErrFileName <tt>string</tt>: the file name to be used for
///				output from the standard error stream (or an empty string for
///				no redirection).
/// \param		stdOutFileName <tt>string</tt>: the file name to be used for
///				output from the standard output stream (or an empty string for
///				no redirection).
/// \since		27-10-2015
/// \author		Modifies behaviour of the stdout, stderr output streams
///				as required
///
void setOutputOptions(const bool& disableBuffers,
					  const std::string& stdOutFileName,
					  const std::string& stdErrFileName)
{
	if (disableBuffers)
	{
		//	Turn off Qt's buffered output behaviour
		setbuf(stdout, NULL);
		setbuf(stderr, NULL);
	}

	if (stdOutFileName != "")
	{
		//	Redirect stdout to the specified output file
		cout << "stdout is being redirected to '";
		cout << stdOutFileName << "'." << endl;
		freopen(stdOutFileName.c_str(), "w", stdout);

		//	Mark that the file needs to be closed on exit
		atexit(closeStdOut);
	}

	if (stdErrFileName != "")
	{
		//	Redirect stderr to the specified output file
		cout << "stderr is being redirected to '";
		cout << stdErrFileName << "'." << endl;
		freopen(stdErrFileName.c_str(), "w", stderr);

		//	Mark that the file needs to be closed on exit
		atexit(closeStdErr);
	}
}

///
/// \brief      Entry point to program
/// \param      argc <tt>int</tt>: number of command line arguments
/// \param      argv <tt>char*[]</tt>: string array representing command line
///				arguments
/// \return		<tt>int</tt>: error code from application (or 0, if all was ok).
/// \since      01-12-14
/// \author     Hamish
/// \author		Dean: clean up and simplify code [27-10-2015]
///
int main(int argc, char *argv[])
{
	RegexCommandLineParser argParser;
	auto args = argParser(argc, argv);

	//	configure the command line
	setOutputOptions(false, args.stdOutFile, args.stdErrFile);

	//  use that to create a height field &c.
	DataModel dataModel;

	if (args.noGui)
	{
		QtCommandLineWorker commandLineWorker(dataModel, args);

		commandLineWorker.DoWork();

		return 0;
	}
	else
	{
		QApplication app(argc, argv);

		qDebug() << qApp;

		MDIParentWindow mdiParentWindow(dataModel);
		mdiParentWindow.show();

		return app.exec();
	}
}
