///
///		Represents the simplest container, STL style fixed array.
///
///		Provides the required minimum set of requirements needed
///		to produce a container compatible with the STL.
///
///		Taken from: Generic Programming and the STL: Using and
///		extending the C++ Standard Template Library (M.H.Austern),
///		pg. 66-67.
///
///		\since		09-11-2015
///		\author		Dean
///
#ifndef BLOCK_H
#define BLOCK_H

#include <cstddef>
#include <iterator>

template <typename T, size_t N>
struct block
{
	typedef T value_type;

	typedef value_type* pointer;
	typedef const value_type* const_pointer;
	typedef value_type& reference;
	typedef const value_type& const_reference;

	typedef ptrdiff_t difference_type;
	typedef size_t size_type;

	typedef pointer iterator;
	typedef const_pointer const_iterator;

	typedef std::reverse_iterator<iterator> reverse_iterator;
	typedef std::reverse_iterator<const_iterator> const_reverse_iterator;

	iterator begin() { return m_data; }
	iterator end() { return m_data + N; }

	const_iterator begin() const { return m_data; }
	const_iterator end() const { return m_data + N; }

	reverse_iterator rbegin() { return reverse_iterator(end()); }
	reverse_iterator rend() { return reverse_iterator(begin()); }

	const_reverse_iterator rbegin() const
	{
		return const_reverse_iterator(end());
	}

	const_reverse_iterator rend() const
	{
		return const_reverse_iterator(begin());
	}

	reference operator[](size_type n) { return data[n]; }
	const_reference operator[](size_type n) const { return data[n]; }

	size_type size() const { return N; }
	size_type max_size() const { return N; }
	bool empty() const { return N == 0; }

	void swap(block& x)
	{
		for (size_t n = 0; n < N; ++n)
		{
			std::swap(data[n], x.data[n]);
		}
	}

	T data[N];
};

///
///		Provide an Equality operator (==) to make the class
///		'Equality comparable'
///
template <typename T, size_t N>
bool operator==(const block<T, N>& x, const block<T, N>& y)
{
	for (size_t n = 0; n < N; ++n)
	{
		if (x.data[n] != y.data[n]) return false;
	}
	return true;
}

///
///		Provide an less-thatn comparison operator (<) to make the class
///		'Less-than comparable'
///
template <typename T, size_t N>
bool operator<(const block<T, N>& x, const block<T, N>& y)
{
	for (size_t n = 0; n < N; ++n)
	{
		if (x.data[n] < y.data[n])
		{
			return true;
		}
		else if (y.data[n] < x.data[n])
		{
			return false;
		}
	}
	return false;

}

#endif