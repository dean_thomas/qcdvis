///
///		Represents a skeleton class for a custom container.
///
///		Provides the required minimum set of requirements needed
///		to produce a container compatible with the STL.
///
///		Taken from: Generic Programming and the STL: Using and
///		extending the C++ Standard Template Library (M.H.Austern),
///		pg. 71-72.
///
///		\since		09-11-2015
///		\author		Dean
///
#ifndef TRIVIAL_CONTAINER_H
#define TRIVIAL_CONTAINER_H

#include <cstddef>

template <typename T>
struct trivial_container
{
	typedef T value_type;

	typedef value_type* pointer;
	typedef const value_type* const_pointer;
	typedef value_type& reference;
	typedef const value_type& const_reference;

	typedef pointer iterator;
	typedef const_pointer const_iterator;

	typedef ptrdiff_t difference_type;
	typedef size_t size_type;

	const_iterator begin() { return 0; }
	const_iterator end() { return 0; }

	size_type size() const { return 0; }
	bool empty() const { return true; }
	size_type max_size() const { return 0; }

	void swap(trivial_container&) { } 
};

#endif