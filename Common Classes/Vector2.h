#ifndef VECTOR_2D_H
#define VECTOR_2D_H

#include <vector>
#include <cstdlib>
#include <cassert>
#include <string>
#include <sstream>
#include <iomanip>
#include <iostream>
#include <utility>

//  Work around for VS2013
#if defined(_MSC_VER) && (_MSC_VER < 1900)
#define noexcept _NOEXCEPT
#endif

namespace Storage
{
	///
	/// \brief	Allows indices to be passed as 2-tuples
	/// \since	28-09-2015
	/// \author	Dean
	///
	struct Index2
	{
		size_t x;
		size_t y;
	};

	//		Forward declaration of our template class
	template<typename T>
	class Vector2;

	//		Forward declaration of our stream operators
	template<typename T>
	std::ostream& operator<<(std::ostream&, const Vector2<T>&);

	//template<typename T>
	//std::istream& operator>>(std::istream&, Array2d<T>&);

	//		Actual template declaration
	template <typename T>
	class Vector2
	{
	private:
		//	Aliases for the underlying data storage container
		using container_type = std::vector<T>;

	public:
		using size_type = typename container_type::size_type;
		using reference = typename container_type::reference;
		using const_reference = typename container_type::const_reference;

		//	Aliases for container iterators
		using iterator = typename container_type::iterator;
		using const_iterator = typename container_type::const_iterator;
		using reverse_iterator = typename container_type::reverse_iterator;
		using const_reverse_iterator = typename container_type::const_reverse_iterator;

	private:
		container_type m_elements;
		size_type m_xSize;
		size_type m_ySize;

	public:
		//	Iterator pass-through
		iterator begin() { return m_elements.begin(); }
		iterator end() { return m_elements.end(); }
		const_iterator begin() const { return m_elements.begin(); }
		const_iterator end() const { return m_elements.end(); }

		const_iterator cbegin() { return m_elements.cbegin(); }
		const_iterator cend() { return m_elements.cend(); }

		//	Reverse Iterator pass-through
		reverse_iterator rbegin() { return m_elements.rbegin(); }
		reverse_iterator rend() { return m_elements.rend(); }
		const_reverse_iterator crbegin() { return m_elements.crbegin(); }
		const_reverse_iterator crend() { return m_elements.crend(); }

		//	Nested Container function pass-through
		size_type size() const noexcept { return m_elements.size(); }
		bool empty() const noexcept { return m_elements.empty(); }

		size_type index2DtoIndex1D(const size_type& x,
					   const size_type& y) const
		{
		assert(x < m_xSize);
		assert(y < m_ySize);

		return x + m_xSize * y;
		}
	public:

		size_type size_x() const { return m_xSize; }
		size_type size_y() const { return m_ySize; }

		///
		/// \brief		Returns a writeable reference the element at the
		///				specified index
		/// \param		index
		/// \return
		/// \since		28-09-2015
		/// \author		Dean
		///
		reference operator[](const Index2& index)
		{
		return m_elements[index2DtoIndex1D(index.x, index.y)];
		}

		///
		/// \brief		Returns a read-only reference the element at the
		///				specified index
		/// \param		index
		/// \return
		/// \since		28-09-2015
		/// \author		Dean
		///
		const_reference operator[](const Index2& index) const
		{
		return m_elements[index2DtoIndex1D(index.x, index.y)];
		}

		const_reference Get(const size_t &x, const size_t &y) const
		{
		return m_elements[index2DtoIndex1D(x, y)];
		}

		///
		/// \brief	Returns the array as a formatted string
		///	\author	Dean
		///	\since	25-08-2015
		///
		std::string ToString() const
		{
		std::stringstream result;

		for (size_type y = 0; y < m_ySize; ++y)
		{
			for (size_type x = 0; x < m_xSize; ++x)
			{
			result << std::setw(12) << std::setprecision(6);
			result << m_elements[index2DtoIndex1D(x, y)];

			//	Tab between values
			if (x != m_xSize - 1)
				result << "\t";
			}
			result << std::endl;
		}

		return result.str();
		}

		///
		/// \brief	Resizes the array to the specified dimensions
		///	\author	Dean
		///	\since	25-08-2015
		///
		void resize(const size_type &xSize, const size_type &ySize)
		{
		m_xSize = xSize;
		m_ySize = ySize;

		m_elements.resize(xSize * ySize);
		}

	public:
		//	Constructor, destructors, operators

		///
		/// \brief	Default constructor (creates an empty 1x1 array)
		///	\author	Dean
		///	\since	25-08-2015
		///
		Vector2()
		: Vector2(1, 1)
		{ }

		///
		/// \brief	Constructor that creates an empty xSize by ySize array)
		///	\author	Dean
		///	\since	25-08-2015
		///
		Vector2(const size_type &xSize,
			 const size_type &ySize)
		: m_xSize{xSize}, m_ySize{ySize}
		{
		//	Reserve space (creating empty elements)
		auto count = xSize * ySize;
		m_elements.resize(count);
		}

		///
		/// \brief	Constructor that fills an array with the specified value
		///	\author	Dean
		///	\since	25-08-2015
		///
		Vector2(const size_type &xSize,
			 const size_type &ySize,
			 const T& value)
		: Vector2(xSize, ySize)
		{
		//	Clone the data
		for(auto it = m_elements.begin(); it != m_elements.end(); ++it)
		{
			*it = value;
		}
		}

		///
		/// \brief	Constructor that takes a number of values enclodes in braces
		///	\author	Dean
		///	\since	25-08-2015
		///
		Vector2(const size_type &xSize,
			 const size_type &ySize,
			 std::initializer_list<T> values)
		: Vector2(xSize, ySize)
		{
		//	Check the number of values matches the number of elements in
		//	our array
		assert(values.size() == size());

		//	Copy values from the iterator list to the member vector
		for(auto it1 = m_elements.begin(), it2 = values.begin();
			it2 != values.end(); ++it1, ++it2)
		{
			*it1 = *it2;
		}
		}

		///
		/// \brief	Constructor that takes a number of values from a std::vector
		///	\author	Dean
		///	\since	23-09-2015
		///
		Vector2(const size_type &xSize,
			 const size_type &ySize,
			 const std::vector<T>& values)
		: Vector2(xSize , ySize)
		{
		//	Check the number of values matches the number of elements in
		//	our array
		assert(values.size() == size());

		//	Copy values from the iterator list to the member vector
		auto it1 = m_elements.begin();
		auto it2 = values.begin();
		for (;it2 != values.end(); ++it1, ++it2)
		{
			*it1 = *it2;
		}
		}

		///
		///	\brief	Destructor
		///	\author	Dean
		///	\since	25-08-2015
		///
		~Vector2()
		{ }

		///
		/// \brief	Copy constructor
		/// \param rhs
		///	\author	Dean
		///	\since	25-08-2015
		///
		Vector2(const Vector2& rhs)
		{
		m_xSize = rhs.m_xSize;
		m_ySize = rhs.m_ySize;

		m_elements.resize(rhs.m_elements.size());

		for (size_t i = 0; i < rhs.m_elements.size(); ++i)
		{
			m_elements[i] = rhs.m_elements[i];
		}
		}

		///
		/// \brief	Move constructor
		/// \param rhs
		///	\author	Dean
		///	\since	25-08-2015
		///
		Vector2(Vector2&& rhs)
		{
		m_xSize = rhs.m_xSize;
		m_ySize = rhs.m_ySize;
		m_elements = rhs.m_elements;
		rhs.m_elements.clear();
		}

		///
		/// \brief	Copy assignment
		/// \param rhs
		/// \return
		///	\author	Dean
		///	\since	25-08-2015
		///
		Vector2& operator=(const Vector2& rhs)
		{
		//	Check for self-assigment
		if (this != &rhs)
		{
			m_xSize = rhs.m_xSize;
			m_ySize = rhs.m_ySize;
			m_elements = rhs.m_elements;
		}
		return *this;
		}

		///
		/// \brief	Move assignment
		/// \param rhs
		/// \return
		///	\author	Dean
		///	\since	25-08-2015
		///
		Vector2& operator=(Vector2&& rhs)
		{
		assert(this != &rhs);

		m_xSize = rhs.m_xSize;
		m_ySize = rhs.m_ySize;
		m_elements = rhs.m_elements;
		//rhs.m_elements.clear();

		return *this;
		}

		///
		/// \brief		Returns a writeable reference to the item with the
		///				specified x and y position in the array
		/// \param x
		/// \param y
		/// \return
		/// \since		28-09-2015
		/// \author		Dean
		///
		reference at(const size_type& x, const size_type& y)
		{
		return m_elements[index2DtoIndex1D(x,y)];
		}

		///
		/// \brief		Returns a read-onlf reference to the item with the
		///				specified x and y position in the array
		/// \param x
		/// \param y
		/// \return
		/// \since		28-09-2015
		/// \author		Dean
		///
		const_reference at(const size_type &x, const size_type &y) const
		{
		return m_elements[index2DtoIndex1D(x, y)];
		}

	private:
		//	Allow non-member operator to access our data
		friend std::ostream& operator<< <T>(std::ostream&, const Vector2&);
		//friend std::istream& operator>> <T, X_SIZE, Y_SIZE>(std::istream&, Array2&);
	};

	///
	///	\brief	Allows the class to be output to a standard iostream
	///	\author	Dean
	///	\since	25-08-2015
	///
	template <typename T>
	std::ostream& operator<<(std::ostream& os, const Vector2<T>& obj)
	{
	for (auto it = obj.m_elements.cbegin(); it != obj.m_elements.cend(); ++it)
	{
		os << *it << " ";
	}
	return os;
	}
}

#endif // VECTOR_2D_H

