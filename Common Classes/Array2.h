#ifndef ARRAY_2_H
#define ARRAY_2_H

#include <array>
#include <cstdlib>
#include <cassert>
#include <string>
#include <sstream>
#include <iomanip>
#include <iostream>

//		Forward declaration of our template class
template<typename T, size_t X_SIZE, size_t Y_SIZE>
class Array2;

//		Forward declaration of our stream operators
template<typename T, size_t X_SIZE, size_t Y_SIZE>
std::ostream& operator<<(std::ostream&, const Array2<T, X_SIZE, Y_SIZE>&);

//template<typename T, size_t X_SIZE, size_t Y_SIZE>
//std::istream& operator>>(std::istream&, Array2d<T, X_SIZE, Y_SIZE>&);

//		Actual template declaration
template <typename T, size_t X_SIZE, size_t Y_SIZE>
class Array2
{
	private:
		std::array<T, X_SIZE * Y_SIZE> m_elements;

		size_t index2DtoIndex1D(const size_t& x, const size_t& y) const
		{
			assert(x < X_SIZE);
			assert(y < Y_SIZE);

			return x + X_SIZE * y;
		}
	public:
		size_t Size() const { return m_elements.size(); }
		size_t SizeX() const { return X_SIZE; }
		size_t SizeY() const { return Y_SIZE; }

		///
		/// \brief	Returns the array as a formatted string
		///	\author	Dean
		///	\since	25-08-2015
		///
		std::string ToString() const
		{
			std::stringstream result;

			for (size_t y = 0; y < Y_SIZE; ++y)
			{
				for (size_t x = 0; x < X_SIZE; ++x)
				{
					result << std::setw(12) << std::setprecision(6);
					result << m_elements[index2DtoIndex1D(x, y)];

					//	Tab between values
					if (x != X_SIZE - 1)
						result << "\t";
				}
				result << std::endl;
			}

			return result.str();
		}

	public:
		//	Constructor, destructors, operators

		///
		/// \brief	Default constructor
		///	\author	Dean
		///	\since	25-08-2015
		///
		Array2()
		{ }

		///
		/// \brief	Constructor that fills an array with the specified value
		///	\author	Dean
		///	\since	25-08-2015
		///
		Array2(const T& value)
		{
			m_elements.fill(value);
		}

		///
		/// \brief	Constructor that takes a number of values enclodes in braces
		///	\author	Dean
		///	\since	25-08-2015
		///
		Array2(std::initializer_list<T> values)
		{
			//	Check the number of values matches the number of elements in
			//	our array
			assert(values.size() == Size());

			//	Copy values from the iterator list to the member array
			for(auto it1 = m_elements.begin(), it2 = values.begin();
				it2 != values.end(); ++it1, ++it2)
			{
				*it1 = *it2;
			}
		}

		///
		/// \brief	Constructor that takes a number of values from a std::vector
		///	\author	Dean
		///	\since	23-09-2015
		///
		Array2(const std::vector<T>& values)
		{
			//	Check the number of values matches the number of elements in
			//	our array
			assert(values.size() == Size());

			//	Copy values from the iterator list to the member vector
			for(auto it1 = m_elements.begin(), it2 = values.begin();
				it2 != values.end(); ++it1, ++it2)
			{
				*it1 = *it2;
			}
		}

		///
		///	\brief	Destructor
		///	\author	Dean
		///	\since	25-08-2015
		///
		~Array2()
		{ }

		///
		/// \brief	Copy constructor
		/// \param rhs
		///	\author	Dean
		///	\since	25-08-2015
		///
		Array2(const Array2& rhs)
		{
			for (size_t i = 0; i < rhs.m_elements.size(); ++i)
			{
				m_elements[i] = rhs.m_elements[i];
			}
		}

		///
		/// \brief	Move constructor
		/// \param rhs
		///	\author	Dean
		///	\since	25-08-2015
		///
		Array2(Array2&& rhs)
		{
			m_elements = rhs.m_elements;
			rhs.m_elements.clear();
		}

		///
		/// \brief	Copy assignment
		/// \param rhs
		/// \return
		///	\author	Dean
		///	\since	25-08-2015
		///
		Array2& operator=(const Array2& rhs)
		{
			//	Check for self-assigment
			if (this != &rhs)
			{
				m_elements = rhs.m_elements;
			}
			return *this;
		}

		///
		/// \brief	Move assignment
		/// \param rhs
		/// \return
		///	\author	Dean
		///	\since	25-08-2015
		///
		Array2& operator=(Array2&& rhs)
		{
			assert(this != &rhs);

			m_elements = rhs.m_elements;
			//rhs.m_elements.clear();

			return *this;
		}
	private:
		//	Allow non-member operator to access our data
		friend std::ostream& operator<< <T, X_SIZE, Y_SIZE>(std::ostream&, const FixedArray2d&);
		//friend std::istream& operator>> <T, X_SIZE, Y_SIZE>(std::istream&, FixedArray2d&);
};

///
///	\brief	Allows the class to be output to a standard iostream
///	\author	Dean
///	\since	25-08-2015
///
template <typename T, size_t X_SIZE, size_t Y_SIZE>
std::ostream& operator<<(std::ostream& os, const Array2<T, X_SIZE, Y_SIZE>& obj)
{
	for (auto it = obj.m_elements.cbegin(); it != obj.m_elements.cend(); ++it)
	{
		os << *it << " ";
	}
	return os;
}


#endif // ARRAY_2D_H

