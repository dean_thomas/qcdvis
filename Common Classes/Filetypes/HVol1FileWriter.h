#ifndef HVOL1FILEWRITER_H
#define HVOL1FILEWRITER_H

#include <Vector4.h>
#include <string>

class HVol1FileWriter
{
	using Array4f = Storage::Vector4<double>;
	using size_type = size_t;

	//const std::string EXT = "hvol1";
	const char TAB = '\t';

	std::string m_ensembleName;
	std::string m_configurationName;
	std::string m_fieldName;
	size_type m_cools;
    public:
	///
	/// \brief HVol1FileWriter
	/// \param ensembleName
	/// \param fieldName
	/// \param configurationName
	/// \param cools
	/// \since	19-11-2015
	/// \author	Dean
	///
	HVol1FileWriter(const std::string& ensembleName,
			const std::string& fieldName,
			const std::string& configurationName,
			const size_type& cools)
	{
	    m_ensembleName = ensembleName;
	    m_fieldName = fieldName;
	    m_configurationName = configurationName;
	    m_cools = cools;
	}

	///
	/// \brief operator ()
	/// \param filename
	/// \param data
	/// \return
	/// \since	19-11-2015
	/// \author	Dean
	///
	bool operator()(const std::string& filename, const Array4f& data)
	{
	    return WriteFile(filename, data);
	}

	///
	/// \brief WriteFile
	/// \param filename
	/// \param data
	/// \return
	/// \since	19-11-2015
	/// \author	Dean
	///
	bool WriteFile(const std::string& filename, const Array4f& data)
	{
	    using namespace std;

	    ofstream outFile(filename);
	    if(outFile.is_open())
	    {
		//	Compute stats
		auto minVal = (*min_element(data.cbegin(), data.cend()));
		auto maxVal = (*max_element(data.cbegin(), data.cend()));
		auto sum = accumulate(data.cbegin(), data.cend(), 0.0);
		auto average = sum / data.size();

		outFile << m_ensembleName << endl;
		outFile << m_configurationName << endl;

		outFile << m_cools << endl;

		outFile << m_fieldName << endl;

		//	Example: 12 12 12 24
		outFile << data.size_x() << " ";
		outFile << data.size_y() << " ";
		outFile << data.size_z() << " ";
		outFile << data.size_w() << endl;
		outFile << endl;


		outFile << setprecision(20) << minVal << endl;
		outFile << setprecision(20) << maxVal << endl;
		outFile << setprecision(20) << sum << endl;
		outFile << setprecision(20) << average << endl;

		//	Data - time step by time step
		for (auto t = 0; t < data.size_w(); ++t)
		{
		    outFile << "#" << TAB << "timestep " << t << endl;
		    outFile << endl;

		    for (auto x = 0; x < data.size_x(); ++x)
		    {
			for (auto y = 0; y < data.size_y(); ++y)
			{
			    for (auto z = 0; z < data.size_z(); ++z)
			    {
				outFile << setw(4) << x << setw(4) << y;
				outFile << setw(4) << z << setw(4) << t;
				outFile << "\t" << setw(24) << setprecision(20)
					<< data.at(x, y, z, t);
				outFile << endl;
			    }
			}
		    }
		    outFile << endl;
		}
		outFile.close();

		return true;
	    }
	    else return false;
	}
};

#endif // HVOL1FILEWRITER_H
