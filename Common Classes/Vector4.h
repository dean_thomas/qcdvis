#ifndef ARRAY_4_H
#define ARRAY_4_H

#include <string>
#include <sstream>
#include <iostream>
#include <iomanip>
#include <cstdlib>
#include <cstdio>
#include <cstring>
#include <cassert>
#include <vector>

//#define MSVC_2013

//  Work around for VS2013
#if defined(_MSC_VER) && (_MSC_VER < 1900)
#define noexcept _NOEXCEPT
#endif

namespace Storage
{
    template <typename T>
    class Vector4
    {
	private:
	    using container_type = std::vector<T>;

	public:
	    using size_type = typename container_type::size_type;
	    using reference = typename container_type::reference;
	    using const_reference = typename container_type::const_reference;

	    //	Aliases for container iterators
	    using iterator = typename container_type::iterator;
	    using const_iterator = typename container_type::const_iterator;
	    using reverse_iterator = typename container_type::reverse_iterator;
	    using const_reverse_iterator = typename container_type::const_reverse_iterator;

	    struct Index4
	    {
		    size_type x;
		    size_type y;
		    size_type z;
		    size_type t;
	    };

	    //	Iterator pass-through
	    iterator begin() { return m_elements.begin(); }
	    iterator end() { return m_elements.end(); }
	    const_iterator begin() const { return m_elements.begin(); }
	    const_iterator end() const { return m_elements.end(); }

	    const_iterator cbegin() const { return m_elements.cbegin(); }
	    const_iterator cend() const { return m_elements.cend(); }

	    //	Reverse Iterator pass-through
	    reverse_iterator rbegin() { return m_elements.rbegin(); }
	    reverse_iterator rend() { return m_elements.rend(); }
	    const_reverse_iterator crbegin() { return m_elements.crbegin(); }
	    const_reverse_iterator crend() { return m_elements.crend(); }

	private:
	    container_type m_elements;

	    size_type m_xSize = 0;
	    size_type m_ySize = 0;
	    size_type m_zSize = 0;
	    size_type m_wSize = 0;

	    //  Compute data partitioning
	    size_type rowSize() const { return m_zSize; }
	    size_type planeSize() const { return m_zSize * m_ySize; }
	    size_type volSize() const { return m_zSize * m_ySize * m_xSize; }
	public:
	    //	Nested Container function pass-through
	    size_type size() const noexcept { return m_elements.size(); }

	    size_type index4DtoIndex1D(const size_type& x,
				       const size_type& y,
				       const size_type& z,
				       const size_type& w) const
	    {
		assert((x < m_xSize) && (y < m_ySize)
		       && (z < m_zSize) && (w < m_wSize));

		return (z + (rowSize() * y) + (planeSize() * x) + (volSize() * w));
	    }

	    size_type size_x() const { return m_xSize; }
	    size_type size_y() const { return m_ySize; }
	    size_type size_z() const { return m_zSize; }
	    size_type size_w() const { return m_wSize; }

	    ///
	    /// \brief		Returns a writeable reference the element at the
	    ///				specified index
	    /// \param		index
	    /// \return
	    /// \since		23-10-2015
	    /// \author		Dean
	    ///
	    reference operator[](const Index4& index)
	    {
		return m_elements[index4DtoIndex1D(index.x, index.y, index.z, index.t)];
	    }

	    ///
	    /// \brief		Returns a read-only reference the element at the
	    ///				specified index
	    /// \param		index
	    /// \return
	    /// \since		23-10-2015
	    /// \author		Dean
	    ///
	    const_reference operator[](const Index4& index) const
	    {
		return m_elements[index4DtoIndex1D(index.x, index.y, index.z, index.t)];
	    }

	    ///
	    /// \brief	Resizes the array to the specified dimensions
	    ///	\author	Dean
	    ///	\since	23-10-2015
	    ///
	    void resize(const size_type &xSize,
			const size_type &ySize,
			const size_type &zSize,
			const size_type& wSize)
	    {
		m_xSize = xSize;
		m_ySize = ySize;
		m_zSize = zSize;
		m_wSize = wSize;

		m_elements.resize(xSize * ySize * zSize * wSize);
	    }

	    ///
	    /// \brief	Default constructor (creates an empty 1x1x1x1 array)
	    /// \author	Dean
	    /// \since	23-10-2015
	    ///
	    Vector4()
		: Vector4(1, 1, 1, 1)
	    {

	    }

	    ///
	    /// \brief	Constructor that creates an empty xSize by ySize array)
	    /// \author	Dean
	    /// \since	23-10-2015
	    ///
	    Vector4(const size_type& xSize,
		    const size_type& ySize,
		    const size_type& zSize,
		    const size_type& wSize)
		: m_xSize{xSize}, m_ySize{ySize}, m_zSize{zSize}, m_wSize{wSize}
	    {
		std::cout << __FUNCTION__ << std::endl;

		//	Reserve space (creating empty elements)
		auto count = xSize * ySize * zSize * wSize;
		m_elements.resize(count);
	    }

	    ///
	    /// \brief	Constructor that fills an array with the specified value
	    /// \author	Dean
	    /// \since	23-10-2015
	    ///
	    Vector4(const size_type &xSize,
		    const size_type &ySize,
		    const size_type &zSize,
		    const size_type &wSize,
		    const T& value)
		: Vector4(xSize, ySize, zSize, wSize)
	    {
		//	Clone the data
		for(auto it = m_elements.begin(); it != m_elements.end(); ++it)
		{
		    *it = value;
		}
	    }

	    ///
	    /// \brief	Constructor that takes a number of values enclodes in braces
	    /// \author	Dean
	    /// \since	23-10-2015
	    ///
	    Vector4(const size_type &xSize,
		    const size_type &ySize,
		    const size_type &zSize,
		    const size_type &wSize,
		    std::initializer_list<T> values)
		: Vector4(xSize, ySize, zSize, wSize)
	    {
		//	Check the number of values matches the number of elements in
		//	our array
		assert(values.size() == size());

		//	Copy values from the iterator list to the member vector
		std::copy(values.cbegin(), values.cend(), m_elements.begin());
	    }

	    ///
	    /// \brief	Constructor that takes a number of values from a std::vector
	    ///	\author	Dean
	    ///	\since	23-09-2015
	    ///
	    Vector4(const size_type &xSize,
		    const size_type &ySize,
		    const size_type &zSize,
		    const size_type &wSize,
		    const std::vector<T>& values)
		: Vector4(xSize , ySize, zSize, wSize)
	    {
		//	Check the number of values matches the number of elements in
		//	our array
		assert(values.size() == size());

		//	Copy values from the iterator list to the member vector
		std::copy(values.cbegin(), values.cend(), m_elements.begin());
	    }

	    ///
	    /// \brief	Copy constructor
	    /// \param rhs
	    /// \author	Dean
	    /// \since	23-10-2015
	    ///
	    Vector4(const Vector4& rhs)
		: Vector4(rhs.m_xSize , rhs.m_ySize, rhs.m_zSize, rhs.m_wSize)
	    {
		//	Check the number of values matches the number of elements in
		//	our array
		assert(rhs.size() == size());

		//	Copy values from the iterator list to the member vector
		for (size_t i = 0; i < rhs.m_elements.size(); ++i)
		{
		    m_elements[i] = rhs.m_elements[i];
		}
	    }

	    ///
	    /// \brief	Move constructor
	    /// \param rhs
	    /// \author	Dean
	    /// \since	23-10-2015
	    ///
	    Vector4(Vector4&& rhs)
		: Vector4(rhs.m_xSize , rhs.m_ySize, rhs.m_zSize, rhs.m_wSize)
	    {
		m_elements = rhs.m_elements;
		rhs.m_elements.clear();
	    }

	    ~Vector4() { }

	    ///
	    /// \brief	Copy assignment
	    /// \param rhs
	    /// \return
	    /// \author	Dean
	    /// \since	23-10-2015
	    ///
	    Vector4& operator=(const Vector4& rhs)
	    {
		//	Check for self-assigment
		if (this != &rhs)
		{
		    m_xSize = rhs.m_xSize;
		    m_ySize = rhs.m_ySize;
		    m_zSize = rhs.m_zSize;
		    m_wSize = rhs.m_wSize;
		    m_elements = rhs.m_elements;
		}
		return *this;
	    }

	    ///
	    /// \brief	Move assignment
	    /// \param rhs
	    /// \return
	    /// \author	Dean
	    /// \since	23-10-2015
	    ///
	    Vector4& operator=(Vector4&& rhs)
	    {
		assert(this != &rhs);

		m_xSize = rhs.m_xSize;
		m_ySize = rhs.m_ySize;
		m_zSize = rhs.m_zSize;
		m_wSize = rhs.m_wSize;
		m_elements = rhs.m_elements;
		//rhs.m_elements.clear();

		return *this;
	    }
	    //size_type nElements = 0;

	    //  Number of elements in Z, ZY and ZYX constructs
	    //size_type m_rowSize = 0;
	    //size_type m_planeSize = 0;
	    //size_type m_volSize = 0;

	    //T dummy;

	    //unsigned long m_translationX;
	    //unsigned long m_translationY;
	    //unsigned long m_translationZ;
	    //unsigned long m_translationW;

	public:
	    //unsigned long GetTranslationX() const { return m_translationX; }
	    //unsigned long GetTranslationY() const { return m_translationY; }
	    //unsigned long GetTranslationZ() const { return m_translationZ; }
	    //unsigned long GetTranslationW() const { return m_translationW; }
	    ///
	    /// \brief		Returns a reference to the object with the specified
	    ///				x, y, z and w indices
	    /// \param x
	    /// \param y
	    /// \param z
	    /// \return
	    /// \since		22-10-2015
	    /// \author		Dean
	    ///
	    reference at(const size_type& x,
			 const size_type& y,
			 const size_type& z,
			 const size_type& w)
	    {
		assert (x < m_xSize);
		assert (y < m_ySize);
		assert (z < m_zSize);
		assert (w < m_wSize);

		return m_elements[index4DtoIndex1D(x, y, z, w)];
	    }

	    ///
	    /// \brief		Returns a reference to the object with the specified
	    ///				x, y, z and w indices
	    /// \param x
	    /// \param y
	    /// \param z
	    /// \return
	    /// \since		22-10-2015
	    /// \author		Dean
	    ///
	    const_reference at(const size_type& x,
			       const size_type& y,
			       const size_type& z,
			       const size_type& w) const
	    {
		assert (x < m_xSize);
		assert (y < m_ySize);
		assert (z < m_zSize);
		assert (w < m_wSize);

		return m_elements[index4DtoIndex1D(x, y, z, w)];
	    }


	    /*
	void SetValue(const unsigned long &x,
		      const unsigned long &y,
		      const unsigned long &z,
		      const unsigned long &w,
		      const T &value)
	{
	    assert((x < m_xSize) && (y < m_ySize)
		   && (z < m_zSize) && (w < m_wSize));

	    using std::cerr;
	    using std::endl;

	    //	i.e. not constructed yet
	    if (m_value == nullptr)
	    {
		cerr << "Attempt to access element of uninitialized array.";
		cerr << endl;
	    }

	    //	compute address and set value
	    m_value[index4DtoIndex1D(x,y,z,w)] = value;
	}
	*/

	    size_type GetLargestDim() const
	    {
		unsigned long temp1, temp2, result;

		temp1 = (m_xSize > m_ySize ? m_xSize : m_ySize);
		temp2 = (m_zSize > m_wSize ? m_zSize : m_wSize);
		result = (temp1 > temp2 ? temp1 : temp2);

		return result;
	    }

	    std::string ToString() const
	    {
		std::stringstream result;

		const char DELIMITER = ' ';

		for (auto w = 0; w < m_wSize; ++w)
		{
		    for (auto z = 0; z < m_zSize; ++z)
		    {
			result << "w = " << w << " : " << "z = " << z << std::endl;

			for (auto y = 0; y < m_ySize; ++y)
			{
			    for (auto x = 0; x < m_xSize; ++x)
			    {
				result << std::setw(12) << std::setprecision(6);
				result << m_elements[index4DtoIndex1D(x, y, z, w)] << DELIMITER;
			    }
			    result << std::endl;
			}
		    }
		}
		return result.str();
	    }

	    /*
    ///
    /// \brief		Shifts the entire lattice across in the X direction
    /// \return		unsigned long: the number of units traslated from zero.
    ///	\author		Dean
    /// \since		30-01-2015
    ///
    unsigned long TranslatePosX()
    {
	ElementType temp[m_yDim][m_zDim];

	//	Copy the YZ plane on the upper limit of X
	for (unsigned long y = 0; y < m_yDim; ++y)
	{
	    for (unsigned long z = 0; z < m_zDim; ++z)
	    {
		temp[y][z] = m_value[index3DtoIndex1D(m_xDim-1, y, z)];
	    }
	}

	//	Shift columns across
	for (long x = m_xDim-2; x >= 0; --x)
	{
	    for (unsigned long y = 0; y < m_yDim; ++y)
	    {
		for (unsigned long z = 0; z < m_zDim; ++z)
		{
		    m_value[index3DtoIndex1D(x + 1, y, z)]
			    = m_value[index3DtoIndex1D(x, y, z)];
		}
	    }
	}

	//	Copy temp storage to the 0th column
	for (unsigned long y = 0; y < m_yDim; ++y)
	{
	    for (unsigned long z = 0; z < m_zDim; ++z)
	    {
		m_value[index3DtoIndex1D(0, y, z)] = temp[y][z];
	    }
	}

	//	Update the shift count
	if (m_translationX == m_xDim - 1)
	{
	    m_translationX = 0;
	}
	else
	{
	    ++m_translationX;
	}

	return m_translationX;
    }

    ///
    /// \brief		Shifts the entire lattice across in the X direction
    /// \return		unsigned long: the number of units traslated from zero.
    ///	\author		Dean
    /// \since		30-01-2015
    ///
    unsigned long TranslateNegX()
    {
	ElementType temp[m_yDim][m_zDim];

	//	Copy the Y and Z values of the lower limit on the X axis
	for (unsigned long y = 0; y < m_yDim; ++y)
	{
	    for (unsigned long z = 0; z < m_zDim; ++z)
	    {
		temp[y][z] = m_value[index3DtoIndex1D(0, y, z)];
	    }
	}

	//	Shift columns across
	for (unsigned long x = 1; x < m_xDim; ++x)
	{
	    for (unsigned long y = 0; y < m_yDim; ++y)
	    {
		for (unsigned long z = 0; z < m_zDim; ++z)
		{
		    m_value[index3DtoIndex1D(x - 1, y, z)]
			    = m_value[index3DtoIndex1D(x, y, z)];
		}
	    }
	}

	//	Copy temp storage to the last column
	for (unsigned long y = 0; y < m_yDim; ++y)
	{
	    for (unsigned long z = 0; z < m_zDim; ++z)
	    {
		m_value[index3DtoIndex1D(m_xDim-1, y, z)] = temp[y][z];
	    }
	}

	//	Update the shift count
	if (m_translationX == 0)
	{
	    m_translationX = m_xDim - 1;
	}
	else
	{
	    --m_translationX;
	}

	return m_translationX;
    }

    ///
    /// \brief		Shifts the entire lattice across in the Y direction
    /// \return		unsigned long: the number of units traslated from zero.
    ///	\author		Dean
    /// \since		30-01-2015
    ///
    unsigned long TranslatePosY()
    {
	Real temp[m_xDim][m_zDim];

	//	Copy the Y and Z values of the upper limit on the X axis
	for (unsigned long x = 0; x < m_xDim; ++x)
	{
	    for (unsigned long z = 0; z < m_zDim; ++z)
	    {
		temp[x][z] = m_value[index3DtoIndex1D(x, m_yDim-1, z)];
	    }
	}

	//	Shift columns across
	for (unsigned long x = 0; x < m_xDim; ++x)
	{
	    for (long y = m_yDim-2; y >= 0; --y)
	    {
		for (unsigned long z = 0; z < m_zDim; ++z)
		{
		    m_value[index3DtoIndex1D(x, y + 1, z)]
			    = m_value[index3DtoIndex1D(x, y, z)];
		}
	    }
	}

	//	Copy temp storage to the 0th column
	for (unsigned long x = 0; x < m_xDim; ++x)
	{
	    for (unsigned long z = 0; z < m_zDim; ++z)
	    {
		m_value[index3DtoIndex1D(x, 0, z)] = temp[x][z];
	    }
	}

	//	Update the shift count
	if (m_translationY == m_yDim - 1)
	{
	    m_translationY = 0;
	}
	else
	{
	    ++m_translationY;
	}

	return m_translationY;
    }

    ///
    /// \brief		Shifts the entire lattice across in the Y direction
    /// \return		unsigned long: the number of units traslated from zero.
    ///	\author		Dean
    /// \since		30-01-2015
    ///
    unsigned long TranslateNegY()
    {
	Real temp[m_xDim][m_zDim];

	//	Copy the Y and Z values of the lower limit on the X axis
	for (unsigned long x = 0; x < m_xDim; ++x)
	{
	    for (unsigned long z = 0; z < m_zDim; ++z)
	    {
		temp[x][z] = m_value[index3DtoIndex1D(x, 0, z)];
	    }
	}

	//	Shift columns across
	for (unsigned long x = 0; x < m_xDim; ++x)
	{
	    for (unsigned long y = 1; y < m_yDim; ++y)
	    {
		for (unsigned long z = 0; z < m_zDim; ++z)
		{
		    m_value[index3DtoIndex1D(x, y - 1, z)]
			    = m_value[index3DtoIndex1D(x, y, z)];
		}
	    }
	}

	//	Copy temp storage to the last column
	for (unsigned long x = 0; x < m_xDim; ++x)
	{
	    for (unsigned long z = 0; z < m_zDim; ++z)
	    {
		m_value[index3DtoIndex1D(x, m_yDim-1, z)] = temp[x][z];
	    }
	}

	//	Update the shift count
	if (m_translationY == 0)
	{
	    m_translationY = m_yDim - 1;
	}
	else
	{
	    --m_translationY;
	}

	return m_translationY;
    }

    ///
    /// \brief		Shifts the entire lattice across in the Z direction
    /// \return		unsigned long: the number of units traslated from zero.
    ///	\author		Dean
    /// \since		30-01-2015
    ///
    unsigned long TranslatePosZ()
    {
	Real temp[m_xDim][m_yDim];

	//	Copy the Y and Z values of the upper limit on the X axis
	for (unsigned long x = 0; x < m_xDim; ++x)
	{
	    for (unsigned long y = 0; y < m_yDim; ++y)
	    {
		temp[x][y] = m_value[index3DtoIndex1D(x, y, m_zDim-1)];
	    }
	}

	//	Shift columns across
	for (unsigned long x = 0; x < m_xDim; ++x)
	{
	    for (unsigned long y = 0; y < m_yDim; ++y)
	    {
		for (long z = m_zDim-2; z >= 0; --z)
		{
		    m_value[index3DtoIndex1D(x, y, z + 1)]
			    = m_value[index3DtoIndex1D(x, y, z)];
		}
	    }
	}

	//	Copy temp storage to the 0th column
	for (unsigned long x = 0; x < m_xDim; ++x)
	{
	    for (unsigned long y = 0; y < m_yDim; ++y)
	    {
		m_value[index3DtoIndex1D(x, y, 0)] = temp[x][y];
	    }
	}

	//	Update the shift count
	if (m_translationZ == m_zDim - 1)
	{
	    m_translationZ = 0;
	}
	else
	{
	    ++m_translationZ;
	}

	return m_translationZ;
    }

    ///
    /// \brief		Shifts the entire lattice across in the Z direction
    /// \return		unsigned long: the number of units traslated from zero.
    ///	\author		Dean
    /// \since		30-01-2015
    ///
    unsigned long TranslateNegZ()
    {
	Real temp[m_xDim][m_yDim];

	//	Copy the Y and Z values of the lower limit on the X axis
	for (unsigned long x = 0; x < m_xDim; ++x)
	{
	    for (unsigned long y = 0; y < m_yDim; ++y)
	    {
		temp[x][y] = m_value[index3DtoIndex1D(x, y, 0)];
	    }
	}

	//	Shift columns across
	for (unsigned long x = 0; x < m_xDim; ++x)
	{
	    for (unsigned long y = 0; y < m_yDim; ++y)
	    {
		for (unsigned long z = 1; z < m_zDim; ++z)
		{
		    m_value[index3DtoIndex1D(x, y, z - 1)]
			    = m_value[index3DtoIndex1D(x, y, z)];
		}
	    }
	}

	//	Copy temp storage to the last column
	for (unsigned long x = 0; x < m_xDim; ++x)
	{
	    for (unsigned long y = 0; y < m_yDim; ++y)
	    {
		m_value[index3DtoIndex1D(x, y, m_zDim-1)] = temp[x][y];
	    }
	}

	//	Update the shift count
	if (m_translationZ == 0)
	{
	    m_translationZ = m_zDim - 1;
	}
	else
	{
	    --m_translationZ;
	}

	return m_translationZ;
    }

    void SetTranslation(const unsigned long x,
			const unsigned long y,
			const unsigned long z)
    {
	if ((x >= m_xDim)
		|| (y >= m_yDim)
		|| (z >= m_zDim))
	{
	    //	Not quite true (more accurate would be InvalidVectorException
	    throw InvalidCoordinateException(x, y, z,
					     __func__, __FILE__, __LINE__);
	}
	else
	{
	    //	Set X translation
	    if (x > m_translationX)
	    {
		while (m_translationX != x)
		    TranslateNegX();
	    }
	    else if (x < m_translationX)
	    {
		while (m_translationX != x)
		    TranslatePosX();
	    }

	    //	Set Y translation
	    if (y > m_translationY)
	    {
		while (m_translationY != y)
		    TranslateNegY();
	    }
	    else if (y < m_translationY)
	    {
		while (m_translationY != y)
		    TranslatePosY();
	    }

	    //	Set Z translation
	    if (z > m_translationZ)
	    {
		while (m_translationZ != z)
		    TranslateNegZ();
	    }
	    else if (z < m_translationZ)
	    {
		while (m_translationZ != z)
		    TranslatePosZ();
	    }
	}
    }
    */
    };
}

#endif
