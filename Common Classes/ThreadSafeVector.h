#ifndef THREAD_SAFE_VECTOR_H
#define THREAD_SAFE_VECTOR_H

///		/brief		Most methods are directly inherited from std::vector
///					but with a mutex lock applied where necessary.
///		/author		Dean
///		/since		28-07-2015
///
///


#include <vector>
#include <mutex>

using std::vector;
using std::mutex;
using std::lock_guard;

template <class T>
class ThreadSafeVector
{
		//	'Import' these from our templated vector
typedef typename vector<T>::size_type size_type;
typedef typename vector<T>::reference reference;
typedef typename vector<T>::const_reference const_reference;
typedef typename vector<T>::iterator iterator;
typedef typename vector<T>::const_iterator const_iterator;

private:
	mutex m_mutex;
	vector<T> m_vector;

public:
	//	Note: much of the functionality of a std::vector will still need to be
	//	re-enabled (ie constructors etc.)
	iterator begin() { return m_vector.begin(); }
	const_iterator begin() const { return m_vector.begin(); }

	iterator erase(iterator first, iterator last)
	{
		//	Lock the mutex
		lock_guard<mutex> lock(m_mutex);

		//	Pop the last item
		m_vector.erase(first, last);

		return last + 1;
		//	Mutex is automatically unlocked on
		//	destruction of the lock
	}

	iterator erase(iterator position)
	{
		//	Lock the mutex
		lock_guard<mutex> lock(m_mutex);

		//	Pop the last item
		m_vector.erase(position);

		//	Mutex is automatically unlocked on
		//	destruction of the lock
	}

	void pop_back()
	{
		//	Lock the mutex
		lock_guard<mutex> lock(m_mutex);

		//	Pop the last item
		m_vector.pop_back();

		//	Mutex is automatically unlocked on
		//	destruction of the lock
	}

	bool empty() const { return m_vector.empty(); }

	ThreadSafeVector(const ThreadSafeVector& v)
	{
		//	Lock the mutex
		lock_guard<mutex> lock(m_mutex);

		//	Set the number of elements and copy across
		m_vector.resize(v.size());
		for (size_type i = 0; i < v.size(); ++i)
		{
			m_vector[i] = v[i];
		}

		//	Mutex is automatically unlocked on
		//	destruction of the lock
	}

	void reserve(size_type n)
	{
		//	Lock the mutex
		lock_guard<mutex> lock(m_mutex);

		//	Add the data to the vector
		m_vector.reserve(n);

		//	Mutex is automatically unlocked on
		//	destruction of the lock
	}

	size_type size() const
	{
		return m_vector.size();
	}

	reference back()
	{
		//	Lock the mutex
		lock_guard<mutex> lock(m_mutex);

		//	Add the data to the vector
		return m_vector.back();

		//	Mutex is automatically unlocked on
		//	destruction of the lock
	}

	reference front()
	{
		//	Lock the mutex
		lock_guard<mutex> lock(m_mutex);

		//	Add the data to the vector
		return m_vector.front();

		//	Mutex is automatically unlocked on
		//	destruction of the lock
	}

	reference at(size_type n)
	{
		//	Lock the mutex
		lock_guard<mutex> lock(m_mutex);

		//	Add the data to the vector
		return m_vector.at(n);

		//	Mutex is automatically unlocked on
		//	destruction of the lock
	}

	reference operator[] (size_type n)
	{
		//	Lock the mutex
		lock_guard<mutex> lock(m_mutex);

		//	Add the data to the vector
		return m_vector.operator[](n);

		//	Mutex is automatically unlocked on
		//	destruction of the lock
	}

	const_reference operator[] (size_type n) const
	{
		//	Lock the mutex
		//lock_guard<mutex> lock(m_mutex);

		//	Add the data to the vector
		return m_vector.operator[](n);

		//	Mutex is automatically unlocked on
		//	destruction of the lock
	}

	void push_back(const T &val)
	{
		//	Lock the mutex
		lock_guard<mutex> lock(m_mutex);

		//	Add the data to the vector
		m_vector.push_back(val);

		//	Mutex is automatically unlocked on
		//	destruction of the lock
	}

	ThreadSafeVector()
	{

	}

	~ThreadSafeVector()
	{

	}
};

#endif //THREAD_SAFE_VECTOR_H
