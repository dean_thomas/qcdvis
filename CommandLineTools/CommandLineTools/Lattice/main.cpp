//	Uncomment to pause on exit, even in release mode
#define PAUSE_ON_EXIT

#include <Common.h>
#include <DataModel.h>
#include <iostream>
#include <Filetypes\Vol1.h>
#include <Filetypes\HVol1FileWriter.h>
#include <regex>
#include <exception>
#include <cstdlib>

using namespace std;

bool make_dir(const string& path)
{
	auto command = "mkdir \"" + path + "\"";

	auto result = system(command.c_str());

	if (result == 0)
	{
		cout << "Made directory at: " << path << endl;
	}
	return (result == 0);
}

void print_usage()
{
	const auto TAB = "  ";
	cout << "Usage:" << endl << endl;
	cout << "Lattice <filename> <output root dir> <output field 1> [output field 2] ... [output field n]" << endl;
	cout << TAB << "<output root dir> is the output root directory" << endl;
	cout << TAB << "[output field n] is any one or more from the following:" << endl;
	cout << TAB << TAB << "poly : " << "compute the polyakov field in R^3." << endl;
	cout << TAB << TAB << "sp : " << "compute the space-like plaquette field in R^4." << endl;
	cout << TAB << TAB << "tp : " << "compute the time-like plaquette field in R^4." << endl;
	cout << TAB << TAB << "dp : " << "compute the difference plaquette field in R^4." << endl;
	cout << TAB << TAB << "ap : " << "compute the average plaquette field in R^4." << endl;
	cout << TAB << TAB << "tcd : " << "compute the topological charge density field in R^4." << endl;
	cout << endl;
}

struct EnsembleProperties
{
	size_t dim_xyz;
	size_t dim_t;
	size_t mu;
	std::string ensemble;
	std::string configuration;
	size_t cooling_slice;
};

string generate_output_path(const string& root,
							const size_t dim_xyz,
							const size_t dim_t,
							const size_t mu,
							const string& ensemble,
							const string& configuration,
							const size_t cools)
{
	//	Lazy conversion of number padded to length 4 with leading zeroes
	auto zero_padded = [&](const size_t input)
	{
		if (input < 10)
		{
			return string("000") + to_string(input);
		}
		else if (input < 100)
		{
			return string("00") + to_string(input);
		}
		else if (input < 1000)
		{
			return string("0") + to_string(input);
		}
		else return to_string(input);
	};

	return root + "/"
		+ to_string(dim_xyz) + "x" + to_string(dim_t) + "/"
		+ "mu" + zero_padded(mu) + "/"
		+ ensemble + "/"
		+ configuration + "/"
		+ "cool" + zero_padded(cools) + "/";
}

EnsembleProperties extract_ensemble_settings(const std::string& absolute_path)
{
	//	Examples: "conp0052", "conp0230", "conp9833"
	const regex confRegex(R"|(.*(conp\d{4}))|");
	smatch conf_smatch;

	//	Examples: "cool0000_conp0052", "cool0100_conp0332"
	//	We will capture the cooling number and configuration number
	const regex coolRegex(R"|(.*cool(\d{4})_conp(\d{4}))|");
	smatch cool_smatch;

	//	Try to extract the ensemble name
	const regex ensembleRegex(R"|(.*(config.b(\d+)k(\d+)mu(\d+)j(\d+)s(\d+)t(\d+)).*)|");
	smatch ensemble_smatch;

	cout << absolute_path << endl;

	if (!regex_match(absolute_path, cool_smatch, coolRegex))
	{
		throw invalid_argument("Could not parse cooling properties.");
	}
	if (!regex_match(absolute_path, conf_smatch, confRegex))
	{
		throw invalid_argument("Could not parse configuration properties.");
	}
	if (!regex_match(absolute_path, ensemble_smatch, ensembleRegex))
	{
		throw invalid_argument("Could not parse ensemble properties.");
	}

	return { stoul(ensemble_smatch[6]),
		stoul(ensemble_smatch[7]),
		stoul(ensemble_smatch[4]),
		ensemble_smatch[1],
		conf_smatch[1],
		stoul(cool_smatch[1]) };
}


bool do_polyakov_calculation(const DataModel& data_model, const string& output_root, const EnsembleProperties& ensemble_properties)
{
	cout << "Saving Polyakov field..." << endl;
	auto polyakov_field = data_model.GetPolyakovLoopField();
	auto output_path = generate_output_path(output_root,
											ensemble_properties.dim_xyz,
											ensemble_properties.dim_t,
											ensemble_properties.mu,
											ensemble_properties.ensemble,
											ensemble_properties.configuration,
											ensemble_properties.cooling_slice);
	//	Make an output directory (if it doesn't exist)
	make_dir(output_path);

	//	Set the output path to the new folder
	auto polyakov_file = output_path + "/" + "polyakov.vol1";
	Vol1Writer<double> vol_writer(ensemble_properties.ensemble,
								  string("Polyakov"),
								  ensemble_properties.configuration,
								  ensemble_properties.cooling_slice,
								  0,
								  polyakov_field);

	//	Report any write errors
	if (!vol_writer(polyakov_file))
	{
		cerr << "Unable to save to file: '" << polyakov_file << endl;
		return false;
	}

	//	Report success
	cout << "Saved file at: " << polyakov_file << "." << endl;
	return true;
}

bool do_topological_charge_density_calculation(const DataModel& data_model, const string& output_root, const EnsembleProperties& ensemble_properties)
{
	cout << "Saving Topological Charge Density field..." << endl;
	auto topological_charge_density_field = data_model.GetTopologicalChargeDensityField();
	auto output_path = generate_output_path(output_root,
											ensemble_properties.dim_xyz,
											ensemble_properties.dim_t,
											ensemble_properties.mu,
											ensemble_properties.ensemble,
											ensemble_properties.configuration,
											ensemble_properties.cooling_slice);
	//	Make an output directory (if it doesn't exist)
	make_dir(output_path);

	//	Set the output path to the new folder
	auto topological_charge_density_file = output_path + "/" + "Topological charge density.hvol1";
	HVol1FileWriter hvol_writer(ensemble_properties.ensemble,
								  string("Topological charge density"),
								  ensemble_properties.configuration,
								  ensemble_properties.cooling_slice);

	//	Report any write errors
	if (!hvol_writer(topological_charge_density_file, topological_charge_density_field))
	{
		cerr << "Unable to save to file: '" << topological_charge_density_file << endl;
		return false;
	}

	//	Report success
	cout << "Saved file at: " << topological_charge_density_file << "." << endl;
	return true;
}

bool do_space_like_calculation(const DataModel& data_model, const string& output_root, const EnsembleProperties& ensemble_properties)
{
	cout << "Saving Space-like Plaquette field..." << endl;
	auto field = data_model.GetSpaceLikePlaquetteField();
	auto output_path = generate_output_path(output_root,
											ensemble_properties.dim_xyz,
											ensemble_properties.dim_t,
											ensemble_properties.mu,
											ensemble_properties.ensemble,
											ensemble_properties.configuration,
											ensemble_properties.cooling_slice);
	//	Make an output directory (if it doesn't exist)
	make_dir(output_path);

	//	Set the output path to the new folder
	auto file = output_path + "/" + "spacelike plaquette.hvol1";
	HVol1FileWriter hvol_writer(ensemble_properties.ensemble,
								string("Spacelike Plaquette"),
								ensemble_properties.configuration,
								ensemble_properties.cooling_slice);

	//	Report any write errors
	if (!hvol_writer(file, field))
	{
		cerr << "Unable to save to file: '" << file << endl;
		return false;
	}

	//	Report success
	cout << "Saved file at: " << file << "." << endl;
	return true;
}

bool do_time_like_calculation(const DataModel& data_model, const string& output_root, const EnsembleProperties& ensemble_properties)
{
	cout << "Saving Time-like Plaquette field..." << endl;
	auto field = data_model.GetTimeLikePlaquetteField();
	auto output_path = generate_output_path(output_root,
											ensemble_properties.dim_xyz,
											ensemble_properties.dim_t,
											ensemble_properties.mu,
											ensemble_properties.ensemble,
											ensemble_properties.configuration,
											ensemble_properties.cooling_slice);
	//	Make an output directory (if it doesn't exist)
	make_dir(output_path);

	//	Set the output path to the new folder
	auto file = output_path + "/" + "timelike plaquette.hvol1";
	HVol1FileWriter hvol_writer(ensemble_properties.ensemble,
								string("Timelike Plaquette"),
								ensemble_properties.configuration,
								ensemble_properties.cooling_slice);

	//	Report any write errors
	if (!hvol_writer(file, field))
	{
		cerr << "Unable to save to file: '" << file << endl;
		return false;
	}

	//	Report success
	cout << "Saved file at: " << file << "." << endl;
	return true;
}

bool do_average_plaquette_calculation(const DataModel& data_model, const string& output_root, const EnsembleProperties& ensemble_properties)
{
	cout << "Saving Average Plaquette field..." << endl;
	auto field = data_model.GetAveragePlaquetteField();
	auto output_path = generate_output_path(output_root,
											ensemble_properties.dim_xyz,
											ensemble_properties.dim_t,
											ensemble_properties.mu,
											ensemble_properties.ensemble,
											ensemble_properties.configuration,
											ensemble_properties.cooling_slice);
	//	Make an output directory (if it doesn't exist)
	make_dir(output_path);

	//	Set the output path to the new folder
	auto file = output_path + "/" + "average plaquette.hvol1";
	HVol1FileWriter hvol_writer(ensemble_properties.ensemble,
								string("Average Plaquette"),
								ensemble_properties.configuration,
								ensemble_properties.cooling_slice);

	//	Report any write errors
	if (!hvol_writer(file, field))
	{
		cerr << "Unable to save to file: '" << file << endl;
		return false;
	}

	//	Report success
	cout << "Saved file at: " << file << "." << endl;
	return true;
}

bool do_difference_plaquette_calculation(const DataModel& data_model, const string& output_root, const EnsembleProperties& ensemble_properties)
{
	cout << "Saving Difference Plaquette field..." << endl;
	auto field = data_model.GetDifferencePlaquetteField();
	auto output_path = generate_output_path(output_root,
											ensemble_properties.dim_xyz,
											ensemble_properties.dim_t,
											ensemble_properties.mu,
											ensemble_properties.ensemble,
											ensemble_properties.configuration,
											ensemble_properties.cooling_slice);
	//	Make an output directory (if it doesn't exist)
	make_dir(output_path);

	//	Set the output path to the new folder
	auto file = output_path + "/" + "difference plaquette.hvol1";
	HVol1FileWriter hvol_writer(ensemble_properties.ensemble,
								string("Difference Plaquette"),
								ensemble_properties.configuration,
								ensemble_properties.cooling_slice);

	//	Report any write errors
	if (!hvol_writer(file, field))
	{
		cerr << "Unable to save to file: '" << file << endl;
		return false;
	}

	//	Report success
	cout << "Saved file at: " << file << "." << endl;
	return true;
}

int main(int argc, char* argv[])
{
	if (argc < 4)
	{
		print_usage();
		pause_before_exit();
		return EXIT_INVALID_ARGUMENTS;
	}

	//	Data structure
	ArgParser arguments(argc, argv);
	DataModel data_model;
	auto path_to_file = split_path_no_extension(argv[1]);
	auto output_root = string(argv[2]);

	cout << "Output dir: " << output_root << endl;

	//	Fields to be computed
	auto polyakov = arguments.contains("poly", false);
	auto space_like = arguments.contains("sp", false);
	auto time_like = arguments.contains("tp", false);
	auto average = arguments.contains("ap", false);
	auto difference = arguments.contains("dp", false);
	auto topological_charge_density = arguments.contains("tcd", false);

	//	Print configuration options
	cout << "Filename: " << path_to_file.filename_with_extension << endl;
	cout << "polyakov: " << boolalpha << polyakov << endl;
	cout << "space like: " << boolalpha << space_like << endl;
	cout << "time like: " << boolalpha << time_like << endl;
	cout << "difference: " << boolalpha << difference << endl;
	cout << "average: " << boolalpha << average << endl;

	//	Load the input lattice
	if (!data_model.LoadConfiguration(path_to_file.full_path))
	{
		cerr << "Unable to open file: '" << path_to_file.full_path << "'." << endl;
		pause_before_exit();
		return EXIT_INPUT_ERROR;
	}

	try
	{
		//	Parse the properties of the ensemble data
		auto ensemble_properties = extract_ensemble_settings(path_to_file.full_path);

		if (polyakov)
		{
			if (!do_polyakov_calculation(data_model, output_root, ensemble_properties))
			{
				pause_before_exit();
				return EXIT_OUTPUT_ERROR;
			}
		}

		if (topological_charge_density)
		{
			if (!do_topological_charge_density_calculation(data_model, output_root, ensemble_properties))
			{
				pause_before_exit();
				return EXIT_OUTPUT_ERROR;
			}
		}

		if (space_like)
		{
			if (!do_space_like_calculation(data_model, output_root, ensemble_properties))
			{
				pause_before_exit();
				return EXIT_OUTPUT_ERROR;
			}
		}

		if (time_like)
		{
			if (!do_time_like_calculation(data_model, output_root, ensemble_properties))
			{
				pause_before_exit();
				return EXIT_OUTPUT_ERROR;
			}
		}

		if (average)
		{
			if (!do_average_plaquette_calculation(data_model, output_root, ensemble_properties))
			{
				pause_before_exit();
				return EXIT_OUTPUT_ERROR;
			}
		}

		if (difference)
		{
			if (!do_difference_plaquette_calculation(data_model, output_root, ensemble_properties))
			{
				pause_before_exit();
				return EXIT_OUTPUT_ERROR;
			}
		}
	}
	catch (exception& ex)
	{
		cerr << "Unable to detect configuration properties: " << ex.what() << endl;
		pause_before_exit();
		return EXIT_INPUT_ERROR;
	}

	return EXIT_OK;
}