#include <Graphs/rb.h>
#include <Common.h>
#include <iostream>
#include <cassert>
#include <set>
#include <fstream>
#include <iomanip>
#include <cmath>

#include "rbh_format.h"

using namespace std;

vector<float> create_isovalue_sample_array(const float data_min, const float data_max, const size_t number_of_samples)
{
	assert(data_min < data_max);
	assert(number_of_samples != 0);

	vector<float> result;

	//	Compute the range, exit if the range is zero
	const auto range = data_max - data_min;
	if (range == 0.0f) return result;

	//	Compute the sample step size
	const auto step = range / (static_cast<float>(number_of_samples-1));

	//	Loop over the range inserting each value
	result.reserve(number_of_samples);
	for (auto i = data_min; i < data_max; i += step)
	{
		cout << i << endl;
		result.push_back(i);
	}

	if (result[result.size()-1] != data_max)
	{
		//	Guard against potential floating point error by resizing the last bin
		if (result.size() == number_of_samples) result.pop_back();

		cout << "Manually pushing last value due to floating point error." << endl;
		result.push_back(data_max);
		cout << data_max << endl;
	}
	cout << result.size() << " : " << number_of_samples << endl;
	assert(result.size() == number_of_samples);
	return result;
}

///
///	\brief	Creates a set of values in a range for a numpy array of intervals
///	\author	Dean
///	\since	06-07-2016
///
vector<float> create_interval_array(const float data_min, const float data_max, const float step_size)
{
	assert(data_min < data_max);
	vector<float> result;

    auto min_neg = floor(data_min - (step_size / 2.0f));
    auto max_pos = ceil(data_max + (step_size / 2.0f));

	//cout << "Min: " << min_neg << " | Max: " << max_pos << endl;
	for (auto n = min_neg; n < 0.0f; n += step_size)
	{
		//cout << n << " ";
		result.push_back(n);
	}
	for (auto p = 0.0f; p <= max_pos; p += step_size)
	{
		//cout << p << " ";
		result.push_back(p);
	}
	return result;
}

///
///		Tests if a superarc persists throughout an entire range, otherwise returns false
///
bool exists_in_range(const float min, const float max, const ReebGraphSuperarc* arc)
{
	assert(min <= max);
	auto top_height = arc->GetTopSupernode()->GetHeight();
	auto bottom_height = arc->GetBottomSupernode()->GetHeight();
	//cout << top_height << ".." << bottom_height << endl;
	
	if(bottom_height > top_height)
		swap(top_height, bottom_height);
	assert(bottom_height <= top_height);

	if ((top_height >= max) && (bottom_height <= min))
		return true;
	else
		return false;
}

bool exists_at_isovalue(const float isovalue, const ReebGraphSuperarc* arc)
{
	auto top_height = arc->GetTopSupernode()->GetHeight();
	auto bottom_height = arc->GetBottomSupernode()->GetHeight();
	//cout << top_height << ".." << bottom_height << endl;

	if(bottom_height > top_height)
		swap(top_height, bottom_height);
	assert(bottom_height <= top_height);

	if ((top_height >= isovalue) && (bottom_height <= isovalue))
		return true;
	else
		return false;
}

float mid_point(const float min, const float max)
{
	return (min + max) / 2.0f;
}

set<ReebGraphSuperarc*> get_superarcs_in_isovalue_range(const ReebGraph& reeb_graph, const float min, const float max)
{
	set<ReebGraphSuperarc*> result;
	for (auto it = reeb_graph.Superarcs_cbegin(); it != reeb_graph.Superarcs_cend(); ++it)
	{
		auto current_arc = *it;

		if (exists_at_isovalue(mid_point(min, max), current_arc)) result.insert(current_arc);
		//if (in_range(min, max, current_arc)) result.insert(current_arc);
	}
	return result;
}

vector<set<ReebGraphSuperarc*>> partiton_superarcs(const vector<float>& bins, const ReebGraph& reeb_graph)
{
	vector<set<ReebGraphSuperarc*>> result;

	for (auto i = 0; i < bins.size() - 1; ++i)
	{
		auto binned_superarcs = get_superarcs_in_isovalue_range(reeb_graph, bins[i], bins[i + 1]);

		result.push_back(binned_superarcs);
	}

	return result;
}

///
///	\brief		Computes the basic stats about the Reeb graph and
///				each of the vertices and arcs within it
///	\author		Dean
///	\since		13-07-2016
///
string computeReebGraphStats(const ReebGraph& reeb_graph)
{
	const auto TAB = '\t';

	stringstream result;
	result << "#" << TAB << "Reeb graph statistics" << endl;
	result << "#" << TAB << "Nodes: " << reeb_graph.GetVertexCount() << endl;
	result << "#" << TAB << "Arcs: " << reeb_graph.GetArcCount() << endl;
	result << "#" << TAB << "Loops: " << "0" << endl;
	result << "#" << TAB << "Minimum isovalue: " << reeb_graph.GetMinHeight() << endl;
	result << "#" << TAB << "Maximum isovalue: " << reeb_graph.GetMaxHeight() << endl;
	result << "#" << TAB << "Isovalue range: " 
		<< reeb_graph.GetMaxHeight() - reeb_graph.GetMinHeight() 
		<< endl << endl;

	auto list_supernodes = [&]()
	{
		for (auto it = reeb_graph.Supernodes_cbegin(); it != reeb_graph.Supernodes_cend(); ++it)
		{
			auto supernode = *it;
			auto pos = supernode->GetPosition();
			auto p_x = get<0>(pos);
			auto p_y = get<1>(pos);
			auto p_z = get<2>(pos);

			auto computeUpwardHeightSum = [&](const ReebGraphSupernode& node)
			{
				auto sum = 0.0f;

				for (auto& arc : node.getAllUpwardArcs())
				{
					sum += arc->GetArcLength();
				}
				return sum;
			};

			auto computeDownwardHeightSum = [&](const ReebGraphSupernode& node)
			{
				auto sum = 0.0f;

				for (auto& arc : node.getAllDownwardArcs())
				{
					sum += arc->GetArcLength();
				}
				return sum;
			};

			result << "Supernode " << supernode->GetId() << endl
				<< TAB << "height: " << supernode->GetHeight() << endl
				<< TAB << "norm: " << supernode->GetNormalisedHeight() << endl
				<< TAB << "pos: (" << p_x << ", " << p_y << ", " << p_z << ")" << endl
				<< TAB << "degree: " << supernode->GetDegree() << endl
				<< TAB << "up degree: " << supernode->GetUpDegree() << endl
				<< TAB << "down degree: " << supernode->GetDownDegree() << endl
				<< TAB << "upward subtree supernode count: " << supernode->getAllUpwardNodes().size() << endl
				<< TAB << "upward subtree superarc count: " << supernode->getAllUpwardArcs().size() << endl
				<< TAB << "downward subtree supernode count: " << supernode->getAllDownwardNodes().size() << endl
				<< TAB << "downward subtree superarc count: " << supernode->getAllDownwardArcs().size() << endl
				<< TAB << "upward height sum: " << computeUpwardHeightSum(*supernode) << endl
				<< TAB << "downward height sum: " << computeDownwardHeightSum(*supernode) << endl;
			//	Surafce area, volume, moment of inertia etc...
			result << endl;
		}
	};

	auto list_superarcs = [&]()
	{
		for (auto it = reeb_graph.Superarcs_cbegin(); it != reeb_graph.Superarcs_cend(); ++it)
		{
			auto superarc = *it;
			auto top_node = superarc->GetTopSupernode();
			auto bottom_node = superarc->GetBottomSupernode();
			auto p_tx = get<0>(top_node->GetPosition());
			auto p_ty = get<1>(top_node->GetPosition());
			auto p_tz = get<2>(top_node->GetPosition());
			auto p_bx = get<0>(bottom_node->GetPosition());
			auto p_by = get<1>(bottom_node->GetPosition());
			auto p_bz = get<2>(bottom_node->GetPosition());

			result << "Supearc " << superarc->GetId() << endl
				<< TAB << "top height (norm): " << top_node->GetHeight()
				<< " (" << top_node->GetNormalisedHeight() << ")" << endl
				<< TAB << "bottom height (norm): " << bottom_node->GetHeight()
				<< " (" << bottom_node->GetNormalisedHeight() << ")" << endl
				<< TAB << "height range: " << superarc->GetArcLength() << endl
				<< TAB << "top location: (" << p_tx << ", " << p_ty << ", " << p_tz << ")" << endl
				<< TAB << "bottom location: (" << p_bx << ", " << p_by << ", " << p_bz << ")" << endl;
			result << endl;
		}
	};

	list_supernodes();
	list_superarcs();

	return result.str();
}

string compute_isovalue_distribution(const ReebGraph& reeb_graph,
									 const float min,
									 const float max,
									 const size_t sample_count)
{
	stringstream result;
	const auto TAB = '\t';

	//	Check the range and sample count is valid
	assert(min < max);
	assert(sample_count > 0);

	auto sample_array = create_isovalue_sample_array(min, max, sample_count);
	vector<size_t> sample_counts;
	sample_counts.reserve(sample_count);

	for (auto isovalue : sample_array)
	{
		auto count = 0ul;
		for (auto it = reeb_graph.Superarcs_cbegin(); it != reeb_graph.Superarcs_cend(); ++it)
		{
			auto& superarc = *it;

			if (exists_at_isovalue(isovalue, superarc)) ++count;
		}

		cout << fixed << setprecision(10) << "isovalue: " << isovalue << "\t:" << count << endl;
		sample_counts.push_back(count);
	}

	assert(sample_array.size() == sample_counts.size());

	result << "#" << TAB << "x axis: isovalue" << endl;
	for (auto i : sample_array)
	{
		result << i << endl;
	}
	result << endl;
	result << "#" << TAB << "y axis: frequency" << endl;
	for (auto b : sample_counts)
	{
		result << b << endl;
	}
	result << endl;

	return result.str();
}

tuple<float, float, size_t> parse_arguments(int argc, char* argv[])
{
	assert(argc > 1);

	auto min_val = 0.0f;
	auto max_val = 0.0f;
	auto samples = 256;

	for (auto i = 2; i < argc; ++i)
	{
		auto param = string(argv[i]);
		std::transform(param.begin(), param.end(), param.begin(),
					   [](unsigned char c) { return std::toupper(c); });

		if (param.find("R") != std::string::npos)
		{
			assert(argc > i + 2);
			min_val = stof(argv[i+1]);
			max_val = stof(argv[i+2]);

			//	Advance past the parameters
			i += 2;
		}
		else if (param.find("S") != std::string::npos)
		{
			assert(argc > i + 1);
			samples = stoi(argv[i+1]);

			//	Advance past the parameters
			i += 1;
		}
	}

	cout << "Parsed parameters: " << min_val << " "
		 << max_val << " " << samples << endl;
	return std::make_tuple(min_val, max_val, samples);
}

int main(int argc, char* argv[])
{
	if (argc < 2)
	{
		cerr << "Usage: reeb <absolute_path_to_file> [-r] [-b]" << endl
			 <<	"\t<absolute_path_to_file>	Path to a .rb format Reeb graph file." << endl
			 << "\t[-r min max]				Range to normalise data to          ." << endl
			 << "\t[-b number_of_bins]		Number of bins (default = 256)." << endl;

		return EXIT_INVALID_ARGUMENTS;
	}

	ReebGraph reebGraph;
	RbReader rb_reader(reebGraph);

	auto input_file = argv[1];
	auto path = split_path(input_file);
	auto output_dir = path.absolute_path;

	if (!rb_reader(input_file))
	{
		cerr << "Could not open file: '" << "filename" << "'" << endl;
		return EXIT_INPUT_ERROR;
	}

	//	Parse and unpack arguments
	auto args = parse_arguments(argc, argv);
	auto data_min = get<0>(args);
	auto data_max = get<1>(args);
	auto sample_count = get<2>(args);

	if ((data_min == 0.0f) && (data_max == 0.0f))
	{
		cout << "Reeb graph" << endl
			 << "\tmin: " << reebGraph.GetMinHeight()
			 << "\tmax: " << reebGraph.GetMaxHeight() << endl;

		//	Compute a range if none was specified
		data_min = reebGraph.GetMinHeight();
		data_max = reebGraph.GetMaxHeight();
	}

	cout << "Reeb graph properties: " << endl;
	cout << "\tVertex Count: " << reebGraph.GetVertexCount() << endl;
	cout << "\tArc Count: " << reebGraph.GetArcCount() << endl;
	cout << "\tMinimum value: " << reebGraph.GetMinHeight() << endl;
	cout << "\tMaximum value: " << reebGraph.GetMaxHeight() << endl;


	auto frequency_distribution = [](const ReebGraph& reebGraph, const string& filename,
			const float min, const float max, const size_t sample_count) -> int
	{
		//auto sample_count = 256;

		auto distribution = compute_isovalue_distribution(reebGraph, min, max, sample_count);
		
		ofstream output_freqs_file(filename);
		if (output_freqs_file.is_open() && output_freqs_file.good())
		{
			output_freqs_file << distribution << endl;
			cout << "Wrote reeb graph frequecy stats to: " << filename << "." << endl;
		}
		else
		{
			cerr << "Unable to write to file '" << filename << "." << endl;
			return EXIT_OUTPUT_ERROR;
		}
		output_freqs_file.close();
		return EXIT_OK;
	};

	auto frequency_histogram = [](const ReebGraph& reebGraph, const string& filename,
			const float min, const float max, const size_t sample_count) -> int
	{
		//auto interval_size = 0.01f;
		auto interval_size = (max - min) / static_cast<float>(sample_count);
		cout << "Interval size: " << interval_size << endl;

		auto intervals = create_interval_array(min, max, interval_size);
		//cout << endl << intervals << endl;

		auto bins = partiton_superarcs(intervals, reebGraph);

		ofstream output_file(filename);
		if (output_file.is_open() && output_file.good())
		{
			output_file << rbh_file(intervals, bins, "isovalue", "frequency");
			cout << "Wrote reeb graph histogram stats to: " << filename << "." << endl;
		}
		else
		{
			cerr << "Unable to write to file '" << filename << "." << endl;
			return EXIT_OUTPUT_ERROR;
		}
		output_file.close();
		return EXIT_OK;
	};

	auto freq_distribution_result = frequency_distribution(reebGraph, output_dir + "/" + path.filename_without_extension + ".rbf",
														   data_min, data_max, sample_count);
	if (freq_distribution_result != EXIT_OK) return freq_distribution_result;

	auto freq_histogram_result = frequency_histogram(reebGraph, output_dir + "/" + path.filename_without_extension + ".rbh",
													 data_min, data_max, sample_count);
	if (freq_histogram_result != EXIT_OK) return freq_histogram_result;

	const auto stats_filename = output_dir + "/" + path.filename_without_extension + ".rbs";
	ofstream output_stats_file(stats_filename);
	if (output_stats_file.is_open() && output_stats_file.good())
	{
		output_stats_file << computeReebGraphStats(reebGraph) << endl;
		cout << "Wrote reeb graph stats to: " << stats_filename << "." << endl;
	}
	else
	{
		cerr << "Unable to write to file '" << stats_filename << "." << endl;
		return EXIT_OUTPUT_ERROR;
	}

#ifdef _DEBUG
	pause_before_exit();
#endif
	return EXIT_OK;
}
