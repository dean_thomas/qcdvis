#include "Points_4.h"


Points_4::Points_4(size_t dims_xyz, size_t dim_t)
{
	m_lookupTable = generate_points(dims_xyz, dim_t);
}


Points_4::~Points_4()
{
}

Points_4::points_4d Points_4::generate_points(const size_t dim_xyz, const size_t dim_t) const
{
	points_4d result;

	for (auto t = 0ul; t < dim_t; ++t)
	{
		auto points_3d = vptr_vtkPoints::New();

		for (auto z = 0ul; z < dim_xyz; ++z)
		{
			for (auto y = 0ul; y < dim_xyz; ++y)
			{
				for (auto x = 0ul; x < dim_xyz; ++x)
				{
					//	Load 3d slice
					points_3d->InsertNextPoint(static_cast<float>(x), static_cast<float>(y), static_cast<float>(z));
				}
			}
		}
		//	Add to lookup map
		result[t] = points_3d;
	}
	return result;
}

size_t Points_4::get_number_of_points() const
{
	auto total = 0ul;

	for (auto& slice : m_lookupTable)
	{
		total += slice.second->GetNumberOfPoints();
	}
	return total;
}