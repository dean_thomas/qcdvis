#pragma once


#include <vtkPoints.h>
#include <vtkSmartPointer.h>

#include <map>


class Points_4
{
	using vptr_vtkPoints = vtkSmartPointer<vtkPoints>;
	using points_4d = std::map<size_t, vptr_vtkPoints>;
private:
	points_4d m_lookupTable;
	points_4d generate_points(const size_t dim_xyz, const size_t dim_t) const;
public:
	Points_4(size_t dims_xyz, size_t dim_t);
	~Points_4();



	size_t get_number_of_points() const;
};

