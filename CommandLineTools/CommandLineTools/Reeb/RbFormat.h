#pragma once

#include <vtkReebGraph.h>
#include <vtkSmartPointer.h>
#include <vtkVertexListIterator.h>
#include <vtkEdgeListIterator.h>

#include <cassert>
#include <string>
#include <ostream>

struct RbFormat
{
public:
	vtkReebGraph* const graph;
	const std::string header;

	RbFormat(vtkReebGraph* const graph, const std::string& header = "")
		: graph { graph }, header { header }
	{	}
};

inline std::ostream& operator<<(std::ostream& os, const RbFormat& rb_format)
{
	//	This should match the version in the Rb file reader
	//const auto TARGET_RB_VERSION = 1.1f;
	cout << "Rb format version set to: " << TARGET_RB_VERSION << endl;

	using namespace std;
	const char TAB = '\t';
	const auto X = 0ul;
	const auto Y = 1ul;
	const auto Z = 2ul;
	const auto PRECISION = 8ul;
	
	assert(rb_format.graph != nullptr);

	//	Shortcut alias
	auto& reeb_graph = rb_format.graph;

	if (TARGET_RB_VERSION > 1.0)
	{
		//	Make sure the Reeb graph has been augmented with the information we want
		assert(reeb_graph->GetVertexData()->HasArray("Normalised value"));
		assert(reeb_graph->GetVertexData()->HasArray("Position"));
		//	For release mode...
		if (!reeb_graph->GetVertexData()->HasArray("Normalised value"))
			throw std::invalid_argument("Could not find array of normalised isovalues");
		if (!reeb_graph->GetVertexData()->HasArray("Position"))
			throw std::invalid_argument("Could not find array of critical vertex positions");

		if (TARGET_RB_VERSION > 1.1)
		{
			//	Extra persistence information
			assert(reeb_graph->GetEdgeData()->HasArray("Node Count"));
			assert(reeb_graph->GetEdgeData()->HasArray("Sample Sum"));
		}
	}
	//	cout << "Arrays attached to vertices in Reeb graph:" << endl;
	//	for (auto i = 0ul; i < reeb_graph->GetVertexData()->GetNumberOfArrays(); ++i)
	//	{
	//		cout << TAB << reeb_graph->GetVertexData()->GetArrayName(i) << endl;
	//	}

	//	cout << "Arrays attached to edges in Reeb graph:" << endl;
	//	for (auto i = 0ul; i < reeb_graph->GetEdgeData()->GetNumberOfArrays(); ++i)
	//	{
	//		cout << TAB << reeb_graph->GetEdgeData()->GetArrayName(i) << endl;
	//	}



	//	Write header
	os << "#" << TAB << "version " << TARGET_RB_VERSION << endl;
	os << endl;


	//	List vertices
	os << "#" << TAB << "Nodes: " << reeb_graph->GetNumberOfVertices() << endl;
	os << "#" << TAB << "ID / Isovalue / Normalised Isovalue / ( x y z )" << endl;
	auto v_it = vtkSmartPointer<vtkVertexListIterator>::New();
	reeb_graph->GetVertices(v_it);
	while (v_it->HasNext())
	{
		auto v_id = v_it->Next();
		
		if (TARGET_RB_VERSION > 1.0)
		{
			auto val = reeb_graph->GetVertexData()->GetArray("Value")->GetTuple1(v_id);
			auto norm_val = reeb_graph->GetVertexData()->GetArray("Normalised value")->GetTuple1(v_id);
			auto x = reeb_graph->GetVertexData()->GetArray("Position")->GetTuple3(v_id)[X];
			auto y = reeb_graph->GetVertexData()->GetArray("Position")->GetTuple3(v_id)[Y];
			auto z = reeb_graph->GetVertexData()->GetArray("Position")->GetTuple3(v_id)[Z];

			os << fixed << setprecision(PRECISION);
			//os << v_id << TAB << reeb_graph->GetNodeScalarValue(v_id) << endl;
			os << v_id << TAB << val << TAB << norm_val << TAB
			   << "(" << static_cast<int>(x)
			   << ", " << static_cast<int>(y)
			   << ", " << static_cast<int>(z) << ")" << endl;
		}
		else
		{
			auto height = 0.0;
			//	Just an id and a height
			os << v_id << TAB << height << endl;
		}
	}
	os << endl;

	//	List edges
	os << "#" << TAB << "Arcs: " << reeb_graph->GetNumberOfEdges() << endl;
	if (TARGET_RB_VERSION > 1.1)
	{
		//	From version 1.1 we will also store persistence measures
		os << "#" << TAB << "ID / Source Vertex / Destination vertex / Node count / Sample sum" << endl;
	}
	else
	{
		//	No persistence data
		os << "#" << TAB << "ID / Source Vertex / Destination vertex" << endl;
	}
	auto e_it = vtkSmartPointer<vtkEdgeListIterator>::New();
	reeb_graph->GetEdges(e_it);
	while (e_it->HasNext())
	{
		auto edge_id = e_it->Next();
		if (TARGET_RB_VERSION > 1.1)
		{
			auto node_count = static_cast<int>(reeb_graph->GetEdgeData()->GetArray("Node Count")->GetTuple1(edge_id.Id));
			auto sample_sum = static_cast<double>(reeb_graph->GetEdgeData()->GetArray("Sample Sum")->GetTuple1(edge_id.Id));

			os << edge_id.Id << TAB << edge_id.Source << TAB << edge_id.Target
			   << TAB << node_count << TAB << sample_sum << endl;
		}
		else
		{
			os << edge_id.Id << TAB << edge_id.Source << TAB << edge_id.Target << endl;
		}
	}
	os << endl;
	return os;
}

