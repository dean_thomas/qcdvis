#include <vtkAutoInit.h>
// VTK_MODULE_INIT(vtkRenderingOpenGL);
// VTK_MODULE_INIT(vtkInteractionStyle);
// VTK_MODULE_INIT(vtkRenderingFreeType);
#include <Common.h>
#include <HeightField3D.h>
#include <cassert>
#include <tuple>
#include <vector>
#include <vtkDoubleArray.h>
#include <vtkEdgeListIterator.h>
#include <vtkFloatArray.h>
#include <vtkInEdgeIterator.h>
#include <vtkOutEdgeIterator.h>
#include <vtkOutEdgeIterator.h>
#include <vtkPointData.h>
#include <vtkPoints.h>
#include <vtkPolyData.h>
#include <vtkReebGraph.h>
#include <vtkReebGraphSimplificationFilter.h>
#include <vtkReebGraphToJoinSplitTreeFilter.h>
#include <vtkReebGraphVolumeSkeletonFilter.h>
#include <vtkSmartPointer.h>
#include <vtkUnstructuredGrid.h>
#include <vtkUnstructuredGridToReebGraphFilter.h>
#include <vtkVariantArray.h>
#include <vtkVertexListIterator.h>

#include <vtk_ostream.h>

#include "SpecFormat.h"

//#include "../../../QTFlexibleIsosurfaces_Modified/Graphs/rb.h"
#include "../Common/ArgParser.h"
#include "RbFormat.h"
//#include <vtk_ostream_formats.h>

using namespace std;

// using vptr_vtkPolydata = vtkSmartPointer<vtkPolyData>;
using vptr_vtkPolydata = vtkPolyData*;

// using vptr_vtkUnstructuredGrid = vtkSmartPointer<vtkUnstructuredGrid>;
using vptr_vtkUnstructuredGrid = vtkUnstructuredGrid*;

// using vptr_vtkPoints = vtkSmartPointer<vtkPoints>;
using vptr_vtkPoints = vtkPoints*;

// using vptr_vtkDoubleArray = vtkSmartPointer<vtkDoubleArray>;
using vptr_vtkDoubleArray = vtkDoubleArray*;

#define DISABLE_OUTPUT

///
///	\brief	Generates a set of points at integer intervals in 3D space
///	\since	06-06-2016
///	\author Dean
///
vptr_vtkPoints
generate_points(const size_t dim_x, const size_t dim_y, const size_t dim_z)
{
	auto result = vtkPoints::New();

	for (auto z = 0ul; z < dim_z; ++z) {
		for (auto y = 0ul; y < dim_y; ++y) {
			for (auto x = 0ul; x < dim_x; ++x) {
				result->InsertNextPoint(static_cast<double>(x), static_cast<double>(y),
					static_cast<double>(z));
			}
		}
	}
	return result;
}

///
///	\brief	Generates a 3D grid of cubic cells, each split into 6
/// tetrahedral cells
///			NOTE: Boundaries are assumed periodic
///	\since	06-06-2016
///	\author Dean
///
vptr_vtkUnstructuredGrid
generate_grid(const size_t dim_x, const size_t dim_y, const size_t dim_z)
{
	auto result = vtkUnstructuredGrid::New();
	result->Allocate();

	//	Create the actual points
	auto points = generate_points(dim_x, dim_y, dim_z);
	result->SetPoints(points);
	// points->Delete();

	//	If using periodic boundaries we want the last cell on
	//	the far boundary
	auto max_x = dim_x;
	auto max_y = dim_y;
	auto max_z = dim_z;

	//	Create the cells
	auto getIndex = [&](const size_t x, const size_t y,
		const size_t z) -> vtkIdType {
		return (dim_z * (dim_y * (x) + (y)) + (z));
	};

	for (auto z = 0ul; z < max_z; ++z) {
		//	Will also support periodic axis
		auto z_next = (z + 1) % dim_z;
		for (auto y = 0; y < max_y; ++y) {
			//	Will also support periodic axis
			auto y_next = (y + 1) % dim_y;
			for (auto x = 0; x < max_x; ++x) {
				//	Will also support periodic axis
				auto x_next = (x + 1) % dim_x;

				const auto v_0 = getIndex(x, y, z);
				const auto v_1 = getIndex(x_next, y, z);
				const auto v_2 = getIndex(x, y, z_next);
				const auto v_3 = getIndex(x_next, y, z_next);
				const auto v_4 = getIndex(x, y_next, z);
				const auto v_5 = getIndex(x_next, y_next, z);
				const auto v_6 = getIndex(x, y_next, z_next);
				const auto v_7 = getIndex(x_next, y_next, z_next);

				//	6-tets per cell
				vtkIdType tetra0_points[] = { v_4, v_5, v_7, v_0 };
				vtkIdType tetra1_points[] = { v_0, v_1, v_7, v_5 };
				vtkIdType tetra2_points[] = { v_0, v_1, v_7, v_3 };
				vtkIdType tetra3_points[] = { v_6, v_7, v_4, v_0 };
				vtkIdType tetra4_points[] = { v_6, v_2, v_0, v_7 };
				vtkIdType tetra5_points[] = { v_0, v_2, v_7, v_3 };

				result->InsertNextCell(VTK_TETRA, 4, tetra0_points);
				result->InsertNextCell(VTK_TETRA, 4, tetra1_points);
				result->InsertNextCell(VTK_TETRA, 4, tetra2_points);
				result->InsertNextCell(VTK_TETRA, 4, tetra3_points);
				result->InsertNextCell(VTK_TETRA, 4, tetra4_points);
				result->InsertNextCell(VTK_TETRA, 4, tetra5_points);
			}
		}
	}
	return result;
}

/*
template <typename I>
vptr_vtkDoubleArray generate_field_data(const I& begin, const I& end)
{
				auto result = vtkSmartPointer<vtkDoubleArray>::New();
				auto count = end - begin;
				cout << "Number of values: " << count << endl;
				result->SetNumberOfTuples(count);
				result->SetNumberOfComponents(1);

				auto i = 0ul;
				for (auto it = begin; it != end; ++it)
				{
								auto value = *it;

								//cout << value << endl;

								result->SetTuple1(i, value);
								++i;
				}
				return result;
}
*/

///
///	\brief	Displays the Reeb graph as text
///	\since	06-06-2016
///	\author Dean
///
int DisplayReebGraph(vtkReebGraph* g)
{
	vtkDataArray* vertexInfo = vtkDataArray::SafeDownCast(
		g->GetVertexData()->GetAbstractArray("Vertex Ids"));
	if (!vertexInfo)
		return 1;

	vtkVariantArray* edgeInfo = vtkVariantArray::SafeDownCast(
		g->GetEdgeData()->GetAbstractArray("Vertex Ids"));
	if (!edgeInfo)
		return 2;

	cout << "   Reeb graph nodes:" << endl;
	for (int i = 0; i < vertexInfo->GetNumberOfTuples(); i++)
		cout << "      Node #" << i
			 << ") VertexMeshId: " << ((int)*(vertexInfo->GetTuple(i))) << endl;

	cout << "   Reeb graph arcs:" << endl;
	vtkEdgeListIterator* eIt = vtkEdgeListIterator::New();
	g->GetEdges(eIt);
	do {
		vtkEdgeType e = eIt->Next();
		vtkAbstractArray* deg2NodeList = edgeInfo->GetPointer(e.Id)->ToArray();
		cout << "     Arc #" << e.Id << ": " << *(vertexInfo->GetTuple(e.Source))
			 << " -> " << *(vertexInfo->GetTuple(e.Target)) << " ("
			 << deg2NodeList->GetNumberOfTuples() << " degree-2 nodes)" << endl;
	} while (eIt->HasNext());
	eIt->Delete();

	return 0;
}

///
///	\brief	Reduces the graph by collapsing a single vertex to an edge
///	\since	06-06-2016
///	\author Dean
///
inline bool
reductionStep(vtkMutableDirectedGraph* graph)
{
	vtkVertexListIterator* vertexIt = vtkVertexListIterator::New();
	graph->GetVertices(vertexIt);
	while (vertexIt->HasNext()) {
		auto vertex = vertexIt->Next();

		//	Remove self loops
		//	We should only have 1 available incoming edge
		vtkInEdgeIterator* edgeInIt = vtkInEdgeIterator::New();
		graph->GetInEdges(vertex, edgeInIt);
		while (edgeInIt->HasNext()) {
			auto eIn = edgeInIt->Next();

			//	Get the source vertex of the edge
			auto source = eIn.Source;

			//	We should only have 1 available outgoing edge
			vtkOutEdgeIterator* edgeOutIt = vtkOutEdgeIterator::New();
			graph->GetOutEdges(vertex, edgeOutIt);
			while (edgeOutIt->HasNext()) {
				auto eOut = edgeOutIt->Next();

				//	Get the target vertex of the vertex
				auto dest = eOut.Target;

				if (source == dest) {
					assert(eOut.Id == eIn.Id);
					graph->RemoveEdge(eIn.Id);
					return true;
				}
			}
		}

		auto inDegree = graph->GetInDegree(vertex);
		auto outDegree = graph->GetOutDegree(vertex);

		//	Look for a regular vertex
		if ((inDegree == 1) && (outDegree == 1)) {
			vtkIdType newSource;
			vtkIdType newTarget;

			//	Access persistence data
			auto node_count_array = vtkIntArray::SafeDownCast(graph->GetEdgeData()->GetArray("Node Count"));
			auto sample_sum_array = vtkDoubleArray::SafeDownCast(graph->GetEdgeData()->GetArray("Sample Sum"));
			assert(node_count_array != nullptr);
			assert(sample_sum_array != nullptr);

			//	And height data
			auto height_array = graph->GetVertexData()->GetArray("Value");
			assert(height_array != nullptr);

			//	We'll need to extract existing persistence values from the arcs
			auto nc_0 = 0;
			auto ss_0 = 0.0;
			auto nc_1 = 0;
			auto ss_1 = 0.0;

			//	We should only have 1 available incoming edge
			vtkInEdgeIterator* edgeInIt = vtkInEdgeIterator::New();
			graph->GetInEdges(vertex, edgeInIt);
			while (edgeInIt->HasNext()) {
				auto eIn = edgeInIt->Next();

				//	Retreive existing node count and sample sum on the arc
				nc_0 = node_count_array->GetValue(eIn.Id);
				ss_0 = sample_sum_array->GetValue(eIn.Id);

				//	Get the source vertex of the edge
				newSource = eIn.Source;
			}

			//	We should only have 1 available outgoing edge
			vtkOutEdgeIterator* edgeOutIt = vtkOutEdgeIterator::New();
			graph->GetOutEdges(vertex, edgeOutIt);
			while (edgeOutIt->HasNext()) {
				auto eOut = edgeOutIt->Next();

				//	Retreive existing node count and sample sum on the arc
				nc_1 = node_count_array->GetValue(eOut.Id);
				ss_1 = sample_sum_array->GetValue(eOut.Id);

				//	Get the target vertex of the vertex
				newTarget = eOut.Target;
			}

			//	Create the new edge to by-pass this vertex
			auto new_edge = graph->AddEdge(newSource, newTarget);

			//	Update persistence (node count)
			auto new_node_count = nc_0 + nc_1 + 1;
			cout << "Node count for arc " << new_edge.Id << " is " << new_node_count << endl;
			assert(new_edge.Id == node_count_array->GetNumberOfTuples() + 1);
			node_count_array->InsertNextValue(new_node_count);

			auto new_sample_sum = ss_0 + ss_1 + height_array->GetTuple1(vertex);
			cout << "New sample sum for arc " << new_edge.Id << " is " << new_sample_sum << endl;
			assert(new_edge.Id == sample_sum_array->GetNumberOfTuples() + 1);
			sample_sum_array->InsertNextValue(new_sample_sum);


			//	And delete the vertex
			graph->RemoveVertex(vertex);

			//	Iterators have probably become
			//	invalid, just exit and they can
			//	be re-initialised next time around
			return true;
		}
	}

	//	Nothing was removed
	return false;
}

///
///	\brief	Removes any quotes around the filename string (if present)
///	\since	06-06-2016
///	\author Dean
///
string
convertToAbsoluteFilePath(const string& inputPath)
{
	//	Trims any quotes etc around the file path
	if (((inputPath[0] == '\"') && (inputPath[inputPath.length() - 1] == '\"')) || ((inputPath[0] == '\'') && (inputPath[inputPath.length() - 1] == '\'')))
		return string(inputPath.cbegin() + 1, inputPath.cend() - 1);
	else
		return inputPath;
}

string
compute_output_filename(const string& input_filename,
	const string& user_specified_outdir = "",
	const string& file_extension = ".rb")
{
	auto absolute_path_parts = split_path(input_filename);

	if (user_specified_outdir != "") {
		//	Use the users specified directory
		return user_specified_outdir + "/" + absolute_path_parts.filename_without_extension + file_extension;
	} else if (absolute_path_parts.absolute_path != ".") {
		//	Calculate output directory from the input
		return absolute_path_parts.absolute_path + "/" + absolute_path_parts.filename_without_extension + file_extension;
	} else {
		//	No directory detected on input (probably treat as an error)
		return "";
	}
}

///
///	\brief	Entry point
///	\since	06-06-2016
///	\author Dean
///
int main(int argc, char* argv[])
{
	if (!((argc == 2) || (argc == 3))) {
		cerr << "<absolute_path_to_file> [absolute_path_to_output]" << endl
			 << "\t<absolute_path_to_file> 	Specifies the input scalar field."
			 << endl
			 << "\t[absolute_path_to_output]	Specifies location Reeb graph "
				"files will be written to."
			 << endl;
		return EXIT_INVALID_ARGUMENTS;
	}

	//	Extract the output directory given by user (if any)
	string specified_out_dir = "";
	if (argc == 3) {
		//	User specified output directory
		specified_out_dir = argv[2];
	}

	//	Extract input filename
	auto input_filename = convertToAbsoluteFilePath(argv[1]);

	//	Compute a filename for the output
	auto output_filename_rb = compute_output_filename(input_filename, specified_out_dir, ".rb");
	auto output_filename_spec = compute_output_filename(input_filename, specified_out_dir, ".spec");

	if (output_filename_rb == "") {
		cerr << "Unable to determine the output directory." << endl;
		return EXIT_INVALID_ARGUMENTS;
	}

	//	Load the file
	HeightField3D heightField;
	auto file_loaded = heightField.LoadFromVol1(input_filename);
	if (!file_loaded) {
		cerr << "Could not open file: '" << input_filename << "'." << endl;
		return EXIT_INPUT_ERROR;
	}

	cout << "Data loaded from file: '" << input_filename << "'" << endl;
	// auto data = generate_field_data(values.cbegin(), values.cend());
	auto volumeMesh = generate_grid(heightField.GetBaseDimX(), heightField.GetBaseDimY(),
		heightField.GetBaseDimZ());
	cout << "Input field properties: " << endl;
	cout << "\tInput dimensions: " << heightField.GetBaseDimX() << " * "
		 << heightField.GetBaseDimY() << " * " << heightField.GetBaseDimZ()
		 << endl;
	cout << "\tNumber of vertices: " << volumeMesh->GetNumberOfPoints() << endl;
	cout << "\tNumber of tets: " << volumeMesh->GetNumberOfCells() << endl;

	//	Assign the scalar values to a heightfield array
	vptr_vtkDoubleArray volumeScalarField = vtkDoubleArray::New();
	for (auto it = heightField.cbegin(); it != heightField.cend(); ++it) {
		auto value = *it;
		// cout << value << endl;
		volumeScalarField->InsertNextTuple1(value);
	}
	assert(volumeScalarField->GetNumberOfTuples() == volumeMesh->GetNumberOfPoints());
	volumeMesh->GetPointData()->SetScalars(volumeScalarField);

#ifndef DISABLE_OUTPUT
	cout << volumeScalarField->GetNumberOfTuples() << " : "
		 << volumeMesh->GetNumberOfPoints();
	cout << "Number of arrays: "
		 << volumeMesh->GetPointData()->GetNumberOfArrays() << endl;
#endif

	//	Compute the reeb graph
	vtkReebGraph* reeb_graph = vtkReebGraph::New();
	reeb_graph->Build(volumeMesh, volumeScalarField);
	cout << "Reeb graph properties (before simplification): " << endl
		 << "\tVertex count: " << reeb_graph->GetNumberOfVertices() << endl
		 << "\tEdge count: " << reeb_graph->GetNumberOfEdges() << endl;
	//<< "\tLoop count: " << reeb_graph->GetNumberOfLoops() << endl;

	auto gen_presistence_arrays = [&]() {
		//	An empty array to be used to keep track of the number of
		//	nodes collapsed into a new superarc
		auto node_count_array = vtkSmartPointer<vtkIntArray>::New();
		node_count_array->SetNumberOfComponents(1);
		node_count_array->SetName("Node Count");

		//	An empty array to be used to keep track of the sample sum
		//	of nodes collapsed into a new superarc
		auto sample_sum_array = vtkSmartPointer<vtkDoubleArray>::New();
		sample_sum_array->SetNumberOfComponents(1);
		sample_sum_array->SetName("Sample Sum");

		//	Loop over each existing arc in the data
		for (auto i = 0ul; i < reeb_graph->GetNumberOfEdges(); ++i) {
			node_count_array->InsertNextValue(0);
			sample_sum_array->InsertNextValue(0.0);
		}

		//	Add the arrays to the graph
		reeb_graph->GetEdgeData()->AddArray(node_count_array);
		reeb_graph->GetEdgeData()->AddArray(sample_sum_array);
	};

	//	Generate the persistence arrays so we can keep track of
	//	persistence as the graph is reduced
	gen_presistence_arrays();

	//	Reduce non-critical vertices
	cout << "Performing reduction of Reeb graph";
	do {
		cout << ".";
	} while (reductionStep(reeb_graph));
	cout << endl;

	//	Output data about the reduced graph
	cout << "Reeb graph properties (after simplification): " << endl
		 << "\tVertex count: " << reeb_graph->GetNumberOfVertices() << endl
		 << "\tEdge count: " << reeb_graph->GetNumberOfEdges() << endl;
	//<< "\tLoop count: " << reeb_graph->GetNumberOfLoops() << endl;

	/*
  cout << "   Test 3D.4 Reeb graph based volume skeleton... " << endl;
  vtkReebGraphVolumeSkeletonFilter *volumeSkeletonFilter =
				  vtkReebGraphVolumeSkeletonFilter::New();
  volumeSkeletonFilter->SetInputData(0, volumeMesh);
  volumeSkeletonFilter->SetInputData(1, reeb_graph);
  volumeSkeletonFilter->Update();
  vtkTable *volumeSkeleton = volumeSkeletonFilter->GetOutput();
  */

	//	Creates an array of non-normalised isovalues so that they can be added
	// to
	//	the reeb graph
	auto gen_value_array = [&]() -> vtkSmartPointer<vtkDoubleArray> {
		auto value_array = vtkSmartPointer<vtkDoubleArray>::New();
		value_array->SetNumberOfComponents(1);
		value_array->SetName("Value");

		auto v_ids = reeb_graph->GetVertexData()->GetAbstractArray("Vertex Ids");
		for (auto i = 0ul; i < v_ids->GetNumberOfTuples(); ++i) {
			auto tuple_id = v_ids->GetVariantValue(i);
			auto value = *volumeScalarField->GetTuple(tuple_id.ToInt());

			value_array->InsertNextTuple1(value);
			// cout << endl << endl;
		}
		return value_array;
	};

	//	Creates an array of normalised isovalues so that they can be added to
	//	the reeb graph
	auto gen_normalised_value_array = [&]() -> vtkSmartPointer<vtkDoubleArray> {
		//	Function for normalising values
		auto norm = [&](const float& in) -> double {
			auto min = heightField.GetMinHeight();
			auto max = heightField.GetMaxHeight();
			assert(min < max);
			// cout << "min: " << min << " max: " << max << endl;

			if (isinf(min))
				return 0.0;

			auto result = (in - min) / (max - min);
			// cout << in << " -> " << result << endl;
			assert((result >= 0.0) && (result <= 1.0));

			return result;
		};

		auto norm_array = vtkSmartPointer<vtkDoubleArray>::New();
		norm_array->SetNumberOfComponents(1);
		norm_array->SetName("Normalised value");

		auto v_ids = reeb_graph->GetVertexData()->GetAbstractArray("Vertex Ids");
		for (auto i = 0ul; i < v_ids->GetNumberOfTuples(); ++i) {
			auto tuple_id = v_ids->GetVariantValue(i);
			auto value = *volumeScalarField->GetTuple(tuple_id.ToInt());

			norm_array->InsertNextTuple1(norm(value));
			// cout << endl << endl;
		}
		return norm_array;
	};

	//	This method looks up the samples relating to a critical
	//	vertices in the reeb graph and translates it to the
	//	3D position in the scalar data
	auto gen_critical_vertex_pos_array = [&]() {
		//	Used to map the 1D index to a 3D vector
		using vec3 = tuple<size_t, size_t, size_t>;
		auto to_vec3 = [&](const size_t index) -> vec3 {
			size_t pos = index;
			auto x_dim = heightField.GetBaseDimX();
			auto y_dim = heightField.GetBaseDimY();
			auto z_dim = heightField.GetBaseDimZ();

			auto z = pos % z_dim;
			pos = (pos - z) / z_dim;
			auto y = pos % y_dim;
			auto x = (pos - y) / y_dim;

			return make_tuple(x, y, z);
		};

		//	Create a vector of 3 doubles to represent coordinates
		auto pos_array = vtkSmartPointer<vtkDoubleArray>::New();
		pos_array->SetNumberOfComponents(3);
		pos_array->SetName("Position");

		//	Loop over the existing list
		auto v_ids = reeb_graph->GetVertexData()->GetAbstractArray("Vertex Ids");
		for (auto i = 0ul; i < v_ids->GetNumberOfTuples(); ++i) {
			//	Look up the corresponding sample for the vertex
			auto tuple_id = v_ids->GetVariantValue(i);
			auto value = *volumeScalarField->GetTuple(tuple_id.ToInt());

			//	Translate the sample index to a 3D coordinate
			auto pos = to_vec3(tuple_id.ToInt());
			// cout << value << " - (" << get<0>(pos) << " " << get<1>(pos) << " " <<
			// get<2>(pos) << ") " << endl;

			//	Unpack the result and add to the array
			pos_array->InsertNextTuple3(get<0>(pos), get<1>(pos), get<2>(pos));
			// cout << endl << endl;
		}
		return pos_array;
	};

	//	Generate the non-normalised array and add to the Reeb graph
	auto values = gen_value_array();
	reeb_graph->GetVertexData()->AddArray(values);

	//	Generate the normalised array and add to the Reeb graph
	auto normalised_values = gen_normalised_value_array();
	reeb_graph->GetVertexData()->AddArray(normalised_values);

	//	Generate the positional array and add to the Reeb graph
	auto positional_values = gen_critical_vertex_pos_array();
	reeb_graph->GetVertexData()->AddArray(positional_values);

	//	Write the reeb graph in 'rb' format
	cout << "Saving reeb graph to file: '" << output_filename_rb << "'." << endl;
	ofstream test_file(output_filename_rb);
	if (test_file.is_open() && test_file.good()) {
		test_file << RbFormat(reeb_graph) << endl;
		test_file.close();
	} else {
		cerr << "Could not save Reeb graph to file: '" << output_filename_rb << "'."
			 << endl;
		return EXIT_OUTPUT_ERROR;
	}

//vtkReebGraph* reeb_graph_2 = vtkReebGraph::New();
//reeb_graph_2->Build(volumeMesh, volumeScalarField);
//	cout << "Dumping contour stats: '" << output_filename_spec << "." << endl;
//	ofstream spec_file(output_filename_spec);
//	if (spec_file.is_open() && spec_file.good()) {
//		spec_file << SpecFormat(volumeMesh, reeb_graph, 2) << endl;
//		spec_file.close();
//	} else {
//		cerr << "Could not save contour spectrum data to file: '" << output_filename_spec << "'."
//			 << endl;
//		return EXIT_OUTPUT_ERROR;
//	}

// DisplayReebGraph(reeb_graph);
#ifdef _DEBUG
	pause_before_exit();
#endif
	return EXIT_OK;
}
