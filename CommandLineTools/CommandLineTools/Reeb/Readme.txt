#	Readme.txt
#
#	Command line usage:
#
#		$ Reeb <input_file> [output_dir]
#
#		<input_file>: can be an absolute file path (which can be used to determine the output path)
#		[output_dir]: optional output directory for automation
#
#	Automated processing (on all heightfields in the current dir, output in current dir):
#	
#		Windows:
#			$ for %f in (*.vol1) do Reeb.exe "%f" "%CD%"
#
#		Linux:
#			$
#