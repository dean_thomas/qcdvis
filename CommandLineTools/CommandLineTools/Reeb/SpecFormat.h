#ifndef SPECFORMAT_H
#define SPECFORMAT_H

#include <vtkReebGraph.h>
#include <vtkSmartPointer.h>
#include <vtkVertexListIterator.h>
#include <vtkEdgeListIterator.h>
#include <cassert>
#include <string>
#include <ostream>
#include <vtkVolumeContourSpectrumFilter.h>
#include <vtkAreaContourSpectrumFilter.h>
#include <vtkDataObjectAlgorithm.h>
#include <vtkGeometryFilter.h>
#include <vtkTable.h>

struct SpecFormat
{
#define TARGET_SPEC_VERSION 1.0
public:
    vtkReebGraph* const graph = nullptr;
    const std::string header;
    size_t samples;
    vtkUnstructuredGrid* const unstructured_grid = nullptr;

    SpecFormat(vtkUnstructuredGrid* const unstructured_grid,
               vtkReebGraph* const graph,
               const size_t samples, const std::string& header = "")
        : unstructured_grid{ unstructured_grid }, graph { graph }, samples{ samples }, header { header }
    {	}
};

inline std::ostream& operator<<(std::ostream& os, const SpecFormat& spec_format)
{
    using namespace std;
    const char TAB = '\t';

    assert(spec_format.graph != nullptr);

    //	Shortcut alias
    auto& reeb_graph = spec_format.graph;

    //	Make sure the Reeb graph has been augmented with the information we want
    //assert(reeb_graph->GetVertexData()->HasArray("Normalised value"));
    //assert(reeb_graph->GetVertexData()->HasArray("Position"));

    auto volume_filter = vtkSmartPointer<vtkVolumeContourSpectrumFilter>::New();
    volume_filter->SetInputData(0, spec_format.unstructured_grid);
    volume_filter->SetInputData(1, reeb_graph);
    volume_filter->SetNumberOfSamples(spec_format.samples);

    vtkSmartPointer<vtkGeometryFilter> geometryFilter =
            vtkSmartPointer<vtkGeometryFilter>::New();
#if VTK_MAJOR_VERSION <= 5
    geometryFilter->SetInput(spec_format.unstructured_grid);
#else
    geometryFilter->SetInputData(spec_format.unstructured_grid);
#endif
    geometryFilter->Update();

    auto polydata = geometryFilter->GetOutput();
    auto area_filter = vtkSmartPointer<vtkAreaContourSpectrumFilter>::New();
    area_filter->SetInputData(0, polydata);
    area_filter->SetInputData(1, reeb_graph);
    area_filter->SetNumberOfSamples(spec_format.samples);

    //  Used to convert a pair of nodes id's to a height
    auto height_func = [&](const size_t src_id, const size_t tgt_id)
    {
        auto src_val = reeb_graph->GetVertexData()->GetArray("Value")->GetTuple1(src_id);
        auto tgt_val = reeb_graph->GetVertexData()->GetArray("Value")->GetTuple1(tgt_id);

        return tgt_val - src_val;
    };

    //  Used to convert a pair of nodes id's to a normalised height
    auto n_height_func = [&](const size_t src_id, const size_t tgt_id)
    {
        auto src_val = reeb_graph->GetVertexData()->GetArray("Normalised value")->GetTuple1(src_id);
        auto tgt_val = reeb_graph->GetVertexData()->GetArray("Normalised value")->GetTuple1(tgt_id);

        return tgt_val - src_val;
    };

    auto volume_func = [&](const size_t arc_id)
    {
        volume_filter->SetArcId(arc_id);
        volume_filter->Update();

        auto tab = volume_filter->GetOutput();
        auto tot = 0.0;
        for (auto i = 0ul; i < 100; ++i)
        {
            tot += tab->GetValue(i, 0).ToDouble();
        }
        //area_filter->GetOutput()->Dump();
        cout << arc_id << " : " << tot << endl;

        return -1;
    };

    auto area_func = [&](const size_t arc_id)
    {
        area_filter->SetArcId(arc_id);
        area_filter->Update();

        auto tab = area_filter->GetOutput();
        auto tot = 0.0;
        for (auto i = 0ul; i < spec_format.samples; ++i)
        {
            tot += tab->GetValue(i, 0).ToDouble();
        }
        //area_filter->GetOutput()->Dump();
        cout << arc_id << " : " << tot << endl;

        return -1;
    };

    //	For release mode...
    //if (!reeb_graph->GetVertexData()->HasArray("Normalised value"))
    //    throw std::invalid_argument("Could not find array of normalised isovalues");
    //if (!reeb_graph->GetVertexData()->HasArray("Position"))
    //    throw std::invalid_argument("Could not find array of critical vertex positions");

    //	Write header
    os << "#" << TAB << "version " << TARGET_SPEC_VERSION << endl;
    os << "#" << TAB << "Number of samples per arc: " << spec_format.samples << endl;
    os << endl;

    //	List edges
    os << "#" << TAB << "Arcs: " << reeb_graph->GetNumberOfEdges() << endl;
    os << "#" << TAB << "ID / Source Vertex / Destination vertex / height / norm.height / area / volume" << endl;
    auto e_it = vtkSmartPointer<vtkEdgeListIterator>::New();
    reeb_graph->GetEdges(e_it);

    while (e_it->HasNext())
    {
        auto edge = e_it->Next();
        os << edge.Id << TAB << edge.Source << TAB << edge.Target
          // << TAB << height_func(edge.Source, edge.Target)
           //<< TAB << n_height_func(edge.Source, edge.Target)
           << TAB << area_func(edge.Id) << endl
           << TAB << volume_func(edge.Id) << endl;

        //getchar();
    }

    os << endl;
    return os;
}

#endif // SPECFORMAT_H
