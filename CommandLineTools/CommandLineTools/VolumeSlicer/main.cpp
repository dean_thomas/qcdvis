#include <iostream>
#include <string>
#include <exception>
#include <ostream>

#include "../Common/Common.h"
#include "../../../QTFlexibleIsosurfaces_Modified/HeightField/HeightField3D.h"
#include "../../../QTFlexibleIsosurfaces_Modified/HeightField/HeightField4D.h"

using namespace std;

using AxisSet = std::bitset<4>;

///
/// \brief			Formats the AxisSet bit stream to a set of chars from { 'X', 'Y', 'Z', 'T'}
/// \param os		ostream that receives the output
/// \param axis_set	bitsetr representing the set of axis
/// \return			ostream for chaining output statements
///
std::ostream& operator<<(std::ostream& os, const AxisSet& axis_set)
{
	if (axis_set.test(0))
		os << "X";
	if (axis_set.test(1))
		os << "Y";
	if (axis_set.test(2))
		os << "Z";
	if (axis_set.test(3))
		os << "T";
	os << " = " << axis_set.to_ulong();

	return os;
}

///
/// \brief		Prints usage options to the command line
///
void printUsage()
{
	const std::vector<Switch> PARAMS = {
		{ "[drive:][path]filename", "Specifies the input file to be sliced.", false },
		{ "YZT", "Produces a 3D volume, sliced along the 'x' axis." },
		{ "XZT", "Produces a 3D volume, sliced along the 'y' axis." },
		{ "XYT", "Produces a 3D volume, sliced along the 'z' axis." },
		{ "XYZ", "Produces a 3D volume, sliced along the 'xt' axis." },
		{ "XY", "Produces a 2D plane, sliced along the 'z' and 't' axis." },
		{ "XZ", "Produces a 2D plane, sliced along the 'y' and 't' axis." },
		{ "XT", "Produces a 2D plane, sliced along the 'y' and 'z' axis." },
		{ "YZ", "Produces a 2D plane, sliced along the 'x' and 't' axis." },
		{ "YT", "Produces a 2D plane, sliced along the 'x' and 'z' axis." },
		{ "ZT", "Produces a 2D plane, sliced along the 'x' and 'y' axis." },
		{ "P", "Output sliced data will include a layer of ghost vertices" },
		{ "?", "This help message." }
	};

	std::cout << getUsageString("Volume Slicer", "Slices 4D scalar volumes into 3D or 2D slices.",
								PARAMS);
}

///
/// \brief							Takes a 4D input and reduces to 3D
/// \param filename:				The 4D input file to be loaded
/// \param axis:					A set of axis that will appear in the output (as a bitset)
/// \param addGhostVertices:		If true an additional set of vertices will be appended to the far edge of the output
/// \return vector<HeightField3D>:	a vector containing each of the 3D slices
///
std::vector<HeightField3D> slice3d(const std::string& filename, const AxisSet& axis, const bool addGhostVertices = false)
{
	assert(axis.count() == 3);

	std::vector<HeightField3D> result;

	HeightField4D heightfield4;
	if (heightfield4.LoadFromFile(filename))
	{
		cout << "Adding ghost vertices to output: " << boolalpha << addGhostVertices << "." << endl;

		//	Combine input axes as bit field
		auto flags = axis.to_ulong();

		switch (flags)
		{
		case 0b0111:
		{
			//	XYZ_
			for (auto t = 0ul; t < heightfield4.GetDimW(); ++t)
			{
				//	Add the slice to the result vector
				auto temp = *(heightfield4.GetVolumeXYZ(t, addGhostVertices));
				result.push_back(temp);
			}
		}
			break;
		case 0b1110:
		{
			//	_YZT
			for (auto x = 0ul; x < heightfield4.GetDimX(); ++x)
			{
				//	Add the slice to the result vector
				result.push_back(*heightfield4.GetVolumeYZT(x, addGhostVertices));
			}
		}
			break;
		case 0b1101:
		{
			//	X_ZT
			for (auto y = 0ul; y < heightfield4.GetDimY(); ++y)
			{
				//	Add the slice to the result vector
				result.push_back(*heightfield4.GetVolumeXZT(y, addGhostVertices));
			}
		}
			break;
		case 0b1011:
		{
			//	XY_T
			for (auto z = 0ul; z < heightfield4.GetDimZ(); ++z)
			{
				//	Add the slice to the result vector
				result.push_back(*heightfield4.GetVolumeXYT(z, addGhostVertices));
			}
		}
			break;

		default:
			throw std::invalid_argument("Slice error, unknown axes.");
		}
		return result;
	}
	else
	{
		//	Unable to open file
		auto msg = "Could not open file: " + filename;
		throw std::invalid_argument(msg.c_str());
	}
}

///
/// \brief					Extarcts an absolute path to the input file
/// \param absolute_path:	absolute path to the input file
/// \return string:			the absolute with the filename removed
///
std::string extractPath(const std::string& absolute_path)
{
	size_t found = absolute_path.find_last_of("/\\");
	//	cout << "Splitting: " << str << endl;
	if (found == string::npos) throw std::invalid_argument("Absolute path to file required.");

	return absolute_path.substr(0, found);
}

///
/// \brief			Parses the output dimensions specified by the user
/// \param argc:	number of command line parameters
/// \param argv:	array of C strings of parameters
/// \return bitset: where '1' represents a axis to appear in the output file
///
AxisSet get_slice_axis(int argc, char* argv[])
{
	assert(argc > 2);

	AxisSet result;

	for (auto i = 2;i < argc; ++i)
	{
		result.reset();

		auto param_str = std::string(argv[i]);

		for (auto& c : param_str)
		{
			c = toupper(c);

			switch (c)
			{
			case 'X': result.set(0);
				break;
			case 'Y': result.set(1);
				break;
			case 'Z': result.set(2);
				break;
			case 'T': result.set(3);
				break;
			}
		}

		if (result.count() >= 2)
			return result;
	}

	return result;
}


///
/// \brief			Entry point
/// \param argc:	number of command line parameters
/// \param argv:	array of C strings of parameters
/// \return	0:		for sucess, non-zero otherwise
///
int main(int argc, char* argv[])
{
	bool pause_on_exit = true;
	bool periodic = false;

	if (argc < 3)
	{
		cerr << "Must include at least 3 parameters." << endl;
		printUsage();

		if (pause_on_exit) pause_before_exit();
		return EXIT_INVALID_ARGUMENTS;
	}

	auto axis_set = get_slice_axis(argc, argv);
	cout << "NOTE: at present, will only accept an input file (4D) as parameter." << endl;
	cout << "NOTE: at present, will only output into 3D volumes." << endl;
	cout << "\tFiles will be written to destop as XYZ volumes." << endl;
	cout << "\t[-np] no pause on exit." << endl << endl;
	cout << "Slicing input on " << axis_set << "." << endl;

	//	Check other flags
	for (auto i = 1; i < argc; ++i)
	{
		if (string(argv[i]) == "-np") pause_on_exit = false;
		if ((string(argv[i]) == "-p") || (string(argv[i]) == "-P")) periodic = true;
	}

	if ((axis_set.count() < 2) || (axis_set.count() > 3))
	{
		cerr << "Invalid axis configuration specified for output.  Must be 2 or 3 unique axes." << endl;
		printUsage();

		if (pause_on_exit) pause_before_exit();
		return EXIT_INVALID_ARGUMENTS;
	}

	if (axis_set.count() == 2)
	{
		cerr << "Print slicing into 2D planes is currently disabled." << endl;

		if (pause_on_exit) pause_before_exit();
		return EXIT_INVALID_ARGUMENTS;
	}

	//ArgParser args(argc, argv);
	try
	{
		const string OUT_DIR = extractPath(argv[1]);
		const string outputPath = OUT_DIR + "/";

		cout << "Working directory: " << outputPath << "." << endl;

		auto heightfield3Array = slice3d(argv[1], axis_set, periodic);

		for (auto i = 0ul; i < heightfield3Array.size(); ++i)
		{
			//	Generate filename
			auto fieldName = heightfield3Array[i].GetFieldName();
			auto axis_name = heightfield3Array[i].GetFixedVariableName();

			//	Stupid Windows line endings!!
			std::size_t pos = fieldName.find("\r");
			if (pos != string::npos)
				fieldName = fieldName.substr(0, pos);

			//	Remember in QCD we work 1..n
			auto slice = axis_name + "=" + to_string(i+1);
			auto extension = "vol1";
			auto filename = outputPath + fieldName + "_" + slice + "." + extension;

			cout << "Writing file: " << filename << "." << endl;

			//	Save the file
			auto result = heightfield3Array[i].SaveToFile(filename);

			//	Report errors
			if (!result)
			{
				cerr << "Error writing file: " << filename << "." << endl;

				if (pause_on_exit) pause_before_exit();
				return EXIT_OUTPUT_ERROR;
			}
		}

		cout << "Wrote " << heightfield3Array.size() << " files to '" << outputPath << "'." << endl;

		if (pause_on_exit) pause_before_exit();
		return EXIT_OK;
	}
	catch (std::invalid_argument& ex)
	{
		//	Catch an invalid file being passed
		cerr << ex.what() << endl;

		if (pause_on_exit) pause_before_exit();
		return EXIT_INPUT_ERROR;
	}
}
