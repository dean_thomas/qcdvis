QT += core
QT -= gui


DEFINES += NO_DATA_MODEL
DEFINES += NO_TOPOLOGY
DEFINES += BASIC_EXCEPTIONS

#   Enable c++11 support (std::array)
CONFIG += c++11
CONFIG  +=  c++14

TARGET = volume_slicer
CONFIG += console
CONFIG -= app_bundle

TEMPLATE = app

INCLUDEPATH += "/home/dean/qcdvis/Common Classes"
DEPENDPATH += "/home/dean/qcdvis/Common Classes"

INCLUDEPATH += "/home/dean/qcdvis/CommandLineTools/CommandLineTools/Common"
DEPENDPATH += "/home/dean/qcdvis/CommandLineTools/CommandLineTools/Common"

INCLUDEPATH += "/home/dean/qcdvis/QTFlexibleIsosurfaces_Modified"
DEPENDPATH += "/home/dean/qcdvis/QTFlexibleIsosurfaces_Modified"

INCLUDEPATH += "/home/dean/qcdvis/CommandLineTools/CommandLineTools/VolumeSlicer"
DEPENDPATH += "/home/dean/qcdvis/CommandLineTools/CommandLineTools/VolumeSlicer"

SOURCES += \
    ../../VolumeSlicer/main.cpp \
    ../../../../QTFlexibleIsosurfaces_Modified/HeightField/HeightField3D.cpp \
    ../../../../QTFlexibleIsosurfaces_Modified/HeightField/HeightField4D.cpp \
    ../../../../QTFlexibleIsosurfaces_Modified/HeightField/HeightSort.cpp \
    ../../../../QTFlexibleIsosurfaces_Modified/HeightField/NeighbourQueue/NeighbourQueue.cpp

HEADERS += \
    ../../../../QTFlexibleIsosurfaces_Modified/HeightField/Component.h \
    ../../../../QTFlexibleIsosurfaces_Modified/HeightField/HeightField3D.h \
    ../../../../QTFlexibleIsosurfaces_Modified/HeightField/HeightField4D.h \
    ../../../../QTFlexibleIsosurfaces_Modified/HeightField/HeightSort.h \
    ../../../../QTFlexibleIsosurfaces_Modified/HeightField/PriorityIndex.h \
    ../../../../QTFlexibleIsosurfaces_Modified/HeightField/NeighbourQueue/NeighbourFunctor6.h \
    ../../../../QTFlexibleIsosurfaces_Modified/HeightField/NeighbourQueue/NeighbourFunctor6P.h \
    ../../../../QTFlexibleIsosurfaces_Modified/HeightField/NeighbourQueue/NeighbourFunctor18.h \
    ../../../../QTFlexibleIsosurfaces_Modified/HeightField/NeighbourQueue/NeighbourFunctor18P.h \
    ../../../../QTFlexibleIsosurfaces_Modified/HeightField/NeighbourQueue/NeighbourQueue.h
