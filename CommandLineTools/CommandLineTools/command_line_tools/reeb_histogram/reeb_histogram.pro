SOURCES += \
    ../../ReebHistogram/main.cpp \
    ../../../../QTFlexibleIsosurfaces_Modified/Graphs/ContourTree.cpp \
    ../../../../QTFlexibleIsosurfaces_Modified/Graphs/ContourTreeSuperarc.cpp \
    ../../../../QTFlexibleIsosurfaces_Modified/Graphs/ContourTreeSupernode.cpp \
    ../../../../QTFlexibleIsosurfaces_Modified/Graphs/JoinTree.cpp \
    ../../../../QTFlexibleIsosurfaces_Modified/Graphs/MergeTree.cpp \
    ../../../../QTFlexibleIsosurfaces_Modified/Graphs/rb.cpp \
    ../../../../QTFlexibleIsosurfaces_Modified/Graphs/ReebGraph.cpp \
    ../../../../QTFlexibleIsosurfaces_Modified/Graphs/ReebGraphSuperarc.cpp \
    ../../../../QTFlexibleIsosurfaces_Modified/Graphs/ReebGraphSupernode.cpp \
    ../../../../QTFlexibleIsosurfaces_Modified/Graphs/SplitTree.cpp \
    ../../../../QTFlexibleIsosurfaces_Modified/HeightField/HeightField3D.cpp \
    ../../../../QTFlexibleIsosurfaces_Modified/CommandLine/ArgumentParser.cpp \
    ../../../../QTFlexibleIsosurfaces_Modified/CommandLine/QtCommandLineWorker.cpp \
    ../../../../QTFlexibleIsosurfaces_Modified/CommandLine/RegexCommandLineParser.cpp \
    ../../../../QTFlexibleIsosurfaces_Modified/Contour/Generation/ContourFactory.cpp \
    ../../../../QTFlexibleIsosurfaces_Modified/Contour/Generation/FollowCubeTables.cpp \
    ../../../../QTFlexibleIsosurfaces_Modified/Contour/Generation/PolygonFactory2.cpp \
    ../../../../QTFlexibleIsosurfaces_Modified/Contour/Generation/PolygonFactory3.cpp \
    ../../../../QTFlexibleIsosurfaces_Modified/Contour/Generation/PolylineList.cpp \
    ../../../../QTFlexibleIsosurfaces_Modified/Contour/Storage/Contour.cpp \
    ../../../../QTFlexibleIsosurfaces_Modified/Contour/Storage/MeshCache.cpp \
    ../../../../QTFlexibleIsosurfaces_Modified/Contour/Storage/MeshTriangle.cpp \
    ../../../../QTFlexibleIsosurfaces_Modified/DataExploration/IsovalueBookmark.cpp \
    ../../../../QTFlexibleIsosurfaces_Modified/DataExploration/IsovalueBookmarkList.cpp \
    ../../../../QTFlexibleIsosurfaces_Modified/HeightField/NeighbourQueue/NeighbourQueue.cpp \
    ../../../../QTFlexibleIsosurfaces_Modified/HeightField/HeightField4D.cpp \
    ../../../../QTFlexibleIsosurfaces_Modified/HeightField/HeightSort.cpp \
    ../../../../QTFlexibleIsosurfaces_Modified/libDesignPatterns/AbstractObservable.cpp \
    ../../../../QTFlexibleIsosurfaces_Modified/libDesignPatterns/AbstractObserver.cpp \
    ../../../../QTFlexibleIsosurfaces_Modified/libDesignPatterns/MacroCommand.cpp \
    ../../../../QTFlexibleIsosurfaces_Modified/Parallel/ThreadQueue.cpp \
    ../../../../QTFlexibleIsosurfaces_Modified/Statistics/SuperarcDistribution.cpp \
    ../../../../QTFlexibleIsosurfaces_Modified/Statistics/SupernodeDistribution.cpp \
    ../../../../QTFlexibleIsosurfaces_Modified/DataModel.cpp \
    ../../../../QTFlexibleIsosurfaces_Modified/processinfo.cpp

INCLUDEPATH += "/home/dean/qcdvis/QTFlexibleIsosurfaces_Modified"
DEPENDPATH += "/home/dean/qcdvis/QTFlexibleIsosurfaces_Modified"

INCLUDEPATH += "/home/dean/qcdvis/Common Classes"
DEPENDPATH += "/home/dean/qcdvis/Common Classes"

INCLUDEPATH += "/home/dean/qcdvis/CommandLineTools/CommandLineTools/Common"
DEPENDPATH += "/home/dean/qcdvis/CommandLineTools/CommandLineTools/Common"



HEADERS += \
    ../../../../QTFlexibleIsosurfaces_Modified/Graphs/ContourTree.h \
    ../../../../QTFlexibleIsosurfaces_Modified/Graphs/ContourTreeSuperarc.h \
    ../../../../QTFlexibleIsosurfaces_Modified/Graphs/ContourTreeSupernode.h \
    ../../../../QTFlexibleIsosurfaces_Modified/Graphs/JoinTree.h \
    ../../../../QTFlexibleIsosurfaces_Modified/Graphs/MergeTree.h \
    ../../../../QTFlexibleIsosurfaces_Modified/Graphs/rb.h \
    ../../../../QTFlexibleIsosurfaces_Modified/Graphs/ReebGraph.h \
    ../../../../QTFlexibleIsosurfaces_Modified/Graphs/ReebGraphSuperarc.h \
    ../../../../QTFlexibleIsosurfaces_Modified/Graphs/ReebGraphSupernode.h \
    ../../../../QTFlexibleIsosurfaces_Modified/Graphs/SplitTree.h \
    ../../../../QTFlexibleIsosurfaces_Modified/Graphs/SuperarcBase.h \
    ../../../../QTFlexibleIsosurfaces_Modified/Graphs/SupernodeBase.h \
    ../../../../QTFlexibleIsosurfaces_Modified/Graphs/TreeBase.h \
    ../../../../QTFlexibleIsosurfaces_Modified/HeightField/HeightField3D.h
