QT += gui widgets

HEADERS += \
    ../../Reeb/Points_4.h \
    ../../Reeb/RbFormat.h \
    ../../Common/Common.h \
    ../../Common/ArgParser.h \
    ../../Common/ExitCodes.h \
    ../../Common/Usage.h \
    ../../../../QTFlexibleIsosurfaces_Modified/HeightField/HeightField3D.h \
    ../../../../QTFlexibleIsosurfaces_Modified/Graphs/ContourTree.h \
    ../../../../QTFlexibleIsosurfaces_Modified/Graphs/ContourTreeSuperarc.h \
    ../../../../QTFlexibleIsosurfaces_Modified/Graphs/ContourTreeSupernode.h \
    ../../../../QTFlexibleIsosurfaces_Modified/Graphs/JoinTree.h \
    ../../../../QTFlexibleIsosurfaces_Modified/Graphs/MergeTree.h \
    ../../../../QTFlexibleIsosurfaces_Modified/Graphs/rb.h \
    ../../../../QTFlexibleIsosurfaces_Modified/Graphs/ReebGraph.h \
    ../../../../QTFlexibleIsosurfaces_Modified/Graphs/ReebGraphSuperarc.h \
    ../../../../QTFlexibleIsosurfaces_Modified/Graphs/ReebGraphSupernode.h \
    ../../../../QTFlexibleIsosurfaces_Modified/Graphs/SplitTree.h \
    ../../../../QTFlexibleIsosurfaces_Modified/Graphs/SuperarcBase.h \
    ../../../../QTFlexibleIsosurfaces_Modified/Graphs/SupernodeBase.h \
    ../../../../QTFlexibleIsosurfaces_Modified/Graphs/TreeBase.h \
    ../../../../QTFlexibleIsosurfaces_Modified/HeightField/NeighbourQueue/NeighbourFunctor6.h \
    ../../../../QTFlexibleIsosurfaces_Modified/HeightField/NeighbourQueue/NeighbourFunctor6P.h \
    ../../../../QTFlexibleIsosurfaces_Modified/HeightField/NeighbourQueue/NeighbourFunctor18.h \
    ../../../../QTFlexibleIsosurfaces_Modified/HeightField/NeighbourQueue/NeighbourFunctor18P.h \
    ../../../../QTFlexibleIsosurfaces_Modified/HeightField/NeighbourQueue/NeighbourQueue.h \
    ../../../../QTFlexibleIsosurfaces_Modified/Contour/Generation/CellSurface.h \
    ../../../../QTFlexibleIsosurfaces_Modified/Contour/Generation/ContourFactory.h \
    ../../../../QTFlexibleIsosurfaces_Modified/Contour/Generation/FollowCubeTables.h \
    ../../../../QTFlexibleIsosurfaces_Modified/Contour/Generation/PolygonFactory2.h \
    ../../../../QTFlexibleIsosurfaces_Modified/Contour/Generation/PolygonFactory3.h \
    ../../../../QTFlexibleIsosurfaces_Modified/Contour/Generation/PolylineList.h \
    ../../../../QTFlexibleIsosurfaces_Modified/CommandLine/ArgumentParser.h \
    ../../../../QTFlexibleIsosurfaces_Modified/CommandLine/CommandLine.h \
    ../../../../QTFlexibleIsosurfaces_Modified/CommandLine/QtCommandLineWorker.h \
    ../../../../QTFlexibleIsosurfaces_Modified/CommandLine/RegexCommandLineParser.h \
    ../../../../QTFlexibleIsosurfaces_Modified/CommandLine/Switch.h \
    ../../../../QTFlexibleIsosurfaces_Modified/Contour/Storage/Contour.h \
    ../../../../QTFlexibleIsosurfaces_Modified/Contour/Storage/ContourList.h \
    ../../../../QTFlexibleIsosurfaces_Modified/Contour/Storage/eigenvector.h \
    ../../../../QTFlexibleIsosurfaces_Modified/Contour/Storage/MeshCache.h \
    ../../../../QTFlexibleIsosurfaces_Modified/Contour/Storage/MeshTriangle.h \
    ../../../../QTFlexibleIsosurfaces_Modified/Contour/Storage/MeshVertex.h \
    ../../../../QTFlexibleIsosurfaces_Modified/DataExploration/Annotation.h \
    ../../../../QTFlexibleIsosurfaces_Modified/DataExploration/IsovalueBookmark.h \
    ../../../../QTFlexibleIsosurfaces_Modified/DataExploration/IsovalueBookmarkList.h \
    ../../../../QTFlexibleIsosurfaces_Modified/Geometry/Globals/CompilerDefines.h \
    ../../../../QTFlexibleIsosurfaces_Modified/Geometry/Globals/Exceptions.h \
    ../../../../QTFlexibleIsosurfaces_Modified/Geometry/Globals/Functions.h \
    ../../../../QTFlexibleIsosurfaces_Modified/Geometry/Globals/Globals.h \
    ../../../../QTFlexibleIsosurfaces_Modified/Geometry/Globals/QtFunctions.h \
    ../../../../QTFlexibleIsosurfaces_Modified/Geometry/Globals/QtOstream.h \
    ../../../../QTFlexibleIsosurfaces_Modified/Geometry/Globals/stl_ostream.h \
    ../../../../QTFlexibleIsosurfaces_Modified/Geometry/Globals/TypeDefines.h \
    ../../../../QTFlexibleIsosurfaces_Modified/Geometry/Geometry.h \
    ../../../../QTFlexibleIsosurfaces_Modified/Geometry/LineSegment2.h \
    ../../../../QTFlexibleIsosurfaces_Modified/Geometry/LineSegment3.h \
    ../../../../QTFlexibleIsosurfaces_Modified/Geometry/Polygon2.h \
    ../../../../QTFlexibleIsosurfaces_Modified/Geometry/Polygon3.h \
    ../../../../QTFlexibleIsosurfaces_Modified/Geometry/SimpleVector3.h \
    ../../../../QTFlexibleIsosurfaces_Modified/Geometry/Triangle2.h \
    ../../../../QTFlexibleIsosurfaces_Modified/Geometry/Triangle3.h \
    ../../../../QTFlexibleIsosurfaces_Modified/Geometry/Vector2.h \
    ../../../../QTFlexibleIsosurfaces_Modified/Geometry/Vector3.h \
    ../../../../QTFlexibleIsosurfaces_Modified/Geometry/VertexTriple.h \
    ../../../../QTFlexibleIsosurfaces_Modified/Globals/CompilerDefines.h \
    ../../../../QTFlexibleIsosurfaces_Modified/Globals/Exceptions.h \
    ../../../../QTFlexibleIsosurfaces_Modified/Globals/Functions.h \
    ../../../../QTFlexibleIsosurfaces_Modified/Globals/Globals.h \
    ../../../../QTFlexibleIsosurfaces_Modified/Globals/QtFunctions.h \
    ../../../../QTFlexibleIsosurfaces_Modified/Globals/QtOstream.h \
    ../../../../QTFlexibleIsosurfaces_Modified/Globals/stl_ostream.h \
    ../../../../QTFlexibleIsosurfaces_Modified/Globals/TypeDefines.h \
    ../../../../QTFlexibleIsosurfaces_Modified/HeightField/Component.h \
    ../../../../QTFlexibleIsosurfaces_Modified/HeightField/HeightField4D.h \
    ../../../../QTFlexibleIsosurfaces_Modified/HeightField/HeightSort.h \
    ../../../../QTFlexibleIsosurfaces_Modified/HeightField/PriorityIndex.h \
    ../../../../QTFlexibleIsosurfaces_Modified/libDesignPatterns/AbstractObservable.h \
    ../../../../QTFlexibleIsosurfaces_Modified/libDesignPatterns/AbstractObserver.h \
    ../../../../QTFlexibleIsosurfaces_Modified/libDesignPatterns/DesignPatterns.h \
    ../../../../QTFlexibleIsosurfaces_Modified/libDesignPatterns/ICommand.h \
    ../../../../QTFlexibleIsosurfaces_Modified/libDesignPatterns/IObservable.h \
    ../../../../QTFlexibleIsosurfaces_Modified/libDesignPatterns/IObserver.h \
    ../../../../QTFlexibleIsosurfaces_Modified/libDesignPatterns/MacroCommand.h \
    ../../../../QTFlexibleIsosurfaces_Modified/Parallel/ThreadQueue.h \
    ../../../../QTFlexibleIsosurfaces_Modified/Statistics/SuperarcDistribution.h \
    ../../../../QTFlexibleIsosurfaces_Modified/Statistics/SupernodeDistribution.h \
    ../../../../QTFlexibleIsosurfaces_Modified/DataModel.h \
    ../../../../QTFlexibleIsosurfaces_Modified/processinfo.h \
    ../../../../QTFlexibleIsosurfaces_Modified/ProgramParameters.h

SOURCES += \
    ../../Reeb/main.cpp \
    ../../Reeb/Points_4.cpp \
    ../../../../QTFlexibleIsosurfaces_Modified/HeightField/HeightField3D.cpp \
    ../../../../QTFlexibleIsosurfaces_Modified/Graphs/ContourTree.cpp \
    ../../../../QTFlexibleIsosurfaces_Modified/Graphs/ContourTreeSuperarc.cpp \
    ../../../../QTFlexibleIsosurfaces_Modified/Graphs/ContourTreeSupernode.cpp \
    ../../../../QTFlexibleIsosurfaces_Modified/Graphs/JoinTree.cpp \
    ../../../../QTFlexibleIsosurfaces_Modified/Graphs/MergeTree.cpp \
    ../../../../QTFlexibleIsosurfaces_Modified/Graphs/rb.cpp \
    ../../../../QTFlexibleIsosurfaces_Modified/Graphs/ReebGraph.cpp \
    ../../../../QTFlexibleIsosurfaces_Modified/Graphs/ReebGraphSuperarc.cpp \
    ../../../../QTFlexibleIsosurfaces_Modified/Graphs/ReebGraphSupernode.cpp \
    ../../../../QTFlexibleIsosurfaces_Modified/Graphs/SplitTree.cpp \
    ../../../../QTFlexibleIsosurfaces_Modified/HeightField/NeighbourQueue/NeighbourQueue.cpp \
    ../../../../QTFlexibleIsosurfaces_Modified/Contour/Generation/ContourFactory.cpp \
    ../../../../QTFlexibleIsosurfaces_Modified/Contour/Generation/FollowCubeTables.cpp \
    ../../../../QTFlexibleIsosurfaces_Modified/Contour/Generation/PolygonFactory2.cpp \
    ../../../../QTFlexibleIsosurfaces_Modified/Contour/Generation/PolygonFactory3.cpp \
    ../../../../QTFlexibleIsosurfaces_Modified/Contour/Generation/PolylineList.cpp \
    ../../../../QTFlexibleIsosurfaces_Modified/CommandLine/ArgumentParser.cpp \
    ../../../../QTFlexibleIsosurfaces_Modified/CommandLine/QtCommandLineWorker.cpp \
    ../../../../QTFlexibleIsosurfaces_Modified/CommandLine/RegexCommandLineParser.cpp \
    ../../../../QTFlexibleIsosurfaces_Modified/Contour/Storage/Contour.cpp \
    ../../../../QTFlexibleIsosurfaces_Modified/Contour/Storage/MeshCache.cpp \
    ../../../../QTFlexibleIsosurfaces_Modified/Contour/Storage/MeshTriangle.cpp \
    ../../../../QTFlexibleIsosurfaces_Modified/DataExploration/IsovalueBookmark.cpp \
    ../../../../QTFlexibleIsosurfaces_Modified/DataExploration/IsovalueBookmarkList.cpp \
    ../../../../QTFlexibleIsosurfaces_Modified/HeightField/HeightField4D.cpp \
    ../../../../QTFlexibleIsosurfaces_Modified/HeightField/HeightSort.cpp \
    ../../../../QTFlexibleIsosurfaces_Modified/libDesignPatterns/AbstractObservable.cpp \
    ../../../../QTFlexibleIsosurfaces_Modified/libDesignPatterns/AbstractObserver.cpp \
    ../../../../QTFlexibleIsosurfaces_Modified/libDesignPatterns/MacroCommand.cpp \
    ../../../../QTFlexibleIsosurfaces_Modified/Parallel/ThreadQueue.cpp \
    ../../../../QTFlexibleIsosurfaces_Modified/Statistics/SuperarcDistribution.cpp \
    ../../../../QTFlexibleIsosurfaces_Modified/Statistics/SupernodeDistribution.cpp \
    ../../../../QTFlexibleIsosurfaces_Modified/DataModel.cpp \
    ../../../../QTFlexibleIsosurfaces_Modified/processinfo.cpp

unix:!macx: LIBS += -L/usr/lib/x86_64-linux-gnu/ -lvtkFiltersReebGraph-6.3 -lvtkCommonComputationalGeometry-6.3 -lvtkCommonCore-6.3 -lvtkCommonDataModel-6.3 -lvtkCommonExecutionModel-6.3 -lvtkFiltersGeneral-6.3 -lvtkFiltersGeometry-6.3
#-lvtkInfovisBoostGraphAlgorithms-6.3
#vtkalglib-6.1
#vtkChartsCore-6.1
#vtkCommonColor-6.1
#vtkCommonExecutionModel-6.1
#vtkCommonMath-6.1
#vtkCommonMisc-6.1
#vtkCommonSystem-6.1
#vtkCommonTransforms-6.1
#vtkDICOMParser-6.1
#vtkDomainsChemistry-6.1
#vtkexoIIc-6.1
#vtkexpat-6.1
#vtkFiltersAMR-6.1
#vtkFiltersCore-6.1
#vtkFiltersExtraction-6.1
#vtkFiltersFlowPaths-6.1
#vtkFiltersGeneral-6.1
#vtkFiltersGeneric-6.1
#vtkFiltersGeometry-6.1
#vtkFiltersHybrid-6.1
#vtkFiltersHyperTree-6.1
#vtkFiltersImaging-6.1
#vtkFiltersModeling-6.1
#vtkFiltersParallel-6.1.lib;vtkFiltersParallelImaging-6.1.lib;vtkFiltersProgrammable-6.1.lib;vtkFiltersReebGraph-6.1.lib;vtkFiltersSelection-6.1.lib;vtkFiltersSMP-6.1.lib;vtkFiltersSources-6.1.lib;vtkFiltersStatistics-6.1.lib;vtkFiltersTexture-6.1.lib;vtkFiltersVerdict-6.1.lib;vtkfreetype-6.1.lib;vtkftgl-6.1.lib;vtkGeovisCore-6.1.lib;vtkgl2ps-6.1.lib;vtkhdf5-6.1.lib;vtkhdf5_hl-6.1.lib;vtkImagingColor-6.1.lib;vtkImagingCore-6.1.lib;vtkImagingFourier-6.1.lib;vtkImagingGeneral-6.1.lib;vtkImagingHybrid-6.1.lib;vtkImagingMath-6.1.lib;vtkImagingMorphological-6.1.lib;vtkImagingSources-6.1.lib;vtkImagingStatistics-6.1.lib;vtkImagingStencil-6.1.lib;vtkInfovisCore-6.1.lib;vtkInfovisLayout-6.1.lib;vtkInteractionImage-6.1.lib;vtkInteractionStyle-6.1.lib;vtkInteractionWidgets-6.1.lib;vtkIOAMR-6.1.lib;vtkIOCore-6.1.lib;vtkIOEnSight-6.1.lib;vtkIOExodus-6.1.lib;vtkIOExport-6.1.lib;vtkIOGeometry-6.1.lib;vtkIOImage-6.1.lib;vtkIOImport-6.1.lib;vtkIOInfovis-6.1.lib;vtkIOLegacy-6.1.lib;vtkIOLSDyna-6.1.lib;vtkIOMINC-6.1.lib;vtkIOMovie-6.1.lib;vtkIONetCDF-6.1.lib;vtkIOParallel-6.1.lib;vtkIOPLY-6.1.lib;vtkIOSQL-6.1.lib;vtkIOVideo-6.1.lib;vtkIOXML-6.1.lib;vtkIOXMLParser-6.1.lib;vtkjpeg-6.1.lib;vtkjsoncpp-6.1.lib;vtklibxml2-6.1.lib;vtkmetaio-6.1.lib;vtkNetCDF-6.1.lib;vtkNetCDF_cxx-6.1.lib;vtkoggtheora-6.1.lib;vtkParallelCore-6.1.lib;vtkpng-6.1.lib;vtkproj4-6.1.lib;vtkRenderingAnnotation-6.1.lib;vtkRenderingContext2D-6.1.lib;vtkRenderingOpenGL-6.1.lib;vtkRenderingCore-6.1.lib;vtkRenderingFreeType-6.1.lib;vtkRenderingGL2PS-6.1.lib;vtkRenderingImage-6.1.lib;vtkRenderingLabel-6.1.lib;vtkRenderingLIC-6.1.lib;vtkRenderingLOD-6.1.lib;vtkRenderingVolume-6.1.lib;vtkRenderingVolumeOpenGL-6.1.lib;vtksqlite-6.1.lib;vtksys-6.1.lib;vtktiff-6.1.lib;vtkverdict-6.1.lib;vtkViewsContext2D-6.1.lib;vtkViewsCore-6.1.lib;vtkViewsInfovis-6.1.lib;vtkzlib-6.1.lib

INCLUDEPATH += /usr/include/vtk-6.3
DEPENDPATH += /usr/include/vtk-6.3

DEFINES += NO_DATA_MODEL
#   Version 1.1 introduces positional information and normalised values for vertices
#   Version 1.2 introduces persistence information for arcs (this code is called as the
#   graphs don't seem torequire simplification...
DEFINES += TARGET_RB_VERSION=1.1

#   Enable c++11 support (std::array)
CONFIG  +=  c++11
CONFIG  +=  c++14

#   Location of Common.h
INCLUDEPATH += "/home/dean/qcdvis/CommandLineTools/CommandLineTools/Common"
DEPENDPATH += "/home/dean/qcdvis/CommandLineTools/CommandLineTools/Common"

#   Location of vtk_ostream.h
INCLUDEPATH += "/home/dean/qcdvis/vtkReebGraphTest/vtkReebGraphTest"
DEPENDPATH += "/home/dean/qcdvis/vtkReebGraphTest/vtkReebGraphTest"

#   Geometry clases
INCLUDEPATH += "/home/dean/qcdvis/Common Classes"
DEPENDPATH += "/home/dean/qcdvis/Common Classes"

#   Heightfield classes
INCLUDEPATH += "/home/dean/qcdvis/QTFlexibleIsosurfaces_Modified/HeightField"
DEPENDPATH += "/home/dean/qcdvis/QTFlexibleIsosurfaces_Modified/HeightField"

#   Imports from main application
INCLUDEPATH += "/home/dean/qcdvis/QTFlexibleIsosurfaces_Modified"
DEPENDPATH += "/home/dean/qcdvis/QTFlexibleIsosurfaces_Modified"
