#pragma once

//	Defines common exit codes for command line tools
enum ExitCodes
{
	EXIT_OK,				//	Successful execution
	EXIT_INVALID_ARGUMENTS, //	One or more of the supplied arguments was invalid and stopped execution
	EXIT_INPUT_ERROR,		//	There was a problem processing the input
	EXIT_OUTPUT_ERROR,		//	There was a problem writing the output

	EXIT_FAIL = 255			//	Generic error
};

