#pragma once

#include <string>
#include <sstream>
#include <algorithm>
#include <vector>
#include <iomanip>

struct Switch
{
	std::string param;
	std::string description;
	bool optional;
	//	No provision for sub parameters (so far)
	//	No provision for supressing switch char (eg for filenames)
	Switch(const std::string param, const std::string& description, const bool optional = true)
		: param { param }, description { description }, optional { optional }
	{}
};

///
///	\brief		Prints a command usage string similar in form to that used
///				for DIR or MORE
///	\author		Dean
///	\since		02-06-2016
///
std::string getUsageStringWindows(const std::string& exeName, 
				const std::string& description, 
				const std::vector<Switch>& switches, 
				const std::string& footer = "")
{
	using namespace std;

	//	Maximum number of characters before a description
	//	will be printed on the line following the switch
	constexpr auto MAX_PARAM_LENGTH_INLINE = 10;

	constexpr auto NEW_LINE = '\n';
	const auto INDENT = "  ";

	string exeNameCaps;
	stringstream result;

	//	Finds the largest field we'll need to represent the switches specified
	auto findMaxSwitchLength = [&](const vector<Switch>& switches)
	{
		auto max = 0ul;
		for (auto& s : switches)
		{
			auto len = s.param.length();

			if ((len > max) && (len <MAX_PARAM_LENGTH_INLINE))
				max = len;
		}
		return max;
	};

	//	Calculate field width
	const auto MAX_SWITCH_WIDTH = findMaxSwitchLength(switches);

	//	We want to convert the command to uppercase for display
	exeNameCaps.resize(exeName.size());
    //std::transform(exeName.cbegin(), exeName.cend(), exeNameCaps.begin(), std::toupper);

	//	Write the header
	result << description << NEW_LINE << NEW_LINE;
	
	//	Write the command line parameters
	result << exeNameCaps << ' ';
	for (auto& s : switches)
	{
		constexpr auto SWITCH_CHAR = "/";
		constexpr auto OPT_OPEN = "[";
		constexpr auto OPT_CLOSE = "]";

		//	examples
		//	[/a] /N /YES [/123]
		result << (s.optional ? OPT_OPEN : "")
			<< SWITCH_CHAR << s.param
			<< (s.optional ? OPT_CLOSE : "") << " ";
	}
	result << NEW_LINE << NEW_LINE;

	//	List the paremetrs and descriptions
	for (auto& s : switches)
	{
		constexpr auto SWITCH_CHAR = "/";

		if (s.param.length() < MAX_PARAM_LENGTH_INLINE)
		{
			//	Print the description inline
			result << INDENT << setw(MAX_SWITCH_WIDTH + 1) << (SWITCH_CHAR + s.param);
			result << INDENT << s.description << NEW_LINE;
		}
		else
		{
			//	Print the description on the next line
			result << INDENT << (SWITCH_CHAR + s.param) << NEW_LINE;
			result << INDENT << setw(MAX_SWITCH_WIDTH + 1) << " "
				   << INDENT << s.description << NEW_LINE;
		}
	}

	//	Write the footer
	if (footer != "")
	{
		result << footer << NEW_LINE << NEW_LINE;
	}

	return result.str();
}

///
///	\brief		Prints a command usage string similar in form to that used
///				on the current platform
///	\author		Dean
///	\since		02-06-2016
///
std::string getUsageString(const std::string& exeName, 
						   const std::string& description, 
						   const std::vector<Switch>& switches, 
						   const std::string& footer = "")
{
	//	Do one for LINUX/UNIX later...
	return getUsageStringWindows(exeName, description, switches, footer);
}
