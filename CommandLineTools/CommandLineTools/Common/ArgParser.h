#pragma once

#include <string>
#include <vector>
#include <regex>
#include <cassert>
#include <iostream>

struct ArgParser
{
	//friend std::ostream& operator<<(std::ostream& os, const ArgParser& argParser);

	//	Used to extract just the alphanumeric part of a switch (ignores leading slashes and dashes)
	const std::string SWITCH_REGEX = R"|([\-{0,2}\\/]*(.+))|";
	std::regex m_switchRegex;

	//	This contains a list of switches without suffixes
	std::vector<std::string> m_args;

	///
	///	\brief		...
	///	\author		Dean
	///	\since		02-06-2016
	///
	ArgParser()
	{
		m_switchRegex = std::regex(SWITCH_REGEX);
	}

	///
	///	\brief		...
	///	\author		Dean
	///	\since		02-06-2016
	///
	ArgParser(const size_t count, char* args[])
		: ArgParser()
	{
		parseInput(std::vector<std::string>(&args[0], &args[count]));
	}

	///
	///	\brief		...
	///	\author		Dean
	///	\since		02-06-2016
	///
	ArgParser(const std::vector<std::string>& args)
		: ArgParser()
	{
		parseInput(args);
	}

	///
	///	\brief		...
	///	\author		Dean
	///	\since		02-06-2016
	///
	void parseInput(const std::vector<std::string>& args)
	{
		for (auto& s : args)
		{
			std::smatch matches;

			if (std::regex_match(s, matches,  m_switchRegex))
			{
				assert(matches.size() == 2);

				//	Switch stripped of delimiters
				m_args.push_back(matches[1]);
			}
		}
	}

	///
	///	\brief		...
	///	\author		Dean
	///	\since		02-06-2016
	///
	bool contains(const std::string& param, const bool caseSensitive = true) const
	{
		//	Do a bruteforce check for now (in the
		//	future we'll parse the input into a map
		//	using regular expressions maybe).
		for (auto& s : m_args)
		{
			if (caseSensitive)
			{
				//	Look for an exact match
				if (s == param) return true;
			}
			else
			{
				//	Convert both to uppercase first
				auto uParam = param;
				std::transform(uParam.begin(), uParam.end(), uParam.begin(), toupper);
				auto uS = s;
				std::transform(uS.begin(), uS.end(), uS.begin(), toupper);

				//	Now compare
				if (uParam == uS) return true;
			}
		}
		return false;
	}
};


///
///	\brief		...
///	\author		Dean
///	\since		02-06-2016
///
std::ostream& operator<<(std::ostream& os, const ArgParser& argParser)
{
	os << "ArgParser = { ";

	for (auto it = argParser.m_args.cbegin(); it != argParser.m_args.cend(); ++it)
	{
		if (it != argParser.m_args.cbegin()) os << ", ";

		os << *it;
	}

	os << " }";
	return os;
}