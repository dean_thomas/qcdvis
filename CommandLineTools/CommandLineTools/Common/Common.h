#pragma once

#include <iostream>
#include <regex>
#include <string>

#include "ExitCodes.h"
#include "Usage.h"
#include "ArgParser.h"

struct split_path_parts
{
	std::string full_path;					//	Should match input
	std::string absolute_path;				//	Path upto, but not including filename.  Will default to . if not found
	std::string filename_with_extension;	//	filename with extension
	std::string filename_without_extension;	//	just the filename
	std::string extension;					//	just the extension (with dot)
};

split_path_parts split_path_no_extension(const std::string& full_path)
{
	std::regex full_path_regex(R"|((.*)[/\\](.*))|");
	std::smatch matches;

	if (std::regex_match(full_path, matches, full_path_regex))
	{
		assert(matches.size() == 3);

		//	(1: string = absolute path to file)
		//	(2: string = filename)
		return { matches[0], matches[1], matches[2], matches[2], "" };
	}
	else return { "", "", "", "", "" };
}

split_path_parts split_path(const std::string& full_path)
{
	std::regex full_path_regex(R"|((.*)[/\\]((.*)(\..*)))|");
	std::regex no_path_regex(R"|(((.*)(\..*)))|");
	std::smatch matches;

	if (std::regex_match(full_path, matches, full_path_regex))
	{
		assert(matches.size() == 5);

		//	(1: string = absolute path)
		//	(2: string = filename.ext)
		//	(3: string = filename)
		//	(4: string = .ext)
		return { matches[0], matches[1], matches[2], matches[3], matches[4] };
	}
	else if (std::regex_match(full_path, matches, no_path_regex))
	{
		assert(matches.size() == 4);

		//	(1: string = filename.ext)
		//	(2: string = filename)
		//	(3: string = .ext)
		return { matches[0], ".", matches[1], matches[2], matches[3] };
	}
	else return { "", "", "", "", "" };
}

void pause_before_exit()
{
#if defined(_DEBUG) || defined(PAUSE_ON_EXIT)
	std::cout << "Press <RETURN> to exit." << std::endl;
	getchar();
#endif
}