/*=========================================================================

Program:   Visualization Toolkit
Module:    TestGraph.cxx

Copyright (c) Ken Martin, Will Schroeder, Bill Lorensen
All rights reserved.
See Copyright.txt or http://www.kitware.com/Copyright.htm for details.

This software is distributed WITHOUT ANY WARRANTY; without even
the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the above copyright notice for more information.

=========================================================================*/
/*-------------------------------------------------------------------------
Copyright 2008 Sandia Corporation.
Under the terms of Contract DE-AC04-94AL85000 with Sandia Corporation,
the U.S. Government retains certain rights in this software.
-------------------------------------------------------------------------*/

#include <vtkAdjacentVertexIterator.h>
#include <vtkDataSetAttributes.h>
#include <vtkDirectedAcyclicGraph.h>
#include <vtkEdgeListIterator.h>
#include <vtkIdTypeArray.h>
#include <vtkInEdgeIterator.h>
#include <vtkIntArray.h>
#include <vtkMutableDirectedGraph.h>
#include <vtkMutableUndirectedGraph.h>
#include <vtkOutEdgeIterator.h>
#include <vtkSmartPointer.h>
#include <vtkTree.h>
#include <vtkVertexListIterator.h>

#define VTK_CREATE(type, name) \
  vtkSmartPointer<type> name = vtkSmartPointer<type>::New()

void printGraph(vtkMutableDirectedGraph* graph)
{
	assert(graph != nullptr);

	vtkVertexListIterator* vertexIt = vtkVertexListIterator::New();
	graph->GetVertices(vertexIt);
	while (vertexIt->HasNext())
	{
		auto vertex = vertexIt->Next();
		auto inDegree = graph->GetInDegree(vertex);
		auto outDegree = graph->GetOutDegree(vertex);
		auto degree = inDegree + outDegree;
		cout << "v" << vertex << ": degree " << degree << "(in: " << inDegree << ", out: " << outDegree << ")" << endl;
	}
}

/*
		Tests to see if there are any non-critical vertices
		in the graph
*/
bool graphCanBeReduced(vtkMutableDirectedGraph* graph)
{
	vtkVertexListIterator* vertexIt = vtkVertexListIterator::New();
	graph->GetVertices(vertexIt);
	while (vertexIt->HasNext())
	{
		auto vertex = vertexIt->Next();
		auto inDegree = graph->GetInDegree(vertex);
		auto outDegree = graph->GetOutDegree(vertex);
		auto degree = inDegree + outDegree;
		
		if ((inDegree == 1) && (outDegree == 1)) return true;
	}

	return false;
}

/*
		Reduces the graph by collapsing a single vertex to an edge
*/
void reductionStep(vtkMutableDirectedGraph* graph)
{
	vtkVertexListIterator* vertexIt = vtkVertexListIterator::New();
	graph->GetVertices(vertexIt);
	while (vertexIt->HasNext())
	{
		auto vertex = vertexIt->Next();
		auto inDegree = graph->GetInDegree(vertex);
		auto outDegree = graph->GetOutDegree(vertex);

		//	Look for a regular vertex
		if ((inDegree == 1) && (outDegree == 1))
		{
			vtkIdType newSource;
			vtkIdType newTarget;

			//	We should only have 1 available incoming edge
			vtkInEdgeIterator* edgeInIt = vtkInEdgeIterator::New();
			graph->GetInEdges(vertex, edgeInIt);
			while (edgeInIt->HasNext())
			{
				auto eIn = edgeInIt->Next();

				//	Get the source vertex of the edge
				newSource = eIn.Source;
			}

			//	We should only have 1 available outgoing edge
			vtkOutEdgeIterator* edgeOutIt = vtkOutEdgeIterator::New();
			graph->GetOutEdges(vertex, edgeOutIt);
			while (edgeOutIt->HasNext())
			{
				auto eOut = edgeOutIt->Next();
				
				//	Get the target vertex of the vertex
				newTarget = eOut.Target;
			}

			//	Create the new edge to by-pass this vertex
			graph->AddEdge(newSource, newTarget);

			//	And delete the vertex
			graph->RemoveVertex(vertex);

			//	Iterators have probably become
			//	invalid, just exit and they can
			//	be re-initialised next time around
			break;
		}
	}
}

int TestGraph(int vtkNotUsed(argc), char* vtkNotUsed(argv)[])
{
	int errors = 0;

	VTK_CREATE(vtkMutableDirectedGraph, mdgTree);
	
	for (vtkIdType i = 0; i < 10; ++i)
	{
		mdgTree->AddVertex();
	}

	// Create a valid tree.
	mdgTree->AddEdge(0, 1);	//	e0
	mdgTree->AddEdge(0, 2);	//	e1
	mdgTree->AddEdge(1, 3);	//	e2
	mdgTree->AddEdge(1, 4);	//	e3
	mdgTree->AddEdge(2, 5);	//	e4
	mdgTree->AddEdge(3, 6);	//	e5
	mdgTree->AddEdge(4, 7);	//	e6
	mdgTree->AddEdge(5, 8);	//	e7
	mdgTree->AddEdge(5, 9);	//	e8

	cout << "Original graph" << endl;
	printGraph(mdgTree);

	while (graphCanBeReduced(mdgTree))
	{
		reductionStep(mdgTree);
	}
	cout << "Reduced graph" << endl;
	printGraph(mdgTree);

	/*
	vtkVertexListIterator* vertexIt = vtkVertexListIterator::New();
	mdgTree->GetVertices(vertexIt);
	while (vertexIt->HasNext())
	{
		auto vertex = vertexIt->Next();
		auto degree = mdgTree->GetInDegree(vertex) + mdgTree->GetOutDegree(vertex);
		cout << "v" << vertex << ": degree " << degree;

		if (degree == 2)
		{
			vtkIdType newSource;
			vtkIdType newTarget;

			vtkInEdgeIterator* edgeInIt = vtkInEdgeIterator::New();
			mdgTree->GetInEdges(vertex, edgeInIt);

			while (edgeInIt->HasNext())
			{
				auto eIn = edgeInIt->Next();
				cout << "e" << eIn.Source;

				newSource = eIn.Source;
			}

			cout << " -> ";

			vtkOutEdgeIterator* edgeOutIt = vtkOutEdgeIterator::New();
			mdgTree->GetOutEdges(vertex, edgeOutIt);

			while (edgeOutIt->HasNext())
			{
				auto eOut = edgeOutIt->Next();
				cout << "e" << eOut.Target;

				newTarget = eOut.Target;
			}
		}

		cout << endl;
	}
	*/

	return errors;
}

int main(int argc, char* argv[])
{
	TestGraph(argc, argv);

	getchar();
}