#pragma once
#ifndef JCN_3D_H
#define JCN_3D_H

#include <vtkSmartPointer.h>
#include <vtkFloatArray.h>
#include <vtkIdTypeArray.h>

namespace JCN_3d
{
	vtkSmartPointer<vtkDataArray> generateGeometryArray()
	{
		constexpr size_t dims = 3;

		//	I think this represents the position of cells in space?
		vtkSmartPointer<vtkDataArray> pointsArray = vtkSmartPointer<vtkFloatArray>::New();

		//	We'll try for R^2 space
		pointsArray->SetNumberOfComponents(3);
		pointsArray->SetName("geometry");

		for (float z = 0.0f; z <= 2.0f; z += 1.0f)
		{
			for (float y = 0.0f; y <= 2.0f; y += 1.0f)
			{
				for (float x = 0.0f; x <= 2.0f; x += 1.0f)
				{
					float coord[dims] = { x, y, z };
					pointsArray->InsertNextTuple(coord);
				}
			}
		}
		return pointsArray;
	}

	vtkSmartPointer<vtkIdTypeArray> freudenthalDecomposition()
	{
		//	For 4D this seems to be 5? (vertices in a pentachoron maybe?)
		constexpr size_t M = 4;

		//	Core data - number of components represents M (spatial dimension)
		//	in the algorithm
		auto topoArray = vtkSmartPointer<vtkIdTypeArray>::New();
		topoArray->SetName("topology");
		topoArray->SetNumberOfComponents(M);

		for (auto i = 0; i < 8; ++i)
		{
			//	Try inserting something into the array
			vtkIdType cell0[M] = { 0, 1, 5, 2 };
			vtkIdType cell1[M] = { 1, 2, 3, 5 };
			vtkIdType cell2[M] = { 0, 2, 5, 4 };
			vtkIdType cell3[M] = { 3, 2, 7, 5 };
			vtkIdType cell4[M] = { 6, 7, 5, 2 };
			vtkIdType cell5[M] = { 2, 4, 5, 6 };

			topoArray->InsertNextTupleValue(cell0);
			topoArray->InsertNextTupleValue(cell1);
			topoArray->InsertNextTupleValue(cell2);
			topoArray->InsertNextTupleValue(cell3);
			topoArray->InsertNextTupleValue(cell4);
			topoArray->InsertNextTupleValue(cell5);
		}
		return topoArray;
	}

	vtkSmartPointer<vtkIdTypeArray> generateTopologyArray()
	{
		//	For 4D this seems to be 5? (vertices in a pentachoron maybe?)
		constexpr size_t M = 4;

		//	Core data - number of components represents M (spatial dimension)
		//	in the algorithm
		auto topoArray = vtkSmartPointer<vtkIdTypeArray>::New();
		topoArray->SetName("topology");
		topoArray->SetNumberOfComponents(M);

		//	Try inserting something into the array
		vtkIdType cell0[M] = { 3, 0, 4 };
		vtkIdType cell1[M] = { 4, 0, 1 };
		vtkIdType cell2[M] = { 4, 1, 2 };
		vtkIdType cell3[M] = { 4, 2, 5 };
		vtkIdType cell4[M] = { 6, 3, 4 };
		vtkIdType cell5[M] = { 6, 4, 7 };
		vtkIdType cell6[M] = { 7, 4, 8 };
		vtkIdType cell7[M] = { 8, 4, 5 };

		topoArray->InsertNextTupleValue(cell0);
		topoArray->InsertNextTupleValue(cell1);
		topoArray->InsertNextTupleValue(cell2);
		topoArray->InsertNextTupleValue(cell3);
		topoArray->InsertNextTupleValue(cell4);
		topoArray->InsertNextTupleValue(cell5);
		topoArray->InsertNextTupleValue(cell6);
		topoArray->InsertNextTupleValue(cell7);

		return topoArray;
	}

	vtkSmartPointer<vtkFloatArray> generateScalarData0()
	{

	}
}

#endif