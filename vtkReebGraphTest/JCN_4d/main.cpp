#include <cstdio>
#include <string>
#include <array>
#include <iostream>
#include <fstream>



#include "C:\\Users\\4thFloor\\Documents\\Qt\\QtFlexibleIsosurfaces\\vtkReebGraphTest\\vtkReebGraphTest\\vtk_ostream_formats.h"


#include "vtkJointContourNet.h"
#include "vtkPolytopeGeometry.h"
#include "vtkExtractReebGraph.h"


#include "JCN_2d.h"

#include <vtkPolyData.h>
#include <vtkSmartPointer.h>
#include <vtkFloatArray.h>
#include <vtkArrayData.h>
#include <vtkFieldData.h>
#include <vtkFileOutputWindow.h>
#include <vtkDataSet.h>
#include <vtkUnstructuredGrid.h>
#include <vtkVertex.h>
#include <vtkPolygon.h>
#include <vtkPolyhedron.h>
#include <vtkCellArray.h>
#include <vtkIntArray.h>
#include <vtkDataSetAttributes.h>
#include <vtkPointData.h>
#include <vtkDataArray.h>
#include <vtkUndirectedGraph.h>



int main(int argc, char* argv[])
{
	using namespace std;
	initializeErrorLogger("vtkErrorLog.txt");

	
	auto jcn = vtkSmartPointer<vtkJointContourNet>::New();

	//	Get an array representing the m-dimensional
	//	topology
	auto topologyArray = JCN_2d::generateTopologyArray();
	auto geometryArray = JCN_2d::generateGeometryArray();


	//	The algorithm expects the core data array to be castable
	//	to vtkDataSetAttributes - so wrap it here before
	//	it gets returned
	//auto attrib = vtkSmartPointer<vtkDataSetAttributes>::New();
	//attrib->AddArray(topoArray);
	//cout << "topology array is present: " << (attrib->HasArray("topology") ? "true" : "false") << endl;

	//	Not sure why this needs adding to new pointData (but if it
	//	isn't it seems to disappear?!)
	auto pointData = vtkPointData::New();
	//pd->AddArray(pointsArray);
	pointData->AddArray(topologyArray);

	//	The JCN algorithm requires data to be input as an unstructured grid
	//	Here we add our topology and geometry arrays to allow data in > R^3
	auto unstructuredGrid = vtkUnstructuredGrid::New();
	unstructuredGrid->SetFieldData(pointData);
	unstructuredGrid->GetPointData()->AddArray(geometryArray);

	//	Make sure nothing has gone missing
	cout << "Has topologoy: " << (unstructuredGrid->GetFieldData()->HasArray("topology") ? "true" : "false") << endl;
	cout << "Has Geometry: " << (unstructuredGrid->GetPointData()->HasArray("geometry") ? "true" : "false") << endl;




	auto testField0 = JCN_2d::generateScalarData0();
	auto testField1 = JCN_2d::generateScalarData1();
	unstructuredGrid->GetPointData()->AddArray(testField0);
	jcn->AddField(testField0->GetName(), 1.0f);
	unstructuredGrid->GetPointData()->AddArray(testField1);
	jcn->AddField(testField1->GetName(), 1.0);

	//	Set the data structure and tell the algorithm to use the
	//	supplied geometry and toplogy arrays
	jcn->AddInputDataObject(unstructuredGrid);
	jcn->SetTopologyArrayName("topology");
	jcn->SetUseCellTopologyArray(true);
	jcn->SetSpatialArrayName("geometry");
	jcn->SetUseSpatialCoordinateArray(true);
	jcn->CenterFirstSlabOn();

	//auto d_o = jcn->GetInputDataObject(0, 0);
	//cout << d_o->GetFieldData()->HasArray("topology") << endl;
	//jcn->SetSpatialArrayName("coordinates");
	//jcn->SetUseSpatialCoordinateArray(true);
	//jcn->AddInputConnection(z);

	//jcn->SetSpatialArrayName("coordinates");


	//auto polytopeGeometry = vtkSmartPointer<vtkPolytopeGeometry>::New();
	//polytopeGeometry->AddCoord()

	jcn->Update();

	auto jcnGraph = jcn->GetTopologyGraph();
	assert(jcnGraph != nullptr);

	cout << "JCN graph:" << endl 
		<< "\tv: " << jcnGraph->GetNumberOfVertices() << endl
		<< "\te: " << jcnGraph->GetNumberOfEdges() << endl;

	ofstream outFile(R"(C:\Users\4thFloor\Desktop\jcn2d\jcn_2d.dot)");
	outFile << DotFormat(jcnGraph) << endl;
	outFile.close();

	auto reebExtractor = vtkExtractReebGraph::New();
	for (auto i = 0; i < jcn->GetNumberOfFields(); ++i)
	{
		auto fieldName = jcn->GetField(i);
		reebExtractor->SetInputConnection(jcn->GetOutputPort(0));
		reebExtractor->SetFieldName(fieldName);
		reebExtractor->Update();

		auto reebGraph = reebExtractor->GetReebGraph();
		assert(reebGraph != nullptr);

		auto filePath = R"(C:\Users\4thFloor\Desktop\jcn2d\)" + string(fieldName) + R"(.dot)";

		ofstream outFile(filePath);
		outFile << DotFormat(reebGraph) << endl;
		outFile.close();
	}

	
	getchar();
	return 0;
}