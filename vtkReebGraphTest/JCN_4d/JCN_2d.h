#pragma once
#ifndef JCN_2D_H
#define JCN_2D_H

#include <vtkSmartPointer.h>
#include <vtkFloatArray.h>
#include <vtkIdTypeArray.h>

namespace JCN_2d
{
	vtkSmartPointer<vtkDataArray> generateGeometryArray()
	{
		constexpr size_t dims = 2;

		//	I think this represents the position of cells in space?
		vtkSmartPointer<vtkDataArray> pointsArray = vtkSmartPointer<vtkFloatArray>::New();

		//	We'll try for R^2 space
		pointsArray->SetNumberOfComponents(dims);
		pointsArray->SetName("geometry");

		for (float y = 0.0; y <= 2.0; y += 1.0)
		{
			for (float x = 0.0; x <= 2.0; x += 1.0)
			{
				float coord[dims] = { x, y };
				pointsArray->InsertNextTuple(coord);
			}
		}

		return pointsArray;
	}

	vtkSmartPointer<vtkIdTypeArray> generateTopologyArray()
	{
		//	For 4D this seems to be 5? (vertices in a pentachoron maybe?)
		constexpr size_t M = 3;

		//	Core data - number of components represents M (spatial dimension)
		//	in the algorithm
		auto topoArray = vtkSmartPointer<vtkIdTypeArray>::New();
		topoArray->SetName("topology");
		topoArray->SetNumberOfComponents(M);

		//	Try inserting something into the array
		vtkIdType cell0[M] = { 3, 0, 4 };
		vtkIdType cell1[M] = { 4, 0, 1 };
		vtkIdType cell2[M] = { 4, 1, 2 };
		vtkIdType cell3[M] = { 4, 2, 5 };
		vtkIdType cell4[M] = { 6, 3, 4 };
		vtkIdType cell5[M] = { 6, 4, 7 };
		vtkIdType cell6[M] = { 7, 4, 8 };
		vtkIdType cell7[M] = { 8, 4, 5 };

		topoArray->InsertNextTupleValue(cell0);
		topoArray->InsertNextTupleValue(cell1);
		topoArray->InsertNextTupleValue(cell2);
		topoArray->InsertNextTupleValue(cell3);
		topoArray->InsertNextTupleValue(cell4);
		topoArray->InsertNextTupleValue(cell5);
		topoArray->InsertNextTupleValue(cell6);
		topoArray->InsertNextTupleValue(cell7);

		return topoArray;
	}

	vtkSmartPointer<vtkFloatArray> generateScalarData0()
	{
		//	Values have been arranged from top-left of field
		auto result = vtkSmartPointer<vtkFloatArray>::New();
		result->SetName("f0");

		result->InsertNextValue(8);
		result->InsertNextValue(7);
		result->InsertNextValue(1);
		result->InsertNextValue(4);
		result->InsertNextValue(5);
		result->InsertNextValue(2);
		result->InsertNextValue(3);
		result->InsertNextValue(6);
		result->InsertNextValue(9);

		return result;
	}

	vtkSmartPointer<vtkFloatArray> generateScalarData1()
	{
		//	Values have been arranged from top-left of field
		auto result = vtkSmartPointer<vtkFloatArray>::New();
		result->SetName("f1");

		result->InsertNextValue(7);
		result->InsertNextValue(8);
		result->InsertNextValue(9);
		result->InsertNextValue(4);
		result->InsertNextValue(5);
		result->InsertNextValue(6);
		result->InsertNextValue(1);
		result->InsertNextValue(2);
		result->InsertNextValue(3);

		return result;
	}
}

#endif
