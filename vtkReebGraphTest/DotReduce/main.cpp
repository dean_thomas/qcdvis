#include <string>
#include <map>
#include <fstream>
#include <cassert>
#include <regex>
#include <streambuf>
#include <set>

#include <vtkgraph.h>
#include <vtkMutableDirectedGraph.h>
#include <vtkmutablegraphhelper.h>
#include <vtksmartpointer.h>

using namespace std;

//	[1]: graph/digraph; [2]: name
const std::string header_regex = R"|((.+)\s+(.+)\s*\{)|";

const std::string footer_regex = R"|(.*\}.*)|";

//	[1]: srcvertex id; [2]: edgetype; [3]: dstvertex id; [4]: edge id;
const std::string edge_regex = R"|(v(\d+)\s*(-+>?)\s*v(\d+)\s*\[label="e(.+)"\];)|";

const enum exitcode
{
	ok, corrupt_file, unknown_graph
};

const enum class graphtype
{
	unknown_graph, directed, undirected
};

struct EdgeInfo
{
	size_t src;
	size_t dst;
	size_t id;
};

EdgeInfo parseEdgeInfo(const smatch& edgeMatch)
{
	auto src = stoull(edgeMatch[1]);
	auto dst = stoull(edgeMatch[3]);
	auto id = stoull(edgeMatch[4]);

	return { src, dst, id };
}

graphtype parsegraphtype(const smatch& regexmatch)
{
	auto graphtype = regexmatch[1];
	cout << "graph is " << graphtype << "." << endl;

	if (graphtype == "graph")
	{
		return  graphtype::undirected;
	}
	else if (graphtype == "digraph")
	{
		return  graphtype::directed;
	}
	else return graphtype::unknown_graph;
}

int main(int argc, char* argv[])
{
	const string filename = R"(c:\users\4thfloor\desktop\mdrg.dot)";

	regex headerregex(header_regex);
	regex footerregex(footer_regex);
	regex edgeregex(edge_regex);

	auto builder = vtkSmartPointer<vtkMutableDirectedGraph>::New();

	//	open the file, load the entire contents in a string, and close.
	ifstream inputfile(filename);
	assert(inputfile.is_open() && inputfile.good());
	vector<string> filecontents;
	while (!inputfile.eof())
	{
		string buffer;
		getline(inputfile, buffer);
		filecontents.push_back(buffer);
	}
	inputfile.close();

	smatch headermatch;
	smatch footermatch;

	graphtype graphtype = graphtype::unknown_graph;

	for (auto& s : filecontents)
	{
		if (regex_match(s, headermatch, headerregex))
		{
			graphtype = parsegraphtype(headermatch);

			if (graphtype != graphtype::unknown_graph) break;
		}
	}

	if (graphtype != graphtype::unknown_graph)
	{
		set<vtkIdType> vertices;

		for (auto& s : filecontents)
		{
			smatch edgematch;

			if (regex_match(s, edgematch, edgeregex))
			{
				auto edgeInfo = parseEdgeInfo(edgematch);
				
				if (vertices.find(edgeInfo.src) == vertices.cend())
				{
					vertices.insert(edgeInfo.src);
					builder->AddVertex();
				}
				if (vertices.find(edgeInfo.src) == vertices.cend())
				{
					vertices.insert(edgeInfo.src);
					builder->AddVertex();
				}

				auto newEdge = builder->AddEdge(edgeInfo.src, edgeInfo.dst);
			}
		}

		cout << builder->GetNumberOfVertices() << endl;
		cout << builder->GetDegree(5) << endl;
	}
	else
	{
		cerr << "could not determine graph type in file: "
			<< filename << "." << endl;
		return unknown_graph;
	}


	getchar();
	return 0;
}