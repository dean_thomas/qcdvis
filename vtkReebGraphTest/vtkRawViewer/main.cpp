#include <vtkLocal\vtkRAWReader.h>
#include <vtkImageData.h>
#include <vtkPointData.h>
#include <vtkDataArray.h>
#include <vtkSmartPointer.h>
#include <string>
#include <iostream>

using namespace std;

const string FILENAME = R"(C:\Users\4thFloor\Desktop\metacodevtk6\Datasets\density_n.raw)";

void printToScreen(vtkDataArray* const array, const size_t numberPerPage)
{
	for (auto i = 0; i < array->GetNumberOfTuples(); i += numberPerPage)
	{
		constexpr auto COLS = 4;

		cout << "Listing values " << i << " to " << i + numberPerPage << endl;

		for (auto j = i; j < i + numberPerPage; j += COLS)
		{
			for (auto k = 0; k < COLS; ++k)
			{
				cout << setw(10) << string(to_string(j+k) + ":") << " " << *(array->GetTuple(j+k)) << "\t";
			}
			cout << endl;
		}
		cout << "Press enter" << endl;
		getchar();
	}
}

void printUsage(const std::string exe)
{
	cout << "Takes a raw file as input and lists the scalar values in the order they appear in the file."
	//cout << exe << 
}

int main(int argc, char* argv[])
{
	//	In reality we don't actually need to use VTK
	//	the actual RAW format is just a list of n integers 
	//	(stored as unsigned chars).

	const size_t DIM_X = 40;
	const size_t DIM_Y = 40;
	const size_t DIM_Z = 66;
	constexpr size_t EXPECTED_TUPLES = DIM_X * DIM_Y * DIM_Z;
	
	auto rawReader = vtkSmartPointer<vtkRAWReader>::New();
	rawReader->SetDimX(DIM_X);
	rawReader->SetDimY(DIM_Y);
	rawReader->SetDimZ(DIM_Z);
	rawReader->SetFileName(FILENAME.c_str());

	rawReader->Update();

	cout << rawReader->GetDimX() << ", "
		<< rawReader->GetDimY() << ", "
		<< rawReader->GetDimZ() << endl;

	auto arr = rawReader->GetOutput()->GetPointData()->GetArray("RAW");

	cout << "Expected number of tuples: " << EXPECTED_TUPLES << endl;
	cout << "Number of tuples: " << arr->GetNumberOfTuples() << endl;
	cout << "Min value: " << arr->GetRange()[0] << ", Max value: " << arr->GetRange()[1] << endl;
	printToScreen(arr, 50);

	getchar();
	return 0;
}


