#include <fstream>
#include <iostream>
#include <string>
#include <cassert>
#include <exception>
#include <sstream>
#include <vector>
#include <functional>
#include <iomanip>

using namespace std;

enum EXIT_CODE { OK, INVALID_PARAMETER, OUT_OF_RANGE, INPUT_FILE_ERROR, OUTPUT_FILE_ERROR };

void printUsage()
{
	const auto TAB = "  ";
	const auto PROG_DESCRIPTION = "Opens a .vol1 or .hvol1 file and normalizes the values into the specified range using a specified number of bins.";
	const auto PROG_NAME = "RENORMALIZE";
	const auto SWITCHES = R"|(<input file> <output file> "number of bins" "min" "max")|";
	const string SWITCH_USE[] = { 
		"number of bins\tSets the number bins used in the normalization process.",
		"min\t\t\tSets the minimum of the data range.",
		"max\t\t\tSets the maximum of the data range." };
	const auto FOOTNOTE = "";

	cout << PROG_DESCRIPTION << endl;
	cout << endl;
	cout << PROG_NAME << " " << SWITCHES << endl;
	cout << endl;
	for (auto& s : SWITCH_USE)
	{
		cout << TAB << s << endl;
	}
	cout << endl;

	if (FOOTNOTE != "") cout << FOOTNOTE << endl << endl;
}

float normalize(const float value, const float min = 0.0f, const float max = 1.0f)
{
	//	Check range is valid
	assert(min < max);

	//	Check the input is with the expected range
	assert((value >= min) && (value <= max));
	
	//	Do the normalization
	const float range = max - min;
	const float result = (value - min) / range;

	//	Result should be 0.0f - 1.0f
	assert((result >= 0.0f) && (result <= 1.0f));
	return result;
}

///	Parses a string and returns it as an integer
unsigned long long parseInteger(const string& input)
{
	unsigned long long result;

	try
	{
		result = stoull(input);
		return result;
	}
	catch (invalid_argument& ex)
	{
		cerr << "Argument '" << input << "' is invalid." << endl;
		exit(INVALID_PARAMETER);
	}
	catch (out_of_range& ex)
	{
		cerr << "Argument '" << input << "' is out of range." << endl;
		exit(OUT_OF_RANGE);
	}
}

///	Parses a string and returns it as an float
float parseFloat(const string& input)
{
	float result;

	try
	{
		result = stof(input);
		return result;
	}
	catch (invalid_argument& ex)
	{
		cerr << "Argument '" << input << "' is invalid." << endl;
		exit(INVALID_PARAMETER);
	}
	catch (out_of_range& ex)
	{
		cerr << "Argument '" << input << "' is out of range." << endl;
		exit(OUT_OF_RANGE);
	}
}

string processLine(const string& input, const function<int(float)>& normFunc)
{
	//cout << "Parsing '" << input << "'" << endl;

	istringstream is(input);
	ostringstream os;

	size_t x = numeric_limits<size_t>::max();
	size_t y = numeric_limits<size_t>::max();
	size_t z = numeric_limits<size_t>::max();
	long double scalar = numeric_limits<long double>::max();

	is >> x >> y >> z >> scalar;

	if ((x != numeric_limits<size_t>::max()) && (y != numeric_limits<size_t>::max())
		&& (z != numeric_limits<size_t>::max()) && (scalar != numeric_limits<long double>::max()))
	{
		os << setw(4) << x << setw(4) << y << setw(4) << z << setw(25) << normFunc(scalar) << endl;
	}
	else
	{
		os << input << endl;
	}
	return os.str();
}

int main(int argc, char* argv[])
{
	if (argc != 6) printUsage();

	auto inputFile = argv[1];
	auto outputFile = argv[2];

	if (inputFile == outputFile)
	{
		cerr << "input filename and output filename must be different." << endl;
		exit(INVALID_PARAMETER);
	}

	auto bins = parseInteger(argv[3]);
	auto dataMin = parseFloat(argv[4]);
	auto dataMax = parseFloat(argv[5]);
	if (!(dataMin < dataMax))
	{
		cerr << "Data range minimum (" << dataMin << ") and maximum value("
			<< dataMax << ") are invalid." << endl;
		exit(INVALID_PARAMETER);
	}

	//	Create our binning function with the users parameters
	auto binFunc = [](const float value, const size_t bins, const float min, const float max) -> int
	{
		return round(static_cast<float>(bins) * normalize(value, min, max));
	};

	ifstream inputFileStream(inputFile);
	if (!(inputFileStream.is_open() && inputFileStream.good()))
	{
		cerr << "Could not open the input file '" << inputFile << "' for reading." << endl;
		exit(INPUT_FILE_ERROR);
	}

	ofstream outputFileStream(outputFile);
	if (!(outputFileStream.is_open() && outputFileStream.good()))
	{
		cerr << "Could not open the output file '" << outputFile << "' for reading." << endl;
		exit(OUTPUT_FILE_ERROR);
	}

	cout << "Values will in put into " << bins << " bins, with a range of "
		<< dataMin << ".." << dataMax << "." << endl;

	while (!inputFileStream.eof())
	{
		string currentLine;
		getline(inputFileStream, currentLine);
		outputFileStream << processLine(currentLine, std::bind(binFunc,placeholders::_1,bins, dataMin, dataMax));
	}

	/*
	for (auto i = -45.0f; i < +46.0f; ++i)
	{
		cout << "in: " << i << " out: " << binFunc(i, 256, -46.0f, +46.0f) << endl;
	}
	*/

	//getchar();
	return OK;
}
