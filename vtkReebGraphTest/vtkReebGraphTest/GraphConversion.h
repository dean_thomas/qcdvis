#ifndef GRAPH_CONVERSION_H
#define GRAPH_CONVERSION_H

#include <string>

#include <vtkMutableDirectedGraph.h>
#include <vtkVertexListIterator.h>
#include <vtkInEdgeIterator.h>
#include <vtkOutEdgeIterator.h>
#include <vtkReebGraph.h>
#include <vtkDataArray.h>
#include <vtkDataSetAttributes.h>
#include <vtkSmartPointer.h>
#include <vtkEdgeListIterator.h>
#include <vtkFloatArray.h>

#include "Graphs/ReebGraph.h"

/*
Tests to see if there are any non-critical vertices
in the graph
*/
inline bool graphCanBeReduced(vtkMutableDirectedGraph* graph)
{
	vtkVertexListIterator* vertexIt = vtkVertexListIterator::New();
	graph->GetVertices(vertexIt);
	while (vertexIt->HasNext())
	{
		auto vertex = vertexIt->Next();
		auto inDegree = graph->GetInDegree(vertex);
		auto outDegree = graph->GetOutDegree(vertex);
		auto degree = inDegree + outDegree;

		if ((inDegree == 1) && (outDegree == 1)) return true;
	}
	return false;
}

/*
Reduces the graph by collapsing a single vertex to an edge
*/
inline bool reductionStep(vtkMutableDirectedGraph* graph)
{
	vtkVertexListIterator* vertexIt = vtkVertexListIterator::New();
	graph->GetVertices(vertexIt);
	while (vertexIt->HasNext())
	{
		auto vertex = vertexIt->Next();
		auto inDegree = graph->GetInDegree(vertex);
		auto outDegree = graph->GetOutDegree(vertex);

		//	Look for a regular vertex
		if ((inDegree == 1) && (outDegree == 1))
		{
			vtkIdType newSource;
			vtkIdType newTarget;

			//	We should only have 1 available incoming edge
			vtkInEdgeIterator* edgeInIt = vtkInEdgeIterator::New();
			graph->GetInEdges(vertex, edgeInIt);
			while (edgeInIt->HasNext())
			{
				auto eIn = edgeInIt->Next();

				//	Get the source vertex of the edge
				newSource = eIn.Source;
			}

			//	We should only have 1 available outgoing edge
			vtkOutEdgeIterator* edgeOutIt = vtkOutEdgeIterator::New();
			graph->GetOutEdges(vertex, edgeOutIt);
			while (edgeOutIt->HasNext())
			{
				auto eOut = edgeOutIt->Next();

				//	Get the target vertex of the vertex
				newTarget = eOut.Target;
			}

			//	Create the new edge to by-pass this vertex
			graph->AddEdge(newSource, newTarget);

			//	And delete the vertex
			graph->RemoveVertex(vertex);

			//	Iterators have probably become
			//	invalid, just exit and they can
			//	be re-initialised next time around

			//	Maybe we could return to true to signify that
			//	the operation suceeded.  We would then simply
			//	keep applying the reduction until it returned
			//	false...meaning no need to re-test for reducable vertices
			return true;
		}
	}

	//	Nothing was removed
	return false;
}


inline ReebGraph convertToReebGraph(vtkReebGraph* const reebGraph, const std::string& fieldName, vtkFloatArray* const fieldData)
{
	ReebGraph result;

	float globalMin = std::numeric_limits<float>::max();
	float globalMax = std::numeric_limits<float>::min();

	vtkDataArray* vertexInfo2 = vtkDataArray::SafeDownCast(
		reebGraph->GetVertexData()->GetAbstractArray(fieldName.c_str()));
	assert(vertexInfo2);
	for (auto i = (size_t)0; i < vertexInfo2->GetNumberOfTuples(); i++)
	{
		auto height = *vertexInfo2->GetTuple(i);
		auto ind = fieldData->LookupValue(*vertexInfo2->GetTuple(i));
		
		cout << height << ":" << ind << endl;

		if (height < globalMin) globalMin = height;
		if (height > globalMax) globalMax = height;
	}
	auto range = globalMax - globalMin;

	cout << "'" << fieldName << "' Global min: " << globalMin 
		<< ", global max: " << globalMax 
		<< ", range: " << range 
		<< "." << endl;
	
	//	Normalization of input to the range -1..+1
	auto normalize = [&](const float& unnormalised) -> float
	{
		auto normalised = (unnormalised - globalMin) / range;

		//cout << "In: " << unnormalised << ", out: " << normalised << endl;

		assert((normalised >= 0.0f) && (normalised <= 1.0f));

		return normalised;
	};

	//	Add vertices
	for (auto i = (size_t)0; i < vertexInfo2->GetNumberOfTuples(); i++)
	{
		float height = *vertexInfo2->GetTuple(i);
        result.AddNode(i, ReebGraphSupernode(i, height, normalize(height), 0, 0, 0));
	}

	//	Add edges
	vtkSmartPointer<vtkEdgeListIterator> itE = vtkSmartPointer<vtkEdgeListIterator>::New();
	reebGraph->GetEdges(itE);
	while (itE->HasNext())
	{
		auto edge = itE->Next();
		result.AddEdge(edge.Id, edge.Source, edge.Target);
	}

	return result;
}

inline std::string ToNormalisedHeightDOT(vtkReebGraph* const reebGraph, 
										 vtkFloatArray* const scalarData, 
										 const std::string& fieldName, 
										 const size_t numberOfIntervals)
	{
		assert(reebGraph != nullptr);
		assert(scalarData != nullptr);

#define tab "\t"
		std::stringstream result;

		//	We will need the min/max values for normalization
		auto dataMin = scalarData->GetRange()[0];
		auto dataMax = scalarData->GetRange()[1];

		
		std::map<size_t, float> vertexToHeight;

		vtkDataArray *vertexInfo2 = vtkDataArray::SafeDownCast(
			reebGraph->GetVertexData()->GetAbstractArray(fieldName.c_str()));
		assert(vertexInfo2);
		for (auto i = 0; i < vertexInfo2->GetNumberOfTuples(); i++)
		{
			auto height = vertexInfo2->GetTuple(i);
			vertexToHeight[i] = *height;
		}

		//	Graph header
		result << "digraph G {" << endl;

		auto dataRange = dataMax - dataMin;
		auto intervalStep = dataRange / numberOfIntervals;

		//	Height Scale (0-256)
		for (auto i = 0; i < numberOfIntervals - 1; ++i)
		{
			//	Reverse numbering (n-1..0)
			auto currentValue = numberOfIntervals - 1 - i;
			
			auto currentNode = std::to_string(currentValue);
			auto nextNode = std::to_string(currentValue - 1);

			result << tab << currentNode << " -> " << nextNode << endl;
		}

		//	Iterate over the edge list 
		vtkSmartPointer<vtkEdgeListIterator> itE = vtkSmartPointer<vtkEdgeListIterator>::New();
		reebGraph->GetEdges(itE);
		while (itE->HasNext())
		{
			auto edge = itE->Next();

			result << tab;
			result << "v" << edge.Source;
			result << " -> ";
			result << "v" << edge.Target;
			result << " [label=\"e" << edge.Id << "\"]";
			result << endl;
		}

		//	Height's
		for (auto i = 0; i < numberOfIntervals; ++i)
		{
			//	Reverse numbering (n-1..0)
			auto currentValue = numberOfIntervals - 1 - i;

			result << tab << "{ rank = same; " << currentValue << ";";

			for (auto it = vertexToHeight.cbegin(); it != vertexToHeight.cend(); ++it)
			{
				//	Retreive the vertex index (in the reeb graph)
				//	and the relevant height (from the scalar field)
				auto vertexIndex = it->first;
				auto height = it->second;

				//	Normalize in the range 0..255
				auto nHeight = (height - dataMin) / (dataMax - dataMin);
				auto norm = (size_t)(numberOfIntervals * nHeight);

				//cout << height << "\t";

				//	And write to the graph for alignment
				if (norm == i)
				{
					result << " v" << vertexIndex << ";";
				}
			}
			result << " }" << endl;
		}

		//	Add footer and return
		result << "}" << endl;
		return result.str();
	}

#endif
