#-------------------------------------------------
#
# Project created by QtCreator 2016-06-07T17:57:23
#
#-------------------------------------------------

QT       += core

#CONFIG += c++11
#QMAKE_CXXFLAGS += -std=c++11

CONFIG += c++14
QMAKE_CXXFLAGS += -std=c++14

QT       -= gui

TARGET = vtkReebGraphTest
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app

INCLUDEPATH += "../vtkMETA/"

DEFINES += TARGET_RB_VERSION=1.0

unix:!macx:
{
INCLUDEPATH += /usr/include/vtk-6.3
DEPENDPATH += /usr/include/vtk-6.3
INCLUDEPATH += /home/dean/qcdvis/CommandLineTools/CommandLineTools/Reeb
DEPENDPATH += /home/dean/qcdvis/CommandLineTools/CommandLineTools/Reeb
INCLUDEPATH += /usr/local/include/ogdf
DEPENDPATH += /usr/local/include/ogdf

#   Just a few imports from VTK
LIBS += -L/usr/lib/x86_64-linux-gnu/
LIBS += -lvtkFiltersReebGraph-6.3 -lvtkCommonComputationalGeometry-6.3
LIBS += -lvtkCommonCore-6.3 -lvtkCommonDataModel-6.3 -lvtkCommonExecutionModel-6.3 -lvtkFiltersGeneral-6.3
LIBS += -lvtkFiltersGeometry-6.3 -lvtkCommonMisc-6.3 -lvtkCommonTransforms-6.3 -lvtkFiltersCore-6.3
LIBS += -lvtkInfovisLayout-6.3 -lvtkInfovisCore-6.3 -lvtkFiltersSources-6.3 -lvtkFiltersExtraction-6.3
LIBS += -lvtkInteractionWidgets-6.3 -lvtkRenderingAnnotation-6.3 -lvtkRenderingFreeType-6.3
LIBS += -lvtkRenderingCore-6.3 -lvtkViewsInfovis-6.3 -lvtkInteractionStyle-6.3 -lvtkIOImage-6.3
LIBS += -lvtkRenderingOpenGL-6.3 -lvtkInfovisBoostGraphAlgorithms-6.3 -lvtkIOExport-6.3
}

INCLUDEPATH += "../Logging/"
INCLUDEPATH += "../../Common Classes/"
INCLUDEPATH += "../../QTFlexibleIsosurfaces_Modified/"
INCLUDEPATH += "/home/dean/Documents/qcdvis/CommandLineTools/CommandLineTools/Reeb/"

SOURCES += \
    JCNTools.cpp \
    main.cpp \
    TestJCN.cpp \
    TestReebGraph.cxx \
    ../vtkMETA/vtkLocal/vtkApplyLayout.cxx \
    ../vtkMETA/vtkLocal/vtkAssignColors.cxx \
    ../vtkMETA/vtkLocal/vtkBillboardMultivarGlyphs.cxx \
    ../vtkMETA/vtkLocal/vtkExplodeGrid.cxx \
    ../vtkMETA/vtkLocal/vtkExtractReebGraph.cxx \
    ../vtkMETA/vtkLocal/vtkFilterColour.cxx \
    ../vtkMETA/vtkLocal/vtkGrid.cxx \
    ../vtkMETA/vtkLocal/vtkJCNLayoutStrategy.cxx \
    ../vtkMETA/vtkLocal/vtkJCNMouseInteractorStyle.cxx \
    ../vtkMETA/vtkLocal/vtkJCNWithGeometry.cxx \
    ../vtkMETA/vtkLocal/vtkJointContourNet.cxx \
    ../vtkMETA/vtkLocal/vtkMultiDimensionalReebGraph.cxx \
    ../vtkMETA/vtkLocal/vtkOGDFLayoutStrategy.cxx \
    ../vtkMETA/vtkLocal/vtkPolyhedralMeshToPolyData.cxx \
    ../vtkMETA/vtkLocal/vtkPolytopeGeometry.cxx \
    ../vtkMETA/vtkLocal/vtkPyramidTree.cxx \
    ../vtkMETA/vtkLocal/vtkRAWReader.cxx \
    ../vtkMETA/vtkLocal/vtkReduceGraph.cxx \
    ../vtkMETA/vtkLocal/vtkScissionSimplification.cxx \
    ../vtkMETA/vtkLocal/vtkSimplicate.cxx \
    ../vtkMETA/vtkLocal/vtkSimplifyJCN.cxx \
    ../vtkMETA/vtkLocal/vtkSimplifyMetric.cxx \
    ../vtkMETA/vtkLocal/override/vtkJCNArrayCalculator.cxx \
    ../vtkMETA/vtkLocal/override/vtkJCNMergeFields.cxx \
    ../vtkMETA/vtkLocal/override/vtkJCNMergePoints.cxx \
    ../vtkMETA/vtkLocal/override/vtkJCNSplitField.cxx \
    ../vtkMETA/vtkLocal/override/vtkLocalOverrides.cxx \
    ../Logging/MemoryLogger.cpp \
    ../Logging/ProcessTimer.cpp \
    ../../QTFlexibleIsosurfaces_Modified/Graphs/rb.cpp \
    ../../QTFlexibleIsosurfaces_Modified/Graphs/ReebGraph.cpp \
    ../../QTFlexibleIsosurfaces_Modified/Graphs/ReebGraphSupernode.cpp \
    ../../QTFlexibleIsosurfaces_Modified/Graphs/ReebGraphSuperarc.cpp

HEADERS += \
    ArgParser.h \
    GraphConversion.h \
    JCNTools.h \
    TestJCN.h \
    TestReebGraph.h \
    vtk_ostream.h \
    vtk_ostream_formats.h \
    ../vtkMETA/vtkLocal/vtkApplyLayout.h \
    ../vtkMETA/vtkLocal/vtkAssignColors.h \
    ../vtkMETA/vtkLocal/vtkBillboardMultivarGlyphs.h \
    ../vtkMETA/vtkLocal/vtkExplodeGrid.h \
    ../vtkMETA/vtkLocal/vtkExtractReebGraph.h \
    ../vtkMETA/vtkLocal/vtkFilterColour.h \
    ../vtkMETA/vtkLocal/vtkGrid.h \
    ../vtkMETA/vtkLocal/vtkJCNLayoutStrategy.h \
    ../vtkMETA/vtkLocal/vtkJCNMouseInteractorStyle.h \
    ../vtkMETA/vtkLocal/vtkJCNWithGeometry.h \
    ../vtkMETA/vtkLocal/vtkJointContourNet.h \
    ../vtkMETA/vtkLocal/vtkMETAModule.h \
    ../vtkMETA/vtkLocal/vtkMultiDimensionalReebGraph.h \
    ../vtkMETA/vtkLocal/vtkOGDFLayoutStrategy.h \
    ../vtkMETA/vtkLocal/vtkPolyhedralMeshToPolyData.h \
    ../vtkMETA/vtkLocal/vtkPolytopeGeometry.h \
    ../vtkMETA/vtkLocal/vtkPyramidTree.h \
    ../vtkMETA/vtkLocal/vtkRAWReader.h \
    ../vtkMETA/vtkLocal/vtkReduceGraph.h \
    ../vtkMETA/vtkLocal/vtkScissionSimplification.h \
    ../vtkMETA/vtkLocal/vtkSimplicate.h \
    ../vtkMETA/vtkLocal/vtkSimplifyJCN.h \
    ../vtkMETA/vtkLocal/vtkSimplifyMetric.h \
    ../vtkMETA/vtkLocal/override/vtkJCNArrayCalculator.h \
    ../vtkMETA/vtkLocal/override/vtkJCNMergeFields.h \
    ../vtkMETA/vtkLocal/override/vtkJCNMergePoints.h \
    ../vtkMETA/vtkLocal/override/vtkJCNSplitField.h \
    ../vtkMETA/vtkLocal/override/vtkLocalOverrides.h \
    ../Logging/LogRedirect.h \
    ../Logging/MemoryLogger.h \
    ../Logging/ProcessTimer.h \
    ../../QTFlexibleIsosurfaces_Modified/Graphs/rb.h \
    ../../QTFlexibleIsosurfaces_Modified/Graphs/ReebGraph.h \
    ../../QTFlexibleIsosurfaces_Modified/Graphs/ReebGraphSuperarc.h \
    ../../QTFlexibleIsosurfaces_Modified/Graphs/ReebGraphSupernode.h \
    slab_stats.h

#   VTK libs
#win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../../../../../../../Program Files/VTK/6.1.0/lib/ -lvtkCommonCore-6.1
#else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../../../../../../../Program Files/VTK/6.1.0/lib/ -lvtkCommonCore-6.1d
#else:unix: LIBS += -L$$PWD/../../../../../../../Program Files/VTK/6.1.0/lib/ -lvtkCommonCore-6.1

#   vtk libs
#LIBS += -L"/usr/local/lib/" -lvtkalglib-6.1
#LIBS += -L"/usr/local/lib/" -lvtkChartsCore-6.1
#LIBS += -L"/usr/local/lib/" -lvtkCommonColor-6.1
#LIBS += -L"/usr/local/lib/" -lvtkCommonComputationalGeometry-6.1
#LIBS += -L"/usr/local/lib/" -lvtkCommonCore-6.1
#LIBS += -L"/usr/local/lib/" -lvtkCommonDataModel-6.1
#LIBS += -L"/usr/local/lib/" -lvtkCommonExecutionModel-6.1
#LIBS += -L"/usr/local/lib/" -lvtkCommonMath-6.1
#LIBS += -L"/usr/local/lib/" -lvtkCommonMisc-6.1
#LIBS += -L"/usr/local/lib/" -lvtkCommonSystem-6.1
#LIBS += -L"/usr/local/lib/" -lvtkCommonTransforms-6.1
#LIBS += -L"/usr/local/lib/" -lvtkDICOMParser-6.1
#LIBS += -L"/usr/local/lib/" -lvtkDomainsChemistry-6.1
#LIBS += -L"/usr/local/lib/" -lvtkexoIIc-6.1
#LIBS += -L"/usr/local/lib/" -lvtkexpat-6.1
#LIBS += -L"/usr/local/lib/" -lvtkFiltersAMR-6.1
#LIBS += -L"/usr/local/lib/" -lvtkFiltersCore-6.1
#LIBS += -L"/usr/local/lib/" -lvtkFiltersExtraction-6.1
#LIBS += -L"/usr/local/lib/" -lvtkFiltersFlowPaths-6.1
#LIBS += -L"/usr/local/lib/" -lvtkFiltersGeneral-6.1
#LIBS += -L"/usr/local/lib/" -lvtkFiltersGeneric-6.1
#LIBS += -L"/usr/local/lib/" -lvtkFiltersGeometry-6.1
#LIBS += -L"/usr/local/lib/" -lvtkFiltersHybrid-6.1
#LIBS += -L"/usr/local/lib/" -lvtkFiltersHyperTree-6.1
#LIBS += -L"/usr/local/lib/" -lvtkFiltersImaging-6.1
#LIBS += -L"/usr/local/lib/" -lvtkFiltersModeling-6.1
#LIBS += -L"/usr/local/lib/" -lvtkFiltersParallel-6.1
#LIBS += -L"/usr/local/lib/" -lvtkFiltersParallelImaging-6.1
#LIBS += -L"/usr/local/lib/" -lvtkFiltersProgrammable-6.1
#LIBS += -L"/usr/local/lib/" -lvtkFiltersReebGraph-6.1
#LIBS += -L"/usr/local/lib/" -lvtkFiltersSelection-6.1
#LIBS += -L"/usr/local/lib/" -lvtkFiltersSMP-6.1
#LIBS += -L"/usr/local/lib/" -lvtkFiltersSources-6.1
#LIBS += -L"/usr/local/lib/" -lvtkFiltersStatistics-6.1
#LIBS += -L"/usr/local/lib/" -lvtkFiltersTexture-6.1
#LIBS += -L"/usr/local/lib/" -lvtkFiltersVerdict-6.1
#LIBS += -L"/usr/local/lib/" -lvtkfreetype-6.1
#LIBS += -L"/usr/local/lib/" -lvtkftgl-6.1
#LIBS += -L"/usr/local/lib/" -lvtkGeovisCore-6.1
#LIBS += -L"/usr/local/lib/" -lvtkgl2ps-6.1
#LIBS += -L"/usr/local/lib/" -lvtkhdf5-6.1
#LIBS += -L"/usr/local/lib/" -lvtkhdf5_hl-6.1
#LIBS += -L"/usr/local/lib/" -lvtkImagingColor-6.1
#LIBS += -L"/usr/local/lib/" -lvtkImagingCore-6.1
#LIBS += -L"/usr/local/lib/" -lvtkImagingFourier-6.1
#LIBS += -L"/usr/local/lib/" -lvtkImagingGeneral-6.1
#LIBS += -L"/usr/local/lib/" -lvtkImagingHybrid-6.1
#LIBS += -L"/usr/local/lib/" -lvtkImagingMath-6.1
#LIBS += -L"/usr/local/lib/" -lvtkImagingMorphological-6.1
#LIBS += -L"/usr/local/lib/" -lvtkImagingSources-6.1
#LIBS += -L"/usr/local/lib/" -lvtkImagingStatistics-6.1
#LIBS += -L"/usr/local/lib/" -lvtkImagingStencil-6.1
#LIBS += -L"/usr/local/lib/" -lvtkInfovisBoostGraphAlgorithms-6.1
#LIBS += -L"/usr/local/lib/" -lvtkInfovisCore-6.1
#LIBS += -L"/usr/local/lib/" -lvtkInfovisLayout-6.1
#LIBS += -L"/usr/local/lib/" -lvtkInteractionImage-6.1
#LIBS += -L"/usr/local/lib/" -lvtkInteractionStyle-6.1
#LIBS += -L"/usr/local/lib/" -lvtkInteractionWidgets-6.1
#LIBS += -L"/usr/local/lib/" -lvtkIOAMR-6.1
#LIBS += -L"/usr/local/lib/" -lvtkIOCore-6.1
#LIBS += -L"/usr/local/lib/" -lvtkIOEnSight-6.1
#LIBS += -L"/usr/local/lib/" -lvtkIOExodus-6.1
#LIBS += -L"/usr/local/lib/" -lvtkIOExport-6.1
#LIBS += -L"/usr/local/lib/" -lvtkIOGeometry-6.1
#LIBS += -L"/usr/local/lib/" -lvtkIOImage-6.1
#LIBS += -L"/usr/local/lib/" -lvtkIOImport-6.1
#LIBS += -L"/usr/local/lib/" -lvtkIOInfovis-6.1
#LIBS += -L"/usr/local/lib/" -lvtkIOLegacy-6.1
#LIBS += -L"/usr/local/lib/" -lvtkIOLSDyna-6.1
#LIBS += -L"/usr/local/lib/" -lvtkIOMINC-6.1
#LIBS += -L"/usr/local/lib/" -lvtkIOMovie-6.1
#LIBS += -L"/usr/local/lib/" -lvtkIONetCDF-6.1
#LIBS += -L"/usr/local/lib/" -lvtkIOParallel-6.1
#LIBS += -L"/usr/local/lib/" -lvtkIOPLY-6.1
#LIBS += -L"/usr/local/lib/" -lvtkIOSQL-6.1
#LIBS += -L"/usr/local/lib/" -lvtkIOVideo-6.1
#LIBS += -L"/usr/local/lib/" -lvtkIOXML-6.1
#LIBS += -L"/usr/local/lib/" -lvtkIOXMLParser-6.1
#LIBS += -L"/usr/local/lib/" -lvtkjpeg-6.1
#LIBS += -L"/usr/local/lib/" -lvtkjsoncpp-6.1
#LIBS += -L"/usr/local/lib/" -lvtklibxml2-6.1
#LIBS += -L"/usr/local/lib/" -lvtkmetaio-6.1
#LIBS += -L"/usr/local/lib/" -lvtkNetCDF-6.1
#LIBS += -L"/usr/local/lib/" -lvtkNetCDF_cxx-6.1
#LIBS += -L"/usr/local/lib/" -lvtkoggtheora-6.1
#LIBS += -L"/usr/local/lib/" -lvtkParallelCore-6.1
#LIBS += -L"/usr/local/lib/" -lvtkpng-6.1
#LIBS += -L"/usr/local/lib/" -lvtkproj4-6.1
#LIBS += -L"/usr/local/lib/" -lvtkPythonInterpreter-6.1
#LIBS += -L"/usr/local/lib/" -lvtkRenderingAnnotation-6.1
#LIBS += -L"/usr/local/lib/" -lvtkRenderingContext2D-6.1
#LIBS += -L"/usr/local/lib/" -lvtkRenderingCore-6.1
#LIBS += -L"/usr/local/lib/" -lvtkRenderingFreeType-6.1
#LIBS += -L"/usr/local/lib/" -lvtkRenderingFreeTypeOpenGL-6.1
#LIBS += -L"/usr/local/lib/" -lvtkRenderingGL2PS-6.1
#LIBS += -L"/usr/local/lib/" -lvtkRenderingImage-6.1
#LIBS += -L"/usr/local/lib/" -lvtkRenderingLabel-6.1
#LIBS += -L"/usr/local/lib/" -lvtkRenderingLIC-6.1
#LIBS += -L"/usr/local/lib/" -lvtkRenderingLOD-6.1
#LIBS += -L"/usr/local/lib/" -lvtkRenderingOpenGL-6.1
#LIBS += -L"/usr/local/lib/" -lvtkRenderingVolume-6.1
#LIBS += -L"/usr/local/lib/" -lvtkRenderingVolumeAMR-6.1
#LIBS += -L"/usr/local/lib/" -lvtkRenderingVolumeOpenGL-6.1
#LIBS += -L"/usr/local/lib/" -lvtksqlite-6.1
#LIBS += -L"/usr/local/lib/" -lvtksys-6.1
#LIBS += -L"/usr/local/lib/" -lvtktiff-6.1
#LIBS += -L"/usr/local/lib/" -lvtkverdict-6.1
#LIBS += -L"/usr/local/lib/" -lvtkViewsContext2D-6.1
#LIBS += -L"/usr/local/lib/" -lvtkViewsCore-6.1
#LIBS += -L"/usr/local/lib/" -lvtkViewsGeovis-6.1
#LIBS += -L"/usr/local/lib/" -lvtkViewsInfovis-6.1
#LIBS += -L"/usr/local/lib/" -lvtkWrappingPython27Core-6.1
#LIBS += -L"/usr/local/lib/" -lvtkzlib-6.1

LIBS += -L"/usr/local/lib/" -lOGDF -lCOIN

#   VTK Sources
INCLUDEPATH += "/usr/local/include/vtk-6.1/"
DEPENDPATH += "/usr/local/include/vtk-6.1/"

#   Boost sources
INCLUDEPATH += "/usr/local/include/boost_1_60_0/"
DEPENDPATH += "/usr/local/include/boost_1_60_0/"
