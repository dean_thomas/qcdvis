#pragma once

/*=========================================================================

Program:   Visualization Toolkit
Module:    $RCSfile$

Copyright (c) Ken Martin, Will Schroeder, Bill Lorensen
All rights reserved.
See Copyright.txt or http://www.kitware.com/Copyright.htm for details.

This software is distributed WITHOUT ANY WARRANTY; without even
the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the above copyright notice for more information.

=========================================================================*/

// Testings of the vtkReebGraph features.
// Julien Tierny <jtierny@sci.utah.edu>, 2010
/*

#include <vtkAxesActor.h>
#include <vtkOrientationMarkerWidget.h>
#include "vtksetGet.h"
#include <vtkActor.h>
#include <vtkAreaContourSpectrumFilter.h>
#include <vtkCamera.h>
#include <vtkDataSetAttributes.h>
#include <vtkDoubleArray.h>
#include <vtkEdgeListIterator.h>
#include <vtkIdList.h>
#include <vtkLight.h>
#include <vtkObjectFactory.h>
#include "vtkPointData.h"
#include "vtkPolyData.h"
#include "vtkPolyDataMapper.h"
#include "vtkPolyDataToReebGraphFilter.h"
#include "vtkProperty.h"
#include "vtkPNGWriter.h"
#include "vtkReebGraph.h"
#include "vtkReebGraphSurfaceSkeletonFilter.h"
#include "vtkReebGraphSimplificationFilter.h"
#include "vtkReebGraphSimplificationMetric.h"
#include "vtkReebGraphToJoinSplitTreeFilter.h"
#include "vtkReebGraphVolumeSkeletonFilter.h"
#include "vtkRenderer.h"
#include "vtkRenderWindow.h"
#include "vtkRenderWindowInteractor.h"
#include "vtkSphereSource.h"
#include "vtkTable.h"
#include "vtkTriangle.h"
#include "vtkUnstructuredGrid.h"
#include "vtkUnstructuredGridToReebGraphFilter.h"
#include "vtkVariantArray.h"
#include "vtkVolumeContourSpectrumFilter.h"
#include "vtkWindowToImageFilter.h"
#include <vtkVertexListIterator.h>
#include <vtkEdgeListIterator.h>


#include <string>
#include <sstream>
#include <map>
#include <vector>

class AreaSimplificationMetric : public vtkReebGraphSimplificationMetric {
public:
	vtkTypeMacro(AreaSimplificationMetric, vtkReebGraphSimplificationMetric);
	static AreaSimplificationMetric* New();
	double ComputeMetric(vtkDataSet *mesh, vtkDataArray *scalarField,
		vtkIdType startCriticalPoint, vtkAbstractArray *vertexList,
		vtkIdType endCriticalPoint)
	{
		// In this example, the metric algorithm just evaluates the area of the
		// surface region corresponding to the arc of the Reeb graph passed as an
		// argument.
		// As a result, the arcs corresponding to small surface regions (below the
		// threshold specified to the simplificatin filter) will be
		// simplified in priority in the surface simplification algorithm.

		double  fieldLowerBound = scalarField->GetComponent(startCriticalPoint, 0),
			fieldUpperBound = scalarField->GetComponent(endCriticalPoint, 0);

		double  cumulativeArea = 0;

		std::map<vtkIdType, bool> visitedTriangles;

		for (int i = 0; i < vertexList->GetNumberOfTuples(); i++)
		{
			int vId = vertexList->GetVariantValue(i).ToInt();
			vtkIdList *starTriangleList = vtkIdList::New();

			mesh->GetPointCells(vId, starTriangleList);

			for (int j = 0; j < starTriangleList->GetNumberOfIds(); j++)
			{
				vtkIdType tId = starTriangleList->GetId(j);
				vtkTriangle *t = vtkTriangle::SafeDownCast(mesh->GetCell(tId));
				std::map<vtkIdType, bool>::iterator tIt = visitedTriangles.find(tId);
				if (tIt == visitedTriangles.end())
				{
					if ((scalarField->GetComponent(t->GetPointIds()->GetId(0), 0)
						<= fieldUpperBound)
						&& (scalarField->GetComponent(t->GetPointIds()->GetId(1), 0)
							<= fieldUpperBound)
						&& (scalarField->GetComponent(t->GetPointIds()->GetId(2), 0)
							<= fieldUpperBound)
						&& (scalarField->GetComponent(t->GetPointIds()->GetId(0), 0)
							>= fieldLowerBound)
						&& (scalarField->GetComponent(t->GetPointIds()->GetId(1), 0)
							>= fieldLowerBound)
						&& (scalarField->GetComponent(t->GetPointIds()->GetId(2), 0)
							>= fieldLowerBound))
					{
						// the triangle fully maps inside the arc function interval
						cumulativeArea += t->ComputeArea();
					}
					visitedTriangles[tId] = true;
				}
			}

			starTriangleList->Delete();
		}

		return cumulativeArea / (this->UpperBound - this->LowerBound);
	}
};

void writeToFile(const std::string& filename, const std::string& data);
std::string ToNormalisedHeightDOT(vtkReebGraph * reebGraph, vtkDoubleArray * scalarData);
std::string ToSimplifedDOT(vtkReebGraph * reebGraph);
int TestReebGraph(const std::vector<std::string>& inputFiles);
int LoadVolumeMesh(vtkUnstructuredGrid *vMesh);
int generateHeightfieldStructure(vtkPolyData *sMesh);
int DisplayReebGraph(vtkReebGraph *g);
int DisplaySurfaceSkeleton(vtkPolyData *surfaceMesh, vtkTable *skeleton);
int DisplayVolumeSkeleton(vtkUnstructuredGrid* vtkNotUsed(volumeMesh), vtkTable *skeleton);
*/