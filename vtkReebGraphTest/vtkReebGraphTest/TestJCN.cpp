#include "TestJCN.h"

/*
#include "JCNTools.h"


#include <array>
#include <utility>
#include <fstream>
#include <string>
#include <vector>

#include <Filetypes\Vol1.h>

#include "vtkLocal\vtkJointContourNet.h"
#include "vtkLocal\vtkOGDFLayoutStrategy.h"
#include "vtkLocal\vtkRAWReader.h"
#include "vtkLocal\vtkSimplicate.h"

#include <vtkPoints.h>
#include <vtkIdList.h>
#include <vtkFloatArray.h>
#include <vtkUnstructuredGrid.h>
#include <vtkCellType.h>
#include <vtkPointData.h>
#include <vtkIdFilter.h>
#include <vtkImageData.h>

using namespace std;


void TestJCN::qcdTest(const std::vector<std::string>& argv)
{
	//	We need at least to filenames (and the program name to function).
	//assert(argv.size() > 4);
	/*
	auto filename1 = argv[1];
	auto filename2 = argv[2];
	auto filename3 = argv[3];
	Vol1Reader vol1reader;

	vol1reader(filename1);
	auto heightField1 = vol1reader.data;
	
	vol1reader(filename2);
	auto heightField2 = vol1reader.data;

	vol1reader(filename3);
	auto heightField3 = vol1reader.data;

	auto f1 = vtkFloatArray::New();
	auto f2 = vtkFloatArray::New();
	auto f3 = vtkFloatArray::New();

	for (auto it = heightField1.cbegin(); it != heightField1.cend(); ++it)
	{
		f1->InsertNextValue(*it);
	}

	for (auto it = heightField2.cbegin(); it != heightField2.cend(); ++it)
	{
		f2->InsertNextValue(*it);
	}

	for (auto it = heightField3.cbegin(); it != heightField3.cend(); ++it)
	{
		f3->InsertNextValue(*it);
	}

	f1->SetName("f1");
	f2->SetName("f2");
	f3->SetName("f3");


	JCNTools jcnTools;
	jcnTools.ProcessArgs(std::vector<string>(argv.cbegin(), argv.cend()));

	//	Make sure the user has specifed some data...
	if (jcnTools.getFieldCount() < 1)
	{
		jcnTools.PrintHelp();
	}
	else
	{
		auto res = jcnTools.PipeLine();
		auto win = std::get<0>(res);
		auto gui = std::get<1>(res);

		if (!jcnTools.isNoGui())
		{
			win->Render();
			gui->Start();
		}
	}
}

void TestJCN::paraHeightDemo()
{
	/*
	const vector<string> argv = { 
		"paraHeightDemo", 
		"C:\\Users\\4thFloor\\Desktop\\metacodevtk6\\Datasets\\Parab_20.txt",
		"C:\\Users\\4thFloor\\Desktop\\metacodevtk6\\Datasets\\Height_20.txt",
		"-s", 
		"f1", 
		"1", 
		"-s", 
		"f2", 
		"1" };

	auto filename1 = argv[1];
	auto filename2 = argv[2];

	ifstream ins1(filename1);
	vector<double> field1;
	for (std::string line; getline(ins1, line);)
	{
		field1.push_back(atof(line.c_str()));
	}
	ins1.close();

	ifstream ins2(filename2);
	vector<double> field2;
	for (std::string line; getline(ins2, line);)
	{
		field2.push_back(atof(line.c_str()));
	}
	ins2.close();

	auto arr1 = vtkFloatArray::New();
	for (auto it = field1.cbegin(); it != field1.cend(); ++it)
	{
		arr1->InsertNextValue(*it);
	}
	arr1->SetName("f1");

	auto arr2 = vtkFloatArray::New();
	for (auto it = field2.cbegin(); it != field2.cend(); ++it)
	{
		arr2->InsertNextValue(*it);
	}
	arr2->SetName("f2");

	auto atom = vtkImageData::New();
	atom->SetDimensions(20, 20, 20);
	atom->GetPointData()->AddArray(arr1);
	atom->GetPointData()->AddArray(arr2);

	auto simp = vtkSimplicate::New();
	simp->SetInputData(atom);
	simp->SetScheme(1);
	simp->Update();

	JCNTools jcnTools;
	jcnTools.ProcessArgs(std::vector<string>(argv.cbegin() + 3, argv.cend()));

	auto strat = vtkOGDFLayoutStrategy::New();
	strat->SetLayoutModuleByName("fmmm");
	strat->LayoutEdgesOff();
	jcnTools.Layout = strat;

	auto res = jcnTools.PipeLine(simp->GetOutputPort());
	auto win = std::get<0>(res);
	auto gui = std::get<1>(res);

	win->Render();
	gui->Start();

}

void TestJCN::scissionTest()
{
	/*
	const vector<string> argv = {
		"scissionTest",
		"C:\\Users\\4thFloor\\Desktop\\metacodevtk6\\Datasets\\density",
		"-s",
		"p",
		"1",
		"-s",
		"n",
		"1"
	};

	auto filename = argv[1];

	JCNTools jcnTools;
	jcnTools.ProcessArgs(std::vector<string>(argv.cbegin() + 2, argv.cend()));

	//	Create dataset container for p / n fields.

	auto atom = vtkImageData::New();
	atom->SetDimensions(40, 40, 66);
	auto arr_p = vtkUnsignedCharArray::New();
	auto arr_n = vtkUnsignedCharArray::New();
	arr_p->SetName("p");
	arr_n->SetName("n");
	atom->GetPointData()->AddArray(arr_p);
	atom->GetPointData()->AddArray(arr_n);

	auto rawN = vtkRAWReader::New();
	rawN->SetDimX(40);
	rawN->SetDimY(40);
	rawN->SetDimZ(66);
	rawN->SetFileName("C:\\Users\\4thFloor\\Desktop\\metacodevtk6\\Datasets\\density_n.raw");
	rawN->Update();
	auto arrN = rawN->GetOutput()->GetPointData()->GetArray("RAW");
	assert(arrN != nullptr);
	atom->GetPointData()->GetArray("n")->DeepCopy(arrN);
	
	auto rawP = vtkRAWReader::New();
	rawP->SetDimX(40);
	rawP->SetDimY(40);
	rawP->SetDimZ(66);
	rawP->SetFileName("C:\\Users\\4thFloor\\Desktop\\metacodevtk6\\Datasets\\density_p.raw");
	rawP->Update();
	auto arrP = rawP->GetOutput()->GetPointData()->GetArray("RAW");
	assert(arrP != nullptr);
	atom->GetPointData()->GetArray("p")->DeepCopy(arrP);

	auto simp = vtkSimplicate::New();
	simp->SetInputData(atom);
	simp->SetScheme(0);
	simp->Update();

	auto strat = vtkOGDFLayoutStrategy::New();
	strat->SetLayoutModuleByName("fmmm");
	strat->LayoutEdgesOff();
	jcnTools.Layout = strat;
	jcnTools.SubDir = "Physics";
	jcnTools.ImageSubDir = "PhysicsImage";

	auto res = jcnTools.PipeLine(simp->GetOutputPort());
	auto win = get<0>(res);
	auto gui = get<1>(res);

	win->Render();
	gui->Start();
}

TestJCN::TestJCN(const std::vector<std::string>& argv)
{
	//paraHeightDemo();
	//ringBarDemo();
	//scissionTest();
	//qcdTest(R"(C:\Users\4thFloor\Desktop\config.b210k1577mu0700j02s16t32\conp0015\cool0015\cache\Spacelike Plaquette_t=10.vol1)",
			//R"(C:\Users\4thFloor\Desktop\config.b210k1577mu0700j02s16t32\conp0015\cool0015\cache\Timelike Plaquette_t=10.vol1)");
	
	cout << "TestJCN build: " << __DATE__ << ", " << __TIME__ << endl;
	cout << "=========================================" << endl << endl;

	qcdTest(argv);

	/*
	qcdTest({ "qcdTest",
			"-input",
			R"(C:\Users\4thFloor\Desktop\config.b210k1577mu0700j02s16t32\conp0015\cool0015\cache\Timelike Plaquette_t=10.vol1)",
			"timelike",
			"0.004",
			//"-input",
			//R"(C:\Users\4thFloor\Desktop\config.b210k1577mu0700j02s16t32\conp0015\cool0015\cache\Spacelike Plaquette_t=10.vol1)",
			//"spacelike",
			//"0.004",
			"-input",
			R"(C:\Users\4thFloor\Desktop\config.b210k1577mu0700j02s16t32\conp0015\cool0015\cache\Topological charge density_t=10.vol1)",
			"tcd",
			"0.5",
			"-layout",
			"mmmexamplenice",
			"-save",
			//"-RS",
			//"-geom"
	});

	//qcdTest(R"(C:\Users\4thFloor\Desktop\config.b210k1577mu0700j02s16t32\conp0015\cool0015\cache\Topological charge density_t=10.vol1)",
	//		R"(C:\Users\4thFloor\Desktop\config.b210k1577mu0700j02s16t32\conp0015\cool0015\cache\Timelike Plaquette_t=10.vol1)",
	//		R"(C:\Users\4thFloor\Desktop\config.b210k1577mu0700j02s16t32\conp0015\cool0015\Topological charge density_Timelike Plaquette_t=10.jpg)");
}


TestJCN::~TestJCN()
{
}

*/