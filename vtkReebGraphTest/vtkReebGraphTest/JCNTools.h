#pragma once
#ifndef JCN_TOOLS_H
#define JCN_TOOLS_H

#include "vtkLocal/vtkJCNWithGeometry.h"
#include "vtkLocal/vtkJointContourNet.h"
#include "vtkLocal/vtkJCNLayoutStrategy.h"
#include "vtkLocal/vtkOGDFLayoutStrategy.h"
#include "vtkLocal/vtkReduceGraph.h"
#include "vtkLocal/vtkMultiDimensionalReebGraph.h"
#include "vtkLocal/vtkScissionSimplification.h"
#include "vtkLocal/override/vtkJCNMergeFields.h"
#include "vtkLocal/vtkBillboardMultivarGlyphs.h"
#include "vtkLocal/vtkAssignColors.h"
#include "vtkLocal/vtkJCNMouseInteractorStyle.h"
#include "vtkLocal/vtkExplodeGrid.h"
#include "vtkLocal/vtkSimplicate.h"
#include "vtkLocal/vtkJointContourNet.h"


#include <vtkReebGraphToJoinSplitTreeFilter.h>
#include <vtkVariantArray.h>
#include <vtkIdTypeArray.h>
#include <vtkGraphLayoutStrategy.h>
#include <vtkForceDirectedLayoutStrategy.h>
#include <vtkGraphAlgorithm.h>
#include <vtkDataSetAttributes.h>
#include <vtkDataArray.h>
#include <vtkLookupTable.h>
#include <vtkDoubleArray.h>
#include <vtkGraphLayout.h>
#include <vtkAssignAttribute.h>
#include <vtkScalarsToColors.h>
#include <vtkApplyColors.h>
#include <vtkPolyDataMapper.h>
#include <vtkActor.h>
#include <vtkProperty.h>
#include <vtkSelectionSource.h>
#include <vtkSphereSource.h>
#include <vtkGlyph3D.h>
#include <vtkRenderer.h>
#include <vtkRenderWindow.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkImplicitPlaneWidget.h>
#include <vtkWindowToImageFilter.h>
#include <vtkJPEGWriter.h>
#include <vtkEdgeListIterator.h>
#include <vtkFloatArray.h>
#include <vtkVertexListIterator.h>
#include <vtkOutEdgeIterator.h>
#include <vtkInEdgeIterator.h>

#include "vtk_ostream.h"

#include "ArgParser.h"

#include <string>
#include <map>
#include <set>
#include <iostream>
#include <vector>
#include <cassert>
#include <algorithm>
#include <sstream>
#include <vtkPNGWriter.h>

#include <Graphs/ReebGraph.h>
#include <Filetypes/Vol1.h>
#include <Graphs/rb.h>
//#define DOT_TO_PNG

#ifndef __PRETTY_FUNCTION__
#ifdef __FUNCSIG__
#define __PRETTY_FUNCTION__ __FUNCSIG__
#else
#define __PRETTY_FUNCTION__ __func__
#endif
#endif

//	If defined we use the old algorithm for computing the JCN,
//	otherwise we will use the new filter
#define USE_VTK_WITH_GEOMETRY_FILTER

class JCNTools
{
#ifdef USE_VTK_WITH_GEOMETRY_FILTER
	using Algorithm_Core = vtkJCNWithGeometry;
#else
	//using Algorithm_Core = vtkJointContourNet;
#endif

	//	Smart pointers
	using vtkActor_vptr = vtkSmartPointer<vtkActor>;
	using vtkSimplifyMetric_vptr = vtkSmartPointer<vtkSimplifyMetric>;
	using vtkRenderWindowInteractor_vptr = vtkSmartPointer<vtkRenderWindowInteractor>;
	using vtkJCNMouseInteractorStyle_vptr = vtkSmartPointer<vtkJCNMouseInteractorStyle>;
	using vtkWindowToImageFilter_vptr = vtkSmartPointer<vtkWindowToImageFilter>;
	using vtkPNGWriter_vptr = vtkSmartPointer<vtkPNGWriter>;
	using vtkExtractReebGraph_vptr = vtkSmartPointer<vtkExtractReebGraph>;
	using vtkRenderer_vptr = vtkSmartPointer<vtkRenderer>;
	using vtkRenderWindow_vptr = vtkSmartPointer<vtkRenderWindow>;
	using vtkDoubleArray_vptr = vtkSmartPointer<vtkDoubleArray>;
	using vtkDataObject_vptr = vtkSmartPointer<vtkDataObject>;
	using vtkReduceGraph_vptr = vtkSmartPointer<vtkReduceGraph>;
	using vtkGraph_vptr = vtkSmartPointer<vtkGraph>;
	using vtkDataArray_vptr = vtkSmartPointer<vtkDataArray>;
	using vtkFloatArray_vptr = vtkSmartPointer<vtkFloatArray>;
	using vtkGraphLayoutStrategy_vptr = vtkSmartPointer<vtkGraphLayoutStrategy>;
	using vtkBillboardMultivarGlyphs_vptr = vtkSmartPointer<vtkBillboardMultivarGlyphs>;
	using vtkVertexListIterator_vptr = vtkSmartPointer<vtkVertexListIterator>;
	using vtkOutEdgeIterator_vptr = vtkSmartPointer<vtkOutEdgeIterator>;
	using vtkInEdgeIterator_vptr = vtkSmartPointer<vtkInEdgeIterator>;

	Arguments m_arguments;
	std::map<std::string, size_t> Objects;

	struct FieldData
	{
		vtkFloatArray_vptr data = nullptr;
		size_t dimX = 0;
		size_t dimY = 0;
		size_t dimZ = 0;
		float slabSize = 1.0;
		size_t intervals = 0;
        float min = 0.0f;
        float max = 0.0f;
		FieldData() { }

        FieldData(vtkFloatArray_vptr data, const size_t& dimX, const size_t& dimY, const size_t& dimZ,
                  const float slabSize, const size_t intervals,
                  const size_t min, const size_t max)
            : data { data }, dimX { dimX }, dimY { dimY }, dimZ { dimZ }, slabSize { slabSize }, intervals { intervals }, min { min }, max { max }
		{ }
	};

	std::string generateJcnFilename() const;

	std::map<std::string, FieldData> m_data;

	bool loadHeightField(const std::string& filename, const std::string& identifier, const float slabs, bool normalizeToRange = false);

	vtkDataObject_vptr m_jcnGraph = nullptr;
	vtkDataObject_vptr m_jcnGrid = nullptr;

	vtkDataArray_vptr m_mdrgList = nullptr;
	vtkReduceGraph_vptr m_graphReductionAlgorithm = nullptr;

	vtkGraphLayoutStrategy_vptr layout_strategy = nullptr;
	vtkBillboardMultivarGlyphs_vptr glyphs = vtkBillboardMultivarGlyphs::New();
	vtkBillboardMultivarGlyphs_vptr glyphsComponent = vtkBillboardMultivarGlyphs::New();
	vtkImplicitPlaneWidget* planeWidget = vtkImplicitPlaneWidget::New();

	vtkSmartPointer<vtkSimplicate> generateSimplicateData() const;

	vtkSmartPointer<Algorithm_Core> m_jcn = nullptr;
	vtkSmartPointer<Algorithm_Core> doJcnComputation() const;

	std::vector<double> highs;
	std::vector<double> lows;

	void output_frag_data(vtkUnstructuredGrid* const fragments) const;
	void output_slab_data(vtkUnstructuredGrid* const boundaries) const;

	void doRangeComputation(vtkDataObject_vptr const& jcnGraph);

	double globalMaxima() const;
	double globalMinima() const;

	void printJcnStructures(vtkGraph* const graph, vtkUnstructuredGrid* const grid) const;

	void saveImages(vtkRenderWindow_vptr const renwin) const;
	void saveReebGraphs(const bool dot, const bool rb, const bool png) const;

	std::string generateJcnFile(JCNTools::vtkGraph_vptr const jcnGraph) const;
	void saveJointContourNetToDot(vtkGraph_vptr const jcnGraph, const std::string& filename) const;
public:


	JCNTools(const Arguments& cmdArguments);

	bool setLayoutStrategy(const std::string& val);

	std::tuple<vtkActor_vptr, vtkActor_vptr, vtkSimplifyMetric_vptr>
		SimplifyGraphPipeline(vtkDataObject_vptr const Port, /*ColourTable, dataRange,*/ vtkDataArray_vptr const mdrgList);

	vtkDataArray* JacobiSetComputation(vtkDataObject* const InputGraph);

	std::tuple<vtkActor_vptr, vtkActor_vptr, vtkActor_vptr, vtkActor_vptr>
		GraphPipeline(vtkAlgorithmOutput* const defaultPort, vtkDoubleArray* const dataRange);

	vtkActor_vptr GeometryPipeline(vtkDataObject* const jcnGraph, vtkAlgorithmOutput* const defaultPort);
	
	/*
	template <typename T>
	std::tuple<vtkRenderWindow*, vtkRenderWindowInteractor*>
		pipeLine(T* const jcn, vtkAlgorithmOutput* const InputGridPort);
	*/
	std::tuple<vtkRenderWindow_vptr, vtkRenderWindowInteractor_vptr> visualizationPipeline();
	void computationPipeline();
};

#endif
