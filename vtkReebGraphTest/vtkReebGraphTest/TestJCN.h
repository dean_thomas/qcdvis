#pragma once

#include <vector>
#include <string>

class TestJCN
{
private:
	void ringBarDemo();
	void paraHeightDemo();
	void scissionTest();
	void qcdTest(const std::vector<std::string>& argv);
public:
	TestJCN(const std::vector<std::string>& argv);
	~TestJCN();
};

