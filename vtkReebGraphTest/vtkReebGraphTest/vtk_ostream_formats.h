#pragma once


#include <vtkGraph.h>
#include <vtkEdgeListIterator.h>
#include <vtkGenericCell.h>
#include <vtkPoints.h>
#include <vtkGeometryFilter.h>
#include <vtkPolygon.h>
#include <vtkSmartPointer.h>

#include <string>
#include <cassert>
#include <iostream>
#include <sstream>
#include <iomanip>

struct ObjFormat
{
	vtkCell* const cell;
	bool forceTriangulation;

	ObjFormat(vtkCell* const cell, bool forceTriangulation = false)
		: cell { cell }, forceTriangulation { forceTriangulation }
	{ }
};

inline std::ostream& operator<<(std::ostream& os, const ObjFormat& objFormat)
{
	auto cell = objFormat.cell;
	assert(cell != nullptr);

	using namespace std;
	
	if (cell->RequiresExplicitFaceRepresentation())
	{
		stringstream vertexStream;
		stringstream normalStream;

		size_t vCount = 0;
		
		stringstream faceStream;
		size_t fCount = 0;

		for (auto f = 0; f < cell->GetNumberOfFaces(); ++f)
		{
			auto face = objFormat.cell->GetFace(f);
			assert(face != nullptr);
			auto points = face->GetPoints();
			assert(points != nullptr);

			auto pointCount = points->GetNumberOfPoints();

			//	Use face normals
			double faceNormal[3];
			vtkPolygon::ComputeNormal(points, faceNormal);

			//	Writes the polygon as-is regardless of the number of points
			auto writePolygon = [&]()
			{
				//	Start the face stream line for this face
				faceStream << "f";

				for (auto p = 0; p < pointCount; ++p)
				{
					//	Extract the next point
					double point[3];
					points->GetPoint(p, point);

					//	Add the point the vertex list
					vertexStream << "v" << ' ' 
						<< std::setw(11) << std::fixed
						<< point[0] << ' ' 
						<< point[1] << ' ' 
						<< point[2] << endl;
					//	Write the normal to the buffer 3 times
					normalStream << "vn" << ' ' 
						<< std::setw(11) << std::fixed
						<< faceNormal[0] << ' '
						<< faceNormal[1] << ' '
						<< faceNormal[2] << endl;

					++vCount;

					//	Enter this vertex into the face stream
					//	Note: the vertex list runs from 1 (so we can increment the vCount first)
					faceStream << ' ' << vCount;
				}

				//	Close the face definition line for this face
				faceStream << endl;
				++fCount;
			};

			//	Writes the polygon as a series of triangles
			auto writeTriangles = [&]()
			{
				//	Convert to a polygon for triangulation
				auto polygon = vtkPolygon::SafeDownCast(face);
				assert(polygon != nullptr);

				//	Split the polygon into a list of id's defining the triangulation
				auto triangleIds = vtkSmartPointer<vtkIdList>::New();
				auto result = polygon->NonDegenerateTriangulate(triangleIds);
				assert(result == 1);

				//	Iterate over the list of individual polygons
				for (auto pId = 0; pId < triangleIds->GetNumberOfIds(); pId += 3)
				{
					//	Extract the next point
					double point0[3];
					points->GetPoint(triangleIds->GetId(pId), point0);
					double point1[3];
					points->GetPoint(triangleIds->GetId(pId+1), point1);
					double point2[3];
					points->GetPoint(triangleIds->GetId(pId+2), point2);

					//	Add the point the vertex list
					vertexStream << "v" << ' '
						<< std::setw(11) << std::fixed
						<< point0[0] << ' ' << point0[1] << ' ' << point0[2] << endl;
					//	Add the point the vertex list
					vertexStream << "v" << ' '
						<< std::setw(11) << std::fixed
						<< point1[0] << ' ' << point1[1] << ' ' << point1[2] << endl;
					//	Add the point the vertex list
					vertexStream << "v" << ' '
						<< std::setw(11) << std::fixed
						<< point2[0] << ' ' << point2[1] << ' ' << point2[2] << endl;

					//	Write the normal to the buffer 3 times
					normalStream << "vn" << ' ' << std::setw(11) << std::fixed
						<< faceNormal[0] << ' '
						<< faceNormal[1] << ' '
						<< faceNormal[2] << endl;
					normalStream << "vn" << ' ' << std::setw(11) << std::fixed
						<< faceNormal[0] << ' '
						<< faceNormal[1] << ' '
						<< faceNormal[2] << endl;
					normalStream << "vn" << ' ' << std::setw(11) << std::fixed
						<< faceNormal[0] << ' '
						<< faceNormal[1] << ' '
						<< faceNormal[2] << endl;

					//	Enter this vertex into the face buffer (v//vn)
					//	Note: the vertex list runs from 1
					faceStream << 'f' << ' '
						<< vCount + 1 << "//" << vCount + 1 << ' '
						<< vCount + 2 << "//" << vCount + 2 << ' '
						<< vCount + 3 << "//" << vCount + 3;

					vCount += 3;

					//	Close the face definition line for this face
					faceStream << endl;
					++fCount;
				}
			};

			if (objFormat.forceTriangulation)
			{
				//	Convert to triangles
				writeTriangles();			
			}
			else
			{
				//	Write the polygon as-is
				writePolygon();
			}
		}

		//	Compose the final output from the three buffers
		os << "#\tList of geometric vertices ( v\tvx,\tvy,\tvz )" << endl
			<< "#\tNumber of points in mesh: " << vCount << endl
			<< "#\tPolygons have been converted to triangles: " << boolalpha << objFormat.forceTriangulation << endl
			<< "#\tNote: vertices in obj format are indexed from 1 NOT 0 " << endl
			<< "#\tNote: duplicates have not been removed." << endl;
		os << vertexStream.str() << endl;

		os << "#\tList of normals for each vertex ( v\tnx,\tny,\tnz )" << endl
			<< "#\tNote: we are defining face normals." << endl
			<< "#\tNote: the normal index for each vertex matches that of the"
			<< " corresponding position (with duplicates)." << endl;
		os << normalStream.str() << endl;

		os << "#\tList of faces ( f\tv1\tv2\tv3 )" << endl
			<< "#\tNumber of faces in mesh: " << fCount << endl;
		os << faceStream.str() << endl;

		os << endl;
	}
	else
	{
		assert(false);
		/*
		auto points = objFormat.cell->GetPoints();
		assert(points != nullptr);


		auto pointCount = points->GetNumberOfPoints();
		auto faceCount = objFormat.cell->GetNumberOfFaces();

		os << "#\tList of geometric vertices ( v\tvx,\tvy,\tvz )" << endl
			<< "#\tNumber of points in mesh: " << pointCount << endl
			<< "#\tNote: vertices in obj format are indexed from 1 NOT 0." << endl;

		for (auto i = 0; i < pointCount; ++i)
		{
			double temp[3];
			points->GetPoint(i, temp);

			os << "v" << '\t' << temp[0] << '\t' << temp[1] << '\t' << temp[2] << endl;
		}
		os << endl;

		os << "#\tList of faces ( f\tv1\tv2\tv3 )" << endl
			<< "#\tNumber of faces in mesh: " << faceCount << endl;

		for (auto i = 0; i < faceCount; ++i)
		{
			auto face = objFormat.cell->GetFace(i);
			auto idList = face->GetPointIds();

			os << "f";
			for (auto j = 0; j < idList->GetNumberOfIds(); ++j)
			{
				os << '\t' << (idList->GetId(j) + 1);
			}
			os << endl;
		}
		*/
	}
	return os;
}

///
///		Prints a vtkGraph (and derrived classes) to a std::ostream
///		using DOT format.
///
///		\author		Dean
///		\since		19-01-2015
///
struct DotFormat
{
	vtkGraph* const graph;
	const std::string header;
	//std::ostream& m_os;

	DotFormat(vtkGraph * const graph, const std::string& header = "")
		: graph { graph }, header { header }
	{	}
};

inline std::ostream& operator<<(std::ostream& os, const DotFormat& dotFormat)
{
	assert(dotFormat.graph != nullptr);

	using namespace std;
	const char TAB = '\t';

	os << dotFormat.header << endl;

	std::string GRAPH_TYPE = "graph";
	std::string GRAPH_DELIMITER = "--";
	//std::string DELIMITER = "--";

	//	Make sure we have something to print
	auto it = vtkEdgeListIterator::New();
	dotFormat.graph->GetEdges(it);
	assert(it);

	//	Check for diagraphs
	if (dotFormat.graph->IsA("vtkDirectedGraph"))
	{
		GRAPH_TYPE = "digraph";
		GRAPH_DELIMITER = "->";
	}

	//	Summary as comment
	os << "//" << TAB
		<< "Number of vertices: " << dotFormat.graph->GetNumberOfVertices()
		<< ", number of edges: " << dotFormat.graph->GetNumberOfEdges()
		<< "." << endl << endl;

	//	Header
	os << GRAPH_TYPE << " g {" << endl;

	//	Iterate through edges
	while (it->HasNext())
	{
		//	Extract vertex information
		auto edge = it->Next();
		auto sourceVertex = edge.Source;
		auto targetVertex = edge.Target;
		auto edgeId = edge.Id;

		//	Print to os
		os << TAB
			<< "v" << sourceVertex
			<< " " << GRAPH_DELIMITER << " "
			<< "v" << targetVertex
			<< " [label=\"e" << edgeId << "\"];"
			<< endl;

	}
	//	Footer
	os << "}";

	return os;
}