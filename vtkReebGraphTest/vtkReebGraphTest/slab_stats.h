#ifndef SLAB_STATS_H
#define SLAB_STATS_H

#include <tuple>
#include <cassert>

#include <vtkCell.h>
#include <vtkPolygon.h>

#include "ProcessTimer.h"

///
/// \brief	Computes the number of polygon and triangles in the slab
/// \param  slab
/// \return
///
inline std::tuple<size_t, size_t>
count_triangles_and_polygons_in_slab(vtkCell* const slab)
{
	using namespace std;

	vtkCell* const cell = slab;
	assert(cell != nullptr);
	assert(cell->RequiresExplicitFaceRepresentation());

	//  Calculate the number of triangles and polygons
	auto triangle_count = 0ul;
	auto polygon_count = cell->GetNumberOfFaces();

	for (auto face = 0; face < cell->GetNumberOfFaces(); ++face)
	{
		//	Convert to a polygon for triangulation
		auto polygon = vtkPolygon::SafeDownCast(cell->GetFace(face));
		assert(polygon != nullptr);

		//	Split the polygon into a list of id's defining the triangulation
		auto triangle_ids = vtkSmartPointer<vtkIdList>::New();
		auto result = polygon->NonDegenerateTriangulate(triangle_ids);
		assert(result == 1);

		triangle_count += triangle_ids->GetNumberOfIds() / 3;
	}

	return make_tuple(polygon_count, triangle_count);
}

///
/// \brief Computes the surface area of the supplied slab
/// \param slab
/// \return
///
inline double calculate_surface_area(vtkCell* const slab)
{
	vtkCell* const cell = slab;
	assert(cell != nullptr);
	assert(cell->RequiresExplicitFaceRepresentation());

	//  Calculate the area of this mesh
	auto surface_area = 0.0;

	//  Used later to compute triangle area
	auto length_of = [&](const std::array<double,3>& v_a, const std::array<double,3>& v_b)
	{
		return sqrt(((v_a[0] - v_b[0]) * (v_a[0] - v_b[0]))
				+ ((v_a[1] - v_b[1]) * (v_a[1] - v_b[1]))
				+ ((v_a[2] - v_b[2]) * (v_a[2] - v_b[2])));
	};

	for (auto face = 0; face < cell->GetNumberOfFaces(); ++face)
	{
		//	Convert to a polygon for triangulation
		auto polygon = vtkPolygon::SafeDownCast(cell->GetFace(face));
		assert(polygon != nullptr);
		auto points = polygon->GetPoints();
		assert(points != nullptr);

		//	Split the polygon into a list of id's defining the triangulation
		auto triangle_ids = vtkSmartPointer<vtkIdList>::New();
		auto result = polygon->NonDegenerateTriangulate(triangle_ids);
		assert(result == 1);

		//	Iterate over the list of individual triangles
		for (auto p_id = 0; p_id < triangle_ids->GetNumberOfIds(); p_id += 3)
		{
			//	Extract points
			double point_a[3];
			points->GetPoint(triangle_ids->GetId(p_id), point_a);
			double point_b[3];
			points->GetPoint(triangle_ids->GetId(p_id+1), point_b);
			double point_c[3];
			points->GetPoint(triangle_ids->GetId(p_id+2), point_c);

			//  Compute lengths of edges
			auto a_b = length_of({ point_a[0], point_a[1], point_a[2]}, { point_b[0], point_b[1], point_b[2]});
			auto b_c = length_of({ point_b[0], point_b[1], point_b[2]}, { point_c[0], point_c[1], point_c[2]});
			auto c_a = length_of({ point_c[0], point_c[1], point_c[2]}, { point_a[0], point_a[1], point_a[2]});

			//  Semi-perimeter
			auto s = (a_b + b_c + c_a) / 2.0;

			//  Contribution from this triangle
			auto triangle_area = sqrt(s * (s-a_b) * (s-b_c) * (s-c_a));

			//  Add to mesh area
			surface_area += triangle_area;
		}
	}
	return surface_area;
}

///
/// \brief Computes the volume of the supplied slab
/// \param slab
/// \return
///
inline double calculate_volume(vtkCell* const slab)
{
	vtkCell* const cell = slab;
	assert(slab != nullptr);

	auto total_volume = 0.0;

	struct Point
	{
		double x;
		double y;
		double z;
	};

	auto volume_of_tet = [&](const Point& a, const Point& b, const Point& c,
			const Point& origin = { 0.0, 0.0, 0.0 })
	{
		Point t_a = { a.x - origin.x, a.y - origin.y, a.z - origin.z };
		Point t_b = { b.x - origin.x, b.y - origin.y, b.z - origin.z };
		Point t_c = { c.x - origin.x, c.y - origin.y, c.z - origin.z };

		return (1.0 / 6.0) * (- (t_a.x * t_b.y * t_c.z)
							  + (t_b.x * t_a.y * t_c.z)
							  + (t_a.x * t_c.y * t_b.z)
							  - (t_c.x * t_a.y * t_b.z)
							  - (t_b.x * t_c.y * t_a.z)
							  + (t_c.x * t_b.y * t_a.z));
	};

	for (auto face = 0; face < cell->GetNumberOfFaces(); ++face)
	{
		//	Convert to a polygon for triangulation
		auto polygon = vtkPolygon::SafeDownCast(cell->GetFace(face));
		assert(polygon != nullptr);
		auto points = polygon->GetPoints();
		assert(points != nullptr);

		//	Split the polygon into a list of id's defining the triangulation
		auto triangle_ids = vtkSmartPointer<vtkIdList>::New();
		auto result = polygon->NonDegenerateTriangulate(triangle_ids);
		assert(result == 1);

		//	contribution to volume from this face
		auto contrib_volume_face = 0.0f;

		//	Iterate over the list of individual triangles
		for (auto p_id = 0; p_id < triangle_ids->GetNumberOfIds(); p_id += 3)
		{
			//	Extract points
			double point_a[3];
			points->GetPoint(triangle_ids->GetId(p_id), point_a);
			double point_b[3];
			points->GetPoint(triangle_ids->GetId(p_id+1), point_b);
			double point_c[3];
			points->GetPoint(triangle_ids->GetId(p_id+2), point_c);

			//	Convert to easier to handle structs
			Point a = { point_a[0], point_a[1], point_a[2] };
			Point b = { point_b[0], point_b[1], point_b[2] };
			Point c = { point_c[0], point_c[1], point_c[2] };
			//Point o = { 6.0, 6.0, 6.0 };

			//	Compute the contributing volume of this triangle
			auto contrib_volume_triangle = volume_of_tet(a, b, c);

			//	Compute total volume contribution from this face
			contrib_volume_face += contrib_volume_triangle;
		}

		//	Add the contriubtion from this face to the total
		total_volume += contrib_volume_face;
	}
	return total_volume;
}

#endif // SLAB_STATS_H
