#pragma once
#ifndef ARG_PARSER_H
#define ARG_PARSER_H

#include <string>
#include <map>
#include <vector>
#include <cassert>
#include <iostream>
#include <algorithm>
#include <set>
//#include <wordexp.h>

struct Arguments
{
	struct Size2
	{
		size_t width;
		size_t height;
	};

	struct InputField
	{
		std::string fieldName;
		std::string slabSize;
	};

	//	Output stream files
	bool saveLogs = false;

	//	Flags for output obj meshes
    bool frag_obj = false;
    bool slab_obj = false;

    //  Summarize fragments and slabs
    bool frag_stats = false;
    bool slab_stats = false;
	
	Size2 jcnOutputSize = { 1920, 1080 };
	
	//	Updated switches
	//bool m_2d = true;
	std::map<std::string, InputField> m_files;

	//SaveOutput m_saveOutput;
	bool m_withGeometry = false;
	bool m_reebSkeleton = false;
	std::string m_applicationName;
	bool m_noGui = false;
	std::string m_outdir = "";

	bool m_tetScheme = 0;
	bool m_isPeridiocX = false;
	bool m_isPeridiocY = false;
	bool m_isPeridiocZ = false;

	//	Save Reeb files in DOT and Rb formats
	bool reebDot = false;
	bool reebRb = false;
	bool reebPng = false;

	//	Save JCN in DOT, JPG format
	std::string jcn_png_filename = "";
	std::string jcnDotFilename = "";

	std::string m_layoutName = "jcnlayout";

	float m_glyphScale = 0.0f;

	//	To check and update
	bool CentreSlabs = true;
	
	size_t m_explodeFactor = 1;
	bool Cut = false;

	//std::string CaptureFile;
	//std::string SubDir;
	//std::string ImageSubDir;
	//bool CaptureFlag = false;
	std::string ColourField;

	float lineOpacity = 0.2f;
	//std::map<std::string, float> Fields;
	//std::map<std::string, float> Filter;
	//std::map<std::string, vtkFloatArray*> ;
	//Bounds = (0, 0)
	//bool Smooth = false;
	//bool PhysFilter = false;
	
	
	//Objects["step"] = "-"
	//bool SaveFile = false;
	//bool PresetCam = false;
	//bool PresetCam1 = false;
	bool Colour = false;
	//bool Naive = true;
	//bool Data = false;
	//float Sample = 0.5;
	bool ShowBoundary = false;
	//bool JcnLayout = false;
	bool MDRG = false;

	struct
	{
		bool active = false;
		std::string measure = "persistence";
		float threshold = 0.1;
	} simplify;

	//size_t TimeStep = 0;
	bool ExtractG = false;
};

inline bool validateStrategyName(const std::string& val)
{
	//	Currently known layout options
	const std::set<std::string> knownLayouts =
	{
		"balloon", "circular", "davidsonharel", "dominance",
		"fmmm", "gem", "mmmexamplefast", "mmmexamplenice",
		"mmmexamplenotwist", "fruchtermanreingold", "kamadakawai",
		"sugiyama", "jcnlayout"
	};

	//	Convert input to lower case for the comparison
	std::string lower = val;
	std::transform(val.cbegin(), val.cend(), lower.begin(), tolower);

	return (knownLayouts.find(lower) != knownLayouts.cend());
}

inline std::string extractFilename(const std::string& path)
{
	using namespace std;

	auto pos = path.find_last_of("/\\");
	assert(pos != string::npos);

	return path.substr(pos + 1);
}

inline int toInt(const char* string)
{
	return atoi(string);
}

inline float toFloat(const char* string)
{
	return atof(string);
}

class ArgumentProcessor
{
private:
    static std::string expandPath(const std::string& input)
    {
        auto result = input;

        if (*result.cbegin() == '~')
        {
            //  Remove tilde
            result.erase(0, 1);

            //  Extract home directory and place at start of string
            std::string homepath = getenv("HOME");
            result = homepath + result;
        }
        return result;
    }

public:
	static Arguments ProcessArgs(const std::vector<std::string>& argv)
	{
		Arguments result;

		//	Should at least contain the application name as the first argument
		auto nrArgs = argv.size(); 
		assert(nrArgs >= 1);

		result.m_applicationName = argv[0];
		auto a = 1;

		cout << argv[0] << endl;

        if (nrArgs == 1)
        {
            //  Print the help message if no args were provided
            PrintHelp();
            return result;
        }

		while (a < nrArgs)
		{
			//	Process the current argument (converting it to uppercase)
			std::cout << argv[a] << std::endl;
			std::string currentArg = argv[a];
			std::transform(currentArg.begin(), currentArg.end(), currentArg.begin(), toupper);

			if ((currentArg == "-LINE_OPACITY") && (a + 1 < nrArgs))
			{
				auto opacity = toFloat(argv[a + 1].c_str());
				
				assert((opacity >= 0.0f) && (opacity <= 1.0f));
				result.lineOpacity = opacity;

				a += 2;
			}
            else if (currentArg == "-FRAG_OBJ")
			{
                result.frag_obj = true;
				a += 1;
			}
            else if (currentArg == "-FRAG_STATS")
            {
                result.frag_stats = true;
                a += 1;
            }
            else if (currentArg == "-SLAB_OBJ")
			{
                result.slab_obj = true;
				a += 1;
			}
            else if (currentArg == "-SLAB_STATS")
            {
                result.slab_stats = true;
                a += 1;
            }
			else if (currentArg == "-JCN_DOT")
			{
				result.jcnDotFilename = "jcn.dot";
				a += 1;
			}
			else if (currentArg == "-JCN_PNG")
			{
				result.jcn_png_filename = "jcn.png";
				a += 1;
			}
			else if ((currentArg == "-OUT_DIR") && (a + 1 < nrArgs))
			{
                std::string path = expandPath(argv[a + 1]);

                std::cout << "Output directory is set to: " << path << std::endl;

                result.m_outdir = path;
				a += 2;
			}
			else if ((currentArg == "-NO_GUI"))
			{
				result.m_noGui = true;
				++a;
			}
			else if ((currentArg == "-INPUT") && (a + 3 < nrArgs))
			{
                auto filename = expandPath(argv[a + 1]);
				auto fieldName = argv[a + 2];
				auto interval = argv[a + 3];

				result.m_files[filename] = { fieldName, interval };
				
				a += 4;
			}
			else if ((currentArg == "-LAYOUT") && (a + 1 < nrArgs))
			{
				//	Allow the specification of layout strategies via the
				//	command line
				auto strategy = argv[a + 1];

				if (!validateStrategyName(strategy))
				{
					std::cerr << "Layout strategy '" << strategy << "' is not recognized." << std::endl;
				}
				else
				{
					result.m_layoutName = strategy;
				}
				a += 2;
			}
			else if ((currentArg == "-oaeS") && (a + 1 < nrArgs))
			{
				result.m_glyphScale = toFloat(argv[a + 1].c_str());
				a += 2;
			}
			else if ((currentArg == "-EXPLODE") && (a + 1 < nrArgs))
			{
				result.m_explodeFactor = toFloat(argv[a + 1].c_str());
				a += 2;
			}
			else if (argv[a] == "-NCENTRE")
			{
				result.CentreSlabs = false;
				a += 1;
			}
			else if (currentArg == "-GEOM")
			{
				result.m_withGeometry = true;
				a += 1;
			}
			else if ((currentArg == "-COLOUR") && (a + 1 < nrArgs))
			{
				result.ColourField = argv[a + 1];
				a += 2;
			}
			//else if (argv[a] == "-smooth")
			//{
			//	result.Smooth = true;
			//	a += 1;
			//}
			else if (currentArg == "-CUT")
			{
				result.Cut = true;
				a += 1;
			}
			//else if (argv[a] == "-physics")
			//{
			//	result.PhysFilter = true;
			//	a += 1;
			//}
			else if ((currentArg == "-REEB_DOT"))
			{
				result.reebDot = true;
				a += 1;
			}
			else if ((currentArg == "-REEB_RB"))
			{
				result.reebRb = true;
				a += 1;
			}
			else if ((currentArg == "-REEB_PNG"))
			{
				result.reebPng = true;
				a += 1;
			}
			else if (currentArg == "-COLOURMAP")
			{
				result.Colour = true;
				a += 1;
			}
			//else if (argv[a] == "-HigherDim")
			//{
			//	result.Naive = false;
			//	a += 1;
			//}
			//else if (argv[a] == "-data")
			//{
			//	result.Data = true;
			//	a += 1;
			//}
			//else if ((argv[a] == "-sample") && (a + 1 < nrArgs))
			//{
			//	result.Sample = toFloat(argv[a + 1].c_str());
			//	a += 2;
			//}
			else if (currentArg == "-BOUNDARY")
			{
				result.ShowBoundary = true;
				a += 1;
			}
			//else if (argv[a] == "-jcnlayout")
			//{
			//	result.JcnLayout = true;
			//	a += 1;
			//}
			//else if (argv[a] == "-cam")
			//{
			//	result.PresetCam = true;
			//	a += 1;
			//}
			else if (currentArg == "-MDRG")
			{
				result.MDRG = true;
				a += 1;
			}
			else if (currentArg == "-RS")
			{
				result.m_reebSkeleton = true;
				a += 1;
			}
			else if ((currentArg == "-SIMPLIFY") && (a + 2 < nrArgs))
			{
				result.simplify.active = true;

				//	Extract persistence measure and threshold
				auto metric = argv[a+1];
				std::transform(metric.begin(), metric.end(), metric.begin(), tolower);
				assert((metric == "volume") || (metric == "persistence"));
				result.simplify.measure = metric;
				result.simplify.threshold = toFloat(argv[a+2].c_str());

				a += 3;
			}
			else if (currentArg == "-EXTRACTG")
			{
				result.ExtractG = true;
				a += 1;
			}
			else if (currentArg == "-PERIODIC")
			{
				result.m_isPeridiocX = true;
				result.m_isPeridiocY = true;
				result.m_isPeridiocZ = true;

				//std::cout << "Set periodic boundaries on." << std::endl;
				a += 1;
			}
			else if (currentArg == "-PERIODIC_X")
			{
				result.m_isPeridiocX = true;

				a += 1;
			}
			else if (currentArg == "-PERIODIC_Y")
			{
				result.m_isPeridiocY = true;

				a += 1;
			}
			else if (currentArg == "-PERIODIC_Z")
			{
				result.m_isPeridiocZ = true;

				a += 1;
			}
			else if ((currentArg == "-TET") && (a + 1 < nrArgs))
			{
				auto ts = toInt(argv[a + 1].c_str());

				if (ts < 2)
				{
					result.m_tetScheme = ts;
					std::cout << "Set tet scheme to " << ts << "." << std::endl;
				}
				else
				{
					std::cerr << "Invalid tetrahedral scheme provided.  Value must be 0 or 1." << std::endl;
				}
				a += 2;
			}
			else if ((currentArg == "-SAVE_LOGS"))
			{
				result.saveLogs = true;
				a += 1;
			}
			else if (currentArg == "-HELP")
			{
				PrintHelp();
			}
			else
			{
				std::cerr << "unexpected arg: " << argv[a] << " skipping: use -help for help." << std::endl;
				//	Deprecated flags
				std::cerr << "\t-saveOutput flag is deprecated and should be replaced by -jcnJpg or -jcnDot." << std::endl;
				std::cerr << "\t-saveReebGraphs flag is deprecated and should be replaced by -reebDot, -reebPng or -reebRb." << std::endl;
				a += 1;
			}
		}

		return result;
	}

	static void PrintHelp()
	{
		using namespace std;
		cout << "Usage: ComputeJCN [args...]" << endl;
		cout << "args consist of script-specific arguments, followed by:" << endl;
		cout << "  -save_logs: logging streams will be stored in files in the output directory." << endl;
		cout << "  -help: this message." << endl;
		cout << "  -layout [name]: set the graph layout style." << endl;
		//...to check and modify
		cout << "  -glyphs [num]: scale graph glyphs by real value num" << endl;
		cout << "  -explode [num]: push slab geometry apart, by the specified real scaling factor" << endl; 
		cout << "  -nCentre: turns off centreing on the first slab." << endl;
		cout << "  -geom: switch to the older vtkJCNWithGeometry filter, generating slab geometry" << endl;
		cout << "  -colour [name]: use the named field to colour slab geometry (defaults to first field)" << endl;
		cout << "  -smooth: turn on antialiasing in window" << endl;
		cout << "  -cut: turn on cutting plane in geometric view (press 'i' in the window to see interactor" << endl;
		cout << "  -HigherDim : can hanlde higher dimensional (>3) domain and range space. " << endl;
		cout << "  -RS: Render Reeb Skeleton graph." << endl;
		cout << "  -simplify [measure] [threshold]: applies simplification to the Reeb skeleton.  Allowable the persistence measures are 'volume' or 'persistence'." << endl;
		//	Drawing parameters
		cout << "  -line_opacity [num]: sets the opacity of edges in the JCN output." << endl;
		//	Input formatting
		cout << "  -input [filename] [identifier] [slab size]: add the named field to the JCN, using the specific slab width (prefix with an 'i' to calculate the slab size based on range and number of intervals)." << endl;
		cout << "  -periodic_x: data wraps around on the x-axis." << endl;
		cout << "  -periodic_y: data wraps around on the y-axis." << endl;
		cout << "  -periodic_z: data wraps around on the z-axis." << endl;
		cout << "  -periodic: data wraps around to the origin." << endl;
		cout << "  -tet [0/1]: set the tetrahedra scheme to either 6 or 5 tets per cell." << endl;
		//	Output formatting
		cout << "  Output Formats:" << endl;
		cout << "  -out_dir [path]: output directory for files." << endl;
		cout << "  -jcn_dot [filename]: saves the joint contour net in dot format." << endl;
		cout << "  -jcn_jpg [filename]: save the rendered JCN as a jpeg image." << endl;
		cout << "  -reeb_dot: saves the reeb graphs in dot format." << endl;
		cout << "  -reeb_rb: saves the reeb graphs in rb format." << endl;
		cout << "  -reeb_png: saves the reeb graphs in png format." << endl;
        cout << "  -slab_obj: output slab meshes in obj format." << endl;
        cout << "  -frag_obj: output fragment meshes in obj format (not recommended)." << endl;
        cout << "  -slab_stats: output a summary of the slabs in 'slab_stats.txt'." << endl;
        cout << "  -frag_stats: output a summary of the slabs in 'frag_stats.txt'." << endl;
		cout << "" << endl;
	}
};

#endif
