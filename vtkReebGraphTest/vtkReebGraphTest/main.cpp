//#include "TestReebGraph.h"
#include "JCNTools.h"
#include "ArgParser.h"
#include "LogRedirect.h"
#include "MemoryLogger.h"
#include "ProcessTimer.h"

#include <vtkAutoInit.h>
#include <vtkFileOutputWindow.h>
#include <vtkSmartPointer.h>

VTK_MODULE_INIT(vtkRenderingOpenGL);
VTK_MODULE_INIT(vtkInteractionStyle);
VTK_MODULE_INIT(vtkRenderingFreeType);

#include <string>
#include <vector>
#include <iostream>
#include <chrono>
#include <ctime>

//#define PAUSE_ON_EXIT

#ifndef NDEBUG
#define BUILD_TYPE "DEBUG"
#else
#define BUILD_TYPE "Release"
#endif

using namespace std;


void initializeVtkErrorLogger(const std::string& filename)
{
	//	Initilize an error log and set it as active
	auto errorLogOutput = vtkSmartPointer<vtkFileOutputWindow>::New();
	errorLogOutput->SetFileName(filename.c_str());
	vtkFileOutputWindow::SetInstance(errorLogOutput);
}

int main(int argc, char* argv[])
{
	cout << "vtkReebGraphTest [" << BUILD_TYPE 
		<< "] build: " 	<< __DATE__ << " " << __TIME__ << endl << endl;
	
	//	Parse command line arguments
	auto args = ArgumentProcessor::ProcessArgs(std::vector<std::string>(argv, argv + argc));

	LogRedirect logRedirect;
	if (args.saveLogs)
	{
		//	Generate a filename for the logfiles
		stringstream ss;
		std::time_t t = std::time(nullptr);
		std::tm tm = *std::localtime(&t);
		ss << (args.m_outdir != "" ? args.m_outdir + "/" : "");
		ss << std::put_time(&tm, "%d%m%y_%H%M%S");
		auto prefix =  ss.str();
		cout << prefix << endl;

		logRedirect.redirectCoutToFile(prefix + "_stdout.txt");
		logRedirect.redirectCerrToFile(prefix + "_stderr.txt");
		logRedirect.redirectClogToFile(prefix + "_stdlog.txt");
		initializeVtkErrorLogger(prefix + "_vtkError.txt");
	}

	//	Start logging
	MemoryLogger memoryLogger(__PRETTY_FUNCTION__);
	ProcessTimer processTimer(__PRETTY_FUNCTION__);

	//	Create a data structure for the algorithm
	auto jcnComputation = JCNTools(args);
	
	try
	{
		//	Process the loaded data
		jcnComputation.computationPipeline();

		//	Visualization via vtk required
		//if (args.jcnJpgFilename != "")
		//{

			//	No UI required
			if (!args.m_noGui)
			{
                auto result = jcnComputation.visualizationPipeline();

                //	If we want to examine the data directly
                auto win = std::get<0>(result);
                auto gui = std::get<1>(result);

				win->Render();
				gui->Start();
			}
		//}
	}
	catch (const std::length_error& ex)
	{
		//	Thrown if there is no data to be computed
		cerr << ex.what() << endl;
	}

	cout << "Done." << endl;
#if ((!defined(NDEBUG)) && defined(PAUSE_ON_EXIT))
	getchar();
#endif
	return 0;
}
