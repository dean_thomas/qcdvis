#pragma once
#ifndef VTK_OSTREAM_H
#define VTK_OSTREAM_H

#include "vtk_ostream_formats.h"

#include <vtkGraph.h>
#include <vtkEdgeListIterator.h>
#include <vtkSmartPointer.h>
#include <vtkCellIterator.h>
#include <vtkCell.h>
#include <vtkCellType.h>
#include <vtkCellTypes.h>
#include <vtkCellData.h>
#include <vtkPointData.h>
#include <vtkOutEdgeIterator.h>
#include <vtkVertexListIterator.h>
#include <vtkFloatArray.h>
#include <vtkInEdgeIterator.h>

#ifdef VTK_META_SUPPORT
#include "vtkLocal/vtkJCNWithGeometry.h"
#endif
//#include "vtkLocal\vtkJCNWithGeometry.cxx"

#include <ostream>
#include <cassert>
#include <string>

#define ATTRIB_OPEN "[ "
#define ATTRIB_CLOSE " ]"

#define LIST_OPEN "{ "
#define LIST_CLOSE " }"

#define TUPLE_OPEN "( "
#define TUPLE_CLOSE " )"

#define DELIMITER std::string(", ")

#ifdef VTK_META_SUPPORT
inline std::ostream& operator<<(std::ostream& os, _Field* const field)
{
	os << "_Field = " << ATTRIB_OPEN;

	
	os << "name : " << field->name << DELIMITER
		<< "isBounded : " << field->isBounded << DELIMITER
		<< "min / max : " << TUPLE_OPEN << field->minBound
		<< DELIMITER << field->maxBound << TUPLE_CLOSE << DELIMITER
		<< "slab width : " << field->slabWidth << DELIMITER
		<< "node scalars : " << field->nodeScalars.GetPointer() << DELIMITER
		<< "point scalars : " << field->pointScalars << DELIMITER
		<< "slab scalars : " << field->slabScalars.GetPointer() << DELIMITER
		<< "temp scalars : " << field->tempScalars.GetPointer();

	os << ATTRIB_CLOSE;
	return os;
}

inline std::ostream& operator<<(std::ostream& os, vtkJCNWithGeometry* const jcn)
{
	assert(jcn != nullptr);
	auto fieldCount = jcn->GetNumberOfFields();


	os << "vtkJCNWithGeometry = "
		<< ATTRIB_OPEN
		<< "number of fields : " << fieldCount << DELIMITER;

	os << "fields : " << LIST_OPEN;
	for (auto i = 0; i < fieldCount; ++i)
	{
		auto field = jcn->GetFieldData(i);
		assert(field != nullptr);

		if (i > 0) os << DELIMITER;

		os << TUPLE_OPEN << i << DELIMITER << field << TUPLE_CLOSE;
	}
	os << LIST_CLOSE;
	
	os << ATTRIB_CLOSE;
	return os;
}
#endif
///
///		\brief		Output the contents of a vtkLookupTable to the output stream
///		\since		08-04-2016
///		\author		Dean
///
inline std::ostream& operator<<(std::ostream&os, vtkLookupTable* const lookupTable)
{
	os << "vtkLookupTable = ";
	os << ATTRIB_OPEN;

	os << ATTRIB_CLOSE;
	return os;
}

///
///		\brief		Output the contents of a vtkDataArray to the output stream
///		\since		07-04-2016
///		\author		Dean
///
inline std::ostream& operator<<(std::ostream& os, vtkDataArray* const dataArray)
{
	assert(dataArray != nullptr);

	auto tupleCount = dataArray->GetNumberOfTuples();
	auto tupleSize = dataArray->GetNumberOfComponents();

	os << "vtkDataArray<" << dataArray->GetDataTypeAsString()
		<< "> = " << ATTRIB_OPEN;
	os << "name : " << dataArray->GetName() << DELIMITER;
	os << "number of tuples : " << tupleCount;

	//	List the tuples
	os << "tuples : " << LIST_OPEN;
	double* temp = new double[tupleSize];
	for (auto i = 0; i < tupleCount; ++i)
	{
		if (i > 0) os << DELIMITER;

		//	Extract the tuple
		dataArray->GetTuple(i, temp);
		os << TUPLE_OPEN << i << DELIMITER
			<< TUPLE_OPEN;
		for (auto j = 0; j < tupleSize; ++j)
		{
			//	List elements of tuple
			if (j > 0) os << DELIMITER;
			os << temp[j];
		}
		os << TUPLE_CLOSE << TUPLE_CLOSE;
	}
	delete[] temp;
	os << LIST_CLOSE;
	/*
	<< DELIMITER;

	//	List the lookup table
	os << "lookup table : ";
	os << dataArray->GetLookupTable();
	*/
	os << ATTRIB_CLOSE;
	return os;
}

///
///		\brief		Output the contents of a vtkPointData to the output stream
///		\details	A vtkPointData structure consists of *multiple* arrays of
///					elements, each with their own string identifier.  The number
///					of tuples in each array appears to be constant.
///		\since		07-04-2016
///		\author		Dean
///
inline std::ostream& operator<<(std::ostream& os, vtkPointData* const pointData)
{
	assert(pointData != nullptr);

	auto arrayCount = pointData->GetNumberOfArrays();

	os << "vtkPointData = " << ATTRIB_OPEN
		<< "Number of Arrays : " << arrayCount << DELIMITER;

	//	List array names
	os << "Array names : " << LIST_OPEN;
	for (auto i = 0; i < arrayCount; ++i)
	{
		os << TUPLE_OPEN << i << DELIMITER << pointData->GetArrayName(i) << TUPLE_CLOSE;
	}
	os << LIST_CLOSE << DELIMITER;

	os << "Number of Tuples : " << pointData->GetNumberOfTuples() << DELIMITER
		<< "Number of Components : " << pointData->GetNumberOfComponents() << DELIMITER;

	//	List data arrays
	os << "Array data : " << LIST_OPEN;
	for (auto i = 0; i < arrayCount; ++i)
	{
		if (i > 0) os << DELIMITER;

		os << TUPLE_OPEN << pointData->GetArrayName(i) 
			<< DELIMITER << pointData->GetArray(pointData->GetArrayName(i)) 
			<< TUPLE_CLOSE;
	}
	os << LIST_CLOSE;

	os << ATTRIB_CLOSE;
	return os;
}

///
///		\brief		Output the contents of a vtkPoints to the output stream
///		\details	vtkPoints represents a series of points in 3D space
///		\since		07-04-2016
///		\author		Dean
///
inline std::ostream& operator<<(std::ostream& os, vtkPoints* const points)
{
	assert(points != nullptr);

	auto count = points->GetNumberOfPoints();

	os << "vtkPoints = " << ATTRIB_OPEN
		<< "Number of points : " << count << DELIMITER
		<< LIST_OPEN;

	for (auto i = 0; i < points->GetNumberOfPoints(); ++i)
	{
		if (i > 0) os << DELIMITER;

		double temp[3];
		points->GetPoint(i, temp);

		//	Printed form "( n, { nx, ny, nz } )"
		os << TUPLE_OPEN << i << DELIMITER
			<< LIST_OPEN << temp[0] << DELIMITER
			<< temp[1] << DELIMITER
			<< temp[2] << LIST_CLOSE << TUPLE_CLOSE;
	}
	os << LIST_CLOSE;
	os << ATTRIB_CLOSE;
	return os;
}

inline std::ostream& operator<<(std::ostream& os, vtkCell* const cell)
{
	assert(cell != nullptr);

	os << "vtkCell<" << vtkCellTypes::GetClassNameFromTypeId(cell->GetCellType())
		<< "> = " << ATTRIB_OPEN
		<< "IsExplicitCell : " << (cell->IsExplicitCell() ? "true" : "false") << DELIMITER
		<< "Requires explicit face representation : " 
		<< (cell->RequiresExplicitFaceRepresentation() ? "true" : "false" ) << DELIMITER
		<< "Numbor Of Points : " << cell->GetNumberOfPoints() << DELIMITER
		<< "Number Of Edges : " << cell->GetNumberOfEdges() << DELIMITER
		<< "Number Of Faces : " << cell->GetNumberOfFaces() << DELIMITER;

	//	Print the points array
	auto points = cell->GetPoints();
	assert(points != nullptr);
	os << "Points : " << points << DELIMITER;

	os << "Faces : " << LIST_OPEN;
	for (auto i = 0; i < cell->GetNumberOfFaces(); ++i)
	{
		if (i > 0) os << DELIMITER;

		auto faceCell = cell->GetFace(i);
		os << TUPLE_OPEN << i << DELIMITER << faceCell << TUPLE_CLOSE;
	}
	os << LIST_CLOSE;
	

	os << ATTRIB_CLOSE;
	return os;
}

inline std::ostream& operator<<(std::ostream& os, vtkUnstructuredGrid* const unstructuredGrid)
{
	assert(unstructuredGrid != nullptr);

	auto cellCount = unstructuredGrid->GetNumberOfCells();

	os << "vtkUnstructuredGrid = " << ATTRIB_OPEN
		<< "Number of cells: " << cellCount << DELIMITER
		<< "Number of points: " << unstructuredGrid->GetNumberOfPoints() << DELIMITER;

	//	Print the list of cells
	os << "Cell array: " << LIST_OPEN;
	for (auto i = 0; i < cellCount; ++i)
	{
		if (i > 0) os << DELIMITER;

		auto cell = unstructuredGrid->GetCell(i);
		assert(cell != nullptr);

		os << TUPLE_OPEN << i << DELIMITER << cell << TUPLE_CLOSE;
	}
	os << LIST_CLOSE;

	//	Print the point data
	auto pointData = unstructuredGrid->GetPointData();
	assert(pointData != nullptr);
	os << pointData;

	os << ATTRIB_CLOSE;
	return os;
}

inline std::ostream& operator<<(std::ostream& os, vtkFloatArray* const floatArray)
{
	for (auto it = floatArray->Begin(); it != floatArray->End(); ++it)
	{
		os << *it << "\t";
	}

	return os;
}

/*
inline std::ostream& operator<<(std::ostream& os, vtkDataArray* const dataArray)
{
	os << "vtkDataArray<" << vtkDataSetAttributes::GetAttributeTypeAsString(dataArray->GetArrayType())
		<< "> = " << ATTRIB_OPEN;

	os << ATTRIB_CLOSE;
	return os;
}
*/

inline std::ostream& operator<<(std::ostream& os, vtkDataSetAttributes* const dataSetAttributes)
{
	//if (vtkCellData::SafeDownCast(dataSetAttributes))
	//{
	//	os << vtkCellData::SafeDownCast(dataSetAttributes);
	//}
	//else if (vtkPointData::SafeDownCast(dataSetAttributes))
	//{
	//	os << vtkPointData::SafeDownCast(dataSetAttributes);
	//}
	//else
	{
		os << "vtkDataSetAttributes = " << ATTRIB_OPEN;

		auto numberOfArrays = dataSetAttributes->GetNumberOfArrays();
		
		os << "number of arrays : " << numberOfArrays;

		

		os << "array names : " << LIST_OPEN;
		for (auto i = 0; i < numberOfArrays; ++i)
		{
			if (i > 0) os << DELIMITER;
			dataSetAttributes->GetArrayName(i);
		}
		os << LIST_CLOSE;

		os << "arrays : " << LIST_OPEN;
		for (auto i = 0; i < numberOfArrays; ++i)
		{
			if (i > 0) os << DELIMITER;

			os << dataSetAttributes->GetArray(dataSetAttributes->GetArrayName(i));
		}
		os << LIST_CLOSE;
		

		os << ATTRIB_CLOSE;
	}
	return os;
}

inline std::ostream& operator<<(std::ostream& os, vtkCellData* const cellData)
{
	os << "vtkCellData" << ATTRIB_OPEN;

	auto numberOfArrays = cellData->GetNumberOfArrays();

	//for (auto i = 0; )
	//dataSetAttribtes->GetArrayName(i);

	os << ATTRIB_CLOSE;
	return os;
}

inline std::ostream& operator<<(std::ostream& os, vtkGraph* const graph)
{
	assert(graph != nullptr);

	//	Is this graph directed or undirected?
	auto isDirectedGraph = graph->IsA("vtkDirectedGraph");

	auto vertexPrinter = [&](std::ostream& os, const vtkIdType& vertexId) -> std::ostream&
	{
		os << TUPLE_OPEN;
		os << "vertex id : " << vertexId << DELIMITER;

		if (isDirectedGraph)
		{
			//	Lists of inward and outward edges will be different, so print both
			os << "in-degree : " << graph->GetInDegree(vertexId) << DELIMITER
				<< "out-degree : " << graph->GetOutDegree(vertexId) << DELIMITER;

			//	List in-edge id's
			os << "in edge list : " << LIST_OPEN;
			vtkSmartPointer<vtkInEdgeIterator> inEdgeIterator = vtkInEdgeIterator::New();
			graph->GetInEdges(vertexId, inEdgeIterator);
			assert(inEdgeIterator != nullptr);
			auto inCount = 0;
			while (inEdgeIterator->HasNext())
			{
				if (inCount > 0) os << DELIMITER;
				auto edge = inEdgeIterator->Next();

				os << edge.Id;
				++inCount;
			}
			os << LIST_CLOSE << DELIMITER;

			//	List out-edge id's
			os << "out edge list : " << LIST_OPEN;
			vtkSmartPointer<vtkOutEdgeIterator> outEdgeIterator = vtkOutEdgeIterator::New();
			graph->GetOutEdges(vertexId, outEdgeIterator);
			assert(outEdgeIterator != nullptr);
			auto outCount = 0;
			while (outEdgeIterator->HasNext())
			{
				if (outCount > 0) os << DELIMITER;
				auto edge = outEdgeIterator->Next();

				os << edge.Id;
				++outCount;
			}
			os << LIST_CLOSE;
		}
		else
		{
			//	Lists of inward and outward edges will be the same, so only print one set
			os << "degree : " << graph->GetDegree(vertexId) << DELIMITER;

			//	List connected edge id's - note: we are using a 'vtkOutEdgeIterator'
			//	but as the graph is undirected we could equally use a 'vtkInEdgeIterator'
			//	to achieve the same list of edges
			os << "edge list : " << LIST_OPEN;
			vtkSmartPointer<vtkOutEdgeIterator> edgeIterator = vtkOutEdgeIterator::New();
			graph->GetOutEdges(vertexId, edgeIterator);
			assert(edgeIterator != nullptr);
			auto edgeCount = 0;
			while (edgeIterator->HasNext())
			{
				if (edgeCount > 0) os << DELIMITER;
				auto edge = edgeIterator->Next();

				os << edge.Id;
				++edgeCount;
			}
			os << LIST_CLOSE;
		}

		os << TUPLE_CLOSE;
		return os;
	};

	auto edgePrinter = [&](std::ostream& os, const vtkIdType& edgeId) -> std::ostream&
	{
		os << TUPLE_OPEN;
		os << "edge id : " << edgeId << DELIMITER
			<< "source vertex id : " << graph->GetSourceVertex(edgeId) << DELIMITER
			<< "target vertex id : " << graph->GetTargetVertex(edgeId);
		os << TUPLE_CLOSE;

		return os;
	};

	os << "vtkGraph = " << ATTRIB_OPEN
		<< "is directed graph : " << std::boolalpha << isDirectedGraph << DELIMITER
		<< "number of vertices : " << graph->GetNumberOfVertices() << DELIMITER
		<< "number of edges : " << graph->GetNumberOfEdges() << DELIMITER;

	//	List vertices
	os << LIST_OPEN;
	vtkSmartPointer<vtkVertexListIterator> vIt = vtkVertexListIterator::New();
	graph->GetVertices(vIt);
	assert(vIt != nullptr);
	auto vCount = 0;
	while (vIt->HasNext())
	{
		if (vCount > 0) os << DELIMITER;
		auto vId = vIt->Next();
		vertexPrinter(os, vId);
		++vCount;
	}
	os << LIST_CLOSE;

	//	List edges
	os << LIST_OPEN;
	vtkSmartPointer<vtkEdgeListIterator> eIt = vtkEdgeListIterator::New();
	graph->GetEdges(eIt);
	assert(eIt != nullptr);
	auto eCount = 0;
	while (eIt->HasNext())
	{
		if (eCount > 0) os << DELIMITER;
		auto eId = eIt->Next();
		edgePrinter(os, eId.Id);
		++eCount;
	}
	os << LIST_CLOSE;

	//	Print vertex attributes
	os << "vertex data : " << graph->GetVertexData();

	os << ATTRIB_CLOSE;
	return os;
}

#undef DELIMITER

#endif
