#include "JCNTools.h"

#include "GraphConversion.h"
#include "RbFormat.h"
#include <ProcessTimer.h>

#include <exception>
#include <chrono>
#include <string>

#include <vtkFloatArray.h>
#include <vtkCell.h>
#include <vtkPNGWriter.h>
#include <vtkGraphLayout.h>
#include <vtkImageCast.h>
#include <vtkGL2PSExporter.h>

#include "slab_stats.h"

using namespace std;

//#define DESKTOP std::string("C:\\Users\\4thFloor\\Desktop\\")

bool JCNTools::loadHeightField(const std::string& filename, const std::string& identifier, const float slabs, bool normalizeToRange)
{
	//cout << "Loading file: " << filename << endl;

	Vol1Reader<float> vol1reader;

	float slabSize;

	try
	{
		//	Load the data in to HeightField3 structure
		vol1reader(filename);
		auto heightField = vol1reader.data;
		auto min = 100000.0f;
		auto max = -100000.0f;
		for (auto it = heightField.cbegin(); it != heightField.cend(); ++it)
		{
			if (*it > max) max = *it;
			if (*it < min) min = *it;
		}

		if (normalizeToRange)
		{
			slabSize = (max - min) / slabs;

			cout << "Slab size calculated to be: " << slabSize
				<< " (range: " << min << ".." << max << ") in "
				<< slabs << " intervals." << endl;
		}
		else
		{
			slabSize = slabs;
		}
		//ofstream logFile("C:\\Users\\4thFloor\\Desktop\\log_file.txt");

		//	Convert the HeightField3 structure to something VTK can handle
		auto f1 = vtkFloatArray::New();
		for (auto it = heightField.cbegin(); it != heightField.cend(); ++it)
		{
		//	logFile << *it << endl;

			f1->InsertNextValue(*it);
		}
		//logFile.close();

		//	Name the field
		f1->SetName(identifier.c_str());

		//	Store the field data for processing later
		FieldData data = { f1, heightField.GetDimX(), heightField.GetDimY(), heightField.GetDimZ(), slabSize, (size_t)slabs, min, max };
		m_data[identifier] = data;

		return true;
	}
	catch (const std::invalid_argument& ex)
	{
		//	could be throw if a file is not found etc
		cerr << ex.what() << endl;
		return false;
	}
}

vtkSmartPointer<vtkSimplicate> JCNTools::generateSimplicateData() const
{
	auto it = m_data.cbegin();
	auto dimX = it->second.dimX;
	auto dimY = it->second.dimY;
	auto dimZ = it->second.dimZ;

	auto gr = vtkSmartPointer<vtkImageData>(vtkImageData::New());
	gr->SetDimensions(dimZ, dimY, dimX);
	//	gr.SetScalarTypeToFloat()

	for (; it != m_data.cend(); ++it)
	{
		//	Presumably dimensions of field must be the same
		assert(it->second.dimX == dimX);
		assert(it->second.dimY == dimY);
		assert(it->second.dimZ == dimZ);

		gr->GetPointData()->AddArray(it->second.data);
	}

	auto simp = vtkSmartPointer<vtkSimplicate>(vtkSimplicate::New());

	//	Set periodic nature of data
	simp->SetIsPeriodicX(m_arguments.m_isPeridiocX);
	simp->SetIsPeriodicY(m_arguments.m_isPeridiocY);
	simp->SetIsPeriodicZ(m_arguments.m_isPeridiocZ);

	//	Set the tetrahedra scheme for the data
	simp->SetScheme(m_arguments.m_tetScheme);

	//	Set the scalar values (for multiple fields)
	simp->SetInputData(gr);
	simp->Update();

	auto unstructuredGrid = simp->GetOutput();


	//  Not sure how to extract the edge count just yet...
	const char TAB = '\t';
	clog << "vtkSimplicate output: " << endl;
	clog << TAB << "vertices: " << unstructuredGrid->GetNumberOfPoints() << endl;
	clog << TAB << "edges: " << "null" << endl;
	clog << TAB << "triangles: " << unstructuredGrid->GetNumberOfCells() << endl;

	return simp;
}

JCNTools::JCNTools(const Arguments& cmdArguments)
	: m_arguments { cmdArguments }
{
	for (auto it = m_arguments.m_files.cbegin(); it != m_arguments.m_files.cend(); ++it)
	{
		auto fileName = it->first;
		auto fieldName = it->second.fieldName;
		auto interval = it->second.slabSize;

		if (interval.at(0) == 'i')
		{
			//cout << "Detected normalization flag" << endl;

			std::string param = interval.substr(1);

			//	Slab size should be normalized to range
			auto intervals = toFloat(param.c_str());

			if (!loadHeightField(fileName, fieldName, intervals, true))
			{
				std::cerr << "Loading data: '" << fileName << "' failed." << std::endl;
			}
		}
		else
		{
			//cout << "No normalization flag" << endl;

			//	SSlab size should be interpreted literally
			auto slabSize = toFloat(interval.c_str());

			if (!loadHeightField(fileName, fieldName, slabSize))
			{
				std::cerr << "Loading data: '" << fileName << "' failed." << std::endl;
			}
		}
	}

	assert(setLayoutStrategy(m_arguments.m_layoutName));
}

std::tuple<JCNTools::vtkActor_vptr, JCNTools::vtkActor_vptr, JCNTools::vtkSimplifyMetric_vptr>
JCNTools::SimplifyGraphPipeline(JCNTools::vtkDataObject_vptr const Port, /*ColourTable, dataRange,*/ JCNTools::vtkDataArray_vptr const mdrgList)
{
	ProcessTimer processTimer("Extracting reeb skeleton");

	//	If the mdrgList bit array has no values set (all equal to 0)
	//	it cannot be simplified
	auto mdrgMin = mdrgList->GetRange()[0];
	auto mdrgMax = mdrgList->GetRange()[1];
	if (mdrgMin != mdrgMax)
	{
		auto jcnsimplify = vtkSmartPointer<vtkSimplifyJCN>(vtkSimplifyJCN::New());
		jcnsimplify->SetInputData(Port);

		for (auto it = m_data.cbegin(); it != m_data.cend(); ++it)
		{
			auto nm = it->first.c_str();

			jcnsimplify->AddField(nm);
		}

		assert(vtkBitArray::SafeDownCast(mdrgList));
		jcnsimplify->AddJacobiStructure(vtkBitArray::SafeDownCast(mdrgList));
		jcnsimplify->Update();

		auto jcnMetric = vtkSimplifyMetric_vptr(vtkSimplifyMetric::New());
		for (auto it = m_data.cbegin(); it != m_data.cend(); ++it)
		{
			auto nm = it->first.c_str();
			auto stepSize = it->second.slabSize;

			jcnMetric->AddField(nm);
			jcnMetric->AddStepSize(stepSize);
		}

		jcnMetric->SetInputConnection(jcnsimplify->GetOutputPort());

		//	Add a bit more control over simplification
		jcnMetric->SetApplySimplify(m_arguments.simplify.active);
		jcnMetric->Setpersistence_measure_type(m_arguments.simplify.measure);
		jcnMetric->Setpersistence_threshold(m_arguments.simplify.threshold);

		jcnMetric->SetJCNList(jcnsimplify);
		jcnMetric->Update();

		auto func_print_reeb_skeleton_stats = [&](vtkGraph* graph)
		{
			//	The data we're really interested is attached to
			//	vertices of the Reeb skeleton
			auto vertex_data = graph->GetVertexData();

			//	Test this is actually the correct structure
			assert(vertex_data->HasArray("nodeType"));
			assert(vertex_data->HasArray("jcnList"));
			assert(vertex_data->HasArray("persistence"));
			assert(vertex_data->HasArray("volume"));

			//	Aliases to important arrays
			auto node_type_array = vertex_data->GetArray("nodeType");
			auto jcn_list_array = vertex_data->GetArray("jcnList");
			auto persistence_array = vertex_data->GetArray("persistence");
			auto volume_array = vertex_data->GetArray("volume");

			//	Print the details about each vertex
			cout << "Reeb skeleton data: " << endl;
			for (auto i = 0; i < graph->GetNumberOfVertices(); ++i)
			{
				cout << "\t" << "Vertex: " << i << endl;

				//	blue nodes are regular components where the topolology features at most trivial changes
				cout << "\t\t" << "Node type: " << (node_type_array->GetTuple(i)[0] == 0 ? "blue" : "red") << endl;

				//	number of vertices in the JCN of the current Reeb skeleton vertex
				cout << "\t\t" << "JCN vertex count: " << jcn_list_array->GetTuple(i)[0] << endl;

				//	'persistence' persistence measure of the current Reeb skeleton vertex
				cout << "\t\t" << "Persistence: " << persistence_array->GetTuple(i)[0] << endl;

				//	'volume' persistence measure of the current Reeb skeleton vertex
				cout << "\t\t" << "Volume: " << volume_array->GetTuple(i)[0] << endl;
			}
		};

		func_print_reeb_skeleton_stats(jcnMetric->GetOutput());
		//cout << jcnMetric->GetOutput() << endl;

		auto layout = vtkSmartPointer<vtkGraphLayout>(vtkGraphLayout::New());
		layout->SetLayoutStrategy(layout_strategy);
		layout->SetInputConnection(jcnMetric->GetOutputPort());
		layout->Update();

		auto merge = vtkSmartPointer<vtkJCNMergeFields>(vtkJCNMergeFields::New());
		merge->SetInputConnection(layout->GetOutputPort());

		//	Node pipeline : draw each node as a 3D glyph.
		//	Merge each of the scalar fields into a single
		//	field and assign it as the dataset scalars,
		//	where the billboard glyph filter will find it.
		merge->SetOutputField("merged fields", vtkJCNMergeFields::VERTEX_DATA);
		merge->SetNumberOfComponents(m_data.size());
		auto fnr = 0;
		for (auto it = m_data.cbegin(); it != m_data.cend(); ++it)
		{
			auto nm = it->first.c_str();

			merge->Merge(fnr, nm, 0);
			fnr += 1;
		}

		auto ass = vtkSmartPointer<vtkAssignAttribute>(vtkAssignAttribute::New());
		ass->SetInputConnection(merge->GetOutputPort());
		ass->Assign("merged fields", "SCALARS", "VERTEX_DATA");

		glyphsComponent->SetInputConnection(ass->GetOutputPort());
		glyphsComponent->SetScale(m_arguments.m_glyphScale);
		glyphsComponent->SetSmoothness(20);
		glyphsComponent->SetLogOn();

		auto colTable = vtkSmartPointer<vtkLookupTable>(vtkLookupTable::New());
		colTable->SetTableRange(0, 3);
		colTable->SetNumberOfColors(4);
		colTable->SetTableValue(0, 1, 1, 1, 1);
		colTable->SetTableValue(1, 1, 0, 0, 1);
		colTable->SetTableValue(2, 0, 0, 1, 1);
		colTable->SetTableValue(3, 0, 1, 0, 1);
		colTable->Build();

		auto apply = vtkSmartPointer<vtkApplyColors>(vtkApplyColors::New());
		apply->SetInputConnection(glyphsComponent->GetOutputPort());
		apply->SetCellLookupTable(colTable);
		apply->UseCellLookupTableOn();
		apply->ScaleCellLookupTableOff();
		apply->SetInputArrayToProcess(1, 0, 0, vtkDataObject::FIELD_ASSOCIATION_CELLS, "vtkBillboardMG values");

		auto nodeMap = vtkSmartPointer<vtkPolyDataMapper>(vtkPolyDataMapper::New());
		nodeMap->SetInputConnection(apply->GetOutputPort());
		nodeMap->SelectColorArray("vtkApplyColors color");
		nodeMap->SetScalarModeToUseCellFieldData();
		nodeMap->SetScalarVisibility(true);

		auto nodeActor = vtkSmartPointer<vtkActor>(vtkActor::New());
		nodeActor->SetMapper(nodeMap);
		nodeActor->GetProperty()->SetColor(1, 1, 1);
		nodeActor->GetProperty()->EdgeVisibilityOn();
		nodeActor->GetProperty()->SetEdgeColor(0, 0, 0);
		if (m_arguments.ShowBoundary)
		{
			nodeActor->GetProperty()->SetOpacity(0.5);
		}

		//	Edge pipeline :
		auto graphPD = vtkSmartPointer<vtkGraphToPolyData>(vtkGraphToPolyData::New());
		graphPD->SetInputConnection(layout->GetOutputPort());

		auto edgeMap = vtkSmartPointer<vtkPolyDataMapper>(vtkPolyDataMapper::New());
		edgeMap->SetInputConnection(graphPD->GetOutputPort());
		edgeMap->ScalarVisibilityOff();

		auto edgeActor = vtkSmartPointer<vtkActor>(vtkActor::New());
		edgeActor->SetMapper(edgeMap);
		edgeActor->GetProperty()->SetColor(0, 0, 0);
		edgeActor->GetProperty()->SetOpacity(0.8);

		//	Return graph actors.
		return std::make_tuple(nodeActor, edgeActor, jcnMetric);
	}
	else
	{
		throw std::invalid_argument("Unable to extract Reeb skeleton.");
	}
}

bool JCNTools::setLayoutStrategy(const std::string& val)
{
	if (!(val == "jcnlayout"))
	{
		auto strat = vtkSmartPointer<vtkOGDFLayoutStrategy>(vtkOGDFLayoutStrategy::New());
		strat->SetLayoutModuleByName(val.c_str());
		strat->LayoutEdgesOff();
		layout_strategy = strat;
	}
	else
	{
		auto strat = vtkSmartPointer<vtkJCNLayoutStrategy>(vtkJCNLayoutStrategy::New());
		layout_strategy = strat;
	}

	cout << "Updated layout strategy to: " << val << endl;

	return true;
}

vtkDataArray* JCNTools::JacobiSetComputation(vtkDataObject* const InputGraph)
{
	const char TAB = '\t';

	ProcessTimer processTimer("Computing Jacobi Set");

	auto myList = vtkIdTypeArray::New();
	//	 Multidimensional Reeb Graph
	auto mdrg = vtkSmartPointer<vtkMultiDimensionalReebGraph>(vtkMultiDimensionalReebGraph::New());
	mdrg->SetInputData(InputGraph);

	for (auto it = m_data.cbegin(); it != m_data.cend(); ++it)
	{
		auto name = it->first.c_str();
		mdrg->AddField(name);
	}

	mdrg->Update();
	auto jcnMdrgList = mdrg->GetOutput()->GetVertexData()->GetArray("MDRG");

	auto mdrgTree = mdrg->GetMultiDimensionalReebGraph();

	cout << "Multidimensional Reeb Graph: " << endl;
	cout << TAB << "vertex count: " << mdrgTree->GetNumberOfVertices() << endl;
	cout << TAB << "edge count: " << mdrgTree->GetNumberOfEdges() << endl;

	//  Extract each 3D reeb graph from the MDRG and output basic information about it
	auto extractReebGraph = vtkExtractReebGraph_vptr(vtkExtractReebGraph::New());
	for (auto it = m_data.cbegin(); it != m_data.cend(); ++it)
	{
		auto fieldName = it->first.c_str();

		extractReebGraph->SetFieldName(fieldName);
		extractReebGraph->SetInputConnection(m_jcn->GetOutputPort(0));
		extractReebGraph->Update();

		auto reebGraph = extractReebGraph->GetReebGraph();
		assert(reebGraph != nullptr);

		cout << "Reeb Graph (" << fieldName << "): " << endl;
		cout << TAB << "vertex count: " << reebGraph->GetNumberOfVertices() << endl;
		cout << TAB << "edge count: " << reebGraph->GetNumberOfEdges() << endl;
		cout << TAB << "loop count: " << "null" << endl;
	}
	auto outputTree = [&](vtkGraph* const tree)
	{
		ofstream outFile((m_arguments.m_outdir != "" ? m_arguments.m_outdir + "/" : "") +"mdrg.dot");

		assert(outFile.is_open() && outFile.good());

		stringstream headerStream;

		headerStream << "//\t" << "Multidimensional Reeb Graph" << endl << "//" << endl;
		headerStream << "//\t" << "Input fields (slabsize)" << endl;

		for (auto& field : m_data)
		{
			auto fname = field.first;
			auto slabSize = field.second.slabSize;

			headerStream << "//\t\t" << fname << '(' << slabSize << ')' << endl;
		}

		outFile << DotFormat(tree, headerStream.str()) << endl;

		outFile.close();
	};

	outputTree(mdrgTree);

	return jcnMdrgList;
}

/*
	Find the highest value across all data fields
*/
double JCNTools::globalMaxima() const
{
	assert(highs.size() == m_data.size());
	return *std::max_element(highs.cbegin(), highs.cend());
}

/*
	Find the lowest value across all data fields
*/
double JCNTools::globalMinima() const
{
	assert(lows.size() == m_data.size());
	return *std::min_element(lows.cbegin(), lows.cend());
}

std::tuple<JCNTools::vtkActor_vptr, JCNTools::vtkActor_vptr, JCNTools::vtkActor_vptr, JCNTools::vtkActor_vptr>
JCNTools::GraphPipeline(vtkAlgorithmOutput* const defaultPort, vtkDoubleArray* const dataRange)
{
	//cout << __PRETTY_FUNCTION__ << endl;
	auto computeColorTable1 = [&]()
	{
		//	Compute the full range of values for all fields
		assert(globalMinima() <= globalMaxima());

		//	Create a colour lookup table covering the full range.
		//	NOTE: in practice we would want a separate table for
		//	each range attribute in the data.This is a quick and
		//	dirty approach.
		auto col = vtkSmartPointer<vtkLookupTable>(vtkLookupTable::New());
		col->SetTableRange(globalMinima(), globalMaxima());
		col->SetNumberOfColors(200);
		col->SetHueRange(0.66667, 0.0);
		col->SetSaturationRange(1, 1);
		col->SetValueRange(1, 1);
		col->SetAlphaRange(1, 1);
		col->Build();

		return col;
	};

	auto ColourTable = computeColorTable1();

	//	Default pathway
	auto port = defaultPort;

	//if (m_arguments.PhysFilter)
	//{
	//	auto star = vtkScissionSimplification::New();
	//	star->SetInputConnection(port);

	//	for (auto it = m_data.cbegin(); it != m_data.cend(); ++it)
	//	{
	//		auto name = it->first.c_str();
	//		star->AddField(name);
	//	}

	//	port = star->GetOutputPort();
	//}

	auto doGraphLayoutFunc =
		[](vtkGraphLayoutStrategy* const layout_strategy, vtkAlgorithmOutput* const port) -> vtkSmartPointer<vtkGraphLayout>
	{
		ProcessTimer processTimer("Graph layout algorithm");

		auto layout = vtkSmartPointer<vtkGraphLayout>(vtkGraphLayout::New());
		layout->SetLayoutStrategy(layout_strategy);
		layout->SetInputConnection(port);
		layout->Update();

		return layout;
	};

	auto layout = doGraphLayoutFunc(layout_strategy, port);

	auto merge = vtkSmartPointer<vtkJCNMergeFields>(vtkJCNMergeFields::New());
	merge->SetInputConnection(layout->GetOutputPort());


	//	Node pipeline : draw each node as a 3D glyph.
	//	Merge each of the scalar fields into a single
	//	field and assign it as the dataset scalars,
	//	where the billboard glyph filter will find it.
	merge->SetOutputField("merged fields", merge->VERTEX_DATA);
	merge->SetNumberOfComponents(m_data.size());

	auto fnr = 0;

	for (auto it = m_data.cbegin(); it != m_data.cend(); ++it)
	{
		auto nm = it->first.c_str();
		merge->Merge(fnr, nm, 0);
		fnr += 1;
	}

	auto ass = vtkSmartPointer<vtkAssignAttribute>(vtkAssignAttribute::New());
	ass->SetInputConnection(merge->GetOutputPort());
	ass->Assign("merged fields", "SCALARS", "VERTEX_DATA");

	glyphs->SetInputConnection(ass->GetOutputPort());
	glyphs->SetScale(m_arguments.m_glyphScale);
	glyphs->SetSmoothness(20);
	glyphs->SetLogOn();

	//	Colour the node glyphs, and map to geometry
	auto app = vtkSmartPointer<vtkAssignColors>(vtkAssignColors::New());
	app->SetInputConnection(glyphs->GetOutputPort());
	app->SetCellLookupTable(ColourTable);
	app->SetTableRange(dataRange);
	app->UseCellLookupTableOn();

	//	Use the provided range table
	app->ScaleCellLookupTableOff();
	//	The first index is the data type index : 1 means vectors.
	app->SetInputArrayToProcess(1, 0, 0, vtkDataObject::FIELD_ASSOCIATION_CELLS, "vtkBillboardMG values");

	auto colTable = vtkSmartPointer<vtkLookupTable>(vtkLookupTable::New());
	colTable->SetTableRange(0, 3);
	colTable->SetNumberOfColors(4);
	colTable->SetTableValue(0, 1, 1, 1, 1);
	colTable->SetTableValue(1, 1, 0, 0, 1);
	colTable->SetTableValue(2, 0, 0, 1, 1);
	colTable->SetTableValue(3, 0, 1, 0, 1);
	colTable->Build();

	auto apply = vtkSmartPointer<vtkApplyColors>(vtkApplyColors::New());
	apply->SetInputConnection(glyphs->GetOutputPort());

	if (m_arguments.Colour)
	{
		apply->SetCellLookupTable(ColourTable);
	}
	else
	{
		apply->SetCellLookupTable(colTable);
	}

	apply->UseCellLookupTableOn();
	apply->ScaleCellLookupTableOff();
	apply->SetInputArrayToProcess(1, 0, 0, vtkDataObject::FIELD_ASSOCIATION_CELLS, "vtkBillboardMG values");

	auto nodeMap = vtkSmartPointer<vtkPolyDataMapper>(vtkPolyDataMapper::New());
	if (m_arguments.Colour)
	{
		nodeMap->SetInputConnection(app->GetOutputPort());
		nodeMap->SelectColorArray("vtkAssignColors color");
	}
	else if (m_arguments.MDRG)
	{
		nodeMap->SetInputConnection(apply->GetOutputPort());
		nodeMap->SelectColorArray("vtkApplyColors color");
	}
	else
	{
		nodeMap->SetInputConnection(glyphs->GetOutputPort());
	}

	nodeMap->SetScalarModeToUseCellFieldData();
	nodeMap->SetScalarVisibility(true);


	auto nodeActor = vtkSmartPointer<vtkActor>(vtkActor::New());
	nodeActor->SetMapper(nodeMap);
	nodeActor->GetProperty()->SetColor(1, 1, 1);
	nodeActor->GetProperty()->EdgeVisibilityOn();
	nodeActor->GetProperty()->SetEdgeColor(0, 0, 0);

	if (m_arguments.ShowBoundary)
	{
		nodeActor->GetProperty()->SetOpacity(0.5);
	}

	//	Edge pipeline :

	auto graphPD = vtkSmartPointer<vtkGraphToPolyData>(vtkGraphToPolyData::New());
	graphPD->SetInputConnection(layout->GetOutputPort());

	auto edgeMap = vtkSmartPointer<vtkPolyDataMapper>(vtkPolyDataMapper::New());
	edgeMap->SetInputConnection(graphPD->GetOutputPort());
	edgeMap->ScalarVisibilityOff();

	auto edgeActor = vtkSmartPointer<vtkActor>(vtkActor::New());
	edgeActor->SetMapper(edgeMap);
	edgeActor->GetProperty()->SetColor(0, 0, 0);
	//edgeActor->GetProperty()->SetLineWidth(5.0);

	if (m_arguments.ShowBoundary)
	{
		edgeActor->GetProperty()->SetOpacity(0.1);
	}
	else
	{
		edgeActor->GetProperty()->SetOpacity(m_arguments.lineOpacity);
	}

	auto sact = vtkSmartPointer<vtkActor>(vtkActor::New());
	auto edgeSact = vtkSmartPointer<vtkActor>(vtkActor::New());

	if (m_arguments.ShowBoundary)
	{
		auto vsel = vtkSmartPointer<vtkSelectionSource>(vtkSelectionSource::New());
		vsel->SetContentType(vtkSelectionNode::THRESHOLDS);
		vsel->SetFieldType(vtkSelectionNode::VERTEX);
		vsel->SetArrayName("Boundary");
		vsel->AddThreshold(1, 1);
		vsel->Update();

		auto vext = vtkSmartPointer<vtkExtractSelectedGraph>(vtkExtractSelectedGraph::New());
		vext->SetSelectionConnection(vsel->GetOutputPort());
		vext->SetInputConnection(merge->GetOutputPort());

		auto myGraphPD = vtkSmartPointer<vtkGraphToPolyData>(vtkGraphToPolyData::New());
		myGraphPD->SetInputConnection(vext->GetOutputPort());

		auto myEdgeMap = vtkSmartPointer<vtkPolyDataMapper>(vtkPolyDataMapper::New());
		myEdgeMap->SetInputConnection(myGraphPD->GetOutputPort());
		myEdgeMap->ScalarVisibilityOff();

		edgeSact->SetMapper(myEdgeMap);
		edgeSact->GetProperty()->SetColor(0, 0, 0);
		edgeSact->GetProperty()->SetOpacity(0.9);

		auto vxpd = vtkSmartPointer<vtkGraphToPolyData>(vtkGraphToPolyData::New());
		vxpd->SetInputConnection(vext->GetOutputPort());

		auto sph = vtkSmartPointer<vtkSphereSource>(vtkSphereSource::New());
		sph->SetRadius(10);

		auto glsel = vtkSmartPointer<vtkGlyph3D>(vtkGlyph3D::New());
		glsel->SetInputConnection(vxpd->GetOutputPort());
		glsel->SetSourceConnection(sph->GetOutputPort());
		glsel->ScalingOff();

		auto smap = vtkSmartPointer<vtkPolyDataMapper>(vtkPolyDataMapper::New());
		smap->SetInputConnection(glsel->GetOutputPort());
		
		sact->SetMapper(smap);
		sact->GetProperty()->SetColor(0, 0, 1);
		sact->GetProperty()->SetOpacity(0.9);
	}

	//	Return graph actors.
	return std::make_tuple(nodeActor, edgeActor, sact, edgeSact);
}

JCNTools::vtkActor_vptr JCNTools::GeometryPipeline(vtkDataObject* const jcnGraph, vtkAlgorithmOutput* const defaultPort)
{
	auto computeColourTable = [&]()
	{
		auto arr = vtkGraph::SafeDownCast(jcnGraph)->GetVertexData()->GetArray(m_data.cbegin()->first.c_str());
		auto lo = arr->GetRange()[0];
		auto hi = arr->GetRange()[1];

		auto colGeom = vtkSmartPointer<vtkLookupTable>(vtkLookupTable::New());
		colGeom->SetTableRange(lo, hi);
		colGeom->SetNumberOfColors(200);
		colGeom->SetHueRange(0.66667, 0.0);
		colGeom->SetSaturationRange(1, 1);
		colGeom->SetValueRange(1, 1);
		colGeom->SetAlphaRange(1, 1);
		colGeom->Build();

		return colGeom;
	};

	auto colourTable = computeColourTable();

	//	default pathway
	auto port = defaultPort;

	if (m_arguments.m_explodeFactor > 0)
	{
		auto exp = vtkSmartPointer<vtkExplodeGrid>(vtkExplodeGrid::New());
		exp->SetScaleFactor(m_arguments.m_explodeFactor);
		exp->SetInputConnection(port);
		port = exp->GetOutputPort();
	}

	//if (m_arguments.m_2d)
	//{
	//	auto geom = vtkGeometryFilter::New();
	//	geom->SetInputConnection(port);

	//	auto tri = vtkTriangleFilter::New();
	//	tri->SetInputConnection(geom->GetOutputPort());
	//	port = tri->GetOutputPort();
	//}
	//else
	//{
	auto geom = vtkSmartPointer<vtkPolyhedralMeshToPolyData>(vtkPolyhedralMeshToPolyData::New());
	geom->SetInputConnection(port);

	//	TODO: return this...
	port = geom->GetOutputPort();

	if (m_arguments.Cut)
	{
		auto plane = vtkSmartPointer<vtkPlane>(vtkPlane::New());
		plane->SetOrigin(0, 0, 0);
		plane->SetNormal(1, 1, 0);

		auto clip = vtkSmartPointer<vtkClipPolyData>(vtkClipPolyData::New());
		clip->SetInputConnection(geom->GetOutputPort());
		clip->SetClipFunction(plane);
		port = clip->GetOutputPort();

		//auto myCallback = [=](vtkImplicitPlaneWidget*& obj)
		//{
		//def myCallback(obj, event) :
		//	obj->GetPlane(plane);
		//};

		planeWidget->SetPlaceFactor(1.25);
		planeWidget->SetInputData(geom->GetOutput());
		planeWidget->PlaceWidget();
		//planeWidget->AddObserver("InteractionEvent", myCallback);
		planeWidget->SetOrigin(0, 0, 0);
		planeWidget->SetNormal(1, 1, 0);
		planeWidget->GetPlaneProperty()->SetOpacity(0.3);
	}
	//}

	//	Assign colour to the geometry.
	auto ass = vtkSmartPointer<vtkAssignAttribute>(vtkAssignAttribute::New());
	ass->SetInputConnection(port);
	if (m_arguments.ColourField != "")
	{
		ass->Assign(m_arguments.ColourField.c_str(), "SCALARS", "POINT_DATA");
	}
	else
	{
		auto it = m_data.cbegin();
		auto cf = it->first;
		ass->Assign(cf.c_str(), "SCALARS", "POINT_DATA");
	}

	auto mapper = vtkSmartPointer<vtkPolyDataMapper>(vtkPolyDataMapper::New());
	mapper->SetInputConnection(ass->GetOutputPort());
	mapper->SetColorModeToMapScalars();
	mapper->SetScalarModeToUsePointData();
	mapper->SetLookupTable(colourTable);
	mapper->ScalarVisibilityOn();
	mapper->UseLookupTableScalarRangeOn();

	auto actor = vtkSmartPointer<vtkActor>(vtkActor::New());
	actor->SetMapper(mapper);

	return actor;
}

vtkSmartPointer<JCNTools::Algorithm_Core> JCNTools::doJcnComputation() const
{
	
	//if (m_arguments.Naive)
	//{
	//
	//}
	//else
	//{
	//	assert(false);


		auto result = vtkSmartPointer<Algorithm_Core>::New();
#ifdef USE_VTK_WITH_GEOMETRY_FILTER
		cout << "Using JCNWithGeometry" << endl;
#else
		cout << "Using JointContourNet" << endl;

		//	We can define our own grid geomotry and topology
		//	here (see JCN_4d for an example)
		result->UseCellTopologyArrayOff();
		result->UseSpatialCoordinateArrayOff();
		result->IdentifyBoundarySlabsOn();
#endif	
		//}

	assert(result != nullptr);
	auto inputGridPort = generateSimplicateData()->GetOutputPort();
	assert(inputGridPort);

	

	result->SetInputConnection(inputGridPort);

	//	Defines if the topology is centred on the slabs
	//	Effectively, if we look at the extracted Reeb graphs
	//	if this is off we get a 'dual' of the Reeb graph
	if (m_arguments.CentreSlabs)
	{
		result->CenterFirstSlabOn();
	}
	else
	{
		result->CenterFirstSlabOff();
	}

	for (auto it = m_data.cbegin(); it != m_data.cend(); ++it)
	{
		auto name = it->first.c_str();
		auto stepSize = it->second.slabSize;

		result->AddField(name, stepSize);
	}

	result->Update();

	return result;
}

void JCNTools::doRangeComputation(JCNTools::vtkDataObject_vptr const& jcnGraph)
{
	//	Compute the value range of each field.
	auto arrs = vtkGraph::SafeDownCast(jcnGraph)->GetVertexData();
	assert(arrs);

	for (auto it = m_data.cbegin(); it != m_data.cend(); ++it)
	{
		auto name = it->first.c_str();
		auto arr = arrs->GetArray(name);
		double range[2];
		arr->GetRange(range);

		//cout << "range[0]: " << range[0] << ", range[1]: " << range[1] << endl;
		auto lo = range[0];
		auto hi = range[1];

		assert(lo <= hi);
		lows.push_back(lo);
		highs.push_back(hi);

		//cout << name << ": " << range[0] << "  " << range[1] << endl;
	}
};

///
///	\brief		Creates a concatenated string of the input fieldnames
///	\author		Dean
///	\since		04-03-2016
///
std::string JCNTools::generateJcnFilename() const
{
	const char DELIMITER = '_';
	std::stringstream result;

	for (auto it = m_data.cbegin(); it != m_data.cend(); ++it)
	{
		if (it != m_data.cbegin()) result << DELIMITER;
		
		//	Append fieldname
		auto fieldName = it->first;
		result << fieldName;
	}

	return result.str();
}


string JCNTools::generateJcnFile(JCNTools::vtkGraph_vptr const jcnGraph) const
{
	const char TAB = '\t';
	const char COMMENT = '#';

	auto jcnHeaderFunc = [=](const auto& jcnGraph, const auto& m_data)
	{
		//	We'll move this into a full function once the
		//	jcnFileDescriptor is implemented as a function object.
		stringstream header;

		auto dimX = m_data.cbegin()->second.dimX;
		auto dimY = m_data.cbegin()->second.dimY;
		auto dimZ = m_data.cbegin()->second.dimZ;

		header << COMMENT << TAB << "Joint Contour Net" << endl;
		header << COMMENT << TAB << "input fields ("
			<< dimX << ", " << dimY << ", " << dimZ << "): "
			<< endl;

		for (auto it = m_data.cbegin(); it != m_data.cend(); ++it)
		{
			auto fieldName = it->first;
			auto fieldProperties = it->second;

			header << COMMENT << TAB << TAB << fieldName
				<< " (" << fieldProperties.slabSize << ")" << endl;
		}

		header << COMMENT << TAB
			<< "Number of vertices: " << jcnGraph->GetNumberOfVertices()
			<< ", number of edges: " << jcnGraph->GetNumberOfEdges()
			<< ".";

		return header.str();
	};

	auto jcnVertexListing = [&](const auto& jcnGraph)
	{
		//	We'll move this into a full function once the
		//	jcnFileDescriptor is implemented as a function object.
		stringstream vertexListing;

		//	List the vertices first
		vertexListing << COMMENT << TAB << "Nodes" << endl;
		vertexListing << COMMENT << TAB << "=====" << endl;

		//	Obtain an iterator and traverse the list
		auto vertexIterator = vtkVertexListIterator_vptr(vtkVertexListIterator::New());
		jcnGraph->GetVertices(vertexIterator);
		assert(vertexIterator != nullptr);
		while (vertexIterator->HasNext())
		{
			//	Index of the vertex in the graph structure
			auto vertexId = vertexIterator->Next();

			//	Some details about the vertex
			//	Because the graph is undirected we should have no
			//	distinction between inward and outward vertices
			auto outDegree = jcnGraph->GetOutDegree(vertexId);
			auto inDegree = jcnGraph->GetInDegree(vertexId);
			auto degree = jcnGraph->GetDegree(vertexId);
			assert((outDegree == degree) && (inDegree == degree));

//			m_jcnGraph->

			//	Write a comment summarising the vertex (this could also be mined
			//	using a regex)
			vertexListing << COMMENT << TAB << "vertex = { id: " << vertexId << ", "
				<< "isovalues = { " << 0.0 << ", " << 0.0 << " }, "
				<< "degree: " << degree << " ("
				<< "inDegree: " << inDegree << ", "
				<< "outDegree: " << outDegree << ") }" << endl;

			//	Write the actual data for the vertex
			//os << vertexId << TAB;
		}

		return vertexListing.str();
	};

	stringstream result;

	//	Generate a human readable description of the data
	result << jcnHeaderFunc(jcnGraph, m_data);
	result << COMMENT << endl << jcnVertexListing(jcnGraph);

	return result.str();
}

///
/// \brief		Outputs the JCN as a undirected graph in DOT format
///	\author		Dean
///	\since		04-03-2016
///
void JCNTools::saveJointContourNetToDot(JCNTools::vtkGraph_vptr const jcnGraph,  const std::string& filename) const
{
	const std::string DOT_COMMENT = "//";
	const char TAB = '\t';

	auto outputPath = (m_arguments.m_outdir != "" ? m_arguments.m_outdir + "/" : "") + filename;
	if (outputPath.size() > 255)
	{
		cerr << "Warning filename is over 255 chars!" << endl;
	}

	ofstream outputFile(outputPath);
	if (outputFile.is_open())
	{
		outputFile << DOT_COMMENT << TAB << "Joint Contour Net" << endl;
		outputFile << DOT_COMMENT << TAB << "input fields:" << endl;
		for (auto it = m_data.cbegin(); it != m_data.cend(); ++it)
		{
			auto fieldName = it->first;
			outputFile << DOT_COMMENT << TAB << TAB << fieldName << endl;
		}

		//	operator<< requires a raw pointer (for now)
		outputFile << DotFormat(jcnGraph);
		outputFile.close();
	}
	else
	{
		throw std::invalid_argument("Could not open file: " + outputPath + " for writing");
	}
}

///	
///	\brief		Separate computation process from visualization process
///	\author		Dean
///	\since		04-03-2016
///
void JCNTools::computationPipeline()
{
	if (!m_data.empty())
	{
		{
			//	Use the stack to time this process explicitly
			ProcessTimer processTimer("Compute Joint Contour Net");

			m_jcn = doJcnComputation();
			assert(m_jcn != nullptr);
		}
		
		m_graphReductionAlgorithm = vtkReduceGraph_vptr(vtkReduceGraph::New());

		m_jcnGraph = vtkDataObject_vptr(vtkDataObject::New());
		m_jcnGrid = vtkDataObject_vptr(vtkDataObject::New());

		if (m_arguments.ExtractG)
		{
			m_graphReductionAlgorithm->SetInputConnection(0, m_jcn->GetOutputPort(0));
			m_graphReductionAlgorithm->SetInputConnection(1, m_jcn->GetOutputPort(2));
			m_graphReductionAlgorithm->Update();

			m_jcnGraph = m_graphReductionAlgorithm->GetOutput(0);
			m_jcnGrid = m_graphReductionAlgorithm->GetOutput(1);
		}
		else
		{
			//m_jcn->Update();
			m_jcnGraph = m_jcn->GetOutput(0);
			m_jcnGrid = m_jcn->GetOutput(2);
		}

		assert(m_jcnGraph && m_jcnGrid);
		assert(vtkGraph::SafeDownCast(m_jcnGraph));
		assert(vtkUnstructuredGrid::SafeDownCast(m_jcnGrid));


		//	Save the JCN for examination later
		if (m_arguments.jcnDotFilename != "")
		{
			try
			{
				ofstream outFile((m_arguments.m_outdir != "" ? m_arguments.m_outdir + "/" : "") + "joint_contour_net.jcn");

				outFile << generateJcnFile(vtkGraph::SafeDownCast(m_jcnGraph));

				outFile.close();

				saveJointContourNetToDot(vtkGraph::SafeDownCast(m_jcnGraph), m_arguments.jcnDotFilename);
			}
			catch (const std::invalid_argument& ex)
			{
				cerr << ex.what() << endl;
			}
		}


		m_mdrgList = JacobiSetComputation(m_jcnGraph);
		auto jacobiNr = 0;

		for (auto i = 0; i < m_mdrgList->GetNumberOfTuples(); ++i)
		{
			if (m_mdrgList->GetComponent(i, 0) == 1)
			{
				jacobiNr += 1;
			}
		}


		const char TAB = '\t';
		cout << "Joint Contour Net: " << endl;
		cout << TAB << "vertex count: " << vtkGraph::SafeDownCast(m_jcnGraph)->GetNumberOfVertices() << endl;
		cout << TAB << "edge count: " << vtkGraph::SafeDownCast(m_jcnGraph)->GetNumberOfEdges() << endl;
		cout << TAB << "jacobi number: " << jacobiNr << " mdrgList." << endl;


		Objects["v"] = vtkGraph::SafeDownCast(m_jcnGraph)->GetNumberOfVertices();
		Objects["e"] = vtkGraph::SafeDownCast(m_jcnGraph)->GetNumberOfEdges();

		//	Compute the minima and maxima of each field
		doRangeComputation(m_jcnGraph);

		//auto ranges = rangeComputation();
		//auto lows = std::get<0>(ranges);
		//auto highs = std::get<1>(ranges);
		/*
		ofstream debugFile(DESKTOP + "debug\\" + "debug_boundary.txt");
		debugFile << "Boundary: " << m_jcn->GetBoundary() << endl << endl;
		debugFile.close();

		debugFile.open(DESKTOP + "debug\\" + "debug_fragments.txt");
		debugFile << "Fragments: " << m_jcn->GetFragments() << endl << endl;
		debugFile.close();

		debugFile.open(DESKTOP + "debug\\" + "debug_jcn.txt");
		debugFile << "Topology (jcn): " << m_jcn->GetTopologyGraph() << endl << endl;
		debugFile.close();
		*/
		//auto col = computeColorTable1();
		//auto colGeom = computeColourTable2();

		if (m_arguments.reebDot || m_arguments.reebRb || m_arguments.reebPng)
		{
			saveReebGraphs(m_arguments.reebDot, m_arguments.reebRb, m_arguments.reebPng);
		}

//		ofstream jcnDebug(DESKTOP + "debug\\" + "debug_vtkJCNwithGeometry.txt");
//		jcnDebug << m_jcn.GetPointer() << endl;
//		jcnDebug.close();

		//	Dump some extra data about the JCN to help understand the internal
		//	structures used in the algorithm
		auto slabs = vtkUnstructuredGrid::SafeDownCast(m_jcn->GetOutputDataObject(2));
		assert(slabs != nullptr);
		output_slab_data(slabs);

		auto frags = vtkUnstructuredGrid::SafeDownCast(m_jcn->GetOutputDataObject(1));
		assert(frags != nullptr);
		output_frag_data(frags);
	}
	else throw std::length_error("No data is present for the JCN computation!");
}

///
///	\brief		Output the JCN boundary structure in various forms
///	\since		12-04-2016
///	\author		Dean
///
void JCNTools::output_slab_data(vtkUnstructuredGrid* const boundary) const
{
	auto boundaryObjWriter = [&](vtkUnstructuredGrid* const boundary, const size_t boundMax = 0)
	{
		cout << "Writing slab obj files" << endl;

		ProcessTimer pt("writing_slab_obj_files.");
		size_t cellCount = static_cast<size_t>(boundary->GetNumberOfCells());
		size_t last_slab = cellCount;

		//	If a maximum has been specified, use this as the limit (clamped to the actual
		//	number of cells)
		if (boundMax > 0) last_slab = std::min(boundMax, cellCount);
		assert(last_slab <= cellCount);



		for (auto slab = 0ul; slab < last_slab; ++slab)
		{
//			cout << i << " of " << lastCell << " (" << (100.0f * ((float)i / (float)lastCell)) << "%)." << endl;
			ofstream debugFile(m_arguments.m_outdir + "/slab_" + to_string(slab) + ".obj");
			debugFile << ObjFormat(boundary->GetCell(slab), true) << endl;
			debugFile.close();
		}
	};


	auto boundaryTxtWriter = [&](vtkUnstructuredGrid* const boundary, const size_t boundaryMax = 0)
	{
		cout << "Writing slab stats" << endl;
		ProcessTimer pt("writing_slab_stats.");
		size_t cellCount = static_cast<size_t>(boundary->GetNumberOfCells());
		size_t last_slab = cellCount;

		//	If a maximum has been specified, use this as the limit (clamped to the actual
		//	number of cells)
		if (boundaryMax > 0) last_slab = std::min(boundaryMax, cellCount);
		assert(last_slab <= cellCount);

		auto global_polygon_count = 0ul;
		auto global_triangle_count = 0ul;
		auto global_surface_area = 0.0f;
		stringstream slab_data;

		for (auto slab = 0ul; slab < last_slab; ++slab)
		{
			auto mesh_stats =
					count_triangles_and_polygons_in_slab(boundary->GetCell(slab));
			auto mesh_polygon_count = std::get<0>(mesh_stats);
			auto mesh_triangle_count = std::get<1>(mesh_stats);
			auto mesh_surface_area = calculate_surface_area(boundary->GetCell(slab));

			slab_data << "\tSlab: " << slab << "\tPolygons: " << mesh_polygon_count
									<< "\tTriangles: " << mesh_triangle_count
									<< "\tSurface Area: " << mesh_surface_area
									<< endl;

			global_polygon_count += mesh_polygon_count;
			global_triangle_count += mesh_triangle_count;
			global_surface_area += mesh_surface_area;
		}

		ofstream debugFile(m_arguments.m_outdir + "/slab_stats.txt");

		//  Global stats
		debugFile << "Total number of slabs: " << cellCount << endl << endl;
		debugFile << "Total number of polygons: " << global_polygon_count << endl;
		debugFile << "Total number of triangles: " << global_triangle_count << endl << endl;
		debugFile << "Total surface area: " << global_surface_area << endl << endl;

		if (cellCount > 0)
		{
			debugFile << "Average number of polygons per slab: "
					  << (static_cast<float>(global_polygon_count) / static_cast<float>(cellCount)) << endl;
			debugFile << "Average number of triangles per slab: "
					  << (static_cast<float>(global_triangle_count) / static_cast<float>(cellCount)) << endl;
			debugFile << "Average surface area per slab: "
					  << (static_cast<float>(global_surface_area) / static_cast<float>(cellCount)) << endl << endl;
		}

		//  Stats for each slab
		debugFile << slab_data.str();

		debugFile.close();
	};

	if (m_arguments.slab_obj)
		boundaryObjWriter(boundary);
	if (m_arguments.slab_stats)
		boundaryTxtWriter(boundary);
}

///
///	\brief		Output the JCN fragment structure in various forms
///	\since		12-04-2016
///	\author		Dean
///
void JCNTools::output_frag_data(vtkUnstructuredGrid* const fragments) const
{
	auto fragObjWriter = [&](vtkUnstructuredGrid* const fragments, const size_t fragMax = 0)
	{
		cout << "Writing fragment obj files" << endl;
		ProcessTimer pt("writing_fragment_obj_files.");
		size_t cellCount = static_cast<size_t>(fragments->GetNumberOfCells());
		size_t lastCell = cellCount;

		//	If a maximum has been specified, use this as the limit (clamped to the actual
		//	number of cells)
		if (fragMax > 0) lastCell = std::min(fragMax, cellCount);
		assert(lastCell <= cellCount);

		for (auto i = 0; i < lastCell; ++i)
		{
//			//if (i > fragMax) return;
//			cout << i << " of " << lastCell << " (" << ((float)i / (float)lastCell) << "%)." << endl;
			ofstream debugFile(m_arguments.m_outdir + "/frag_" + to_string(i) + ".obj");
			debugFile << ObjFormat(fragments->GetCell(i), true) << endl;
			debugFile.close();
		}
	};

	auto fragTxtWriter = [&](vtkUnstructuredGrid* const fragments, const size_t fragMax = 0)
	{
		auto count_triangles_in_fragment = [&](vtkCell* const fragment)
		{
			vtkCell* const cell = fragment;
			assert(cell != nullptr);
			assert(cell->RequiresExplicitFaceRepresentation());

			//  Calculate the number of triangles and polygons
			auto triangle_count = 0ul;
			auto polygon_count = cell->GetNumberOfFaces();

			for (auto face = 0ul; face < cell->GetNumberOfFaces(); ++face)
			{
				//	Convert to a polygon for triangulation
				auto polygon = vtkPolygon::SafeDownCast(cell->GetFace(face));
				assert(polygon != nullptr);

				//	Split the polygon into a list of id's defining the triangulation
				auto triangle_ids = vtkSmartPointer<vtkIdList>::New();
				auto result = polygon->NonDegenerateTriangulate(triangle_ids);
				assert(result == 1);

				triangle_count += triangle_ids->GetNumberOfIds() / 3;
			}

			return make_tuple(polygon_count, triangle_count);
		};

		cout << "Writing fragment stats" << endl;
		ProcessTimer pt("writing_fragment_stats.");
		size_t cellCount = static_cast<size_t>(fragments->GetNumberOfCells());
		size_t last_frag = cellCount;

		//	If a maximum has been specified, use this as the limit (clamped to the actual
		//	number of cells)
		if (fragMax > 0) last_frag = std::min(fragMax, cellCount);
		assert(last_frag <= cellCount);

		auto global_polygon_count = 0ul;
		auto global_triangle_count = 0ul;
		stringstream frag_data;

		for (auto frag = 0ul; frag < last_frag; ++frag)
		{
			auto mesh_stats = count_triangles_in_fragment(fragments->GetCell(frag));
			auto mesh_polygon_count = std::get<0>(mesh_stats);
			auto mesh_triangle_count = std::get<1>(mesh_stats);

			frag_data << "\tFrag: " << frag << "\tPolygons: " << mesh_polygon_count
									<< "\tTriangles: " << mesh_triangle_count
									<< endl;

			global_polygon_count += mesh_polygon_count;
			global_triangle_count += mesh_triangle_count;
		}

		ofstream debugFile(m_arguments.m_outdir + "/frag_stats.txt");

		//  Global stats
		debugFile << "Total number of fragments: " << cellCount << endl << endl;
		debugFile << "Total number of polygons: " << global_polygon_count << endl;
		debugFile << "Total number of triangles: " << global_triangle_count << endl << endl;
		if (cellCount > 0)
		{
			debugFile << "Average number of polygons per fragment: "
					  << (static_cast<float>(global_polygon_count) / static_cast<float>(cellCount)) << endl;
			debugFile << "Average number of triangles per fragment: "
					  << (static_cast<float>(global_triangle_count) / static_cast<float>(cellCount)) << endl << endl;
		}

		//  Stats for each fragment
		debugFile << frag_data.str();

		debugFile.close();
	};

	if (m_arguments.frag_obj)
		fragObjWriter(fragments);
	if (m_arguments.frag_stats)
		fragTxtWriter(fragments);
}

///	
///	\brief		Separate computation process from visualization process
///	\author		Dean
///	\since		04-03-2016
///
std::tuple<JCNTools::vtkRenderWindow_vptr, JCNTools::vtkRenderWindowInteractor_vptr> 
JCNTools::visualizationPipeline()
{
	assert(m_jcnGraph != nullptr);
	assert(m_jcnGrid != nullptr);

	//	Data Range for each field
	auto dim = m_data.size();
	auto dataRange = vtkDoubleArray_vptr(vtkDoubleArray::New());
	dataRange->SetNumberOfComponents(2);
	dataRange->SetNumberOfTuples(dim);

	for (auto i = 0; i < dim; ++i)
	{
		dataRange->SetComponent(i, 0, lows[i]);
		dataRange->SetComponent(i, 1, highs[i]);
	}

	auto componentGraph = vtkRenderer_vptr(vtkRenderer::New());
	auto renwin = vtkRenderWindow_vptr(vtkRenderWindow::New());
	auto graph = vtkRenderer_vptr(vtkRenderer::New());
	auto slabs = vtkRenderer_vptr(vtkRenderer::New());

	vtkActor_vptr nodeA = nullptr;
	vtkActor_vptr edgeA = nullptr;
	vtkActor_vptr selA = nullptr;
	vtkActor_vptr selEdgeA = nullptr;

	if (m_arguments.ExtractG)
	{
		assert(m_graphReductionAlgorithm != nullptr);

		auto actors = GraphPipeline(m_graphReductionAlgorithm->GetOutputPort(0), dataRange);
		nodeA = std::get<0>(actors);
		edgeA = std::get<1>(actors);
		selA = std::get<2>(actors);
		selEdgeA = std::get<3>(actors);
	}
	else
	{
		//	Create a pipeline for generating a graph.
		auto actors = GraphPipeline(m_jcn->GetOutputPort(0), dataRange);
		nodeA = std::get<0>(actors);
		edgeA = std::get<1>(actors);
		selA = std::get<2>(actors);
		selEdgeA = std::get<3>(actors);
	}

	vtkActor_vptr cNode = nullptr;
	vtkActor_vptr cEdge = nullptr;
	vtkSimplifyMetric_vptr mGraph = nullptr;

	if (m_arguments.m_reebSkeleton)
	{
		try
		{
			assert(m_mdrgList != nullptr);

			auto res = SimplifyGraphPipeline(m_jcnGraph, /*col, dataRange,*/ m_mdrgList);
			cNode = std::get<0>(res);
			cEdge = std::get<1>(res);
			mGraph = std::get<2>(res);
		}
		catch (const std::invalid_argument& ex)
		{
			cerr << ex.what() << endl;
			m_arguments.m_reebSkeleton = false;
		}
	}

	if (m_arguments.m_withGeometry)
	{
		renwin->SetSize(2000, 2000);

		if (m_arguments.m_reebSkeleton)
		{
			componentGraph->SetViewport(0, 0, 0.5, 0.5);
			graph->SetViewport(0, 0.5, 0.5, 1);
			slabs->SetViewport(0.5, 0, 1, 1);
		}
		else
		{
			graph->SetViewport(0, 0, 0.5, 1);
			slabs->SetViewport(0.5, 0, 1, 1);
		}

		graph->SetBackground(1, 1, 1);
		slabs->SetBackground(1, 1, 1);
		componentGraph->SetBackground(1, 1, 1);

		vtkActor_vptr sact = nullptr;


		if (m_arguments.ExtractG)
		{
			assert(m_graphReductionAlgorithm != nullptr);
			sact = GeometryPipeline(m_jcnGraph, m_graphReductionAlgorithm->GetOutputPort(1));
		}
		else
		{
			sact = GeometryPipeline(m_jcnGraph, m_jcn->GetOutputPort(2));
		}

		assert(sact);
		slabs->AddActor(sact);
		graph->AddActor(edgeA);
		graph->AddActor(nodeA);


		if (m_arguments.ShowBoundary)
		{
			graph->AddActor(selA);
			graph->AddActor(selEdgeA);
		}

		glyphs->SetCamera(graph->GetActiveCamera());

		if (m_arguments.m_reebSkeleton)
		{
			assert(cEdge && cNode);
			componentGraph->AddActor(cEdge);
			componentGraph->AddActor(cNode);
			glyphsComponent->SetCamera(componentGraph->GetActiveCamera());
			renwin->AddRenderer(componentGraph);
		}

		renwin->AddRenderer(graph);
		renwin->AddRenderer(slabs);
	}
	else
	{
		renwin->SetSize(m_arguments.jcnOutputSize.width, m_arguments.jcnOutputSize.height);
		graph->SetViewport(0, 0, 1, 1);
		graph->SetBackground(1, 1, 1);
		graph->AddActor(edgeA);
		graph->AddActor(nodeA);
		if (m_arguments.ShowBoundary)
		{
			graph->AddActor(selA);
		}
		graph->AddActor(selEdgeA);
		glyphs->SetCamera(graph->GetActiveCamera());

		renwin->AddRenderer(graph);

	}

	/*
	if (m_arguments.Smooth)
	{
		renwin->PolygonSmoothingOn();
		renwin->LineSmoothingOn();
		renwin->PointSmoothingOn();
		renwin->SetAAFrames(renwin->GetAAFrames() * 4);
	}
	*/

	auto iren = vtkRenderWindowInteractor_vptr(vtkRenderWindowInteractor::New());
	iren->SetRenderWindow(renwin);


	//	Interaction setup
	if (m_arguments.m_withGeometry)
	{
		assert(m_mdrgList != nullptr);

		auto style = vtkJCNMouseInteractorStyle_vptr(vtkJCNMouseInteractorStyle::New());
		style->SetGraph(vtkGraph::SafeDownCast(m_jcnGraph));
		style->SetGrid(vtkUnstructuredGrid::SafeDownCast(m_jcnGrid));
		//style->SetTwoD(false);
		style->SetFieldNr(m_data.size());
		style->SetGlyphScale(m_arguments.m_glyphScale);
		style->SetJacobiSet(m_mdrgList);

		if (m_arguments.m_reebSkeleton)
		{
			assert(mGraph);
			style->SetMetric(mGraph);
			style->SetRenderRS(true);
		}
		for (auto it = m_data.cbegin(); it != m_data.cend(); ++it)
		{
			auto name = it->first.c_str();
			style->AddField(name);
		}
		style->Initialize();
		iren->SetInteractorStyle(style);
	}

	if (m_arguments.Cut)
	{
		planeWidget->SetInteractor(iren);
	}

	//	Prepare to start!
	graph->ResetCamera();
	slabs->ResetCamera();
	componentGraph->ResetCamera();
	iren->Initialize();


	if (m_arguments.jcn_png_filename != "")
	{
		saveImages(renwin);
	}
	return std::make_tuple(renwin, iren);
}

///	\author		Dean
///	\since		26-02-2016
void JCNTools::saveImages(JCNTools::vtkRenderWindow_vptr const renwin) const
{
	auto capture = vtkWindowToImageFilter_vptr(vtkWindowToImageFilter::New());
	capture->SetInput(renwin);
	capture->SetInputBufferTypeToRGBA(); //also record the alpha (transparency) channel
	capture->ReadFrontBufferOff(); // read from the back buffer
	//	Take a screen capture
	capture->Modified();
	capture->Update();

	auto png = vtkPNGWriter_vptr(vtkPNGWriter::New());
	png->SetInputConnection(capture->GetOutputPort());

	//	Create path to file
	std::string filename = ((m_arguments.m_outdir != "" ? m_arguments.m_outdir + "/" : "") + m_arguments.jcn_png_filename);
	if (filename.size() > 255)
	{
		cerr << "Warning filename is over 255 chars!" << endl;
	}

	//	Save to file
	png->SetFileName(filename.c_str());
	png->Update();
	png->Write();

	auto pdf = vtkSmartPointer<vtkGL2PSExporter>::New();
	pdf->SetRenderWindow(renwin);
	pdf->SetFileFormatToPDF();
	pdf->CompressOff();
	pdf->SetFilePrefix(((m_arguments.m_outdir != "" ? m_arguments.m_outdir + "/" : "") + "jcn").c_str());
	pdf->Update();
	pdf->Write();

	cout << "Output saved to file: " << png->GetFileName() << "." << endl;
}

///	\author		Dean
///	\since		26-02-2016
void JCNTools::saveReebGraphs(const bool dot, const bool rb, const bool png) const
{
	ProcessTimer processTimer("Save Reeb Graphs");

	auto extractReebGraph = vtkExtractReebGraph_vptr(vtkExtractReebGraph::New());

	for (auto it = m_data.cbegin(); it != m_data.cend(); ++it)
	{
		auto fieldName = it->first.c_str();
		auto intervals = it->second.intervals;
		auto floatField = it->second.data;
		auto slabSize = it->second.slabSize;

		//  dims of scalar input field
		auto dim_x = it->second.dimX;
		auto dim_y = it->second.dimY;
		auto dim_z = it->second.dimZ;

		//  isovalue range
		auto iso_min = it->second.min;
		auto iso_max = it->second.max;

		extractReebGraph->SetFieldName(fieldName);
		extractReebGraph->SetInputConnection(m_jcn->GetOutputPort(0));
		extractReebGraph->Update();

		auto reebGraph = extractReebGraph->GetReebGraph();
		assert(reebGraph != nullptr);

		auto criticalNodeList = extractReebGraph->GetCriticalNodes();
		auto expectedCriticalNodeCount = criticalNodeList->GetNumberOfTuples();

		auto jcnGraph = vtkGraph::SafeDownCast(m_jcnGraph);
		assert(jcnGraph != nullptr);
		auto vd = jcnGraph->GetVertexData();
		assert(vd != nullptr);
		cout << "Vertex data has array " << fieldName << ": " << (vd->HasArray(fieldName) ? "true" : "false") << endl;
		auto scalarData = vd->GetScalars(fieldName);
		assert(scalarData != nullptr);
		cout << "Number of tuples in scalar array: " << scalarData->GetNumberOfTuples() << endl;

		auto listScalars = [](vtkDataArray* const scalarData, vtkIdTypeArray* const criticalNodeList)
		{
			cout << "Critical node list number of tuples: " << criticalNodeList->GetNumberOfTuples() << endl;
			for (auto i = 0; i < scalarData->GetNumberOfTuples(); ++i)
			{

				//floatField->LookupValue(0);

				auto x1 = i;
				auto x2 = scalarData->GetTuple1(i);
				cout << x1 << '\t' << x2 << endl;
			}
		};
		//listScalars(scalarData, criticalNodeList);

		//for (auto i = 0; i < criticalNodeList->GetNumberOfTuples(); ++i)
		//{
		//	std::cout << criticalNodeList->GetTuple(i) << std::endl;
		//}


		std::stringstream outfile;
		outfile << fieldName;

		if (dot || png)
		{
			cout << "Saving dot file to: " << m_arguments.m_outdir << "." << endl;

			//	If saving to png we need to create a file for the DOT command to use
			//	save to a user specified output directory or the current directory if none is specified
			std::string dotFilename = ((m_arguments.m_outdir != "" ? m_arguments.m_outdir + "/" : "") + outfile.str() + ".dot");
			std::string dotNormFilename = ((m_arguments.m_outdir != "" ? m_arguments.m_outdir + "/" : "") + outfile.str() + "_norm.dot");

			if ((dotFilename.size() > 255) || (dotNormFilename.size() > 255))
			{
				cerr << "Warning filename is over 255 chars!" << endl;
			}


			//	Simple topology
			stringstream header;
			header << "//\t" << "Reeb graph" << endl;
			header << "//\t" << fieldName << '(' << slabSize << ')' << endl;
			ofstream os(dotFilename);
			assert(os.is_open());
			os << DotFormat(reebGraph, header.str());
			os.close();

			//	Normalized to height
			//ofstream os2(dotNormFilename);
			//os2 << ToNormalisedHeightDOT(reebGraph, m_data.cbegin()->second.data, fieldName, intervals);
			//os2.close();
		}

		if (png)
		{
			stringstream dotCmd;
			stringstream dotCmd2;
			std::string dotFilename = ((m_arguments.m_outdir != "" ? m_arguments.m_outdir + "/" : "") + outfile.str() + ".dot");
			std::string dotNormFilename = ((m_arguments.m_outdir != "" ? m_arguments.m_outdir + "/" : "") + outfile.str() + "_norm.dot");
			std::string pngFilename = ((m_arguments.m_outdir != "" ? m_arguments.m_outdir + "/" : "") + outfile.str() + ".png");
			std::string pngNormFilename = ((m_arguments.m_outdir != "" ? m_arguments.m_outdir + "/" : "") + outfile.str() + "_norm.png");

			//	Construct and execute DOT command for simple reeb graph
			dotCmd << "dot";
			dotCmd << " -Tpng ";
			dotCmd << "\"" << dotFilename << "\"";
			dotCmd << " -o ";
			dotCmd << "\"" << pngFilename << "\"";
			cout << "Command line: " << dotCmd.str() << endl;
			cout << "Executing DOT command." << endl;
			system(dotCmd.str().c_str());
			cout << "Done." << endl;

			//	Construct and execute DOT command for normalised reeb graph
			dotCmd2 << "dot";
			dotCmd2 << " -Tpng ";
			dotCmd2 << "\"" << dotNormFilename << "\"";
			dotCmd2 << " -o ";
			dotCmd2 << "\"" << pngNormFilename << "\"";
			cout << "Command line: " << dotCmd2.str() << endl;
			cout << "Executing DOT command." << endl;
			system(dotCmd2.str().c_str());
			cout << "Done." << endl;
		}

		if (rb)
		{
			//	Creates an array of non-normalised isovalues so that they can be added to
			//	the reeb graph
			auto gen_value_array = [&](vtkReebGraph* reeb_graph,
					vtkFloatArray* volumeScalarField,
					const string& field_name) -> vtkSmartPointer<vtkDoubleArray>
			{
				auto value_array = vtkSmartPointer<vtkDoubleArray>::New();
				value_array->SetNumberOfComponents(1);
				value_array->SetName("Value");

				for (auto i = 0ul; i < reeb_graph->GetVertexData()->GetNumberOfArrays(); ++i)
				{
					cout << "\t" << reeb_graph->GetVertexData()->GetAbstractArray(i)->GetName() << endl;
				}

				auto v_ids = reeb_graph->GetVertexData()->GetAbstractArray(field_name.c_str());
				for (auto i = 0ul; i < v_ids->GetNumberOfTuples(); ++i)
				{
					auto tuple_id = v_ids->GetVariantValue(i);
					auto value = *volumeScalarField->GetTuple(tuple_id.ToInt());

					value_array->InsertNextTuple1(value);
					//cout << endl << endl;
				}
				return value_array;
			};

			//	Creates an array of normalised isovalues so that they can be added to
			//	the reeb graph
			auto gen_normalised_value_array = [&](vtkReebGraph* reeb_graph,
					vtkFloatArray* volumeScalarField,
					const float imin, const float imax,
					const string& field_name) -> vtkSmartPointer<vtkDoubleArray>
			{
				//	Function for normalising values
				auto norm = [&](const float& in) -> double
				{
					//auto min = heightField.GetMinHeight();
					//auto max = heightField.GetMaxHeight();
					cout << "min: " << imin << " max: " << imax << endl;
					assert(imin <= imax);

					if (isinf(imin)) return 0.0;

					auto result = (in - imin) / (imax - imin);
					//cout << in << " -> " << result << endl;
					assert((result >= 0.0) && (result <= 1.0));

					return result;
				};

				auto norm_array = vtkSmartPointer<vtkDoubleArray>::New();
				norm_array->SetNumberOfComponents(1);
				norm_array->SetName("Normalised value");

				//auto v_ids = reeb_graph->GetVertexData()->GetAbstractArray("Vertex Ids");
				auto v_ids = reeb_graph->GetVertexData()->GetAbstractArray(field_name.c_str());
				for (auto i = 0ul; i < v_ids->GetNumberOfTuples(); ++i)
				{
					auto tuple_id = v_ids->GetVariantValue(i);
					auto value = *volumeScalarField->GetTuple(tuple_id.ToInt());

					norm_array->InsertNextTuple1(norm(value));
					//cout << endl << endl;
				}
				return norm_array;
			};

			//	This method looks up the samples relating to a critical
			//	vertices in the reeb graph and translates it to the
			//	3D position in the scalar data
			auto gen_critical_vertex_pos_array = [&](vtkReebGraph* reeb_graph,
					vtkFloatArray* volumeScalarField,
					const size_t& x_dim,
					const size_t& y_dim,
					const size_t& z_dim,
					const string& field_name)
			{
				//	Used to map the 1D index to a 3D vector
				using vec3 = tuple<size_t, size_t, size_t>;
				auto to_vec3 = [&](const size_t index) -> vec3
				{
					size_t pos = index;
					//auto x_dim = heightField.GetBaseDimX();
					//auto y_dim = heightField.GetBaseDimY();
					//auto z_dim = heightField.GetBaseDimZ();

					auto z = pos % z_dim;
					pos = (pos - z) / z_dim;
					auto y = pos % y_dim;
					auto x = (pos - y) / y_dim;

					return make_tuple(x, y, z);
				};

				//	Create a vector of 3 doubles to represent coordinates
				auto pos_array = vtkSmartPointer<vtkDoubleArray>::New();
				pos_array->SetNumberOfComponents(3);
				pos_array->SetName("Position");

				//	Loop over the existing list
				//auto v_ids = reeb_graph->GetVertexData()->GetAbstractArray("Vertex Ids");
				auto v_ids = reeb_graph->GetVertexData()->GetAbstractArray(field_name.c_str());
				for (auto i = 0ul; i < v_ids->GetNumberOfTuples(); ++i)
				{
					//	Look up the corresponding sample for the vertex
					auto tuple_id = v_ids->GetVariantValue(i);
					auto value = *volumeScalarField->GetTuple(tuple_id.ToInt());

					//	Translate the sample index to a 3D coordinate
					auto pos = to_vec3(tuple_id.ToInt());
					//cout << value << " - (" << get<0>(pos) << " " << get<1>(pos) << " " << get<2>(pos) << ") " << endl;

					//	Unpack the result and add to the array
					pos_array->InsertNextTuple3(get<0>(pos), get<1>(pos), get<2>(pos));
					//cout << endl << endl;
				}
				return pos_array;
			};

			auto vertexCountBefore = reebGraph->GetNumberOfVertices();
			auto edgeCountBefore = reebGraph->GetNumberOfEdges();

			cout << "Reducing " << fieldName << " reeb graph...";
			do
			{
				cout << ".";
			} while (reductionStep(reebGraph));
			cout << "done." << endl;

			auto vertexCountAfter = reebGraph->GetNumberOfVertices();
			auto edgeCountAfter = reebGraph->GetNumberOfEdges();

			cout << "'" << fieldName
				<< "' reeb graph before reduction = { "
				<< "vertex count: " << vertexCountBefore << ", "
				<< "edge count: " << edgeCountBefore << " }." << endl;
			cout << "'" << fieldName
				<< "' reeb graph after reduction = { "
				<< "vertex count: " << vertexCountAfter << ", "
				<< "edge count: " << edgeCountAfter << " }." << endl;
			cout << "'" << fieldName << "' reeb graph reduction removed "
				<< (vertexCountBefore - vertexCountAfter) << " vertices and "
				<< (edgeCountBefore - edgeCountAfter) << " edges." << endl;

			assert(expectedCriticalNodeCount == vertexCountAfter);

			auto rbFilename = (m_arguments.m_outdir != "" ? m_arguments.m_outdir + "/" : "") + fieldName + ".rb";
			cout << "Writing reeb graph file '" << rbFilename << "'" << std::endl;

			auto simpleReebGraph = convertToReebGraph(reebGraph, fieldName, floatField);
			RbWriter rbWriter(simpleReebGraph);
			auto result = rbWriter(rbFilename);
			std::cout << "Writing reeb graph file '" << rbFilename << "': " << (result ? "ok" : "failed") << std::endl;
			/*
			 * Stick with old method for now...
			 *
			//	Generate the non-normalised array and add to the Reeb graph
			auto values = gen_value_array(reebGraph, floatField, fieldName);
			reebGraph->GetVertexData()->AddArray(values);

			//	Generate the normalised array and add to the Reeb graph
			auto normalised_values = gen_normalised_value_array(reebGraph, floatField, iso_min, iso_max, fieldName);
			reebGraph->GetVertexData()->AddArray(normalised_values);

			//	Generate the positional array and add to the Reeb graph
			auto positional_values = gen_critical_vertex_pos_array(reebGraph, floatField, dim_x, dim_y, dim_z, fieldName);
			reebGraph->GetVertexData()->AddArray(positional_values);

			std::ofstream rb_file(rbFilename);
			assert(rb_file.is_open());
			rb_file << RbFormat(reebGraph) << std::endl;
			rb_file.close();
			*/

			cout << "done." << std::endl;
		}
	}
}
