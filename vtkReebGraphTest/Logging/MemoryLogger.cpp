#include "MemoryLogger.h"
#include <cassert>
#include <sstream>
#include <algorithm>

using namespace std;

///
///	\brief		Default constructor.
///	\author		Dean
///	\since		12-05-2016
///
MemoryLogger::MemoryLogger()
    : MemoryLogger("untitled process") { }

///
///	\brief		Automatically write to the log stream on construction and store
///				the initial state for later.
///	\author		Dean
///	\since		12-05-2016
///
MemoryLogger::MemoryLogger(const std::string& processName)
    : m_processName { processName }
{
    m_pid = getPid();

    try
    {
        m_outStream << "Process '" << m_processName << "' initial memory stats: " << endl;

        //	Access current process stats
        m_initalMemoryState = getMemoryUsageData();

        //	Mark that the reading was sucessful
        m_receivedInitialStats = true;

        //	Output to log
        logMemoryUsage(m_initalMemoryState);
    }
    catch (exception& ex)
    {
        cerr << ex.what() << endl;
    }
}

///
///	\brief		Returns the process id (eventually) in a non OS specific way.
///	\author		Dean
///	\since		13-05-2016
///
size_t MemoryLogger::getPid() const
{
#ifdef WINDOWS
    return GetCurrentProcessId();
#endif

#ifdef LINUX
    return static_cast<size_t>(getpid());
#endif
}

///
///	\brief		Automatically write to the log stream on destruction with the final
///				stats, also calculate the memory usage delta.
///	\author		Dean
///	\since		13-05-2016
///
MemoryLogger::~MemoryLogger()
{
    try
    {
        //	Receive most recent stats
        auto currentPmc = getMemoryUsageData();

        m_outStream << "Process '" << m_processName << "' final memory stats: " << endl;

        //	Output current status to log
        logMemoryUsage(currentPmc);

        //	If we can, compute the delta over the lifetime
        if (m_receivedInitialStats)
        {
            auto deltaPmc = currentPmc - m_initalMemoryState;

            m_outStream << "Process '" << m_processName << "' memory stats delta: " << endl;

            logMemoryUsage(deltaPmc);
        }
    }
    catch (exception& ex)
    {
        cerr << ex.what() << endl;
    }
}

#ifdef WINDOWS

///
///	\brief		Computes the difference between two sets of process memory stats
///	\author		Dean
///	\since		13-05-2016
///
PROCESS_MEMORY_COUNTERS operator-(const PROCESS_MEMORY_COUNTERS& lhs, const PROCESS_MEMORY_COUNTERS& rhs)
{
    PROCESS_MEMORY_COUNTERS result;

    //assert(lhs.cb == rhs.cb);
    result.cb = lhs.cb;

    //assert(lhs.PageFaultCount >= rhs.PageFaultCount);
    result.PageFaultCount = lhs.PageFaultCount - rhs.PageFaultCount;

    //assert(lhs.PeakWorkingSetSize >= rhs.PeakWorkingSetSize);
    result.PeakWorkingSetSize = lhs.PeakWorkingSetSize - rhs.PeakWorkingSetSize;

    //assert(lhs.WorkingSetSize >= rhs.WorkingSetSize);
    result.WorkingSetSize = lhs.WorkingSetSize - rhs.WorkingSetSize;

    //assert(lhs.QuotaPeakPagedPoolUsage >= rhs.QuotaPeakPagedPoolUsage);
    result.QuotaPeakPagedPoolUsage = lhs.QuotaPeakPagedPoolUsage - rhs.QuotaPeakPagedPoolUsage;

    //assert(lhs.QuotaPagedPoolUsage >= rhs.QuotaPagedPoolUsage);
    result.QuotaPagedPoolUsage = lhs.QuotaPagedPoolUsage - rhs.QuotaPagedPoolUsage;

    //assert(lhs.QuotaPeakNonPagedPoolUsage >= rhs.QuotaPeakNonPagedPoolUsage);
    result.QuotaPeakNonPagedPoolUsage = lhs.QuotaPeakNonPagedPoolUsage - rhs.QuotaPeakNonPagedPoolUsage;

    //assert(lhs.QuotaNonPagedPoolUsage >= rhs.QuotaNonPagedPoolUsage);
    result.QuotaNonPagedPoolUsage = lhs.QuotaNonPagedPoolUsage - rhs.QuotaNonPagedPoolUsage;

    //assert(lhs.PagefileUsage >= rhs.PagefileUsage);
    result.PagefileUsage = lhs.PagefileUsage - rhs.PagefileUsage;

    //assert(lhs.PeakPagefileUsage >= rhs.PeakPagefileUsage);
    result.PeakPagefileUsage = lhs.PeakPagefileUsage - rhs.PeakPagefileUsage;

    return result;
}

///
///	\brief		Writes the current memory stats to the log stream
///	\author		Dean
///	\since		13-05-2016
///
void MemoryLogger::logMemoryUsage(const PROCESS_MEMORY_COUNTERS& pmc) const
{
    const char TAB = '\t';

    clog << TAB << "Process id: " << m_pid << endl;

    auto formatMemUsage = [](const size_t bytes) -> std::string
    {
        auto toKb = [](const size_t bytes) -> float
        {
            return static_cast<float>(bytes) / 1024.0f;
        };

        auto toMb = [](const size_t bytes) -> float
        {
            return static_cast<float>(bytes) / 1024.0f / 1024.0f;
        };

        auto toGb = [](const size_t bytes) -> float
        {
            return static_cast<float>(bytes) / 1024.0f / 1024.0f / 1024.0f;
        };

        string b = to_string(bytes) + "B";
        string kb = to_string(toKb(bytes)) + "kB";
        string mb = to_string(toMb(bytes)) + "MB";
        string gb = to_string(toGb(bytes)) + "GB";

        return b + " ( " + kb + ", " + mb + ", " + gb + " )";
    };

    clog << TAB << TAB << "PageFaultCount: " << pmc.PageFaultCount << endl;
    clog << TAB << TAB << "PeakWorkingSetSize: " << formatMemUsage(pmc.PeakWorkingSetSize) << endl;
    clog << TAB << TAB << "WorkingSetSize: " << formatMemUsage(pmc.WorkingSetSize) << endl;
    clog << TAB << TAB << "QuotaPeakPagedPoolUsage: " << formatMemUsage(pmc.QuotaPeakPagedPoolUsage) << endl;
    clog << TAB << TAB << "QuotaPagedPoolUsage: " << formatMemUsage(pmc.QuotaPagedPoolUsage) << endl;
    clog << TAB << TAB << "QuotaPeakNonPagedPoolUsage: " << formatMemUsage(pmc.QuotaPeakNonPagedPoolUsage) << endl;
    clog << TAB << TAB << "QuotaNonPagedPoolUsage: " << formatMemUsage(pmc.QuotaNonPagedPoolUsage) << endl;
    clog << TAB << TAB << "PagefileUsage: " << formatMemUsage(pmc.PagefileUsage) << endl;
    clog << TAB << TAB << "PeakPagefileUsage: " << formatMemUsage(pmc.PeakPagefileUsage) << endl;
}

///
///	\brief		Returns the current memory usage data for the active process
///	\author		Dean
///	\since		13-05-2016
//	See	https://msdn.microsoft.com/en-gb/library/windows/desktop/ms682050%28v=vs.85%29.aspx
//	[10-05-2016]
//
PROCESS_MEMORY_COUNTERS MemoryLogger::getMemoryUsageData() const
{
    constexpr char TAB = '\t';

    HANDLE hProcess;
    PROCESS_MEMORY_COUNTERS pmc;
    DWORD aProcesses[1024], cbNeeded, cProcesses;
    unsigned int i;

    //	Enumerate all of the currently runnig processes
    if (!EnumProcesses(aProcesses, sizeof(aProcesses), &cbNeeded))
        throw std::exception("Unable to enumerate processes");

    // Calculate how many process identifiers were returned.
    cProcesses = cbNeeded / sizeof(DWORD);

    //	Loop through each process
    for (i = 0; i < cProcesses; i++)
    {
        //	Find a match for our process id
        if (aProcesses[i] == m_pid)
        {
            //	Try to open the process
            hProcess = OpenProcess(PROCESS_QUERY_INFORMATION |
                                   PROCESS_VM_READ,
                                   FALSE, m_pid);
            if (NULL == hProcess)
                throw std::exception("Unable to open process");

            //	Try to read the data
            if (!GetProcessMemoryInfo(hProcess, &pmc, sizeof(pmc)))
                throw std::exception("Unable to access process memory information");

            //	Close the open process handle
            CloseHandle(hProcess);

            //	And return the result
            return pmc;
        }
    }

    throw std::exception("Was unable to find current process in open processes");

}
#endif

#ifdef LINUX

///
///	\brief		Computes the difference between two sets of process memory stats
///	\author		Dean
///	\since		07-06-2016
///
MemoryLogger::MemStats operator-(const MemoryLogger::MemStats& lhs, const MemoryLogger::MemStats& rhs)
{
    MemoryLogger::MemStats result;

    result.currentResidentSetSize = lhs.currentResidentSetSize - rhs.currentResidentSetSize;
    result.peakResidentSetSize = lhs.peakResidentSetSize - rhs.peakResidentSetSize;

    return result;
}

///l
///	\brief		Writes the current memory stats to the log stream
///	\author		Dean
///	\since		08-06-2016
///
void MemoryLogger::logMemoryUsage(const MemoryLogger::MemStats& pmc) const
{
    const char TAB = '\t';

    clog << TAB << "Process id: " << m_pid << endl;

    auto formatMemUsage = [](const size_t bytes) -> std::string
    {
        auto toKb = [](const size_t bytes) -> float
        {
            return static_cast<float>(bytes) / 1024.0f;
        };

        auto toMb = [](const size_t bytes) -> float
        {
            return static_cast<float>(bytes) / 1024.0f / 1024.0f;
        };

        auto toGb = [](const size_t bytes) -> float
        {
            return static_cast<float>(bytes) / 1024.0f / 1024.0f / 1024.0f;
        };

        string b = to_string(bytes) + "B";
        string kb = to_string(toKb(bytes)) + "kB";
        string mb = to_string(toMb(bytes)) + "MB";
        string gb = to_string(toGb(bytes)) + "GB";

        return b + " ( " + kb + ", " + mb + ", " + gb + " )";
    };

    //clog << TAB << TAB << "PageFaultCount: " << pmc.PageFaultCount << endl;
    clog << TAB << TAB << "PeakWorkingSetSize: " << formatMemUsage(pmc.peakResidentSetSize) << endl;
    clog << TAB << TAB << "WorkingSetSize: " << formatMemUsage(pmc.currentResidentSetSize) << endl;
    //clog << TAB << TAB << "QuotaPeakPagedPoolUsage: " << formatMemUsage(pmc.QuotaPeakPagedPoolUsage) << endl;
    //clog << TAB << TAB << "QuotaPagedPoolUsage: " << formatMemUsage(pmc.QuotaPagedPoolUsage) << endl;
    //clog << TAB << TAB << "QuotaPeakNonPagedPoolUsage: " << formatMemUsage(pmc.QuotaPeakNonPagedPoolUsage) << endl;
    //clog << TAB << TAB << "QuotaNonPagedPoolUsage: " << formatMemUsage(pmc.QuotaNonPagedPoolUsage) << endl;
    //clog << TAB << TAB << "PagefileUsage: " << formatMemUsage(pmc.PagefileUsage) << endl;
    //clog << TAB << TAB << "PeakPagefileUsage: " << formatMemUsage(pmc.PeakPagefileUsage) << endl;
}

///
///	\brief		Returns the current memory usage data for the active process
///	\author		Dean
///	\since		08-06-2016
// See http://en.wikichip.org/wiki/resident_set_size [08-06-2016]
//
MemoryLogger::MemStats MemoryLogger::getMemoryUsageData() const
{
    MemStats result;

    auto getStatus = [](const size_t pid, const std::string& field) -> size_t
    {
        //  This uses code adapted from http://en.wikichip.org/wiki/resident_set_size
        //  [08-06-2016] to get information from the process status file
        //
        const std::string CAT_CMD = "/bin/cat";
        const std::string GREP_CMD = "/bin/grep";

        std::string cmd = CAT_CMD + " /proc/" + std::to_string(pid) + "/status | " + GREP_CMD + " " + field;

        FILE *stream = popen(cmd.c_str(), "r");
        assert(stream != NULL);

        char buffer[256];
        assert(fgets(buffer, sizeof(buffer), stream) != NULL);

        //std::cout << std::string(buffer) << std::endl;
        pclose(stream);

        std::string fieldName;
        size_t value;
        std::string units;

        //  Split the line into the field, value and units
        std::stringstream(std::string(buffer)) >> fieldName >> value >> units;
        std::transform(units.begin(), units.end(), units.begin(), ::toupper);

        //std::cout << fieldName << "\t" << value << "\t" << units << std::endl;

        //  Convert to bytes
        if (units == "B") return value;
        if (units == "KB") return value * 1024;
        if (units == "MB") return value * 1024 * 1024;
        if (units == "GB") return value * 1024 * 1024 * 1024;

        //  Missed a conversion
        assert(false);
    };

    auto getPeakRSS = []()
    {
        //  This uses code adapted from http://en.wikichip.org/wiki/resident_set_size
        //  [08-06-2016] to get information from the process status file
        //
        struct rusage rusage;
        assert(!getrusage(RUSAGE_SELF, &rusage));

        //cout << "Peak RSS " << (size_t)rusage.ru_maxrss;

        //  kB -> B
        return (size_t)rusage.ru_maxrss * 1024;
    };

    result.currentResidentSetSize = getStatus(m_pid, "VmRSS");
    result.peakResidentSetSize = getPeakRSS();

    return result;
}

#endif
