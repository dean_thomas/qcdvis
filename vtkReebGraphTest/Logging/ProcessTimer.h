///
///		Timer class that uses RAII principles to automate the process of
///		timing using scoped variables.
///
#pragma once
#ifndef PROCESS_TIMER_H
#define PROCESS_TIMER_H

#include <chrono>
#include <string>
#include <iostream>

class ProcessTimer
{
public:
	using ClockType = std::chrono::system_clock;
	using TimePointType = std::chrono::time_point<ClockType>;
	using DurationType = std::chrono::nanoseconds;
	
private:
	TimePointType m_startTimePoint;
	std::string m_processName;
	std::ostream& m_outStream = std::clog;

	std::string toHumanReadableUnits(const DurationType& duration) const;
public:
	ProcessTimer();
	ProcessTimer(const std::string& processName);
	~ProcessTimer();
};

#endif