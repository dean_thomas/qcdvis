#ifndef MEMORYLOGGER_H
#define MEMORYLOGGER_H

#include <string>
#include <iostream>

#if (defined(_WIN32) || defined(_WIN64))
#define WINDOWS
#include <Windows.h>
#include <Psapi.h>
#else
#define LINUX
#include "sys/types.h"
#include "sys/sysinfo.h"
#include <sys/resource.h>
#include <unistd.h>
#include <cstdio>
#endif

class MemoryLogger
{
private:
	std::string m_processName;
	std::ostream& m_outStream = std::clog;
	size_t m_pid;

	bool m_receivedInitialStats = false;

#ifdef WINDOWS
	PROCESS_MEMORY_COUNTERS m_initalMemoryState;
	PROCESS_MEMORY_COUNTERS getMemoryUsageData() const;
	void logMemoryUsage(const PROCESS_MEMORY_COUNTERS& pmc) const;    
#endif
#ifdef LINUX
public:
    struct MemStats
    {
        size_t currentResidentSetSize;
        size_t peakResidentSetSize;
    };

    MemStats m_initalMemoryState;
    MemStats getMemoryUsageData() const;
    void logMemoryUsage(const MemStats& pmc) const;
#endif

	size_t getPid() const;
public:
	MemoryLogger();
	MemoryLogger(const std::string& processName);
	~MemoryLogger();
};

#ifdef WINDOWS
PROCESS_MEMORY_COUNTERS operator-(const PROCESS_MEMORY_COUNTERS& lhs, const PROCESS_MEMORY_COUNTERS& rhs);
#endif

#ifdef LINUX
MemoryLogger::MemStats operator-(const MemoryLogger::MemStats& lhs, const MemoryLogger::MemStats& rhs);
#endif

#endif
