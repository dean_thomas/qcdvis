#pragma once
#ifndef LOGREDIRECT_H
#define LOGREDIRECT_H

//	Uses source code adapted from:
//	http://stackoverflow.com/questions/10150468/how-to-redirect-cin-and-cout-to-file
//	[10-05-2016]
//
#include <iostream>
#include <fstream>
#include <string>

class LogRedirect
{
private:
	std::streambuf* m_coutbuf = nullptr;
	std::ofstream m_coutFile;

	std::streambuf* m_cerrbuf = nullptr;
	std::ofstream m_cerrFile;
	
	std::streambuf* m_clogbuf = nullptr;
	std::ofstream m_clogFile;

public:

	LogRedirect()
	{
	}

	//LogRedirect& operator=(const LogRedirect& rhs) = default;
	LogRedirect(const LogRedirect& rhs) = default;

	explicit LogRedirect(const std::string& coutFilename, const std::string& cerrFilename = "", const std::string& clogFilename = "")
	{
		redirectCoutToFile(coutFilename);

		if (cerrFilename != "")
		{
			redirectCerrToFile(cerrFilename);
		}

		if (clogFilename != "")
		{
			redirectClogToFile(clogFilename);
		}
	}

	~LogRedirect()
	{
		std::cout << "Closing log files" << std::endl;

		//	Close open files
		if (m_coutFile.is_open()) m_coutFile.close();
		if (m_cerrFile.is_open()) m_cerrFile.close();
		if (m_clogFile.is_open()) m_clogFile.close();

		//	Restore buffers
		if (m_coutbuf != nullptr) std::cout.rdbuf(m_coutbuf);
		if (m_cerrbuf != nullptr) std::cerr.rdbuf(m_cerrbuf);
		if (m_clogbuf != nullptr) std::clog.rdbuf(m_clogbuf);
	}

	bool redirectCoutToFile(const std::string& filename)
	{
		m_coutFile = std::ofstream(filename);
		
		if (m_coutFile.is_open() && m_coutFile.good())
		{
			//	Store current buffer
			m_coutbuf = std::cout.rdbuf();

			//	Redirect to the file
			std::cout.rdbuf(m_coutFile.rdbuf());

			return true;
		}
		else return false;
	}

	bool redirectCerrToFile(const std::string& filename)
	{
		m_cerrFile = std::ofstream(filename);

		if (m_cerrFile.is_open() && m_cerrFile.good())
		{
			//	Store current buffer
			m_cerrbuf = std::cerr.rdbuf();

			//	Redirect to the file
			std::cerr.rdbuf(m_cerrFile.rdbuf());

			return true;
		}
		else return false;
	}

	bool redirectClogToFile(const std::string& filename)
	{
		m_clogFile = std::ofstream(filename);

		if (m_clogFile.is_open() && m_clogFile.good())
		{
			//	Store current buffer
			m_clogbuf = std::clog.rdbuf();

			//	Redirect to the file
			std::clog.rdbuf(m_clogFile.rdbuf());

			return true;
		}
		else return false;
	}
};

#endif