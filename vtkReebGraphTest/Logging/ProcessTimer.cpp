#include "ProcessTimer.h"

#include <iostream>
#include <iomanip>

using namespace std;

using Seconds = std::chrono::seconds;
using Minutes = std::chrono::minutes;
using Hours = std::chrono::hours;
using Milliseconds = std::chrono::milliseconds;

///
///	\brief		Outputs the duration of the process in a human readable form.
///	\author		Dean
///	\since		03-03-2016
///
std::string ProcessTimer::toHumanReadableUnits(const DurationType& duration) const
{
	const auto SECS_PER_MINUTE = 60;
	const auto MINS_PER_HOUR = 60;

	//	Less than 1 minute
	auto seconds = chrono::duration_cast<Seconds>(duration).count();
	if (seconds < SECS_PER_MINUTE) return (to_string(seconds) + " seconds");

	//	Less than 1 hour
	auto minutes = chrono::duration_cast<Minutes>(duration).count();
	if (minutes < MINS_PER_HOUR)
	{
		seconds -= (minutes * SECS_PER_MINUTE);
		return (to_string(minutes) + " minutes, " 
				+ to_string(seconds) + " seconds");
	}

	//	Show in hours
	auto hours = chrono::duration_cast<Hours>(duration).count();
	minutes -= (hours * MINS_PER_HOUR);
	seconds -= ((hours * MINS_PER_HOUR) + (minutes * SECS_PER_MINUTE));
	return (to_string(hours) + " hours," 
			+ to_string(minutes) + " minutes, " 
			+ to_string(seconds) + " seconds");
}

///
///	\brief		Default constructor.
///	\author		Dean
///	\since		03-03-2016
///
ProcessTimer::ProcessTimer()
	: ProcessTimer("untitled process") { }

///
///	\brief		Automatically write to the log stream on construction.
///	\author		Dean
///	\since		03-03-2016
///
ProcessTimer::ProcessTimer(const std::string& processName)
	: m_processName{ processName }
{
	//	Measure current time and print to screen
	m_startTimePoint = chrono::system_clock::now();
	auto startTime = chrono::system_clock::to_time_t(m_startTimePoint);

	m_outStream << "Process '" << m_processName << "' started at: " 
		<< std::put_time(std::localtime(&startTime), "%F %T") 
		<< "." << endl;
}

///
///	\brief		Automatically write to the logging stream when the calling
///				function exits.
///	\author		Dean
///	\since		03-03-2016
///
ProcessTimer::~ProcessTimer()
{
	//	Measure current time and print to screen
	auto endTimePoint = chrono::system_clock::now();
	auto endTime = chrono::system_clock::to_time_t(endTimePoint);
	auto duration = endTimePoint - m_startTimePoint;

	m_outStream << "Process '" << m_processName << "' ended at: "
		<< std::put_time(std::localtime(&endTime), "%F %T") << endl;
	m_outStream << "Process '" << m_processName << "' duration: " 
		<< toHumanReadableUnits(duration)
		<< " (" << chrono::duration_cast<Milliseconds>(duration).count() 
		<< "  milliseconds)." 
		<< endl;
}
