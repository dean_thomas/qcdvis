/*=========================================================================

 *	File: vtkPolyhedraMeshToPolyData.cxx
 *	Graph visualization library for VTK
 * 
 *	This software is distributed WITHOUT ANY WARRANTY; 
 *	without even the implied warranty of MERCHANTABILITY 
 *	or FITNESS FOR A PARTICULAR PURPOSE.  
 * 
 *	See the file copyright.txt for details.  

=========================================================================*/
#include "vtkCell.h"
#include "vtkCellArray.h"
#include "vtkCellData.h"
#include "vtkDoubleArray.h"
#include "vtkIdList.h"
#include "vtkIdTypeArray.h"
#include "vtkInformation.h"
#include "vtkInformationVector.h"
#include "vtkGenericCell.h"
#include "vtkMergePoints.h"
#include "vtkMath.h"
#include "vtkObjectFactory.h"
#include "vtkPolyhedralMeshToPolyData.h"
#include "vtkPolyhedron.h"
#include "vtkPointData.h"
#include "vtkSmartPointer.h"
#include "vtkUnsignedCharArray.h"
#include "vtkUnstructuredGrid.h"


vtkStandardNewMacro(vtkPolyhedralMeshToPolyData);


//----------------------------------------------------------------------------
vtkPolyhedralMeshToPolyData::vtkPolyhedralMeshToPolyData()
{
  this->GenerateCellIds = false;
}

//----------------------------------------------------------------------------
vtkPolyhedralMeshToPolyData::~vtkPolyhedralMeshToPolyData()
{
}

//----------------------------------------------------------------------------
void vtkPolyhedralMeshToPolyData::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os,indent);
}

//----------------------------------------------------------------------------
int vtkPolyhedralMeshToPolyData::FillInputPortInformation(int, vtkInformation* info)
{
  // This filter uses the vtkDataSet cell traversal methods so it
  // suppors any data set type as input.
  info->Set(vtkAlgorithm::INPUT_REQUIRED_DATA_TYPE(), "vtkUnstructuredGrid");
  return 1;
}

//----------------------------------------------------------------------------
int vtkPolyhedralMeshToPolyData::RequestData(vtkInformation*,
                                 vtkInformationVector** inputVector,
                                 vtkInformationVector* outputVector)
{
  // Get input and output data.
  // get the info objects
  vtkInformation *inInfo = inputVector[0]->GetInformationObject(0);
  vtkInformation *outInfo = outputVector->GetInformationObject(0);

  // get the input and output
  vtkUnstructuredGrid *input = vtkUnstructuredGrid::SafeDownCast(
    inInfo->Get(vtkDataObject::DATA_OBJECT()));
  vtkPolyData *output = vtkPolyData::SafeDownCast(
    outInfo->Get(vtkDataObject::DATA_OBJECT()));

  vtkPoints *points = input->GetPoints();

  vtkIdType numCells = input->GetNumberOfCells();
  vtkIdType numPts   = input->GetNumberOfPoints();
  
  vtkPointData *inPD = input->GetPointData(), *outPD = output->GetPointData();
  vtkCellData  *inCD = input->GetCellData(),  *outCD = output->GetCellData();

  // Skip execution if there is no input geometry.
  if(numCells < 1 || numPts < 1)
    {
    vtkErrorMacro("No input data.");
    return 1;
    }

  vtkSmartPointer<vtkIdTypeArray> cellIds = NULL;

  if (this->GenerateCellIds)
    {
    cellIds = vtkSmartPointer<vtkIdTypeArray>::New();
    cellIds->SetName("polyhedral cell id");
    // estimate initial size
    cellIds->SetNumberOfValues(numCells*4);
    }
    
  outPD->ShallowCopy(inPD);
  outCD->CopyAllocate(inCD, numCells*5, numCells/2);  

  output->Allocate(numCells * 2);  // Estimate of output size.
  
  vtkIdType nrFaces;
  vtkSmartPointer<vtkIdList> pids = vtkSmartPointer<vtkIdList>::New();
  vtkSmartPointer<vtkIdList> poly = vtkSmartPointer<vtkIdList>::New();

  vtkIdType pnr, cid;
  int nrPoints;

  for (vtkIdType c = 0; c < numCells; c++)
    {    
    pnr = 0;

    // Get the face stream for next cell.
    // Face stream = |nr_faces|nr_pnts_face_0|..face 0 points|nr_pnts_face_1|...   |face_n-1_points|
    input->GetFaceStream(c, pids);
    nrFaces = pids->GetId(pnr++);

    // For each face in the face stream.
    for (vtkIdType i = 0; i < nrFaces; i++)
      {
      // Get the points, and create a polygon in the output dataset.
      nrPoints = pids->GetId(pnr++);
      poly->Reset();
      for (vtkIdType j = 0; j < nrPoints; j++)
        {
        poly->InsertNextId(pids->GetId(pnr++));
        }

      cid = output->InsertNextCell(VTK_POLYGON, poly); 

      // Copy cell data from input POLYHEDRON to output POLYGON.
      outCD->CopyData(inCD, c, cid);
      if (this->GenerateCellIds)
        {
        cellIds->InsertValue(cid, c);
        }
      }
    }

   output->SetPoints(points);
   if (this->GenerateCellIds)
     {
     output->GetCellData()->AddArray(cellIds);
     }

  // Avoid keeping extra memory around.
  output->Squeeze();
  return 1;
}


