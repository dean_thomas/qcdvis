/*=========================================================================

 *	File: vtkJCNMouseInteractorStyle.h
 *	Graph visualization library for VTK
 *
 *	This software is distributed WITHOUT ANY WARRANTY;
 *	without even the implied warranty of MERCHANTABILITY
 *	or FITNESS FOR A PARTICULAR PURPOSE.
 *
 *	See the file copyright.txt for details.

=========================================================================*/
// .NAME vtkJCNMouseInteractorStyle - Provides interaction options between JCN graph, Reeb 
//  Skeleton graph and Slab geometry.
// .SECTION Description
//  This filter takes the input of few components, including JCN graph, JCN slabs and 
//  Reeb Skeleton Graph.  At the moment, the user option can be handled by
//  keyboard events and mouse events. The keyboard events contain most of the interaction option. 
//  The mouse event is mainly used for brushing.  The user could freely brush any subset of the 
//  graph. If they want to the see the corresponding slab geometry of the brushed graph nodes, 
//  press 'g' to render the geometry. 
//
// r: Enter/exit selection mode. To select node, press "r" first and drag the mouse. If in 
//    selection mode, press "r" again to exit. 
// c: Turn On/off color mapping of the JCN
// b: Only display the JCN nodes touching the boundary in Domain
// v: Only display the nodes which are part of Jacobi Structure in Reeb space
// Up/Down: Increase/Decrease glyph size of the JCN graph.
// Right/Left: Increase/Decrease the graph edge opacity.
// z/x : Increase/Decrease the glyph size of Reeb Skeleton graph.
// o: If currently a subgraph is rendered, then revert back to display all of the graph nodes. 
// s: Render a subgraph only containing the selected JCN nodes.
// p: Only display the interior JCN nodes. These slabs do not touch the domain boundary. 
// i: Only display the interior JCN nodes in Jacobi Structure. These slabs do not touch 
//   the domain boundary. 
// f: Apply Force-directed layout on JCN graph. 
// j: Apply Domain-specific layout on JCN graph. 
// g: Render slab geometry corresponding to the current selected JCN graph nodes. 
// d: Switch between data fields. When switching different fields, the colour mapping of 
//   slab will be changed accordingly.  
// y/t: Filter the selected graph nodes based on its size. The size is measured by the number 
//     of fragments.  Press "y" to increase the threshold and "t" to decrease. 
// n/m : Increase / Decrease the clipping plane. 
// a: Decide along which axis the clipping plane starts. 
// k/l : Increase / Decrease the threshold of scalar fields values for filtering process. 
// u : Change the type of filtering.  Such as And or Or filtering. 
// ";" : Change the direction where the clipping plane starts. 

// The Mouse Events include: 
// Left Button Release :  To select nodes, first press "r" to enter selection node. 
//                       Then press the left mouse button and drag the ruberband to 
//                       cover the area to be selected.  
// Right Button Release:  Cancel all of the selection and initialise the data structure. 

#include "vtkActor.h"
#include "vtkActor.h"
#include "vtkAssignAttribute.h"
#include "vtkApplyColors.h"
#include "vtkAreaPicker.h"
#include "vtkAssignColors.h"
#include "vtkBitArray.h"
#include "vtkBillboardMultivarGlyphs.h"
#include "vtkCamera.h"
#include "vtkCommand.h"
#include "vtkCellData.h"
#include "vtkCoordinate.h"
#include "vtkCubeAxesActor.h"
#include "vtkCallbackCommand.h"
#include "vtkClipPolyData.h"
#include "vtkCell.h"
#include "vtkDataSetMapper.h"
#include "vtkDataObject.h"
#include "vtkExtractSelection.h"
#include "vtkExtractSelectedGraph.h"
#include "vtkExtractGeometry.h"
#include "vtkForceDirectedLayoutStrategy.h"
#include "vtkFilterColour.h"
#include "vtkForceDirectedLayoutStrategy.h"
#include "vtkFloatArray.h"
#include "vtkGraphLayout.h"
#include "vtkGeometryFilter.h"
#include "vtkGraphToPolyData.h"
#include "vtkInteractorStyleRubberBandPick.h"
#include "vtkIdTypeArray.h"
#include "vtkImplicitPlaneWidget.h"
#include "vtkJCNLayoutStrategy.h"
#include "vtkJCNLayoutStrategy.h"
#include "vtkMETAModule.h"
#include "vtkLookupTable.h"
#include "vtkMutableUndirectedGraph.h"
#include "vtkMultiDimensionalReebGraph.h"
#include "vtkMergeFields.h"
#include "vtkPolyDataMapper.h"
#include "vtkPlane.h"
#include "vtkRenderWindow.h"
#include "vtkRenderWindowInteractor.h"
#include "vtkRenderer.h"
#include "vtkRenderWindow.h"
#include "vtkRendererCollection.h"
#include "vtkSimplifyJCN.h"
#include "vtkSmartPointer.h"
#include "vtkSelectionNode.h"
#include "vtkSelection.h"
#include "vtkSimplifyMetric.h"
#include "vtkTriangleFilter.h"
#include "vtkTextProperty.h"
#include "vtkTransform.h"
#include "vtkOGDFLayoutStrategy.h"
#include "vtkObjectFactory.h"
#include "vtkPoints.h"
#include "vtkPolyData.h"
#include "vtkPolyhedralMeshToPolyData.h"
#include "vtkPointData.h"
#include "vtkProperty.h"
#include "vtkUnstructuredGrid.h"
#include "vtkVertexGlyphFilter.h"
#include "vtkMath.h"
#include "override/vtkJCNMergeFields.h"
#include <vector>


// Define interaction style
class VTK_META_EXPORT vtkJCNMouseInteractorStyle : public vtkInteractorStyleRubberBandPick
{
public:
	static vtkJCNMouseInteractorStyle* New();
	vtkTypeMacro(vtkJCNMouseInteractorStyle, vtkInteractorStyleRubberBandPick);

	// Description:
	// Get the input of graph structure from the JCN implementation.
	void SetGraph(vtkGraph *graph);
	void SetGrid(vtkUnstructuredGrid *grid);

	// Description:
	// Get the number of fields in the original data set.
	vtkSetMacro(FieldNr,int);
	vtkGetMacro(FieldNr,int);

	// Description:
	// Set the original glyph scale for JCN graph layout.
	vtkSetMacro(GlyphScale,double);
	vtkGetMacro(GlyphScale,double);

	// Description:
	// Set the flag if the input geometry is 2D or not.
	vtkSetMacro(TwoD, bool);
	vtkGetMacro(TwoD, bool);
	vtkBooleanMacro(TwoD, bool);

	// Description:
	// Set the flag if rendering Reeb Skeleton or not.
	vtkSetMacro(RenderRS, bool);
	vtkGetMacro(RenderRS, bool);
	vtkBooleanMacro(RenderRS, bool);

	// Description:
	// Initialise the rendering structure. This includes initialize the graph structure and
	// all of the needed arrays for filtering.
	int Initialize();

	// Description:
	// Add a field to be used when considering whether to retain a node.
	void AddField(const char *fieldName);

	// Description:
	// Set the input Simplified metric
	void SetMetric(vtkSimplifyMetric *metric);

	//Description:
	// Set boundary JacobiSet
	void SetJacobiSet(vtkDataArray *array);

protected:
	vtkJCNMouseInteractorStyle();
	~vtkJCNMouseInteractorStyle();

	// Left button event. We define the node selection event in this function.
	virtual void OnLeftButtonDown();
	// Middle button event.
	virtual void OnMiddleButtonDown();
	// Right mouse button event. When this button is pressed, the visualisation is
	// initialised.
	virtual void OnRightButtonDown();
	// Release the left button.
	virtual void OnLeftButtonUp();
	// Define the mouse movement event.
	virtual void OnMouseMove();
	// Key press event
	virtual void OnKeyPress();
	// Mouse selection on the graph nodes is controlled by an array of selected ids.
	// This method is called every time a selection is finished.
	void UpdateSelectIds();
	// Update the graph layout according to different selections and parameters.
	void UpdateGraph();
	// Update the slab geometry according to different selections and parameters.
	void UpdateSlabs();
	// Update render window to redraw everything.
	void UpdateRenderWindow();
	// Update the Component Connected Graph
	void UpdateComponentGraph();
	// The graph structure from the JCN output.
	vtkGraph *OutputGraph;
	// The geometry information of the JCN output.
	vtkUnstructuredGrid *Grid;
	// Component Connected Graph
	vtkGraph *ComponentGraph;
	// Flag to determine weather an input geometry is 2D or 3D.
	bool TwoD;

	// Dara array containing the vertex ids which are part of Jacobi sets.
	vtkSmartPointer<vtkIdTypeArray> InterMDRGIds;
	// Dara array containing the vertex ids which are part of Jacobi sets.
	vtkSmartPointer<vtkIdTypeArray> InterIds;
	// Data array containing the vertex ids which are touching boundary of the domains.
	vtkSmartPointer<vtkIdTypeArray> BoundaryIds;
	// Dara array containing the vertex ids which are part of Jacobi sets.
	vtkSmartPointer<vtkIdTypeArray> MDRGIds;
	// Data array containing the selected graph node ids.
	vtkSmartPointer<vtkIdTypeArray> SelectIds;
	vtkSmartPointer<vtkBitArray> SelectGraphBool;
	// Component Graph Selection List
	vtkSmartPointer<vtkIdTypeArray> SelectCGraphIds;
	vtkSmartPointer<vtkBitArray> SelectComponentGraphBool;
	// Renderer for slabs and graph.
	vtkSmartPointer<vtkRenderer> Slabs;
	vtkSmartPointer<vtkRenderer> Graph;
	vtkSmartPointer<vtkRenderer> ComponentGraphRenderer;
	// Actor to draw the bounding box of the brushed slabs.
	vtkSmartPointer<vtkCubeAxesActor> CubeAxesActor;
	// Mappers for slab, node and edge.
	vtkSmartPointer<vtkDataSetMapper> SelectedMapper;
	vtkSmartPointer<vtkPolyDataMapper> NodeMap;
	vtkSmartPointer<vtkPolyDataMapper> EdgeMap;
	vtkSmartPointer<vtkActor> SelectedActor;
	vtkSmartPointer<vtkActor> NodeActor;
	vtkSmartPointer<vtkActor> EdgeActor;
	vtkSmartPointer<vtkActor> CGNodeActor;
	vtkSmartPointer<vtkActor> CGEdgeActor;
	// Callback functions to handle picking event. To be used in the future.
	vtkSmartPointer<vtkCallbackCommand> StartPickCallback;
	vtkSmartPointer<vtkCallbackCommand> EndPickCallback;
	// Data range in each dimension. It is used for colour mapping.
	vtkSmartPointer<vtkDoubleArray> DataRange;
	// Global parameter for setting up graph layout.
	vtkSmartPointer<vtkGraphLayout> GraphLayout;
	// Global parameter for setting up component graph layout.
	vtkSmartPointer<vtkGraphLayout> ComponentGraphLayout;
	// OGDF layout strategy to render force-directed layout.
	vtkSmartPointer<vtkOGDFLayoutStrategy> LayoutStrategy;
	// JCN Layout Strategy
	vtkSmartPointer<vtkJCNLayoutStrategy> JCNLayout;
	// Filter for generating Jacobi sets.
	vtkSmartPointer<vtkDataArray> MDRGList;
	// Transform the word coordinate to display coordinate for brushing.
	vtkSmartPointer<vtkCoordinate> DisplayCoord;
	// Structure for cell vertex array. For each vertex, a list of cell ids are attached.
	std::vector< std::vector<int> > ComponentArray;
	// Glyph size for JCN
	double GlyphScale;
	// Glyph size for RS
	double GlyphScaleRS;
	// Determine weather a pick event is happend.
	int PickEvent;
	// Determine weather to use colour mapping.
	bool ColourMap;
	// Determine weather to show boundary.
	bool ShowBoundary;
	// Determine weather to show Jacobi sets.
	bool ShowMDRG;
	// Is the renderer is initialised or not ?
	bool IsInitialRender;
	// Number of fields.
	int FieldNr;
	// Number of vertex in a graph structure.
	int VertexNr;
	// Show the Jacobi Sets inside domain boundary
	bool ShowInterMDRG;
	// Global max value of a data range in multiple fields
	double GlobalMax;
	// Global min value of a data range in multiple fields
	double GlobalMin;
	// Glyph increase scale
	double GlyphInterval;
	// Which scalar field are currently used.
	int ColourField;
	// If render subgraph of selected nodes.
	bool SubGraph;
	// Name of scalar fields.
	std::vector<char*> Fields;
	// If show interior of reeb space.
	bool ShowInter;
	// The opacity of graph edges.
	double LineOpacity;
	// Threshold for filtering.
	double Threshold;
	// Weather render Reeb Skeleton graph.
	bool RenderRS;
	// 1 : And ; 0 : Or
	int AndOrRelation;
	// in the form (min,max), (min,max), (min,max)
	std::vector<double> ThresholdField;

	//	Used to iterate through possible layout options
	std::set<std::string> m_graph_layouts;
	std::string m_current_layout_name;
};

// Determine the max of two integers. 
inline int MaxInt(int a, int b)
{ 
	if (a >= b)
	{
		return a;
	}
	else
	{
		return b;
	}
}

// Determine the min of two integers. 
inline int MinInt(int a, int b)
{ 
	if (a >= b)
	{
		return b;
	}
	else
	{
		return a;
	}
}


// Callback function for Pick event. 
inline void StartPickCallbackFunction ( vtkObject* caller,
										long unsigned int eventId,
										void* clientData,
										void* callData )
{

	vtkJCNMouseInteractorStyle *style =
			static_cast<vtkJCNMouseInteractorStyle*>(caller);
}

// Callback function for Pick event. 
inline void EndPickCallbackFunction ( vtkObject* caller,
									  long unsigned int eventId,
									  void* clientData,
									  void* callData )
{
	vtkJCNMouseInteractorStyle *style =
			static_cast<vtkJCNMouseInteractorStyle*>(caller);
}
