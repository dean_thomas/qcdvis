/*=========================================================================

 *	File: vtkFilterColour.cxx
 *	Graph visualization library for VTK
 * 
 *	This software is distributed WITHOUT ANY WARRANTY; 
 *	without even the implied warranty of MERCHANTABILITY 
 *	or FITNESS FOR A PARTICULAR PURPOSE.  
 * 
 *	See the file copyright.txt for details.  

=========================================================================*/
#include "vtkAdjacentVertexIterator.h"
#include "vtkBitArray.h"
#include "vtkDataSetAttributes.h"
#include "vtkFilterColour.h"
#include "vtkFloatArray.h"
#include "vtkGraphEdge.h"
#include "vtkIdTypeArray.h"
#include "vtkIntArray.h"
#include "vtkInformation.h"
#include "vtkMutableDirectedGraph.h"
#include "vtkMutableUndirectedGraph.h"
#include "vtkObjectFactory.h"

vtkStandardNewMacro(vtkFilterColour);


//-----------------------------------------------------------------------
vtkFilterColour::vtkFilterColour()
{
  this->SetNumberOfOutputPorts(1);
  this->SelectIds = vtkSmartPointer<vtkIdTypeArray>::New();
  this->FieldNr = 0;
  this->VertexNr = 0;
}

vtkFilterColour::~vtkFilterColour()
{
  
}


void vtkFilterColour::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os, indent);
}

int vtkFilterColour::Initialize()
{
  if (this->FieldNr == 0 || this->VertexNr == 0)
    {
    vtkErrorMacro("A fieldNr or VertexNr has to be defined");
    return 0;
    }

  this->SelectIds->SetNumberOfComponents(this->FieldNr);
  this->SelectIds->SetNumberOfTuples(this->VertexNr);
  for (int i = 0; i < this->VertexNr; i++)
     {
     for (int j = 0; j < this->FieldNr; j++)
        {
        this->SelectIds->SetComponent(i,j,0);
        }
     }
}

//----------------------------------------------------------------------------
int vtkFilterColour::RequestData(
  vtkInformation* vtkNotUsed(request),
  vtkInformationVector** inputVector, 
  vtkInformationVector* outputVector)
{
  // Ensure we have valid inputs ...
  vtkGraph* const inputGraph = vtkGraph::GetData(inputVector[0]);
  vtkGraph* const outputGraph = vtkGraph::GetData(outputVector);
  vtkDataSetAttributes *inPD   = inputGraph->GetVertexData(),
                       *outPD  = outputGraph->GetVertexData();
  
  if (!inputGraph)
    {
    vtkErrorMacro("A vtkGraph object is required.");
    return 0;
    }

  vtkIdType nrVertices = inputGraph->GetNumberOfVertices();
  if (!nrVertices)
    {
    vtkErrorMacro("Empty graph, no vertices.");
    return 0;
    }

  if (Fields.size()==0)
    {
    vtkErrorMacro("Field Name required");
    return 0;
    }

  outputGraph->DeepCopy(inputGraph);

  bool notSelect;
  for (int i = 0; i < nrVertices; i++)
     {
     for (int j = 0; j < this->FieldNr; j++)
        {
        outPD->GetArray(Fields[j])->SetComponent(i,0,this->SelectIds->GetComponent(i,j));
        }              
     }  
  outputGraph->Squeeze();  
  return 1;
}
//--------------------------------------------------------------------------------------

void vtkFilterColour::SetSelectIds(vtkIdTypeArray *selectIds, int value)
{
   vtkIdType size = selectIds->GetNumberOfTuples();
   vtkIdType id;
   for (int i = 0; i < size; i++)
      {
      id = selectIds->GetValue(i);
      for (int j = 0; j < this->FieldNr; j++)
         {
         this->SelectIds->SetComponent(id,j,value);
         }
      }
}

void vtkFilterColour::SetFieldNr(int nr)
{
   this->FieldNr = nr;
}

void vtkFilterColour::SetVertexNr(int nr)
{
   this->VertexNr = nr;
}

void vtkFilterColour::SetFieldName(vtkstd::vector<char*> fieldName)
{
  this->Fields = fieldName;
}
