/*=========================================================================

 *	File: vtkBillboardMultivarGlyphs.h
 *	Graph visualization library for VTK
 * 
 *	This software is distributed WITHOUT ANY WARRANTY; 
 *	without even the implied warranty of MERCHANTABILITY 
 *	or FITNESS FOR A PARTICULAR PURPOSE.  
 * 
 *	See the file copyright.txt for details.  

=========================================================================*/
// .NAME vtkBillboardMultivarGlyphs - generate glyphs for multifield data.
// .SECTION Description
// vtkBillboardMultivarGlyphs generates pie glyphs for multifield data.
// Each glyph consists of a circle subdivided into equal sized segments,
// one segment per field.  The geometry of the glyph is oriented so that
// its normal is aligned with VPN of a given camera, ensuring that the
// glyphs act as billboards, tracking camera motion.  One glyph is
// positioned for each point in the input vtkPolyData; field data
// must be available as a multi-component scalar field of the
// input dataset's point data.

// .SECTION Caveats
// Currently a single colour map is used for each segment of the glyph;
// this needs to be corrected.
#ifndef __vtkBillboardMultivarGlyphs_h
#define __vtkBillboardMultivarGlyphs_h

#include "vtkCamera.h"
#include "vtkMETAModule.h"
#include "vtkPolyDataAlgorithm.h"
#include "vtkSmartPointer.h"

class vtkTransform;

inline double Log2( double n )  
{  
    // log(n)/log(2) is log2.  
    return log( n ) / log( 2.0 );  
}

class VTK_META_EXPORT vtkBillboardMultivarGlyphs : public vtkPolyDataAlgorithm
{
public:
  static vtkBillboardMultivarGlyphs *New();
  vtkTypeMacro(vtkBillboardMultivarGlyphs,vtkPolyDataAlgorithm);
  void PrintSelf(ostream& os, vtkIndent indent);

  // Description:
  // Set the camera used to determine glyph orientation.
  vtkSetObjectMacro(Camera, vtkCamera);
  vtkGetObjectMacro(Camera, vtkCamera);

  // Description:
  // Set number of points around arc of each segment.
  vtkSetMacro(Smoothness, int);
  vtkGetMacro(Smoothness, int);

  // Description:
  // Set scale factor applied to each glyph.
  vtkSetMacro(Scale, float);
  vtkGetMacro(Scale, float);
  
  // Description:
  // Set if using Log scale on the size of the graph node. 
  vtkSetMacro(SetLog, bool);
  vtkGetMacro(SetLog, bool);
  vtkBooleanMacro(SetLog, bool);
  
protected:
  vtkBillboardMultivarGlyphs();
  ~vtkBillboardMultivarGlyphs();

  // Override to specify support for any vtkDataSet input type.
  virtual int FillInputPortInformation(int port, vtkInformation* info);

  virtual unsigned long int GetMTime();

  // Main implementation.
  virtual int RequestData(vtkInformation*,
                          vtkInformationVector**,
                          vtkInformationVector*);

  vtkCamera *Camera;
  vtkTransform *Transform;
  double Orientation[4];
  int Smoothness;
  float Scale;
  bool SetLog;

private:
  vtkBillboardMultivarGlyphs(const vtkBillboardMultivarGlyphs&);  // Not implemented.
  void operator=(const vtkBillboardMultivarGlyphs&);  // Not implemented.
};

#endif

