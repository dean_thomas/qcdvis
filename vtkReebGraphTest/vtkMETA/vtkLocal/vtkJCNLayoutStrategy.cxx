/*=========================================================================

 *	File: vtkJCNLayoutStrategy.cxx
 *	Graph visualization library for VTK
 * 
 *	This software is distributed WITHOUT ANY WARRANTY; 
 *	without even the implied warranty of MERCHANTABILITY 
 *	or FITNESS FOR A PARTICULAR PURPOSE.  
 * 
 *	See the file copyright.txt for details.  

=========================================================================*/
#include "vtkBitArray.h"
#include "vtkDoubleArray.h"
#include "vtkGraph.h"
#include "vtkJCNLayoutStrategy.h"
#include "vtkMath.h"
#include "vtkObjectFactory.h"
#include "vtkDataSetAttributes.h"
#include "vtkPoints.h"
#include "vtkSmartPointer.h"

vtkStandardNewMacro(vtkJCNLayoutStrategy);

vtkJCNLayoutStrategy::vtkJCNLayoutStrategy()
{
  this->LockPoints = false;
  this->ClearRadius = 0.5;
}

vtkJCNLayoutStrategy::~vtkJCNLayoutStrategy()
{
}

void vtkJCNLayoutStrategy::Layout() 
{
  vtkPoints* points = vtkPoints::New();
  vtkIdType numVerts = this->Graph->GetNumberOfVertices();
  points->SetNumberOfPoints(numVerts);
  double *p = new double[3];
  double *q = new double[3];
  double ep[3];
  double delta, maxdelta;
  int axis;

  vtkSmartPointer<vtkDoubleArray> region
     = vtkSmartPointer<vtkDoubleArray>::New();

  vtkDoubleArray *arr = vtkDoubleArray::SafeDownCast(
       this->Graph->GetVertexData()->GetArray("center"));

  if (!arr)
    {
    vtkErrorMacro("No slab center array present");
    }

  for (vtkIdType i = 0; i < numVerts; i++)
     {
     arr->GetTuple(i, p);
     points->SetPoint(i, p);
     }
  this->Graph->SetPoints(points);
  points->Delete();


  if (this->LockPoints)
    {
    const double d = this->ClearRadius * this->ClearRadius;
    region->SetNumberOfValues(numVerts);
    region->SetName("region");
    for (vtkIdType u = 0; u < numVerts; u++)
       {
       region->SetValue(u, 0);
       }
    for (vtkIdType u = 0; u < numVerts; u++)
       {
       points->GetPoint(u, p);
       for (vtkIdType v = 0; v < numVerts; v++)
          {
          if (u == v)
            {
            continue;
            }
          points->GetPoint(v, q);
          if (vtkMath::Distance2BetweenPoints(p, q) < d)
            {
            region->SetValue(u, 100);
            }
          }
       } // for each vertex u
    this->Graph->GetVertexData()->AddArray(region);
    } // if locked

   delete [] p;
   delete [] q;
}

void vtkJCNLayoutStrategy::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os,indent);
}

