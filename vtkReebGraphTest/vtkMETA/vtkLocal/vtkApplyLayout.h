/*=========================================================================

 *	File: vtkApplyLayout.h
 *	Graph visualization library for VTK
 * 
 *	This software is distributed WITHOUT ANY WARRANTY; 
 *	without even the implied warranty of MERCHANTABILITY 
 *	or FITNESS FOR A PARTICULAR PURPOSE.  
 * 
 *	See the file copyright.txt for details.  

=========================================================================*/
// .NAME vtkApplyLayout - set the layout of one graph to that of a second.
// .SECTION Description
// vtkApplyLayout copies its input graph to its output, and assigns
// to the output a layout extracted from a second input, the layout 
// source.  It is assumed that the node Ids of the input graph are
// a subset of the node Ids of the layout source graph.  

#ifndef __vtkApplyLayout_h
#define __vtkApplyLayout_h

#include "vtkGraphAlgorithm.h"
#include "vtkMETAModule.h"
#include "vtkPassInputTypeAlgorithm.h"

class VTK_META_EXPORT vtkApplyLayout : public vtkPassInputTypeAlgorithm
{
public:
  static vtkApplyLayout *New();
  vtkTypeMacro(vtkApplyLayout,vtkPassInputTypeAlgorithm);
  void PrintSelf(ostream& os, vtkIndent indent);

protected:
  vtkApplyLayout();
  ~vtkApplyLayout();
  virtual int FillInputPortInformation(int port, vtkInformation *info);
  virtual int RequestData(
  vtkInformation*, 
  vtkInformationVector**, 
  vtkInformationVector*);

private:
  vtkApplyLayout(const vtkApplyLayout&) {};
  void operator=(const vtkApplyLayout&) {};
};

#endif	/* __vtkApplyLayout_h */

