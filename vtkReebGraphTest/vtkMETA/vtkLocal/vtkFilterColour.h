/*=========================================================================

 *	File: vtkFilterColour.h
 *	Graph visualization library for VTK
 * 
 *	This software is distributed WITHOUT ANY WARRANTY; 
 *	without even the implied warranty of MERCHANTABILITY 
 *	or FITNESS FOR A PARTICULAR PURPOSE.  
 * 
 *	See the file copyright.txt for details.  

=========================================================================*/
// .NAME vtkFilterColour - Filter colours of graph nodes
// .SECTION Description
//  This filter is used to control the selection list during the interaction process. 
//  The input of this filter is a graph structure. The scalar array of the output graph
//  only contains one or zero which indicating whether this node is selected or not. 
//  This list is further used for colouring the selected nodes. 

#ifndef __vtkFilterColour_h
#define __vtkFilterColour_h

#include "vtkGraphAlgorithm.h"
#include "vtkIdTypeArray.h"
#include "vtkMETAModule.h"
#include "vtkMutableGraphHelper.h"
#include "vtkSmartPointer.h"
#include <vector>

class VTK_META_EXPORT vtkFilterColour : public vtkGraphAlgorithm
{
public:
  static vtkFilterColour* New();
  vtkTypeMacro(vtkFilterColour, vtkGraphAlgorithm);
  void PrintSelf(ostream& os, vtkIndent indent);
  // Description:
  // Set the array of selected vertex ids. 
  void SetSelectIds(vtkIdTypeArray *selectIds, int value);
  // Description:
  // Set the number of input fields. 
  void SetFieldNr(int nr);
  // Description:
  // Set the number of input graph vertexes. 
  void SetVertexNr(int vertex);
  // Description:
  // Initialise data structure. 
  int Initialize();
  // Description:
  // Set the names of the fields. 
  void SetFieldName(std::vector<char*> fieldName);

protected:
  vtkFilterColour();
  ~vtkFilterColour();

  
  int RequestData(
    vtkInformation*, 
    vtkInformationVector**, 
    vtkInformationVector*);
  
  // The array of selected vertex ids.  
  vtkSmartPointer<vtkIdTypeArray> SelectIds;
  int FieldNr;
  int VertexNr;
  vtkstd::vector<char*> Fields;
  
private:
  vtkFilterColour(const vtkFilterColour&); // Not implemented
  void operator=(const vtkFilterColour&);   // Not implemented
};

#endif

