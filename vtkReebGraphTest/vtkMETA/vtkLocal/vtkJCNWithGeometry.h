/*=========================================================================

 *	File: vtkJCNWithGeometry.h
 *	Graph visualization library for VTK
 * 
 *	This software is distributed WITHOUT ANY WARRANTY; 
 *	without even the implied warranty of MERCHANTABILITY 
 *	or FITNESS FOR A PARTICULAR PURPOSE.  
 * 
 *	See the file copyright.txt for details.  

=========================================================================*/

// .NAME vtkJCNWithGeometry - Generate the Joint Contour Net and corresponding spatial decomposition.
// .SECTION Description:
// This filter extracts the Joint Contour Net with the algorithm described in 
// "H.Carr and D.Duke, Joint contour nets: Topological analysis of multivariate data. PacificVis 2013".
// The filter takes a simplicial mesh as input, and produces two outputs, 
//    (1) a graph dataset encoding the topological structure of the network, and 
//    (2) a set of 3D polyhedra (or 2D polygons) stored as an unstructured grid, representing the slabs. 
// The implementation processes the input grid one cell at a time, explicitly 
// computing the intersection of the cell with the cutting planes defined by the 
// slab boundaries, and constructing the resulting geometry.  
//
// .SECTION Caveats:
// The filter was implemented as an early testbed for multifield topology:
// 1.  The domain of the function is limited to R^2 and R^3.
// 2.  It has not been optimized for performance.
// 3.  The code is monolithic - poorly structured.
// 4.  In some cases (e.g. concentric shells) the geometry per slab consists of disconnected 
// polygonal meshes, i.e. the inner and outer shells of a slab.
// 5.  THIS FILTER WILL NOT BE MAINTAINED FURTHER - functionality for generating slab and fragment 
// geometry will be added to vtkJointContourNet.
// 
// .SECTION See also:
// vtkJointContourNet, vtkPolytopeGeometry

#ifndef __vtkJCNWithGeometry_h
#define __vtkJCNWithGeometry_h

#include "vtkMETAModule.h"
#include "vtkGraphAlgorithm.h"
#include "vtkSmartPointer.h"
#include <vector>
#include <vtkDataArray.h>
#include <vtkIdTypeArray.h>

//struct _Field;
class vtkPoints;
class vtkIdList;
class vtkPoints;
class vtkUndirectedGraph;
class vtkUnstructuredGrid;
//class vtkIdTypeArray;
class vtkPolyData;

typedef struct _Field {
	_Field() 
	{
		name = NULL; 
		pointScalars = NULL;
		slabScalars = NULL;
		tempScalars = NULL;
		nodeScalars = NULL;
		parents     = NULL;
		isBounded   = false;
	}

	~_Field() 
	{ 
		if (name) 
		{
			delete [] name;
			name = NULL;
		}
		pointScalars = NULL;
		if (tempScalars)
		{
			tempScalars->Delete();
		}
		if (slabScalars)
		{
			slabScalars->Delete();
		}
		if (nodeScalars)
		{
			nodeScalars->Delete();
		}
		if (parents)
		{
			parents->Delete();
		}
	}

	char *name;
	vtkDataArray                   *pointScalars;    // scalar value at cell vertices.
	vtkSmartPointer<vtkDataArray>   tempScalars;     // inputQ id -> thr for current field
	vtkSmartPointer<vtkDataArray>   slabScalars;     // cid -> thr for current field
	vtkSmartPointer<vtkDataArray>   nodeScalars;     // cid -> thr for current field
	vtkSmartPointer<vtkIdTypeArray> parents;         // parent cell in prev generation.
	double slabWidth;    // width of the intervals to slice the function range
	bool isBounded;      // bound the interval in which to perform slicing.
	double minBound;     // when bounded, the minimum value on which to slice
	double maxBound;     // ..                maximum  ...
	double range[2];     // range of values in this field.
} Field;

class VTK_META_EXPORT vtkJCNWithGeometry : public vtkGraphAlgorithm
{
public:
	friend _Field* GetFieldData(const size_t index);

  // Standard VTK object c
  static vtkJCNWithGeometry *New();
  vtkTypeMacro(vtkJCNWithGeometry,vtkGraphAlgorithm);
  void PrintSelf(ostream& os, vtkIndent indent);

  // Description:
  // Determine whether to center the first slab over the minimum field value.
  // Centering aligns sampling with integration and measure theory (cf 
  // papers by Duffy and Carr) Default is OFF.
  vtkSetMacro(CenterFirstSlab, bool);
  vtkGetMacro(CenterFirstSlab, bool);
  vtkBooleanMacro(CenterFirstSlab, bool);
    
  // Description:
  // Get the dimensionality of the most recent dataset.
  vtkGetMacro(Dimensionality, int);
  
  // Description:
  // Set the error factor used in double precision calculation, in
  // particular when matching points.
  vtkGetMacro(Epsilon, double);
  vtkSetMacro(Epsilon, double);

  // Description:
  // Specify a field, and the field width, to be used in subdividing
  // the dataset.  
  // Resetting field widths currently not supported.
  void AddField(const char *fieldName, double slabWidth);

  // Description:
  // Specify a field, and the field width, along with a minimum and
  // maximum bound on which to sample this field.
  void AddFieldWithBounds(const char *fieldName, double slabWidth, double minB, double maxB);
  
  // Description:
  // Return the name of the ith field to be sliced against.
  char *GetField(int n);

  _Field* GetFieldData(const size_t index) const;

  // Description:
  // Return number of fields to be sliced against.
  int GetNumberOfFields();

  // Description:
  // Reset all fields.
  void ClearFields();

  vtkUndirectedGraph  * GetTopologyGraph();
  vtkUnstructuredGrid * GetFragments();
  vtkUnstructuredGrid * GetBoundary();
  
  // Description:
  // Get the ith output.  Output 0 is the graph, output 1
  // is the fragment geometry, output 2 is the slab geometry.
  vtkDataObject* GetOutput(int index);

protected:
     
  vtkJCNWithGeometry();
  ~vtkJCNWithGeometry();


  // Decide the input port of the JCN.  The type of the input port is vtkDataSet.
  virtual int FillInputPortInformation(int port, vtkInformation* info);


  // This method setup the static characteristic of the output port such as data type
  // requirements and whetere the port is optional or repreatable. There are three output
  // ports produced by JCN.
  virtual int FillOutputPortInformation(int port, vtkInformation* info);


  // This method asks the algorithm to create an instance of vtkDataObject( or appropriate subclass )
  // for all of its output ports. We have defined the type of the output ports as undirected graph
  // and unstructured grids respectively. If the algorithm does not handle the request then by default
  // the executive will look at the output port information to see what type the output port has been
  // set to from FillOutputPortInformation. If this is a concrete type then it will instantiate an
  // instance of that type and usee it.
  virtual int RequestDataObject(vtkInformation*, vtkInformationVector**, vtkInformationVector*);


  // This method is the main implementatio of the fiter. ln In the first, each cell of the mesh is explicitly
  // subdivided along contours (isosurfaces) of the individual variables to decompose the domain into slabs of
  // constant (quantized) values in each cell. In the second, the adjacency graph of the slabs is extracted:
  // i.e. the graph representation of isovalued regions. Finally, adjacent slabs (in neighbouring cells) with
  // identical isovalues are collapsed to compute the Joint Contour Net. This representation then encapsulates
  // the topological structure of the multifield at the chosen level of quantization. Moreover, varying the
  // coarseness of quantization can be used to simplify the Joint Contour Net on demand, and the contour tree
  // turns out to be a special case.
  virtual int RequestData(vtkInformation*,
                          vtkInformationVector**,
                          vtkInformationVector*);


  // Find the center of the face of polyhedra.
  void FaceCenter(vtkIdList *face, vtkPoints *points, double center[]);


  // Find the root of a value stored in the union-find structure, performing path compression.
  vtkIdType Find(vtkIdType x);


  // The flag for sample centers.
  bool CenterFirstSlab;


  //  fraction of shrink for each cell. The default is 0.5.
  double Epsilon;


  //  the number of dimensionality of fragments
  int Dimensionality; 


  //Union find structure
  vtkSmartPointer<vtkIdTypeArray> UF;

  // Data about range fir
  std::vector<_Field*> Fields;
};

#endif

