/*=========================================================================

 *	File: vtkScissionSimplification.h
 *	Graph visualization library for VTK
 * 
 *	This software is distributed WITHOUT ANY WARRANTY; 
 *	without even the implied warranty of MERCHANTABILITY 
 *	or FITNESS FOR A PARTICULAR PURPOSE.  
 * 
 *	See the file copyright.txt for details.  

=========================================================================*/
// .NAME vtkScissionSimplification - JCN simplification for scission-point datasets.
//
// .SECTION Description
// Although vtkScissionSimplification operates on an arbitrary graph with multiple
// fields defined at nodes, it was designed and is intended for simplifying Joint
// Contour Nets, and specifically those arising from nuclear scission data (see
// Duke et.al., Visualizing Scission Through a Multifield Extension of Topological
// Analysis, Proc. IEEE VisWeek 2012.
// The filter retains only those nodes of a graph for which the value of each of a
// specified list of fields is within some threshold of the maximum value for that
// field in the dataset.  Edges in the output are exactly those input edges that
// connect retained nodes.  
// The filter may yet be useful for JCNs from other domains.  Alternatively, a more
// generic filter using selections and working in collaboration with VTK's array
// calculator may be developed, if sufficient use-cases appear.

#ifndef __vtkScissionSimplification_h
#define __vtkScissionSimplification_h

#include "vtkGraphAlgorithm.h"
#include "vtkMETAModule.h"
#include "vtkSmartPointer.h"  
#include <vector>

class vtkDirectedGraph;
class vtkDataArray;  

class VTK_META_EXPORT vtkScissionSimplification : public vtkGraphAlgorithm
{
public:
  static vtkScissionSimplification* New();
  vtkTypeMacro(vtkScissionSimplification,vtkGraphAlgorithm);
  void PrintSelf(ostream& os, vtkIndent indent);

  // Description:
  // Add a field to be used when considering whether to retain a node.
  void AddField(const char *fieldName);
   
  // Description:
  // Set/get the threshold (0 <= t <= 1) that each attribute of the node must
  // pass to trigger retention.  For an attribute ranging over amin ... amax,
  // an attribute value of A is mapped to 1 - ((amax - A) / (amax-amin))
  vtkSetMacro(Threshold, double);
  vtkGetMacro(Threshold, double);
    
protected:
  vtkScissionSimplification();
  ~vtkScissionSimplification();

  virtual int RequestData(
     vtkInformation*, 
     vtkInformationVector**, 
     vtkInformationVector*);

  std::vector<char*> Fields;

  double Threshold;

private:
  vtkScissionSimplification(const vtkScissionSimplification&); // Not implemented
  void operator=(const vtkScissionSimplification&);   // Not implemented
};

#endif

