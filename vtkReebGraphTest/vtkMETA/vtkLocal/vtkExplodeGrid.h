/*=========================================================================

 *	File: vtkExplodeGrid.h
 *	Graph visualization library for VTK
 * 
 *	This software is distributed WITHOUT ANY WARRANTY; 
 *	without even the implied warranty of MERCHANTABILITY 
 *	or FITNESS FOR A PARTICULAR PURPOSE.  
 * 
 *	See the file copyright.txt for details.  

=========================================================================*/

// .NAME vtkExplodeGrid - Expose slab structure induced by a joint contour net.
// .SECTION Description
// vtkExplodeGrid exposes the geometric slabs induced by a joint
// contour net by translating each slab by a constant factor from
// the centre of the spatial extent occupied by the slabs. The
// direction of translation is the vector from the center of the
// spatial extent to the barycenter of the cell.
//
// .SECTION Caveats
// The utility of slab explosion clearly depends on the slab geometry.
// In some cases, e.g. data whose slabs form concentric regions around
// the center, explosion is unhelpful.
//
// .SECTION See Also
// vtkJointContourNet

#ifndef __vtkExplodeGrid_h
#define __vtkExplodeGrid_h

#include "vtkMETAModule.h"
#include "vtkUnstructuredGridAlgorithm.h"


class VTK_META_EXPORT vtkExplodeGrid : public vtkUnstructuredGridAlgorithm
{
public:
  static vtkExplodeGrid *New();
  vtkTypeMacro(vtkExplodeGrid,vtkUnstructuredGridAlgorithm);
  void PrintSelf(ostream& os, vtkIndent indent);

  // Description
  // The factor by which to translate each slab from the geometric
  // center.
  vtkSetMacro(ScaleFactor, float);
  vtkGetMacro(ScaleFactor, float);
  
protected:
  vtkExplodeGrid();
  ~vtkExplodeGrid();

  // Override to specify support for any vtkDataSet input type.
  virtual int FillInputPortInformation(int port, vtkInformation* info);

  // Main implementation.
  virtual int RequestData(vtkInformation*,
                          vtkInformationVector**,
                          vtkInformationVector*);

  float ScaleFactor;
  
private:
  vtkExplodeGrid(const vtkExplodeGrid&);  // Not implemented.
  void operator=(const vtkExplodeGrid&);  // Not implemented.
};

#endif

