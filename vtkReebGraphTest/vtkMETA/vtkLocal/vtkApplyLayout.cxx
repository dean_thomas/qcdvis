/*=========================================================================

 *	File: vtkApplyLayout.cxx
 *	Graph visualization library for VTK
 * 
 *	This software is distributed WITHOUT ANY WARRANTY; 
 *	without even the implied warranty of MERCHANTABILITY 
 *	or FITNESS FOR A PARTICULAR PURPOSE.  
 * 
 *	See the file copyright.txt for details.  
=========================================================================*/

#include "vtkApplyLayout.h"
#include "vtkDataArray.h"
#include "vtkDataSetAttributes.h"
#include "vtkInformation.h"
#include "vtkInformationVector.h"
#include "vtkObjectFactory.h"
#include "vtkObjectFactory.h"
#include "vtkPoints.h"
#include "vtkSmartPointer.h"

//-------------------------------------------------
vtkStandardNewMacro(vtkApplyLayout);

vtkApplyLayout::vtkApplyLayout()
{
  this->SetNumberOfInputPorts(2);
}

vtkApplyLayout::~vtkApplyLayout()
{
}

//---------------------------------------------------------------------------

int vtkApplyLayout::FillInputPortInformation(int port, vtkInformation *info)
{
  if (port == 0)
    {
    info->Set(vtkAlgorithm::INPUT_REQUIRED_DATA_TYPE(), "vtkGraph");
    }
  else if (port == 1)
    {
    info->Set(vtkAlgorithm::INPUT_REQUIRED_DATA_TYPE(), "vtkGraph");
    }

  return 1;
}


//-------------------------------------------------

// Simple graph layout method
int vtkApplyLayout::RequestData(
  vtkInformation* vtkNotUsed(request), 
  vtkInformationVector** inputVector, 
  vtkInformationVector* outputVector)
{
  
  // Access and check minimal expected structure: a graph (the 
  // reduced contour tree) on port 0, along with a scalar field
  // defined at each vertex.
  vtkInformation *info0 = inputVector[0]->GetInformationObject(0);
  vtkGraph *graph0 = vtkGraph::SafeDownCast(info0->Get(vtkDataObject::DATA_OBJECT()));
  vtkInformation *info1 = inputVector[1]->GetInformationObject(0);
  vtkGraph *graph1 = vtkGraph::SafeDownCast(info1->Get(vtkDataObject::DATA_OBJECT()));
    
  vtkDataSetAttributes *inVertData, *inEdgeData;

  if (!graph0 || !graph1)
    {
    vtkErrorMacro("Two vtkGraph objects are reqired.");
    return 1;
    }

  // Grab output structure and copy structure of appropriate graph.
  vtkInformation *outputInfo = outputVector->GetInformationObject(0);
  vtkGraph *output = vtkGraph::SafeDownCast(
    outputInfo->Get(vtkDataObject::DATA_OBJECT()));

  output->ShallowCopy(graph0);

  vtkSmartPointer<vtkPoints> copy = vtkSmartPointer<vtkPoints>::New();
  copy->SetNumberOfPoints(graph0->GetNumberOfVertices());
  double p[3];

  for (vtkIdType i = 0; i < graph0->GetNumberOfVertices(); i++)
     {
     graph1->GetPoints()->GetPoint(i, p);
     copy->SetPoint(i, p);
     }

  vtkDataArray *arr;
  // Copy across arrays, if appropriate
  for (int a = 0; a < graph1->GetVertexData()->GetNumberOfArrays(); a++)
     {
     arr = graph1->GetVertexData()->GetArray(a);
     if (!graph0->GetVertexData()->HasArray(arr->GetName()))
       {
       graph0->GetVertexData()->AddArray(arr);
       }
      }
  
  for (int a = 0; a < graph1->GetEdgeData()->GetNumberOfArrays(); a++)
     {
     arr = graph1->GetEdgeData()->GetArray(a);
     if (!graph0->GetEdgeData()->HasArray(arr->GetName()))
       {
       graph0->GetEdgeData()->AddArray(arr);
       }
      }
  output->SetPoints(copy);
  return 1;
}

void vtkApplyLayout::PrintSelf(ostream& os, vtkIndent indent)
{
   vtkPassInputTypeAlgorithm::PrintSelf(os,indent);
   os << indent << "ApplyLayout filter.\n";
}


