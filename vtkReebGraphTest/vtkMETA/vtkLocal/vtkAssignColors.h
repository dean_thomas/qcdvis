/*=========================================================================

 *	File: vtkAssignColors.h
 *	Graph visualization library for VTK
 * 
 *	This software is distributed WITHOUT ANY WARRANTY; 
 *	without even the implied warranty of MERCHANTABILITY 
 *	or FITNESS FOR A PARTICULAR PURPOSE.  
 * 
 *	See the file copyright.txt for details.  

=========================================================================*/

// .NAME vtkAssignColors - apply colors according to range of each scalar field. 
// .SECTION Description
// This filter evaluates the colour value for each JCN node. This colour value 
// for each JCN node in each field is depicted as RGBA format, which is computed by the 
// interpolation along the range of the current field values.  


#ifndef __vtkAssignColors_h
#define __vtkAssignColors_h

#include "vtkDoubleArray.h"
#include "vtkMETAModule.h"
#include "vtkPassInputTypeAlgorithm.h"
#include "vtkSmartPointer.h"
#include <iostream>

class vtkScalarsToColors;
class vtkUnsignedCharArray;

class VTK_META_EXPORT vtkAssignColors : public vtkPassInputTypeAlgorithm 
{
public:
  static vtkAssignColors *New();
  vtkTypeMacro(vtkAssignColors, vtkPassInputTypeAlgorithm);
  void PrintSelf(ostream& os, vtkIndent indent);

  // Description:
  // The lookup table to use for point colors. This is only used if
  // input array 0 is set and UsePointLookupTable is on.
  virtual void SetPointLookupTable(vtkScalarsToColors* lut);
  vtkGetObjectMacro(PointLookupTable, vtkScalarsToColors);

  // Description:
  // If on, uses the point lookup table to set the colors of unannotated,
  // unselected elements of the data.
  vtkSetMacro(UsePointLookupTable, bool);
  vtkGetMacro(UsePointLookupTable, bool);
  vtkBooleanMacro(UsePointLookupTable, bool);

  // Description:
  // If on, uses the range of the data to scale the lookup table range.
  // Otherwise, uses the range defined in the lookup table.
  vtkSetMacro(ScalePointLookupTable, bool);
  vtkGetMacro(ScalePointLookupTable, bool);
  vtkBooleanMacro(ScalePointLookupTable, bool);

  // Description:
  // The default point color for all unannotated, unselected elements
  // of the data. This is used if UsePointLookupTable is off.
  vtkSetVector3Macro(DefaultPointColor, double);
  vtkGetVector3Macro(DefaultPointColor, double);

  // Description:
  // The default point opacity for all unannotated, unselected elements
  // of the data. This is used if UsePointLookupTable is off.
  vtkSetMacro(DefaultPointOpacity, double);
  vtkGetMacro(DefaultPointOpacity, double);

  // Description:
  // The point color for all selected elements of the data.
  // This is used if the selection input is available.
  vtkSetVector3Macro(SelectedPointColor, double);
  vtkGetVector3Macro(SelectedPointColor, double);

  // Description:
  // The point opacity for all selected elements of the data.
  // This is used if the selection input is available.
  vtkSetMacro(SelectedPointOpacity, double);
  vtkGetMacro(SelectedPointOpacity, double);

  // Description:
  // The output array name for the point color RGBA array.
  // Default is "vtkAssignColors color".
  vtkSetStringMacro(PointColorOutputArrayName);
  vtkGetStringMacro(PointColorOutputArrayName);

  // Description:
  // The lookup table to use for cell colors. This is only used if
  // input array 1 is set and UseCellLookupTable is on.
  virtual void SetCellLookupTable(vtkScalarsToColors* lut);
  vtkGetObjectMacro(CellLookupTable, vtkScalarsToColors);

  // Description:
  // If on, uses the cell lookup table to set the colors of unannotated,
  // unselected elements of the data.
  vtkSetMacro(UseCellLookupTable, bool);
  vtkGetMacro(UseCellLookupTable, bool);
  vtkBooleanMacro(UseCellLookupTable, bool);

  // Description:
  // If on, uses the range of the data to scale the lookup table range.
  // Otherwise, uses the range defined in the lookup table.
  vtkSetMacro(ScaleCellLookupTable, bool);
  vtkGetMacro(ScaleCellLookupTable, bool);
  vtkBooleanMacro(ScaleCellLookupTable, bool);

  // Description:
  // The default cell color for all unannotated, unselected elements
  // of the data. This is used if UseCellLookupTable is off.
  vtkSetVector3Macro(DefaultCellColor, double);
  vtkGetVector3Macro(DefaultCellColor, double);

  // Description:
  // The default cell opacity for all unannotated, unselected elements
  // of the data. This is used if UseCellLookupTable is off.
  vtkSetMacro(DefaultCellOpacity, double);
  vtkGetMacro(DefaultCellOpacity, double);

  // Description:
  // The cell color for all selected elements of the data.
  // This is used if the selection input is available.
  vtkSetVector3Macro(SelectedCellColor, double);
  vtkGetVector3Macro(SelectedCellColor, double);

  // Description:
  // The cell opacity for all selected elements of the data.
  // This is used if the selection input is available.
  vtkSetMacro(SelectedCellOpacity, double);
  vtkGetMacro(SelectedCellOpacity, double);

  // Description:
  // The output array name for the cell color RGBA array.
  // Default is "vtkAssignColors color".
  vtkSetStringMacro(CellColorOutputArrayName);
  vtkGetStringMacro(CellColorOutputArrayName);

  // Description:
  // Use the annotation to color the current annotation
  // (i.e. the current selection). Otherwise use the selection
  // color attributes of this filter.
  vtkSetMacro(UseCurrentAnnotationColor, bool);
  vtkGetMacro(UseCurrentAnnotationColor, bool);
  vtkBooleanMacro(UseCurrentAnnotationColor, bool);

  // Description:
  // If on, uses the point lookup table to set the colors of unannotated,
  // unselected elements of the data.
  void SetTableRange(vtkDoubleArray *array);

  // Description:
  // Retrieve the modified time for this filter.
  virtual long unsigned int GetMTime();

protected:
  vtkAssignColors();
  ~vtkAssignColors();
  

  // Convert the vtkGraph into vtkPolyData.
  int RequestData(
    vtkInformation *, vtkInformationVector **, vtkInformationVector *);
  
  // Set the input type of the algorithm to vtkGraph.
  int FillInputPortInformation(int port, vtkInformation* info);

  void ProcessColorArray(
    vtkUnsignedCharArray* colorArr,
    vtkScalarsToColors* lut,
    vtkAbstractArray* arr,
    unsigned char color[4],
    bool scale);

  vtkScalarsToColors* PointLookupTable;
  vtkScalarsToColors* CellLookupTable;
  double DefaultPointColor[3];
  double DefaultPointOpacity;
  double DefaultCellColor[3];
  double DefaultCellOpacity;
  double SelectedPointColor[3];
  double SelectedPointOpacity;
  double SelectedCellColor[3];
  double SelectedCellOpacity;
  bool ScalePointLookupTable;
  bool ScaleCellLookupTable;
  bool UsePointLookupTable;
  bool UseCellLookupTable;
  char* PointColorOutputArrayName;
  char* CellColorOutputArrayName;
  bool UseCurrentAnnotationColor;
  vtkSmartPointer<vtkDoubleArray> Range;

private:
  vtkAssignColors(const vtkAssignColors&);  // Not implemented.
  void operator=(const vtkAssignColors&);  // Not implemented.
};


// Set the dimensionality of the fields. 
inline void vtkAssignColors::SetTableRange(vtkDoubleArray *data)
{
  int size = data->GetNumberOfTuples();
  int dim = data->GetNumberOfComponents();   
  this->Range->SetNumberOfComponents(dim); 
  this->Range->SetNumberOfTuples(size);
   
  for (vtkIdType i = 0; i < size; i++)
     {
       double *value = data->GetTuple(i);
       this->Range->SetTuple(i,value);
     }

}

#endif
