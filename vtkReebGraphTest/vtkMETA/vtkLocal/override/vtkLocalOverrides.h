
#include "vtkObjectFactory.h"
#include "vtkVersion.h"

        

class vtkCustomObjectFactory: public vtkObjectFactory
{
public:  
    
    vtkTypeMacro( vtkCustomObjectFactory, vtkObjectFactory );
    static vtkCustomObjectFactory* New();
    
    virtual vtkObject* CreateObject( const char* classname );
    
    virtual const char* GetVTKSourceVersion() { return VTK_VERSION; }
    
    virtual const char* GetDescription() { return "Overrides for META project"; }
};


