#include "vtkJCNArrayCalculator.h"
#include "vtkJCNMergeFields.h"
#include "vtkJCNMergePoints.h"
#include "vtkJCNSplitField.h"

#include "vtkObjectFactory.h"
#include "vtkVersion.h"

class vtkCustomObjectFactory: public vtkObjectFactory
{
public:  
    
    vtkTypeMacro( vtkCustomObjectFactory, vtkObjectFactory );
    static vtkCustomObjectFactory* New();
    
    virtual vtkObject* CreateObject( const char* classname )
    {

        #define OVERRIDE( oldcn, newcn ) \
        if( !strcmp( classname, #oldcn ) ) { \
            return newcn::New(); \
        }

        OVERRIDE( vtkMergeFields, vtkJCNMergeFields );
        OVERRIDE( vtkArrayCalculator, vtkJCNArrayCalculator );
        OVERRIDE( vtkMergePoints, vtkJCNMergePoints );
        OVERRIDE( vtkSplitField, vtkJCNSplitField );
        
        return NULL;
    }
    
    virtual const char* GetVTKSourceVersion() {
        return VTK_VERSION;
    }
    
    virtual const char* GetDescription() {
        return "foo";
    }
};

vtkStandardNewMacro(vtkCustomObjectFactory);

// --------------------------------

#ifdef __GNUC__
#define CONSTRUCT(_func) static void _func (void) __attribute__((constructor));
#define DESTRUCT(_func) static void _func (void) __attribute__((destructor));
#elif defined (_MSC_VER) && (_MSC_VER >= 1500)
#ifndef CONSTRUCT
#define CONSTRUCT(_func) \
	static void _func(void); \
	static int _func ## _wrapper(void) { _func(); return 0; } \
	__pragma(section(".CRT$XCU",read)) \
	__declspec(allocate(".CRT$XCU")) static int (* _array ## _func)(void) = _func ## _wrapper;
#endif
#ifndef DESTRUCT
#define DESTRUCT(_func) \
  static void _func(void); \
  static int _func ## _constructor(void) { atexit (_func); return 0; } \
  __pragma(section(".CRT$XCU",read)) \
  __declspec(allocate(".CRT$XCU")) static int (* _array ## _func)(void) = _func ## _constructor;
#endif
#else
#error "You will need constructor support for your compiler"
#endif

static void setup() 
{
    std::cerr << "vtkLocal: class overrides enabled\n";

    vtkCustomObjectFactory* cof = vtkCustomObjectFactory::New();
    vtkObjectFactory::RegisterFactory( cof );
}

CONSTRUCT(setup)
