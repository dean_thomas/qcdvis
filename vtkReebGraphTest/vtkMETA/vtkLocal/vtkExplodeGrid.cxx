/*=========================================================================

 *	File: vtkExplodeGrid.cxx
 *	Graph visualization library for VTK
 * 
 *	This software is distributed WITHOUT ANY WARRANTY; 
 *	without even the implied warranty of MERCHANTABILITY 
 *	or FITNESS FOR A PARTICULAR PURPOSE.  
 * 
 *	See the file copyright.txt for details.  

=========================================================================*/

#include "vtkCell.h"
#include "vtkCellData.h"
#include "vtkCellArray.h"
#include "vtkDoubleArray.h"
#include "vtkExplodeGrid.h"
#include "vtkGenericCell.h"
#include "vtkIdList.h"
#include "vtkInformation.h"
#include "vtkInformationVector.h"
#include "vtkMath.h"
#include "vtkMergePoints.h"
#include "vtkObjectFactory.h"
#include "vtkPolyhedron.h"
#include "vtkPointData.h"
#include "vtkSmartPointer.h"
#include "vtkUnsignedCharArray.h"
#include "vtkUnstructuredGrid.h"

vtkStandardNewMacro(vtkExplodeGrid);


//----------------------------------------------------------------------------
vtkExplodeGrid::vtkExplodeGrid()
{
  this->ScaleFactor = 1.0;
}

//----------------------------------------------------------------------------
vtkExplodeGrid::~vtkExplodeGrid()
{
}

//----------------------------------------------------------------------------
void vtkExplodeGrid::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os,indent);
}

//----------------------------------------------------------------------------
int vtkExplodeGrid::FillInputPortInformation(int, vtkInformation* info)
{
  // This filter uses the vtkDataSet cell traversal methods so it
  // suppors any data set type as input.
  info->Set(vtkAlgorithm::INPUT_REQUIRED_DATA_TYPE(), "vtkUnstructuredGrid");
  return 1;
}

//----------------------------------------------------------------------------
int vtkExplodeGrid::RequestData(vtkInformation*,
                                 vtkInformationVector** inputVector,
                                 vtkInformationVector* outputVector)
{
  // Get input and output data.
  // get the info objects
  vtkInformation *inInfo = inputVector[0]->GetInformationObject(0);
  vtkInformation *outInfo = outputVector->GetInformationObject(0);

  // get the input and output
  vtkUnstructuredGrid *input = vtkUnstructuredGrid::SafeDownCast(
    inInfo->Get(vtkDataObject::DATA_OBJECT()));
  vtkUnstructuredGrid *output = vtkUnstructuredGrid::SafeDownCast(
    outInfo->Get(vtkDataObject::DATA_OBJECT()));

  vtkPoints *points = input->GetPoints();

  // Skip execution if there is no input geometry.
  vtkIdType numCells = input->GetNumberOfCells();
  vtkIdType numPts   = input->GetNumberOfPoints();
  
  vtkPointData *inPD = input->GetPointData(), *outPD = output->GetPointData();
  vtkCellData  *inCD = input->GetCellData(),  *outCD = output->GetCellData();

  if (numCells < 1 || numPts < 1)
    {
    vtkDebugMacro("No input data.");
    return 1;
    }
    
  outCD->ShallowCopy(inCD);
  outPD->CopyAllocate(inPD, numCells*4, numCells);  

  output->Allocate(numCells);  // Estimate of output size.
  
  vtkPoints *oldPoints = input->GetPoints();
  vtkCell *cell;

  vtkSmartPointer<vtkIdList> pids      = vtkSmartPointer<vtkIdList>::New();
  vtkSmartPointer<vtkIdList> newPIDs   = vtkSmartPointer<vtkIdList>::New();
  vtkSmartPointer<vtkPoints> newPoints = vtkSmartPointer<vtkPoints>::New();
  newPoints->Allocate(numPts*4, numPts);

  vtkIdType pid, cid;
  double p[3], center[3], bc[3], newbc[3], delta[3];

  vtkIdType newPID = 0;
  vtkIdType oldPID;
  int offset;

  input->GetCenter(center);
  for (int c = 0; c < numCells; c++)
     {    
     input->GetCellPoints(c, pids);
    
     // Compute barycenter of cell
     for (int i = 0; i < 3; i++)
        {
        bc[i] = 0.0;
        }
     for (int i = 0; i < pids->GetNumberOfIds(); i++)
        {
        oldPoints->GetPoint(pids->GetId(i), p);
        for (int j = 0; j < 3; j++)
           {
           bc[j] += p[j];
           }
         }
     // Calculate newbc = Scale * (bc - center)  
     for (int j = 0; j < 3; j++)
        {
        bc[j] /= pids->GetNumberOfIds();
        newbc[j] = (bc[j] - center[j]) * this->ScaleFactor;
        delta[j] = newbc[j] - bc[j];
        }

     // Create new cell, with distinct points.
     // All pids in original cell get mapped to new pids
     // (no shared points); points are offset by delta.
     int nrFaces;
     int nrPoints;

     if (input->GetCellType(c) == VTK_POLYHEDRON)
       {
       input->GetFaceStream(c, pids);
       offset = 0;
       nrFaces = pids->GetId(offset++);
       newPIDs->Reset();
       newPIDs->InsertNextId(nrFaces);
       for (int f = 0; f < nrFaces; f++)
          {
          nrPoints = pids->GetId(offset++);
          newPIDs->InsertNextId(nrPoints);
          for (int i = 0; i < nrPoints; i++)
             {
             oldPID = pids->GetId(offset++);
             newPIDs->InsertNextId(newPID);
             oldPoints->GetPoint(oldPID, p);
             for (int j = 0; j < 3; j++)
                {
                p[j] += delta[j];
                }
              newPoints->InsertPoint(newPID, p);
              outPD->CopyData(inPD, oldPID, newPID);
              newPID++;
              }
           }
          output->InsertNextCell(VTK_POLYHEDRON, newPIDs);
         }
       else
         {
         newPIDs->Reset();
         for (int i = 0; i < pids->GetNumberOfIds(); i++)
            {
            newPIDs->InsertNextId(newPID);
            oldPoints->GetPoint(pids->GetId(i), p);
            for (int j = 0; j < 3; j++)
               {
               p[j] += delta[j];
               }
             newPoints->InsertPoint(newPID, p);
             outPD->CopyData(inPD, pids->GetId(i), newPID);
             newPID++;
             }
           output->InsertNextCell(input->GetCellType(c), newPIDs);
           }
    } // for each cell C

  // output->SetCells(types, locations, fragments);
  output->SetPoints(newPoints);
  output->BuildLinks();
  // Avoid keeping extra memory around.
  output->Squeeze();
  return 1;
}


