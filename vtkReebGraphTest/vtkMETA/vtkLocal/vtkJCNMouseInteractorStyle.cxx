/*=========================================================================

 *	File: vtkJCNMouseInteractorStyle.cxx
 *	Graph visualization library for VTK
 *
 *	This software is distributed WITHOUT ANY WARRANTY;
 *	without even the implied warranty of MERCHANTABILITY
 *	or FITNESS FOR A PARTICULAR PURPOSE.
 *
 *	See the file copyright.txt for details.

=========================================================================*/
#include "vtkJCNMouseInteractorStyle.h"
#include <vtkGL2PSExporter.h>

#include "slab_stats.h"
#include <climits>

using namespace std;

vtkStandardNewMacro(vtkJCNMouseInteractorStyle); 

vtkJCNMouseInteractorStyle::vtkJCNMouseInteractorStyle() 
{
	//	List of supported layouts in the OGDF layout strategy
	this->m_graph_layouts = { "balloon", "circular", "davidsonharel", "dominance", "fmmm",
							  "gem", "mmmexamplefast", "mmmexamplenice", "mmmexamplenotwist",
							  "fruchtermanreingold", "kamadakawai", "sugiyama" };
	this->m_current_layout_name = *m_graph_layouts.cbegin();

	this->OutputGraph = NULL;
	this->Grid = NULL;
	this->ComponentGraph = NULL;
	this->MDRGList = NULL;
	this->TwoD = false;
	this->VertexNr = 0;
	this->GlyphScale = this->GlyphScaleRS = 4;

	this->BoundaryIds = vtkSmartPointer<vtkIdTypeArray>::New();
	this->BoundaryIds->Initialize();

	this->MDRGIds = vtkSmartPointer<vtkIdTypeArray>::New();
	this->MDRGIds->Initialize();

	this->InterMDRGIds = vtkSmartPointer<vtkIdTypeArray>::New();
	this->InterIds = vtkSmartPointer<vtkIdTypeArray>::New();
	this->InterMDRGIds->Initialize();

	this->SelectIds = vtkSmartPointer<vtkIdTypeArray>::New();
	this->SelectCGraphIds = vtkSmartPointer<vtkIdTypeArray>::New();
	this->SelectGraphBool = vtkSmartPointer<vtkBitArray>::New();
	this->SelectComponentGraphBool = vtkSmartPointer<vtkBitArray>::New();
	this->SelectIds->Initialize();
	this->SelectCGraphIds->Initialize();
	this->SelectComponentGraphBool = vtkSmartPointer<vtkBitArray>::New();
	this->SelectedMapper = vtkSmartPointer<vtkDataSetMapper>::New();
	this->SelectedActor = vtkSmartPointer<vtkActor>::New();
	this->CubeAxesActor = vtkSmartPointer<vtkCubeAxesActor>::New();
	this->NodeMap = vtkSmartPointer<vtkPolyDataMapper>::New();
	this->NodeActor = vtkSmartPointer<vtkActor>::New();
	this->EdgeMap = vtkSmartPointer<vtkPolyDataMapper>::New();
	this->EdgeActor = vtkSmartPointer<vtkActor>::New();
	this->CGEdgeActor = vtkSmartPointer<vtkActor>::New();
	this->CGNodeActor = vtkSmartPointer<vtkActor>::New();

	this->Graph = vtkSmartPointer<vtkRenderer>::New();
	this->Graph->SetViewport(0,0,0.5,1);
	this->Graph->SetBackground(1, 1, 1);
	this->Slabs = vtkSmartPointer<vtkRenderer>::New();
	this->Slabs->SetViewport(0.5,0,1,1);
	this->Slabs->SetBackground(1, 1, 1);

	this->ComponentGraphRenderer =  vtkSmartPointer<vtkRenderer>::New();
	this->ComponentGraphRenderer->SetViewport(0, 0, 0.5, 0.5);
	this->ComponentGraphRenderer->SetBackground(1,1,1);

	this->Slabs->AddActor(this->SelectedActor);
	this->Slabs->AddActor(this->CubeAxesActor);

	this->Graph->AddActor(this->NodeActor);
	this->Graph->AddActor(this->EdgeActor);

	this->ComponentGraphRenderer->AddActor(this->CGNodeActor);
	this->ComponentGraphRenderer->AddActor(this->CGEdgeActor);

	this->StartPickCallback=vtkSmartPointer<vtkCallbackCommand>::New();
	this->StartPickCallback->SetCallback(StartPickCallbackFunction);
	this->EndPickCallback=vtkSmartPointer<vtkCallbackCommand>::New();
	this->EndPickCallback->SetCallback(EndPickCallbackFunction);

	this->DataRange = vtkSmartPointer<vtkDoubleArray>::New();
	this->GraphLayout = vtkSmartPointer<vtkGraphLayout>::New();
	this->LayoutStrategy = vtkSmartPointer<vtkOGDFLayoutStrategy>::New();
	this->ComponentGraphLayout = vtkSmartPointer<vtkGraphLayout>::New();

	this->PickEvent = 0;
	this->ColourMap = false;
	this->ShowBoundary = false;
	this->ShowMDRG = false;
	this->ShowInter = false;
	this->IsInitialRender = true;
	this->GlobalMax = 0;
	this->GlobalMin = 0;
	this->DisplayCoord = vtkSmartPointer<vtkCoordinate>::New();
	this->DisplayCoord->SetCoordinateSystemToWorld();
	this->JCNLayout = vtkSmartPointer<vtkJCNLayoutStrategy>::New();
	this->ShowInterMDRG = false;
	this->GlyphInterval = 0.2;
	this->Fields = std::vector<char*>();
	this->ColourField = 0;
	this->SubGraph = false;
	this->LineOpacity = 0.8;
	this->Threshold = 0;
	this->RenderRS = false;
	this->AndOrRelation = 1;
}

vtkJCNMouseInteractorStyle::~vtkJCNMouseInteractorStyle() 
{    
	if (this->GraphLayout)
	{
		this->GraphLayout = NULL;
	}

	if (this->Grid)
	{
		this->Grid = NULL;
	}

	while (this->Fields.size() > 0)
	{
		delete [] this->Fields.back();
		this->Fields.pop_back();
	}
	this->Fields.clear();
}

// Initialise all of the data structures and rendering primitives. 
int vtkJCNMouseInteractorStyle::Initialize()
{
	if (!this->OutputGraph)
	{
		vtkErrorMacro("A jcnGraph object is required.");
		return 0;
	}
	if (!this->Grid)
	{
		vtkErrorMacro("A unstructuredgrid is required.");
		return 0;
	}
	if (this->FieldNr == 0)
	{
		vtkErrorMacro("A dim is required.");
		return 0;
	}
	if (this->Fields.size() == 0)
	{
		vtkErrorMacro("Fields name is required");
		return 0;
	}
	if (!this->MDRGList)
	{
		vtkErrorMacro("Boundary Jacobi Set required");
		return 0;
	}

	if (!this->ComponentGraph && this->RenderRS)
	{
		vtkErrorMacro("A componentGraph is required.");
		return 0;
	}
	if (this->ComponentArray.size()==0 && this->RenderRS)
	{
		vtkErrorMacro("A component graph array is required.");
		return 0;
	}
	// Number of vertex in the graph
	this->VertexNr = this->OutputGraph->GetNumberOfVertices();

	// Data Range for each field
	this->DataRange->SetNumberOfComponents(2);
	this->DataRange->SetNumberOfTuples(this->FieldNr);
	if (this->RenderRS)
	{
		this->Graph->SetViewport(0,0.5,0.5,1);
	}
	// We could apply a logarithmic scale on the scalar data for colour mapping.
	for (int i = 0; i < this->Fields.size();i++)
	{
		float minValue;
		float maxValue;
		for (int j = 0; j < this->OutputGraph->GetVertexData()->GetArray(this->Fields[i])->GetNumberOfTuples();j++)
		{
			float temp = this->OutputGraph->GetVertexData()->GetArray(this->Fields[i])->GetComponent(j,0);
			if (j == 0)
			{
				minValue = temp;
				maxValue = temp;
			}
			else
			{
				if (temp < minValue)
				{
					minValue = temp;
				}
				if (temp > maxValue)
				{
					maxValue = temp;
				}
			}
		}
		this->DataRange->SetComponent(i,0,minValue);
		this->DataRange->SetComponent(i,1,maxValue);
		this->ThresholdField.push_back(minValue);
		this->ThresholdField.push_back(maxValue);
	}

	// Set up graph layout strategy
	this->LayoutStrategy->SetLayoutModuleByName("fmmm");
	this->LayoutStrategy->LayoutEdgesOff();

	this->GraphLayout->SetLayoutStrategy(this->LayoutStrategy);
	this->GraphLayout->SetInputData(this->OutputGraph);
	this->GraphLayout->Update();

	if (this->RenderRS)
	{
		this->ComponentGraphLayout->SetLayoutStrategy(this->LayoutStrategy);
		this->ComponentGraphLayout->SetInputData(this->ComponentGraph);
		this->ComponentGraphLayout->Update();

		int vertexNr = this->ComponentGraph->GetNumberOfVertices();
		this->SelectComponentGraphBool->SetNumberOfTuples(vertexNr);
		for (int i = 0; i < vertexNr; i++)
		{
			this->SelectComponentGraphBool->SetValue(i,0);
		}
	}

	//Filter for brushing graph nodes
	this->SelectGraphBool->SetNumberOfTuples(this->VertexNr);
	for (int i = 0; i < this->VertexNr; i++)
	{
		this->SelectGraphBool->SetValue(i,0);
	}

	//Boundary graph and slab ids
	vtkBitArray *array = vtkBitArray::SafeDownCast(this->OutputGraph->GetVertexData()->GetArray("Boundary"));
	if (array->GetNumberOfTuples() == this->VertexNr)
	{
		for (int i = 0; i < this->VertexNr; i++)
		{
			if (array->GetValue(i))
			{
				this->BoundaryIds->InsertNextValue(i);
			}
			else
			{
				this->InterIds->InsertNextValue(i);
			}
		}
	}

	//MDRG graph and slab ids
	if (this->FieldNr < 4)
	{
		bool isNotBoundary;
		int num = 0;
		for (int i = 0; i < this->VertexNr; i++)
		{
			if (this->MDRGList->GetComponent(i,0))
			{
				num++;
				isNotBoundary = true;
				this->MDRGIds->InsertNextValue(i);
				for (int p = 0; p < this->BoundaryIds->GetNumberOfTuples(); p++)
				{
					if (i == this->BoundaryIds->GetValue(p))
					{
						isNotBoundary = false;
						break;
					}
				}
				if (isNotBoundary)
				{
					this->InterMDRGIds->InsertNextValue(i);
				}
			}
		}
	}

	return 1;
}

void vtkJCNMouseInteractorStyle::OnLeftButtonDown() 
{
	vtkInteractorStyleRubberBandPick::OnLeftButtonDown();
}


void vtkJCNMouseInteractorStyle::OnMiddleButtonDown() 
{
	vtkInteractorStyleRubberBandPick::OnMiddleButtonUp();
}


void vtkJCNMouseInteractorStyle::OnRightButtonDown() 
{
	vtkInteractorStyleRubberBandPick::OnRightButtonDown();
	for (int i = 0; i < this->VertexNr; i++)
	{
		this->SelectGraphBool->SetValue(i,0);
	}

	for (int i = 0; i < this->FieldNr; i++)
	{
		this->ThresholdField[i*2] = this->DataRange->GetComponent(i,0);
		this->ThresholdField[i*2+1] = this->DataRange->GetComponent(i,1);
	}

	if (this->RenderRS)
	{
		for (int i = 0; i < this->ComponentGraph->GetNumberOfVertices(); i++)
		{
			this->SelectComponentGraphBool->SetValue(i,0);
		}
	}
	this->ColourMap = false;
	this->ShowBoundary = false;
	this->ShowMDRG = false;
	this->ShowInterMDRG = false;
	this->SubGraph = false;
	this->ShowInter = false;
	this->Threshold = 0;
	this->UpdateSelectIds();
	this->UpdateGraph();
	if (this->RenderRS)
	{
		this->UpdateComponentGraph();
	}
	this->UpdateSlabs();
	this->UpdateRenderWindow();
}


void vtkJCNMouseInteractorStyle::OnMouseMove()
{
	vtkInteractorStyleRubberBandPick::OnMouseMove();
}    


void vtkJCNMouseInteractorStyle::UpdateRenderWindow()
{
	// If is the first time rendering, then remove the
	// old renderers and replace the new one that we
	// can manipulate in the interaction.
	if (this->IsInitialRender)
	{
		vtkCollectionSimpleIterator si;
		vtkRendererCollection  *rs = this->Interactor->GetRenderWindow()->GetRenderers();
		vtkRenderer *tempRender = rs->GetNextItem();
		while (tempRender)
		{
			if (this->Interactor->GetRenderWindow()->HasRenderer(tempRender))
			{
				this->Interactor->GetRenderWindow()->RemoveRenderer(tempRender);
			}
			tempRender = rs->GetNextItem();
		}
		this->Interactor->GetRenderWindow()->AddRenderer(this->Graph);
		this->Interactor->GetRenderWindow()->AddRenderer(this->Slabs);
		if (this->RenderRS)
		{
			this->Interactor->GetRenderWindow()->AddRenderer(this->ComponentGraphRenderer);
		}
		this->IsInitialRender= false;
	}
	this->Interactor->GetRenderWindow()->Render();
}

// Update selected id list. 
void vtkJCNMouseInteractorStyle::UpdateSelectIds()
{
	vtkBitArray *array = vtkBitArray::SafeDownCast(this->OutputGraph->GetVertexData()->GetArray("Boundary"));
	this->SelectIds->Initialize();

	if (this->RenderRS)
	{
		this->SelectCGraphIds->Initialize();
		int CGVertexNr = this->ComponentGraph->GetNumberOfVertices();
		for (int i = 0; i < CGVertexNr;i++)
		{
			if (this->SelectComponentGraphBool->GetValue(i))
			{
				int myIndex = this->ComponentGraph->GetVertexData()->GetArray("compId")->GetComponent(i,0);
				this->SelectCGraphIds->InsertNextValue(i);
				for (int j = 0; j < this->ComponentArray[myIndex].size(); j++)
				{
					int index = this->ComponentArray[myIndex][j];
					this->SelectIds->InsertNextValue(index);
				}
			}
		}
	}

	for (int i = 0; i < this->VertexNr; i++)
	{
		if (this->SelectGraphBool->GetValue(i))
		{
			this->SelectIds->InsertNextValue(i);
		}
	}

	auto func_print_selection = [&]()
	{
		auto func_print_skeleton_selection = [&]()
		{
			auto count_selected = [&]()
			{
				auto count = 0;

				for (auto i = 0; i < this->SelectComponentGraphBool->GetNumberOfTuples(); ++i)
				{
					if (this->SelectComponentGraphBool->GetValue(i) == 1) ++count;
				}
				return count;
			};

			auto selected_count = count_selected();
			cout << "Selected Reeb skeleton vertices (" << selected_count << ") = { ";
			auto comma = false;
			for (auto i = 0; i < this->SelectComponentGraphBool->GetNumberOfTuples(); ++i)
			{
				//	Check the node is selected
				if (this->SelectComponentGraphBool->GetValue(i) == 1)
				{
					//	Look up it's actual ID
					//int myIndex = this->ComponentGraph->GetVertexData()->GetArray("compId")->GetComponent(i,0);
					if (comma) cout << ", ";
					cout << i;

					comma = true;
				}
			}
			cout << " }" << endl;

			auto vertex_data = this->ComponentGraph->GetVertexData();
			//	Test this is actually the correct structure
			assert(vertex_data->HasArray("persistence"));
			assert(vertex_data->HasArray("volume"));

			auto tot_persistence = 0.0f;
			auto min_persistence = numeric_limits<float>::infinity();
			auto max_persistence = -numeric_limits<float>::infinity();

			cout << "Persistence of selected Reeb skeleton vertices = { ";
			comma = false;
			for (auto i = 0; i < this->SelectComponentGraphBool->GetNumberOfTuples(); ++i)
			{
				//	Check the node is selected
				if (this->SelectComponentGraphBool->GetValue(i) == 1)
				{
					//	Look up it's actual ID
					//int myIndex = this->ComponentGraph->GetVertexData()->GetArray("compId")->GetComponent(i,0);
					if (comma) cout << ", ";
					auto persistence = vertex_data->GetArray("persistence")->GetTuple(i)[0];
					cout << persistence;

					//	Update stats
					tot_persistence += persistence;
					if (persistence > max_persistence)
						max_persistence = persistence;
					if (persistence < min_persistence)
						min_persistence = persistence;

					//	No comma next time round
					comma = true;
				}
			}
			cout << " }" << endl;
			cout << "Total persistence of selected Reeb skeleton vertices = "
				 << tot_persistence << "." << endl;
			cout << "Average persistence of selected Reeb skeleton vertices = "
				 << (tot_persistence / static_cast<float>(selected_count))
				 << "." << endl;
			cout << "Minimum persistence of selected Reeb skeleton vertices = "
				 << min_persistence << "." << endl;
			cout << "Maximum persistence of selected Reeb skeleton vertices = "
				 << max_persistence << "." << endl;

			auto tot_volume = 0.0f;
			cout << "Volume of selected Reeb skeleton vertices = { ";
			comma = false;
			for (auto i = 0; i < this->SelectComponentGraphBool->GetNumberOfTuples(); ++i)
			{
				//	Check the node is selected
				if (this->SelectComponentGraphBool->GetValue(i) == 1)
				{
					//	Look up it's actual ID
					//int myIndex = this->ComponentGraph->GetVertexData()->GetArray("compId")->GetComponent(i,0);
					if (comma) cout << ", ";
					cout << vertex_data->GetArray("volume")->GetTuple(i)[0];
					tot_volume += vertex_data->GetArray("volume")->GetTuple(i)[0];

					comma = true;
				}
			}
			cout << " }" << endl;
			cout << "Total volume of selected Reeb skeleton vertices = "
				 << tot_volume << "." << endl;
		};

		auto func_print_jcn_selection = [&]()
		{
			//auto volume_total = 0.0f;
			//auto persistence_total = 0.0f;

			auto vertex_count = this->SelectIds->GetNumberOfTuples();
			cout << "Selected JCN vertices ("
				 << this->SelectIds->GetNumberOfTuples() << ") = { ";
			auto comma = false;
			for (auto i = 0; i < vertex_count; ++i)
			{
				if (comma) cout << ", ";
				cout << this->SelectIds->GetValue(i);
				comma = true;
			}
			cout << " }" << endl;

			//			auto slabs = Grid;
			//			assert(slabs != nullptr);

			//			cout << "Selected slab volumes ("
			//				 << vertex_count << ") = { ";
			//			comma = false;
			//			for (auto i = 0; i < vertex_count; ++i)
			//			{
			//				if (comma) cout << ", ";
			//				auto this_slab = slabs->GetCell(i);
			//				assert(this_slab != nullptr);
			//				auto volume = calculate_volume(this_slab);
			//				volume_total += volume;

			//				cout << volume;
			//				comma = true;
			//			}
			//			cout << " }" << endl;

			//cout << "Persistence measure of selected JCN vertices = " << persistence_total << "." << endl;
			//cout << "Volume measure of selected JCN vertices = " << volume_total << "." << endl;
		};

		if (this->ComponentGraph != nullptr) func_print_skeleton_selection();
		func_print_jcn_selection();
	};
	func_print_selection();
}

// Update the graph rendering. 
void vtkJCNMouseInteractorStyle::UpdateGraph()
{
	vtkDataSetAttributes *inPD = this->OutputGraph->GetVertexData();
	vtkSmartPointer<vtkFilterColour> filter = vtkSmartPointer<vtkFilterColour>::New();
	filter->SetFieldNr(this->FieldNr);
	filter->SetVertexNr(this->VertexNr);
	filter->SetFieldName(this->Fields);
	filter->Initialize();
	filter->SetInputConnection(this->GraphLayout->GetOutputPort());

	vtkSmartPointer<vtkSelectionNode> selectionNode = vtkSmartPointer<vtkSelectionNode>::New();
	selectionNode->SetFieldType(vtkSelectionNode::VERTEX);
	selectionNode->SetContentType(vtkSelectionNode::INDICES);

	if (this->ShowBoundary)
	{
		filter->SetSelectIds(this->BoundaryIds,1);
		if (this->SubGraph && this->BoundaryIds->GetNumberOfTuples() > 0)
		{
			selectionNode->SetSelectionList(this->BoundaryIds);
		}
	}
	else if (this->ShowMDRG)
	{
		filter->SetSelectIds(this->MDRGIds,1);
		if (this->SubGraph && this->MDRGIds->GetNumberOfTuples() > 0)
		{
			selectionNode->SetSelectionList(this->MDRGIds);
		}
	}
	else if (this->ShowInterMDRG)
	{
		filter->SetSelectIds(this->InterMDRGIds,1);
		if (this->SubGraph && this->InterMDRGIds->GetNumberOfTuples() > 0)
		{
			selectionNode->SetSelectionList(this->InterMDRGIds);
		}
	}
	else if (this->ShowInter)
	{
		filter->SetSelectIds(this->InterIds,2);
		if (this->SubGraph && this->InterIds->GetNumberOfTuples() > 0)
		{
			selectionNode->SetSelectionList(this->InterIds);
		}
	}
	else
	{
		vtkSmartPointer<vtkIdTypeArray> selId = vtkSmartPointer<vtkIdTypeArray>::New();
		for (int i = 0; i < this->SelectIds->GetNumberOfTuples(); i++)
		{
			int index = this->SelectIds->GetComponent(i,0);
			double size = this->OutputGraph->GetVertexData()->GetArray("size")->GetComponent(index,0);
			double *range = this->OutputGraph->GetVertexData()->GetArray("size")->GetRange();
			double val = (range[1]-range[0]) > 0 ? (size-range[0]) / (range[1] -range[0]) : 0;
			bool inRange;
			if (this->AndOrRelation)
			{
				inRange = true;
			}
			else
			{
				inRange = false;
			}
			for (int m = 0; m < this->FieldNr; m++)
			{
				char *fieldName = this->Fields[m];
				double currValue = this->OutputGraph->GetVertexData()->GetArray(fieldName)->GetComponent(index,0);
				double min = this->ThresholdField[m*2];
				double max =  this->ThresholdField[m*2+1];

				if (this->AndOrRelation)
				{
					// And relationship
					if (currValue < min || currValue > max)
					{
						inRange = false;
						break;
					}
				}
				else
				{
					// Or relationship
					if (currValue <= max && currValue >= min)
					{
						inRange = true;
						break;
					}
				}
			}
			if (inRange)
			{
				selId->InsertNextValue(index);
			}
		}
		if (this->SubGraph ) selectionNode->SetSelectionList(selId);
		filter->SetSelectIds(selId,2);
		this->SelectIds = selId;
	}

	vtkSmartPointer<vtkSelection> selection = vtkSmartPointer<vtkSelection>::New();
	selection->AddNode(selectionNode);

	vtkSmartPointer<vtkExtractSelectedGraph> extractSelection = vtkSmartPointer<vtkExtractSelectedGraph>::New();
	extractSelection->SetInputData(0, this->GraphLayout->GetOutput());
	extractSelection->SetInputData(1, selection);
	extractSelection->Update();

	int extractNr = extractSelection->GetOutput()->GetNumberOfVertices();
	vtkSmartPointer<vtkJCNMergeFields> merge = vtkSmartPointer<vtkJCNMergeFields>::New();

	if (this->ColourMap)
	{
		if (this->SubGraph && extractNr > 0)
		{
			merge->SetInputConnection(extractSelection->GetOutputPort());
		}
		else
		{
			merge->SetInputConnection(this->GraphLayout->GetOutputPort());
		}
	}
	else
	{
		for (int i = 0; i < extractNr; i++)
		{
			for (int j = 0; j < this->FieldNr; j++)
			{
				extractSelection->GetOutput()->GetVertexData()->GetArray(
							this->Fields[j])->SetComponent(i,0,2);
			}
		}
		if (this->SubGraph && extractNr > 0)
		{
			merge->SetInputConnection(extractSelection->GetOutputPort());
		}
		else
		{
			merge->SetInputConnection(filter->GetOutputPort());
		}
	}

	char *fieldName = this->Fields[this->ColourField];
	merge->SetOutputField("merged fields", merge->VERTEX_DATA);
	merge->SetNumberOfComponents(1);
	merge->Merge(0,fieldName,0);

	vtkSmartPointer<vtkAssignAttribute> assGraph = vtkSmartPointer<vtkAssignAttribute>::New();
	assGraph->SetInputConnection(merge->GetOutputPort());
	assGraph->Assign("merged fields", "SCALARS", "VERTEX_DATA");

	vtkSmartPointer<vtkBillboardMultivarGlyphs> glyphs = vtkSmartPointer<vtkBillboardMultivarGlyphs>::New();
	glyphs->SetInputConnection(assGraph->GetOutputPort());
	glyphs->SetScale(this->GlyphScale);
	glyphs->SetSmoothness(20);
	glyphs->SetCamera(this->Graph->GetActiveCamera());
	glyphs->SetLogOn();
	glyphs->Update();

	vtkSmartPointer<vtkLookupTable> col = vtkSmartPointer<vtkLookupTable>::New();
	double min = this->DataRange->GetComponent(this->ColourField, 0);
	double max = this->DataRange->GetComponent(this->ColourField, 1);
	if (this->ColourMap)
	{
		col->SetTableRange(min,max);
		col->SetNumberOfColors(200);
		col->SetHueRange(0.66667, 0.0);
		col->SetSaturationRange(1,1);
		col->SetValueRange(1,1);
		col->SetAlphaRange(1,1);
	}
	else
	{
		col->SetTableRange(0,3);
		col->SetNumberOfColors(3);
		col->SetTableValue(0, 1.0, 1.0, 1.0, 0.4);
		col->SetTableValue(1, 1.0, 0.0, 0.0, 1.0);
		col->SetTableValue(2, 0.0, 0.0, 1.0, 1.0);
	}
	col->Build();


	if (this->ColourMap)
	{
		vtkSmartPointer<vtkApplyColors> apply = vtkSmartPointer<vtkApplyColors>::New();
		apply->SetInputConnection(glyphs->GetOutputPort());
		apply->SetCellLookupTable(col);
		apply->UseCellLookupTableOn();
		apply->ScaleCellLookupTableOff();
		apply->SetInputArrayToProcess(1, 0, 0, vtkDataObject::FIELD_ASSOCIATION_CELLS, "vtkBillboardMG values");
		apply->SetSelectedCellOpacity(1);
		this->NodeMap->SetInputConnection(apply->GetOutputPort());
		this->NodeMap->SelectColorArray("vtkApplyColors color");
	}
	else
	{
		vtkSmartPointer<vtkApplyColors> apply = vtkSmartPointer<vtkApplyColors>::New();
		apply->SetInputConnection(glyphs->GetOutputPort());
		apply->SetCellLookupTable(col);
		apply->UseCellLookupTableOn();
		apply->ScaleCellLookupTableOff();
		apply->SetInputArrayToProcess(1, 0, 0, vtkDataObject::FIELD_ASSOCIATION_CELLS, "vtkBillboardMG values");
		apply->SetSelectedCellOpacity(1);
		this->NodeMap->SetInputConnection(apply->GetOutputPort());
		this->NodeMap->SelectColorArray("vtkApplyColors color");
	}

	this->NodeMap->SetScalarModeToUseCellFieldData();
	this->NodeMap->SetScalarVisibility(true);
	this->NodeActor->SetMapper(this->NodeMap);
	this->NodeActor->GetProperty()->EdgeVisibilityOn();
	this->NodeActor->GetProperty()->SetEdgeColor(0.8,0.8,0.8);
	this->NodeActor->GetProperty()->SetOpacity(1);

	vtkSmartPointer<vtkGraphToPolyData> graphPD = vtkSmartPointer<vtkGraphToPolyData>::New();
	if (this->SubGraph &&  extractNr > 0)
	{
		graphPD->SetInputConnection(extractSelection->GetOutputPort());
	}
	else
	{
		graphPD->SetInputConnection(this->GraphLayout->GetOutputPort());
	}



	this->EdgeMap->SetInputConnection(graphPD->GetOutputPort());
	this->EdgeMap->ScalarVisibilityOff();

	this->EdgeActor->SetMapper(this->EdgeMap);
	this->EdgeActor->GetProperty()->SetColor(0, 0, 0);
	this->EdgeActor->GetProperty()->SetOpacity(this->LineOpacity);

	this->Graph->RemoveActor(this->NodeActor);
	this->Graph->RemoveActor(this->EdgeActor);
	this->Graph->AddActor(this->NodeActor);
	this->Graph->AddActor(this->EdgeActor);
	if (this->IsInitialRender)
	{
		this->Graph->ResetCamera();
	}
}

// Update the Skeleton graph rendering. 
void vtkJCNMouseInteractorStyle::UpdateComponentGraph()
{   
	vtkSmartPointer<vtkFilterColour> filter = vtkSmartPointer<vtkFilterColour>::New();
	filter->SetFieldNr(this->FieldNr);
	filter->SetVertexNr(this->ComponentGraph->GetNumberOfVertices());
	filter->SetFieldName(this->Fields);
	filter->Initialize();
	filter->SetInputConnection(this->ComponentGraphLayout->GetOutputPort());
	filter->SetSelectIds(this->SelectCGraphIds,3);

	vtkSmartPointer<vtkJCNMergeFields> merge = vtkSmartPointer<vtkJCNMergeFields>::New();
	if (this->ColourMap)
	{
		merge->SetInputConnection(this->ComponentGraphLayout->GetOutputPort());
	}
	else
	{
		merge->SetInputConnection(filter->GetOutputPort());
	}
	merge->SetOutputField("merged fields", merge->VERTEX_DATA);
	merge->SetNumberOfComponents(this->FieldNr);

	for (int i = 0; i < this->FieldNr; i++)
	{
		merge->Merge(i,this->Fields[i],0);
	}

	vtkSmartPointer<vtkAssignAttribute> assGraph = vtkSmartPointer<vtkAssignAttribute>::New();
	assGraph->SetInputConnection(merge->GetOutputPort());
	assGraph->Assign("merged fields", "SCALARS", "VERTEX_DATA");

	vtkSmartPointer<vtkBillboardMultivarGlyphs> glyphs = vtkSmartPointer<vtkBillboardMultivarGlyphs>::New();
	glyphs->SetInputConnection(assGraph->GetOutputPort());
	glyphs->SetScale(this->GlyphScaleRS);
	glyphs->SetSmoothness(20);
	glyphs->SetCamera(this->ComponentGraphRenderer->GetActiveCamera());
	glyphs->Update();

	vtkSmartPointer<vtkLookupTable> col = vtkSmartPointer<vtkLookupTable>::New();
	col->SetTableRange(0,3);
	col->SetNumberOfColors(4);
	col->SetTableValue(0, 1.0, 1.0, 1.0, 1.0);
	col->SetTableValue(1, 1.0, 0.0, 0.0, 1.0);
	col->SetTableValue(2, 0.0, 0.0, 1.0, 1.0);
	col->SetTableValue(3, 0.0, 1.0, 0.0, 1.0);
	vtkSmartPointer<vtkApplyColors> apply = vtkSmartPointer<vtkApplyColors>::New();
	apply->SetInputConnection(glyphs->GetOutputPort());
	apply->SetCellLookupTable(col);
	apply->UseCellLookupTableOn();
	apply->ScaleCellLookupTableOff();
	apply->SetInputArrayToProcess(1, 0, 0, vtkDataObject::FIELD_ASSOCIATION_CELLS, "vtkBillboardMG values");
	apply->SetSelectedCellOpacity(1);

	vtkSmartPointer<vtkPolyDataMapper> cNodeMap = vtkSmartPointer<vtkPolyDataMapper>::New();
	cNodeMap->SetInputConnection(apply->GetOutputPort());
	cNodeMap->SelectColorArray("vtkApplyColors color");
	cNodeMap->SetScalarModeToUseCellFieldData();
	cNodeMap->SetScalarVisibility(true);

	this->CGNodeActor->SetMapper(cNodeMap);
	this->CGNodeActor->GetProperty()->EdgeVisibilityOn();
	this->CGNodeActor->GetProperty()->SetEdgeColor(0,0,0);

	vtkSmartPointer<vtkGraphToPolyData> graphPD = vtkSmartPointer<vtkGraphToPolyData>::New();
	graphPD->SetInputConnection(this->ComponentGraphLayout->GetOutputPort());

	vtkSmartPointer<vtkPolyDataMapper> cEdgeMap = vtkSmartPointer<vtkPolyDataMapper>::New();
	cEdgeMap->SetInputConnection(graphPD->GetOutputPort());
	cEdgeMap->ScalarVisibilityOff();

	this->CGEdgeActor->SetMapper(cEdgeMap);
	this->CGEdgeActor->GetProperty()->SetColor(0, 0, 0);
	this->CGEdgeActor->GetProperty()->SetOpacity(0.2);

	this->ComponentGraphRenderer->RemoveActor(this->CGNodeActor);
	this->ComponentGraphRenderer->RemoveActor(this->CGEdgeActor);
	this->ComponentGraphRenderer->AddActor(this->CGNodeActor);
	this->ComponentGraphRenderer->AddActor(this->CGEdgeActor);
	if (this->IsInitialRender)
	{
		this->ComponentGraphRenderer->ResetCamera();
	}
}

// Update the slab renderer. 
void vtkJCNMouseInteractorStyle::UpdateSlabs()
{
	this->Slabs->RemoveActor(this->SelectedActor);
	this->Slabs->RemoveActor(this->CubeAxesActor);
	//bounding box
	this->CubeAxesActor->SetBounds(this->Grid->GetBounds());
	this->CubeAxesActor->SetCamera(this->Slabs->GetActiveCamera());
	this->CubeAxesActor->GetXAxesLinesProperty()->SetColor(0,0,0);
	this->CubeAxesActor->GetYAxesLinesProperty()->SetColor(0,0,0);
	this->CubeAxesActor->GetZAxesLinesProperty()->SetColor(0,0,0);
	this->CubeAxesActor->SetXAxisLabelVisibility(1);
	this->CubeAxesActor->SetYAxisLabelVisibility(1);
	this->CubeAxesActor->SetZAxisLabelVisibility(1);
	this->CubeAxesActor->GetTitleTextProperty(0)->SetColor(0.0,0.0,0.0);
	this->CubeAxesActor->GetTitleTextProperty(1)->SetColor(0.0,0.0,0.0);
	this->CubeAxesActor->GetTitleTextProperty(2)->SetColor(0.0,0.0,0.0);
	this->Slabs->AddActor(this->CubeAxesActor);

	if (this->IsInitialRender)
	{
		//this->Slabs->ResetCamera();
		auto camera = this->Slabs->GetActiveCamera();
		camera->SetPosition(-40, 30, 40);
		//this->Grid->GetBounds()[1] / 2.0
		camera->SetFocalPoint(this->Grid->GetBounds()[1] / 2.0,
				this->Grid->GetBounds()[3] / 2.0,
				this->Grid->GetBounds()[5] / 2.0);
	}

	if (this->SelectIds->GetNumberOfTuples() > 0
			|| this->ShowBoundary || this->ShowMDRG  || this->ShowInterMDRG || this->ShowInter)
	{
		vtkSmartPointer<vtkSelectionNode> selectionNode = vtkSmartPointer<vtkSelectionNode>::New();
		selectionNode->SetFieldType(vtkSelectionNode::CELL);
		selectionNode->SetContentType(vtkSelectionNode::INDICES);
		if (this->ShowBoundary)
		{
			selectionNode->SetSelectionList(this->BoundaryIds);
		}
		else if (this->ShowMDRG)
		{
			selectionNode->SetSelectionList(this->MDRGIds);
		}
		else if (this->ShowInterMDRG)
		{
			vtkIdTypeArray *jointArray = vtkIdTypeArray::New();
			for (int i = 0;i < this->InterMDRGIds->GetNumberOfTuples();i++)
			{
				jointArray->InsertNextValue(this->InterMDRGIds->GetValue(i));
			}
			selectionNode->SetSelectionList(jointArray);
			jointArray->Delete();
		}
		else if (this->ShowInter)
		{
			vtkIdTypeArray *jointArray = vtkIdTypeArray::New();
			for (int i = 0;i < this->InterIds->GetNumberOfTuples();i++)
			{
				jointArray->InsertNextValue(this->InterIds->GetValue(i));
			}
			selectionNode->SetSelectionList(jointArray);
			jointArray->Delete();
		}
		else
		{
			selectionNode->SetSelectionList(this->SelectIds);
		}

		vtkSmartPointer<vtkSelection> selection = vtkSmartPointer<vtkSelection>::New();
		selection->AddNode(selectionNode);

		vtkSmartPointer<vtkExtractSelection> extractSelection = vtkSmartPointer<vtkExtractSelection>::New();
		extractSelection->SetInputData(0, this->Grid);
		extractSelection->SetInputData(1, selection);
		extractSelection->Update();

		// In selection
		vtkSmartPointer<vtkUnstructuredGrid> selected = vtkSmartPointer<vtkUnstructuredGrid>::New();
		selected->ShallowCopy(extractSelection->GetOutput());

		vtkSmartPointer<vtkAssignAttribute> ass = vtkSmartPointer<vtkAssignAttribute>::New();

		if (this->TwoD)
		{
			vtkSmartPointer<vtkGeometryFilter> geom = vtkSmartPointer<vtkGeometryFilter>::New();
			geom->SetInputData(selected);

			vtkSmartPointer<vtkTriangleFilter> tri =  vtkSmartPointer<vtkTriangleFilter>::New();
			tri->SetInputConnection(geom->GetOutputPort());
			ass->SetInputConnection(tri->GetOutputPort());
		}
		else
		{
			vtkSmartPointer<vtkPolyhedralMeshToPolyData> geom = vtkSmartPointer<vtkPolyhedralMeshToPolyData>::New();
			geom->SetInputData(selected);
			ass->SetInputConnection(geom->GetOutputPort());
		}

		char *fieldName = this->Fields[this->ColourField];
		ass->Assign(fieldName, "SCALARS", "POINT_DATA");

		double min = this->Grid->GetPointData()->GetArray(fieldName)->GetRange()[0];
		double max = this->Grid->GetPointData()->GetArray(fieldName)->GetRange()[1];

		vtkSmartPointer<vtkLookupTable> col = vtkSmartPointer<vtkLookupTable>::New();
		col->SetTableRange(min, max);
		col->SetNumberOfColors(2000);
		col->SetHueRange(0.667,0);
		col->SetSaturationRange(1,1);
		col->SetValueRange(1,1);
		col->SetAlphaRange(1,1);

		this->SelectedMapper->SetInputConnection(ass->GetOutputPort());
		this->SelectedMapper->SetColorModeToMapScalars();
		this->SelectedMapper->SetScalarModeToUsePointData();
		this->SelectedMapper->SetLookupTable(col);
		this->SelectedMapper->ScalarVisibilityOn();
		this->SelectedMapper->UseLookupTableScalarRangeOn();
		this->SelectedActor->SetMapper(this->SelectedMapper);
		this->Slabs->AddActor(this->SelectedActor);
	}

}

// Selection event defined here. 
void vtkJCNMouseInteractorStyle::OnLeftButtonUp()
{
	// Forward events
	vtkInteractorStyleRubberBandPick::OnLeftButtonUp();
	InvokeEvent(vtkCommand::EndPickEvent);
	InvokeEvent(vtkCommand::SelectionChangedEvent);
	bool inRange;
	if (this->PickEvent % 2 > 0)
	{
		int startX = this->StartPosition[0];
		int startY = this->StartPosition[1];
		int endX = this->EndPosition[0];
		int endY = this->EndPosition[1];
		int xRange, yRange;
		xRange = MaxInt(startX,endX) - MinInt(startX,endX);
		yRange = MaxInt(startY,endY) - MinInt(startY,endY);

		if (this->RenderRS)
		{
			inRange =  xRange > 0 && yRange > 0
					&& startX >= this->Graph->GetOrigin()[0]
					&& startX <= this->Graph->GetOrigin()[0] + this->Graph->GetSize()[0]
					&& startY <= this->Graph->GetOrigin()[1]*2
					&& startY >= this->Graph->GetOrigin()[1]*2 - this->Graph->GetSize()[1]
					&& endX >= this->Graph->GetOrigin()[0]
					&& endX <= this->Graph->GetOrigin()[0] + this->Graph->GetSize()[0]
					&& endY <= this->Graph->GetOrigin()[1]*2
					&& endY >= this->Graph->GetOrigin()[1]*2 - this->Graph->GetSize()[1];

		}
		else
		{
			inRange = true;
		}

		if (this->OutputGraph && inRange)
		{
			int *display = nullptr;
			bool select = false;
			vtkPoints *nodePosition = this->GraphLayout->GetOutput()->GetPoints();
			for (int i = 0; i < this->VertexNr; i++)
			{
				double nodeX = nodePosition->GetPoint(i)[0];
				double nodeY = nodePosition->GetPoint(i)[1];
				double nodeZ = nodePosition->GetPoint(i)[2];
				this->DisplayCoord->SetValue(nodeX,nodeY,nodeZ);
				if (this->IsInitialRender)
				{
					display = this->DisplayCoord->GetComputedDisplayValue(
								this->Interactor->GetRenderWindow()->GetRenderers()->GetFirstRenderer());
				}
				else
				{
					display =this->DisplayCoord->GetComputedDisplayValue(this->Graph);
				}
				nodeX = display[0];
				nodeY = display[1];

				if (nodeX >= MinInt(startX,endX) && nodeX <= MaxInt(startX,endX)
						&& nodeY >= MinInt(startY,endY) && nodeY <= MaxInt(startY,endY))
				{
					this->SelectGraphBool->SetValue(i,1);
					select = true;
				}
			}

			if (display)
			{
				display = NULL;
			}

			if (select)
			{
				this->ColourMap = false;
				this->ShowBoundary = false;
				this->ShowMDRG = false;
				this->ShowInterMDRG = false;
				this->UpdateSelectIds();
				this->UpdateGraph();
				if (this->RenderRS)
				{
					this->UpdateComponentGraph();
				}
				this->UpdateRenderWindow();
			}
		}

		if (this->RenderRS)
		{
			if (this->ComponentGraph && xRange > 0 && yRange > 0
					&& startX >= this->ComponentGraphRenderer->GetOrigin()[0]
					&& startX <= this->ComponentGraphRenderer->GetOrigin()[0] + this->ComponentGraphRenderer->GetSize()[0]
					&& startY >= this->ComponentGraphRenderer->GetOrigin()[1]
					&& startY <= this->ComponentGraphRenderer->GetOrigin()[1] + this->ComponentGraphRenderer->GetSize()[1]
					&& endX >= this->ComponentGraphRenderer->GetOrigin()[0]
					&& endX <= this->ComponentGraphRenderer->GetOrigin()[0] + this->ComponentGraphRenderer->GetSize()[0]
					&& endY >= this->ComponentGraphRenderer->GetOrigin()[1]
					&& endY <= this->ComponentGraphRenderer->GetOrigin()[1] + this->ComponentGraphRenderer->GetSize()[1])
			{
				int *display = nullptr;
				bool select = false;
				vtkPoints *nodePosition = this->ComponentGraphLayout->GetOutput()->GetPoints();
				for (int i = 0; i < this->ComponentGraph->GetNumberOfVertices(); i++)
				{
					double nodeX = nodePosition->GetPoint(i)[0];
					double nodeY = nodePosition->GetPoint(i)[1];
					double nodeZ = nodePosition->GetPoint(i)[2];
					this->DisplayCoord->SetValue(nodeX,nodeY,nodeZ);
					if (this->IsInitialRender)
					{
						display = this->DisplayCoord->GetComputedDisplayValue(
									this->Interactor->GetRenderWindow()->GetRenderers()->GetFirstRenderer());
					}
					else
					{
						display =this->DisplayCoord->GetComputedDisplayValue(this->ComponentGraphRenderer);
					}
					nodeX = display[0];
					nodeY = display[1];

					if (nodeX >= MinInt(startX,endX) && nodeX <= MaxInt(startX,endX)
							&& nodeY >= MinInt(startY,endY) && nodeY <= MaxInt(startY,endY))
					{
						this->SelectComponentGraphBool->SetValue(i,1);
						select = true;
					}
				}

				if (display)
				{
					display = NULL;
				}

				if (select)
				{
					this->ColourMap = false;
					this->ShowBoundary = false;
					this->ShowMDRG = false;
					this->ShowInterMDRG = false;
					this->ShowInter = false;
					this->UpdateSelectIds();
					this->UpdateGraph();
					this->UpdateComponentGraph();
					this->UpdateRenderWindow();
				}
			}
		}
	}

}

// User options defined here. 
void vtkJCNMouseInteractorStyle::OnKeyPress()
{ 
	const char TAB = '\t';
	// Forward events
	vtkInteractorStyleRubberBandPick::OnKeyPress();
	// Get the keypress
	vtkRenderWindowInteractor *rwi = this->Interactor;
	std::string key = rwi->GetKeySym();

	auto func_set_layout_by_name = [&](const std::string& name)
	{
		//	Change the layout module
		this->LayoutStrategy->SetLayoutModuleByName(name.c_str());
		this->LayoutStrategy->Layout();

		//	Reset glyph size to something predictable
		this->GlyphScale = 4;

		//	Set the next layout and redraw graph etc.
		//	We'll set the layout to null first, then set it to the desired layout
		//	to trick vtk into redrawing the graph
		this->GraphLayout->SetLayoutStrategy(nullptr);
		//this->GraphLayout->Update();
		this->GraphLayout->SetLayoutStrategy(this->LayoutStrategy);
		this->GraphLayout->Update();

		this->Graph->ResetCamera();
		this->ComponentGraphRenderer->ResetCamera();
		this->UpdateGraph();
		if (this->IsInitialRender)
		{
			if (this->RenderRS)
			{
				this->UpdateComponentGraph();
			}
			this->UpdateSlabs();
		}
		this->UpdateRenderWindow();
	};

	//	Print command list
	if (key == "h" || key == "H")
	{
		std::cout << "Interactor keys" << std::endl;
		std::cout << "r" << TAB << TAB << "Toggle rotate / selection mode" << endl;
		std::cout << "c" << TAB << TAB << "Toggle colour mapping mode" << endl;
		std::cout << "b" << TAB << TAB << "Toggle boundary highlighting" << endl;
		std::cout << "v" << TAB << TAB << "Jacobi Structure nodes in the JCN" << endl;
		std::cout << "g" << TAB << TAB << "Toggle geometry" << endl;
		std::cout << "up/down" << TAB << "Increase/decrese glyph size" << endl;
		std::cout << "left/right" << TAB << "Increase/decrese line opacity" << endl;
		std::cout << "z / x" << TAB << "Increase/Decrease the node size of Reeb Skeleton graph." << endl;
		std::cout << "d" << TAB << "Change the current field. Can be used to change the colour mapping of geometry" << endl;
		std::cout << "y / t" << TAB << "Increase/Decrease the filter out the selected graph nodes according"
									   "to its size which is measured by the number of fragments" << endl;
		std::cout << "j" << TAB << "JCN domain-specific layout" << endl;
		std::cout << "f" << TAB << "Force-directed layout" << endl;
		std::cout << "i" << TAB << "Nodes of the Jacobi Structure interior of the domain" << endl;
		std::cout << "p" << TAB << "Nodes of JCN interior in the domain" << endl;
		std::cout << "s" << TAB << "Subgraph of selected JCN" << endl;
		std::cout << "o" << TAB << "Render original graph" << endl;
		std::cout << "l / k" << TAB << "decrease / increase min" << endl;
		std::cout << "u" << TAB << "toggle and / or filter" << endl;

		std::cout << "F1" << TAB << "Set graph layout to 'fmmm'." << endl;
		std::cout << "F2" << TAB << "Set graph layout to 'mmmexamplefast'." << endl;
		std::cout << "F3" << TAB << "Set graph layout to 'mmmexamplenice'." << endl;
		std::cout << "F4" << TAB << "Set graph layout to 'mmmexamplenotwist'." << endl;

		std::cout << "F7" << TAB << "Prev graph layout." << endl;
		std::cout << "F8" << TAB << "Next graph layout." << endl;

		std::cout << "F12" << TAB << "Save graph layout to PDF."  << endl;

	}
	// r: Selection Mode
	else if (key == "r" || key=="R")
	{
		this->PickEvent++;
		if ( this->PickEvent > 2)
		{
			this->PickEvent -= 2;
		}
		if (this->PickEvent % 2 > 0)
		{
			std::cout << "Enter Selection Mode" << std::endl;
		}
		else
		{
			std::cout << "Leave Selection Mode" << std::endl;
		}
		if (this->IsInitialRender)
		{
			this->UpdateGraph();
			if (this->RenderRS)
			{
				this->UpdateComponentGraph();
			}
			this->UpdateSlabs();
			this->UpdateRenderWindow();
		}
	}
	// c: Color mapping of the JCN
	else if (key == "c" || key=="C")
	{
		this->ColourMap = true;
		this->UpdateGraph();
		if (this->RenderRS)
		{
			this->UpdateComponentGraph();
		}
		if (this->IsInitialRender)
		{
			this->UpdateSlabs();
		}
		this->UpdateRenderWindow();
	}

	// b: JCN nodes touching the boundary in Domain
	else if (key == "b" || key=="B")
	{
		this->ColourMap = false;
		this->ShowBoundary = true;
		this->ShowMDRG = false;
		this->ShowInterMDRG = false;
		this->ShowInter = false;
		this->UpdateGraph();
		if (this->IsInitialRender)
		{
			if (this->RenderRS)
			{
				this->UpdateComponentGraph();
			}
			this->UpdateSlabs();
		}
		this->UpdateRenderWindow();
	}

	// v: Jacobi Structure nodes in the JCN
	else if (key == "v" || key=="V")
	{
		this->ShowMDRG = true;
		this->ColourMap = false;
		this->ShowBoundary = false;
		this->ShowInterMDRG = false;
		this->ShowInter = false;
		this->UpdateGraph();
		if (this->IsInitialRender)
		{
			if (this->RenderRS)
			{
				this->UpdateComponentGraph();
			}
			this->UpdateSlabs();
		}
		this->UpdateRenderWindow();
	}

	// g: render geometry.
	else if (key == "g" || key=="G")
	{
		this->UpdateSlabs();
		this->UpdateGraph();
		if (this->IsInitialRender)
		{
			if (this->RenderRS)
			{
				this->UpdateComponentGraph();
			}
		}
		this->UpdateRenderWindow();
	}

	// Up/Down: Increase glyph-size of the JCN and Reeb Skeleton
	else if (key=="Up")
	{
		this->GlyphScale+=this->GlyphInterval*this->GlyphScale;
		this->UpdateGraph();
		if (IsInitialRender)
		{
			if (this->RenderRS)
			{
				this->UpdateComponentGraph();
			}
			this->UpdateSlabs();
		}
		this->UpdateRenderWindow();

		std::cout << "Glyph scale is now: " << this->GlyphScale << std::endl;
	}

	// Up/Down: Increase glyph-size of the JCN and Reeb Skeleton
	else if (key=="Down")
	{
		this->GlyphScale -= this->GlyphInterval*this->GlyphScale;
		this->GlyphScale = this->GlyphScale > 0  ? this->GlyphScale : 1;
		this->UpdateGraph();
		if (IsInitialRender)
		{
			if (this->RenderRS)
			{
				this->UpdateComponentGraph();
			}
			this->UpdateSlabs();
		}
		this->UpdateRenderWindow();

		std::cout << "Glyph scale is now: " << this->GlyphScale << std::endl;
	}

	// Right/Left: Increase/Decrease the graph edge opacity.
	else if (key=="Right")
	{
		if (this->LineOpacity < 1.0)
		{
			this->LineOpacity += 0.05;
			this->LineOpacity = this->LineOpacity < 1 ? this->LineOpacity : 1;
			cout << "Line opacity is now: " << this->LineOpacity << endl;
			this->UpdateGraph();
			if (IsInitialRender)
			{
				if (this->RenderRS)
				{
					this->UpdateComponentGraph();
				}
				this->UpdateSlabs();
			}
			this->UpdateRenderWindow();
		}
	}

	// Right/Left: Increase/Decrease the graph edge opacity.
	else if (key=="Left")
	{
		if (this->LineOpacity > 0.02)
		{
			this->LineOpacity -= 0.05;
			this->LineOpacity = this->LineOpacity > 0 ? this->LineOpacity : 0.02;
			cout << "Line opacity is now: " << this->LineOpacity << endl;
			this->UpdateGraph();
			if (IsInitialRender)
			{
				if (this->RenderRS)
				{
					this->UpdateComponentGraph();
				}
				this->UpdateSlabs();
			}
			this->UpdateRenderWindow();
		}
	}
	// z/x : Increase/Decrease the node size of Reeb Skeleton graph.

	else if (key=="z" || key=="Z")
	{
		if (this->RenderRS)
		{
			this->GlyphScaleRS -= this->GlyphInterval*this->GlyphScaleRS;
			this->GlyphScaleRS = this->GlyphScaleRS > 0  ? this->GlyphScaleRS : 1;
			cout << "Reeb graph glyph scale: " << this->GlyphScaleRS << endl;
			this->UpdateComponentGraph();
			if (IsInitialRender)
			{
				this->UpdateSlabs();
				this->UpdateGraph();
			}
			this->UpdateRenderWindow();
		}
	}

	// z/x : Increase/Decrease the node size of Reeb Skeleton graph.
	else if (key=="x" || key=="X")
	{
		if (this->RenderRS)
		{
			this->GlyphScaleRS += this->GlyphInterval*this->GlyphScaleRS;
			cout << "Reeb graph glyph scale: " << this->GlyphScaleRS << endl;
			this->UpdateComponentGraph();
			if (IsInitialRender)
			{
				this->UpdateSlabs();
				this->UpdateGraph();
			}
			this->UpdateRenderWindow();
		}
	}
	// d: Change the current field. Can be used to change the colour mapping of geometry.
	else if (key=="d" || key=="D")
	{
		this->ColourField++;
		if (this->ColourField > this->Fields.size()-1 )
		{
			this->ColourField = 0;
		}
		std::cout << this->Fields[ColourField] << std::endl;

		this->UpdateGraph();
		this->UpdateRenderWindow();
	}

	// y/t: Filter out the selected graph nodes according to its size which is measured by the number of
	//      fragments.  "y" to increase the threshold and "t" to decrease.
	else if (key=="y" || key == "Y")
	{
		if (!IsInitialRender)
		{
			this->Threshold += 0.005;
			this->Threshold = this->Threshold < 1 ? this->Threshold : 1;
			std::cout << "Threshold: " << this->Threshold << std::endl;
			this->SubGraph = true;
			this->UpdateGraph();
			this->UpdateRenderWindow();
		}
	}

	// y/t: Filter out the selected graph nodes according to its size which is measured by the number of
	//      fragments.  "y" to increase the threshold and "t" to decrease.
	else if (key=="t" || key == "T")
	{
		if (!IsInitialRender)
		{
			this->Threshold -= 0.005;
			this->Threshold = this->Threshold > 0 ? this->Threshold : 0;
			std::cout << "Threshold: " << this->Threshold << std::endl;
			this->SubGraph = true;
			this->UpdateGraph();
			this->UpdateRenderWindow();
		}
	}

	// j: JCN domain-specific layout
	else if (key == "j" || key == "J")
	{
		this->GlyphScale = 0.1;

		//	Set the next layout and redraw graph etc.
		//	We'll set the layout to null first, then set it to the desired layout
		//	to trick vtk into redrawing the graph
		this->GraphLayout->SetLayoutStrategy(nullptr);
		//this->GraphLayout->Update();
		this->GraphLayout->SetLayoutStrategy(this->JCNLayout);
		this->GraphLayout->Update();
		this->UpdateGraph();
		this->Graph->ResetCamera();
		this->ComponentGraphRenderer->ResetCamera();
		if (this->IsInitialRender)
		{
			if (this->RenderRS)
			{
				this->UpdateComponentGraph();
			}
			this->UpdateSlabs();
		}
		this->UpdateRenderWindow();
	}

	// f: Force-directed layout
	else if (key == "f" || key == "F")
	{
		this->GlyphScale = 4;

		//	Set the next layout and redraw graph etc.
		//	We'll set the layout to null first, then set it to the desired layout
		//	to trick vtk into redrawing the graph
		this->GraphLayout->SetLayoutStrategy(nullptr);
		//this->GraphLayout->Update();
		this->GraphLayout->SetLayoutStrategy(this->LayoutStrategy);
		this->GraphLayout->Update();
		this->Graph->ResetCamera();
		this->ComponentGraphRenderer->ResetCamera();
		this->UpdateGraph();
		if (this->IsInitialRender)
		{
			if (this->RenderRS)
			{
				this->UpdateComponentGraph();
			}
			this->UpdateSlabs();
		}
		this->UpdateRenderWindow();
	}

	// i: Nodes of the Jacobi Structure interior of the domain
	else if (key == "i" || key == "I")
	{
		this->ShowMDRG = false;
		this->ColourMap = false;
		this->ShowBoundary = false;
		this->ShowInterMDRG = true;
		this->ShowInter = false;
		this->UpdateGraph();
		if (this->IsInitialRender)
		{
			if (this->RenderRS)
			{
				this->UpdateComponentGraph();
			}
			this->UpdateSlabs();
		}
		this->UpdateRenderWindow();
	}

	// l: Nodes of JCN interior in the domain
	else if (key == "p" || key =="P")
	{
		this->ShowMDRG = false;
		this->ColourMap = false;
		this->ShowBoundary = false;
		this->ShowInterMDRG = false;
		this->ShowInter = true;
		this->UpdateGraph();
		if (this->IsInitialRender)
		{
			if (this->RenderRS)
			{
				this->UpdateComponentGraph();
			}
			this->UpdateSlabs();
		}
		this->UpdateRenderWindow();
	}

	// s: Subgraph of selected JCN
	else if (key == "s" || key=="S")
	{
		if (!IsInitialRender)
		{
			this->SubGraph = true;
			this->UpdateGraph();
			this->UpdateRenderWindow();
		}
	}

	// o: Render original graph
	else if (key == "o" || key == "O")
	{
		this->SubGraph = false;
		this->UpdateGraph();
		this->UpdateRenderWindow();
	}

	// increase min
	else if (key=="k")
	{
		if (!IsInitialRender)
		{
			double th = this->ThresholdField[this->ColourField*2];
			double range = this->DataRange->GetComponent(this->ColourField,1) - this->DataRange->GetComponent(this->ColourField,0);
			th += range * 0.01;
			th = th < this->ThresholdField[this->ColourField*2+1] ? th : this->ThresholdField[this->ColourField*2+1] ;
			this->ThresholdField[this->ColourField*2] = th;
			std::cout << "MinThres " << this->ThresholdField[ColourField*2] << "  "
					  << "MaxThres " << this->ThresholdField[ColourField*2+1]
					  << std::endl;

			this->UpdateGraph();
			this->SubGraph = true;
			this->UpdateRenderWindow();
		}
	}

	// decrease min
	else if (key=="l")
	{
		if (!IsInitialRender)
		{
			double th = this->ThresholdField[this->ColourField*2+1];
			double range = this->DataRange->GetComponent(this->ColourField,1) - this->DataRange->GetComponent(this->ColourField,0);
			th -= range * 0.01;
			th = th > this->ThresholdField[this->ColourField*2] ? th : this->ThresholdField[this->ColourField*2];
			this->ThresholdField[this->ColourField*2+1] = th;
			std::cout << "MinThres " << this->ThresholdField[ColourField*2] << "  "
					  << "MaxThres " << this->ThresholdField[ColourField*2+1]
					  << std::endl;
			this->SubGraph = true;
			this->UpdateGraph();
			this->UpdateRenderWindow();
		}
	}

	else if (key=="u")
	{
		for (int i = 0; i < this->FieldNr; i++)
		{
			this->ThresholdField[i*2] = this->DataRange->GetComponent(i,0);
			this->ThresholdField[i*2+1] = this->DataRange->GetComponent(i,1);
		}
		if (this->AndOrRelation)
		{
			this->AndOrRelation = 0;
			std::cout << "Or Filter" << std::endl;
		}
		else
		{
			this->AndOrRelation = 1;
			std::cout << "And Relation" << std::endl;
		}
	}
	else if (key=="F1")
	{
		cout << "Setting graph layout to " << "fmmm" << "." << endl;
		func_set_layout_by_name("fmmm");
	}
	else if (key=="F2")
	{
		cout << "Setting graph layout to " << "mmmexamplefast" << "." << endl;
		func_set_layout_by_name("mmmexamplefast");
	}
	else if (key=="F3")
	{
		cout << "Setting graph layout to " << "mmmexamplenice" << "." << endl;
		func_set_layout_by_name("mmmexamplenice");
	}
	else if (key=="F4")
	{
		cout << "Setting graph layout to " << "mmmexamplenotwist" << "." << endl;
		func_set_layout_by_name("mmmexamplenotwist");
	}
	else if (key=="F7")
	{
		//	Next layout style
		//	Determine current layout
		auto current_layout = m_graph_layouts.find(m_current_layout_name);
		assert(current_layout != m_graph_layouts.cend());
		cout << "Current layout is " << *current_layout << "." << endl;
		//cout << "Next layout is " << *(current_layout++) << "." << endl;

		//	Decrement to the next layout or loop to start
		auto next_layout = --current_layout;
		if (next_layout == m_graph_layouts.cend())
			next_layout = --m_graph_layouts.cend();

		//	Extract layout name from iterator
		m_current_layout_name = *next_layout;
		cout << "Setting graph layout to " << m_current_layout_name << "." << endl;

		//	Update the view
		func_set_layout_by_name(m_current_layout_name);
	}
	else if (key=="F8")
	{
		//	Next layout style
		//	Determine current layout
		auto current_layout = m_graph_layouts.find(m_current_layout_name);
		assert(current_layout != m_graph_layouts.cend());
		cout << "Current layout is " << *current_layout << "." << endl;
		//cout << "Next layout is " << *(current_layout++) << "." << endl;

		//	Increment to the next layout or loop to start
		auto next_layout = ++current_layout;
		if (next_layout == m_graph_layouts.cend())
			next_layout = m_graph_layouts.cbegin();

		//	Extract layout name from iterator
		m_current_layout_name = *next_layout;
		cout << "Setting graph layout to " << m_current_layout_name << "." << endl;

		//	Update the view
		func_set_layout_by_name(m_current_layout_name);
	}
	else if (key=="F12")
	{
		static size_t ScreenshotNumber;


		auto render_window = rwi->GetRenderWindow();
		assert(render_window != nullptr);

		//	Make sure the window is updated
		render_window->Render();

		//	Save as a PDF
		auto pdf = vtkSmartPointer<vtkGL2PSExporter>::New();
		pdf->SetRenderWindow(render_window);
		pdf->SetFileFormatToPDF();
		pdf->CompressOff();
		pdf->SetFilePrefix(("screenshot_" + std::to_string(ScreenshotNumber)).c_str());
		pdf->Update();
		pdf->Write();

		cout << "Output saved to file: '" << pdf->GetFilePrefix() << ".pdf" << "'." <<endl;
		++ScreenshotNumber;
	}
	else
	{
		std::cout << "User pressed the '" << key << "' this is unbound to an action." << endl;
	}
}

void vtkJCNMouseInteractorStyle::SetGraph(vtkGraph *graph)
{
	this->OutputGraph = graph;
}

void vtkJCNMouseInteractorStyle::SetGrid(vtkUnstructuredGrid *grid)
{
	this->Grid = grid;

}

void vtkJCNMouseInteractorStyle::SetMetric(vtkSimplifyMetric *metric)
{
	this->ComponentGraph = metric->GetOutput();
	this->ComponentArray = metric->GetJCNList();
}

void vtkJCNMouseInteractorStyle::SetJacobiSet(vtkDataArray *array)
{
	this->MDRGList = array;
}


void vtkJCNMouseInteractorStyle::AddField(const char *nm)
{
	char *f = new char[strlen(nm) + 1];
	strcpy(f, nm);
	this->Fields.push_back(f);
}
