/*=========================================================================

 *	File: vtkJCNWithGeometry.cxx
 *	Graph visualization library for VTK
 * 
 *	This software is distributed WITHOUT ANY WARRANTY; 
 *	without even the implied warranty of MERCHANTABILITY 
 *	or FITNESS FOR A PARTICULAR PURPOSE.  
 * 
 *	See the file copyright.txt for details.  

=========================================================================*/

#include "vtkAdjacentVertexIterator.h"
#include "vtkBitArray.h"
#include "vtkCell.h"
#include "vtkCellData.h"
#include "vtkDataSetAttributes.h"
#include "vtkDoubleArray.h"
#include "vtkExecutive.h"
#include "vtkIdList.h"
#include "vtkInformation.h"
#include "vtkInformationVector.h"
#include "vtkIdTypeArray.h"
#include "vtkJCNWithGeometry.h"
#include "vtkMergePoints.h"
#include "vtkMath.h"
#include "vtkMutableUndirectedGraph.h"
#include "vtkObjectFactory.h"
#include "vtkPointData.h"
#include "vtkPolyData.h"
#include "vtkUnstructuredGrid.h"
#include <iostream>
#include <algorithm>

vtkStandardNewMacro(vtkJCNWithGeometry);



typedef std::vector<vtkSmartPointer<vtkIdList> > *Polytope;


vtkJCNWithGeometry::vtkJCNWithGeometry()
{
  //this->GetInformation()->Set(vtkAlgorithm::PRESERVES_RANGES(), 1);
  this->CenterFirstSlab = false;
  this->Dimensionality = 0;
  this->Fields = std::vector<Field*>();
  this->UF = vtkSmartPointer<vtkIdTypeArray>::New();
  this->Epsilon = 1.0e-6;
  this->SetNumberOfOutputPorts(3);
  
  vtkUndirectedGraph *output1 = vtkUndirectedGraph::New();
  this->GetExecutive()->SetOutputData(0, output1);
  output1->Delete();

  vtkUnstructuredGrid *output2 = vtkUnstructuredGrid::New();
  this->GetExecutive()->SetOutputData(1, output2);
  output2->Delete();
}

vtkJCNWithGeometry::~vtkJCNWithGeometry()
{
  while (this->Fields.size() > 0)
    {
    this->Fields.pop_back();
    }
  this->Fields.clear();
  
}


void vtkJCNWithGeometry::AddField(const char *nm, double wd)
{
  Field *f = new Field();
  f->name = new char[strlen(nm) + 1];
  strcpy(f->name, nm);
  f->slabWidth = wd;
  
  this->Fields.push_back(f);
}

_Field* vtkJCNWithGeometry::GetFieldData(const size_t index) const
{
	assert(index < this->Fields.size());

	return this->Fields[index];
}


void vtkJCNWithGeometry::AddFieldWithBounds(const char *nm, double wd, double minB, double maxB)
{
  Field *f = new Field();
  f->name = new char[strlen(nm) + 1];
  strcpy(f->name, nm);
  f->slabWidth = wd;
  f->isBounded = true;
  f->minBound = minB;
  f->maxBound = maxB;
  this->Fields.push_back(f);
}


char * vtkJCNWithGeometry::GetField(int n)
{
  if (n < this->Fields.size())
    {
    return this->Fields[n]->name;
    }
  else
    {
    vtkWarningMacro("Attempting to access non-existent field");
    return NULL;
    }
}    



int vtkJCNWithGeometry::GetNumberOfFields()
{
  return this->Fields.size();
}


void vtkJCNWithGeometry::ClearFields()
{
  this->Fields.clear();
}


void vtkJCNWithGeometry::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os,indent);
  os << indent << "Field names: ";
  if (this->Fields.size() < 1) 
    {
    os << "none defined." << endl;
    }
  else
    {
    os << endl;
    for (std::vector<Field*>::iterator it = this->Fields.begin();
        it != this->Fields.end();
        it++)
      {
      os << indent.GetNextIndent() << ((*it)->name);
      os << ", width " << (*it)->slabWidth << endl;
      }
    }
}


int vtkJCNWithGeometry::FillInputPortInformation(int, vtkInformation* info)
{
  // This filter uses the vtkDataSet cell traversal methods so it
  // suppors any data set type as input.
  info->Set(vtkAlgorithm::INPUT_REQUIRED_DATA_TYPE(), "vtkDataSet");
  return 1;
}


int vtkJCNWithGeometry::FillOutputPortInformation(int port, vtkInformation* info)
{
  // Port 1 = optional unstructured grid of slabs.
  if (port == 0)
    {
    info->Set(vtkDataObject::DATA_TYPE_NAME(), "vtkUndirectedGraph");
    return 1;
    }
  if (port == 1)
    {
    info->Set(vtkDataObject::DATA_TYPE_NAME(), "vtkUnstructuredGrid");
    return 1;
    }
  if (port == 2)
    {
    info->Set(vtkDataObject::DATA_TYPE_NAME(), "vtkUnstructuredGrid");
    return 1;
    }
  return 0;
}


vtkDataObject* vtkJCNWithGeometry::GetOutput(int index)
{
  vtkDataObject *obj;
  if (index == 0)
    {
    obj = vtkUndirectedGraph::SafeDownCast(this->GetOutputDataObject(0));
    return obj;
    }
  if (index == 1)
    {
    obj = vtkUnstructuredGrid::SafeDownCast(this->GetOutputDataObject(1));
    return obj;
    }
  if (index == 2)
    {
    obj = vtkUnstructuredGrid::SafeDownCast(this->GetOutputDataObject(2));
    return obj;
    }
  return NULL;
}


vtkUndirectedGraph *vtkJCNWithGeometry::GetTopologyGraph()
{
  return vtkUndirectedGraph::SafeDownCast(this->GetOutputDataObject(0));
}


vtkUnstructuredGrid *vtkJCNWithGeometry::GetFragments()
{
  return vtkUnstructuredGrid::SafeDownCast(this->GetOutputDataObject(1));
}


vtkUnstructuredGrid *vtkJCNWithGeometry::GetBoundary()
{
  return vtkUnstructuredGrid::SafeDownCast(this->GetOutputDataObject(2));
}


int vtkJCNWithGeometry::RequestDataObject(
  vtkInformation* request,
  vtkInformationVector** inputVector ,
  vtkInformationVector* outputVector)
{
  vtkInformation* inInfo = inputVector[0]->GetInformationObject(0);
  if (!inInfo)
    {
    return 0;
    }

  vtkUnstructuredGrid *input = vtkUnstructuredGrid::GetData(inInfo);
  if (input)
    {
    vtkInformation* info0 = outputVector->GetInformationObject(0);
    vtkInformation* info1 = outputVector->GetInformationObject(1);
    vtkInformation* info2 = outputVector->GetInformationObject(2);

    vtkUndirectedGraph *output = vtkUndirectedGraph::GetData(outputVector);
    vtkUnstructuredGrid *outputFrags = vtkUnstructuredGrid::GetData(outputVector);
    vtkUnstructuredGrid *outputBoundary = vtkUnstructuredGrid::GetData(outputVector);

    if (!output)
      {
      output = vtkUndirectedGraph::New();
      info0->Set(vtkDataObject::DATA_OBJECT(), output);
      output->Delete();
      }

    if (!outputFrags)
      {
      outputFrags = vtkUnstructuredGrid::New();
      info1->Set(vtkDataObject::DATA_OBJECT(), outputFrags);
      outputFrags->Delete();
      }
   
    if (!outputBoundary)
      {
      outputBoundary = vtkUnstructuredGrid::New();
      info2->Set(vtkDataObject::DATA_OBJECT(), outputBoundary);
      outputBoundary->Delete();
      }
    return 1;
    }
  return 0;
}


vtkIdType vtkJCNWithGeometry::Find(vtkIdType x)
{
  vtkIdType entry = this->UF->GetValue(x);
  if (entry < 0)
    {
    return x;
    }
  else
    {
    vtkIdType root = this->Find(entry);
    this->UF->SetValue(x, root);
    return root;
    }
}



void vtkJCNWithGeometry::FaceCenter(vtkIdList *face, vtkPoints *points, double center[])
{
  int nrPoints = face->GetNumberOfIds();
  double p[3];
  
  center[0] = center[1] = center[2] = 0.0;
  for (int i = 0; i < nrPoints; i++)
    {
    points->GetPoint(face->GetId(i), p);
    for (int j = 0; j < 3; j++) 
      {
      center[j] += p[j];
      }
    }
  for (int j = 0; j < 3; j++) 
    {
    center[j] /= nrPoints;
    }
}



int vtkJCNWithGeometry::RequestData(vtkInformation*,
                                 vtkInformationVector** inputVector,
                                 vtkInformationVector* outputVector)
{
  // get the info objects
  vtkInformation *inInfo = inputVector[0]->GetInformationObject(0);
  vtkInformation *outInfo = outputVector->GetInformationObject(0);
  vtkInformation *outFragInfo;
  vtkInformation *outBoundaryInfo;

  vtkUnstructuredGrid *outFrags;
  vtkUnstructuredGrid *outBoundary;
  vtkPointData *outFragPD, *outBndrPD;
  vtkCellData  *outFragCD, *outBndrCD;
  
  // get the input and output data sets, point & cell data, and points.
  // Input structured data
  vtkUnstructuredGrid *input = vtkUnstructuredGrid::SafeDownCast(
      inInfo->Get(vtkDataObject::DATA_OBJECT()));
  
  // Output graph data
  vtkGraph *output = vtkGraph::SafeDownCast(
      outInfo->Get(vtkDataObject::DATA_OBJECT()));
  
  // Input and output point data attribute. 
  // vtkDataAttributes: Both of vtkPointData and vtkCellData are subclasses of vtkDataAttributes ( which is a subclass of
  // vtkFieldData ) and have nearly identical interfaces. All datasets have attribute data (both cell and point), so all
  // datasets' attribute data respond to these methods.
  vtkDataSetAttributes *inPD   = input->GetPointData(), 
                       *outPD  = output->GetVertexData();
  vtkDataSetAttributes *inCD   = input->GetCellData(),  
                       *outCD  = output->GetEdgeData();
  
  // Get points coordinates with point ID. 
  vtkPoints    *points = input->GetPoints();

  //vtkIdType is the integer type used for point and cell identification.
  //This type may be either 32-bit or 64-bit, depending on whether VTK_USE_64BIT_IDS is defined.
  vtkIdType numCells = input->GetNumberOfCells();
  vtkIdType numPts   = input->GetNumberOfPoints();
   
   // get the dataset statistics and check it is non-pathological.
  if(numCells < 1 || numPts < 1)
    {
    vtkDebugMacro("No input data.");
    return 1;
    }

  // Crude estimate : estimate the number of cells.
  vtkIdType estOutputSize = numCells * 5;

  // Allcoate a rough memory for the output fragment
  outFragInfo = outputVector->GetInformationObject(1);
  outFrags = vtkUnstructuredGrid::SafeDownCast(
        outFragInfo->Get(vtkDataObject::DATA_OBJECT()));
  outFrags->Allocate(estOutputSize);

  outFragPD = outFrags->GetPointData();
  outFragCD = outFrags->GetCellData();
        
  outBoundaryInfo = outputVector->GetInformationObject(2);
  outBoundary = vtkUnstructuredGrid::SafeDownCast(
      outBoundaryInfo->Get(vtkDataObject::DATA_OBJECT()));
  outBoundary->Allocate(estOutputSize);

  outBndrPD = outBoundary->GetPointData();
  outBndrCD = outBoundary->GetCellData();
     
  // Devide the simplicial mesh into four
  int tetTemplate[4][3] = { {0, 1, 2}, {0, 1, 3}, {0, 2, 3}, {1, 2, 3} };

  // Data structures for constructing the graph.
  // Graph structure for the cell
  vtkSmartPointer<vtkMutableUndirectedGraph> cellgraph = vtkSmartPointer<vtkMutableUndirectedGraph>::New();
  
  // Topology graph
  vtkSmartPointer<vtkMutableUndirectedGraph> topograph = vtkSmartPointer<vtkMutableUndirectedGraph>::New();
  // Vertex list of cell
  vtkSmartPointer<vtkIdTypeArray> cellVertex = vtkSmartPointer<vtkIdTypeArray>::New();
  //Adjacent vertex
  vtkSmartPointer<vtkAdjacentVertexIterator> adj = vtkSmartPointer<vtkAdjacentVertexIterator>::New();
  //Center of the fragment
  vtkSmartPointer<vtkDoubleArray> fragCenters = vtkSmartPointer<vtkDoubleArray>::New();
  // List of the fragment sizes
  vtkSmartPointer<vtkIntArray> fragSize = vtkSmartPointer<vtkIntArray>::New();
  // Array of scalar ?
  vtkSmartPointer<vtkDoubleArray> deltaArr = vtkSmartPointer<vtkDoubleArray>::New();
    
  // Set up output data arrays.
  // Have to do this first, as passes through later fields need to
  // access new points created by fragmentation on earlier fields.
  outCD->CopyAllocate(inCD, numPts * 5, numPts);
  outPD->CopyScalarsOn();
  outPD->InterpolateAllocate(input->GetPointData(), numPts*4, numPts, false);

  // Points for the output grid.  Output points will be a superset of the
  // input, so allocate and deep copy the input points.  Output will also
  // have the same bounds as the input.
  vtkIdType ignored;
  vtkPoints *oldPoints = input->GetPoints();
  vtkSmartPointer<vtkPoints> newPoints = vtkSmartPointer<vtkPoints>::New();
      
  // Compute dimensionality of dataset: must be 2D or 3D.
  double bounds[6];
  oldPoints->ComputeBounds();
  oldPoints->GetBounds(bounds);
  this->Dimensionality = 0;
  for (int d = 0; d < 3; d++)
    {
    if (bounds[d*2] != bounds[d*2 + 1])
      {
      this->Dimensionality++;
      }
    }
  if (this->Dimensionality < 2)
    {
    vtkErrorMacro("Dimensionality must be 2 or 3.");
    return 1;
    }

  // Use floats for points to reduce memory o/head:
  // may need to review in case of artefacts.
  
  newPoints->Allocate(estOutputSize, numPts/2);

  vtkSmartPointer<vtkMergePoints> pointLocator = vtkSmartPointer<vtkMergePoints>::New();

  pointLocator->InitPointInsertion (newPoints, input->GetBounds());
  for (int i = 0; i < oldPoints->GetNumberOfPoints(); i++)
     {
     pointLocator->InsertUniquePoint(oldPoints->GetPoint(i), ignored);
     outPD->CopyData(inPD,i,i);
     }
  
  const double dEpsilon = this->Epsilon;
    
  // Collect the multiple scalar fields based on array names,
  // confirming that these fields are present in the input.
  // Collect field ranges for later use.

  if (this->Fields.size() < 1)
     {
       vtkWarningMacro("No array names set for testing slab equality.");
     }
     else {
  
  vtkSmartPointer<vtkDataArray> array;
  for (std::vector<Field *>::iterator it = this->Fields.begin();
       it != this->Fields.end(); it++)
    {
    // Note we get the *output* array, which will include
    // points formed by interpolation.
    array = outPD->GetArray((*it)->name);
    if (array)
      {
      (*it)->slabScalars = array->NewInstance();
      (*it)->nodeScalars = array->NewInstance();
      (*it)->tempScalars = array->NewInstance();
      (*it)->parents     = vtkSmartPointer<vtkIdTypeArray>::New();
      (*it)->slabScalars->UnRegister(this);
      (*it)->tempScalars->UnRegister(this);
      (*it)->nodeScalars->UnRegister(this);
      (*it)->slabScalars->SetName(array->GetName());
      (*it)->nodeScalars->SetName(array->GetName());
      (*it)->pointScalars = array;
      array->GetRange((*it)->range);
      }
    else
      {
      vtkWarningMacro("Array not found in input point data: " << (*it)->name);
      }
    }
  }
          
  vtkDataArray *scalars;
    
  // fragments of current cell: each cell is placed into the inputQ,
  // then fresh fragments from current slice put into outputQ.
  std::vector<Polytope> inputQ  = std::vector<Polytope>();
  std::vector<Polytope> outputQ = std::vector<Polytope>();

  // structure for storing and searching face centers.
  vtkSmartPointer<vtkPoints> centers = vtkSmartPointer<vtkPoints>::New();
  centers->Allocate(estOutputSize, numPts/10);
  vtkSmartPointer<vtkMergePoints> centerLocator = vtkSmartPointer<vtkMergePoints>::New();
  centerLocator->InitPointInsertion(centers, input->GetBounds());

  vtkSmartPointer<vtkIdTypeArray> centerOf = vtkSmartPointer<vtkIdTypeArray>::New();
  centerOf->SetNumberOfComponents(2);

  vtkSmartPointer<vtkIntArray> centerFace;
  centerFace = vtkSmartPointer<vtkIntArray>::New();
  centerFace->SetNumberOfComponents(2);
  vtkSmartPointer<vtkIdTypeArray> boundary;

  boundary = vtkSmartPointer<vtkIdTypeArray>::New();
  boundary->Allocate(estOutputSize, numPts/10);

  // cut-set variables.
  vtkSmartPointer<vtkIdList> cut = vtkSmartPointer<vtkIdList>::New();  
  int nrPoints;  
  double base[3];
  double vec[3];
  double baseNorm;
  double ang;
  vtkIdType pnt, pnt0, pnt1;
  vtkIdType cnr;

  // For computing convex hull for points lying on cutting plane.
  float theta[100];
  vtkIdType index[100];
      
  double maxCell, minCell;
  double fieldRange[2];
  double initThreshold;
  double lastThreshold;

  double minField = 0.0;
  double maxField = 0.0;

  // working variables for walking around a cel face.
  vtkIdType newPointId;
  vtkIdType thisPointId;
  vtkIdType prevPointId;
  int nrFaceIds;
  int mask;

  double thisScalar; 
  double prevScalar; 
  double delta, t, p[3], p0[3], p1[3], p2[3];

  // face lists  
  Polytope fragment = NULL;
  Polytope residual = NULL;
  Polytope working  = NULL;
  
  vtkSmartPointer<vtkIdList> fragmentFace;
  vtkSmartPointer<vtkIdList> residualFace;
  
  // reading/writing cells from input, output and cell arrays.
  vtkSmartPointer<vtkIdList> cell;
  vtkSmartPointer<vtkIdList> face;      
  vtkIdList *facePtr;
  vtkIdType pedigree;

  // computing face centers
  double center[3];
  vtkIdType centerId;
  
  // boundary computation
  vtkIdType faceNr;
  vtkIdType bid;
  vtkIdType cellU, cellV;
  vtkIdType thisNodeIx, nextNodeIx;
  vtkSmartPointer<vtkIdList> facet = vtkSmartPointer<vtkIdList>::New();
  vtkSmartPointer<vtkIdList> line  = vtkSmartPointer<vtkIdList>::New();
  line->SetNumberOfIds(2);
  
  // Post-processing of graph.
  vtkIdType rootU, rootV;
  vtkIdType u, v;
  vtkIdType topoU, topoV;
  bool equal;
  vtkIdType nextVertex;
  vtkIdType nrCellFragments;
  double scalar;
  vtkIdType size;
  vtkIdType eid;
  
  // Track output fragment nr.
  // Should be equal to the cell nr returned if fragments inserted
  // into output unstructured grid.
  vtkIdType fragmentNr = 0;

  centerFace->Resize(100000);
  centerOf->Resize(100000);

  // Global stats
  vtkIdType fragTriCount = 0;
  vtkIdType bordTriCount = 0;


  for (vtkIdType c = 0; c < input->GetNumberOfCells(); c++)
    {    
    // Get the next cell from input, and place into the working queue.  
    // We place into outputQ as each field takes the output of the
    // last step as its input, swapping queues BEFORE processing.

    Polytope ptp = new std::vector<vtkSmartPointer<vtkIdList> >();
    
    outputQ.push_back(ptp);
    
    minCell = maxField;
    maxCell = minField;
    
    cell = vtkSmartPointer<vtkIdList>::New();
    if (this->Dimensionality == 2)
      {
      input->GetCellPoints(c, cell);
      outputQ[0]->push_back(cell);      
      }
    else
      {
      input->GetCellPoints(c, cell);
      for (int fnr = 0; fnr < 4; fnr++)
        {
        face = vtkSmartPointer<vtkIdList>::New();
        face->SetNumberOfIds(3);
        for (int pnr = 0; pnr < 3; pnr++)
          {
          face->SetId(pnr, cell->GetId(tetTemplate[fnr][pnr]));
          }
        outputQ[0]->push_back(face);
        }
      }
      
    for (std::vector<Field*>::iterator it = this->Fields.begin(); it != this->Fields.end(); it++)
      {
      // Swap role of outputQ and inputQ
      vtkstd::swap(outputQ, inputQ);
      outputQ.clear();
    
      scalars  = (*it)->pointScalars;
      minField = (*it)->range[0];
      maxField = (*it)->range[1];

      // Compute bounds on the cell.
      minCell = maxField;
      maxCell = minField;

      for (int pnr = 0; pnr < cell->GetNumberOfIds(); pnr++)
        {
        double fval = scalars->GetComponent(cell->GetId(pnr), 0);
        maxCell = std::max(maxCell, fval);
        minCell = std::min(minCell, fval);
        }


      if ((*it)->isBounded && (*it)->minBound > minField) minField = (*it)->minBound;      
      if ((*it)->isBounded && (*it)->maxBound < maxCell) maxCell = (*it)->maxBound;      

      // In the first interatrion, the initThreshold is minField of first field
      initThreshold = minField + (1 + floor((minCell - minField) / (*it)->slabWidth)) * (*it)->slabWidth;
      
      if (this->CenterFirstSlab)
        {
        initThreshold -= (*it)->slabWidth / 2;
        }

      for (int cp = 0; cp < inputQ.size(); cp++)
        {
        working = inputQ[cp];
        if ((*it)->isBounded && (*it)->minBound > initThreshold)
          {
          lastThreshold = (*it)->minBound;
          }
        else if ((*it)->isBounded && initThreshold > (*it)->maxBound)
          {
          lastThreshold = (*it)->maxBound;
          }
        else
          {
        lastThreshold = initThreshold;
          }

        int abort = 0;

         // Starting from the minField, to the maxField, every time increase by one slabwidth
        for (double threshold = initThreshold; 
            threshold < maxCell; 
            threshold += (*it)->slabWidth)
          {
          
          if ((*it)->isBounded && threshold < (*it)->minBound)
            {
            continue;
            }
          if ((*it)->isBounded && threshold > (*it)->maxBound)
            {
            abort = 1;
            break;
            }

          if (fragment) delete fragment;
          if (residual) delete residual;          
          fragment = new std::vector<vtkSmartPointer<vtkIdList> >();
          residual = new std::vector<vtkSmartPointer<vtkIdList> >();
          // for each slab interval, create a new cut array
          cut = vtkSmartPointer<vtkIdList>::New();

          // Effectively, we start processing a new cell at this point.
      
          // For each FACE of the polyhedron ...
          for (std::vector<vtkSmartPointer<vtkIdList > >::iterator faceIt = working->begin();
              faceIt != working->end();
              faceIt++)
            {
            fragmentFace = vtkSmartPointer<vtkIdList>::New();
            residualFace = vtkSmartPointer<vtkIdList>::New();

            nrFaceIds   = (*faceIt)->GetNumberOfIds();
            prevPointId = (*faceIt)->GetId(nrFaceIds - 1);      
            prevScalar  = scalars->GetComponent(prevPointId, 0);
            // Walk around the face, clipping edges.
    
            for (int i = 0; i < nrFaceIds; i++)
              {
              thisPointId = (*faceIt)->GetId(i);
              thisScalar  = scalars->GetComponent(thisPointId, 0);
           
              // zero bitweise or to any value equals that value 
              // threshold = minField + n * sw
              mask = 0;
              if (threshold - thisScalar > dEpsilon)      mask |= 1;   // this < thr 
              if (thisScalar - threshold > dEpsilon)      mask |= 2;   // this > thr
              if (threshold -  prevScalar > dEpsilon)     mask |= 4;   // prev < thr
              if (prevScalar - threshold > dEpsilon)      mask |= 8;   // prev > thr
              if (fabs(thisScalar-threshold) <= dEpsilon) mask |= 3;   // this==thr
              if (fabs(prevScalar-threshold) <= dEpsilon) mask |= 12;  // prev == thr
            
             
              switch (mask)
                {
                case 3: 
                case 7: 
                case 11:
                case 15:
                  // this ==, don't care on prev.
                  fragmentFace->InsertNextId(thisPointId);
                  residualFace->InsertNextId(thisPointId);
                  if (cut->IsId(thisPointId) < 0)
                    {
                    cut->InsertNextId(thisPointId);
                    }
                  break;
    
                case 5:
                case 13:
                  // this <, prev <
                  fragmentFace->InsertNextId(thisPointId);
                  break;
    
                case 10:
                case 14:
                  // this >, prev >
                  residualFace->InsertNextId(thisPointId);
                  break;
                  
                case 9:
                  // this <, prev >
                  // Moving to this point crosses the threshold hi-lo
                  // Insert interpolated point into both.
                  delta = prevScalar - thisScalar;
                  t = (threshold - thisScalar) / delta;
    
                  //  * PREV ------- T -------- THIS *
                  newPoints->GetPoint(thisPointId, p1);
                  newPoints->GetPoint(prevPointId, p2);
                  for (int j = 0; j < 3; j++)
                    {
                    p[j] = p1[j] + t*(p2[j] - p1[j]);
                    }
    
                  if (pointLocator->InsertUniquePoint(p, newPointId))
                    {
                    outPD->InterpolateEdge(outPD,newPointId,thisPointId, prevPointId,t);
                    }
                  fragmentFace->InsertNextId(newPointId);
                  fragmentFace->InsertNextId(thisPointId);
                  residualFace->InsertNextId(newPointId);

                  // We have found a cut point, add it if first visit.
                  if (cut->IsId(newPointId) < 0)
                    {
                    cut->InsertNextId(newPointId);
                    }                   
               
                  break;
                  
                case 6:
                  // this >, prev <
                  // Moving to this point crosses the threshold lo-hi
                  // Insert interpolated point into both.
                  delta = thisScalar - prevScalar;
                  t = (threshold - prevScalar) / delta;
    
                  newPoints->GetPoint(thisPointId, p1);
                  newPoints->GetPoint(prevPointId, p2);
                  for (int j = 0; j < 3; j++)
                    {
                    p[j] = p2[j] + t*(p1[j] - p2[j]);
                    }
                  if (pointLocator->InsertUniquePoint(p, newPointId))
                    {
                    outPD->InterpolateEdge(outPD,newPointId,prevPointId,thisPointId,t);
                    }
                  fragmentFace->InsertNextId(newPointId);
                  residualFace->InsertNextId(newPointId);
                  residualFace->InsertNextId(thisPointId);
    
                  if (cut->IsId(newPointId) < 0)
                    {
                    cut->InsertNextId(newPointId);
                    }
                  break;
                
                default:
                  vtkErrorMacro("Incomparable scalars " 
                    << prevScalar << ", " 
                    << thisScalar << ", " << threshold);
                } // switch mask

              prevPointId = thisPointId;
              prevScalar = thisScalar;  
              } // for-each edge

            // Output fragment and residue into new cells, as appropriate.
            // Are the fragment and residual faces well defined?
            if (fragmentFace->GetNumberOfIds() > 2)
              {
              fragmentFace->Squeeze();
              fragment->push_back(fragmentFace);
              }
            if (residualFace->GetNumberOfIds() > 2)
              {
              residualFace->Squeeze();
              residual->push_back(residualFace);
              }

            }  // for each face.

          // In 3D, need to compute the face defined by the cut points.
          // We cannot guarantee that points in the cut-list are ordered wrt
          // polygon boundary, so we recompute an order by effectively working
          // the convex hull.
       
          // Iterator through each edge and record for each slabs,  which are the 
          // divided points included. 
            
          if ((this->Dimensionality == 3) && (cut->GetNumberOfIds() > 2))
            {
            nrPoints = cut->GetNumberOfIds();
            pnt0 = cut->GetId(0);
            pnt1 = cut->GetId(1);
             
            // 2.  Compute vector from p[0] to p[1].
            newPoints->GetPoint(pnt0, p0);
            newPoints->GetPoint(pnt1, p1);
            for (int i = 0; i < 3; i++)
              {
              base[i] = p1[i] - p0[i];
              }
            baseNorm = vtkMath::Norm(base);
            theta[0] = 0.0;
            index[0] = pnt1;
            
            // 3.  For the remaining points p, compute angle between p-p0 and base.
            for (int i = 2; i < nrPoints; i++)
              {
              newPoints->GetPoint(cut->GetId(i), p1);
              for (int j = 0; j < 3; j++)
                {
                vec[j] = p1[j] - p0[j];
                }
              theta[i-1] = acos(vtkMath::Dot(base, vec) / (baseNorm * vtkMath::Norm(vec)));
              index[i-1] = cut->GetId(i);
              }

            // 4.  Sort angles.
            for (int j = 1; j < nrPoints-1; j++)
              {
              ang = theta[j];
              pnt = index[j];
              int i = j-1;
              while (i >= 0 && theta[i] > ang)
                {
                theta[i+1] = theta[i];
                index[i+1] = index[i];
                i--;
                }
              theta[i+1] = ang;
              index[i+1] = pnt;
              }

            cut->Reset();
            cut->InsertNextId(pnt0);
            for (int i = 0; i < nrPoints-1; i++)
              {
              cut->InsertNextId(index[i]);
              }
            fragment->push_back(cut);
            residual->push_back(cut);
            }  // Generate cut plane.
      
      
       // STOP on 1st Nov 2013
        // Iterator through each edge and record for each slabs,  which are the 
          // divided points included. 
   
          // OUTPUT PHASE  -----------------------------------------    
          // Have completed traversing each face.
          // Now update output with new fragment.
          // ------------------------------------------------------    

          if (((this->Dimensionality == 2) && (fragment->size() == 1)) ||
              ((this->Dimensionality == 3) && (fragment->size() > 3)))
            {
            cnr = outputQ.size();
            outputQ.push_back(fragment); 

            fragment = NULL;
            
            // set threshold at which this slab was created, and
            // pointer back to parent slab nr.
            (*it)->tempScalars->InsertComponent(cnr, 0, threshold);           
            (*it)->parents->InsertValue(cnr, cp);
            }
          else
            {
            // Remove any partial fragments.
            while (fragment->size() > 0)
              {
              fragment->pop_back();
              }
            }

          // Faces defining the next working polyhedron are in the residual array.
          vtkstd::swap(working, residual);

          // Clear out anything left in residual
          while (residual->size() > 0)
            {
            residual->pop_back();
            }
    
          lastThreshold += (*it)->slabWidth;   //  Track final threshold      
          } // for each threshold
        
      
        // If we are left with a residual, add it as a fragment.
        // However, note that final step in loop is to swap residual
        // with working, so need to look in the working list.
        if (((this->Dimensionality == 2) && (working->size() == 1)) ||
            ((this->Dimensionality == 3) && (working->size() > 3)))
          {
          cnr = outputQ.size();
          outputQ.push_back(working); 
            
          // set threshold at which this slab was created, and
          // pointer back to parent slab nr.
          if ((*it)->isBounded && abort)
            {
            (*it)->tempScalars->InsertComponent(cnr, 0, (*it)->maxBound);
            (*it)->tempScalars->InsertComponent(cnr, 0, lastThreshold);
            }
          else
            {
            if ((*it)->isBounded && lastThreshold > (*it)->maxBound)
              {
              lastThreshold = (*it)->maxBound;
              }
            (*it)->tempScalars->InsertComponent(cnr, 0, lastThreshold);
            }
    
          (*it)->parents->InsertValue(cnr, cp);
          }
        else
          {
          while (working->size() > 0)
            {
            working->pop_back();
            }
          }
        } // for each fragment of cell C: 3D case: faces, edges. CutIds are the edges with current dividing slabs. 
      } // for each field
      

    // OUTPUT PHASE: ----------------------------------------------
    // Output generated fragment into main output dataset.
    // ------------------------------------------------------------
    
    // Copy cell data, patching in slab scalar value for each
    // field.  The last field contains the finest level of
    // fragmentation, we start there and work backward.
    for (vtkIdType co = 0; co < outputQ.size(); co++)
      {
      // Each new fragment added will correspond to a new graph node.
      cellgraph->AddVertex();
      
      if (this->Dimensionality == 2)
        {
        vtkSmartPointer<vtkIdList> face = (*outputQ[co])[0];
        outFrags->InsertNextCell(VTK_POLYGON, face);
        fragTriCount += face->GetNumberOfIds();

        // Find center of each edge
        for (int j = 0; j < face->GetNumberOfIds(); j++)
          {
          vtkIdType vp1 = face->GetId(j);
          vtkIdType vp2 = face->GetId((j + 1) % face->GetNumberOfIds()); 
          
          newPoints->GetPoint(vp1, p1);
          newPoints->GetPoint(vp2, p2);
          for (int k = 0; k < 3; k++) center[k] = (p1[k] + p2[k])/2.0;
          
          if (centerLocator->InsertUniquePoint(center, centerId))
            {
            // First face to have this center, so record for
            // when we find its mate (if any).
            if (2*centerId >= centerOf->GetSize())
              {
              centerOf->Resize(ceil(centerId * 1.2));
              centerFace->Resize(ceil(centerId * 1.2));
              }
            centerOf->InsertComponent(centerId, 0, fragmentNr);
            centerOf->InsertComponent(centerId, 1, -1);
            centerFace->InsertComponent(centerId, 0, j);
            }
          else
            { 
            // seen before, so we know the matching face
            cellgraph->AddEdge(fragmentNr, centerOf->GetComponent(centerId, 0));
            if (2*centerId >= centerOf->GetSize())
              {
              centerOf->Resize(ceil(centerId * 1.2));
              centerFace->Resize(ceil(centerId * 1.2));
              }
            centerOf->InsertComponent(centerId, 1, fragmentNr);
            centerFace->InsertComponent(centerId, 1, j);
            }
          }
        } // 2D case
      else  
        { // 3D case
        vtkSmartPointer<vtkIdList> poly = vtkSmartPointer<vtkIdList>::New();
        poly->InsertNextId(outputQ[co]->size());
        faceNr = 0;
        for (std::vector<vtkSmartPointer<vtkIdList> >::iterator face = outputQ[co]->begin();
            face != outputQ[co]->end();
            face++)
          {
          poly->InsertNextId((*face)->GetNumberOfIds());
          for (int pnr = 0; pnr < (*face)->GetNumberOfIds(); pnr++)
            {
            poly->InsertNextId((*face)->GetId(pnr));
            }
          // Compute center of this face and record.
          this->FaceCenter(*face, newPoints, center);
          if (centerLocator->InsertUniquePoint(center, centerId))
            {
            // First face to have this center, so record for
            // when we find its mate (if any).
            if (2*centerId >= centerOf->GetSize())
              {
              centerOf->Resize(ceil(centerId * 1.2));
              centerFace->Resize(ceil(centerId * 1.2));
              }
            centerOf->InsertComponent(centerId, 0, fragmentNr);
            centerFace->InsertComponent(centerId, 0, faceNr);
            centerOf->InsertComponent(centerId, 1, -1);
            }
          else
            {
            // seen before, so we know the matching face
            cellgraph->AddEdge(fragmentNr, centerOf->GetComponent(centerId, 0));
            if (2*centerId >= centerOf->GetSize())
              {
              centerOf->Resize(ceil(centerId * 1.2));
              centerFace->Resize(ceil(centerId * 1.2));
              }
            centerOf->InsertComponent(centerId, 1, fragmentNr);
            centerFace->InsertComponent(centerId, 1, faceNr);
            }    
          faceNr++;
          }
        outFrags->InsertNextCell(VTK_POLYHEDRON, poly);
        fragTriCount += poly->GetId(0);
        }  

      pedigree = co;
      for (std::vector<Field*>::reverse_iterator it = this->Fields.rbegin();
          it != this->Fields.rend();
          it++)
        {
        (*it)->slabScalars->InsertComponent(fragmentNr, 0, (*it)->tempScalars->GetComponent(pedigree, 0));
        pedigree = (*it)->parents->GetValue(pedigree);
        }
      outCD->CopyData(inCD, c, fragmentNr);
      fragmentNr++;
      
      // Clear faces from current polytope in output queue.
      while (outputQ[co]->size() > 0)
        {
        outputQ[co]->pop_back();
        }
      }    
    while (outputQ.size() > 0)
      {
      Polytope p;
      p = outputQ.back();
      outputQ.pop_back();
      delete p;
      }
    outputQ.clear();
    
      
    } // For each cell C


  // **************************************************************************
  // Traverse the cell graph edges, taking the union
  // of two nodes iff the corresponding scalar values are equal.
  // **************************************************************************

  nrCellFragments = cellgraph->GetNumberOfVertices();

  // Initialize union-find structure.
  this->UF->SetNumberOfTuples(nrCellFragments);
  for (vtkIdType i = 0; i < nrCellFragments; i++)
    {
    this->UF->SetValue(i, -1);
    }
    
  // Find equal regions within dual graph.  
  for (vtkIdType u = 0; u < nrCellFragments; u++)
    {
    cellgraph->GetAdjacentVertices(u, adj);
    while (adj->HasNext())
      {
      v = adj->Next();
      if (this->Fields.size() > 0)
        {
        equal = true;
        for (std::vector<Field*>::iterator it = this->Fields.begin();
            equal && it != this->Fields.end();
            it++)
          {
          equal = (*it)->slabScalars->GetComponent(u,0) 
                  == 
                  (*it)->slabScalars->GetComponent(v,0);
          }
        }
      else
        {
        equal = false;
        }
      if (equal)
        {
        // Merge sets for u and v.
        rootU = this->Find(u);
        rootV = this->Find(v);
        if (rootU != rootV)
          {
          this->UF->SetValue(rootU, rootV);
          }
        } // equal
      } // for each adjacent vertex v
    } // for each vertex u

  // -ve entries in UF mark distinct nodes.
  // Replace markers with encoding of final node ID.
  nextVertex = 0;
  for (vtkIdType i = 0; i < nrCellFragments; i++)
    {
    if (this->UF->GetValue(i) < 0)
      {
      this->UF->SetValue(i, -(1+nextVertex));
      nextVertex++;
      }
    }
  

  // Set up cell vertex table, mapping node ids in full cell
  // graph into node ids in reduced graph.

  cellVertex->SetNumberOfTuples(nrCellFragments);
  cellVertex->SetName("slab id");
  
  for (vtkIdType i = 0; i < nrCellFragments; i++)
    {
    rootU = this->UF->GetValue(i);
    while (rootU >= 0)
      {
      rootU = this->UF->GetValue(rootU);
      }
    cellVertex->SetValue(i, -rootU-1);
    }
  
  // Finally, reconstruct graph using new vertex ids and
  // using field value to determine direction of edges.
  topograph->SetNumberOfVertices(nextVertex);
  for (vtkIdType u = 0; u < nrCellFragments; u++)
    {
    topoU = cellVertex->GetValue(u);
    cellgraph->GetAdjacentVertices(u, adj);
    while (adj->HasNext())
      {
      v = adj->Next();
      topoV = cellVertex->GetValue(v);
      
      if (topoV <= topoU) 
        {
        continue;  // consider each edge between nodes only once.
        }
      if (topograph->GetEdgeId(topoU, topoV) < 0)
        {
        topograph->AddEdge(topoU, topoV);
        }
      }
    }

  // Compute fragment centers.
  const double origin[] = {0.0, 0.0, 0.0};
  // Initialise array for tracking fragment locations
  fragCenters->SetNumberOfComponents(3);
  fragCenters->SetNumberOfTuples(nextVertex);
  fragCenters->SetName("center");
  fragSize->SetNumberOfTuples(nextVertex);
  fragSize->SetName("size");
  
  for (vtkIdType i = 0; i < nextVertex; i++)
    {
    fragSize->SetValue(i, 0);
    fragCenters->SetTupleValue(i, origin);
    }

  for (vtkIdType c = 0; c < centers->GetNumberOfPoints(); c++)
    {
    centers->GetPoint(c, p);
    // Find which cell faces this is a center of ...
    
    u = centerOf->GetComponent(c,0);
    v = centerOf->GetComponent(c,1);
    topoU = cellVertex->GetValue(u);
    for (int j = 0; j < 3; j++)
      {
      fragCenters->SetComponent(topoU, j, fragCenters->GetComponent(topoU, j)+p[j]);
      }      
    fragSize->SetValue(topoU, fragSize->GetValue(topoU)+1);
    if (v < 0)
      {
      continue;
      }
    topoV = cellVertex->GetValue(v);
    if (topograph->GetEdgeId(topoU, topoV) >= 0)
      {
      continue;
      }
    for (int j = 0; j < 3; j++)
      {
      fragCenters->SetComponent(topoV, j, fragCenters->GetComponent(topoV, j)+p[j]);
      }      
    fragSize->SetValue(topoV, fragSize->GetValue(topoV)+1);
    }
  
  // Complete computation of slab centers.
  for (vtkIdType u = 0; u < nextVertex; u++)
    {
    size = fragSize->GetValue(u);
    for (int j = 0; j < 3; j++)
      {
      fragCenters->SetComponent(u, j, fragCenters->GetComponent(u, j) / size);
      }
    }



  vtkIdType cgr_nodes = cellgraph->GetNumberOfVertices();
  vtkIdType cgr_edges = cellgraph->GetNumberOfEdges();
  
  // Remove cellgraph - no longer needed.
  // Compact topograph.
  cellgraph->Initialize();
  topograph->Squeeze();
  
  // Populate node scalar arrays.
  for (std::vector<Field*>::iterator it = this->Fields.begin();
      it != this->Fields.end();
      it++)
    {
    (*it)->nodeScalars->SetNumberOfTuples(nextVertex);
    for (vtkIdType u = 0; u < nrCellFragments; u++)
      {
      topoU = cellVertex->GetComponent(u, 0);
      scalar = (*it)->slabScalars->GetComponent(u, 0);
      (*it)->nodeScalars->InsertComponent(topoU, 0, scalar);
      }
    }
  deltaArr->SetNumberOfTuples(topograph->GetNumberOfEdges());
  deltaArr->SetName("delta");
  
  
  for (vtkIdType e = 0; e < topograph->GetNumberOfEdges(); e++)
    {
    u = topograph->GetSourceVertex(e);
    v = topograph->GetTargetVertex(e);
    delta = 0;
    for (std::vector<Field*>::iterator it = this->Fields.begin();
        it != this->Fields.end();
        it++)
      {
      delta += fabs((*it)->nodeScalars->GetComponent(u,0) * (*it)->nodeScalars->GetComponent(u,0));
      }
    delta = sqrt(delta);
    deltaArr->SetValue(e, delta);
    }
    
  outFrags->SetPoints(newPoints);
  outFrags->BuildLinks();
  for (std::vector<Field*>::iterator it = this->Fields.begin();
      it != this->Fields.end();
      it++)
    {
    outFragCD->AddArray((*it)->slabScalars);
    }
    
 
 
  outFragPD->ShallowCopy(outPD);
  outFragCD->AddArray(cellVertex);
  outFrags->Squeeze();

  vtkIdList *(*borders) = new vtkIdList*[nextVertex];
  
  // Variable holding array is declared at function level as we need to
  // refer to it later at the end of the method for setting up output.
  vtkIdType nrSlabs = 0;
  vtkSmartPointer<vtkBitArray> isOnBoundary = vtkSmartPointer<vtkBitArray>::New();
  // Create array for holding boundary flag.
  isOnBoundary->SetName("Boundary");
  isOnBoundary->SetNumberOfValues(nextVertex);
     
  for (vtkIdType i = 0; i < nextVertex; i++)
    {
    isOnBoundary->SetValue(i, 0);
    borders[i] = vtkIdList::New();
    if (this->Dimensionality == 3)
      {
      // Add counter for number of polygons.
      borders[i]->InsertNextId(0);
      }
    }

  if (this->Dimensionality == 2)
    {
    
    for (vtkIdType i = 0; i < centerOf->GetNumberOfTuples(); i++)
      {
      u = centerOf->GetComponent(i, 0);
      v = centerOf->GetComponent(i, 1);
      cellU = cellVertex->GetValue(u);
      bid = centerFace->GetComponent(i, 0);
        
      if (v < 0)
        {
        // Outer edge of the dataset, so automatically
        // included in the region boundary.
        outFrags->GetCellPoints(u, line);
        borders[cellU]->InsertNextId(line->GetId(bid));
        borders[cellU]->InsertNextId(line->GetId((bid+1) % line->GetNumberOfIds()));
        bordTriCount += 1;
        isOnBoundary->SetValue(cellU, 1);
        } // if outer edge (v < 0)
      else
        {
        // Check whether the two cells belong to the same 
        // region.  If the same, this is an interior border
        // and we skip over it.

        cellV = cellVertex->GetValue(v);
        if (cellU == cellV)
          {
          continue;
          }
        outFrags->GetCellPoints(u, line);

        borders[cellU]->InsertNextId(line->GetId(bid));
        borders[cellU]->InsertNextId(line->GetId((bid+1) % line->GetNumberOfIds()));

        outFrags->GetCellPoints(v, line);
        bid = centerFace->GetComponent(i, 1);
        borders[cellV]->InsertNextId(line->GetId(bid));
        borders[cellV]->InsertNextId(line->GetId((bid+1) % line->GetNumberOfIds()));
        bordTriCount += 2;
        }  // shared inner edge
          
      } // for each region
       
    // Convert list of vertex pairs into a
    // single polygon.  Painful.  We make an 
    // inverse table to track each of the two
    // indices in the list where a given vertex
    // appears, and then play joint-the-dots.
    for (vtkIdType i = 0; i < nextVertex; i++)
      {
      vtkIdList *poly = borders[i];
      vtkIdType baseId, lastId;
      baseId = lastId = poly->GetId(0);
      for (vtkIdType j = 0; j < poly->GetNumberOfIds(); j++)
        {
        vtkIdType id = poly->GetId(j);
        if (id < baseId) baseId = id;
        if (id > lastId) lastId = id;
        }

      const vtkIdType invSize = 2 * (lastId-baseId+1);
      vtkIdType *inv    = new vtkIdType [invSize];

      for (vtkIdType j = 0; j < invSize; j++)
        {
        inv[j] = -1;
        }
      for (vtkIdType j = 0; j < poly->GetNumberOfIds(); j++)
        {
        vtkIdType offset = 2*(poly->GetId(j) - baseId);

        if (inv[offset] < 0)
          {
          inv[offset] = j;
          }
        else
          {
          inv[offset + 1] = j;
          }
        }
      // Now join-the-dots.
      line->Reset();
      thisNodeIx = 0;        
      do {
        bid = poly->GetId(thisNodeIx);
        line->InsertNextId(bid);          
        nextNodeIx = inv[2*(bid - baseId)];
        if (nextNodeIx == thisNodeIx)
          {
          nextNodeIx = inv[2*(bid - baseId)+1];
          }
        // Select the "other" node on the edge.
        thisNodeIx = (nextNodeIx % 2) ? nextNodeIx-1 : nextNodeIx+1;
        } // do  
      while (thisNodeIx);
      // ... and insert polygon into output.
      outBoundary->InsertNextCell(VTK_POLYGON, line);
      delete [] inv;
      } // For each vertex 
    }  //  Dimensionality == 2
  else
    { //  Dimensionality == 3
    
    for (vtkIdType i = 0; i < centerOf->GetNumberOfTuples(); i++)
      {
      u = centerOf->GetComponent(i, 0);
      v = centerOf->GetComponent(i, 1);
      cellU = cellVertex->GetValue(u);
      bid = centerFace->GetComponent(i, 0);

      if (v < 0)
        {
        // Outer edge of the dataset, so automatically
        // included in the region boundary.
        facePtr = outFrags->GetCell(u)->GetFace(bid)->GetPointIds();
        cell = borders[cellU];
        cell->InsertNextId(facePtr->GetNumberOfIds());
        for (int j = 0; j < facePtr->GetNumberOfIds(); j++)
          {
          cell->InsertNextId(facePtr->GetId(j));
          }
        isOnBoundary->SetValue(cellU, 1);
        // increment polygon count
        cell->SetId(0, cell->GetId(0) + 1);
        } // if outer edge (v < 0)
      else
        {
        // Check whether the two cells belong to the same 
        // region.  If the same, this is an interior border
        // and we skip over it.

        cellV = cellVertex->GetValue(v);
        if (cellU == cellV)
          {
          continue;
          }
          
        // u and v are different regions, so add the
        // boundary to both region polyhedra.
        facePtr = outFrags->GetCell(u)->GetFace(bid)->GetPointIds();
        cell = borders[cellU];
        cell->InsertNextId(facePtr->GetNumberOfIds());
        for (int j = 0; j < facePtr->GetNumberOfIds(); j++)
          {
          cell->InsertNextId(facePtr->GetId(j));
          }
        // increment polygon count for cell U
        cell->SetId(0, cell->GetId(0) + 1);

        bid = centerFace->GetComponent(i, 1);
        facePtr = outFrags->GetCell(v)->GetFace(bid)->GetPointIds();
        cell = borders[cellV];
        cell->InsertNextId(facePtr->GetNumberOfIds());
        for (int j = 0; j < facePtr->GetNumberOfIds(); j++)
          {
          cell->InsertNextId(facePtr->GetId(j));
          }

        // increment polygon count for cell V
        cell->SetId(0, cell->GetId(0) + 1);
        }  // shared inner edge

      } // for each face center

   
    for (vtkIdType i = 0; i < nextVertex; i++)
      {
      outBoundary->InsertNextCell(VTK_POLYHEDRON, borders[i]);  
      bordTriCount += borders[i]->GetId(0);
      }      
    }  //  Dimensionality == 3 
  outBoundary->SetPoints(newPoints);
  outBoundary->BuildLinks();
  for (std::vector<Field*>::iterator it = this->Fields.begin();
      it != this->Fields.end();
      it++)
    {
    outBndrCD->AddArray((*it)->nodeScalars);
    }
  outBndrPD->ShallowCopy(outPD);

  for (vtkIdType i = 0; i < nextVertex; i++)
    {
    borders[i]->Delete();
    }
  delete [] borders;
  outBoundary->Squeeze();

  output->ShallowCopy(topograph);    
  // Add cell arrays.  We do this here rather than earlier as we
  // need to copy data for existing cells.
  for (std::vector<Field*>::iterator it = this->Fields.begin();
      it != this->Fields.end();
      it++)
    {
    output->GetVertexData()->AddArray((*it)->nodeScalars);
    (*it)->slabScalars = NULL;
    (*it)->tempScalars = NULL;
    (*it)->nodeScalars = NULL;
    (*it)->parents     = NULL;
    (*it)->pointScalars = NULL;
    }
 

  output->GetVertexData()->AddArray(fragSize);
  output->GetVertexData()->AddArray(fragCenters);
  output->GetEdgeData()->AddArray(deltaArr);
  output->GetVertexData()->AddArray(isOnBoundary);
  output->Squeeze();
  return 1;
}


