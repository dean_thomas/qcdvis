/*=========================================================================

 *	File: vtkGrid.h
 *	Graph visualization library for VTK
 * 
 *	This software is distributed WITHOUT ANY WARRANTY; 
 *	without even the implied warranty of MERCHANTABILITY 
 *	or FITNESS FOR A PARTICULAR PURPOSE.  
 * 
 *	See the file copyright.txt for details.  

=========================================================================*/
// .NAME vtkGrid - Obtain vtkUnstructuredGrid data type from a pipeline. 
// .SECTION Description
// vtkGrid takes an vtkAlgorithm  as input and outputs a vtkDataSet. 
// This method simply extract vtkDataSet from the pipeline. 


#ifndef __vtkGrid_h
#define __vtkGrid_h

#include "vtkMETAModule.h"
#include "vtkUnstructuredGridAlgorithm.h"


class VTK_META_EXPORT vtkGrid : public vtkUnstructuredGridAlgorithm
{
public:
  static vtkGrid *New();
  vtkTypeMacro(vtkGrid,vtkUnstructuredGridAlgorithm);
  void PrintSelf(ostream& os, vtkIndent indent);

protected:
  vtkGrid();
  ~vtkGrid();

  // Override to specify support for any vtkDataSet input type.
  virtual int FillInputPortInformation(int port, vtkInformation* info);

  // Main implementation.
  virtual int RequestData(
      vtkInformation*,
      vtkInformationVector**,
      vtkInformationVector*);

private:
  vtkGrid(const vtkGrid&);  // Not implemented.
  void operator=(const vtkGrid&);  // Not implemented.
};

#endif

