/*=========================================================================

 *	File: vtkOGDFLayoutStrategy.h
 *	Graph visualization library for VTK
 * 
 *	This software is distributed WITHOUT ANY WARRANTY; 
 *	without even the implied warranty of MERCHANTABILITY 
 *	or FITNESS FOR A PARTICULAR PURPOSE.  
 * 
 *	 Author: Colin Myers   
=========================================================================*/
// .NAME vtkOGDFLayoutStrategy - Compute a graph layout using the Open 
// Graph Drawing Framework.
// .SECTION Description
// Several layouts can be set by name, the algorithms currently available include:
//   * balloon 
//   * circular 
//   * davidsonharel 
//   * dominance 
//   * fmmm 
//   * gem 
//   * fruchtermanreingold 
//   * kamadakawai
//   * sugiyama
//
// This class requires libOGDF, see http://www.ogdf.net/doku.php/start for
// download and installation instructions.
#include "vtkMETAModule.h"

#ifndef __vtkOGDFLayoutStrategy_h
#define __vtkOGDFLayoutStrategy_h

#include "vtkGraphLayoutStrategy.h"
namespace ogdf
{
  class LayoutModule;
}

class VTK_META_EXPORT vtkOGDFLayoutStrategy : public vtkGraphLayoutStrategy
{
public:
  static vtkOGDFLayoutStrategy* New();

  vtkTypeMacro(vtkOGDFLayoutStrategy, vtkGraphLayoutStrategy);
  void PrintSelf(ostream& os, vtkIndent indent);

  // Description:
  // Perform the layout.
  void Layout();

  // Description:
  // Select an OGDF Layout Module by name.
  void SetLayoutModuleByName(const char* name);

  // Description:
  // Set the OGDF Layout Module directly. This class takes ownership of 'layout'.
  void SetLayoutModule(ogdf::LayoutModule* layout);

  // Description:
  // Get the OGDF Layout Module.
  ogdf::LayoutModule* GetLayoutModule() const;

  // Description:
  // Use OGDF edge layout. If set on and the OGDF layout provides them, edge
  // points are added to the output graph.
  vtkSetMacro(LayoutEdges, bool);
  vtkGetMacro(LayoutEdges, bool);
  vtkBooleanMacro(LayoutEdges, bool);

protected:
  vtkOGDFLayoutStrategy();
  ~vtkOGDFLayoutStrategy();

private:
  vtkOGDFLayoutStrategy(const vtkOGDFLayoutStrategy&);  // Not implemented.
  void operator=(const vtkOGDFLayoutStrategy&);  // Not implemented.

  ogdf::LayoutModule* LayoutModule;
  bool LayoutEdges;
};

#endif
