/*=========================================================================

 *	File: vtkSimplifyMetric.cxx
 *	Graph visualization library for VTK
 *
 *	This software is distributed WITHOUT ANY WARRANTY;
 *	without even the implied warranty of MERCHANTABILITY
 *	or FITNESS FOR A PARTICULAR PURPOSE.
 *
 *	See the file copyright.txt for details.

=========================================================================*/
#include "vtkSimplifyMetric.h"
#include "vtkDataSetAttributes.h"
#include "vtkIdTypeArray.h"
#include "vtkBitArray.h"
#include "vtkFloatArray.h"
#include "vtkIntArray.h"
#include "vtkInformation.h"
#include "vtkMutableDirectedGraph.h"
#include "vtkMutableUndirectedGraph.h"
#include "vtkMutableGraphHelper.h"
#include "vtkAdjacentVertexIterator.h"
#include "vtkGraphEdge.h"
#include "vtkObjectFactory.h"
#include "vtkBoostConnectedComponents.h"
#include "vtkNew.h"
#include <queue>
#include <stack>
#include <set>
#include "vtkVoidArray.h"
#include "vtkTreeBFSIterator.h"

vtkStandardNewMacro(vtkSimplifyMetric);

#define VTK_CREATE(type,name) \
	vtkSmartPointer<type> name = vtkSmartPointer<type>::New()

//-----------------------------------------------------------------------
vtkSimplifyMetric::vtkSimplifyMetric()
{
	this->SetNumberOfOutputPorts(1);
	this->Fields = std::vector<char*>();
	this->RegCompNr = this->SingCompNr= -1;
	this->ApplySimplify = false;
	this->SelectionNode = vtkSmartPointer<vtkSelectionNode>::New();
	this->Selection = vtkSmartPointer<vtkSelection>::New();
	this->ExtractSelection = vtkSmartPointer<vtkExtractSelectedGraph>::New();
}

vtkSimplifyMetric::~vtkSimplifyMetric()
{
	while (this->Fields.size() > 0)
	{
		delete [] this->Fields.back();
		this->Fields.pop_back();
	}
	this->Fields.clear();
}

void vtkSimplifyMetric::AddField(const char *nm)
{
	char *f = new char[strlen(nm) + 1];
	strcpy(f, nm);
	this->Fields.push_back(f);
}

void vtkSimplifyMetric::AddStepSize(float size)
{
	this->StepSize.push_back(size);
}


void vtkSimplifyMetric::SetJCNList(vtkSimplifyJCN *simplify)
{
	this->JCNList = simplify->GetJCNList();
}

std::vector< std::vector<int> > vtkSimplifyMetric::GetJCNList()
{
	return this->JCNList;
}

void vtkSimplifyMetric::PrintSelf(ostream& os, vtkIndent indent)
{
	this->Superclass::PrintSelf(os, indent);
}

int vtkSimplifyMetric::FillInputPortInformation(int port, vtkInformation* info)
{
	if (port == 0)
	{
		info->Set(vtkAlgorithm::INPUT_REQUIRED_DATA_TYPE(), "vtkGraph");
		return 1;
	}

	return 0;
}
//--------------------------------------------------------------------------------------
int  vtkSimplifyMetric::ComputeVolumeMetric()
{
	int numNodes= this->ComponentConnectivityGraph->GetNumberOfVertices();
	VTK_CREATE(vtkFloatArray, nodeSize);
	nodeSize->SetName("volume");
	nodeSize->SetNumberOfValues(numNodes);

	double* sizeArr = new double[numNodes];
	for (int i=0; i<numNodes; i++)
	{
		sizeArr[i]=0.0;
	}

	for (int i = 0; i < numNodes; i++)
	{
		int size=this->ComponentConnectivityGraph->GetVertexData()->GetArray("size")->GetComponent(i,0);
		sizeArr[i] += size;
	}


	double Max, Min, Range;
	// Components
	Max=Min=sizeArr[0];
	for (int i=0; i<numNodes; i++)
	{
		if (Max<sizeArr[i])
		{
			Max=sizeArr[i];
		}

		if (Min>sizeArr[i])
		{
			Min=sizeArr[i];
		}
	}

	Range=Max-Min;

	cout << "Maximum volume: " << Max << endl;
	cout << "Minimum volume: " << Min << endl;
	cout << "Range: " << Range << endl;

	for (int i=0; i<numNodes; i++)
	{
		if (Range==0)
		{
			nodeSize->SetValue(i, 1);
			cout << "Volume of node " << i << " is " << 1 << "." << endl;
		}
		else
		{
			auto val = (sizeArr[i]-Min)/Range;
			nodeSize->SetValue(i, val);
			cout << "Raw volume of node " << i << " is " << sizeArr[i]<< "." << endl;
			cout << "Normalised volume of node " << i << " is " << val << "." << endl;
		}

	}
	this->ComponentConnectivityGraph->GetVertexData()->AddArray(nodeSize);
	return 1;
}

//--------------------------------------------------------------------------------------
int  vtkSimplifyMetric::ComputePersistenceMetric()
{
	//	Persistence is computed as:
	//
	//	p = (( C_n / J_n ) * ( J_n / R)) + offset
	//
	//	where	C_n is number of JCN nodes in the current component
	//			J_n is the number of nodes in the entire JCN
	//			R is the range of C_n for all components in the Reeb skeleton
	//	output should be in the range [0.0, 1.0]
	int numNodes= this->ComponentConnectivityGraph->GetNumberOfVertices();
	VTK_CREATE(vtkFloatArray, persistSize);
	persistSize->SetName("persistence");
	persistSize->SetNumberOfValues(numNodes);

	double boxSize=1.0;
	for (int i = 0; i < this->StepSize.size(); i++)
	{
		boxSize = boxSize*this->StepSize[i];
	}

	double* persistenceArray = new double[numNodes];
	for (int i=0; i<numNodes; i++)
	{
		persistenceArray[i]=0.0;
	}

	for (int i = 0; i < numNodes; i++)
	{
		int size=this->ComponentConnectivityGraph->GetVertexData()->GetArray("jcnList")->GetComponent(i,0);
		persistenceArray[i] += boxSize*size;
	}

	double Max, Min, Range;

	//Regular Components
	Max=Min=persistenceArray[0];
	for (int i=0; i<numNodes; i++)
	{
		if (Max<persistenceArray[i])
		{
			Max=persistenceArray[i];
		}
		if (Min>persistenceArray[i])
		{
			Min=persistenceArray[i];
		}
	}
	Range=Max-Min;
	for (int i=0; i<numNodes; i++)
	{
		if (Range==0)
		{
			persistSize->SetValue(i, 1);
			cout << "Persistence of node " << i << " is " << 1 << "." << endl;
		}
		else
		{
			auto value = (persistenceArray[i]-Min)/Range;
			persistSize->SetValue(i, value);
			cout << "Persistence of node " << i << " is " << (persistenceArray[i]-Min)/Range << "." << endl;
		}
	}
	this->ComponentConnectivityGraph->GetVertexData()->AddArray(persistSize);
	return 1;
}


//------------------------------------------------------------
//Simplify based on the 'chosen' metric
int vtkSimplifyMetric::SimplifyComponentConectivityGraphUpdate(double threshold, char *metric)
{
	cout << "Persistence metric used by Reeb skeleton: " << metric << endl;

	if (strcmp(metric, "volume")==0)
	{
		std::cout<<"Using size-metric for simplification" << std::endl;
	}
	else if(strcmp(metric, "persistence")==0)
	{
		std::cout<<"Using persistence-metric for simplification" << std::endl;
	}
	else
	{
		vtkErrorMacro("ERROR: Metric string is not correct, no simplification...");
		return 0;
	}

	vtkIdType prevNodeNr, currNodeNr;
	prevNodeNr = currNodeNr = 0;

	do {
		//Pruning a 'Regular' deg-2/1 nodes from CCG if metric < threshold
		VTK_CREATE(vtkIdTypeArray, remainNodes);
		prevNodeNr = this->ComponentConnectivityGraph->GetNumberOfVertices();
		for (int i=0; i< this->ComponentConnectivityGraph->GetNumberOfVertices(); i++)
		{
			vtkIdType type=this->ComponentConnectivityGraph->GetVertexData()->GetArray("nodeType")->GetComponent(i,0);
			//if regular node
			int neighborNr = this->GetNumberOfAdjacency(this->ComponentConnectivityGraph, i);
			bool isRemain = true;
			if (type == 0 && neighborNr == 1)
			{
				double scaledValue=this->ComponentConnectivityGraph->GetVertexData()->GetArray(metric)->GetComponent(i,0);
				//if the 'metric' value of that regular node is less than the 'threshold'
				if (scaledValue <= threshold)
				{
					isRemain = false;
				}
			}

			if (isRemain)
			{
				remainNodes->InsertNextValue(i);
			}
		}

		this->SelectionNode->SetFieldType(vtkSelectionNode::VERTEX);
		this->SelectionNode->SetContentType(vtkSelectionNode::INDICES);
		this->SelectionNode->SetSelectionList(remainNodes);
		this->Selection->AddNode(this->SelectionNode);

		this->ExtractSelection = vtkSmartPointer<vtkExtractSelectedGraph>::New();
		this->ExtractSelection->SetInputData(0, this->ComponentConnectivityGraph);
		this->ExtractSelection->SetInputData(1, this->Selection);
		this->ExtractSelection->Update();
		this->ComponentConnectivityGraph = vtkMutableUndirectedGraph::SafeDownCast(this->ExtractSelection->GetOutput());
		currNodeNr = this->ComponentConnectivityGraph->GetNumberOfVertices();
	} while (currNodeNr != prevNodeNr);
	return 1;
}

void vtkSimplifyMetric::DeleteDegreeOneSingular()
{
	VTK_CREATE(vtkIdTypeArray, remainNodes);
	for (int i=0; i< this->ComponentConnectivityGraph->GetNumberOfVertices(); i++)
	{
		vtkIdType type= this->ComponentConnectivityGraph->GetVertexData()->GetArray("nodeType")->GetComponent(i,0);
		//if singular node
		int neighborNr = this->GetNumberOfAdjacency(this->ComponentConnectivityGraph, i);
		if (!(type==1 && (neighborNr==0 || neighborNr ==1)))
		{
			remainNodes->InsertNextValue(i);
		}
	}
	this->SelectionNode->SetFieldType(vtkSelectionNode::VERTEX);
	this->SelectionNode->SetContentType(vtkSelectionNode::INDICES);
	this->SelectionNode->SetSelectionList(remainNodes);
	this->Selection->AddNode(this->SelectionNode);

	this->ExtractSelection = vtkSmartPointer<vtkExtractSelectedGraph>::New();
	this->ExtractSelection->SetInputData(0, this->ComponentConnectivityGraph);
	this->ExtractSelection->SetInputData(1, this->Selection);
	this->ExtractSelection->Update();
	this->ComponentConnectivityGraph = vtkMutableUndirectedGraph::SafeDownCast(this->ExtractSelection->GetOutput());
}


void vtkSimplifyMetric::MergeDegreeOneRegular()
{
	vtkIdType prevNodeNr, currNodeNr;
	prevNodeNr = currNodeNr = 0;

	do {
		prevNodeNr = this->ComponentConnectivityGraph->GetNumberOfVertices();
		VTK_CREATE(vtkIdTypeArray, remainNodes);
		for (int i=0; i< this->ComponentConnectivityGraph->GetNumberOfVertices(); i++)
		{
			vtkIdType type= this->ComponentConnectivityGraph->GetVertexData()->GetArray("nodeType")->GetComponent(i,0);
			//if singular node
			std::vector<int> neighbor = this->GetAdjacency(this->ComponentConnectivityGraph, i);
			bool isRemain = true;
			if (type==0 && neighbor.size() ==1 )
			{
				int neighborIndex = neighbor[0];
				vtkIdType neighborType = this->ComponentConnectivityGraph->GetVertexData()->GetArray("nodeType")->GetComponent(neighborIndex,0);
				int nextNeighbor = this->GetNumberOfAdjacency(this->ComponentConnectivityGraph, neighborIndex);
				if (neighborType == 0 && nextNeighbor < 3)
				{
					isRemain = false;
					int deleteJcnCompId = this->ComponentConnectivityGraph->GetVertexData()->GetArray("compId")->GetComponent(i,0);
					int mergeJcnCompId = this->ComponentConnectivityGraph->GetVertexData()->GetArray("compId")->GetComponent(neighborIndex,0);
					this->MergeJCNId(deleteJcnCompId,mergeJcnCompId);
				}
			}
			if (isRemain)
			{
				remainNodes->InsertNextValue(i);
			}
		}
		this->SelectionNode->SetFieldType(vtkSelectionNode::VERTEX);
		this->SelectionNode->SetContentType(vtkSelectionNode::INDICES);

		this->SelectionNode->SetSelectionList(remainNodes);
		this->Selection->AddNode(this->SelectionNode);

		this->ExtractSelection = vtkSmartPointer<vtkExtractSelectedGraph>::New();
		this->ExtractSelection->SetInputData(0, this->ComponentConnectivityGraph);
		this->ExtractSelection->SetInputData(1, this->Selection);
		this->ExtractSelection->Update();

		this->ComponentConnectivityGraph = vtkMutableUndirectedGraph::SafeDownCast(this->ExtractSelection->GetOutput());
		currNodeNr = this->ComponentConnectivityGraph->GetNumberOfVertices();
	} while (prevNodeNr != currNodeNr);
}


int vtkSimplifyMetric::MergeNodeCCG(std::vector<int> v, vtkIdTypeArray *nodeId)
{
	VTK_CREATE(vtkIdTypeArray, deleteNodes);
	VTK_CREATE(vtkIdTypeArray, changeComps);

	vtkIdType mergedNode=v[0];
	vtkIdType mergeComp = mergedNode;
	double mergedSize=this->ComponentConnectivityGraph->GetVertexData()->GetArray("size")->GetComponent(v[0],0);
	for (int i=1;i<v.size();i++)
	{
		std::vector<int> it = this->GetAdjacency(this->ComponentConnectivityGraph,v[i]);
		nodeId->InsertNextValue(v[i]);
		mergedSize += this->ComponentConnectivityGraph->GetVertexData()->GetArray("size")->GetComponent(v[i],0);
	}
	this->ComponentConnectivityGraph->GetVertexData()->GetArray("size")->SetComponent(mergedNode, 0, mergedSize);
	return 1;
}


void vtkSimplifyMetric::FromSingularToRegular()
{
	for (int i=0; i< this->ComponentConnectivityGraph->GetNumberOfVertices(); i++)
	{
		vtkIdType type= this->ComponentConnectivityGraph->GetVertexData()->GetArray("nodeType")->GetComponent(i,0);
		//if singular node
		int neighborNr = this->GetNumberOfAdjacency(this->ComponentConnectivityGraph, i);
		if (type==1 && neighborNr == 2)
		{
			this->ComponentConnectivityGraph->GetVertexData()->GetArray("nodeType")->SetComponent(i,0,0);
		}
	}

}

int vtkSimplifyMetric::GetNumberOfAdjacency(vtkGraph *inputGraph, vtkIdType index)
{
	std::set<int> myset;
	vtkSmartPointer<vtkAdjacentVertexIterator> it = vtkSmartPointer<vtkAdjacentVertexIterator>::New();
	inputGraph->GetAdjacentVertices(index,it);
	int totalNr = 0;
	while (it->HasNext())
	{
		myset.insert(it->Next());
	}
	return myset.size();
}

std::vector<int> vtkSimplifyMetric::GetAdjacency(vtkGraph *inputGraph, vtkIdType index)
{
	std::set<int> myset;
	vtkSmartPointer<vtkAdjacentVertexIterator> it = vtkSmartPointer<vtkAdjacentVertexIterator>::New();
	inputGraph->GetAdjacentVertices(index,it);
	int totalNr = 0;
	while (it->HasNext())
	{
		myset.insert(it->Next());
	}
	std::vector<int> result;
	for (std::set<int>::iterator it=myset.begin(); it!=myset.end(); ++it)
	{
		result.push_back(*it);
	}

	return result;
}

void vtkSimplifyMetric::MergeJCNId(vtkIdType FromNodeId, vtkIdType ToNodeId)
{
	for (int i = 0; i < this->JCNList[FromNodeId].size(); i++)
	{
		this->JCNList[ToNodeId].push_back(this->JCNList[FromNodeId][i]);
	}
}

void vtkSimplifyMetric::ApplySimplicationRule()
{
	cout << "Applying simplification." << endl;
	this->DeleteDegreeOneSingular();
	this->FromSingularToRegular();
	//this->SimplifyComponentConectivityGraphUpdate(0.1, "persistence");
	if (persistence_threshold > 0.0)
		this->SimplifyComponentConectivityGraphUpdate(persistence_threshold, const_cast<char*>(persistence_measure_type.c_str()));
	this->DeleteDegreeOneSingular();
	//this->SimplifyComponentConectivityGraphUpdate(0.1, "persistence");
	if (persistence_threshold > 0.0)
		this->SimplifyComponentConectivityGraphUpdate(persistence_threshold, const_cast<char*>(persistence_measure_type.c_str()));
	this->DeleteDegreeOneSingular();
	this->FromSingularToRegular();
	this->MergeDegreeOneRegular();
}

//-------------------------------------------------------------------------------------
int vtkSimplifyMetric::RequestData(
		vtkInformation* vtkNotUsed(request),
		vtkInformationVector** inputVector,
		vtkInformationVector* outputVector)
{

	// Ensure we have valid inputs ...
	vtkGraph* const inputGraph = vtkGraph::GetData(inputVector[0]);
	vtkGraph* const outputGraph = vtkGraph::GetData(outputVector);

	outputGraph->DeepCopy(inputGraph);
	if (!inputGraph)
	{
		vtkErrorMacro("A vtkGraph object is required.");
		return 0;
	}
	vtkIdType nrVertices = inputGraph->GetNumberOfVertices();
	if (!nrVertices)
	{
		vtkErrorMacro("Empty graph, no vertices.");
		return 0;
	}
	if (!this->Fields[0])
	{
		vtkErrorMacro("No field specified.");
		return 0;
	}
	if (this->JCNList.size()==0)
	{
		vtkErrorMacro("A component graph array is required.");
		return 0;
	}
	this->ComponentConnectivityGraph = vtkSmartPointer<vtkMutableUndirectedGraph>::New();
	this->ComponentConnectivityGraph->DeepCopy(inputGraph);

	vtkSmartPointer<vtkIdTypeArray> compIdArray = vtkSmartPointer<vtkIdTypeArray>::New();
	compIdArray->SetName("compId");
	for (int i = 0; i < this->ComponentConnectivityGraph->GetNumberOfVertices(); i++)
	{
		compIdArray->InsertNextValue(i);
	}

	if (this->ComponentConnectivityGraph->GetVertexData()->HasArray("compId"))
	{
		this->ComponentConnectivityGraph->GetVertexData()->RemoveArray("compId");
		this->ComponentConnectivityGraph->GetVertexData()->AddArray(compIdArray);
	}
	else
	{
		this->ComponentConnectivityGraph->GetVertexData()->AddArray(compIdArray);
	}
	this->ComputePersistenceMetric();
	this->ComputeVolumeMetric();


	if (this->ApplySimplify)
	{
		this->ApplySimplicationRule();
	}

	//Memory allocat
	vtkIdType nrVert = this->ComponentConnectivityGraph->GetNumberOfVertices();
	vtkSmartPointer<vtkIntArray> value[10];
	for (int f = 0; f < this->Fields.size(); f++)
	{
		value[f] = vtkSmartPointer<vtkIntArray>::New();
		value[f]->SetNumberOfTuples(nrVert);
		value[f]->SetName(this->Fields[f]);
	}
	VTK_CREATE(vtkIdTypeArray, typeArray);
	typeArray=vtkIdTypeArray::SafeDownCast(this->ComponentConnectivityGraph->GetVertexData()->GetArray("nodeType"));
	//Setting field-values for Jacobi-nodes, 0: White, 1: Red, 2: Blue, 3: Green
	for (int i = 0; i < nrVert; i++)
	{
		if (typeArray->GetComponent(i,0)==1)
		{
			for (int f = 0; f < this->Fields.size(); f++)
			{
				value[f]->SetValue(i,1);//red
			}
		}
		else
		{
			for (int f = 0; f < this->Fields.size(); f++)
			{
				value[f]->SetValue(i,2);//blue
			}
		}
	}
	vtkDataSetAttributes *inPD = this->ComponentConnectivityGraph->GetVertexData(), *outPD  = outputGraph->GetVertexData();
	outputGraph->DeepCopy(this->ComponentConnectivityGraph);

	outPD->CopyAllocate(inPD, nrVert, 10);
	for (vtkIdType i = 0; i < nrVert; i++)
	{
		outPD->CopyData(this->ComponentConnectivityGraph->GetVertexData(), i, i);
	}

	//Added for coloring component connectivity graph nodes
	for (int f = 0; f < this->Fields.size(); f++)
	{
		outPD->AddArray(value[f]);
	}

	outputGraph->Squeeze();
	return 1;
}


//---------------------------------------------------------------------------
