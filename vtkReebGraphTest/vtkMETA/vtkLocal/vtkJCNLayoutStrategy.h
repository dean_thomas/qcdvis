/*=========================================================================

 *	File: vtkJCNLayoutStrategy.h
 *	Graph visualization library for VTK
 * 
 *	This software is distributed WITHOUT ANY WARRANTY; 
 *	without even the implied warranty of MERCHANTABILITY 
 *	or FITNESS FOR A PARTICULAR PURPOSE.  
 * 
 *	See the file copyright.txt for details.  

=========================================================================*/
// .NAME vtkJCNLayoutStrategy 
//
// .SECTION Description
//This filter implements a domain-specific layout of JCN graph. 
//Assigns points to the vertices around a circle with unit radius.

#ifndef __vtkJCNLayoutStrategy_h
#define __vtkJCNLayoutStrategy_h

#include "vtkGraphLayoutStrategy.h"
#include "vtkMETAModule.h"

class VTK_META_EXPORT vtkJCNLayoutStrategy : public vtkGraphLayoutStrategy 
{
public:
  static vtkJCNLayoutStrategy *New();

  vtkTypeMacro(vtkJCNLayoutStrategy, vtkGraphLayoutStrategy);
  void PrintSelf(ostream& os, vtkIndent indent);

  // Description:
  // Decide whether to fix the point locations in the layout.  
  vtkSetMacro(LockPoints, bool);
  vtkGetMacro(LockPoints, bool);
  vtkBooleanMacro(LockPoints, bool);
  
  // Description:
  // Assigns points to the vertices around a circle with unit radius.
  vtkSetMacro(ClearRadius, double);
  vtkGetMacro(ClearRadius, double);

  // Description:
  // Perform the layout.
  void Layout();
  
protected:
  vtkJCNLayoutStrategy();
  ~vtkJCNLayoutStrategy();

  bool LockPoints;
  double ClearRadius;
  
private:
  vtkJCNLayoutStrategy(const vtkJCNLayoutStrategy&);  // Not implemented.
  void operator=(const vtkJCNLayoutStrategy&);  // Not implemented.
};

#endif

