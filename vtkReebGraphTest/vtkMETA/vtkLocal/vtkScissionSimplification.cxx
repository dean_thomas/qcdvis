/*=========================================================================

 *	File: vtkScissionSimplification.cxx
 *	Graph visualization library for VTK
 * 
 *	This software is distributed WITHOUT ANY WARRANTY; 
 *	without even the implied warranty of MERCHANTABILITY 
 *	or FITNESS FOR A PARTICULAR PURPOSE.  
 * 
 *	See the file copyright.txt for details.  

=========================================================================*/
#include "vtkAdjacentVertexIterator.h"
#include "vtkDataSetAttributes.h"
#include "vtkDataArray.h"
#include "vtkExecutive.h"
#include "vtkFieldData.h"
#include "vtkGraph.h"
#include "vtkGraphEdge.h"
#include "vtkInformation.h"
#include "vtkInformationVector.h"
#include "vtkIdTypeArray.h"
#include "vtkMutableUndirectedGraph.h"
#include "vtkMutableGraphHelper.h"
#include "vtkObjectFactory.h"
#include "vtkScissionSimplification.h"
#include "vtkSmartPointer.h"
#include <vector>

vtkStandardNewMacro(vtkScissionSimplification);

//----------------------------------------------------------------------------

vtkScissionSimplification::vtkScissionSimplification()
{
  this->SetNumberOfOutputPorts(1);
  this->Fields = std::vector<char*>();
  this->Threshold = 0.8;
}

//----------------------------------------------------------------------------

vtkScissionSimplification::~vtkScissionSimplification()
{
  while (this->Fields.size() > 0)
    {
    delete [] this->Fields.back();
    this->Fields.pop_back();
    }
  this->Fields.clear();
}

//----------------------------------------------------------------------------

void vtkScissionSimplification::AddField(const char *nm)
{
  char *f = new char[strlen(nm) + 1];
  strcpy(f, nm);
  this->Fields.push_back(f);
}

//----------------------------------------------------------------------------

int vtkScissionSimplification::RequestData(
  vtkInformation* vtkNotUsed(request), 
  vtkInformationVector** inputVector, 
  vtkInformationVector* outputVector)
{
  vtkGraph* graph = vtkGraph::GetData(inputVector[0]);
  vtkGraph* output = vtkGraph::GetData(outputVector);
 
  // Check that input is what is expected. 
  if (!graph)
    {
    vtkErrorMacro("A vtkGraph object is reqired.");
    return 0;
    }

  std::vector<vtkDataArray*> arrays = std::vector<vtkDataArray*>();

  vtkDataSetAttributes *inPD   = graph->GetVertexData(), 
                       *outPD  = output->GetVertexData();
  vtkDataSetAttributes *data;
  
  double range[2];
  double threshold[50]; // Arbitrary - combine with struct?

  // Get pointer to each data array that we were told to 
  // expect in the input graph.

  for (int i = 0; i < this->Fields.size(); i++)
    {
    vtkDataArray *d = inPD->GetArray(this->Fields[i]);
    if (!d)
      {
      vtkErrorMacro("Missing array: " << this->Fields[i]);
      return 0;
      }
    arrays.push_back(d);
    d->GetRange(range);
    threshold[i] = range[0] + this->Threshold*(range[1] - range[0]);
    }


  int nrVertices = graph->GetNumberOfVertices();

  // Create objects to hold & build output graph.
  vtkSmartPointer<vtkMutableGraphHelper> builder
      = vtkSmartPointer<vtkMutableGraphHelper>::New();
  vtkSmartPointer<vtkMutableUndirectedGraph> undir =
      vtkSmartPointer<vtkMutableUndirectedGraph>::New();
  builder->SetGraph(undir);

  // Array to track final id of vertices.
  vtkSmartPointer<vtkIdTypeArray> fate
      = vtkSmartPointer<vtkIdTypeArray>::New();
  fate->SetNumberOfTuples(nrVertices);
  
  vtkSmartPointer<vtkAdjacentVertexIterator> adj = 
      vtkSmartPointer<vtkAdjacentVertexIterator>::New();
    
  int nextNew = 0;
  bool retain;

  // Iterate over vertices, for each check whether it is
  // to be retained.  If it is retained, add a new vertex
  // to the output graph and track its new id.
  for (vtkIdType i = 0; i < nrVertices; i++)
    {
    retain = true;
    for (int a = 0; retain && a < this->Fields.size(); a++)
      {
      retain &= arrays[a]->GetComponent(i,0) > threshold[a];
      }
    
    if (retain)
      {
      fate->SetValue(i, nextNew++);
      builder->AddVertex();
      }
    else
      {
      fate->SetValue(i, -1);
      }
    }
    
  // For each edge in the input graph, create a new edge
  // in the output graph iff both source and target are
  // retained.
  for (int e = 0; e < graph->GetNumberOfEdges(); e++)
    {
    vtkIdType src = graph->GetSourceVertex(e);
    vtkIdType tgt = graph->GetTargetVertex(e);
    src = fate->GetValue(src);
    tgt = fate->GetValue(tgt);
    if (src < 0 || tgt < 0)
      {
      continue;
      }
    builder->AddEdge(src, tgt);
    }

  // Copy vertex data of retained vertices.
  output->ShallowCopy(builder->GetGraph());
  data = output->GetVertexData();
  data->CopyAllocate(graph->GetVertexData(), output->GetNumberOfVertices(), 10);
  
  for (vtkIdType i = 0; i < nrVertices; i++)
     {
     vtkIdType d = fate->GetValue(i);
     if (d >= 0)
       {
       data->CopyData(graph->GetVertexData(), i, d);
       }
     }
  output->Squeeze();
  return 1;
}

//----------------------------------------------------------------------------
void vtkScissionSimplification::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os, indent);
  os << indent << "Threshold: " << this->Threshold << endl;
  os << indent << "Simplification fields:" << endl;
  for (int i = 0; i < this->Fields.size(); i++)
    {
    os << indent.GetNextIndent() << this->Fields[i] << endl;
    }
}

