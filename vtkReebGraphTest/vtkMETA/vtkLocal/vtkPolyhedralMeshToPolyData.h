/*=========================================================================

 *	File: vtkPolyhedraMeshToPolyData.h
 *	Graph visualization library for VTK
 * 
 *	This software is distributed WITHOUT ANY WARRANTY; 
 *	without even the implied warranty of MERCHANTABILITY 
 *	or FITNESS FOR A PARTICULAR PURPOSE.  
 * 
 *	See the file copyright.txt for details.  

=========================================================================*/
// .NAME vtkPolyhedralMeshToPolyData - convert an unstructured grid of polyhedral cells into polydata.
// .SECTION Description
// vtkPolyhedralMeshToPolyData takes an unstructured grid containing 3D polyhedral cells and
// produces a vtkPolyData output containing the polygonal faces of the polyhedra.  Input points
// are copied to output and reused; cell data on each input polyhedron is copied to each of
// the resulting polygons.  An output array can be generated to track cell ids.
// 
// .SECTION Caveats:
// No tests are peformed on the sanity of the inputs.

#ifndef __vtkPolyhedralMeshToPolyData_h
#define __vtkPolyhedralMeshToPolyData_h

#include "vtkMETAModule.h"
#include "vtkPolyDataAlgorithm.h"


class VTK_META_EXPORT vtkPolyhedralMeshToPolyData : public vtkPolyDataAlgorithm
{
public:
  // Standard VTK filter components.
  static vtkPolyhedralMeshToPolyData *New();
  vtkTypeMacro(vtkPolyhedralMeshToPolyData,vtkPolyDataAlgorithm);
  void PrintSelf(ostream& os, vtkIndent indent);

  // Description:
  // Turn on flag to copy id of input polyhedron to output polygons.
  vtkSetMacro(GenerateCellIds, bool);
  vtkGetMacro(GenerateCellIds, bool);
  vtkBooleanMacro(GenerateCellIds, bool);

protected:
  vtkPolyhedralMeshToPolyData();
  ~vtkPolyhedralMeshToPolyData();

  // Override to specify support for any vtkDataSet input type.
  virtual int FillInputPortInformation(int port, vtkInformation* info);

  // Main implementation.
  virtual int RequestData(vtkInformation*,
                          vtkInformationVector**,
                          vtkInformationVector*);

  bool GenerateCellIds;

private:
  vtkPolyhedralMeshToPolyData(const vtkPolyhedralMeshToPolyData&);  // Not implemented.
  void operator=(const vtkPolyhedralMeshToPolyData&);  // Not implemented.
};

#endif

