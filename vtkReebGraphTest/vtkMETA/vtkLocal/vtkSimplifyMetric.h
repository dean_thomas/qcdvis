/*=========================================================================

 *	File: vtkSimplifyMetric.h
 *	Graph visualization library for VTK
 * 
 *	This software is distributed WITHOUT ANY WARRANTY; 
 *	without even the implied warranty of MERCHANTABILITY 
 *	or FITNESS FOR A PARTICULAR PURPOSE.  
 * 
 *	See the file copyright.txt for details.  

=========================================================================*/

// .NAME vtkSimplifyMetric - This filter takes the Reeb Skeleton Graph as input, and offers
//  two simplification metrics. 
// .SECTION Description
//  This filter takes the original Reeb Skeleton graph as input and outputs the simplified Reeb 
//  Skeleton graph. There are several rules are defined for simplification. 
//  These two metrics are based on Persistence area and Volume area. 
//  The persistence area can be computed by accumulating the number of JCN node in each regular sheet.  
//  The volume area can be computed by accumulating the number of fragments of JCN nodes in each
//  regular nodes. In addition to the metrics, we also have defined a list of simplification rules
//  based on the graph structure : 
//  Rule 1:  Change types of degree two singular nodes as regular, since they capture
//           only a minor topological feature.
//  Rule 2: Prune degree one singular nodes of the Reeb skeleton, since they correspond to the 
//          isolated boundary edges of the JCN (Reeb space) and does not really capture the
//          internal structure of the Reeb space. 
//  Rule 3: Prune degree one regular nodes if the metric associated with the nodes are below a
//          chosen threshold. If attached singular node is of degree two or less
//          (after pruning the regular node) then we change its type as regular using Rule 1.
//  Rule 4: Merge two consecutive regular nodes in the Reeb skeleton and connect their adjacent 
//          nodes with the new node.
//  The order and number of iterations of applying these rules are different in various 
//  data sets. It is up to the user to decide which rules are applied in the method 
//  ApplySimplicationRule(). The default rules are used for Physics scission data set. 
//  In order to set the persistence area metric, should use : 
//      this->SimplifyComponentConectivityGraphUpdate(n, "persistence"); 
//  In order to set the volume metric, should use:
//      this->SimplifyComponentConectivityGraphUpdate(n, "volume");  
//  where n is a random measure value

#ifndef __vtkSimplifyMetric_h
#define __vtkSimplifyMetric_h
#include "vtkAdjacentVertexIterator.h"
#include "vtkExtractSelection.h"
#include "vtkExtractSelectedGraph.h"
#include "vtkGraphAlgorithm.h"
#include "vtkMETAModule.h"
#include "vtkMutableGraphHelper.h"
#include "vtkMultiDimensionalReebGraph.h"
#include "vtkSmartPointer.h"
#include "vtkSelectionNode.h"
#include "vtkSelection.h"
#include "vtkSimplifyJCN.h"
#include <vector>
#include <set>


class vtkIdTypeArray;

class VTK_META_EXPORT vtkSimplifyMetric : public vtkGraphAlgorithm
{
public:
  static vtkSimplifyMetric* New();
  vtkTypeMacro(vtkSimplifyMetric, vtkGraphAlgorithm);
  void PrintSelf(ostream& os, vtkIndent indent);
  // Description :
  // Add filed name  
  void AddField(const char *nm);
  // Description :
  // Add slab width.   
  void AddStepSize(float size);
  // Description :  
  // Set the initial number of regular and singular components. 
  void SetRegCompNr(int regCompNr){RegCompNr = regCompNr;};
  void SetSingCompNr(int singCompNr){SingCompNr = singCompNr;};
  
  void SetJCNList(vtkSimplifyJCN *simplify);
  std::vector< std::vector<int> > GetJCNList();
  
  // Description:
  // Flat to determine if the simplification rules applied. 
  vtkSetMacro(ApplySimplify, bool);
  vtkGetMacro(ApplySimplify, bool);
  vtkBooleanMacro(ApplySimplify, bool);

  //	Implemented in this code: "persistence" or "volume"
  //std::string persistence_measure_type = "persistence";
  vtkSetMacro(persistence_measure_type, std::string);
  vtkGetMacro(persistence_measure_type, std::string);
  vtkSetMacro(persistence_threshold, float);
  vtkGetMacro(persistence_threshold, float);


protected:
  vtkSimplifyMetric();
  ~vtkSimplifyMetric();
  
  int FillInputPortInformation(int port, vtkInformation* info);
  
  // Generic method to compute the simplification metrics. 'metric' decides which 
  // simplification scheme to use. 'threshold' sets the threshold value.   
  int SimplifyComponentConectivityGraphUpdate(double threshold, char *metric);

  // Compute metrics based on Persistence area which measures the number of slabs
  // in each regular sheet. 
  int  ComputePersistenceMetric();

  // Compute metrics based on Volume of the node which measures the number of
  // fragments in each sheet. 
  int  ComputeVolumeMetric();

  // Given a node in input graph, decide the number of unique neighbours. 
  int GetNumberOfAdjacency(vtkGraph *inpugGraph, vtkIdType index);

  // Simplification rules : Prune degree one singular nodes of the Reeb skeleton
  void DeleteDegreeOneSingular();

  // Simplification rules : Merge two consecutive regular nodes in 
  // the Reeb skeleton and connect their adjacent nodes with the new node.
  void MergeDegreeOneRegular();

  // Merge two set of the nodes. Combine all their attribute values. 
  int MergeNodeCCG(std::vector<int> v,vtkIdTypeArray *nodeId);
  // Simplification rules : Change types of degree two singular nodes as regular  
  void FromSingularToRegular();

  void MergeJCNId(vtkIdType FromCompNode, vtkIdType ToCompNode);
  // Set your simplification rules here. The default one is used for Scission data 
  // simplification.  
  void ApplySimplicationRule();
  // Given a node, return its nique neighbours. 
  std::vector<int> GetAdjacency(vtkGraph *inputGraph, vtkIdType index);

  int RequestData(
    vtkInformation*, 
    vtkInformationVector**, 
    vtkInformationVector*);

  // Name of the fields of input data.  
  std::vector<char*> Fields;

  // Slab width of each field. 
  std::vector<float> StepSize;

  // The input Reeb Skeleton graph
  vtkSmartPointer<vtkMutableUndirectedGraph> ComponentConnectivityGraph;   
  
  vtkSmartPointer<vtkSelectionNode> SelectionNode;
  vtkSmartPointer<vtkSelection> Selection;
  vtkSmartPointer<vtkExtractSelectedGraph> ExtractSelection;

  // The number of regular and singular components. 
  int RegCompNr, SingCompNr;
  
  // Apply the simplification scheme
  bool ApplySimplify;
  
  std::vector< std::vector<int> > JCNList;

  std::string persistence_measure_type = "persistence";
  float persistence_threshold = 0.1;
private:
  vtkSimplifyMetric(const vtkSimplifyMetric&);   // Not implemented
  void operator=(const vtkSimplifyMetric&);   // Not implemented
};

#endif

