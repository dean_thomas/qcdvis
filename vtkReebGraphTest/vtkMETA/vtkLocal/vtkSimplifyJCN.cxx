/*=========================================================================

 *	File: vtkSimplifyJCN.cxx
 *	Graph visualization library for VTK
 * 
 *	This software is distributed WITHOUT ANY WARRANTY; 
 *	without even the implied warranty of MERCHANTABILITY 
 *	or FITNESS FOR A PARTICULAR PURPOSE.  
 * 
 *	See the file copyright.txt for details.  

=========================================================================*/
#include "vtkSimplifyJCN.h"
#include "vtkMultiDimensionalReebGraph.h"
#include "vtkDataSetAttributes.h"
#include "vtkIdTypeArray.h"
#include "vtkBitArray.h"
#include "vtkFloatArray.h"
#include "vtkIntArray.h"
#include "vtkInformation.h"
#include "vtkMutableDirectedGraph.h"
#include "vtkMutableUndirectedGraph.h"
#include "vtkMutableGraphHelper.h"
#include "vtkAdjacentVertexIterator.h"
#include "vtkGraphEdge.h"
#include "vtkObjectFactory.h"
#include "vtkBoostConnectedComponents.h"
#include "vtkNew.h"
#include <queue>
#include <stack>
#include <algorithm>
#include "vtkVoidArray.h"
#include "vtkTreeBFSIterator.h"

vtkStandardNewMacro(vtkSimplifyJCN);

#define VTK_CREATE(type,name) \
  vtkSmartPointer<type> name = vtkSmartPointer<type>::New()


//-----------------------------------------------------------------------
vtkSimplifyJCN::vtkSimplifyJCN()
{
  this->SetNumberOfOutputPorts(1);
  this->Fields = std::vector<char*>();
  RegularComponents=NULL;
  SingularComponents=NULL;
  ComponentConnectivityGraph=NULL;
  this->RegCompNr = this->SingCompNr = -1;
  this->JacobiStructure = vtkSmartPointer<vtkIdTypeArray>::New();
}

vtkSimplifyJCN::~vtkSimplifyJCN()
{
  while (this->Fields.size() > 0)
      {
      delete [] this->Fields.back();
      this->Fields.pop_back();
      }
  this->Fields.clear();
}

void vtkSimplifyJCN::AddField(const char *nm)
{
  char *f = new char[strlen(nm) + 1];
  strcpy(f, nm);
  this->Fields.push_back(f);
}

void vtkSimplifyJCN::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os, indent);
}

int vtkSimplifyJCN::FillInputPortInformation(int port, vtkInformation* info)
{
  if (port == 0)
    {
    info->Set(vtkAlgorithm::INPUT_REQUIRED_DATA_TYPE(), "vtkGraph");
    return 1;
    }
    
  return 0;
}
//--------------------------------------------------------------------------------------------------------------
void vtkSimplifyJCN::PrintGraph(vtkGraph *graph)
{
  vtkDataSetAttributes *inPD   = graph->GetVertexData(); 
  vtkDataArray** property = new vtkDataArray*[Fields.size()];
  for (int f=0; f<Fields.size(); f++)
     {
     property[f] = inPD->GetArray(this->Fields[f]);  
     }
     
  std::cout<< "\n << Print Graph  >>   "<<endl;
  std::cout<< "Vertices and Field Value(s)."<< endl;
  for (vtkIdType i = 0; i < graph->GetNumberOfVertices(); i++)
      {
      std::cout<<"  "<<i<<"    [";
      double* prop = new double[Fields.size()];
      for (int f=0; f<Fields.size(); f++)
         {
         prop[f]=property[f]->GetComponent(i, 0);
         std::cout <<"  "<<prop[f];
         }
     std::cout<<" ]";

     vtkSmartPointer<vtkAdjacentVertexIterator> adj =  vtkSmartPointer<vtkAdjacentVertexIterator>::New();
     graph->GetAdjacentVertices(i, adj);
     std::cout<<"  adj-nodes: ";
     while (adj->HasNext())
          {
          std::cout<<" "<<adj->Next();
          }

     std::cout<<endl;	
     }

  std::cout<<"\nEdges   From    To :: (number of edges: "<<graph->GetNumberOfEdges()<<")"<<endl;
  for (vtkIdType i = 0; i < graph->GetNumberOfEdges(); i++)
     {
     vtkIdType source=graph->GetSourceVertex(i);
     vtkIdType target=graph->GetTargetVertex(i);
     std::cout <<"  "<<i<<"      "<<source<<"      "<<target<< endl;
     }
  std::cout<<"--------------------------------------------------"<<endl;
}

//----------------------------------------------------------------------------
//print  components of a disconnected graph created by vtkBoostConnectedComponents
void vtkSimplifyJCN::PrintGraphComponents(vtkGraph *graph)
{
  vtkDataSetAttributes *inPD   = graph->GetVertexData(); 
  vtkDataArray** property = new vtkDataArray*[Fields.size()];
  for (int f=0; f<Fields.size(); f++)
     {
     property[f] = inPD->GetArray(this->Fields[f]);  
     }
  vtkIntArray *componentids = vtkIntArray::SafeDownCast(inPD->GetArray("component"));

  for (vtkIdType i = 0; i < graph->GetNumberOfVertices(); i++)
     {
     std::cout<<" Comp-Id "<<componentids->GetComponent(i,0);
     std::cout<<" Id: "<<i<<"    [";
     double* prop = new double[Fields.size()];
     for (int f=0; f<Fields.size(); f++)
        {
        prop[f]=property[f]->GetComponent(i, 0);
        std::cout <<"  "<<prop[f];
        }
     std::cout<<" ]";
     vtkSmartPointer<vtkAdjacentVertexIterator> adj = vtkSmartPointer<vtkAdjacentVertexIterator>::New();
     graph->GetAdjacentVertices(i, adj);
     std::cout<<"  adj-nodes: ";
     while (adj->HasNext())
          {
          std::cout<<" "<<adj->Next();
          }
     std::cout<<endl;	
     }
   
  std::cout<<"\nEdges   From    To :: (number of edges: "<<graph->GetNumberOfEdges()<<")"<<endl;
  std::cout<<"--------------------------------------------------"<<endl;
}

//---------------------------------------------------------------------------
//This function will create a component connectivity graph and related fields in its nodes
void vtkSimplifyJCN::CreateComponentConnectivityGraph(vtkGraph *inputGraph)
{
  int numRegComp=GetNrBoostGraphComponents(this->RegularComponents);
  int numSingComp=GetNrBoostGraphComponents(this->SingularComponents);
  int numNodes=numRegComp+numSingComp;

  VTK_CREATE(vtkMutableGraphHelper, builder);
  VTK_CREATE(vtkMutableUndirectedGraph, undir);
  builder->SetGraph(undir);

  VTK_CREATE(vtkIdTypeArray, nodeType);
  nodeType->SetName("nodeType");
  nodeType->SetNumberOfValues(numNodes);

  VTK_CREATE(vtkIdTypeArray, compId);
  compId->SetName("compId");
  compId->SetNumberOfValues(numNodes);

  for (int i=0; i<numRegComp; i++)
     {
     builder->AddVertex();
     nodeType->SetValue(i, 0);
     compId->SetValue(i,i);
     }

  for (int i=0; i<numSingComp; i++)
     {
     builder->AddVertex();
     nodeType->SetValue(numRegComp+i, 1);
     compId->SetValue(numRegComp+i,numRegComp+i);
     }

  for (int i=0; i<numRegComp; i++)
     {
     for (int j=0; j<numSingComp; j++)
        {
        if (IsConnected(i, j, inputGraph))
          {
          builder->AddEdge(i,numRegComp+j);
          }
        }
     }

  undir->GetVertexData()->AddArray(nodeType);
  undir->GetVertexData()->AddArray(compId);


  //creating node-size array
  VTK_CREATE(vtkFloatArray, nodeSize);
  nodeSize->SetName("size");
  nodeSize->SetNumberOfValues(numNodes);

  double* sizeArr = new double[numNodes];
  for(int i=0; i<numNodes; i++) 
  sizeArr[i]=0.0;
  for (int i=0; i<this->RegularComponents->GetNumberOfVertices(); i++)
     {
     int compId=RegularComponents->GetVertexData()->GetArray("component")->GetComponent(i,0);
     sizeArr[compId] += RegularComponents->GetVertexData()->GetArray("size")->GetComponent(i,0);
     }

  for (int i=0; i<this->SingularComponents->GetNumberOfVertices(); i++)
     { 
     int compId=SingularComponents->GetVertexData()->GetArray("component")->GetComponent(i,0);
     sizeArr[numRegComp+compId] += SingularComponents->GetVertexData()->GetArray("size")->GetComponent(i,0);
     }
   
  for (int i=0; i<numNodes; i++)
     {
     nodeSize->SetValue(i, sizeArr[i]);
     }
  undir->GetVertexData()->AddArray(nodeSize);


  //creating array(s) of field-values
  int numFields=this->Fields.size();
  double* fieldArr = new double[numNodes];
  for (int f=0; f<numFields; f++)
     {
     VTK_CREATE(vtkFloatArray, field);
     field->SetName(this->Fields[f]);
     field->SetNumberOfValues(numNodes);

     for (int i=0; i<numNodes; i++) 
        {
        fieldArr[i]=0.0;
        }
        
     for (int i=0; i<this->RegularComponents->GetNumberOfVertices(); i++)
        {
        int compId= RegularComponents->GetVertexData()->GetArray("component")->GetComponent(i,0);
        fieldArr[compId] += RegularComponents->GetVertexData()->GetArray(this->Fields[f])->GetComponent(i,0);
        }
     for (int i=0; i<this->SingularComponents->GetNumberOfVertices(); i++)
        {
        int compId=SingularComponents->GetVertexData()->GetArray("component")->GetComponent(i,0);
        fieldArr[numRegComp+compId] += SingularComponents->GetVertexData()->GetArray(this->Fields[f])->GetComponent(i,0);
        }
     for (int i=0; i<numNodes; i++)
        {
        field->SetValue(i, fieldArr[i]);
        }
      undir->GetVertexData()->AddArray(field);
      //delete field;
    }
  this->ComponentConnectivityGraph=undir;
  this->MergeJacobiNodes();
}

//-----------------------------

void vtkSimplifyJCN::MergeJacobiNodes()
{
  int nrVertices = this->ComponentConnectivityGraph->GetNumberOfVertices();
  vtkSmartPointer<vtkAdjacentVertexIterator> it = vtkSmartPointer<vtkAdjacentVertexIterator>::New();

  std::vector< std::vector<int> > adjacencyList;
  std::vector< std::vector<int> > indexList;

  for ( int i = 0; i < nrVertices; i++)
     {
     this->ComponentConnectivityGraph->GetAdjacentVertices(i,it);    
     std::vector<int> adjacency;
     while (it->HasNext())
          {
          adjacency.push_back(it->Next());
          }
     std::sort(adjacency.begin(), adjacency.end());
     if (adjacencyList.size() == 0 ) 
       {
       adjacencyList.push_back(adjacency);
       std::vector<int> index;
       index.push_back(i);
       indexList.push_back(index);
       }
     else 
       {
       bool contains = false;
       for (int j = 0; j < adjacencyList.size(); j++)
          {
          if (adjacencyList[j].size() == adjacency.size()) 
            {
            if (adjacencyList[j] == adjacency)
              {
              contains = true;
              indexList[j].push_back(i);
              break;
              }
             }
           }
        if (!contains) 
          {
          adjacencyList.push_back(adjacency);
          std::vector<int> index;
          index.push_back(i);
          indexList.push_back(index);
          }
        }        
     }
          
  vtkSmartPointer<vtkIdTypeArray> deleteNodes = vtkSmartPointer<vtkIdTypeArray>::New();
      
  int* jcnId = new int[nrVertices];
  for (int i = 0; i < nrVertices; i++) 
     {
     jcnId[i] = i;
     }
     
  int deleteRegCompNr, deleteSingCompNr;
  deleteRegCompNr = deleteSingCompNr = 0;
    
  for (int i = 0; i < indexList.size(); i++)
     {
     if (indexList[i].size() > 1 && adjacencyList[i].size()>0)
       {
       int size = this->ComponentConnectivityGraph->GetVertexData()->GetArray("size")->GetComponent(indexList[i][0],0);
       for (int j = 1; j < indexList[i].size(); j++)
          {
          int removeNodeIndex = indexList[i][j];
          size += this->ComponentConnectivityGraph->GetVertexData()->GetArray("size")->GetComponent(removeNodeIndex,0);
          int type = this->ComponentConnectivityGraph->GetVertexData()->GetArray("nodeType")->GetComponent(removeNodeIndex,0);
          if (type==0) 
            {
            deleteRegCompNr++;
            }
          else
            {
            deleteSingCompNr++;
            }
          jcnId[removeNodeIndex] = indexList[i][0];
          deleteNodes->InsertNextValue(removeNodeIndex);
          }  
        this->ComponentConnectivityGraph->GetVertexData()->GetArray("size")->SetComponent(indexList[i][0],0,size);
       }
     }
         
  this->SingCompNr -= deleteSingCompNr;
  this->RegCompNr -= deleteRegCompNr;
      
  this->ComponentConnectivityGraph->RemoveVertices(deleteNodes);

  std::vector< std::vector<int> > tempId;
           
  int compVerticeNr = this->ComponentConnectivityGraph->GetNumberOfVertices();
      
  for (int i = 0; i < compVerticeNr ; i++)
     {
     std::vector<int> value; 
     tempId.push_back(value);
     }
      
  for (int i = 0; i < compVerticeNr; i++)
     {
     int index = this->ComponentConnectivityGraph->GetVertexData()->GetArray("compId")->GetComponent(i,0);
     for (int j = 0; j < nrVertices; j++)
        {
        int tempIndex = jcnId[j];
        if (tempIndex >= 0)
          { 
          if (index == tempIndex) 
            {
            jcnId[j] = -1;
            for (int k = 0; k < this->JCNList[j].size(); k++)
               {
               tempId[i].push_back(this->JCNList[j][k]);
               }
             }
           }
        }                 
      }  

  this->JCNList.clear();
  this->JCNList = tempId;
}



//---------------------------------------------------------------------------
//Extracts regular and singular components from the input graph
void vtkSimplifyJCN::ExtractRegularAndSingularComponents(vtkGraph *inputGraph)
{
  //Computing "Regular" components of JCN (remove Jacobi Nodes from JCN))
  VTK_CREATE(vtkMutableUndirectedGraph, partitionGraph);
  partitionGraph->Initialize();
  partitionGraph->DeepCopy(inputGraph);
  partitionGraph->RemoveVertices(this->JacobiStructure);
  VTK_CREATE(vtkBoostConnectedComponents, comp);
  comp->SetInputData(partitionGraph);
  comp->Update();
  this->RegularComponents = comp->GetOutput();
  this->RegCompNr = GetNrBoostGraphComponents(this->RegularComponents);

  //Computing "Singular" Components of JCN (remove Regular Nodes from JCN)
  VTK_CREATE(vtkIdTypeArray, regularNodes);
  vtkDataSetAttributes *partPD   = partitionGraph->GetVertexData(); 
  vtkDataArray *ids = partPD->GetArray("jcnids");  
  //make list of regular nodes
  for (vtkIdType i=0; i<partitionGraph->GetNumberOfVertices();  i++)
     {
     regularNodes->InsertNextValue(ids->GetComponent(i, 0));
     }

  VTK_CREATE(vtkMutableUndirectedGraph, partitionGraph2);
  partitionGraph2->Initialize();
  partitionGraph2->DeepCopy(inputGraph);
  partitionGraph2->RemoveVertices(regularNodes);
  VTK_CREATE(vtkBoostConnectedComponents, comp2);
  comp2->SetInputData(partitionGraph2);
  comp2->Update();
  this->SingularComponents = comp2->GetOutput();
  this->SingCompNr = GetNrBoostGraphComponents(this->SingularComponents);
}
//computes number of components in the graph created by vtkBoostGraphComponents
int vtkSimplifyJCN::GetNrBoostGraphComponents(vtkGraph *G)
{
  double *range = G->GetVertexData()->GetArray("component")->GetRange();
  int r=range[1]+1;
  return r;
}

//This function will check whether a regular component is conneced with a singular component in the JCN
bool vtkSimplifyJCN::IsConnected(int iReg, int jSing, vtkGraph *inputG)
{
  //Create union of two sub-graphs
  vtkSmartPointer<vtkIdTypeArray> extractNodes = vtkSmartPointer<vtkIdTypeArray>::New();
  for (int i = 0; i < this->JCNList[iReg].size(); i++)
     {
     extractNodes->InsertNextValue(this->JCNList[iReg][i]);
     }

  for (int j = 0; j < this->JCNList[this->RegCompNr+jSing].size(); j++)
     { 
     extractNodes->InsertNextValue(this->JCNList[this->RegCompNr+jSing][j]);
     }

  vtkSmartPointer<vtkSelectionNode> selectionNode = vtkSmartPointer<vtkSelectionNode>::New();
  selectionNode->SetFieldType(vtkSelectionNode::VERTEX);
  selectionNode->SetContentType(vtkSelectionNode::INDICES);
  
  selectionNode->SetSelectionList(extractNodes);  
  vtkSmartPointer<vtkSelection> selection = vtkSmartPointer<vtkSelection>::New();
  selection->AddNode(selectionNode);
   
  vtkSmartPointer<vtkExtractSelectedGraph> extractSelection = vtkSmartPointer<vtkExtractSelectedGraph>::New();
  extractSelection->SetInputData(0, inputG);
  extractSelection->SetInputData(1, selection);
  extractSelection->Update();
    
  vtkSmartPointer<vtkBoostConnectedComponents> compBoost = vtkSmartPointer<vtkBoostConnectedComponents>::New();
  compBoost->SetInputData(extractSelection->GetOutput());
  compBoost->Update();
  vtkGraph *G = compBoost->GetOutput();	
  int r=GetNrBoostGraphComponents(G);

  if (r>1)
    {
    return false;
    }
  else
    {
    return true;
    }
}
//
int vtkSimplifyJCN::GetComponentNodeIds()
{
  if (this->RegCompNr < 0 || this->SingCompNr < 0)
    {
    vtkErrorMacro("Component Connected Graph should be computed first");
    return 0;
    }
  vtkIdType nrVert = this->RegCompNr + this->SingCompNr;

  for (int i = 0; i < nrVert; i++)
     {
     std::vector<int> temp;
     this->JCNList.push_back(temp);
     }

  for (int i=0; i<this->RegularComponents->GetNumberOfVertices(); i++)
     {
     int compId= RegularComponents->GetVertexData()->GetArray("component")->GetComponent(i,0);
     int index = RegularComponents->GetVertexData()->GetArray("jcnids")->GetComponent(i,0);
     this->JCNList[compId].push_back(index);
     }

  for (int i=0; i<this->SingularComponents->GetNumberOfVertices(); i++)
     {
     int compId= SingularComponents->GetVertexData()->GetArray("component")->GetComponent(i,0);
     int index = SingularComponents->GetVertexData()->GetArray("jcnids")->GetComponent(i,0);
     this->JCNList[this->RegCompNr+compId].push_back(index);
     }
  return 1;
}

std::vector< std::vector<int> > vtkSimplifyJCN::GetJCNList()
{
  return this->JCNList;
}

double vtkSimplifyJCN::ComputeComponentVolume(vtkGraph *G, vtkIdType compId)
{
  VTK_CREATE(vtkFloatArray, sizes);

  //	TODO: ...
  return 0.0;
}

int vtkSimplifyJCN::GetRegCompNr()
{
  return this->RegCompNr;
}

int vtkSimplifyJCN::GetSingCompNr()
{
  return this->SingCompNr;
}


//----------------------------------------------------------------------------
int vtkSimplifyJCN::RequestData(
  vtkInformation* vtkNotUsed(request),
  vtkInformationVector** inputVector, 
  vtkInformationVector* outputVector)
{
  // Ensure we have valid inputs ...
  vtkGraph* const inputGraph = vtkGraph::GetData(inputVector[0]);
  vtkGraph* const outputGraph = vtkGraph::GetData(outputVector);

  if (!inputGraph)
    {
    vtkErrorMacro("A vtkGraph object is required.");
    return 0;
    }

  vtkIdType nrVertices = inputGraph->GetNumberOfVertices();
  if (!nrVertices)
    {
    vtkErrorMacro("Empty graph, no vertices.");
    return 0;
    }

  if (!this->Fields[0])
    {
    vtkErrorMacro("No field specified.");
    return 0;
    }

  //Setting JCN-ids
  VTK_CREATE(vtkIdTypeArray, nodeIds);
  nodeIds->SetName("jcnids");
  nodeIds->SetNumberOfValues(nrVertices);
  for (vtkIdType i = 0; i < nrVertices; i++)
     {
     nodeIds->SetValue(i, i);//copying the origical JCN-ids!	
     }
     
  inputGraph->GetVertexData()->AddArray(nodeIds);//Adding JCN-ids in Input Graph; This will be removed in the end	

  ExtractRegularAndSingularComponents(inputGraph);
  GetComponentNodeIds();
  CreateComponentConnectivityGraph(inputGraph);
  //Memory allocate
  vtkIdType nrVert = this->ComponentConnectivityGraph->GetNumberOfVertices();

  vtkSmartPointer<vtkIntArray> value[10];
  for (int f = 0; f < this->Fields.size(); f++)
     {
     value[f] = vtkSmartPointer<vtkIntArray>::New();
     value[f]->SetNumberOfTuples(nrVert);
     value[f]->SetName(this->Fields[f]);
     }

  VTK_CREATE(vtkIdTypeArray, typeArray);
  typeArray=vtkIdTypeArray::SafeDownCast(this->ComponentConnectivityGraph->GetVertexData()->GetArray("nodeType"));

  //Setting field-values for Jacobi-nodes, 0: White, 1: Red, 2: Blue, 3: Green
  for (int i = 0; i < nrVert; i++)
     {
     if (typeArray->GetComponent(i,0)==1)
       {
       for (int f = 0; f < this->Fields.size(); f++)
          {
          value[f]->SetValue(i,1);//red
          }
       }
     else
       {
       for (int f = 0; f < this->Fields.size(); f++)
          {
          value[f]->SetValue(i,2);//blue
          }
       }
      }


  vtkDataSetAttributes *inPD = this->ComponentConnectivityGraph->GetVertexData(), *outPD  = outputGraph->GetVertexData();
  outputGraph->DeepCopy(this->ComponentConnectivityGraph);
    
  vtkSmartPointer<vtkIdTypeArray> jcnListArray = vtkSmartPointer<vtkIdTypeArray>::New();
  jcnListArray->SetName("jcnList"); 
  for (int i = 0; i < this->JCNList.size(); i++)
     {
     jcnListArray->InsertNextValue(this->JCNList[i].size());
     }
   outputGraph->GetVertexData()->AddArray(jcnListArray);

  outPD->CopyAllocate(inPD, nrVert, 10);	
  for (vtkIdType i = 0; i < nrVert; i++)
     {
     outPD->CopyData(this->ComponentConnectivityGraph->GetVertexData(), i, i);
     }

  //Added for coloring component connectivity graph nodes
  for (int f = 0; f < this->Fields.size(); f++)
     {
     //inPD->RemoveArray(this->Fields[f]);
     outPD->AddArray(value[f]);
     }
     
  outputGraph->Squeeze(); 
  return 1;
}

void vtkSimplifyJCN::AddJacobiStructure(vtkBitArray *input)
{
  for (int i = 0; i < input->GetNumberOfTuples(); i++)
     {
     if (input->GetValue(i))
       {
       this->JacobiStructure->InsertNextValue(i);
       }
     }
}

//--------------------------------------------------------------------------------------
