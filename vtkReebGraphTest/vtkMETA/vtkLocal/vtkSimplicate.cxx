/*=========================================================================

 *	File: vtkSimplicate.cxx
 *	Graph visualization library for VTK
 *
 *	This software is distributed WITHOUT ANY WARRANTY;
 *	without even the implied warranty of MERCHANTABILITY
 *	or FITNESS FOR A PARTICULAR PURPOSE.
 *
 *	See the file copyright.txt for details.

=========================================================================*/
#include "vtkCell.h"
#include "vtkCellData.h"
#include "vtkCellType.h"
#include "vtkImageData.h"
#include "vtkIdList.h"
#include "vtkInformation.h"
#include "vtkInformationVector.h"
#include "vtkIdTypeArray.h"
#include "vtkSimplicate.h"
#include "vtkSmartPointer.h"
#include "vtkObjectFactory.h"
#include "vtkPointData.h"
#include "vtkUnstructuredGrid.h"
#include "vtkMath.h"
#include "vtkFloatArray.h"


#define CellIndex(i, j, k)  (dim[0] * (dim[1] * (k) + (j)) + (i))
#define CellIndex2(i,j)    (dimA * (j) + (i))

//#define VERBOSE

vtkStandardNewMacro(vtkSimplicate);

//----------------------------------------------------------------------------
vtkSimplicate::vtkSimplicate()
{
	this->Scheme = 0;
	this->IsPeriodicX = false;
	this->IsPeriodicY = false;
	this->IsPeriodicZ = false;
}

//----------------------------------------------------------------------------
vtkSimplicate::~vtkSimplicate()
{
}

//----------------------------------------------------------------------------
void vtkSimplicate::PrintSelf(ostream& os, vtkIndent indent)
{
	this->Superclass::PrintSelf(os, indent);
	cout << indent << "Simplification scheme: " << this->Scheme << endl;
}

//----------------------------------------------------------------------------
int vtkSimplicate::FillInputPortInformation(int, vtkInformation* info)
{
	// This filter uses the vtkDataSet cell traversal methods so it
	// suppors any data set type as input.
	info->Set(vtkAlgorithm::INPUT_REQUIRED_DATA_TYPE(), "vtkImageData");
	return 1;
}


//----------------------------------------------------------------------------
int vtkSimplicate::RequestData(vtkInformation*,
							   vtkInformationVector** inputVector,
							   vtkInformationVector* outputVector)
{
//#define IS_PERIODIC

	
	// Get input and output data.
	vtkImageData* input = vtkImageData::GetData(inputVector[0]);
	vtkUnstructuredGrid* output = vtkUnstructuredGrid::GetData(outputVector);

	// Skip execution if there is no input geometry.
	vtkIdType numCells = input->GetNumberOfCells();
	vtkIdType numPts = input->GetNumberOfPoints();

	vtkSmartPointer<vtkIdTypeArray> nbs
		= vtkSmartPointer<vtkIdTypeArray>::New();

	int dim[4];
	vtkIdType pntIds[4];

	input->GetDimensions(dim);

	pntIds[0] = CellIndex(0, 0, 0);


	if (numCells < 1 || numPts < 1)
	{
		vtkDebugMacro("No input data.");
		return 1;
	}

	if (input->GetDataDimension() == 3)
	{
		output->Allocate(numCells * 6);
		nbs->SetNumberOfComponents(4);
		nbs->SetNumberOfTuples(numCells);

		bool render;
		double p0, p1, p2, p3;

		//	Extract data dimensions from input
		auto dimI = dim[0];
		auto dimJ = dim[1];
		auto dimK = dim[2];

		//	Calculate the maximum on each axis
		auto maxI = dimI - 1;
		auto maxJ = dimJ - 1;
		auto maxK = dimK - 1;

		//	If the data is marked periodic, we will add an extra
		//	cell at the end of each axis that will wrap to the origin.
		if (IsPeriodicX) ++maxI;
		if (IsPeriodicY) ++maxJ;
		if (IsPeriodicZ) ++maxK;


		if (IsPeriodicX || IsPeriodicY || IsPeriodicZ)
		{
			cout << "Input data is periodic in: "
				<< (IsPeriodicX ? "X" : "")
				<< (IsPeriodicY ? "Y" : "")
				<< (IsPeriodicZ ? "Z" : "")
				<< "." << endl;
		}
		else
		{
			cout << "Input data non-periodic." << endl;
		}

		switch (this->Scheme)
		{
			case 0:
			nbs->SetNumberOfTuples(numCells * 6);
			//cout << "Data dimensions: " << dimI << ", " << dimJ << ", " << dimK << endl;

			//getchar();


			for (auto k = 0; k < maxK; k++)
			{
				//	Will also support periodic axis
				auto kNext = (k + 1) % dimK;
				for (auto j = 0; j < maxJ; j++)
				{
					//	Will also support periodic axis
					auto jNext = (j + 1) % dimJ;
					for (auto i = 0; i < maxI; i++)
					{
						//	Will also support periodic axis
						auto iNext = (i + 1) % dimI;
						
						//	Common vertex definitions (also used in other programs)
						const auto v_0 = CellIndex(i, j, k);
						const auto v_1 = CellIndex(iNext, j, k);
						const auto v_2 = CellIndex(i, j, kNext);
						const auto v_3 = CellIndex(iNext, j, kNext);
						const auto v_4 = CellIndex(i, jNext, k);
						const auto v_5 = CellIndex(iNext, jNext, k);
						const auto v_6 = CellIndex(i, jNext, kNext);
						const auto v_7 = CellIndex(iNext, jNext, kNext);

#ifdef VERBOSE
						//	t_A0
						cout << "Creating tetrahedral cell using layout t_A0: " << endl;
						cout << "\tv_0 = { " << i		<< ", "	<< jNext << ", " << k		<< "}" << endl;
						cout << "\tv_1 = { " << iNext	<< ", " << jNext << ", " << k		<< "}" << endl;
						cout << "\tv_2 = { " << iNext << ", " << jNext << ", " << kNext	<< "}" << endl;
						cout << "\tv_3 = { " << i		<< ", " << j		<< ", " << k		<< "}" << endl;
#endif
						pntIds[0] = v_4;
						pntIds[1] = v_5;
						pntIds[2] = v_7;
						pntIds[3] = v_0;

						render = true;
						for (auto m = 0; m < input->GetPointData()->GetNumberOfArrays(); m++)
						{
							p0 = input->GetPointData()->GetArray(m)->GetComponent(pntIds[0], 0);
							p1 = input->GetPointData()->GetArray(m)->GetComponent(pntIds[1], 0);
							p2 = input->GetPointData()->GetArray(m)->GetComponent(pntIds[2], 0);
							p3 = input->GetPointData()->GetArray(m)->GetComponent(pntIds[3], 0);
							if (vtkMath::IsNan(p0) || vtkMath::IsNan(p1) || vtkMath::IsNan(p2)
								|| vtkMath::IsNan(p3))
							{
								render = false;
								break;
							}
						}

						if (render)
						{
							output->InsertNextCell(VTK_TETRA, 4, pntIds);
						}
#ifdef VERBOSE
						//	t_A1
						cout << "Creating tetrahedral cell using layout t_A1: " << endl;
						cout << "\tv_0 = { " << i		<< ", " << j		<< ", " << k		<< "}" << endl;
						cout << "\tv_1 = { " << iNext	<< ", " << j		<< ", " << k		<< "}" << endl;
						cout << "\tv_2 = { " << iNext	<< ", " << jNext << ", " << kNext << "}" << endl;
						cout << "\tv_3 = { " << iNext	<< ", " << jNext << ", " << k		<< "}" << endl;
#endif
						pntIds[0] = v_0;
						pntIds[1] = v_1;
						pntIds[2] = v_7;
						pntIds[3] = v_5;

						render = true;
						for (auto m = 0; m < input->GetPointData()->GetNumberOfArrays(); m++)
						{
							p0 = input->GetPointData()->GetArray(m)->GetComponent(pntIds[0], 0);
							p1 = input->GetPointData()->GetArray(m)->GetComponent(pntIds[1], 0);
							p2 = input->GetPointData()->GetArray(m)->GetComponent(pntIds[2], 0);
							p3 = input->GetPointData()->GetArray(m)->GetComponent(pntIds[3], 0);
							if (vtkMath::IsNan(p0) || vtkMath::IsNan(p1) || vtkMath::IsNan(p2)
								|| vtkMath::IsNan(p3))
							{
								render = false;
								break;
							}
						}

						if (render)
						{
							output->InsertNextCell(VTK_TETRA, 4, pntIds);
						}
#ifdef VERBOSE
						//	t_A2
						cout << "Creating tetrahedral cell using layout t_A2: " << endl;
						cout << "\tv_0 = { " << i		<< ", "	<< j		<< ", " << k		<< "}" << endl;
						cout << "\tv_1 = { " << iNext << ", " << j		<< ", " << k		<< "}" << endl;
						cout << "\tv_2 = { " << iNext << ", " << jNext	<< ", " << kNext	<< "}" << endl;
						cout << "\tv_3 = { " << iNext	<< ", " << j		<< ", "	<< kNext << "}" << endl;
#endif
						pntIds[0] = v_0;
						pntIds[1] = v_1;
						pntIds[2] = v_7;
						pntIds[3] = v_3;

						render = true;
						for (auto m = 0; m < input->GetPointData()->GetNumberOfArrays(); m++)
						{
							p0 = input->GetPointData()->GetArray(m)->GetComponent(pntIds[0], 0);
							p1 = input->GetPointData()->GetArray(m)->GetComponent(pntIds[1], 0);
							p2 = input->GetPointData()->GetArray(m)->GetComponent(pntIds[2], 0);
							p3 = input->GetPointData()->GetArray(m)->GetComponent(pntIds[3], 0);
							if (vtkMath::IsNan(p0) || vtkMath::IsNan(p1) || vtkMath::IsNan(p2)
								|| vtkMath::IsNan(p3))
							{
								render = false;
								break;
							}
						}

						if (render)
						{
							output->InsertNextCell(VTK_TETRA, 4, pntIds);
						}
#ifdef VERBOSE
						//	t_A3
						cout << "Creating tetrahedral cell using layout t_A3: " << endl;
						cout << "\tv_0 = { " << i		<< ", " << jNext << ", " << kNext << "}" << endl;
						cout << "\tv_1 = { " << iNext << ", " << jNext << ", " << kNext << "}" << endl;
						cout << "\tv_2 = { " << i		<< ", " << jNext << ", " << k		<< "}" << endl;
						cout << "\tv_3 = { " << i		<< ", " << j		<< ", " << k		<< "}" << endl;
#endif
						pntIds[0] = v_6;  // e
						pntIds[1] = v_7;  // f
						pntIds[2] = v_4;    // a
						pntIds[3] = v_0;    // c

						render = true;
						for (auto m = 0; m < input->GetPointData()->GetNumberOfArrays(); m++)
						{
							p0 = input->GetPointData()->GetArray(m)->GetComponent(pntIds[0], 0);
							p1 = input->GetPointData()->GetArray(m)->GetComponent(pntIds[1], 0);
							p2 = input->GetPointData()->GetArray(m)->GetComponent(pntIds[2], 0);
							p3 = input->GetPointData()->GetArray(m)->GetComponent(pntIds[3], 0);
							if (vtkMath::IsNan(p0) || vtkMath::IsNan(p1) || vtkMath::IsNan(p2)
								|| vtkMath::IsNan(p3))
							{
								render = false;
								break;
							}
						}
						if (render)
						{
							output->InsertNextCell(VTK_TETRA, 4, pntIds);
						}
#ifdef VERBOSE
						//	t_A4
						cout << "Creating tetrahedral cell using layout t_A4: " << endl;
						cout << "\tv_0 = { " << i		<< ", " << jNext << ", " << kNext << "}" << endl;
						cout << "\tv_1 = { " << i		<< ", " << j		<< ", " << kNext << "}" << endl;
						cout << "\tv_2 = { " << i		<< ", " << j		<< ", " << k 		<< "}" << endl;
						cout << "\tv_3 = { " << iNext << ", " << jNext << ", " << kNext << "}" << endl;
#endif
						pntIds[0] = v_6;  // e
						pntIds[1] = v_2;  // g
						pntIds[2] = v_0;    // c
						pntIds[3] = v_7;  // f

						render = true;
						for (auto m = 0; m < input->GetPointData()->GetNumberOfArrays(); m++)
						{
							p0 = input->GetPointData()->GetArray(m)->GetComponent(pntIds[0], 0);
							p1 = input->GetPointData()->GetArray(m)->GetComponent(pntIds[1], 0);
							p2 = input->GetPointData()->GetArray(m)->GetComponent(pntIds[2], 0);
							p3 = input->GetPointData()->GetArray(m)->GetComponent(pntIds[3], 0);
							if (vtkMath::IsNan(p0) || vtkMath::IsNan(p1) || vtkMath::IsNan(p2)
								|| vtkMath::IsNan(p3))
							{
								render = false;
								break;
							}
						}

						if (render)
						{
							output->InsertNextCell(VTK_TETRA, 4, pntIds);
						}
#ifdef VERBOSE
						//	t_A5
						cout << "Creating tetrahedral cell using layout t_A5: " << endl;
						cout << "\tv_0 = { " << i		<< ", "	<< j		<< ", " << k		<< "}" << endl;
						cout << "\tv_1 = { " << i		<< ", " << j		<< ", " << kNext << "}" << endl;
						cout << "\tv_2 = { " << iNext << ", " << jNext << ", " << kNext << "}" << endl;
						cout << "\tv_3 = { " << iNext << ", " << j		<< ", " << kNext << "}" << endl;
#endif
						pntIds[0] = v_0;  // c
						pntIds[1] = v_2;  // g
						pntIds[2] = v_7;  // f
						pntIds[3] = v_3;  // h

						render = true;
						for (auto m = 0; m < input->GetPointData()->GetNumberOfArrays(); m++)
						{
							p0 = input->GetPointData()->GetArray(m)->GetComponent(pntIds[0], 0);
							p1 = input->GetPointData()->GetArray(m)->GetComponent(pntIds[1], 0);
							p2 = input->GetPointData()->GetArray(m)->GetComponent(pntIds[2], 0);
							p3 = input->GetPointData()->GetArray(m)->GetComponent(pntIds[3], 0);
							if (vtkMath::IsNan(p0) || vtkMath::IsNan(p1) || vtkMath::IsNan(p2)
								|| vtkMath::IsNan(p3))
							{
								render = false;
								break;
							}
						}
						if (render)
						{
							output->InsertNextCell(VTK_TETRA, 4, pntIds);
						}
					}
				}
			}
			break;

			case 1:
			nbs->SetNumberOfTuples(numCells * 5);
			for (int k = 0; k < maxK; k++)
			{
				auto kNext = (k + 1) % dimK;
				for (int j = 0; j < maxJ; j++)
				{
					auto jNext = (j + 1) % dimJ;
					for (vtkIdType i = 0; i < maxI; i++)
					{
						auto iNext = (i + 1) % dimI;
						
						//	Common vertex definitions (also used in other programs)
						const auto v_0 = CellIndex(i, j, k);
						const auto v_1 = CellIndex(iNext, j, k);
						const auto v_2 = CellIndex(i, j, kNext);
						const auto v_3 = CellIndex(iNext, j, kNext);
						const auto v_4 = CellIndex(i, jNext, k);
						const auto v_5 = CellIndex(iNext, jNext, k);
						const auto v_6 = CellIndex(i, jNext, kNext);
						const auto v_7 = CellIndex(iNext, jNext, kNext);
							
						if ((i + j + k) % 2)
						{
							//	t_a0
#ifdef VERBOSE
							cout << "Creating tetrahedral cell using layout t_B0: " << endl;
#endif
							pntIds[0] = v_0;
							pntIds[1] = v_4;
							pntIds[2] = v_5;
							pntIds[3] = v_6;
							render = true;
							for (int m = 0; m < input->GetPointData()->GetNumberOfArrays(); m++)
							{
								p0 = input->GetPointData()->GetArray(m)->GetComponent(pntIds[0], 0);
								p1 = input->GetPointData()->GetArray(m)->GetComponent(pntIds[1], 0);
								p2 = input->GetPointData()->GetArray(m)->GetComponent(pntIds[2], 0);
								p3 = input->GetPointData()->GetArray(m)->GetComponent(pntIds[3], 0);
								if (vtkMath::IsNan(p0) || vtkMath::IsNan(p1) || vtkMath::IsNan(p2)
									|| vtkMath::IsNan(p3))
								{
									render = false;
									break;
								}
							}
							if (render)
							{
								output->InsertNextCell(VTK_TETRA, 4, pntIds);
							}
							//	t_a1
#ifdef VERBOSE
							cout << "Creating tetrahedral cell using layout t_B1: " << endl;
#endif
							pntIds[0] = v_6;
							pntIds[1] = v_7;
							pntIds[2] = v_5;
							pntIds[3] = v_3;
							render = true;
							for (int m = 0; m < input->GetPointData()->GetNumberOfArrays(); m++)
							{
								p0 = input->GetPointData()->GetArray(m)->GetComponent(pntIds[0], 0);
								p1 = input->GetPointData()->GetArray(m)->GetComponent(pntIds[1], 0);
								p2 = input->GetPointData()->GetArray(m)->GetComponent(pntIds[2], 0);
								p3 = input->GetPointData()->GetArray(m)->GetComponent(pntIds[3], 0);
								if (vtkMath::IsNan(p0) || vtkMath::IsNan(p1) || vtkMath::IsNan(p2)
									|| vtkMath::IsNan(p3))
								{
									render = false;
									break;
								}
							}
							if (render)
							{
								output->InsertNextCell(VTK_TETRA, 4, pntIds);
							}
							//	t_a2
#ifdef VERBOSE
							cout << "Creating tetrahedral cell using layout t_B2: " << endl;
#endif
							pntIds[0] = v_0;
							pntIds[1] = v_1;
							pntIds[2] = v_5;
							pntIds[3] = v_3;

							render = true;
							for (int m = 0; m < input->GetPointData()->GetNumberOfArrays(); m++)
							{
								p0 = input->GetPointData()->GetArray(m)->GetComponent(pntIds[0], 0);
								p1 = input->GetPointData()->GetArray(m)->GetComponent(pntIds[1], 0);
								p2 = input->GetPointData()->GetArray(m)->GetComponent(pntIds[2], 0);
								p3 = input->GetPointData()->GetArray(m)->GetComponent(pntIds[3], 0);
								if (vtkMath::IsNan(p0) || vtkMath::IsNan(p1) || vtkMath::IsNan(p2)
									|| vtkMath::IsNan(p3))
								{
									render = false;
									break;
								}
							}
							if (render)
							{
								output->InsertNextCell(VTK_TETRA, 4, pntIds);
							}
							//	t_a3
#ifdef VERBOSE
							cout << "Creating tetrahedral cell using layout t_B3: " << endl;
#endif
							pntIds[0] = v_0;
							pntIds[1] = v_2;
							pntIds[2] = v_3;
							pntIds[3] = v_6;

							render = true;
							for (int m = 0; m < input->GetPointData()->GetNumberOfArrays(); m++)
							{
								p0 = input->GetPointData()->GetArray(m)->GetComponent(pntIds[0], 0);
								p1 = input->GetPointData()->GetArray(m)->GetComponent(pntIds[1], 0);
								p2 = input->GetPointData()->GetArray(m)->GetComponent(pntIds[2], 0);
								p3 = input->GetPointData()->GetArray(m)->GetComponent(pntIds[3], 0);
								if (vtkMath::IsNan(p0) || vtkMath::IsNan(p1) || vtkMath::IsNan(p2)
									|| vtkMath::IsNan(p3))
								{
									render = false;
									break;
								}
							}
							if (render)
							{
								output->InsertNextCell(VTK_TETRA, 4, pntIds);
							}
							//	t_a4
#ifdef VERBOSE
							cout << "Creating tetrahedral cell using layout t_B4: " << endl;
#endif
							pntIds[0] = v_0;
							pntIds[1] = v_3;
							pntIds[2] = v_6;
							pntIds[3] = v_5;
							render = true;
							for (int m = 0; m < input->GetPointData()->GetNumberOfArrays(); m++)
							{
								p0 = input->GetPointData()->GetArray(m)->GetComponent(pntIds[0], 0);
								p1 = input->GetPointData()->GetArray(m)->GetComponent(pntIds[1], 0);
								p2 = input->GetPointData()->GetArray(m)->GetComponent(pntIds[2], 0);
								p3 = input->GetPointData()->GetArray(m)->GetComponent(pntIds[3], 0);
								if (vtkMath::IsNan(p0) || vtkMath::IsNan(p1) || vtkMath::IsNan(p2)
									|| vtkMath::IsNan(p3))
								{
									render = false;
									break;
								}
							}
							if (render)
							{
								output->InsertNextCell(VTK_TETRA, 4, pntIds);
							}
						}
						else
						{
							//	t_b0
#ifdef VERBOSE
							cout << "Creating tetrahedral cell using layout t_B5: " << endl;
#endif
							pntIds[0] = v_0;
							pntIds[1] = v_4;
							pntIds[2] = v_1;
							pntIds[3] = v_2;

							render = true;
							for (int m = 0; m < input->GetPointData()->GetNumberOfArrays(); m++)
							{
								p0 = input->GetPointData()->GetArray(m)->GetComponent(pntIds[0], 0);
								p1 = input->GetPointData()->GetArray(m)->GetComponent(pntIds[1], 0);
								p2 = input->GetPointData()->GetArray(m)->GetComponent(pntIds[2], 0);
								p3 = input->GetPointData()->GetArray(m)->GetComponent(pntIds[3], 0);
								if (vtkMath::IsNan(p0) || vtkMath::IsNan(p1) || vtkMath::IsNan(p2)
									|| vtkMath::IsNan(p3))
								{
									render = false;
									break;
								}
							}

							if (render)
							{
								output->InsertNextCell(VTK_TETRA, 4, pntIds);
							}
							//	t_b1
#ifdef VERBOSE
							cout << "Creating tetrahedral cell using layout t_B6: " << endl;
#endif
							pntIds[0] = v_2;
							pntIds[1] = v_7;
							pntIds[2] = v_1;
							pntIds[3] = v_3;
							render = true;
							for (int m = 0; m < input->GetPointData()->GetNumberOfArrays(); m++)
							{
								p0 = input->GetPointData()->GetArray(m)->GetComponent(pntIds[0], 0);
								p1 = input->GetPointData()->GetArray(m)->GetComponent(pntIds[1], 0);
								p2 = input->GetPointData()->GetArray(m)->GetComponent(pntIds[2], 0);
								p3 = input->GetPointData()->GetArray(m)->GetComponent(pntIds[3], 0);
								if (vtkMath::IsNan(p0) || vtkMath::IsNan(p1) || vtkMath::IsNan(p2)
									|| vtkMath::IsNan(p3))
								{
									render = false;
									break;
								}
							}
							if (render)
							{
								output->InsertNextCell(VTK_TETRA, 4, pntIds);
							}
							//	t_b2
#ifdef VERBOSE
							cout << "Creating tetrahedral cell using layout t_B7: " << endl;
#endif
							pntIds[0] = v_7;
							pntIds[1] = v_1;
							pntIds[2] = v_5;
							pntIds[3] = v_4;
							render = true;
							for (int m = 0; m < input->GetPointData()->GetNumberOfArrays(); m++)
							{
								p0 = input->GetPointData()->GetArray(m)->GetComponent(pntIds[0], 0);
								p1 = input->GetPointData()->GetArray(m)->GetComponent(pntIds[1], 0);
								p2 = input->GetPointData()->GetArray(m)->GetComponent(pntIds[2], 0);
								p3 = input->GetPointData()->GetArray(m)->GetComponent(pntIds[3], 0);
								if (vtkMath::IsNan(p0) || vtkMath::IsNan(p1) || vtkMath::IsNan(p2)
									|| vtkMath::IsNan(p3))
								{
									render = false;
									break;
								}
							}
							if (render)
							{
								output->InsertNextCell(VTK_TETRA, 4, pntIds);
							}
							//	t_b3
#ifdef VERBOSE
							cout << "Creating tetrahedral cell using layout t_B8: " << endl;
#endif
							pntIds[0] = v_4;
							pntIds[1] = v_7;
							pntIds[2] = v_6;
							pntIds[3] = v_2;
							render = true;
							for (int m = 0; m < input->GetPointData()->GetNumberOfArrays(); m++)
							{
								p0 = input->GetPointData()->GetArray(m)->GetComponent(pntIds[0], 0);
								p1 = input->GetPointData()->GetArray(m)->GetComponent(pntIds[1], 0);
								p2 = input->GetPointData()->GetArray(m)->GetComponent(pntIds[2], 0);
								p3 = input->GetPointData()->GetArray(m)->GetComponent(pntIds[3], 0);
								if (vtkMath::IsNan(p0) || vtkMath::IsNan(p1) || vtkMath::IsNan(p2)
									|| vtkMath::IsNan(p3))
								{
									render = false;
									break;
								}
							}
							if (render)
							{
								output->InsertNextCell(VTK_TETRA, 4, pntIds);
							}
							//	t_B4
#ifdef VERBOSE
							cout << "Creating tetrahedral cell using layout t_B9: " << endl;
#endif
							pntIds[0] = v_2;
							pntIds[1] = v_1;
							pntIds[2] = v_7;
							pntIds[3] = v_4;
							render = true;
							for (int m = 0; m < input->GetPointData()->GetNumberOfArrays(); m++)
							{
								p0 = input->GetPointData()->GetArray(m)->GetComponent(pntIds[0], 0);
								p1 = input->GetPointData()->GetArray(m)->GetComponent(pntIds[1], 0);
								p2 = input->GetPointData()->GetArray(m)->GetComponent(pntIds[2], 0);
								p3 = input->GetPointData()->GetArray(m)->GetComponent(pntIds[3], 0);
								if (vtkMath::IsNan(p0) || vtkMath::IsNan(p1) || vtkMath::IsNan(p2)
									|| vtkMath::IsNan(p3))
								{
									render = false;
									break;
								}
							}
							if (render)
							{
								output->InsertNextCell(VTK_TETRA, 4, pntIds);
							}
						}
					}
				}
			}
			break;

			default:
			vtkWarningMacro("Unsupported 3D schame, no output.");
		}
	}
	else
	{
		int dimA, dimB;
		if (dim[0] == 1)
		{
			dimA = dim[1];
			dimB = dim[2];
		}
		if (dim[1] == 1)
		{
			dimA = dim[0];
			dimB = dim[2];
		}
		if (dim[2] == 1)
		{
			dimA = dim[0];
			dimB = dim[1];
		}

		output->Allocate(numCells * 2);
		nbs->SetNumberOfComponents(3);
		nbs->SetNumberOfTuples(numCells * 2);

		bool render;
		double p0, p1, p2;
		for (int j = 0; j < dimB - 1; j++)
		{
			for (vtkIdType i = 0; i < dimA - 1; i++)
			{
				if ((i + j) % 2)
				{
					pntIds[0] = CellIndex2(i, j + 1);
					pntIds[1] = CellIndex2(i, j);
					pntIds[2] = CellIndex2(i + 1, j);

					render = true;
					for (int m = 0; m < input->GetPointData()->GetNumberOfArrays(); m++)
					{
						p0 = input->GetPointData()->GetArray(m)->GetComponent(pntIds[0], 0);
						p1 = input->GetPointData()->GetArray(m)->GetComponent(pntIds[1], 0);
						p2 = input->GetPointData()->GetArray(m)->GetComponent(pntIds[2], 0);
						if (vtkMath::IsNan(p0) || vtkMath::IsNan(p1) || vtkMath::IsNan(p2))
						{
							render = false;
							break;
						}
					}
					if (render)
					{
						output->InsertNextCell(VTK_TRIANGLE, 3, pntIds);
					}

					pntIds[0] = CellIndex2(i, j + 1);
					pntIds[1] = CellIndex2(i + 1, j);
					pntIds[2] = CellIndex2(i + 1, j + 1);

					render = true;
					for (int m = 0; m < input->GetPointData()->GetNumberOfArrays(); m++)
					{
						p0 = input->GetPointData()->GetArray(m)->GetComponent(pntIds[0], 0);
						p1 = input->GetPointData()->GetArray(m)->GetComponent(pntIds[1], 0);
						p2 = input->GetPointData()->GetArray(m)->GetComponent(pntIds[2], 0);
						if (vtkMath::IsNan(p0) || vtkMath::IsNan(p1) || vtkMath::IsNan(p2))
						{
							render = false;
							break;
						}
					}
					if (render)
					{
						output->InsertNextCell(VTK_TRIANGLE, 3, pntIds);
					}
				}
				else
				{
					pntIds[0] = CellIndex2(i, j);
					pntIds[1] = CellIndex2(i + 1, j);
					pntIds[2] = CellIndex2(i + 1, j + 1);
					render = true;
					for (int m = 0; m < input->GetPointData()->GetNumberOfArrays(); m++)
					{
						p0 = input->GetPointData()->GetArray(m)->GetComponent(pntIds[0], 0);
						p1 = input->GetPointData()->GetArray(m)->GetComponent(pntIds[1], 0);
						p2 = input->GetPointData()->GetArray(m)->GetComponent(pntIds[2], 0);
						if (vtkMath::IsNan(p0) || vtkMath::IsNan(p1) || vtkMath::IsNan(p2))
						{
							render = false;
							break;
						}
					}
					if (render)
					{
						output->InsertNextCell(VTK_TRIANGLE, 3, pntIds);
					}

					pntIds[0] = CellIndex2(i, j);
					pntIds[1] = CellIndex2(i, j + 1);
					pntIds[2] = CellIndex2(i + 1, j + 1);
					render = true;
					for (int m = 0; m < input->GetPointData()->GetNumberOfArrays(); m++)
					{
						p0 = input->GetPointData()->GetArray(m)->GetComponent(pntIds[0], 0);
						p1 = input->GetPointData()->GetArray(m)->GetComponent(pntIds[1], 0);
						p2 = input->GetPointData()->GetArray(m)->GetComponent(pntIds[2], 0);
						if (vtkMath::IsNan(p0) || vtkMath::IsNan(p1) || vtkMath::IsNan(p2))
						{
							render = false;
							break;
						}
					}
					if (render)
					{
						output->InsertNextCell(VTK_TRIANGLE, 3, pntIds);
					}
				}
			}
		}
	}

	// Explicitly construct points.
	vtkSmartPointer<vtkPoints> points = vtkSmartPointer<vtkPoints>::New();
	vtkIdType pnt = 0;

	std::cout << "dim0: " << dim[0] << "  dim1: " << dim[1] << "  dim2: " << dim[2] << std::endl;
	points->SetNumberOfPoints(dim[0] * dim[1] * dim[2]);

	for (int k = 0; k < dim[2]; k++)
	{
		for (int j = 0; j < dim[1]; j++)
		{
			for (int i = 0; i < dim[0]; i++)
			{
				points->SetPoint(pnt++, i, j, k);
			}
		}
	}
	// Store the new set of points in the output.
	output->SetPoints(points);

	// Pass point data through because we still have the same number
	// of points.
	output->GetPointData()->PassData(input->GetPointData());
	// Avoid keeping extra memory around.
	output->Squeeze();

	return 1;
}

