/*=========================================================================

 *	File: vtkSimplifyJCN.h
 *	Graph visualization library for VTK
 * 
 *	This software is distributed WITHOUT ANY WARRANTY; 
 *	without even the implied warranty of MERCHANTABILITY 
 *	or FITNESS FOR A PARTICULAR PURPOSE.  
 * 
 *	See the file copyright.txt for details.  

=========================================================================*/

// .NAME vtkSimplifyJCN - This filter has rendered the Reeb Skeleton graph.  
// .SECTION Description
//  Given a JCN graph and Jacobi Structure, this filter has subdivided the graph into two
//  groups : one is containing singular nodes, ie. Jacobi nodes, the other 
//  group is containing regular nodes, ie. non-Jacobi nodes.  Then for each
//  Jacobi node, we could find all of its neighbouring regular nodes. 
//  For those regular nodes connected to the same Jacobi nodes, we could group them
//  as one regular sheet. This filter takes a JCN graph as input. The output of this 
//  filter is a Reeb skeleton graph. The output can be further passed to the 
//  vtkSimplifyMetric filter, where different simplification rules can be applied to 
//  further simplify the graph. A vector struture is used to keep track of the 
//  correspondence between the nodes in Reeb Skeleton graph and original JCN. This index
//  list can be used in interaction process. 

#ifndef __vtkSimplifyJCN_h
#define __vtkSimplifyJCN_h
#include "vtkMETAModule.h"
#include "vtkBitArray.h"
#include "vtkSmartPointer.h"
#include "vtkGraphAlgorithm.h"
#include <vtkMutableGraphHelper.h>
#include <vector>
#include "vtkMultiDimensionalReebGraph.h"
#include "vtkSelectionNode.h"
#include "vtkSelection.h"
#include "vtkExtractSelection.h"
#include "vtkExtractSelectedGraph.h"
#include "vtkAdjacentVertexIterator.h"
#include "vtkTimerLog.h"
class vtkIdTypeArray;

class VTK_META_EXPORT vtkSimplifyJCN : public vtkGraphAlgorithm
{
public:
  static vtkSimplifyJCN* New();
  vtkTypeMacro(vtkSimplifyJCN, vtkGraphAlgorithm);
  void PrintSelf(ostream& os, vtkIndent indent);
  void AddField(const char *nm);
  //Description:
  // Print input graph structure. 
  void PrintGraph(vtkGraph *graph);
  //Description:
  // Print output graph structure. 
  void PrintGraphComponents(vtkGraph *graph);
  // Description :   
  // Extract regular and singular nodes from the input graph.  
  void CreateComponentConnectivityGraph(vtkGraph *inputGraph);
  // Description :  
  // Compute the volume of the component for future simplification purpose. 
  void ExtractRegularAndSingularComponents(vtkGraph *inputGraph);
  // Description :   
  // Return the number of connected components in a graph.  
  int  GetNrBoostGraphComponents(vtkGraph *G);
  // Description :  
  // Given a singular and regular node, return whether they are connected. 
  bool IsConnected(int iReg, int jSing, vtkGraph *input);
  int GetComponentNodeIds();
  // Description :  
  // Compute the volume of the component for future simplification purpose. 
  double ComputeComponentVolume(vtkGraph *G, vtkIdType compId);
  // Description :  
  // Return the correspondence between the regular sheet and original JCN nodes.   
  std::vector< std::vector<int> > GetJCNList();
  // Description:
  // Add the MDRG structure
  void AddJacobiStructure(vtkBitArray *jacobiStructure);
  // Description :
  // Return the number of regular components.   
  int GetRegCompNr();
  // Description :
  // Return the number of singular components.  
  int GetSingCompNr();
  

protected:
  vtkSimplifyJCN();
  ~vtkSimplifyJCN();
  // Merge the Jacobi nodes with the same connected regular nodes. 
  void MergeJacobiNodes();
  int FillInputPortInformation(int port, vtkInformation* info);
  
  int RequestData(
    vtkInformation*, 
    vtkInformationVector**, 
    vtkInformationVector*);
  // Field name of the input data.  
  std::vector<char*> Fields;
  // Graph of regular component.   
  vtkSmartPointer<vtkGraph> RegularComponents;
  // Graph of singular component.   
  vtkSmartPointer<vtkGraph> SingularComponents;
  // Reeb Skeleton graph    
  vtkSmartPointer<vtkMutableUndirectedGraph> ComponentConnectivityGraph;
  // The correspondence between the Reeb Skeleton graph and original JCN. 
  std::vector< std::vector<int> > JCNList;
  // Number of Regular and Singular components.  
  int RegCompNr, SingCompNr;
  // List of node ids which are part of Jacobi structure. 
  vtkSmartPointer<vtkIdTypeArray> JacobiStructure;
  
  private:
  vtkSimplifyJCN(const vtkSimplifyJCN&);   // Not implemented
  void operator=(const vtkSimplifyJCN&);   // Not implemented
};

#endif

