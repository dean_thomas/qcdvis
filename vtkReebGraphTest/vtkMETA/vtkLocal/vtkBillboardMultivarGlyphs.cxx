/*=========================================================================

 *	File: vtkBillboardMultivarGlyphs.cxx
 *	Graph visualization library for VTK
 * 
 *	This software is distributed WITHOUT ANY WARRANTY; 
 *	without even the implied warranty of MERCHANTABILITY 
 *	or FITNESS FOR A PARTICULAR PURPOSE.  
 * 
 *	See the file copyright.txt for details.  

=========================================================================*/
#include "vtkBillboardMultivarGlyphs.h"
#include "vtkCell.h"
#include "vtkCellData.h"
#include "vtkCellData.h"
#include "vtkCamera.h"
#include "vtkGraph.h"
#include "vtkIdList.h"
#include "vtkInformation.h"
#include "vtkInformationVector.h"
#include "vtkMath.h"
#include "vtkObjectFactory.h"
#include "vtkSmartPointer.h"
#include "vtkTransform.h"

vtkStandardNewMacro(vtkBillboardMultivarGlyphs);

//----------------------------------------------------------------------------
vtkBillboardMultivarGlyphs::vtkBillboardMultivarGlyphs()
{
  this->Camera = NULL;
  this->Transform = vtkTransform::New();
  this->Smoothness = 2;
  this->Scale = 0.5;
  this->Orientation[0] = 0.0;
  this->Orientation[1] = 1.0;
  this->Orientation[2] = 0.0;
  this->Orientation[3] = 0.0;
  this->SetLog = true;  
}

//----------------------------------------------------------------------------
vtkBillboardMultivarGlyphs::~vtkBillboardMultivarGlyphs()
{
  if (this->Camera)
    {
    this->Camera->UnRegister(this);
    }
  if (this->Transform)
    {
    this->Transform->UnRegister(this);
    }
}

//----------------------------------------------------------------------------
void vtkBillboardMultivarGlyphs::PrintSelf(ostream& os, vtkIndent indent)
{
  this->Superclass::PrintSelf(os,indent);
  os << "Smoothness: " << this->Smoothness << endl;
  os << "Scale: " << this->Scale << endl;
  os << "Camera: " << (this->Camera ? "set" : "not set") << endl;
}

//----------------------------------------------------------------------------
int vtkBillboardMultivarGlyphs::FillInputPortInformation(int, vtkInformation* info)
{
  // This filter requires a vtkGraph or subtype as input.
  info->Set(vtkAlgorithm::INPUT_REQUIRED_DATA_TYPE(), "vtkGraph");
  return 1;
}


unsigned long int vtkBillboardMultivarGlyphs::GetMTime()
{
  unsigned long mtime, result;
  double ov[4];
  bool changed = false;

  result = vtkPolyDataAlgorithm::GetMTime();
  
  if (this->Camera)
    {
    this->Camera->GetViewTransformObject()->GetOrientationWXYZ(ov);
    changed = false;
    for (int i = 0; i < 4; i++)
       {
       if (this->Orientation[i] != ov[i])
         {
         changed = true;
         }
       }
     }
  if (changed)
    {
    for (int i = 0; i < 4; i++)
       {
       this->Orientation[i] = ov[i];
       }
    mtime = this->Camera->GetMTime();
    result = ( mtime > result ? mtime : result );
    }
  return result;
}


//----------------------------------------------------------------------------
int vtkBillboardMultivarGlyphs::RequestData(vtkInformation*,
                                 vtkInformationVector** inputVector,
                                 vtkInformationVector* outputVector)
{
  // Get input and output data.
  // get the info objects
  vtkInformation *inInfo = inputVector[0]->GetInformationObject(0);
  vtkInformation *outInfo = outputVector->GetInformationObject(0);

  // get the input and output
  vtkGraph *input = vtkGraph::SafeDownCast(
    inInfo->Get(vtkDataObject::DATA_OBJECT()));
  vtkPolyData *output = vtkPolyData::SafeDownCast(
    outInfo->Get(vtkDataObject::DATA_OBJECT()));

  vtkPoints *points = input->GetPoints();
  vtkIdType numPts   = points->GetNumberOfPoints();

  // Estimate size of output.
  output->Allocate(numPts * (1 + this->Smoothness));
  output->BuildCells();

  vtkDataArray *scalars = input->GetVertexData()->GetScalars();
  int numFields = scalars->GetNumberOfComponents();
  
  // If present, use the size array on vertex data to scale glyphs.
  vtkDataArray *size = input->GetVertexData()->GetArray("size");
  
  // Each segment of the glyph will be a cell in the output.
  // Allocate an array to hold the scalar data from each input
  // field, one entry per glyph segment.
  vtkSmartPointer<vtkDataArray> glyphCells = scalars->NewInstance();
  glyphCells->UnRegister(this);
  glyphCells->SetNumberOfComponents(1);
  glyphCells->SetNumberOfTuples(numPts * numFields);
  glyphCells->SetName("vtkBillboardMG values");

  // Number of points per glyph segment and per glyph.
  const int pntsPerSegment = 2 + this->Smoothness;
  const int pntsPerGlyph   = pntsPerSegment * numFields;

  vtkSmartPointer<vtkPoints> glyphPoints 
      = vtkSmartPointer<vtkPoints>::New();
  glyphPoints->SetNumberOfPoints(pntsPerGlyph);

  vtkSmartPointer<vtkPoints> newPoints 
      = vtkSmartPointer<vtkPoints>::New();
  newPoints->SetNumberOfPoints(numPts * pntsPerGlyph);
    
  vtkSmartPointer<vtkIdList> pointIds = 
      vtkSmartPointer<vtkIdList>::New();
  pointIds->SetNumberOfIds(pntsPerSegment);
  
  vtkSmartPointer<vtkTransform> transform
      = vtkSmartPointer<vtkTransform>::New();
      
  // Failure to add camera isn't fatal, but glyphs can't
  // then serve as billboards tracking viewer.
  if (!this->Camera)
    {
    vtkWarningMacro("No camera available.");
    }
  if (!scalars)
    {
    vtkErrorMacro("No scalars present.");
    }
    
  // Compute prototype points.
  // The prototype will be copied for each actual glyph.
  const float dTheta = vtkMath::Pi() * 2 / (this->Smoothness * numFields);
  
  float theta = 0;
  for (int i = 0, p = 0; i < numFields; i++)
     {
     glyphPoints->SetPoint(p++, 0, 0, 0);
     for (int j = 1; j < pntsPerSegment; j++)
       {
       glyphPoints->SetPoint(p++, this->Scale * cos(theta), this->Scale * sin(theta), 0);
       theta += dTheta;
       }
     // Start next segment at previous point of last.
     theta -= dTheta;
     }
  
  vtkIdType nextPoint = 0;
  vtkIdType cellId;
  double p[3], q[3];
  double orientation[4];
  
  transform->PostMultiply();  
  for (vtkIdType i = 0; i < numPts; i++)
    {
    transform->Identity();
    transform->RotateWXYZ(-this->Orientation[0],this->Orientation[1],this->Orientation[2],this->Orientation[3]);
    points->GetPoint(i, p);
    transform->Translate(p);

    // If a size array was provided, scale the glyph by log10 of the size entry for the vertex.
    if (size)
      {
      float vscale;
      if (this->SetLog)
        {
        vscale = Log2(size->GetComponent(i,0));
        }
      else 
        {
        vscale = size->GetComponent(i,0);
        }
      float theta = 0;
      // Copy points from the prototype
      for (int k = 0, p = 0; k < numFields; k++)
         {
         glyphPoints->SetPoint(p++, 0, 0, 0);
         for (int j = 1; j < pntsPerSegment; j++)
            {
            glyphPoints->SetPoint(p++, this->Scale * vscale * cos(theta), this->Scale * vscale * sin(theta), 0);
            theta += dTheta;
            }
          // Start next segment at previous point of last.
          theta -= dTheta;
          }
        }
      
    for (int j = 0; j < numFields; j++)
       {
       // Output cell
       pointIds->Reset();
       pointIds->SetNumberOfIds(pntsPerSegment);
       for (int k = 0; k < pntsPerSegment; k++)
          {
          pointIds->SetId(k, nextPoint);
          //Transform point.
          glyphPoints->GetPoint(j * pntsPerSegment + k, p);
          transform->TransformPoint(p, q);
          newPoints->SetPoint(nextPoint, q);
          nextPoint++;
          }
       cellId = output->InsertNextCell(VTK_POLYGON, pointIds);
       glyphCells->SetComponent(cellId, 0, scalars->GetComponent(i, j));      
       // Output points assuming centered on x-y plane.          
       }
        // Undo transformation: cheaper than push/pop?
    transform->Translate(-p[0], -p[1], -p[2]);      
    }

  output->SetPoints(newPoints);
  output->GetCellData()->AddArray(glyphCells);

  // Avoid keeping extra memory around.
  output->Squeeze();
  return 1;
}


