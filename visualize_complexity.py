#	For a given configuration, this python script will calculate the
#	average time taken to compute the JCN on each slice axis.  These
#	are then displayed as a line graph over the number of cools
#
import re
from collections import namedtuple
from os.path import isfile, isdir, join
from os import listdir
import matplotlib.pyplot as plt

BASE_DIR= r'C:\Users\4thFloor\Desktop\EnsembleTest'


JCN_COOLING_DIR_REGEX = r"^cool(\d*)"
JCN_COMPUTATION_LOG_REGEX = r"^([a-zA-Z]*\s*[a-zA-Z]*\s*[a-zA-Z]*)_([a-zA-Z]*\s*[a-zA-Z]*\s*[a-zA-Z]*)_([xyzt])=(\d*)_stderr.txt"
JCN_COMPUTATION_OUTPUT_REGEX = r"^([a-zA-Z]*\s*[a-zA-Z]*\s*[a-zA-Z]*)_([a-zA-Z]*\s*[a-zA-Z]*\s*[a-zA-Z]*)_([xyzt])=(\d*)_stdout.txt"

JCN_COMPUTATION_LINE_REGEX = r"'Compute Joint Contour Net' duration: [\d+\s+hours,\s+\d+\s+minutes,\s+\d+\s+seconds|\d+\s+minutes,\s+\d+\s+seconds|\d+\s+seconds]*\s+\((\d+)\s+(\w+)\)"

JCN_STATISTICS_LINE_REGEX = r"Joint Contour Net = { vertex count: (\d*), edge count: (\d*) }."
JCN_MDRG_LINE = r"(\d*)\s*mdrgList."

CONFIGURATION_NAME_REGEX = r"[A-Za-z0-9\\/:]*\\(\w*)[\\/]?$"

#	Names of our graphs
GRAPH_X_TITLE = 'x-axis split'
GRAPH_Y_TITLE = 'y-axis split'
GRAPH_Z_TITLE = 'z-axis split'
GRAPH_T_TITLE = 't-axis split'
GRAPH_AVERAGE_TITLE = '4 axis average'

#	Directory structure is expected to be as follows...
#
#	Root
#	|
#	|-conf0000
#	| |
#	| |-cool0020
#	| | |
#	| |	|-var1_var2_t=1.stderr.txt
#	| |	| ...
#	| |	\-var1_var2_t=20.stderr.txt
#	| |
#	| |-cool0019
#	| |		...
#	| \-cool0000
#	|
#	|-conf0001
#	| |
#	| |-cool0020
#	| | ...
#	| \-cool0000
#	| ...
#	\-conf9999
#

#	Store a duration as a value and units pair
Duration = namedtuple('Duration', 'value units')

JcnData = namedtuple('JcnData', 'jcnVertices jcnEdges mdrgVertices')

#
#	\brief		Extracts the time taken by the computation from the log file.
#	
#	\author		Dean
#	\since		07-03-2016
#
def extractJcnComputationTime(filename):
	try:
		file = open(filename, 'r')
		fileContents = file.read()
		file.close()
		
		#	Extract the duration of the JCN computation - first match group is the fine 
		#	grained resolution, second is the units (milliseconds etc.)
		matches = re.search(JCN_COMPUTATION_LINE_REGEX, fileContents)
		
		if (matches != None):
			value = int(matches.group(1))
			units = matches.group(2)
	
			#print("File '%s' was extracted in %d %s" %(filename, value, units))
		
			return (Duration(value=value, units=units))
	except FileNotFoundError:
		print("The file: '%s' could not be found." %(filename))
	except ValueError:
		print('There was an error parsing the file: %s' %(filename))


def extractOutput(filename):
	try:
		file = open(filename, 'r')
		fileContents = file.read()
		file.close()
		
		#	Extract the JCN statistics
		jcnMatches = re.search(JCN_STATISTICS_LINE_REGEX, fileContents)
		mdrgMatches = re.search(JCN_MDRG_LINE, fileContents)
		
		if ((jcnMatches != None) and (mdrgMatches != None)):
			vertexCount = int(jcnMatches.group(1))
			edgeCount = int(jcnMatches.group(2))
			mdrgVertexCount = int(mdrgMatches.group(1))
			
			#print("File '%s' was extracted in %d %s" %(filename, value, units))
		
			return (JcnData(jcnVertices=vertexCount, jcnEdges=edgeCount, mdrgVertices=mdrgVertexCount))
	except FileNotFoundError:
		print("The file: '%s' could not be found." %(filename))
	except ValueError:
		print('There was an error parsing the file: %s' %(filename))
		
#
#	\brief		Computes the average for an entire data series (ie 'x' axis).
#	
#	\author		Dean
#	\since		07-03-2016
#
def computeDurationAverage(dataSeries):
	totalDuration = 0
	count = 0
	
	for d in enumerate(dataSeries):
		#	Extract information from the tuple { slice, { duration, unit } )
		slice = d[0]
		duration = d[1].value
		unit = d[1].units
		
		#print('Time slice: %d, duration: %d %s' %(slice, duration, unit))
		totalDuration += duration
		count += 1
	
	if count > 0:
		average = totalDuration / count
	else:
		return 0
	#print('Duration total: %d, count: %d, average %d' %(totalDuration, count, average))
	
	return average

#
#	\brief		Evaluates an entire cooling slice, returning the average on the
#				x, y, z ant axis as a tuple.
#	
#	\param		coolingDir: the base directory containing the files relating to an
#				entire iteration of cooling
#	\author		Dean
#	\since		07-03-2016
#
def evaluateLogFiles(coolingDir):
	#	Obtain list of files in the current directory
	files = [f for f in listdir(coolingDir) if isfile(join(coolingDir, f))]
	
	#	Storage for each series - store data as follows:
	#	{ 'x', [{ 3, { 42000, 'milliseconds'} }, { 4, { 67320, 'milliseconds'} }, ... ] }
	dataSeries = { 'x' : [], 'y' : [], 'z' : [], 't' : []}
	averages = { 'x' : 0.0, 'y' : 0.0, 'z' : 0.0, 't' : 0.0 }
	
	for file in files:
		matches = re.search(JCN_COMPUTATION_LOG_REGEX, file)
		
		if matches != None:
			#print('Processing %s' %(matches.group(0)))
			assert (len(matches.groups()) == 4), "Only bi-variate data is supported at present"
			
			#	Extract information about the log file
			logFilename  = matches.group(0)
			field1 = matches.group(1)
			field2 = matches.group(2)
			#field3 = matches.group(3)
			sliceAxis = matches.group(3)
			sliceValue = matches.group(4)
			
			#	make sure the 'sliceAxis' is valid
			assert sliceAxis in dataSeries.keys(), "Invalid slice extracted from filename"
			
			#	extract the duration for the current file
			filePath = coolingDir + '\\' + logFilename
			duration = extractJcnComputationTime(filePath)
			
			if duration != None:
				dataSeries[sliceAxis].append(duration)
	
	#	Compute averages
	averages['x'] = computeDurationAverage(dataSeries['x'])
	averages['y'] = computeDurationAverage(dataSeries['y'])
	averages['z'] = computeDurationAverage(dataSeries['z'])
	averages['t'] = computeDurationAverage(dataSeries['t'])
	
	return [averages['x'], averages['y'], averages['z'], averages['t']]

#
#	\brief		Analyses an entire configuration by looping over available
#				cooling slices.  Returns a map of {cooling slices, averages}
#				pairs.
#	\author		Dean
#	\since		07-03-2016
#
def analyseConfigurationLogFiles(configurationDir):
	#	Obtain list of directories in the current directory
	dirs = [f for f in listdir(configurationDir) if isdir(join(configurationDir, f))]
	
	durationAverages = {}
	
	for dir in dirs:
		matches = re.search(JCN_COOLING_DIR_REGEX, dir)
		
		if (matches != None):
			#	cooling dir
			coolingDir = configurationDir+ '\\'+ matches.group(0)
			coolingIter = int(matches.group(1))
			
			#print("%i %s" %(coolingIter, coolingDir))
			durationAverages[coolingIter] = evaluateLogFiles(coolingDir)
			
	return durationAverages
	
#
#	\brief		Plots the data series for each split axis (x, y, z, t)
#				and 4 axis average for a given configuration with 
#				regard to cooling.
#	\author		Dean
#	\since		07-03-2016
#
def plotLogData(graph, title, averages):
	currentMin, currentMax = graph.xlim()
	#	Extract the minimum and maximum number of cools available
	#	The cooling iteration is used as a key to retreive the
	#	averages.
	#cools = averages.keys()
	#minCools = min(cools)
	#maxCools = max(cools)
	
	#if minCools < currentMin:
	#	graph.xlim(xmin=minCools)
	#if maxCools > currentMax:
	#	graph.xlim(xmax=maxCools)
	
	#	Keep track of the duration for each axis and the number of
	#	cools for each sample so that it can be passed to matplotlib.
	durationValsX = []
	durationValsY = []
	durationValsZ = []
	durationValsT = []
	durationAverages = []
	coolingVals = []
	
	#	Iterate over the available data for configuration series
	#	where
	#		key: the cooling slice
	#		value: the averages on the x, y, z and t axis
	for key, value in averages.items():
		coolingVals.append(key)
		durationValsX.append(value[0])
		durationValsY.append(value[1])
		durationValsZ.append(value[2])
		durationValsT.append(value[3])
		durationAverages.append((value[0] + value[1] + value[2] + value [3]) / 4)
		#print('Cooling slice: %d %s' %(cooling, averages))
	
	#	Plot X slices
	graph.figure(GRAPH_X_TITLE)
	graph.plot(coolingVals, durationValsX, label=title)
	graph.legend(loc='upper right', shadow=True)
	
	#	Plot Y slices
	graph.figure(GRAPH_Y_TITLE)
	graph.plot(coolingVals, durationValsY, label=title)
	graph.legend(loc='upper right', shadow=True)
	
	#	Plot Z slices
	graph.figure(GRAPH_Z_TITLE)
	graph.plot(coolingVals, durationValsZ, label=title)
	graph.legend(loc='upper right', shadow=True)
	
	#	Plot T slices
	graph.figure(GRAPH_T_TITLE)
	graph.plot(coolingVals, durationValsT, label=title)
	graph.legend(loc='upper right', shadow=True)
	
	#	Plot Average
	graph.figure(GRAPH_AVERAGE_TITLE)
	graph.plot(coolingVals, durationAverages, label=title)
	graph.legend(loc='upper right', shadow=True)
	
#
#	\brief		Plots the data series for each split axis (x, y, z, t)
#				and 4 axis average for a given configuration with 
#				regard to cooling.
#	\author		Dean
#	\since		07-03-2016
#
def plotJcnData(graph, title, averages):
	jcnVertexPlotLabel = str(title + ' jcn vertices')
	jcnEdgePlotLabel = str(title + ' jcn edges')
	jcnMDRGPlotLabel = str(title + ' MDRG vertices')
	
	jcnVertexCountValsX = []
	jcnVertexCountValsY = []
	jcnVertexCountValsZ = []
	jcnVertexCountValsT = []
	jcnVertexCountValsAverage = []
	
	jcnEdgeCountValsX = []
	jcnEdgeCountValsY = []
	jcnEdgeCountValsZ = []
	jcnEdgeCountValsT = []
	jcnEdgeCountValsAverage = []
	
	jcnMDRGCountValsX = []
	jcnMDRGCountValsY = []
	jcnMDRGCountValsZ = []
	jcnMDRGCountValsT = []
	jcnMDRGCountValsAverage = []
	
	
	coolingVals = []
	
	#	Iterate over the available data for configuration series
	#	where
	#		key: the cooling slice
	#		value: the averages on the x, y, z and t axis
	for key, value in averages.items():
		#print(value)
		coolingVals.append(key)
		
		#	x-axis
		jcnVertexCountValsX.append(value[0].jcnVertices)
		jcnEdgeCountValsX.append(value[0].jcnEdges)
		jcnMDRGCountValsX.append(value[0].mdrgVertices)
		
		#	y-axis
		jcnVertexCountValsY.append(value[1].jcnVertices)
		jcnEdgeCountValsY.append(value[1].jcnEdges)
		jcnMDRGCountValsY.append(value[1].mdrgVertices)
		
		#	z-axis
		jcnVertexCountValsZ.append(value[2].jcnVertices)
		jcnEdgeCountValsZ.append(value[2].jcnEdges)
		jcnMDRGCountValsZ.append(value[2].mdrgVertices)
		
		#	t-axis
		jcnVertexCountValsT.append(value[3].jcnVertices)
		jcnEdgeCountValsT.append(value[3].jcnEdges)
		jcnMDRGCountValsT.append(value[3].mdrgVertices)
		
		#durationValsY.append(value[1])
		#durationValsZ.append(value[2])
		#durationValsT.append(value[3])
		#durationAverages.append((value[0] + value[1] + value[2] + value [3]) / 4)
		#print('Cooling slice: %d %s' %(cooling, averages))
	
	#	Plot X slices
	graph.figure(GRAPH_X_TITLE)
	#	Plot vertex count line
	vLine, = graph.plot(coolingVals, jcnVertexCountValsX, label=jcnVertexPlotLabel)
	vColour = vLine.get_color()
	#	Plot the corresponding edge line in the same colour but dashed
	eLine = graph.plot(coolingVals, jcnEdgeCountValsX, label=jcnEdgePlotLabel, color=vColour, linestyle='dashed')
	#	Plot the MDRG vertex count
	mLine = graph.plot(coolingVals, jcnMDRGCountValsX, label=jcnMDRGPlotLabel, color=vColour, linestyle='dotted')
	graph.legend(loc='upper right', shadow=True)
	
	#	Plot Y slices
	graph.figure(GRAPH_Y_TITLE)
	#	Plot vertex count line
	vLine, = graph.plot(coolingVals, jcnVertexCountValsY, label=jcnVertexPlotLabel)
	vColour = vLine.get_color()
	#	Plot the corresponding edge line in the same colour but dashed
	eLine = graph.plot(coolingVals, jcnEdgeCountValsY, label=jcnEdgePlotLabel, color=vColour, linestyle='dashed')
	#	Plot the MDRG vertex count
	mLine = graph.plot(coolingVals, jcnMDRGCountValsY, label=jcnMDRGPlotLabel, color=vColour, linestyle='dotted')
	graph.legend(loc='upper right', shadow=True)
	
	#	Plot Z slices
	graph.figure(GRAPH_Z_TITLE)
	#	Plot vertex count line
	vLine, = graph.plot(coolingVals, jcnVertexCountValsZ, label=jcnVertexPlotLabel)
	vColour = vLine.get_color()
	#	Plot the corresponding edge line in the same colour but dashed
	eLine = graph.plot(coolingVals, jcnEdgeCountValsZ, label=jcnEdgePlotLabel, color=vColour, linestyle='dashed')
	#	Plot the MDRG vertex count
	mLine = graph.plot(coolingVals, jcnMDRGCountValsZ, label=jcnMDRGPlotLabel, color=vColour, linestyle='dotted')
	graph.legend(loc='upper right', shadow=True)
	
	#	Plot T slices
	graph.figure(GRAPH_T_TITLE)
	#	Plot vertex count line
	vLine, = graph.plot(coolingVals, jcnVertexCountValsT, label=jcnVertexPlotLabel)
	vColour = vLine.get_color()
	#	Plot the corresponding edge line in the same colour but dashed
	eLine = graph.plot(coolingVals, jcnEdgeCountValsT, label=jcnEdgePlotLabel, color=vColour, linestyle='dashed')
	#	Plot the MDRG vertex count
	mLine = graph.plot(coolingVals, jcnMDRGCountValsT, label=jcnMDRGPlotLabel, color=vColour, linestyle='dotted')
	graph.legend(loc='upper right', shadow=True)
	
	#	Plot Average
	#graph.figure(GRAPH_AVERAGE_TITLE)
	#graph.plot(coolingVals, durationAverages, label=title)
	#graph.legend(loc='upper right', shadow=True)

#
#	\brief		Extracts the configuration name from a path, assumes
#				that the configuration base name is the last directory
#				in the path.
#	\author		Dean
#	\since		07-03-2016
#
def extractConfigurationName(pathToConfiguration):
	#	Assume an absolute path with the configuration title as the last
	#	directory
	matches = re.search(CONFIGURATION_NAME_REGEX, pathToConfiguration)
	
	if matches != None:
		return matches.group(1)
	else:
		return 'Unknown configuration'

#
#	\brief		Generates and sets up the 5 graphs used to show
#				the data.  Parameters express the limits on the
#				x-axis (cooling).
#	\author		Dean
#	\since		07-03-2016
#		
def createGraphs(title_xAxis, limit_xMin, limit_xMax, title_yAxis):
	#	Create graphs, add data series for each configuration
	plt.figure(GRAPH_X_TITLE)
	plt.title(GRAPH_X_TITLE)
	plt.xlabel(title_xAxis)
	plt.ylabel(title_yAxis)
	plt.xlim(xmin=limit_xMin, xmax=limit_xMax)
	
	plt.figure(GRAPH_Y_TITLE)
	plt.title(GRAPH_Y_TITLE)
	plt.xlabel(title_xAxis)
	plt.ylabel(title_yAxis)
	plt.xlim(xmin=limit_xMin, xmax=limit_xMax)
	
	plt.figure(GRAPH_Z_TITLE)
	plt.title(GRAPH_Z_TITLE)
	plt.xlabel(title_xAxis)
	plt.ylabel(title_yAxis)
	plt.xlim(xmin=limit_xMin, xmax=limit_xMax)
	
	plt.figure(GRAPH_T_TITLE)
	plt.title(GRAPH_T_TITLE)
	plt.xlabel(title_xAxis)
	plt.ylabel(title_yAxis)
	plt.xlim(xmin=limit_xMin, xmax=limit_xMax)
	
	plt.figure(GRAPH_AVERAGE_TITLE)
	plt.title(GRAPH_AVERAGE_TITLE)
	plt.xlabel(title_xAxis)
	plt.ylabel(title_yAxis)
	plt.xlim(xmin=limit_xMin, xmax=limit_xMax)
	
	return plt

def computeJcnAverages(dataSeries):
	jcnVerticesTotal = 0
	jcnEdgesTotal = 0
	mdrgVerticesTotal = 0
	sampleCount = 0
	
	for d in enumerate(dataSeries):
		#	Extract information from the tuple { slice, { jcnVertices, jcnEdges, mdrgVertices } )
		slice = d[0]
		jcnVertexCount = d[1].jcnVertices
		jcnEdgeCount = d[1].jcnEdges
		mdrgVertexCount = d[1].mdrgVertices
		
		#print('Time slice: %d, duration: %d %s' %(slice, duration, unit))
		jcnVerticesTotal += jcnVertexCount
		jcnEdgesTotal += jcnEdgeCount
		mdrgVerticesTotal += mdrgVertexCount
		
		sampleCount += 1
		
	if sampleCount > 0:
		jcnVerticesAverage = jcnVerticesTotal / sampleCount
		jcnEdgesAverage = jcnEdgesTotal / sampleCount
		mdrgVerticesAverage = mdrgVerticesTotal / sampleCount
	else:
		return JcnData(0, 0, 0)
	#print('Duration total: %d, count: %d, average %d' %(totalDuration, count, average))
	
	return (JcnData(jcnVerticesAverage, jcnEdgesAverage, mdrgVerticesAverage))
	
#
#	\brief		Evaluates an entire cooling slice, returning the average on the
#				x, y, z ant axis as a tuple.
#	
#	\param		coolingDir: the base directory containing the files relating to an
#				entire iteration of cooling
#	\author		Dean
#	\since		07-03-2016
#
def evaluateOutputFiles(coolingDir):
	#	Obtain list of files in the current directory
	files = [f for f in listdir(coolingDir) if isfile(join(coolingDir, f))]
	
	#	Storage for each series - store data as follows:
	#	{ 'x', [{ 3, { 42000, 'milliseconds'} }, { 4, { 67320, 'milliseconds'} }, ... ] }
	dataSeries = { 'x' : [], 'y' : [], 'z' : [], 't' : []}
	averages = { 'x' : JcnData, 'y' : JcnData, 'z' : JcnData, 't' : JcnData }
	
	for file in files:
		matches = re.search(JCN_COMPUTATION_OUTPUT_REGEX, file)
		
		if matches != None:
			#print('Processing %s' %(matches.group(0)))
			assert (len(matches.groups()) == 4), "Only bi-variate data is supported at present"
			
			#	Extract information about the log file
			logFilename  = matches.group(0)
			field1 = matches.group(1)
			field2 = matches.group(2)
			#field3 = matches.group(3)
			sliceAxis = matches.group(3)
			sliceValue = matches.group(4)
			
			#	make sure the 'sliceAxis' is valid
			assert sliceAxis in dataSeries.keys(), "Invalid slice extracted from filename"
			
			#	extract the JCN data for the current file
			filePath = coolingDir + '\\' + logFilename
			jcnData = extractOutput(filePath)
			
			if jcnData != None:
				dataSeries[sliceAxis].append(jcnData)
	
	#	Compute averages
	averages['x'] = computeJcnAverages(dataSeries['x'])
	averages['y'] = computeJcnAverages(dataSeries['y'])
	averages['z'] = computeJcnAverages(dataSeries['z'])
	averages['t'] = computeJcnAverages(dataSeries['t'])
	
	return [averages['x'], averages['y'], averages['z'], averages['t']]

def analyseConfigurationOutputFiles(configurationDir):
	#	Obtain list of directories in the current directory
	dirs = [f for f in listdir(configurationDir) if isdir(join(configurationDir, f))]
	
	averages = {}
	
	for dir in dirs:
		matches = re.search(JCN_COOLING_DIR_REGEX, dir)
		
		if (matches != None):
			#	cooling dir
			coolingDir = configurationDir+ '\\'+ matches.group(0)
			coolingIter = int(matches.group(1))
			
			#print("%i %s" %(coolingIter, coolingDir))
			averages[coolingIter] = evaluateOutputFiles(coolingDir)
			
	return averages

def analyseOutputFiles():
	#	Create output graphs for 0-20 cools
	plt = createGraphs("cools", 0, 20, "vertex / edge count")
	
	#	The input configurations to analyse
	CONF_1 = R"C:\Users\4thFloor\Desktop\EnsembleTest\conp0052"
	CONF_2 = R"C:\Users\4thFloor\Desktop\EnsembleTest\conp0056"
	CONF_3 = R"C:\Users\4thFloor\Desktop\EnsembleTest\conp0060"
	
	analyseConfigurationOutputFiles(CONF_1)
	
	#	Storage for duration data
	stats = []
	stats.append(analyseConfigurationOutputFiles(CONF_1))
	stats.append(analyseConfigurationOutputFiles(CONF_2))
	stats.append(analyseConfigurationOutputFiles(CONF_3))
	
	plotJcnData(plt, extractConfigurationName(CONF_1), stats[0])
	plotJcnData(plt, extractConfigurationName(CONF_2), stats[1])
	plotJcnData(plt, extractConfigurationName(CONF_3), stats[2])
	
	plt.show()
	
def analyseLogFiles():
	#	Create output graphs for 0-20 cools
	plt = createGraphs("cools", 0, 20, "duration (ms)")

	#	The input configurations to analyse
	CONF_1 = R"C:\Users\4thFloor\Desktop\EnsembleTest\conp0052"
	CONF_2 = R"C:\Users\4thFloor\Desktop\EnsembleTest\conp0056"
	CONF_3 = R"C:\Users\4thFloor\Desktop\EnsembleTest\conp0060"

	#	Storage for duration data
	stats = []
	stats.append(analyseConfigurationLogFiles(CONF_1))
	stats.append(analyseConfigurationLogFiles(CONF_2))
	stats.append(analyseConfigurationLogFiles(CONF_3))

	#	Plot the cooling series dat for each input configuration
	plotLogData(plt, extractConfigurationName(CONF_1), stats[0])
	plotLogData(plt, extractConfigurationName(CONF_2), stats[1])
	plotLogData(plt, extractConfigurationName(CONF_3), stats[2])
	
	#	Show our graphs
	plt.show()
	
#
#	\brief		Core function for loading and visualizing data.
#	\author		Dean
#	\since		10-03-2016
#
def main():
	#analyseLogFiles()
	analyseOutputFiles()
	
main()