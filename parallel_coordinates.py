import numpy as np
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker

#vectors to plot: 4D for this example
q_max1=[2, 16, 5, 4]
q_max2=[4, 16, 8, 12]
q_max3=[6, 14, 4 , 10]
q_max4=[6, 14, 4 , 10]
q_max5=[6, 14, 4 , 10]
q_max6=[7, 5, 12, 12]
q_max7=[7, 5, 12, 12]
q_max8=[14, 2, 4, 2]
q_max9=[14, 2, 4, 2]
q_max10=[14, 2, 4, 2]
q_max11=[14, 2, 4, 2]
q_max12=[14, 2, 4, 2]
q_max13=[14, 2, 4, 2]
q_max14=[14, 2, 4, 2]
q_max15=[14, 2, 4, 2]
q_max16=[12, 14, 4, 3]
q_max17=[12, 14, 4, 3]
q_max18=[ 9, 14, 1, 29]
q_max19=[12, 14, 4, 3]
q_max20=[12, 14, 4, 3]
q_max21=[1, 7, 13, 15]
q_max22=[1, 7, 13, 15]
q_max23=[1, 7, 13, 15]
q_max24=[1, 7, 13, 15]
q_max25=[1, 7, 13, 15]
q_max26=[1, 7, 13, 15]
q_max27=[1, 7, 13, 15]
q_max28=[12, 2, 5, 15]
q_max29=[12, 2, 5, 15]
q_max30=[1, 10, 2, 27]

q_min1=[9, 13, 3, 6]
q_min2=[6, 13, 15, 23]
q_min3=[4, 15, 6, 14]
q_min4=[14, 6, 7, 13]
q_min5=[7, 3, 1, 29]
q_min6=[9, 14, 1, 30]
q_min7=[9, 14, 1, 30]
q_min8=[9, 14, 1, 30]
q_min9=[9, 14, 1, 30]
q_min10=[9, 14, 1, 30]
q_min11=[9, 14, 1, 30]
q_min12=[9, 14, 1, 30]
q_min13=[9, 14, 1, 30]
q_min14=[9, 14, 1, 30]
q_min15=[9, 14, 1, 30]
q_min16=[9, 14, 1, 30]
q_min17=[4, 8, 9, 5]
q_min18=[12, 2, 5, 16]
q_min19=[5, 12, 16, 16]
q_min20=[12, 2, 5, 16]
q_min21=[12, 2, 5, 16]
q_min22=[12, 2, 5, 16]
q_min23=[12, 2, 5, 16]
q_min24=[12, 2, 5, 16]
q_min25=[12, 2, 5, 16]
q_min26=[7, 8, 11, 24]
q_min27=[7, 8, 11, 24]
q_min28=[7, 8, 11, 24]
q_min29=[7, 8, 11, 24]
q_min30=[7, 8, 11, 24]

x=[1,2,3,4] # spines

fig,(ax,ax2,ax3) = plt.subplots(1, 3, sharey=False)

# plot the same on all the subplots
ax.plot(x,q_max1,'r-', x,q_max2,'r-', x,q_max3, 'r-', x,q_max4, 'r-', x,q_max5, 'r-', x,q_max6, 'r-', x,q_max7, 'r-', x,q_max8, 'r-', x,q_max9, 'r-', x,q_max10, 'r-', x,q_max11, 'r-', x,q_max12, 'r-', x,q_max13, 'r-', x,q_max14, 'r-', x,q_max15, 'r-', x,q_max16, 'r-',x,q_max17, 'r-', x,q_max18, 'r-', x,q_max19, 'r-', x,q_max20, 'r-', x,q_max21, 'r-', x,q_max22, 'r-', x,q_max23, 'r-', x,q_max24, 'r-', x,q_max25, 'r-', x,q_max26, 'r-', x,q_max27, 'r-', x,q_max28, 'r-', x,q_max29, 'r-', x,q_max30, 'r-', alpha=0.2)
ax2.plot(x,q_max1,'r-', x,q_max2,'r-', x,q_max3, 'r-', x,q_max4, 'r-', x,q_max5, 'r-', x,q_max6, 'r-', x,q_max7, 'r-', x,q_max8, 'r-', x,q_max9, 'r-', x,q_max10, 'r-', x,q_max11, 'r-', x,q_max12, 'r-', x,q_max13, 'r-', x,q_max14, 'r-', x,q_max15, 'r-', x,q_max16, 'r-',x,q_max17, 'r-', x,q_max18, 'r-', x,q_max19, 'r-', x,q_max20, 'r-', x,q_max21, 'r-', x,q_max22, 'r-', x,q_max23, 'r-', x,q_max24, 'r-', x,q_max25, 'r-', x,q_max26, 'r-', x,q_max27, 'r-', x,q_max28, 'r-', x,q_max29, 'r-', x,q_max30, 'r-', alpha=0.2)
ax3.plot(x,q_max1,'r-', x,q_max2,'r-', x,q_max3, 'r-', x,q_max4, 'r-', x,q_max5, 'r-', x,q_max6, 'r-', x,q_max7, 'r-', x,q_max8, 'r-', x,q_max9, 'r-', x,q_max10, 'r-', x,q_max11, 'r-', x,q_max12, 'r-', x,q_max13, 'r-', x,q_max14, 'r-', x,q_max15, 'r-', x,q_max16, 'r-',x,q_max17, 'r-', x,q_max18, 'r-', x,q_max19, 'r-', x,q_max20, 'r-', x,q_max21, 'r-', x,q_max22, 'r-', x,q_max23, 'r-', x,q_max24, 'r-', x,q_max25, 'r-', x,q_max26, 'r-', x,q_max27, 'r-', x,q_max28, 'r-', x,q_max29, 'r-', x,q_max30, 'r-', alpha=0.2)

ax.plot(x,q_min1,'b-', x,q_min2,'b-', x,q_min3, 'b-', x,q_min4, 'b-', x,q_min5, 'b-', x,q_min6, 'b-', x,q_min7, 'b-', x,q_min8, 'b-', x,q_min9, 'b-', x,q_min10, 'b-', x,q_min11, 'b-', x,q_min12, 'b-', x,q_min13, 'b-', x,q_min14, 'b-', x,q_min15, 'b-', x,q_min16, 'b-',x,q_min17, 'b-', x,q_min18, 'b-', x,q_min19, 'b-', x,q_min20, 'b-', x,q_min21, 'b-', x,q_min22, 'b-', x,q_min23, 'b-', x,q_min24, 'b-', x,q_min25, 'b-', x,q_min26, 'b-', x,q_min27, 'b-', x,q_min28, 'b-', x,q_min29, 'b-', x,q_min30, 'b-', alpha=0.2)
ax2.plot(x,q_min1,'b-', x,q_min2,'b-', x,q_min3, 'b-', x,q_min4, 'b-', x,q_min5, 'b-', x,q_min6, 'b-', x,q_min7, 'b-', x,q_min8, 'b-', x,q_min9, 'b-', x,q_min10, 'b-', x,q_min11, 'b-', x,q_min12, 'b-', x,q_min13, 'b-', x,q_min14, 'b-', x,q_min15, 'b-', x,q_min16, 'b-',x,q_min17, 'b-', x,q_min18, 'b-', x,q_min19, 'b-', x,q_min20, 'b-', x,q_min21, 'b-', x,q_min22, 'b-', x,q_min23, 'b-', x,q_min24, 'b-', x,q_min25, 'b-', x,q_min26, 'b-', x,q_min27, 'b-', x,q_min28, 'b-', x,q_min29, 'b-', x,q_min30, 'b-', alpha=0.2)
ax3.plot(x,q_min1,'b-', x,q_min2,'b-', x,q_min3, 'b-', x,q_min4, 'b-', x,q_min5, 'b-', x,q_min6, 'b-', x,q_min7, 'b-', x,q_min8, 'b-', x,q_min9, 'b-', x,q_min10, 'b-', x,q_min11, 'b-', x,q_min12, 'b-', x,q_min13, 'b-', x,q_min14, 'b-', x,q_min15, 'b-', x,q_min16, 'b-',x,q_min17, 'b-', x,q_min18, 'b-', x,q_min19, 'b-', x,q_min20, 'b-', x,q_min21, 'b-', x,q_min22, 'b-', x,q_min23, 'b-', x,q_min24, 'b-', x,q_min25, 'b-', x,q_min26, 'b-', x,q_min27, 'b-', x,q_min28, 'b-', x,q_min29, 'b-', x,q_min30, 'b-', alpha=0.2)

# now zoom in each of the subplots 
ax.set_xlim([ x[0],x[1]])
ax2.set_xlim([ x[1],x[2]])
ax3.set_xlim([ x[2],x[3]])

# set the x axis ticks 
for axx,xx in zip([ax,ax2,ax3],x[:-1]):
  axx.xaxis.set_major_locator(ticker.FixedLocator([xx]))
ax3.xaxis.set_major_locator(ticker.FixedLocator([x[-2],x[-1]]))  # the last one

# EDIT: add the labels to the rightmost spine
for tick in ax3.yaxis.get_major_ticks():
  tick.label2On=True

# stack the subplots together
plt.subplots_adjust(wspace=0)

plt.show()