from OpenGL.GL import *
import math

class OBJ:
    """
    Taken from http://www.pygame.org/wiki/OBJFileLoader [10-10-2016]
    """
    def __init__(self, filename, swapyz=False):
        """Loads a Wavefront OBJ file. """
        self.vertices = []
        self.normals = []
        self.texcoords = []
        self.faces = []

        material = None
        for line in open(filename, "r"):
            if line.startswith('#'): continue
            values = line.split()
            if not values: continue
            if values[0] == 'v':
                v = map(float, values[1:4])
                if swapyz:
                    v = v[0], v[2], v[1]
                self.vertices.append(v)
            elif values[0] == 'vn':
                v = map(float, values[1:4])
                if swapyz:
                    v = v[0], v[2], v[1]
                self.normals.append(v)
            elif values[0] == 'vt':
                self.texcoords.append(map(float, values[1:3]))
            #elif values[0] in ('usemtl', 'usemat'):
            #    material = values[1]
            #elif values[0] == 'mtllib':
            #    self.mtl = MTL(values[1])
            elif values[0] == 'f':
                face = []
                texcoords = []
                norms = []
                for v in values[1:]:
                    w = v.split('/')
                    face.append(int(w[0]))
                    if len(w) >= 2 and len(w[1]) > 0:
                        texcoords.append(int(w[1]))
                    else:
                        texcoords.append(0)
                    if len(w) >= 3 and len(w[2]) > 0:
                        norms.append(int(w[2]))
                    else:
                        norms.append(0)
                self.faces.append((face, norms, texcoords, material))

        self.gl_list = glGenLists(1)
        glNewList(self.gl_list, GL_COMPILE)
        glEnable(GL_TEXTURE_2D)
        glFrontFace(GL_CCW)
        for face in self.faces:
            vertices, normals, texture_coords, material = face

            #mtl = self.mtl[material]
            #if 'texture_Kd' in mtl:
                # use diffuse texmap
            #    glBindTexture(GL_TEXTURE_2D, mtl['texture_Kd'])
            #else:
                # just use diffuse colour
             #   glColor(*mtl['Kd'])

            #glBegin(GL_POLYGON)
            #for i in range(len(vertices)):
            #    if normals[i] > 0:
            #        glNormal3fv(self.normals[normals[i] - 1])
            #    if texture_coords[i] > 0:
            #        glTexCoord2fv(self.texcoords[texture_coords[i] - 1])
            #    glVertex3fv(self.vertices[vertices[i] - 1])
            #glEnd()
        glDisable(GL_TEXTURE_2D)
        glEndList()

    def surface_area(self):
        result = 0

        for f in self.faces:
            print(len(f))
            print(f)
            assert((len(f) is 4) and (f[len(f)-1] is None))

            #   Extract the indices of the vertices making up this triangle (assert it is a triangle first)
            assert(len(f[0]) is 3)
            v_indices = f[0]

            #   Now extract the actual coordinates for each vertex
            v0 = list(map(float, self.vertices[v_indices[0]-1]))
            v1 = list(map(float, self.vertices[v_indices[1]-1]))
            v2 = list(map(float, self.vertices[v_indices[2]-1]))

            #print(v0[0], v0[1], v0[2])
            #print(v1[0], v1[1], v1[2])
            #print(v2[0], v2[1], v2[2])

            #   Calculate Euclidean distance between two vertices
            length_of = lambda v_a, v_b: \
                math.sqrt(((v_a[0] - v_b[0]) ** 2) + ((v_a[1] - v_b[1]) ** 2) + ((v_a[2] - v_b[2]) ** 2))

            #   Lengths of sides
            a = length_of(v0, v1)
            b = length_of(v1, v2)
            c = length_of(v2, v0)

            #print(a, b, c)

            #   Semi-perimeter
            s = (a + b + c) / 2

            #print(s)

            #   Area
            area = math.sqrt(s * (s-a) * (s-b) * (s-c))

            print('area of triangle: {:}'.format(area))

            result += area

        return result

test = OBJ(R'/home/dean/vm_share/slab_14.obj')

print('Area of surface: {:}'.format(test.surface_area()))