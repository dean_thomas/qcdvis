import os

field_filename = 'polyakov.vol1'

cooling_dirs = []

for (dir_path, sub_dir_names, filenames) in os.walk(R'/media/dean/raw_data/Dean/QCD Data/Field data/Pygrid/12x24/mu1000/config.b190k1680mu1000j02s12t24/conp0100'):
    for filename in filenames:
        if filename == field_filename:
            print dir_path
            print filename

            cooling_dirs.append(dir_path)

cooling_dirs.sort()
print(cooling_dirs)

database_file = open(R'/home/dean/Desktop/cooling.visit', 'w')
for dir in cooling_dirs:
    database_file.write(os.path.join(dir, field_filename) + '\n')
database_file.close()