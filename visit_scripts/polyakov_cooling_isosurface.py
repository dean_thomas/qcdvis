from visit_utils import *


def setup_plot():
    #  Add the plot
    AddPlot("Pseudocolor", "var03")

    # Modify the options of the plot
    options = PseudocolorAttributes()
    options.colorTableName = "viridis"
    options.minFlag, options.min = 1, -1.0
    options.maxFlag, options.max = 1, +1.0

    # Apply options
    SetPlotOptions(options)
    return


def setup_resample(samples_x=12, samples_y=12, samples_z=12):
    # Add the resample operator for the current plot
    AddOperator("Resample")

    # Modify the options of the resampler
    options = ResampleAttributes()
    options.samplesX = samples_x
    options.samplesY = samples_y
    options.samplesZ = samples_z
    options.cellCenteredOutput = True

    # Apply options
    SetOperatorOptions(options)
    return


def setup_isosurface(surface_count=10):
    # Add the isosurface operator for the current plot
    AddOperator("Isosurface")

    # Modify the options of the isosurface operator
    options = IsosurfaceAttributes()
    options.contourNLevels = surface_count
    options.minFlag, options.min = 1, -1.0
    options.maxFlag, options.max = 1, +1.0

    # Apply options
    SetOperatorOptions(options)
    return

DeleteAllPlots()

setup_plot()
setup_resample()
setup_isosurface()

DrawPlots()
