///////////////////////////////////////////////////
//
//	Contour-tree based isosurfaces
//	Version 4.0 (QT-based)
//	copyright Hamish Carr
//	January, 2014
//
//	------------------------
//	main.cpp
//	------------------------
//	
///////////////////////////////////////////////////

#include <QApplication>
#include "FlexibleIsosurface.h"
#include "FlexibleIsosurfaceWindow.h"
#include "FlexibleIsosurfaceController.h"
#include <stdio.h>

int main(int argc, char **argv)
	{ // main()
	// initialize QT
	QApplication app(argc, argv);

	// check the args to make sure there's an input file
	if (argc < 2) { printf("Usage: %s filename [options]\n", argv[0]); exit (0);};

	//  use that to create a height field &c.
	FlexibleIsosurface *flexIso = new FlexibleIsosurface(argc, argv);

	//	create a window
	FlexibleIsosurfaceWindow *aWindow = new FlexibleIsosurfaceWindow(flexIso, argv[1]);
	
	//  create a controller
	FlexibleIsosurfaceController *aController = new FlexibleIsosurfaceController(flexIso, aWindow);

	// 	set the initial size
	aWindow->resize(1000, 600);

	// show the window
	aWindow->show();

	// set QT running
	return app.exec();
	} // main()
