///////////////////////////////////////////////////
//
//	Contour-tree based isosurfaces
//	Version 4.0 (QT-based)
//	copyright Hamish Carr
//	January, 2014
//
//	------------------------
//	FlexibleIsosurface.h
//	------------------------
//	
//	A unit encapsulating a flexible isosurface as 	
//	a model in the MVC paradigm
//	
//	Of course, since the contour tree, &c. are all
//	tangled up in the Height Field, it's imperfect
//	but the goal is to port the *interface*
//	
///////////////////////////////////////////////////

#ifndef _HEADER_FLEXIBLE_ISOSURFACE_H
#define _HEADER_FLEXIBLE_ISOSURFACE_H

#include "HeightField.h"

class FlexibleIsosurface
	{ // class FlexibleIsosurface
	public:
	// underlying height field (note: includes the contour tree data)
	HeightField *field;

	// current isovalue (scaled to 0.0 - 1.0)
	float isoValue;

	// scale for zooming
	float scale;
  	//	rotation matrices for object & light
	GLfloat rotMat[16], lightMat[16];

	// flag for whether contour tree is coloured
	bool colouredTree;
	// flag for whether contour tree is shown
	bool showTree;
	// flag for whether contours are locally defined
	bool useLocalContours;

	// which measure to use for collapse priority
	long collapsePriority;

	// colour of the current selection
	long selectionColour;	
  
	// constructor
	FlexibleIsosurface(int argc, char **argv);
	
	// routine to reset the flexible isosurface to an isosurface
	void ResetIsosurface(float value);
	
	// routine to reset the flexible isosurface to local contours
	void ResetLocalContours(float value);
	
	}; // class FlexibleIsosurface

#endif