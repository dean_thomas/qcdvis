#ifndef ADDITIONALDEFINES_H
#define ADDITIONALDEFINES_H

//  Defines added by me (Dean)
//  11-11-2014

////////////////////////////////////////////////////////////////////////
///
///                     HeightField_Construction.cpp                 ///
///
////////////////////////////////////////////////////////////////////////

//  Output to .dot
#define OUTPUT_JOIN_TREE 1
#define OUTPUT_SPLIT_TREE 1
#define OUTPUT_CONTOUR_TREE_MORE 1
#define OUTPUT_CONTOUR_TREE_LESS 1

//  Output locations
#define JOIN_TREE_FILENAME "C:\\Jointree.dot"
#define SPLIT_TREE_FILENAME "C:\\Splittree.dot"
#define CONTOUR_TREE_MORE_FILENAME "C:\\ContourTree.dot"
#define CONTOUR_TREE_LESS_FILENAME "C:\\ContourTree_Small.dot"

#endif // ADDITIONALDEFINES_H
