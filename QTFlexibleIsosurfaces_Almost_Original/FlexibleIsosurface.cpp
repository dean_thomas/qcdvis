///////////////////////////////////////////////////
//
//	Contour-tree based isosurfaces
//	Version 4.0 (QT-based)
//	copyright Hamish Carr
//	January, 2014
//
//	------------------------
//	FlexibleIsosurface.cpp
//	------------------------
//	
//	A unit encapsulating a flexible isosurface as 	
//	a model in the MVC paradigm
//	
//	Of course, since the contour tree, &c. are all
//	tangled up in the Height Field, it's imperfect
//	but the goal is to port the *interface*
//	
///////////////////////////////////////////////////

#include "FlexibleIsosurface.h"
#include <stdio.h>

// constructor
FlexibleIsosurface::FlexibleIsosurface(int argc, char **argv)
	{ // FlexibleIsosurface::FlexibleIsosurface()
	// use the CLI parameters to load the data & compute the contour tree
	field = new HeightField(argc, argv);

	// set initial scale & rotation
	scale = 0.0;
	// this sets the homogeneous matrix to the identity
	for (int i = 0; i < 16; i++) 
		if (i/4 == i%4) 
			rotMat[i] = lightMat[i] = 1.0; 
		else 
			rotMat[i] = lightMat[i] = 0.0;

	// set display flags
	colouredTree		= false;
	showTree			= true;
	useLocalContours 	= false;

	// set collapse priority
	collapsePriority	= PRIORITY_RIEMANN_SUM;

	// and selection colour
	selectionColour = -1;

	// tell field to recollapse
	field->ResetCollapsePriority(collapsePriority);
	
	// initial isovalue is 0.5 of range
	ResetIsosurface(0.5);
	} // FlexibleIsosurface::FlexibleIsosurface()

// routine to reset the flexible isosurface to an isosurface
void FlexibleIsosurface::ResetIsosurface(float value)
	{ // FlexibleIsosurface::ResetIsosurface()
	// reset the stored isovalue
	isoValue = value;
	
	// now call the routine to set the flexible isosurface to a standard isosurface
	field->SetIsosurface(field->MinHeight() + isoValue * 
						(field->MaxHeight() - field->MinHeight()));
	} // FlexibleIsosurface::ResetIsosurface()
	
// routine to reset the flexible isosurface to local contours
void FlexibleIsosurface::ResetLocalContours(float value)
	{ // FlexibleIsosurface::ResetLocalContours()
	// reset the stored isovalue
	isoValue = value;
	
	// now call the routine to set the flexible isosurface to a standard isosurface
	field->SetLocal(isoValue);
	} // FlexibleIsosurface::ResetLocalContours()
	

	