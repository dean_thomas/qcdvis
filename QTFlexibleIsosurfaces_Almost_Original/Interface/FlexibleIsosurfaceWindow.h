///////////////////////////////////////////////////
//
//	Contour-tree based isosurfaces
//	Version 3.0 (QT-based)
//	copyright Hamish Carr
//	January, 2014
//
//	------------------------
//	FlexibleIsosurfaceWindow.h
//	------------------------
//	
///////////////////////////////////////////////////

#ifndef _HEADER_FLEXIBLE_ISOSURFACE_WINDOW_H
#define _HEADER_FLEXIBLE_ISOSURFACE_WINDOW_H

#include <QWidget>
#include <QtGui>
#include <QGridLayout>
#include <QCheckBox>
#include <QLabel>
#include <QPushButton>
#include <QComboBox>
#include <QSlider>

#include "ArcBallWidget.h"
#include "FlexibleIsosurfaceWidget.h"
#include "ContourTreeWidget.h"
#include "LogCollapseWidget.h"
#include "ColourSelectionWidget.h"

// a window that displays an isosurface with controls
class FlexibleIsosurfaceWindow : public QWidget
	{ // class FlexibleIsosurfaceWindow
	public:
	// pointer to the model - the flexible isosurface
	FlexibleIsosurface 			*flexIso;

	// window layout	
	QGridLayout 				*windowLayout;

	// custom widgets
	ArcBallWidget 				*lightRotator;
	ArcBallWidget 				*rotator;
	ColourSelectionWidget 		*colourSelector;
	ContourTreeWidget 			*contourTreeWidget;
	FlexibleIsosurfaceWidget	*flexibleIsosurfaceWidget;
	LogCollapseWidget 			*logCollapseWidget;

	// standard widgets
	QCheckBox 					*colouredContourBox;
	QCheckBox 					*colourTreeBox;
	QCheckBox 					*localContourBox;
	QCheckBox 					*showTreeBox;
	QComboBox 					*measureChooser;
	QLabel 						*collapseLabel;
	QLabel 						*infoLabel;
	QLabel 						*lightRotatorLabel;
	QLabel 						*measureLabel;
	QLabel 						*rotatorLabel;
	QLabel 						*stackScaleLabel;
	QLabel 						*treeInfoLabel;
	QLabel 						*zoomLabel;
	QPushButton					*collapseButton;
	QPushButton					*deleteComplementButton;
	QPushButton					*deleteSelectionButton;
	QPushButton					*dotLayoutButton;
	QPushButton					*loadLayoutButton;
	QPushButton					*restoreDeletedButton;
	QPushButton					*saveLayoutButton;
	QPushButton					*uncollapseButton;
	QPushButton					*valueMinusButton;
	QPushButton					*valuePlusButton;
	QSlider						*isovalueSlider;
	QSlider						*leafSizeSlider;
	QSlider						*stackScaleSlider;
	QSlider						*treeSizeSlider;
	QSlider						*zoomSlider;

	// constructor
	FlexibleIsosurfaceWindow(FlexibleIsosurface *newFlexIso, const char *windowName = "Isosurface");	
	
	// routine to reset control elements
	void ResetControlElements();

	}; // class IsosurfaceWindow

#endif
