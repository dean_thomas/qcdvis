///////////////////////////////////////////////////
//
//	Contour-tree based isosurfaces
//	Version 4.0 (QT-based)
//	copyright Hamish Carr
//	January, 2014
//
//	------------------------
//	FlexibleIsosurfaceWindow.cpp
//	------------------------
//	
///////////////////////////////////////////////////

#include "FlexibleIsosurfaceWindow.h"

FlexibleIsosurfaceWindow::FlexibleIsosurfaceWindow(FlexibleIsosurface *newFlexIso, const char *windowName)
	: QWidget(NULL)
	{ // FlexibleIsosurfaceWindow::FlexibleIsosurfaceWindow()
	// set the window's title
	setWindowTitle(QString(windowName));
	
	// store the pointer to the model
	flexIso = newFlexIso;
	
	// initialise the grid layout
	windowLayout = new QGridLayout(this);
	
	// the custom Widgets
	colourSelector 				= new ColourSelectionWidget		(flexIso, 				this);
	contourTreeWidget 			= new ContourTreeWidget			(flexIso, 				this);
	flexibleIsosurfaceWidget 	= new FlexibleIsosurfaceWidget	(flexIso, 				this);
	logCollapseWidget 			= new LogCollapseWidget			(flexIso, 				this);

	// construct custom stock Widgets
	lightRotator 				= new ArcBallWidget				(						this);
	rotator 					= new ArcBallWidget				(						this);

	// construct standard QT widgets
	measureChooser 				= new QComboBox					(						this);
	measureChooser->addItem("Height (Persistence)");
	measureChooser->addItem("Volume");
	measureChooser->addItem("Height * Volume");
	measureChooser->addItem("Riemann Hypervolume");

	colouredContourBox 			= new QCheckBox					("Coloured Contours", 	this);
	colourTreeBox 				= new QCheckBox					("Colour Tree", 		this);
	localContourBox 			= new QCheckBox					("Local Contours", 		this);
	showTreeBox 				= new QCheckBox					("Show Tree", 			this);

	collapseLabel				= new QLabel					("V: Smallest Measure.  H: Tree Size",		this);
	infoLabel 					= new QLabel					("LM: rotate RM: select", 	this);
	lightRotatorLabel 			= new QLabel					("Light", 				this);
	measureLabel 				= new QLabel					("Geometric Measure",	this);
	rotatorLabel 				= new QLabel					("Rotation", 			this);
	stackScaleLabel				= new QLabel					("Stack Scale", 		this);
	treeInfoLabel				= new QLabel					("V: Isovalue LM: drag nodes RM: drag tags", 	this);
	zoomLabel 					= new QLabel					("Zoom (log scale)", 	this);
	
	collapseButton 				= new QPushButton				("Collapse", 			this);
	deleteComplementButton 		= new QPushButton				("Delete Complement", 	this);
	deleteSelectionButton 		= new QPushButton				("Delete Selection", 	this);
	dotLayoutButton 			= new QPushButton				("Dot Layout", 			this);
	loadLayoutButton 			= new QPushButton				("Load Layout", 		this);
	restoreDeletedButton 		= new QPushButton				("Restore Deleted", 	this);
	saveLayoutButton 			= new QPushButton				("Save Layout", 		this);
	uncollapseButton 			= new QPushButton				("Uncollapse", 			this);
	valueMinusButton 			= new QPushButton				("Value --", 			this);
	valuePlusButton 			= new QPushButton				("Value ++", 			this);

	isovalueSlider 				= new QSlider					(Qt::Vertical, 			this);
	leafSizeSlider 				= new QSlider					(Qt::Vertical, 			this);
	stackScaleSlider			= new QSlider					(Qt::Horizontal, 		this);
	treeSizeSlider 				= new QSlider					(Qt::Horizontal, 		this);
	zoomSlider 					= new QSlider					(Qt::Horizontal,		this);

	// do the layout									R	C	RS	CS
	// Row 0
	windowLayout->addWidget(flexibleIsosurfaceWidget,	0, 	0, 	1, 	6);
	windowLayout->addWidget(isovalueSlider, 			0, 	6, 	1, 	1);
	windowLayout->addWidget(contourTreeWidget, 			0, 	7, 	1, 	3);
	windowLayout->addWidget(logCollapseWidget, 			0, 	10,	1, 	2);
	windowLayout->addWidget(leafSizeSlider, 			0, 	12,	1, 	1);
	
	// Row 1
	windowLayout->addWidget(rotator, 					1, 	0, 	2, 	1);
	windowLayout->addWidget(lightRotator, 				1, 	1, 	2, 	1);
	windowLayout->addWidget(zoomSlider, 				1, 	2, 	1, 	3);
	windowLayout->addWidget(zoomLabel, 					1, 	5, 	1, 	1);
	windowLayout->addWidget(showTreeBox, 				1, 	7, 	1, 	1);
	windowLayout->addWidget(colourTreeBox,				1, 	8, 	1, 	1);
	windowLayout->addWidget(treeSizeSlider, 			1,  10,	1, 	2);

	// Row 2
	windowLayout->addWidget(stackScaleSlider,			2, 	2, 	1, 	3);
	windowLayout->addWidget(stackScaleLabel,			2, 	5, 	1, 	1);
	windowLayout->addWidget(treeInfoLabel,				2,	7,	1,	3);
	windowLayout->addWidget(collapseLabel,				2,	10,	1,	3);

	// Row 3
	windowLayout->addWidget(rotatorLabel, 				3, 	0, 	1, 	1);
	windowLayout->addWidget(lightRotatorLabel,			3, 	1, 	1, 	1);
	windowLayout->addWidget(infoLabel,					3,	3,	1,	3);
	windowLayout->addWidget(colourSelector,				3,	7,	1,	3);
	windowLayout->addWidget(collapseButton,				3, 	10,	1, 	1);
	windowLayout->addWidget(uncollapseButton,			3, 	11,	1, 	1);
	
	// Row 4
	windowLayout->addWidget(colouredContourBox, 		4, 	0, 	1, 	2);
	windowLayout->addWidget(deleteSelectionButton, 		4, 	4, 	1, 	1);
	windowLayout->addWidget(restoreDeletedButton, 		4, 	5, 	1, 	1);
	windowLayout->addWidget(dotLayoutButton,	 		4, 	7, 	1, 	1);
	windowLayout->addWidget(saveLayoutButton, 			4, 	8, 	1, 	1);
	windowLayout->addWidget(loadLayoutButton, 			4, 	9, 	1, 	1);
	windowLayout->addWidget(measureLabel,				4,	10,	1,	3);

	// Row 5
	windowLayout->addWidget(localContourBox, 			5, 	0, 	1, 	2);
	windowLayout->addWidget(deleteComplementButton, 	5, 	4, 	1, 	1);
	windowLayout->addWidget(valuePlusButton,			5, 	7, 	1, 	1);
	windowLayout->addWidget(valueMinusButton,			5, 	8, 	1, 	1);
	windowLayout->addWidget(measureChooser,				5,	10,	1,	3);

	// set resize policy
	windowLayout->setColumnStretch(3, 1);

	// force a refresh
	ResetControlElements();
	} // FlexibleIsosurfaceWindow::FlexibleIsosurfaceWindow()

// routine to reset control elements
void FlexibleIsosurfaceWindow::ResetControlElements()
	{ // FlexibleIsosurfaceWindow::ResetControlElements()
	// standard widgets
	colouredContourBox		->setChecked		(flexIso	->field->differentColouredContours);
	colourTreeBox			->setChecked		(flexIso	->colouredTree);
	localContourBox			->setChecked		(flexIso	->useLocalContours);
	showTreeBox				->setChecked		(flexIso	->showTree);
	measureChooser			->setCurrentIndex	(flexIso	->collapsePriority);
	isovalueSlider			->setMinimum(0);
	isovalueSlider			->setMaximum(10000);
	isovalueSlider			->setValue(int	(flexIso->isoValue * 10000.0));
	leafSizeSlider			->setMinimum(0);
	leafSizeSlider			->setMaximum(10000);
	leafSizeSlider			->setValue(flexIso->field->logPriorityBound * 10000.0);
	stackScaleSlider		->setMinimum(100);
	stackScaleSlider		->setMaximum(10000);
	stackScaleSlider		->setValue(int	(flexIso->field->stackScale * 1000.0));
	treeSizeSlider			->setMinimum(0);
	treeSizeSlider			->setMaximum(10000);
	treeSizeSlider			->setValue(flexIso->field->logTreeSize * 10000.0);
	zoomSlider				->setMinimum(-10000);
	zoomSlider				->setMaximum(10000);
	zoomSlider				->setValue(int	(flexIso->scale * 5000.0));
	} // FlexibleIsosurfaceWindow::ResetControlElements()
