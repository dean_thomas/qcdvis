///////////////////////////////////////////////////
//
//	Contour-tree based isosurfaces
//	Version 4.0 (QT-based)
//	copyright Hamish Carr
//	January, 2014
//
//	------------------------
//	FlexibleIsosurfaceController.cpp
//	------------------------
//	
//	The controller object for the flexible isosurface
//	interface
//	
///////////////////////////////////////////////////

#include "FlexibleIsosurfaceController.h"
#include <stdio.h>

// constructor
FlexibleIsosurfaceController::FlexibleIsosurfaceController(
								FlexibleIsosurface *newFlexIso, 
								FlexibleIsosurfaceWindow *newFlexIsoWindow)
	{ // FlexibleIsosurfaceController::FlexibleIsosurfaceController()
	// save pointers to the objects
	flexIso = newFlexIso;
	flexIsoWindow = newFlexIsoWindow;
	nodeID = -1;
	
	// connect up signals to slots

	// signals for arcballs
	QObject::connect(	flexIsoWindow->rotator,		 				SIGNAL(rotationChanged()),
						this,										SLOT(objectRotationChanged()));
	QObject::connect(	flexIsoWindow->lightRotator, 				SIGNAL(rotationChanged()),
						this,										SLOT(lightRotationChanged()));

	// signals for main pane to control arcball
	QObject::connect(	flexIsoWindow->flexibleIsosurfaceWidget,	SIGNAL(BeginDrag(float, float)),
						flexIsoWindow->rotator,						SLOT(BeginDrag(float, float)));
	QObject::connect(	flexIsoWindow->flexibleIsosurfaceWidget,	SIGNAL(ContinueDrag(float, float)),
						flexIsoWindow->rotator,						SLOT(ContinueDrag(float, float)));
	QObject::connect(	flexIsoWindow->flexibleIsosurfaceWidget,	SIGNAL(EndDrag()),
						flexIsoWindow->rotator,						SLOT(EndDrag()));

	// signal for zoom slider
	QObject::connect(	flexIsoWindow->zoomSlider,					SIGNAL(valueChanged(int)),
						this,										SLOT(zoomChanged(int)));
	
	// signal for stack scale slider
	QObject::connect(	flexIsoWindow->stackScaleSlider,			SIGNAL(valueChanged(int)),
						this,										SLOT(stackScaleChanged(int)));

	// signal for isovalue slider
	QObject::connect(	flexIsoWindow->isovalueSlider,				SIGNAL(valueChanged(int)),
						this,										SLOT(isovalueChanged(int)));

	// signal for check box to use coloured contours
	QObject::connect(	flexIsoWindow->colouredContourBox,			SIGNAL(stateChanged(int)),
						this,										SLOT(contourColourCheckChanged(int)));

	// signal for check box to use local contours
	QObject::connect(	flexIsoWindow->localContourBox,				SIGNAL(stateChanged(int)),
						this,										SLOT(localContoursCheckChanged(int)));

	// signal for check box to show contour tree
	QObject::connect(	flexIsoWindow->showTreeBox,					SIGNAL(stateChanged(int)),
						this,										SLOT(showTreeCheckChanged(int)));

	// signal for check box to use local contours
	QObject::connect(	flexIsoWindow->colourTreeBox,				SIGNAL(stateChanged(int)),
						this,										SLOT(colourTreeCheckChanged(int)));

	// signal for leaf size slider
	QObject::connect(	flexIsoWindow->leafSizeSlider,				SIGNAL(valueChanged(int)),
						this,										SLOT(priorityBoundChanged(int)));

	// signal for tree size slider
	QObject::connect(	flexIsoWindow->treeSizeSlider,				SIGNAL(valueChanged(int)),
						this,										SLOT(treeSizeChanged(int)));

	// signals for buttons
	QObject::connect(	flexIsoWindow->deleteSelectionButton,		SIGNAL(clicked()),
						this,										SLOT(deleteSelectionPushed()));
	QObject::connect(	flexIsoWindow->deleteComplementButton,		SIGNAL(clicked()),
						this,										SLOT(deleteComplementPushed()));
	QObject::connect(	flexIsoWindow->restoreDeletedButton,		SIGNAL(clicked()),
						this,										SLOT(restoreDeletedPushed()));

	QObject::connect(	flexIsoWindow->dotLayoutButton,				SIGNAL(clicked()),
						this,										SLOT(dotLayoutPushed()));
	QObject::connect(	flexIsoWindow->saveLayoutButton,			SIGNAL(clicked()),
						this,										SLOT(saveLayoutPushed()));
	QObject::connect(	flexIsoWindow->loadLayoutButton,			SIGNAL(clicked()),
						this,										SLOT(loadLayoutPushed()));

	QObject::connect(	flexIsoWindow->valuePlusButton,				SIGNAL(clicked()),
						this,										SLOT(valueIncrementPushed()));
	QObject::connect(	flexIsoWindow->valueMinusButton,			SIGNAL(clicked()),
						this,										SLOT(valueDecrementPushed()));

	QObject::connect(	flexIsoWindow->collapseButton,				SIGNAL(clicked()),
						this,										SLOT(collapsePushed()));
	QObject::connect(	flexIsoWindow->uncollapseButton,			SIGNAL(clicked()),
						this,										SLOT(uncollapsePushed()));

	// signal for combo box to choose geometric measure
	QObject::connect(	flexIsoWindow->measureChooser,				SIGNAL(activated(int)),
						this,										SLOT(measureChanged(int)));

	// signal for selection clicks from main widget
	QObject::connect(	flexIsoWindow->flexibleIsosurfaceWidget,	SIGNAL(Select(QMouseEvent *)),
						this,										SLOT(selectComponent(QMouseEvent *)));

	// signals for dragging in contour tree pane
	QObject::connect(	flexIsoWindow->contourTreeWidget,			SIGNAL(BeginNodeDrag(float, float)),
						this,										SLOT(BeginNodeDrag(float, float)));

	QObject::connect(	flexIsoWindow->contourTreeWidget,			SIGNAL(ContinueNodeDrag(float, float)),
						this,										SLOT(ContinueNodeDrag(float, float)));

	QObject::connect(	flexIsoWindow->contourTreeWidget,			SIGNAL(BeginTagDrag(float, float)),
						this,										SLOT(BeginTagDrag(float, float)));

	QObject::connect(	flexIsoWindow->contourTreeWidget,			SIGNAL(ContinueTagDrag(float, float)),
						this,										SLOT(ContinueTagDrag(float, float)));

	QObject::connect(	flexIsoWindow->contourTreeWidget,			SIGNAL(EndDrag()),
						this,										SLOT(EndDrag()));

	// signals for colour choices
	QObject::connect(	flexIsoWindow->colourSelector,				SIGNAL(chooseColour(int)),
						this,										SLOT(ChooseColour(int)));

	} // FlexibleIsosurfaceController::FlexibleIsosurfaceController()

// slot for responding to arcball rotation for object
void FlexibleIsosurfaceController::objectRotationChanged()
	{ // FlexibleIsosurfaceController::objectRotationChanged()
	// copy the rotation matrix from the widget to the model
	flexIsoWindow->rotator->copyRotation(flexIso->rotMat);
	
	// force an update for the flexible isosurface pane (only)
	flexIsoWindow->flexibleIsosurfaceWidget->update();
	} // FlexibleIsosurfaceController::objectRotationChanged()

// slot for responding to arcball rotation for object
void FlexibleIsosurfaceController::lightRotationChanged()
	{ // FlexibleIsosurfaceController::lightRotationChanged()
	// copy the rotation matrix from the widget to the model
	flexIsoWindow->lightRotator->copyRotation(flexIso->lightMat);
	
	// force an update for the flexible isosurface pane (only)
	flexIsoWindow->flexibleIsosurfaceWidget->update();
	} // FlexibleIsosurfaceController::lightRotationChanged()

// slot for responding to zoom sliders
void FlexibleIsosurfaceController::zoomChanged(int value)
	{ // FlexibleIsosurfaceController::zoomChanged()
	// reset the model's scale
	flexIso->scale = (float) value / 5000.0;
	
	// force an update for the flexible isosurface pane (only)
	flexIsoWindow->flexibleIsosurfaceWidget->update();
	} // FlexibleIsosurfaceController::zoomChanged()

// slot for responding to stack scale slider
void FlexibleIsosurfaceController::stackScaleChanged(int value)
	{ // FlexibleIsosurfaceController::stackScaleChanged()
	// reset the model's scale
	flexIso->field->stackScale = (float) value / 1000.0;
	
	// retrieve a pointer to the widget
	FlexibleIsosurfaceWidget *fw = flexIsoWindow->flexibleIsosurfaceWidget;
	// force a resize to reset the projection matrix
	fw->resize(fw->width(), fw->height());
	// update the flexible isosurface pane
	fw->update();
	} // FlexibleIsosurfaceController::stackScaleChanged()

// slot for responding to isovalue changes
void FlexibleIsosurfaceController::isovalueChanged(int value)
	{ // FlexibleIsosurfaceController::isovalueChanged()
	// never allow an actual value of 0 - it's crashy (as before)
	if (value <= 0) value = 1;
	if (value >= 9999) value = 9999;
	if (flexIso->field->selectionRoot == noContourSelected)
		{ // no selection: reset the entire set
		if (flexIso->useLocalContours)
			flexIso->ResetLocalContours(value / 10000.0);
		else
			flexIso->ResetIsosurface(value / 10000.0);
		} // no selection: reset the entire set
	else
		{ // there's a selection
		if (flexIso->useLocalContours)
			flexIso->field->ProportionalUpdateSelection(value / 10000.0);
		else
			flexIso->field->UpdateSelection(flexIso->field->MinHeight() + value / 10000.0 * 
								  (flexIso->field->MaxHeight() - flexIso->field->MinHeight()));	
		} // there's a selection
	// force an update for the flexible isosurface pane (only)
	flexIsoWindow->flexibleIsosurfaceWidget->update();
	flexIsoWindow->contourTreeWidget->update();
	flexIsoWindow->colourSelector->update();
	} // FlexibleIsosurfaceController::isovalueChanged()

// slots for responding to check boxes
void FlexibleIsosurfaceController::contourColourCheckChanged(int state)
	{ // FlexibleIsosurfaceController::contourColourCheckChanged()
	// reset the model's flag
	flexIso->field->differentColouredContours = (state == Qt::Checked); 

	// force an update for the flexible isosurface pane (only)
	flexIsoWindow->flexibleIsosurfaceWidget->update();
	flexIsoWindow->contourTreeWidget->update();
	flexIsoWindow->colourSelector->update();
	} // FlexibleIsosurfaceController::contourColourCheckChanged()
	
void FlexibleIsosurfaceController::localContoursCheckChanged(int state)
	{ // FlexibleIsosurfaceController::localContoursCheckChanged()
	// reset the model's flag
	flexIso->useLocalContours = (state == Qt::Checked); 

	// depending on the flag, reset the flexible isosurface
	if (flexIso->useLocalContours)
		flexIso->ResetLocalContours(flexIso->isoValue);
	else
		flexIso->ResetIsosurface(flexIso->isoValue);

	// force an update for the relevant panes
	flexIsoWindow->flexibleIsosurfaceWidget->update();
	flexIsoWindow->contourTreeWidget->update();
	flexIsoWindow->colourSelector->update();
	} // FlexibleIsosurfaceController::localContoursCheckChanged()

void FlexibleIsosurfaceController::showTreeCheckChanged(int state)
	{ // FlexibleIsosurfaceController::showTreeCheckChanged()
	// reset the model's flag
	flexIso->showTree = (state == Qt::Checked); 

	// force an update for the flexible isosurface pane (only)
	flexIsoWindow->contourTreeWidget->update();
	} // FlexibleIsosurfaceController::showTreeCheckChanged()
	
void FlexibleIsosurfaceController::colourTreeCheckChanged(int state)
	{ // FlexibleIsosurfaceController::colourTreeCheckChanged()
	// reset the model's flag
	flexIso->colouredTree = (state == Qt::Checked);

	flexIsoWindow->contourTreeWidget->update();
	} // FlexibleIsosurfaceController::colourTreeCheckChanged()
	
// slots for responding to simplification
void FlexibleIsosurfaceController::priorityBoundChanged(int value)
	{ // FlexibleIsosurfaceController::priorityBoundChanged()
	// have the field update its records
	flexIso->field->logPriorityBound = (float) value / 10000.0;
	flexIso->field->UpdateFromLogPriorityBound();

	// force an update for the relevant panes
	flexIsoWindow->flexibleIsosurfaceWidget->update();
	flexIsoWindow->contourTreeWidget->update();
	flexIsoWindow->logCollapseWidget->update();
	flexIsoWindow->colourSelector->update();
	flexIsoWindow->treeSizeSlider->setValue(flexIso->field->logTreeSize * 10000.0);
	} // FlexibleIsosurfaceController::priorityBoundChanged()

void FlexibleIsosurfaceController::treeSizeChanged(int value)
	{ // FlexibleIsosurfaceController::treeSizeChanged()
	// have the field update its records
	flexIso->field->logTreeSize = ((float) value) / 10000.0;
	flexIso->field->UpdateFromLogTreeSize();

	// force an update for the relevant panes
	flexIsoWindow->flexibleIsosurfaceWidget->update();
	flexIsoWindow->contourTreeWidget->update();
	flexIsoWindow->logCollapseWidget->update();
	flexIsoWindow->colourSelector->update();
	flexIsoWindow->leafSizeSlider->setValue(flexIso->field->logPriorityBound * 10000.0);
	} // FlexibleIsosurfaceController::treeSizeChanged()
	
// slots for responding to buttons
void FlexibleIsosurfaceController::deleteSelectionPushed()
	{ // FlexibleIsosurfaceController::deleteSelectionPushed()
	flexIso->field->DeleteSelection();

	// force an update for the relevant panes
	flexIsoWindow->flexibleIsosurfaceWidget->update();
	flexIsoWindow->contourTreeWidget->update();
	flexIsoWindow->colourSelector->update();
	} // FlexibleIsosurfaceController::deleteSelectionPushed()
	
void FlexibleIsosurfaceController::deleteComplementPushed()
	{ // FlexibleIsosurfaceController::deleteComplementPushed()
	flexIso->field->DeleteComplement();

	// force an update for the relevant panes
	flexIsoWindow->flexibleIsosurfaceWidget->update();
	flexIsoWindow->contourTreeWidget->update();
	flexIsoWindow->colourSelector->update();
	} // FlexibleIsosurfaceController::deleteComplementPushed()
	
void FlexibleIsosurfaceController::restoreDeletedPushed()
	{ // FlexibleIsosurfaceController::restoreDeletedPushed()
	flexIso->field->RestoreDeleted();

	// force an update for the relevant panes
	flexIsoWindow->flexibleIsosurfaceWidget->update();
	flexIsoWindow->contourTreeWidget->update();
	flexIsoWindow->colourSelector->update();
	} // FlexibleIsosurfaceController::restoreDeletedPushed()
	
void FlexibleIsosurfaceController::dotLayoutPushed()
	{ // FlexibleIsosurfaceController::dotLayoutPushed()
	flexIso->field->DoDotLayout();

	// force an update for the relevant panes
	flexIsoWindow->flexibleIsosurfaceWidget->update();
	flexIsoWindow->contourTreeWidget->update();
	} // FlexibleIsosurfaceController::dotLayoutPushed()
	
void FlexibleIsosurfaceController::saveLayoutPushed()
	{ // FlexibleIsosurfaceController::saveLayoutPushed()
	flexIso->field->SaveTreeLayout();

	// force an update for the relevant panes
	flexIsoWindow->flexibleIsosurfaceWidget->update();
	flexIsoWindow->contourTreeWidget->update();
	} // FlexibleIsosurfaceController::saveLayoutPushed()
	
void FlexibleIsosurfaceController::loadLayoutPushed()
	{ // FlexibleIsosurfaceController::loadLayoutPushed()
	flexIso->field->LoadTreeLayout();

	// force an update for the relevant panes
	flexIsoWindow->flexibleIsosurfaceWidget->update();
	flexIsoWindow->contourTreeWidget->update();
	} // FlexibleIsosurfaceController::loadLayoutPushed()
	
void FlexibleIsosurfaceController::valueIncrementPushed()
	{ // FlexibleIsosurfaceController::valueIncrementPushed()
	float newIsoValue = flexIso->isoValue + 1.0 / (flexIso->field->MaxHeight() - flexIso->field->MinHeight());
	if (newIsoValue > 1.0) newIsoValue = 1.0;
	isovalueChanged(newIsoValue * 10000.0);
	} // FlexibleIsosurfaceController::valueIncrementPushed()
	
void FlexibleIsosurfaceController::valueDecrementPushed()
	{ // FlexibleIsosurfaceController::valueDecrementPushed()
	float newIsoValue = flexIso->isoValue - 1.0 / (flexIso->field->MaxHeight() - flexIso->field->MinHeight());
	if (newIsoValue < 0.0) newIsoValue = 0.0;
	isovalueChanged(newIsoValue * 10000.0);
	} // FlexibleIsosurfaceController::valueDecrementPushed()
	
void FlexibleIsosurfaceController::collapsePushed()
	{ // FlexibleIsosurfaceController::collapsePushed()
	flexIso->field->SingleCollapse();

	// force an update for the relevant panes
	flexIsoWindow->flexibleIsosurfaceWidget->update();
	flexIsoWindow->contourTreeWidget->update();
	flexIsoWindow->colourSelector->update();
	flexIsoWindow->logCollapseWidget->update();
	flexIsoWindow->leafSizeSlider->setValue(flexIso->field->logPriorityBound * 10000.0);
	flexIsoWindow->treeSizeSlider->setValue(flexIso->field->logTreeSize * 10000.0);
	} // FlexibleIsosurfaceController::collapsePushed()
	
void FlexibleIsosurfaceController::uncollapsePushed()
	{ // FlexibleIsosurfaceController::uncollapsePushed()
	flexIso->field->SingleUnCollapse();

	// force an update for the relevant panes
	flexIsoWindow->flexibleIsosurfaceWidget->update();
	flexIsoWindow->contourTreeWidget->update();
	flexIsoWindow->colourSelector->update();
	flexIsoWindow->logCollapseWidget->update();
	flexIsoWindow->leafSizeSlider->setValue(flexIso->field->logPriorityBound * 10000.0);
	flexIsoWindow->treeSizeSlider->setValue(flexIso->field->logTreeSize * 10000.0);
	} // FlexibleIsosurfaceController::uncollapsePushed()

// slot for responding to comboBox
void FlexibleIsosurfaceController::measureChanged(int whichMeasure)
	{ // FlexibleIsosurfaceController::measureChanged()
	flexIso->field->ResetCollapsePriority(whichMeasure);

	// force an update for the relevant panes
	flexIsoWindow->flexibleIsosurfaceWidget->update();
	flexIsoWindow->contourTreeWidget->update();
	flexIsoWindow->colourSelector->update();
	flexIsoWindow->logCollapseWidget->update();
	flexIsoWindow->leafSizeSlider->setValue(flexIso->field->logPriorityBound * 10000.0);
	flexIsoWindow->treeSizeSlider->setValue(flexIso->field->logTreeSize * 10000.0);
	} // FlexibleIsosurfaceController::measureChanged()
	
// slots for responding to selection events in main widget
void FlexibleIsosurfaceController::selectComponent(QMouseEvent *event)
	{ // FlexibleIsosurfaceController::selectComponent()
	//	if there is already a selection
	if (flexIso->field->selectionRoot != noContourSelected)
		//	commit the selection
		flexIso->field->CommitSelection();

	// reset the selection colour
	flexIso->selectionColour = -1;

	// pick the component & store the ID
	flexIso->field->selectionRoot = flexIsoWindow->flexibleIsosurfaceWidget->PickComponent(event->x(), event->y());

	//	if it was successful
	if (flexIso->field->selectionRoot != noContourSelected)
		{ // component selected
		//	select the component we clicked on
		flexIso->field->SelectComponent(flexIso->field->selectionRoot);
		//	reset the slider's isovalue
		flexIso->isoValue =   (flexIso->field->currentSelectionValue - flexIso->field->MinHeight()) 
							/ (flexIso->field->MaxHeight() - flexIso->field->MinHeight());
		flexIso->selectionColour = flexIso->field->GetSuperarcColour(flexIso->field->selectionRoot);
		} // component selected

	// force an update for the relevant panes
	flexIsoWindow->flexibleIsosurfaceWidget->update();
	flexIsoWindow->contourTreeWidget->update();
	flexIsoWindow->colourSelector->update();
	flexIsoWindow->ResetControlElements();
	} // FlexibleIsosurfaceController::selectComponent()
	
// slots for responding to mouse control in contour tree pane
void FlexibleIsosurfaceController::BeginNodeDrag(float x, float y)
	{ // FlexibleIsosurfaceController::BeginNodeDrag()
	nodeID = flexIso->field->PickNode(x, y);
	// force an update for the relevant panes
	flexIsoWindow->flexibleIsosurfaceWidget->update();
	flexIsoWindow->contourTreeWidget->update();
	flexIsoWindow->colourSelector->update();
	flexIsoWindow->ResetControlElements();
	} // FlexibleIsosurfaceController::BeginNodeDrag()

void FlexibleIsosurfaceController::ContinueNodeDrag(float x, float y)
	{ // FlexibleIsosurfaceController::ContinueNodeDrag()
	// skip if no id set
	if (nodeID == -1)
		return;

	// update the x-coordinate
	flexIso->field->UpdateNodeXPosn(nodeID, x);

	// force an update for the relevant panes
	flexIsoWindow->flexibleIsosurfaceWidget->update();
	flexIsoWindow->contourTreeWidget->update();
	flexIsoWindow->colourSelector->update();
	flexIsoWindow->ResetControlElements();
	} // FlexibleIsosurfaceController::ContinueNodeDrag()

void FlexibleIsosurfaceController::BeginTagDrag(float x, float y)
	{ // FlexibleIsosurfaceController::BeginTagDrag()
	// if there's a contour already selected, drop it
	if (flexIso->field->selectionRoot != noContourSelected)
		flexIso->field->CommitSelection();
	// disable the selection colour
	flexIso->selectionColour = -1;

	// retrieve the ID of the arc & check if we got one
	long arcID = flexIso->field->PickArc(x, y);
	if (arcID == -1)
		return;

	// select the component
	flexIso->field->SelectComponent(arcID);
	flexIso->selectionColour = flexIso->field->GetSuperarcColour(arcID);

	// force an update for the relevant panes
	flexIsoWindow->flexibleIsosurfaceWidget->update();
	flexIsoWindow->contourTreeWidget->update();
	flexIsoWindow->colourSelector->update();
	flexIsoWindow->ResetControlElements();
	} // FlexibleIsosurfaceController::BeginTagDrag()

void FlexibleIsosurfaceController::ContinueTagDrag(float x, float y)
	{ // FlexibleIsosurfaceController::ContinueTagDrag()
	// if we've got no selection, return
	if (flexIso->field->selectionRoot == noContourSelected)
		return;
	float isovalue = y * 10000.0;
	// update the isovalue
	isovalueChanged(isovalue);

	// force an update for the relevant panes
	flexIsoWindow->flexibleIsosurfaceWidget->update();
	flexIsoWindow->contourTreeWidget->update();
	flexIsoWindow->colourSelector->update();
	flexIsoWindow->ResetControlElements();
	} // FlexibleIsosurfaceController::ContinueTagDrag()

void FlexibleIsosurfaceController::EndDrag()
	{ // FlexibleIsosurfaceController::EndDrag()
	// release any tag we're dragging
	if (nodeID != -1) 
		nodeID = -1;

	// force an update for the relevant panes
	flexIsoWindow->flexibleIsosurfaceWidget->update();
	flexIsoWindow->contourTreeWidget->update();
	flexIsoWindow->colourSelector->update();
	flexIsoWindow->ResetControlElements();
	} // FlexibleIsosurfaceController::EndDrag()

// slot for responding to colour selection pane
void FlexibleIsosurfaceController::ChooseColour(int whichColour)
	{ // FlexibleIsosurfaceController::ChooseColour()
	if (flexIso->field->selectionRoot == noContourSelected)
		return;
	// reset the colour
	flexIso->field->SetSuperarcColour(flexIso->field->selectionRoot, whichColour);
	flexIso->selectionColour = whichColour;
	
	// force an update for the relevant panes
	flexIsoWindow->flexibleIsosurfaceWidget->update();
	flexIsoWindow->contourTreeWidget->update();
	flexIsoWindow->colourSelector->update();
	flexIsoWindow->ResetControlElements();
	} // FlexibleIsosurfaceController::ChooseColour()
