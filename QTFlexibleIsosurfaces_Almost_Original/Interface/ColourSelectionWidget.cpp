///////////////////////////////////////////////////
//
//	Contour-tree based isosurfaces
//	Version 4.0 (QT-based)
//	copyright Hamish Carr
//	January, 2014
//
//	------------------------
//	ColourSelectionWidget.cpp
//	------------------------
//	
//	A widget displaying colour choices
//	
///////////////////////////////////////////////////

#include "ColourSelectionWidget.h"

#define COLOUR_SIZE 30

ColourSelectionWidget::ColourSelectionWidget(FlexibleIsosurface *newFlexIso, QWidget *parent)
	: QGLWidget(parent)
	{ // ColourSelectionWidget()
	// store pointer to the model
	flexIso = newFlexIso;
	// let QT know we are fixed size
	setMinimumHeight(COLOUR_SIZE);
	setMaximumHeight(COLOUR_SIZE);
	setMinimumWidth(COLOUR_SIZE * N_SURFACE_COLOURS);
	setMaximumWidth(COLOUR_SIZE * N_SURFACE_COLOURS);
	} // ColourSelectionWidget()
	
ColourSelectionWidget::~ColourSelectionWidget()
	{ // ~ColourSelectionWidget()
	// nothing yet
	} // ~ColourSelectionWidget()
	
// called when OpenGL context is set up
void ColourSelectionWidget::initializeGL()
	{ // ColourSelectionWidget::initializeGL()
	// this is straightforward 2D rendering, so disable stuff we don't need
	glDisable(GL_LIGHTING);
	glDisable(GL_DEPTH_TEST);
	// background is white
	glClearColor(1.0, 1.0, 1.0, 1.0);
	} // ContourTreeWidget::initializeGL()
	
// called every time the widget is resized
void ColourSelectionWidget::resizeGL(int w, int h)
	{ // ColourSelectionWidget::resizeGL()
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	// set it to be wide enough for multiple colour boxes
	glOrtho(0.0, 5.0 * N_SURFACE_COLOURS, 0.0, 5.0, -1.0, 1.0);
	} // ColourSelectionWidget::resizeGL()
	
// called every time the widget needs painting
void ColourSelectionWidget::paintGL()
	{ // ColourSelectionWidget::paintGL()
	// set model view matrix based on stored translation, rotation &c.
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	// clear the buffer
	glClear(GL_COLOR_BUFFER_BIT);
	glColor3f(0.0, 0.0, 0.0);
	
	for (int colour = 0; colour < N_SURFACE_COLOURS; colour++)
		{ // loop through all the colours
		if (colour == flexIso->selectionColour)
			{ // it's selected
			glColor3f(0.0, 0.0, 0.0);
			glBegin(GL_QUADS);
			glVertex2f(colour * 5, 0);
			glVertex2f(colour * 5 + 5, 0);
			glVertex2f(colour * 5 + 5, 5);
			glVertex2f(colour * 5, 5);
			glEnd();
			} // it's selected
		
		// pull the colour from the array 
		glColor3fv(surface_colour[colour]);
		
		// draw a quad
		glBegin(GL_QUADS);
		glVertex2f(colour * 5 + 1, 1);
		glVertex2f(colour * 5 + 4, 1);
		glVertex2f(colour * 5 + 4, 4);
		glVertex2f(colour * 5 + 1, 4);
		glEnd();

		} // loop through all the colours
	} // ColourSelectionWidget::paintGL()
												
void ColourSelectionWidget::mousePressEvent(QMouseEvent *event)
	{ // ColourSelectionWidget::mousePressEvent()
	int colour = event->x() / COLOUR_SIZE; 
	if (colour < 0) 
		colour = 0; 
	else if (colour > N_SURFACE_COLOURS) 
		colour = N_SURFACE_COLOURS;
	emit chooseColour(colour);
	} // ColourSelectionWidget::mousePressEvent()
