///////////////////////////////////////////////////
//
//	Contour-tree based isosurfaces
//	Version 4.0 (QT-based)
//	copyright Hamish Carr
//	January, 2014
//
//	------------------------
//	ArcBallWidget.h
//	------------------------
//	
//	An arcball widget that modifies a stored quaternion
//	using Ken Shoemake's arcball code
//	
///////////////////////////////////////////////////

#ifndef _HEADER_ARCBALL_WIDGET_H
#define _HEADER_ARCBALL_WIDGET_H

#include <QGLWidget>
#include <QMouseEvent>
#include "Ball.h"

class ArcBallWidget : public QGLWidget
	{ // class ArcBallWidget
	Q_OBJECT
	public:
	BallData theBall;
	
	//	constructor
	ArcBallWidget(QWidget *parent);

	// routine to copy rotation matrix
	void copyRotation(GLfloat *target);

	protected:
	// called when OpenGL context is set up
	void initializeGL();
	// called every time the widget is resized
	void resizeGL(int w, int h);
	// called every time the widget needs painting
	void paintGL();
	
	// mouse-handling
	virtual void mousePressEvent(QMouseEvent *event);
	virtual void mouseMoveEvent(QMouseEvent *event);
	virtual void mouseReleaseEvent(QMouseEvent *event);
	
	public slots:
	// routines called to allow synchronised rotation with other widget
	// coordinates are assumed to be in range of [-1..1] in x,y
	// if outside that range, will be clamped
	void BeginDrag(float x, float y);
	void ContinueDrag(float x, float y);
	void EndDrag();
	
	signals:
	// slot for calling controller when rotation changes
	void rotationChanged();
	}; // class ArcBallWidget

#endif
