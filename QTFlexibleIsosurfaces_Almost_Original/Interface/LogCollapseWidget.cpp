///////////////////////////////////////////////////
//
//	Contour-tree based isosurfaces
//	Version 4.0 (QT-based)
//	copyright Hamish Carr
//	January, 2014
//
//	------------------------
//	LogCollapseWidget.cpp
//	------------------------
//	
//	A widget displaying the tree size / collapse
//	bound as a log-log plot
//	
///////////////////////////////////////////////////

#include "LogCollapseWidget.h"

LogCollapseWidget::LogCollapseWidget(FlexibleIsosurface *newFlexIso, QWidget *parent)
	: QGLWidget(parent)
	{ // LogCollapseWidget()
	// store pointer to the model
	flexIso = newFlexIso;
	} // LogCollapseWidget()
	
LogCollapseWidget::~LogCollapseWidget()
	{ // ~LogCollapseWidget()
	// nothing yet
	} // ~LogCollapseWidget()
	
// called when OpenGL context is set up
void LogCollapseWidget::initializeGL()
	{ // LogCollapseWidget::initializeGL()
	// this is straightforward 2D rendering, so disable stuff we don't need
	glDisable(GL_LIGHTING);
	glDisable(GL_DEPTH_TEST);
	// background is white
	glClearColor(1.0, 1.0, 1.0, 1.0);
	} // ContourTreeWidget::initializeGL()
	
// called every time the widget is resized
void LogCollapseWidget::resizeGL(int w, int h)
	{ // LogCollapseWidget::resizeGL()
	// reset the viewport
	glViewport(0, 0, w, h);
	
	// set projection matrix to have range of 0.0-1.0 in x, y
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(0.0, 1.0, 0.0, 1.0, -1.0, 1.0);
	} // LogCollapseWidget::resizeGL()
	
// called every time the widget needs painting
void LogCollapseWidget::paintGL()
	{ // LogCollapseWidget::paintGL()
	// set model view matrix based on stored translation, rotation &c.
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	// clear the buffer
	glClear(GL_COLOR_BUFFER_BIT);
	
	// draws the current flexible isosurface contours
	glColor3f(0.0, 0.0, 0.0);
	flexIso->field->PlotLogCollapse();
	} // LogCollapseWidget::paintGL()
