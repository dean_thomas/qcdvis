///////////////////////////////////////////////////
//
//	Contour-tree based isosurfaces
//	Version 4.0 (QT-based)
//	copyright Hamish Carr
//	January, 2014
//
//	------------------------
//	FlexibleIsosurfaceWidget.h
//	------------------------
//	
//	The main widget that shows the flexible isosurface
//	itself
//	
///////////////////////////////////////////////////

#include <math.h>
#ifdef __APPLE__
#include <OpenGL/glu.h>
#else
#include <GL/glu.h>
#endif

#include "FlexibleIsosurfaceWidget.h"
static GLfloat light_position[] = {0.4, 1.0, -0.5, 0.0};							

// constructor
FlexibleIsosurfaceWidget::FlexibleIsosurfaceWidget(FlexibleIsosurface *newFlexIso, QWidget *parent)
	: QGLWidget(parent)
	{ // constructor
	// store pointer to the model
	flexIso = newFlexIso;
	whichButton = Qt::NoButton;
	QGLFormat pBufferFormat = format();
	pBufferFormat.setSampleBuffers(false);
	pBuffer = new QGLPixelBuffer(QSize(1,1), pBufferFormat, this);
	} // constructor

// destructor
FlexibleIsosurfaceWidget::~FlexibleIsosurfaceWidget()
	{ // destructor
	// nothing yet
	} // destructor																	

// called when OpenGL context is set up
void FlexibleIsosurfaceWidget::initializeGL()
	{ // FlexibleIsosurfaceWidget::initializeGL()
	// enable Z-buffering
	glEnable(GL_DEPTH_TEST);
	// set lighting parameters
	glShadeModel(GL_SMOOTH);
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	
	// some surfaces will meet the boundary, so we need this
	glLightModelf(GL_LIGHT_MODEL_TWO_SIDE, 1.0);

	// this allows no-headache normals
	glEnable(GL_NORMALIZE);

	// background is white
	glClearColor(1.0, 1.0, 1.0, 1.0);

	// now switch to the pixel buffer & initialise it
	pBuffer->makeCurrent();
	// enable Z-buffering
	glEnable(GL_DEPTH_TEST);
	// set lighting parameters
	glDisable(GL_LIGHTING);
	// background is white
	glClearColor(1.0, 1.0, 1.0, 1.0);
	
	// switch back from the pBuffer
	makeCurrent();
	} // FlexibleIsosurfaceWidget::initializeGL()

// called every time the widget is resized
void FlexibleIsosurfaceWidget::resizeGL(int w, int h)
	{ // FlexibleIsosurfaceWidget::resizeGL()
	// reset the viewport
	glViewport(0, 0, w, h);
	
	// set projection matrix to be glOrtho based on zoom & window size
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	
	// compute depth of field
	float fieldDepth = (w > h) ? w : h;

	// render to pixel scale - means scale everything in the render code
	glOrtho(0, w, 0, h, -fieldDepth * 0.5, fieldDepth * 0.5);
	} // FlexibleIsosurfaceWidget::resizeGL()
	
// called every time the widget needs painting
void FlexibleIsosurfaceWidget::paintGL()
	{ // FlexibleIsosurfaceWidget::paintGL()
	// clear the buffer
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// compute the size of the block
	float xSize = flexIso->field->stackScale * flexIso->field->XDim()-1;
	float ySize = flexIso->field->YDim()-1;
	float zSize = flexIso->field->ZDim()-1;
	float diagonal = sqrt(xSize*xSize + ySize * ySize + zSize * zSize);

	// set model view matrix based on stored translation, rotation &c.
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	// make sure lighting is on
	glEnable(GL_LIGHTING);

	// set light position first
	glPushMatrix();
	glMultMatrixf(flexIso->lightMat);
	glLightfv(GL_LIGHT0, GL_POSITION, light_position);
	glPopMatrix();
		
	// retrieve width and height
	int w = width(), h = height();

	// Translate by half the size of the window
	glTranslatef(w / 2, h / 2, 0.0);
	
	// Scale so that -1,1 maps to [0,w] or [0,h]
	if (w > h)
		glScalef(-h, -h, -h);
	else
		glScalef(-w, -w, -w);

	// scale to make it fit (0,1) in each direction
	glScalef(1.0/diagonal, 1.0/diagonal, 1.0/diagonal);

	// now apply the zoom scale
	float powScale = pow(10.0, flexIso->scale);
	glScalef(powScale, powScale, powScale);

	// apply rotation matrix from arcball
	glMultMatrixf(flexIso->rotMat);

	// translate so that centre of data block starts at (0,0,0) 
	glTranslatef((float) 	-0.5*flexIso->field->stackScale * (flexIso->field->XDim()-1), 
				 (float) 	-0.5*(flexIso->field->YDim()-1), 
				 (float)	-0.5*(flexIso->field->ZDim()-1));

	// draws the current flexible isosurface contours
	flexIso->field->FlexibleContours(false, false);
	} // FlexibleIsosurfaceWidget::paintGL()

// routine to pick a component in the back buffer
long FlexibleIsosurfaceWidget::PickComponent(int x, int y)
	{ // FlexibleIsosurfaceWidget::PickComponent()
	// switch to rendering in the pixel buffer
	pBuffer->makeCurrent();
	
	// reset the viewport
	glViewport(0, 0, 1, 1);
	
	// set projection matrix to be glOrtho based on zoom & window size
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	
	// retrieve width and height
	int w = width(), h = height();
	
	// compute depth of field
	float fieldDepth = (w > h) ? w : h;

	// render to pixel scale - means scale everything in the render code
	glOrtho(x, x + 1, height() - y,  height() - y + 1, -fieldDepth * 0.5, fieldDepth * 0.5);
	
	// clear the buffer
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// set model view matrix based on stored translation, rotation &c.
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	// make sure lighting is OFF
	glDisable(GL_LIGHTING);

	// Translate by half the size of the window
	glTranslatef(w / 2, h / 2, 0.0);
	
	// Scale so that -1,1 maps to [0,w] or [0,h]
	if (w > h)
		glScalef(-h, -h, -h);
	else
		glScalef(-w, -w, -w);

	// compute the size of the block
	float xSize = flexIso->field->stackScale * flexIso->field->XDim()-1;
	float ySize = flexIso->field->YDim()-1;
	float zSize = flexIso->field->ZDim()-1;
	float diagonal = sqrt(xSize*xSize + ySize * ySize + zSize * zSize);

	// scale to make it fit (0,1) in each direction
	glScalef(1.0/diagonal, 1.0/diagonal, 1.0/diagonal);

	// now apply the zoom scale
	float powScale = pow(10.0, flexIso->scale);
	glScalef(powScale, powScale, powScale);

	// apply rotation matrix from arcball
	glMultMatrixf(flexIso->rotMat);

	// translate so that centre of data block starts at (0,0,0) 
	glTranslatef((float) 	-0.5*flexIso->field->stackScale * (flexIso->field->XDim()-1), 
				 (float) 	-0.5*(flexIso->field->YDim()-1), 
				 (float)	-0.5*(flexIso->field->ZDim()-1));

	// draws the current flexible isosurface contours
	flexIso->field->FlexibleContours(false, true);

	// flush to force commit
	glFlush();

	// now read pixel buffer back to image
	long componentID = pBuffer->toImage().pixel(0, 0);

	// return render context to widget
	makeCurrent();

    //  i.e. we didn't find any
    if (componentID == 0xFFFFFFFF) 
		componentID = noContourSelected;
    else
        componentID &= 0xFFFFFF;
	
	// for now, return nothing
	return componentID;
	} // FlexibleIsosurfaceWidget::PickComponent()
	
// mouse-handling
void FlexibleIsosurfaceWidget::mousePressEvent(QMouseEvent *event)
	{ // FlexibleIsosurfaceWidget::mousePressEvent()
	// store the button for future reference
	whichButton = event->button();
	if (whichButton == Qt::RightButton)
		emit Select(event);
	else
		{ // not right - assume left
		// find the minimum of height & width	
		float size = (width() > height()) ? height() : width();
		// scale both coordinates from that
		float x = (2.0 * event->x() - size) / size;
		float y = (size - 2.0 * event->y() ) / size;
		emit BeginDrag(x,y);
		} // not right - assume left
	} // FlexibleIsosurfaceWidget::mousePressEvent()
	
void FlexibleIsosurfaceWidget::mouseMoveEvent(QMouseEvent *event)
	{ // FlexibleIsosurfaceWidget::mouseMoveEvent()
	if (whichButton != Qt::RightButton)
		{ // not right - assume left
		// find the minimum of height & width	
		float size = (width() > height()) ? height() : width();
		// scale both coordinates from that
		float x = (2.0 * event->x() - size) / size;
		float y = (size - 2.0 * event->y() ) / size;
		emit ContinueDrag(x,y);
		} // not right - assume left
	} // FlexibleIsosurfaceWidget::mouseMoveEvent()
	
void FlexibleIsosurfaceWidget::mouseReleaseEvent(QMouseEvent *event)
	{ // FlexibleIsosurfaceWidget::mouseReleaseEvent()
	if (whichButton != Qt::RightButton)
		emit EndDrag();
	} // FlexibleIsosurfaceWidget::mouseReleaseEvent()
	
	