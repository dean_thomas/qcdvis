///////////////////////////////////////////////////
//
//	Contour-tree based isosurfaces
//	Version 4.0 (QT-based)
//	copyright Hamish Carr
//	January, 2014
//
//	------------------------
//	FlexibleIsosurfaceController.h
//	------------------------
//	
//	The controller object for the flexible isosurface
//	interface
//	
///////////////////////////////////////////////////

#ifndef _HEADER_FLEXIBLE_ISOSURFACE_CONTROLLER_H
#define _HEADER_FLEXIBLE_ISOSURFACE_CONTROLLER_H

#include <QtGui>
#include <QMouseEvent>
#include "FlexibleIsosurface.h"
#include "FlexibleIsosurfaceWindow.h"

class FlexibleIsosurfaceController : public QObject
	{ // class FlexibleIsosurfaceController
	Q_OBJECT
	private:
	// the flexible isosurface model
	FlexibleIsosurface *flexIso;
	// the window to use for display
	FlexibleIsosurfaceWindow *flexIsoWindow;
	// node ID for dragging
	long nodeID;
	
	public:
	// constructor
	FlexibleIsosurfaceController(	FlexibleIsosurface *newFlexIso, 
									FlexibleIsosurfaceWindow *newFlexIsoWindow);
	
	public slots:
	// slot for responding to arcball rotation for object
	void objectRotationChanged();
	void lightRotationChanged();

	// slots for responding to zoom & scale sliders
	void zoomChanged(int value);
	void stackScaleChanged(int value);
	
	// slot for responding to isovalue changes
	void isovalueChanged(int value);
	
	// slots for responding to check boxes
	void contourColourCheckChanged(int state);
	void localContoursCheckChanged(int state);
	void showTreeCheckChanged(int state);
	void colourTreeCheckChanged(int state);
	
	// slots for responding to simplification
	void priorityBoundChanged(int value);
	void treeSizeChanged(int value);
	
	// slots for responding to buttons
	void deleteSelectionPushed();
	void deleteComplementPushed();
	void restoreDeletedPushed();
	void dotLayoutPushed();
	void saveLayoutPushed();
	void loadLayoutPushed();
	void valueIncrementPushed();
	void valueDecrementPushed();
	void collapsePushed();
	void uncollapsePushed();
	
	// slot for responding to comboBox
	void measureChanged(int whichMeasure);
	
	// slot for responding to selection events in main widget
	void selectComponent(QMouseEvent *event);
	
	// slots for responding to mouse control in contour tree pane
	void BeginNodeDrag(float x, float y);
	void ContinueNodeDrag(float x, float y);
	void BeginTagDrag(float x, float y);
	void ContinueTagDrag(float x, float y);
	void EndDrag();
	
	// slot for responding to colour selection pane
	void ChooseColour(int whichColour);
	
	}; // class FlexibleIsosurfaceController

#endif