///////////////////////////////////////////////////
//
//	Contour-tree based isosurfaces
//	Version 4.0 (QT-based)
//	copyright Hamish Carr
//	January, 2014
//
//	------------------------
//	ContourTreeWidget.cpp
//	------------------------
//	
//	The widget that shows the contour tree
//	
///////////////////////////////////////////////////

#include "ContourTreeWidget.h"

ContourTreeWidget::ContourTreeWidget(FlexibleIsosurface *newFlexIso, QWidget *parent)
	: QGLWidget(parent)
	{ // ContourTreePane()
	// store pointer to the model
	flexIso = newFlexIso;
	whichButton = Qt::NoButton;
	} // ContourTreePane()
	
ContourTreeWidget::~ContourTreeWidget()
	{ // ~ContourTreeWidget()
	// nothing yet
	} // ~ContourTreeWidget()
	
// called when OpenGL context is set up
void ContourTreeWidget::initializeGL()
	{ // ContourTreeWidget::initializeGL()
	// this is straightforward 2D rendering, so disable stuff we don't need
	glDisable(GL_LIGHTING);
	glDisable(GL_DEPTH_TEST);
	// background is white
	glClearColor(1.0, 1.0, 1.0, 1.0);
	} // ContourTreeWidget::initializeGL()
	
// called every time the widget is resized
void ContourTreeWidget::resizeGL(int w, int h)
	{ // ContourTreeWidget::resizeGL()
	// reset the viewport
	glViewport(0, 0, w, h);
	
	// set projection matrix to have range of 0.0-1.0 in x, y
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(0.0, 1.0, flexIso->field->MinHeight(), flexIso->field->MaxHeight(), -1.0, 1.0);
	} // ContourTreeWidget::resizeGL()
	
// called every time the widget needs painting
void ContourTreeWidget::paintGL()
	{ // ContourTreeWidget::paintGL()
	// set model view matrix based on stored translation, rotation &c.
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	// clear the buffer
	glClear(GL_COLOR_BUFFER_BIT);
	
	// draws the contour tree
	if (flexIso->showTree)
		flexIso->field->DrawPlanarContourTree(flexIso->colouredTree);
	} // ContourTreeWidget::paintGL()

// mouse-handling
void ContourTreeWidget::mousePressEvent(QMouseEvent *event)
	{ // ContourTreeWidget::mousePressEvent()
	// store which button was pressed
	whichButton = event->button();
	// scale both coordinates to window size
	float x = (float) event->x() / (float) width();
	float y = (float) (height() - event->y()) / (float) height();
	if (whichButton == Qt::LeftButton)
		emit BeginNodeDrag(x,y);
	else
		emit BeginTagDrag(x,y);
	} // ContourTreeWidget::mousePressEvent()
	
void ContourTreeWidget::mouseMoveEvent(QMouseEvent *event)
	{ // ContourTreeWidget::mouseMoveEvent()
	float x = (float) event->x() / (float) width();
	float y = (float) (height() - event->y()) / (float) height();
	if (whichButton == Qt::LeftButton)
		emit ContinueNodeDrag(x,y);
	else
		emit ContinueTagDrag(x,y);
	} // ContourTreeWidget::mouseMoveEvent()
	
void ContourTreeWidget::mouseReleaseEvent(QMouseEvent *event)
	{ // ContourTreeWidget::mouseReleaseEvent()
	emit EndDrag();
	} // ContourTreeWidget::mouseReleaseEvent()
	