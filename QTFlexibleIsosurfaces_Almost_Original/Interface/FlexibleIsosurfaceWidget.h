///////////////////////////////////////////////////
//
//	Contour-tree based isosurfaces
//	Version 4.0 (QT-based)
//	copyright Hamish Carr
//	January, 2014
//
//	------------------------
//	FlexibleIsosurfaceWidget.h
//	------------------------
//	
//	The main widget that shows the flexible isosurface
//	itself
//	
///////////////////////////////////////////////////

#ifndef _HEADER_FLEXIBLE_ISOSURFACE_WIDGET_H
#define _HEADER_FLEXIBLE_ISOSURFACE_WIDGET_H

#include <QGLWidget>
#include <QGLPixelBuffer>
#include <QMouseEvent>
#include "FlexibleIsosurface.h"

class FlexibleIsosurfaceWidget : public QGLWidget										
	{ // class FlexibleIsosurfaceWidget
	Q_OBJECT
	public:	
	// the model - i.e. the flexible isosurface data
	FlexibleIsosurface *flexIso;	
	
	// which button was last pressed
	int whichButton;

	// constructor
	FlexibleIsosurfaceWidget(FlexibleIsosurface *newFlexIso, QWidget *parent);
	
	// destructor
	~FlexibleIsosurfaceWidget();
			
	// routine to pick a component in the back buffer
	long PickComponent(int x, int y);

	// a pixel buffer for pick rendering
	QGLPixelBuffer *pBuffer;
			
	protected:
	// called when OpenGL context is set up
	void initializeGL();
	// called every time the widget is resized
	void resizeGL(int w, int h);
	// called every time the widget needs painting
	void paintGL();

	// mouse-handling
	virtual void mousePressEvent(QMouseEvent *event);
	virtual void mouseMoveEvent(QMouseEvent *event);
	virtual void mouseReleaseEvent(QMouseEvent *event);

	public:
	signals:
	void BeginDrag(float x, float y);
	void ContinueDrag(float x, float y);
	void EndDrag();
	void Select(QMouseEvent *event);
	}; // class FlexibleIsosurfaceWidget

#endif
