///////////////////////////////////////////////////
//
//	Contour-tree based isosurfaces
//	Version 4.0 (QT-based)
//	copyright Hamish Carr
//	January, 2014
//
//	------------------------
//	LogCollapseWidget.h
//	------------------------
//	
//	A widget displaying the tree size / collapse
//	bound as a log-log plot
//	
///////////////////////////////////////////////////

#ifndef _HEADER_LOG_COLLAPSE_WIDGET_H
#define _HEADER_LOG_COLLAPSE_WIDGET_H

#include <QGLWidget>
#include "FlexibleIsosurface.h"

class LogCollapseWidget : public QGLWidget
	{ // class LogCollapseWidget
	public:	
	// the model - i.e. the flexible isosurface data
	FlexibleIsosurface *flexIso;	

	// constructor
	LogCollapseWidget(FlexibleIsosurface *newFlexIso, QWidget *parent);

	// destructor
	~LogCollapseWidget();
												
	protected:
	// called when OpenGL context is set up
	void initializeGL();
	// called every time the widget is resized
	void resizeGL(int w, int h);
	// called every time the widget needs painting
	void paintGL();
	}; // class LogCollapseWidget

#endif
