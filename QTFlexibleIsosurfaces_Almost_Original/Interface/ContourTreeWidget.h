///////////////////////////////////////////////////
//
//	Contour-tree based isosurfaces
//	Version 4.0 (QT-based)
//	copyright Hamish Carr
//	January, 2014
//
//	------------------------
//	ContourTreeWidget.h
//	------------------------
//	
//	The widget that shows the contour tree
//	
///////////////////////////////////////////////////

#ifndef _HEADER_FLEXIBLE_CONTOUR_TREE_WIDGET_H
#define _HEADER_FLEXIBLE_CONTOUR_TREE_WIDGET_H

#include <QGLWidget>
#include <QMouseEvent>
#include "FlexibleIsosurface.h"

class ContourTreeWidget : public QGLWidget
	{ // class ContourTreeWidget
	Q_OBJECT
	public:	
	// the model - i.e. the flexible isosurface data
	FlexibleIsosurface *flexIso;	
	
	// which button was last pressed
	int whichButton;

	// constructor
	ContourTreeWidget(FlexibleIsosurface *newFlexIso, QWidget *parent);

	// destructor
	~ContourTreeWidget();
												
	protected:
	// called when OpenGL context is set up
	void initializeGL();
	// called every time the widget is resized
	void resizeGL(int w, int h);
	// called every time the widget needs painting
	void paintGL();
	
	// mouse-handling
	virtual void mousePressEvent(QMouseEvent *event);
	virtual void mouseMoveEvent(QMouseEvent *event);
	virtual void mouseReleaseEvent(QMouseEvent *event);

	public:
	signals:
	void BeginNodeDrag(float x, float y);
	void ContinueNodeDrag(float x, float y);
	void BeginTagDrag(float x, float y);
	void ContinueTagDrag(float x, float y);
	void EndDrag();
	}; // class ContourTreeWidget

#endif
