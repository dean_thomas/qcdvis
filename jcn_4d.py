#	E:\Dean\QCD Data\Field data\Pygrid\12x24\mu1000\config.b190k1680mu1000j02s12t24\conp0005\cool0020\cache\Timelike Plaquette_t=1.vol1

#	Todo:	
#		extract desired output dir and executable from command line
#		give fields correct names when calling the exe
#		create the output directory, if it doesn't already exist

from subprocess import call
from collections import namedtuple
from os import listdir
from os.path import isfile, isdir, join, normcase, sep
from os.path import exists, dirname, basename
from os import makedirs

import sys
import re
#from PyQt5.QtCore import *
#from PyQt5.QtGui import *
#from PyQt5.QtWidgets import *

FILENAME_REGEX = "((.*)_([1xyzt])=)(\d?\d).vol1"

def writeToFile(cmdLine):
	file = open('jcn_4d.bat', 'w')
	
	for elem in cmdLine:
		file.write(elem + ' ')
	
	file.close()

def executeJcnProgram(pathToExe, fileList, slabSize, outputDir):
	assert(isfile(pathToExe))
	assert(isdir(outputDir))
	
	#	Add the exe to the cmd call
	cmdLine = []
	cmdLine.append(pathToExe)
	
	i=0
	
	#	Insert each file into the command line arguments
	for file in fileList:
		assert(isfile(file))
		cmdLine.append('-input')
		cmdLine.append(file)
		cmdLine.append('f' + str(i))
		cmdLine.append(slabSize)
		i = i + 1

	cmdLine.append('-ReebRb')
	cmdLine.append('-ReebDot')
	cmdLine.append('-JcnDot')
	cmdLine.append('jcn.dot')
	cmdLine.append('-nogui')
	cmdLine.append('-periodic')
	cmdLine.append('-outdir')
	cmdLine.append(outputDir)
	cmdLine.append('-save_logs')
	
	writeToFile(cmdLine)
	#print(cmdLine)
	#call(cmdLine)
	
def patternMatchOnAxis(pathToFile, sliceMax):
	#	Make sure the input is an actual file
	assert(isfile(pathToFile))

	#	A list of absolute paths to the files to be analysed
	result = []
	
	#	Extract the path to the folder the file is in and
	#	its filename
	pathToDir = normcase(dirname(pathToFile))
	inputFilename = basename(pathToFile)
	
	#	Get a list of all files in the directory
	files = [f for f in listdir(pathToDir) if isfile(join(pathToDir, f))]

	#	Extract some details from the input file
	inputMatches = re.search(FILENAME_REGEX, inputFilename)
	
	#	e.g. 'Spacelike Plaquette_x='
	inputFilenamePattern = inputMatches.group(1)
	
	#	e.g. 'Spacelike Plaquette'
	inputFieldVar = inputMatches.group(2)
	
	#	'x' / 'y' / 'z' / 't'
	inputAxisVar = inputMatches.group(3)
	
	#	5, 10 etc.
	inputAxisSlice = inputMatches.group(4)
	
	print("Search for '%s' fields split over the '%s' axis" %(inputFieldVar, inputAxisVar))
	
	for currentFilename in files:
		#	Current filename DOES NOT include a path
		#	Extract details from current file
		currentFileMatches = re.search(FILENAME_REGEX, currentFilename)
	
		if (currentFileMatches != None):
			#	e.g. 'Spacelike Plaquette_x='
			currentFilenamePattern = currentFileMatches.group(1)
	
			#	e.g. 'Spacelike Plaquette'
			currentFieldVar = currentFileMatches.group(2)
	
			#	'x' / 'y' / 'z' / 't'
			currentAxisVar = currentFileMatches.group(3)
		
			#	5, 10 etc.
			currentAxisSlice = inputMatches.group(4)
		
			#	Test that the field and split axis match, if so add to the output
			if currentFilenamePattern == inputFilenamePattern:
				if int(currentAxisSlice) <= sliceMax:
					#	Only add if below the max threshold
					result.append(pathToDir + sep + currentFilename)
				
	return result
	
def printUsage():
	print()
	print("Takes a single 3D slice as input.  Generates a 4D ensemble JCN by pattern matching the field and axis variables in the supplied filename.  A specified version of the jcn program can optionally be supplied.")
	print()
	print("Usage:")
	print("py -3 jcn_4d.py <path to .vol1 file> <slab size> [-max n] [jcn executable]")
	print("\t[-max n] only add slices with an index of less than or equal to n")

if __name__ == "__main__":
	#app = QApplication(sys.argv)
	argv = sys.argv[1:]
	
	#	Default place to look for exe
	jcnExe = "G:/qcdvis/vtkReebGraphTest.exe"
	#jcnExe = "C:/Users/4thFloor/Documents/Qt/QtFlexibleIsosurfaces/vtkReebGraphTest/x64/Release/vtkReebGraphTest.exe"
	
	#	Default to an unrealistically high value
	sliceMax = '10000'
	
	#	Default output dir
	outputDir = "./test_output"
	
	if '-max' in argv:
		#	Find the index of the 'max' flag
		index = argv.index('-max')
		
		#	input dir should be the next parameter
		sliceMax = argv[index + 1]
		
		#	Remove the flags
		argv.remove('-max')
		argv.remove(sliceMax)
	
	if len(argv) == 2:
		inFile = argv[0]
		slabsize = argv[1]
	
		if isfile(inFile):
			#	Get a list of matching files
			fileList = patternMatchOnAxis(inFile, int(sliceMax))
			
			executeJcnProgram(jcnExe, fileList, slabsize, outputDir)
			#for f in fileList:
			#print("%s" %f)
		else:
			print("Not a valid file: %s" %(inFile))
			printUsage()
	else:
		printUsage()
	