#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QDir>
#include <QRegExp>
#include <QDirIterator>
#include <QRegularExpression>
#include <cassert>
#include <QQueue>
#include <QTextStream>
#include <QDebug>

#include "LatticeParser/DataModel.h"

namespace Ui {
	class MainWindow;
}

#ifdef _DEBUG
	#ifdef __linux__
		#define DEFAULT_LOAD_DIR QDir(QString("/media/dean/raw_data/Dean/QCD Data/Cooled Configurations/Pygrid/"))
		#define DEFAULT_SAVE_DIR QDir(QString("/media/dean/raw_data/Dean/QCD Data/Field data/Pygrid/"))
	#else
		#define DEFAULT_LOAD_DIR "E:\\Dean\\QCD Data\\Cooled Configurations\\Edinburgh\\V16X32\\J02\\"
		#define DEFAULT_SAVE_DIR "C:\\Users\\4thFloor\\Desktop\\QCD_test_output\\"
	#endif
#else
	#ifdef __linux__
		#define DEFAULT_LOAD_DIR "/media/dean/raw_data/Dean/QCD Data/Cooled Configurations/Pygrid"
		#define DEFAULT_SAVE_DIR "/media/dean/raw_data/Dean/QCD Data/Field data/Pygrid"
	#else
		#define DEFAULT_LOAD_DIR "E:\\Dean\\QCD Data\\Cooled Configurations\\Edinburgh\\V16X32\\J02\\"
		#define DEFAULT_SAVE_DIR "C:\\Users\\4thFloor\\Desktop\\QCD_test_output\\"
	#endif
#endif

//	Where we are to store file paths in each items QVariantArray
#define PATH_TO_FILE_INDEX (Qt::UserRole + 1)
#define CONFIGURATION_NAME_INDEX (Qt::UserRole + 2)
#define COOLING_SLICE_INDEX (Qt::UserRole + 3)

//	Work around for fixed variable functions (until dynamic function loading
//	is implemented)
#define POLYAKOV_INDEX 0
#define SPACELIKE_INDEX 1
#define TIMELIKE_INDEX 2
#define DIFFERENCE_INDEX 3
#define AVERAGE_INDEX 4
#define TOPOLOGICAL_CHARGE_DENSITY_INDEX 5

//#define QCDVIS_EXE "C:\\Users\\4thFloor\\Documents\\Qt\\QtFlexibleIsosurfaces\\QTFlexibleIsosurfaces_Modified\\debug\\QTFlexibleIsosurfaces_Modified.exe"

#ifdef __linux__
	#define QCDVIS_EXE ""
	#define PYTHON_EXE "python3"
#else
	#define QCDVIS_EXE "C:\\Users\\4thFloor\\Documents\\Qt\\QtFlexibleIsosurfaces\\QTFlexibleIsosurfaces_Modified\\release\\QTFlexibleIsosurfaces_Modified.exe"
	#define PYTHON_EXE "C:\\Anaconda3\\Python.exe"
#endif

//	If set will show the absolute path in the input/output folder line edit
#define SHOW_FULLPATH_INLINE


#define HISTOGRAM_4D_PY "C:\\Users\\4thFloor\\Documents\\Qt\\QtFlexibleIsosurfaces\\histogram_4d.py"



class QTreeWidgetItem;
class QStandardItemModel;

///
/// \brief		Used to store a matrix of available configurations from an
///				ensemble directory
/// \since		16-10-2015
/// \author		Dean
///
struct EnsembleData
{
		using CoolNum = size_t;
		using CoolingSliceFileList = std::map<CoolNum, QString>;
		struct Configuration
		{
				QString name;
				CoolingSliceFileList coolingSliceFileList;
		};
		using ConfigurationList = QVector<Configuration>;

		EnsembleData() { }

		EnsembleData(const QDir& m_inputDir)
		{
			auto absolutePath = m_inputDir.absolutePath();

			//	Examples: "conp0052", "conp0230", "conp9833"
			const QRegularExpression confRegEx("^conp\\d{4}");

			//	Examples: "cool0000_conp0052", "cool0100_conp0332"
			//	We will capture the cooling number and configuration number
			const QRegularExpression coolRegEx("^cool(\\d{4})_conp(\\d{4})$");

			//	Try to extract the ensemble name
			const QRegularExpression ensembleRegex("config.b(\\d+)k(\\d+)mu(\\d+)"
												   "j(\\d+)s(\\d+)t(\\d+)");
			QRegularExpressionMatch ensembleMatches;
			auto matchedEnsembleData = absolutePath.contains(ensembleRegex, &ensembleMatches);
			if (matchedEnsembleData)
			{
				ensembleName = ensembleMatches.captured(0);
				std::cout << "Ensemble name is: " << ensembleName.toStdString()
						  << std::endl;
			}

			//	Iterate through everything in the ensemble directory
			QDirIterator confIterator(m_inputDir, QDirIterator::NoIteratorFlags);
			while(confIterator.hasNext())
			{
				//	Find configuration folders (e.g. 'conp0005') within the ensemble
				confIterator.next();
				QFileInfo confInfo(confIterator.filePath());

				if (confInfo.isDir() && confInfo.fileName().contains(confRegEx))
				{
					//	Create a new top level item for the configuration
					Configuration currentConfiguration = { confInfo.fileName(), CoolingSliceFileList() };

					//	Iterate through everything in the configuration folder
					QDirIterator coolIterator(QDir(confInfo.absoluteFilePath()));
					while(coolIterator.hasNext())
					{
						//	Look for cooled configurations in the folder
						coolIterator.next();
						QFileInfo coolInfo(coolIterator.filePath());

						//	For extraction of cooling number etc.
						QRegularExpressionMatch matches;

						if (coolInfo.fileName().contains(coolRegEx, &matches))
						{
							auto coolNum = matches.captured(1).toLong();
							//auto confNum = matches.captured(2).toLong();

							assert (coolNum >= 0);
							//assert (confNum >= 0);

							//	Update the global maximum
							if (coolNum > maxCools) maxCools = coolNum;

							currentConfiguration.coolingSliceFileList[coolNum] = QString(coolInfo.filePath());
						}
					}

					//	Add the configuration to the list
					availableConfigurations.push_back(currentConfiguration);
				}
			}
		}

		ConfigurationList availableConfigurations;
		size_t maxCools = 0;
		QString ensembleName;
};

struct Index4
{
		using size_type = size_t;

		size_type x;
		size_type y;
		size_type z;
		size_type t;
};

struct MinMaxFileParser
{
		const std::string REGEX = R"(^\s*(\d+)\s+(\d+)\s+(\d+)\s+(\d+)\s+(\d+)\s+(\d+)\s+(\d+)\s+(\d+)\s+(\d+)\s+(\d+)\s+(\d+)\s+(\d+))";
		QRegularExpression m_criticalPointRegEx;

		struct CoolData
		{
				Index4 posMaxS;
				Index4 posMaxQ;
				Index4 posMinQ;
		};

		using CoolingSliceFileList = std::vector<CoolData>;

		QString m_filename;

		MinMaxFileParser(const QString& filename)
		{
			SetFilename(filename);
			m_criticalPointRegEx = QRegularExpression(QString::fromStdString(REGEX));
		}

		void SetFilename(const QString& filename)
		{
			QFileInfo fileInfo(filename);
			assert(fileInfo.exists());

			m_filename = filename;
		}

		CoolingSliceFileList operator()()
		{
			CoolingSliceFileList result;

			qDebug() << "Parsing file:" << m_filename;

			QFile inFile(m_filename);
			if (inFile.open(QFile::ReadOnly | QFile::Text))
			{
				//	There will be no data for the 'uncooled' slice
				//	So we'll add a zeroed entry
				result.push_back({{0, 0, 0, 0},
										 {0, 0, 0, 0},
										 {0, 0, 0, 0}});

				QTextStream inStream(&inFile);
				while (!inStream.atEnd())
				{
					QString buffer = inStream.readLine();

					qDebug() << "Parsing line:" << buffer;

					//	For extraction of cooling number etc.
					QRegularExpressionMatch matches;

					if (buffer.contains(m_criticalPointRegEx, &matches))
					{
						Index4 maxS = {	 matches.captured( 1).toLong(),
										 matches.captured( 2).toLong(),
										 matches.captured( 3).toLong(),
										 matches.captured( 4).toLong() };
						Index4 maxQ = {	 matches.captured( 5).toLong(),
										 matches.captured( 6).toLong(),
										 matches.captured( 7).toLong(),
										 matches.captured( 8).toLong() };
						Index4 minQ = {	 matches.captured( 9).toLong(),
										 matches.captured(10).toLong(),
										 matches.captured(11).toLong(),
										 matches.captured(12).toLong() };


						qDebug() << "maxS = ("
								 << maxS.x << maxS.y << maxS.z << maxS.t
								 << ")";
						qDebug() << "maxQ = ("
								 << maxQ.x << maxQ.y << maxQ.z << maxQ.t
								 << ")";
						qDebug() << "minQ = ("
								 << minQ.x << minQ.y << minQ.z << minQ.t
								 << ")";

						result.push_back({ maxS, maxQ, minQ});
					}
				}
			}
			return result;
		}
};

class MainWindow : public QMainWindow
{
		Q_OBJECT
	public:
		//	Define the level of working precision
		using Decimal = double;

		using size_type = size_t;
		using FileList = QQueue<QString>;
		using FieldList = QQueue<bool>;

		struct ConfigurationParameters
		{
				std::string ensembleName;
				std::string confName;
				size_type cools;
				size_type b;
				size_type k;
				size_type mu;
				size_type j;
				size_type s;
				size_type t;
		};

		QDir m_inputDir = DEFAULT_LOAD_DIR;
		QDir m_outputDir = DEFAULT_SAVE_DIR;

		QStandardItemModel* m_tableModel = nullptr;

		QRegularExpression m_configRegex;
		EnsembleData m_currentEnsembleData;

		QStandardItemModel* generateMinMaxTable(const MinMaxFileParser::CoolingSliceFileList& data);
		QStandardItemModel* generateEnsembleModel(EnsembleData &availableConfigurations);

		void createTableViewContextMenu();

		ConfigurationParameters parseConfigurationParameters(
				const QString& absolutePath) const;

		FileList generateMarkedFileList() const;
		std::string generateFullOutputFilename(const QDir& outputBase,
											   const QString& ensembleName,
											   const QString& configurationName,
											   const size_type& cool,
											   const QString& variableName) const;

		void execApplication(const QString& applicationFilename, const FileList& files, const QString& stdoutFile, const QString& stderrFile);

		void updateMaximaTable(const QString& filename);

		FileList generateFieldVariables(const FileList& checkedFileList,
										const bool& spaceLikePlaquette,
										const bool& timeLikePlaquette,
										const bool& averagePlaquette,
										const bool& differencePlaquette,
										const bool& polyakovLoop,
										const bool& topologicalChargeDensity);

		void rowSelect(const int& row, const bool& checked);
		void colSelect(const int& col, const bool& checked);

		void setupTableViewConfigurationsActions();

		virtual void closeEvent(QCloseEvent*);
		void saveSettings() const;
		void restoreSettings();
	public:
		void SetInputDir(const QDir& dir);
		void SetOutputDir(const QDir& dir);
		explicit MainWindow(QWidget *parent = 0);
		~MainWindow();


		bool executePythonHistogramScript(const QString& filename);

	private slots:
		bool isValidEnsembleDir(const QDir& dir) const;
		void updateConfigurationTable();
		void on_actionBrowseOutput_triggered();
		void on_actionBrowseInput_triggered();

		void on_actionEnableAll_triggered();

		void on_actionDisableAll_triggered();

		void on_actionTest_Accessors_triggered();
		void on_pushButton_4_clicked();

		void on_pushButtonViewAction_clicked();

		void on_pushButtonViewFFdual_clicked();


		void on_pushButtonGenerateFields_clicked();

		void on_actionTestMinMaxParse_triggered();

		void on_groundStateCheckButton_clicked();

		void on_pushButton_clicked();

		void on_actionGroundStateCheck_triggered();

	signals:
		void on_clickConfiguration(const QString&);
		void on_clickCoolingSlice(const size_t&);
	private:
		Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
