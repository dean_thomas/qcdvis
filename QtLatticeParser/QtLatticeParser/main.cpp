#include "MainWindow.h"
#include <QApplication>

int main(int argc, char *argv[])
{
	//	Turn off Qt's buffered output behaviour
	setbuf(stdout, NULL);
	setbuf(stderr, NULL);

	QApplication a(argc, argv);
	MainWindow w;
	w.show();

	return a.exec();
}
