#include "DataModel.h"

#include <iostream>
#include <iomanip>
#include <cassert>
#include <fstream>
#include <functional>

#include "parsersu2hmc.h"

using std::function;
using std::cout;
using std::endl;
using std::cerr;
using std::ofstream;
using std::setw;
using std::setprecision;

///
/// \brief		Generates a ground state configuration of specified
///				dimensionality (where all links are identity matrix)
/// \param xDim
/// \param yDim
/// \param zDim
/// \param tDim
/// \return
/// \author		Dean
/// \since		29-02-2016
///
DataModel DataModel::GenerateGroundStateLattice(const size_t xDim,
												const size_t yDim,
												const size_t zDim,
												const size_t tDim)
{
	DataModel result("", "Ground state", 0, 0, 0, 0, 0);

	result.m_lattice = Lattice::GenerateGroundStateLattice(xDim, yDim, zDim, tDim);

	return result;
}

///
/// \brief DataModel::markCacheAsDirty
/// \since		22-10-2015
/// \author		Dean
///
void DataModel::markCacheAsDirty()
{
	m_spaceLikePlaquettesAreComputed = false;
	m_timeLikePlaquettesAreComputed = false;
	m_averagePlaquettesAreComputed = false;
	m_differencePlaquettesAreComputed = false;
	m_topologicalChargeDensityFieldIsComputed = false;
	m_polyakovLoopFieldIsComputed = false;
}

///	Calculate the average timelike plaquettes at each site on the lattice for a given time slice
///
///	Calculates the plaquettes in the XT, YT and ZT planes and returns the average
///	at each site and stores them as a scalar volume.
///
///	@param coordinate: a x,y,z,t coordinate in Euclidean space-time.
///	@return ScalarVolume: the average timelike plaquette at each site on the lattice
/// \author		Dean
/// \author		Dean: update to use C++11 [20-10-2015]
///
DataModel::Array4f DataModel::calculateAveragePlaquetteField() const
{
	//	This should remain a member of the data model to allow us to use
	//	the cached space/time plaquette data
	Array4f result(m_lattice.SizeX(),
				   m_lattice.SizeY(),
				   m_lattice.SizeZ(),
				   m_lattice.SizeT());

	//	Retreive the space-like and time-like plaquettes (if they haven't been
	//	pre-computed, this will force a computation).
	auto sp = GetSpaceLikePlaquetteField();
	auto tp = GetTimeLikePlaquetteField();

	for (auto x = 0; x < m_lattice.SizeX(); ++x)
	{
		for (auto y = 0; y < m_lattice.SizeY(); ++y)
		{
			for (auto z = 0; z < m_lattice.SizeZ(); ++z)
			{
				for (auto t = 0; t < m_lattice.SizeT(); ++t)
				{
					auto averagePlaquette =
							(sp.at(x, y, z, t) + tp.at(x, y, z, t)) / 2.0;

					result.at(x, y, z, t) = averagePlaquette;
				}
			}
		}
	}
	return result;
}

///	Calculate the differnec plaquettes at each site on the lattice for a given time slice
///
///	Calculates the plaquettes in the XT, YT and ZT planes and returns the average
///	at each site and stores them as a scalar volume.
///
///	@param coordinate: a x,y,z,t coordinate in Euclidean space-time.
///	@return ScalarVolume: the average timelike plaquette at each site on the lattice
/// \author		Dean
/// \author		Dean: update to use C++11 [20-10-2015]
///
DataModel::Array4f DataModel::calculateDifferencePlaquetteField() const
{
	//	This should remain a member of the data model to allow us to use
	//	the cached space/time plaquette data
	Array4f result(m_lattice.SizeX(),
				   m_lattice.SizeY(),
				   m_lattice.SizeZ(),
				   m_lattice.SizeT());

	//	Retreive the space-like and time-like plaquettes (if they haven't been
	//	pre-computed, this will force a computation).
	auto sp = GetSpaceLikePlaquetteField();
	auto tp = GetTimeLikePlaquetteField();

	for (auto x = 0; x < m_lattice.SizeX(); ++x)
	{
		for (auto y = 0; y < m_lattice.SizeY(); ++y)
		{
			for (auto z = 0; z < m_lattice.SizeZ(); ++z)
			{
				for (auto t = 0; t < m_lattice.SizeT(); ++t)
				{
					auto differencePlaquette =
							sp.at(x, y, z, t) - tp.at(x, y, z, t);

					result.at(x, y, z, t) = differencePlaquette;
				}
			}
		}
	}
	return result;
}

///
///	\since		08-10-2015
///	\author		Dean
///
DataModel::DataModel(const string& filename,
					 const std::string& conf,
					 const size_type& cools,
					 const size_type& b,
					 const size_type& k,
					 const size_type& mu,
					 const size_type& j)
	: m_cools{cools}, m_beta{b}, m_k{k}, m_mu{mu}, m_j{j}
{
	m_configurationName = conf;

	if (filename != "")
	{
		cout << "Loading lattice configuration: " << filename << endl;

		auto result = LoadConfiguration(filename);

		cout << (result ? "Ok." : "Failed.") << endl;
	}

	markCacheAsDirty();
}

///
///	\since		08-10-2015
///	\author		Dean
/// \author		Dean: add call to clear cache variables [22-10-2015]
///
bool DataModel::LoadConfiguration(const string &filename)
{
	function<Lattice(string)> parser = ParserSU2HMC();

	try
	{
		m_lattice = parser(filename);

		assert (m_lattice.NumberOfSites() > 0);

		markCacheAsDirty();

		std::ofstream outfile(filename + ".txt");
		if (outfile)
		{
			auto avg = m_lattice.CalculateAveragePlaquettes();
			outfile << avg.ToString() << endl;
			outfile.close();
		}
		return true;
	}
	catch (std::invalid_argument& ex)
	{
		cerr << "Error!  Could not load file: " << filename << endl;
		cerr << ex.what() << endl;
		return false;
	}
}

///
/// \brief		Returns the spacelike plaquette array from the cache, or if
///				not available computes the field and caches it before return
/// \return
/// \since		22-10-2015
/// \author		Dean
///
DataModel::Array4f DataModel::GetSpaceLikePlaquetteField() const
{
	if (m_spaceLikePlaquettesAreComputed)
	{
		//	Return cached field
		return m_spaceLikePlaquettes;
	}
	else
	{
		//	Compute cached field
		m_spaceLikePlaquettes = m_lattice.calculateSpaceLikePlaquetteField();

		//	Mark as computed
		m_spaceLikePlaquettesAreComputed = true;

		//	And return
		return m_spaceLikePlaquettes;
	}
}

///
/// \brief		Returns the timelike plaquette array from the cache, or if
///				not available computes the field and caches it before return
/// \return
/// \since		22-10-2015
/// \author		Dean
///
DataModel::Array4f DataModel::GetTimeLikePlaquetteField() const
{
	if (m_timeLikePlaquettesAreComputed)
	{
		//	Return cached field
		return m_timeLikePlaquettes;
	}
	else
	{
		//	Compute cached field
		m_timeLikePlaquettes = m_lattice.calculateTimeLikePlaquetteField();

		//	Mark as computed
		m_timeLikePlaquettesAreComputed = true;

		//	And return
		return m_timeLikePlaquettes;
	}
}

///
/// \brief		Returns the average plaquette array from the cache, or if
///				not available computes the field and caches it before return
/// \return
/// \since		22-10-2015
/// \author		Dean
///
DataModel::Array4f DataModel::GetAveragePlaquetteField() const
{
	if (m_averagePlaquettesAreComputed)
	{
		//	Return cached field
		return m_averagePlaquettes;
	}
	else
	{
		//	Compute cached field
		m_averagePlaquettes = calculateAveragePlaquetteField();

		//	Mark as computed
		m_averagePlaquettesAreComputed = true;

		//	And return
		return m_averagePlaquettes;
	}
}

///
/// \brief		Returns the difference plaquette array from the cache, or if
///				not available computes the field and caches it before return
/// \return
/// \since		22-10-2015
/// \author		Dean
///
DataModel::Array4f DataModel::GetDifferencePlaquetteField() const
{
	if (m_differencePlaquettesAreComputed)
	{
		//	Return cached field
		return m_differencePlaquettes;
	}
	else
	{
		//	Compute cached field
		m_differencePlaquettes = calculateDifferencePlaquetteField();

		//	Mark as computed
		m_differencePlaquettesAreComputed = true;

		//	And return
		return m_differencePlaquettes;
	}
}

///
/// \brief		Returns the magnetic field array from the cache, or if
///				not available computes the field and caches it before return
/// \return
/// \since		22-10-2015
/// \author		Dean
///
DataModel::Array4v3 DataModel::GetMagneticField() const
{
	if (m_magneticFieldIsComputed)
	{
		//	Return cached field
		return m_magneticField;
	}
	else
	{
		//	Compute cached field
		m_magneticField = m_lattice.calculateMagneticField();

		//	Mark as computed
		m_magneticFieldIsComputed = true;

		//	And return
		return m_magneticField;
	}
}

///
/// \brief		Returns the electric field array from the cache, or if
///				not available computes the field and caches it before return
/// \return
/// \since		22-10-2015
/// \author		Dean
///
DataModel::Array4v3 DataModel::GetElectricField() const
{
	if (m_electricFieldIsComputed)
	{
		//	Return cached field
		return m_electricField;
	}
	else
	{
		//	Compute cached field
		m_electricField = m_lattice.calculateElectricField();

		//	Mark as computed
		m_electricFieldIsComputed = true;

		//	And return
		return m_electricField;
	}
}

///
/// \brief		Returns the topological charge density array from the cache,
///				or if not available computes the field and caches it
///				before return
/// \return
/// \since		22-10-2015
/// \author		Dean
///
DataModel::Array4f DataModel::GetTopologicalChargeDensityField() const
{
	if (m_topologicalChargeDensityFieldIsComputed)
	{
		//	Return cached field
		return m_topologicalChargeDensityField;
	}
	else
	{
		//	Compute cached field
		m_topologicalChargeDensityField = m_lattice.calculateTopologicalChargeDensityField();

		//	Mark as computed
		m_topologicalChargeDensityFieldIsComputed = true;

		//	And return
		return m_topologicalChargeDensityField;
	}
}

///
/// \brief		Returns the polyakov loop array from the cache, or if
///				not available computes the field and caches it before return
/// \return
/// \since		22-10-2015
/// \author		Dean
///
DataModel::Array3f DataModel::GetPolyakovLoopField() const
{
	if (m_polyakovLoopFieldIsComputed)
	{
		//	Return cached field
		return m_polyakovLoopField;
	}
	else
	{
		//	Compute cached field
		m_polyakovLoopField = m_lattice.CalculatePolyakovLoopField();

		//	Mark as computed
		m_polyakovLoopFieldIsComputed = true;

		//	And return
		return m_polyakovLoopField;
	}
}

///
/// \brief		Saves the given array data to a file
/// \param		filename
/// \param		data
/// \return
/// \since		22-10-2015
/// \author		Dean
///
bool DataModel::SaveToFile(const std::string& filename, const Array4f&) const
{
	//	Output to file
	ofstream outFile(filename);
	if(outFile.is_open())
	{
		outFile.close();

		return true;
	}
	else return false;
}
