///
/// @file DoublelMatrix.h
/// @author  Dean Thomas <dean.plymouth@googlemail.com>
/// @version 0.1
///
/// @section LICENSE
///
/// This program is free software; you can redistribute it and/or
/// modify it under the terms of the GNU General Public License as
/// published by the Free Software Foundation; either version 2 of
/// the License, or (at your option) any later version.
///
/// This program is distributed in the hope that it will be useful, but
/// WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
/// General Public License for more details at
/// http://www.gnu.org/copyleft/gpl.html
///
/// @section DESCRIPTION
///
/// Represents a Matrix of double precision scalar values
///
#ifndef _DoubleMatrix_h_
#define _DoubleMatrix_h_

#include "AbstractMatrix.h"
//#include "PartitionedDoubleMatrix.h"
//#include "LinearAlgebraConstants.h"
//#include "..\BasicStructs.h"
#include <string>
#include <sstream>

using std::string;
using std::stringstream;

using LinearAlgebra::AbstractMatrix;
//using LinearAlgebra::PartitionedDoubleMatrix;

namespace LinearAlgebra
{
	class DoubleMatrix : public AbstractMatrix < double >
	{
		public:
			using size_type = C::size_type;

			///	Conversion from an abstract matrix
			//	Could this work? No need for call to base constructor...
			//	http://en.cppreference.com/w/cpp/language/reinterpret_cast
//			DoubleMatrix(const AbstractMatrix<double>& rhs)
//				: AbstractMatrix(rhs.RowCount(), rhs.ColCount(), rhs.AsRowOrderedArray()){}

			///	Directly inherited constructors
			DoubleMatrix()
				: AbstractMatrix<double>(){}
			DoubleMatrix(const size_type& rows, const size_type& cols)
				: AbstractMatrix<double>(rows, cols){}
//			DoubleMatrix(const size_type& rows, const size_type& cols, const double values[])
//				: AbstractMatrix<double>(rows, cols, values){}
//			DoubleMatrix(const size_type& rows, const size_type& cols, const std::vector<double> values)
//				: AbstractMatrix<double>(rows, cols, values){}
			DoubleMatrix(const double e11, const double e12, const double e21, const double e22)
				: AbstractMatrix<double>(e11, e12, e21, e22){}
			DoubleMatrix(const double e11, const double e12, const double e13, const double e21, const double e22, const double e23, const double e31, const double e32, const double e33)
				: AbstractMatrix<double>(e11, e12, e13, e21, e22, e23, e31, e32, e33){}
			DoubleMatrix(const double e11, const double e12, const double e13, const double e14, const double e21, const double e22, const double e23, const double e24, const double e31, const double e32, const double e33, const double e34, const double e41, const double e42, const double e43, const double e44)
				: AbstractMatrix<double>(e11, e12, e13, e14, e21, e22, e23, e24, e31, e32, e33, e34, e41, e42, e43, e44){}

			///	Return the matrix as a string
			///
			///	Iteratrate through this matrix and output as
			///	a string of double prescision floating
			///	point numbers on multiple lines
			///
			///	@return: a string representing the matrix
			string ToString()
			{
				unsigned int targetLength = 10;
				unsigned int precision = 4;

				stringstream result;

				for (unsigned int r = 0; r < m_rows; ++r)
				{
					result << "|";
					result << " ";

					for (unsigned int c = 0; c < m_cols; ++c)
					{
						//	Create a temp stringstream
						//	to print the string and measure
						//	how much padding is required
						stringstream temp;
						temp.precision(precision);
						temp << m_values.at(indexOf(r, c));

						//	Insert leading spaces
						for (unsigned int i = 0; i < targetLength - temp.str().length(); ++i)
						{
							result << " ";
						}

						//	and then the number
						result << temp.str();
						result << " ";
					}

					result << "|";
					result << "\n";
				}

				return result.str();
			}

			bool IsSymetric() const
			{
				return (*this == Transpose());
			}

			//		bool IsOrthogonal() const
			//		{
			//			//	Test to 15 decimal place precision
			//			return ((Transpose() * *this).RoundDP(15) == Identity(rows));
			//		}

			//		PartitionedDoubleMatrix PartitionMatrix(vector<unsigned int>colPartitionFormat, vector<unsigned int>rowPartitionFormat)
			//		{
			//			unsigned int subMatrixCount = rowPartitionFormat.size() * colPartitionFormat.size();

			//			Quad* subMatrixParameters = new Quad[subMatrixCount];

			//			unsigned int i = 0;
			//			//unsigned int top = 1;

			//				//unsigned int left = 1;
			//			unsigned int top = 1;

			//			for (unsigned int rowI = 0; rowI < rowPartitionFormat.size(); ++rowI)
			//			{
			//				unsigned int left = 1;

			//				for (unsigned int colI = 0; colI < colPartitionFormat.size(); ++colI)
			//				{
			//					subMatrixParameters[i] = { left, top, left + colPartitionFormat.at(colI) - 1, top + rowPartitionFormat.at(rowI) - 1 };

			//					i++;

			//					left += colPartitionFormat.at(colI);
			//				}
			//				top += rowPartitionFormat.at(rowI);
			//			}

			//			DoubleMatrix* values = new DoubleMatrix[subMatrixCount];

			//			for (unsigned int i = 0; i < subMatrixCount; i++)
			//			{
			//				fprintf(stdout, "%i: { %i, %i, %i, %i}\n", i, subMatrixParameters[i].first, subMatrixParameters[i].second, subMatrixParameters[i].third, subMatrixParameters[i].forth);

			//				values[i] = SubMatrix(subMatrixParameters[i].first, subMatrixParameters[i].second, subMatrixParameters[i].third, subMatrixParameters[i].forth);
			//				fprintf(stdout, "\n%s\n", values[i].ToString().c_str());
			//			}

			//			//DoubleMatrix subMatrix = SubMatrix(2, 2, 3, 3 );
			//			//fprintf(stdout, "\n%s\n", subMatrix.ToString().c_str());
			//			return PartitionedDoubleMatrix(rowPartitionFormat.size(), colPartitionFormat.size(), values);
			//		}


	};
};

#endif _RealMatrix1_h_
