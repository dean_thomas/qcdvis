///
/// @file AbstractMatrix<ElementType>.h
/// @author  Dean Thomas <dean.plymouth@googlemail.com>
/// @version 0.1
///
/// @section LICENSE
///
/// This program is free software; you can redistribute it and/or
/// modify it under the terms of the GNU General Public License as
/// published by the Free Software Foundation; either version 2 of
/// the License, or (at your option) any later version.
///
/// This program is distributed in the hope that it will be useful, but
/// WITHOUT ANY WARRANTY; without even the implied warranty of
/// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
/// General Public License for more details at
/// http://www.gnu.org/copyleft/gpl.html
///
/// @section DESCRIPTION
///
/// Represents a Matrix of double precision scalar values
///
#ifndef _AbstractMatrix_h_
#define _AbstractMatrix_h_

#define _USE_MATH_DEFINES

#define MAT_INVALID_OPERATION 0xE0
#define MAT_NOT_SUPPORTED 0xE1
#define MAT_INVALID_COL_INDEX 0xE2
#define MAT_INVALID_ROW_INDEX 0xE3
#define MAT_INVALID_DIMENSIONALITY 0xE4

//#include "LinearAlgebraConstants.h"
#include "ComplexMatrix.h"
//#include "DoubleMatrix.h"

#include <vector>
#include <string>
#include <sstream>
#include <time.h>
#include <cmath>
#include <cassert>

#define ONE_DEG_IN_RAD (2.0 * M_PI) / 360.0

//using std::vector;
using std::string;
using std::stringstream;
using std::pair;



namespace LinearAlgebra
{
	template <typename T>
	class AbstractMatrix
	{
			using C = std::vector<T>;
			using size_type = typename C::size_type;

			enum class Type
			{
				SquareMatrix,
				ColumnVector,
				RowVector,
				Other
			};

		protected:
			//	Private variables
			C m_values;
			Type m_type;
			size_type m_rows;
			size_type m_cols;

		public:
			inline size_type RowCount() const { return m_rows; }
			inline size_type ColCount() const { return m_cols; }
			inline size_type ElementCount() const { return m_rows*m_cols; }

			///	Return the array index of the specified cell
			///
			///	Convert the row and column number to a one-dimensional
			///	index for looking up the value in the undeerlying vector.
			///	This is designed to be a internally accessed function, for
			///	user access see the [] operator.
			///
			///	@param row: the row number of the element (zero indexed)
			///	@param col: the col number of the element (zero indexed)
			///	@return an integer, the zero indexed position of the cell
			///	in the vector
			size_type indexOf(const size_type& row, const size_type& col) const
			{
				assert(row < m_rows);
				assert(col < m_cols);

				return (row * m_cols) + col;
			}
#pragma endregion Protected functions
#pragma region Private Functions
		private:
			///	Fill the matrix with values
			///
			///	Internally function to fill the matrix with the
			///	values specified in an array.
			///
			/// @param count: the number of values to be parsed
			///	@param values: a one-dimensional array of values
			///	stored row by row
//			void fill(const size_type& count, const T values[])
//			{
//				for (unsigned int i = 0; i < count; ++i)
//				{
//					T val = values[i];
//					m_values.at(i) = val;
//				}
//			}

			///	Quick calculation of determinant of 2x2 matrix
			///
			/// Private method to calculate the determinant of
			///	a 2x2 square matrix.  Public accessor is via
			/// the Determinant() function.
			///
			///	@return a double precision floating point number
			///	that is the determinant of the matrix.
			T det2x2() const
			{
				assert((m_cols == 2) && (m_rows == 2));

				//  |   a   b   |
				//  |   c   d   |
				//
				const T a = m_values.at(indexOf(0, 0));
				const T b = m_values.at(indexOf(0, 1));
				const T c = m_values.at(indexOf(1, 0));
				const T d = m_values.at(indexOf(1, 1));

				//  Sum of the products left-right diagonals
				const T diag1 = (a * d);

				//  Sun of the products right-left diagonals
				const T diag2 = (b * c);

				return diag1 - diag2;
			}

			///	Quick calculation of determinant of 3x matrix
			///
			/// Private method to calculate the determinant of
			///	a 3x3 square matrix.  Public accessor is via
			/// the Determinant() function.
			///
			///	@return a double precision floating point number
			///	that is the determinant of the matrix.
			T det3x3() const
			{
				assert((m_cols == 3) && (m_rows == 3));

				//  |   a   b   c   |
				//  |   d   e   f   |
				//  |   g   h   i   |
				//
				const T a = m_values.at(indexOf(0, 0));
				const T b = m_values.at(indexOf(0, 1));
				const T c = m_values.at(indexOf(0, 2));
				const T d = m_values.at(indexOf(1, 0));
				const T e = m_values.at(indexOf(1, 1));
				const T f = m_values.at(indexOf(1, 2));
				const T g = m_values.at(indexOf(2, 0));
				const T h = m_values.at(indexOf(2, 1));
				const T i = m_values.at(indexOf(2, 2));

				//  Sum of the products left-right diagonals
				const T diag1 = (a * e * i) + (b * f * g) + (c * d * h);

				//  Sum of the products right-left diagonals
				const T diag2 = (c * e * g) + (b * d * i) + (a * f * h);

				return diag1 - diag2;
			}

			///	Quick calculation of determinant of 4x4 matrix
			///
			/// Private method to calculate the determinant of
			///	a 4x4 square matrix.  Public accessor is via
			/// the Determinant() function.
			///
			///	@return a double precision floating point number
			///	that is the determinant of the matrix.
			T det4x4()const
			{
				assert((m_cols == 4) && (m_rows == 4));

				//  |   a   b   c   d   |
				//  |   e   f   g   h   |
				//  |   i   j   k   l   |
				//  |   m   n   o   p   |
				//
				const T a = m_values.at(indexOf(0, 0));
				const T b = m_values.at(indexOf(0, 1));
				const T c = m_values.at(indexOf(0, 2));
				const T d = m_values.at(indexOf(0, 3));

				//  Work out the determinants of each of the
				//  3x3 minor matrices
				const T detMinorA = MinorMatrix(1, 1).Determinant();
				const T detMinorB = MinorMatrix(1, 2).Determinant();
				const T detMinorC = MinorMatrix(1, 3).Determinant();
				const T detMinorD = MinorMatrix(1, 4).Determinant();

				//  Pattern:
				//  +   -   +   -   +
				return ((a * detMinorA) - (b * detMinorB) + (c * detMinorC) - (d * detMinorD));
			}

			void setSizeTo(const size_type& rowCount, const size_type& colCount)
			{
				assert ((rowCount > 0) && (colCount > 0));

				m_rows = rowCount;
				m_cols = colCount;

				m_values.clear();

				for (unsigned int i = 0; i < rowCount*colCount; ++i)
				{
					m_values.push_back(0.0);
				}
			}


#pragma endregion Private Functions
#pragma region Public Function (General)

		public:

			virtual string ToString()
			{
				return "";
			}

			///	Round to the given number of decimal places
			///
			///  Round all the elements of the matrix
			///  to the given number of decimal places
			///
			//			AbstractMatrix<ElementType> RoundDP(unsigned int decimalPlaces)const
			//			{
			//				AbstractMatrix<ElementType> result(rows, cols);

			//				for (unsigned int i = 0; i < size(); ++i)
			//				{
			//					result.at(i) = RoundDP(at(i), 15);
			//				}
			//				return result;

			//				return AbstractMatrix<ElementType>();
			//			}

			///	Return the transpose of the matrix
			///
			///	Return the matrix that represents the transpose
			///	of the matrix - where elements either side of the
			///	diagonal swap position.  If the matrix is square,
			///	a square matrix of the same order is returned.  If
			///	not square a <i>r x c</i> matrix will return an
			///	<i>c x r </i> matrix.
			///
			///	@return a AbstractMatrix<ElementType> which is this matrix with
			///	elements swapped either side of the diagonal.
			AbstractMatrix<T> Transpose()const
			{
				AbstractMatrix<T> result(m_cols, m_rows);

				for (auto r = 0; r < m_rows; ++r)
				{
					for (auto c = 0; c < m_cols; ++c)
					{
						result.m_values.at(result.indexOf(c, r)) = m_values.at(indexOf(r, c));
					}
				}
				return result;
			}

			AbstractMatrix<T> Power(int power)const
			{
				if (power >= -1)
				{
					switch (power)
					{
						case -1:
							//	Return the inverse, if there is one
							if (IsInvertible())
							{
								return Inverse();
							}
							else
							{
								fprintf(stderr, "Matrix is not invertible.\n");
								throw MAT_INVALID_OPERATION;
							}
						case 0:
							//	Return the identity matrix, if there is one
							if (IsSquare())
							{
								return Identity(m_rows);
							}
							else
							{
								fprintf(stderr, "Zeroth power is undefined for non-square matrices.\n");
								throw MAT_INVALID_OPERATION;
							}
						default:
							//	Return this multiplied by itself multiple times
							AbstractMatrix<T> result = *this;

							//	Note: from one not zero
							for (int i = 1; i < power; ++i)
							{
								result *= (*this);
							}
							return result;
					}
				}
				else
				{
					fprintf(stderr, "Operation is not currently implemented.\n");
					throw MAT_NOT_SUPPORTED;
				}
			}

			/**
		*  Return the cofactor of
		*  an individual element
		**/
			T Cofactor(const size_type& row, const size_type& col)const
			{
				AbstractMatrix<T> minorMatrix = MinorMatrix(row, col);

				//  Now we assign an appropriate sign to
				//  the cofactor matrix for that element
				//  Note: indices are running from 1..r+1,
				//	and 1..c+1, hence we test the opposite to
				//	what may be expected
				//
				//          col0    col1    col2    col3
				//  row0    +       -       +       -
				//  row1    -       +       -       +
				//  row2    +       -       +       -
				//  row3    -       +       -       +
				if ((row % 2) != 0)
				{
					if ((col % 2) != 0)
					{
						//  +
						//  Then this element of the cofactor matrix
						//  is the determinant of that 3x3 matrix
						return minorMatrix.Determinant();
					}
					else
					{
						//  -
						//  Then this element of the cofactor matrix
						//  is the determinant of that 3x3 negative matrix
						return minorMatrix.Negative().Determinant();
					}
				}
				else
				{
					if ((col % 2) != 0)
					{
						//  -
						//  Then this element of the cofactor matrix
						//  is the determinant of that 3x3 negative matrix
						return minorMatrix.Negative().Determinant();
					}
					else
					{
						//  +
						//  Then this element of the cofactor matrix
						//  is the determinant of that 3x3 matrix
						return minorMatrix.Determinant();
					}
				}
			}

			/**
		*  Swap the signs of all the
		*  elements in the given
		*  real matrix
		**/
			AbstractMatrix<T> Negative()const
			{
				AbstractMatrix<T> result = AbstractMatrix<T>(m_rows, m_cols);

				for (auto r = 0; r < m_rows; ++r)
				{
					for (auto c = 0; c < m_cols; ++c)
					{
						result.m_values.at(indexOf(r, c)) = -m_values.at(indexOf(r, c));
					}
				}
				return result;
			}

			/**
		*  Return a complete matrix
		*  of cofactors
		**/
			AbstractMatrix<T> CofactorMatrix() const
			{
				AbstractMatrix<T> result = AbstractMatrix<T>(m_rows, m_cols);

				for (auto r = 1; r < m_rows + 1; ++r)
				{
					for (auto c = 1; c < m_cols + 1; ++c)
					{
						result.m_values.at(indexOf(r - 1, c - 1)) = Cofactor(r, c);
					}
				}
				return result;
			}

			/**
		*  The adjoint is the transpose
		*  of the cofactor matrix
		**/
			AbstractMatrix<T> Adjoint() const
			{
				return CofactorMatrix().Transpose();
			}

			/**
		*  Invert the matrix by calculating the Adjoint
		*  and the determinant
		**/
			AbstractMatrix<T> Inverse() const
			{
				AbstractMatrix<T> adjointMatrix = Adjoint();
				T determinant = Determinant();

				return (adjointMatrix * (1.0f / determinant));
			}

			bool CanMultiply(const AbstractMatrix<T>& rhs) const
			{
				return (m_cols == rhs.m_rows);
			}

			///	Return the minor matrix of the matrix
			///
			///	Return a small matrix where the elements in
			///	the specified row and column are excluded.
			///
			///	@param row: the row of the matrix that will be
			///	excluded (one indexed)
			///	@param col: the column of the matrix that will
			///	be excluded (one indexed)
			///	@return the resultant minor matrix.
			AbstractMatrix<T> MinorMatrix(const size_type& row,
										  const size_type& col)const
			{
				assert((m_rows > 2) && (m_cols > 2));

				AbstractMatrix<T> result(m_rows - 1, m_cols - 1);

				//  Keep track of the row and column
				//  in the minor matrix to be written
				//  to
				unsigned int minorR = 0;
				unsigned int minorC = 0;

				//  Iterate through each row of the major
				//  matrix
				for (unsigned int r = 0; r < m_rows; ++r)
				{
					minorC = 0;

					//  Only copy the values if the
					//  row does not match the value given
					if (r != (row - 1))
					{
						//  Iterate through each column of the
						//  major matrix
						for (unsigned int c = 0; c < m_cols; ++c)
						{
							//  Only copy the values if the
							//  col does not match the value given
							if (c != (col - 1))
							{
								result.m_values.at(result.indexOf(minorR, minorC)) = m_values.at(indexOf(r, c));

								minorC++;
							}
						}
						minorR++;
					}
				}
				return result;
			}

			///	Create a sub matrix
			///
			///	Copy the selected rows and columns to form a submatrix.
			///
			///	@param left: the column index that will form the left
			///	of the sub matrix.
			///	@param top: the row index that will form the top
			///	of the sub matrix.
			///	@param right: the column index that will form the right
			///	of the sub matrix.
			///	@param bottom: the row index that will form the bottom
			///	of the sub matrix.
			///	@return Matrix: the submatrix.
			///	@throws MAT_INVALID_COL_INDEX, thrown if the left or
			///	right index given is invalid or if right < left.
			///	@throws MAT_INVALID_ROW_INDEX, thrown if the top or
			///	bottom index given is invalid or if bottom < top.
//			AbstractMatrix<T> SubMatrix(const unsigned int left, const unsigned int top, unsigned int right, unsigned int bottom)const
//			//void SubMatrix(const unsigned int left, const unsigned int top, unsigned int right, unsigned int bottom)const
//			{
//				if (RowIndexIsValid(top))
//				{
//					if ((RowIndexIsValid(bottom)) && (bottom >= top))
//					{
//						if (ColumnIndexIsValid(left))
//						{
//							if ((ColumnIndexIsValid(right)) && (right >= left))
//							{
//								unsigned int colsLeftDiscard = left - 1;
//								unsigned int colsRightDiscard = m_cols - right;
//								unsigned int colsInSubMatrix = m_cols - (colsLeftDiscard + colsRightDiscard);

//								unsigned int rowsAboveDiscard = top - 1;
//								unsigned int rowsBelowDiscard = m_rows - bottom;
//								unsigned int rowsInSubMatrix = m_rows - (rowsBelowDiscard + rowsAboveDiscard);

//								fprintf(stdout, "%i x %i\n", rowsInSubMatrix, colsInSubMatrix);

//								T* values = new T[rowsInSubMatrix*colsInSubMatrix];

//								unsigned int i = 0;

//								//	Copy the desired portion of the matrix
//								for (unsigned int r = top - 1; r < bottom; ++r)
//								{
//									for (unsigned int c = left - 1; c < right; ++c)
//									{
//										values[i] = at(indexOf(r, c));
//										++i;
//									}
//								}
//								return AbstractMatrix<T>(rowsInSubMatrix, colsInSubMatrix, values);
//							}
//							else
//							{
//								fprintf(stderr, MAT_INVALID_COL_INDEX_MSG, right);
//								throw MAT_INVALID_COL_INDEX;
//							}
//						}
//						else
//						{
//							fprintf(stderr, MAT_INVALID_COL_INDEX_MSG, left);
//							throw MAT_INVALID_COL_INDEX;
//						}
//					}
//					else
//					{
//						fprintf(stderr, MAT_INVALID_ROW_INDEX_MSG, bottom);
//						throw MAT_INVALID_ROW_INDEX;
//					}
//				}
//				else
//				{
//					fprintf(stderr, MAT_INVALID_ROW_INDEX_MSG, top);
//					throw MAT_INVALID_ROW_INDEX;
//				}
//			}

#pragma endregion Public Function (General)
#pragma region Public Functions (Square Matrix)

		public:
			///	Is the matrix square?
			///
			///	@return boolean: true if the number of columns
			///	and rows are equal
			bool IsSquare()const
			{
				return (m_rows == m_cols);
			}

			///	Is the matrix diagonal?
			///
			///	A square matrix is diagonal if it's only
			///	non-zero elements exist on the main diagonal.
			///	If any element off the main diagonal is not
			///	zero the result is false.  An example of a
			///	diagonal matrix is any identity matrix.
			///
			///	@return boolean - true if the matrix is
			///	diagonal.
			///	@throws MAT_INVALID_OPERATION raised if matrix is not square.
			bool IsDiagonalMatrix() const
			{
				bool result = true;

				if (IsSquare())
				{
					for (unsigned int r = 0; r < m_rows; ++r)
					{
						for (unsigned int c = 0; c < m_cols; ++c)
						{
							//	Don't check diagonal
							if (r != c)
							{
								//	Any non-zero values outside the
								//	diagonal give a negative result
								if (at(indexOf(r, c)) != 0.0)
									result = false;
							}
						}
					}
					return result;
				}
				else
				{
					fprintf(stderr, "Only square matrices can be diagonal.\n");
					throw MAT_INVALID_OPERATION;
				}
			}

			///	Is the matrix upper triangular?
			///
			///	A matrix is known as upper triangular if all non-zero
			///	entries exist on or above the main diagonal.
			///
			///	@return boolean - true if the matrix is upper diagonal.
			///	@throws MAT_INVALID_OPERATION raised if matrix is not square.
			bool IsUpperTriangularMatrix() const
			{
				bool result = true;

				if (IsSquare())
				{
					for (unsigned int r = 0; r < m_rows; ++r)
					{
						for (unsigned int c = 0; c < m_cols; ++c)
						{
							if (r > c)
							{
								//	Any non-zero values outside the
								//	diagonal give a negative result
								if (at(indexOf(r, c)) != 0.0)
									result = false;
							}
						}
					}
					return result;
				}
				else
				{
					fprintf(stderr, "Only square matrices can be triangular.\n");
					throw MAT_INVALID_OPERATION;
				}
			}

			///	Is the matrix strictly upper triangular?
			///
			///	A matrix is known as stricly upper triangular if all non-zero
			///	entries exist above the main diagonal.
			///
			///	@return boolean - true if the matrix is strictly upper diagonal.
			///	@throws MAT_INVALID_OPERATION raised if matrix is not square.
			bool IsStrictlyUpperTriangularMatrix() const
			{
				bool result = true;

				if (IsSquare())
				{
					for (unsigned int r = 0; r < m_rows; ++r)
					{
						for (unsigned int c = 0; c < m_cols; ++c)
						{
							if (r >= c)
							{
								//	Any non-zero values outside the
								//	diagonal give a negative result
								if (at(indexOf(r, c)) != 0.0)
									result = false;
							}
						}
					}
					return result;
				}
				else
				{
					fprintf(stderr, "Only square matrices can be triangular.\n");
					throw MAT_INVALID_OPERATION;
				}
			}

			///	Is the matrix lower triangular?
			///
			///	A matrix is known as lower triangular if all non-zero
			///	entries exist on or below the main diagonal.
			///
			///	@return boolean - true if the matrix is lower diagonal.
			///	@throws MAT_INVALID_OPERATION raised if matrix is not square.
			bool IsLowerTriangularMatrix() const
			{
				bool result = true;

				if (IsSquare())
				{
					for (unsigned int r = 0; r < m_rows; ++r)
					{
						for (unsigned int c = 0; c < m_cols; ++c)
						{
							if (r < c)
							{
								//	Any non-zero values outside the
								//	diagonal give a negative result
								if (at(indexOf(r, c)) != 0.0)
									result = false;
							}
						}
					}
					return result;
				}
				else
				{
					fprintf(stderr, "Only square matrices can be triangular.\n");
					throw MAT_INVALID_OPERATION;
				}
			}

			///	Is the matrix strictly lower triangular?
			///
			///	A matrix is known as stricly lower triangular if all non-zero
			///	entries exist below the main diagonal.
			///
			///	@return boolean - true if the matrix is strictly lower diagonal.
			///	@throws MAT_INVALID_OPERATION raised if matrix is not square.
			bool IsStrictlyLowerTriangularMatrix() const
			{
				bool result = true;

				if (IsSquare())
				{
					for (unsigned int r = 0; r < m_rows; ++r)
					{
						for (unsigned int c = 0; c < m_cols; ++c)
						{
							if (r <= c)
							{
								//	Any non-zero values outside the
								//	diagonal give a negative result
								if (at(indexOf(r, c)) != 0.0)
									result = false;
							}
						}
					}
					return result;
				}
				else
				{
					fprintf(stderr, "Only square matrices can be triangular.\n");
					throw MAT_INVALID_OPERATION;
				}
			}

			///	Is there a inverted form of the matrix defined?
			///
			///	Not all matrices are capable of being inverted.  First,
			///	non-square matrices don't have a defined inverse.  In the
			///	case of square matrices, if the determinant of the matrix is
			///	zero, no inverse is defined.
			///
			///	@return boolean - true if it is possible to return
			///	an inverse of the matrix.
			///	@throws MAT_INVALID_OPERATION raised if matrix is not square.
			inline bool IsInvertible() const
			{
				if (IsSquare())
				{
					return Determinant() != 0;
				}
				else
				{
					fprintf(stderr, "Inverse of a non-square matrix is undefined.\n");
					throw MAT_INVALID_OPERATION;
				}
			}

			///	Is the matrix Singular or Non-Singular?
			///
			///	A matrix is known as Singular if there is no
			///	inverse of it defined.
			///
			///	@return boolean - true if there is no
			///	defined inverse of the matrix.
			///	@throws MAT_INVALID_OPERATION raised if matrix is not square.
			inline bool IsSingular() const
			{
				if (IsSquare())
				{
					return !IsSingular();
				}
				else
				{
					fprintf(stderr, "Inverse of a non-square matrix is undefined.\n");
					throw MAT_INVALID_OPERATION;
				}
			}

			bool IsZeroDivisor() const
			{
				if (IsSquare())
				{
					//	A = this matrix
					//
					//	If any AB = 0 or BA = 0
					//	then it is a zero divisor.
					//	Note: non-singular matrices cannot be
					//	zero divisors.
					fprintf(stderr, "Not yet implemented.\n");
					throw MAT_NOT_SUPPORTED;
				}
				else
				{
					fprintf(stderr, "Only square matrices can be zero divisors.\n");
					throw MAT_INVALID_OPERATION;
				}
			}

			///	Get the trace of the matrix
			///
			///	Return the trace function of the matrix, that is
			///	the sum of the values on the diagonal of a
			///	square matrix.
			///
			///	@return a double precision floating point number
			///	 representing the trace of the matrix
			T Trace() const
			{
				assert(IsSquare());

				T result = 0.0;


				for (unsigned int r = 0; r < m_rows; ++r)
				{
					for (unsigned int c = 0; c < m_cols; ++c)
					{
						if (r == c)
						{
							result += m_values.at(indexOf(r, c));
						}
					}
				}

				return result;
			}

			///	Get the determinant of the matrix
			///
			/// Calculate the determinant of a square matrix.
			///	<b>Currently limited to a matrix of 4x4
			///	maximum</b>.
			///
			///	@return a double precision floating point number
			///	 that is the determinant of the matrix.
			T Determinant() const
			{
				assert(IsSquare());

				switch (m_cols)
				{
					case 2:
						return det2x2();
					case 3:
						return det3x3();
					case 4:
						return det4x4();
				}
			}

#pragma endregion Public Functions (Square Matrix)
#pragma region Public Functions (Columns)

			///	Return the matrix as a Column major array
			///
			///	Iteratrate through this matrix and output as
			///	a array of double precision floating point
			///	numbers ordered in column major format
			///
			///	@return: double* pointer to the start of
			///	the array
//			T* AsColumnOrderedArray() const
//			{
//				T* result = new T[m_rows*m_cols];
//				unsigned int element = 0;

//				for (unsigned int c = 0; c < m_cols; c++)
//				{
//					for (unsigned int r = 0; r < m_rows; r++)
//					{
//						result[element] = at(indexOf(r, c));

//						element++;
//					}
//				}
//				return result;
//			}

			///	Is the column number given valid?
			///
			///	Is the columnIndex given valid for this matrix?  Indicies
			///	run from 1..Cols.
			///
			///	@param columnIndex: the index to be checked.
			///	@return boolean, true if the given index exists in the matrix.
			bool ColumnIndexIsValid(const unsigned int columnIndex) const
			{
				return ((columnIndex >= 1) && (columnIndex <= m_cols));
			}

			///	Exchange the enteries in the specified columns.
			///
			///	Swap the entire row at rowA and rowB storing
			///	the result in a new matrix structure.
			///
			/// @param rowA: the first row to swap values.
			/// @param rowB: the second row to swap values.
			/// @return AbstractMatrix<ElementType>: this
			///	matrix after the rows have been swapped.
			AbstractMatrix<T> ExchangeColumns(const unsigned int columnA, const unsigned int columnB) const
			{
				//	First check the indices are valid
				if (ColumnIndexIsValid(columnA))
				{
					if (ColumnIndexIsValid(columnB))
					{
						AbstractMatrix<T> result = *this;

						//	Loop through the rows of the specified
						//	columns and swap the elements
						for (unsigned int r = 0; r < m_rows; ++r)
						{
							T temp = result.at(result.indexOf(r, columnA - 1));
							result.at(result.indexOf(r, columnA - 1)) = result.at(result.indexOf(r, columnB - 1));
							result.at(result.indexOf(r, columnB - 1)) = temp;
						}
						return result;
					}
					else
					{
						fprintf(stderr, "ColumnB index %i, is invalid.", columnB);
						throw MAT_INVALID_COL_INDEX;
					}
				}
				else
				{
					fprintf(stderr, "ColumnA index %i, is invalid.", columnA);
					throw MAT_INVALID_COL_INDEX;
				}
			}

			///	How many non-zero values are in the column
			///
			///	Counts the number of non-zero values in a column
			///
			///	@param col: the column to be checked.
			///	@return unisgned int: the total number of non-zero values.
			///	@throws MAT_INVALID_COL_INDEX, thrown if the index is
			///	not in range for the matrix.
			unsigned int NonZeroValueCountInColumn(unsigned int col) const
			{
				if (ColumnIndexIsValid(col))
				{
					unsigned int result = 0;

					for (unsigned int r = 0; r < m_rows; ++r)
					{
						if (!isZero(at(indexOf(r, col - 1))))
						{
							result++;
						}
					}
					return result;
				}
				else
				{
					fprintf(stderr, "Column index %i, is invalid.", col);
					throw MAT_INVALID_COL_INDEX;
				}
			}

			///	Create a submatrix by trimming rows
			///
			///	Creates a submatrix by trimming rows above the given
			///	row index.
			///
			///	@param row, the topmost row in the resultant matrix.
			///	@returns Matrix, a matrix with rows above row removed.
			///	@throws MAT_INVALID_ROW_INDEX, thrown if an invalid row
			///	index is given.
//			AbstractMatrix<T> DeleteRowsAbove(const unsigned int row) const
//			{
//				if (RowIndexIsValid(row))
//				{
//					//	-1 because we want to keep this row
//					unsigned int rowsToRemove = m_rows - row - 1;
//					unsigned int rowsInSubMatrix = m_rows - rowsToRemove;

//					T* values = new T[rowsInSubMatrix*m_cols];

//					unsigned int i = 0;

//					//	-1 because we want to keep this row
//					for (unsigned int r = row - 1; r < m_rows; ++r)
//					{
//						for (unsigned int c = 0; c < m_cols; ++c)
//						{
//							values[i] = at(indexOf(r, c));
//							++i;
//						}
//					}
//					return AbstractMatrix<T>(rowsInSubMatrix, m_cols, values);
//				}
//				else
//				{
//					fprintf(stderr, MAT_INVALID_ROW_INDEX_MSG, row);
//					throw MAT_INVALID_ROW_INDEX;
//				}
//			}

			///	Create a submatrix by trimming rows
			///
			///	Creates a submatrix by trimming rows below the given
			///	row index.
			///
			///	@param row, the bottommost row in the resultant matrix.
			///	@returns Matrix, a matrix with rows below row removed.
			///	@throws MAT_INVALID_ROW_INDEX, thrown if an invalid row
			///	index is given.
//			AbstractMatrix<T> DeleteRowsBelow(const unsigned int row)const
//			{
//				if (RowIndexIsValid(row))
//				{
//					unsigned int rowsToRemove = m_rows - row;
//					unsigned int rowsInSubMatrix = m_rows - rowsToRemove;

//					T* values = new T[rowsInSubMatrix*m_cols];

//					unsigned int i = 0;

//					for (unsigned int r = 0; r < (m_rows - rowsToRemove); ++r)
//					{
//						for (unsigned int c = 0; c < m_cols; ++c)
//						{
//							values[i] = at(indexOf(r, c));
//							++i;
//						}
//					}
//					return AbstractMatrix<T>(rowsInSubMatrix, m_cols, values);
//				}
//				else
//				{
//					fprintf(stderr, MAT_INVALID_ROW_INDEX_MSG, row);
//					throw MAT_INVALID_ROW_INDEX;
//				}
//			}

#pragma endregion Public Functions (Columns)
#pragma region Public Functions (Rows)

			///	Is the row number given valid?
			///
			///	Is the rowIndex given valid for this matrix?  Indicies
			///	run from 1..Rows.
			///
			///	@param rowIndex: the index to be checked.
			///	@return boolean, true if the given index exists in the matrix.
			bool RowIndexIsValid(const unsigned int rowIndex)const
			{
				return ((rowIndex >= 1) && (rowIndex <= m_rows));
			}

			///	Return the matrix as a Row major array
			///
			///	Iteratrate through this matrix and output as
			///	a array of double precision floating point
			///	numbers ordered in row major format
			///
			///	@return: double* pointer to the start of
			///	the array
//			T* AsRowOrderedArray() const
//			{
//				T* result = new T[m_rows*m_cols];
//				unsigned int element = 0;

//				for (unsigned int r = 0; r < m_rows; r++)
//				{
//					for (unsigned int c = 0; c < m_cols; c++)
//					{
//						result[element] = m_values.at(indexOf(r, c));

//						element++;
//					}
//				}
//				return result;
//			}

			///	Return the given row of the Matrix
			///
			///	Returns all the values in the matrix with
			///	the given row index.
			///
			///	@param row: the row to be returned from the
			///	matrix.
			///	@return AbstractMatrix<ElementType>: a RowMatrix of values
			///	copied from the original matrix.
			///	@throws MAT_INVALID_ROW_INDEX if the index
			///	is zero or is greater than the number of rows
			///	in the matrix.
//			AbstractMatrix<T> RowWithIndex(unsigned int row)const
//			{
//				assert(row >= 0 && row < m_rows);

//				T* values = new T[m_cols];

//				//	Copy the values of the given row
//				for (unsigned int col = 0; col < m_cols; ++col)
//				{
//					//	Note: row is one indexed (from user)
//					//	and col is zero indexed (internal index)
//					values[col] = m_values.at(indexOf(row - 1, col));
//				}

//				//	Construct a row matrix result
//				return AbstractMatrix<T>(1, m_cols, values);

//			}

			///	Find the first non-zero value in a row
			///
			///	Return the <b>index</b> of the first non-zero
			///	entry in a row of the matrix
			///
			///	@param row: the row to be checked.
			///	@return integer: the index of the first non-zero entry
			///	or zero if all entries in a row are zero.
			///	@throws MAT_INVALID_ROW_INDEX if the index
			///	is zero or is greater than the number of rows
			///	in the matrix.
			unsigned int LeadingEntryIndexOfRow(unsigned int row) const
			{
				if ((row <= m_rows) && (row != 0))
				{
					//	Copy the desired row
					AbstractMatrix<T> temp = RowWithIndex(row);

					//	Loop through and return the index
					//	of the first non-zero value in the row
					for (unsigned int col = 0; col < temp.m_cols; ++col)
					{
						if (!isZero(temp.at(col)))
						{
							//	Result needs to be one indexed
							return col + 1;
						}
					}
					//	Else return zero
					return 0;
				}
				else
				{
					fprintf(stderr, "Row index is invalid.\n");
					throw MAT_INVALID_ROW_INDEX;
				}
			}

			///	Find the first non-zero value in a row
			///
			///	Return the <b>value</b> of the first non-zero
			///	entry in a row of the matrix
			///
			///	@param row: the row to be checked.
			///	@return double: the value of the first non-zero entry
			///	or zero if all entries in a row are zero.
			///	@throws MAT_INVALID_ROW_INDEX if the index
			///	is zero or is greater than the number of rows
			///	in the matrix.
			T LeadingEntryValueOfRow(unsigned int row)const
			{
				if ((row <= m_rows) && (row != 0))
				{
					//	Copy the desired row
					AbstractMatrix<T> temp = RowWithIndex(row);

					//	Loop through and return the first
					//	non-zero value in the row
					for (unsigned int col = 0; col < temp.m_cols; ++col)
					{
						if (!isZero(temp.at(col)))
						{
							return (temp.at(col));
						}
					}
					//	Else return zero
					return 0;
				}
				else
				{
					fprintf(stderr, "Row index is invalid.\n");
					throw MAT_INVALID_ROW_INDEX;
				}
			}

			///	Exchange the enteries in the specified rows.
			///
			///	Swap the entire row at rowA and rowB storing
			///	the result in this matrix structure.
			///
			/// @param rowA: the first row to swap values.
			/// @param rowB: the second row to swap values.
			/// @return AbstractMatrix<ElementType>: this
			///	matrix after the rows have been swapped.
			AbstractMatrix<T> ExchangeRows(const unsigned int rowA, const unsigned int rowB)
			{
				//	First check the indices are valid
				if (RowIndexIsValid(rowA))
				{
					if (RowIndexIsValid(rowB))
					{
						AbstractMatrix<T> result = *this;

						//	Loop through the cols of the specified
						//	rows and swap the elements
						for (unsigned int c = 0; c < m_cols; ++c)
						{
							T temp = result.at(result.indexOf(rowA - 1, c));
							result.at(result.indexOf(rowA - 1, c)) = result.at(result.indexOf(rowB - 1, c));
							result.at(result.indexOf(rowB - 1, c)) = temp;
						}
						return result;
					}
					else
					{
						fprintf(stderr, "RowB index %i, is invalid.", rowB);
						throw MAT_INVALID_ROW_INDEX;
					}
				}
				else
				{
					fprintf(stderr, "RowA index %i, is invalid.", rowA);
					throw MAT_INVALID_ROW_INDEX;
				}
			}

			///	Multiply a single row by the given value
			///
			///	Multiply each value in a row by a value
			///	matching the element type of the matrix.
			///	<b>Note: multiplication by
			///	a different number type
			///	is defined in derived classes</b>.
			///
			///	@param row: the row to be multiplied.
			///	@param multiplier: the non-zero value to multiply the row
			///	by.
			///	@return AbstractMatrix<ElementType>: the matrix after the row
			///	multiplication applied.
			///	@throws MAT_INVALID_ROW_INDEX: if the row number is
			///	invalid.
			/// @throws MAT_INVALID_OPERATION: if the multiplier is zero.
			AbstractMatrix<T> MultiplyRow(const unsigned int row, const T multiplier)
			{
				if (RowIndexIsValid(row))
				{
					if (!isZero(multiplier))
					{
						AbstractMatrix<T> result = *this;

						for (unsigned int c = 0; c < m_cols; ++c)
						{
							result.at(result.indexOf(row - 1, c)) *= multiplier;
						}
						return result;
					}
					else
					{
						fprintf(stderr, "Row multiplier cannot be zero.");
						throw MAT_INVALID_OPERATION;
					}
				}
				else
				{
					fprintf(stderr, "Row index %i, is invalid.", row);
					throw MAT_INVALID_ROW_INDEX;
				}
			}

			///	Add's the scalar multiple of a single row to another row.
			///
			///	Multiples the source row by a scalar value, then adds
			///	the result to the destination row.
			///
			///	@param destinationRow: the index of the row to be modified.
			///	@param sourceRow: the index of the row containing the values
			///	that will be multiplied.
			///	@param sourceRowMultiplier: the scalar value to multiply
			///	the source row with.
			///	@return Matrix: the result of the matrix once the operation
			///	has been completed.
			///	@throws MAT_INVALID_OPERATION: thrown if the multiplier
			///	is zero.
			///	@throws  MAT_INVALID_ROW_INDEX: thrown if the row indices
			///	are invalid or not unique.
			AbstractMatrix<T> AddScalarMultipleOfRow(const unsigned int destinationRow, const unsigned int sourceRow, const T sourceRowMultiplier)
			{
				//	First check the indices are valid
				if (RowIndexIsValid(destinationRow))
				{
					if (RowIndexIsValid(sourceRow))
					{
						if (destinationRow != sourceRow)
						{
							if (!isZero(sourceRowMultiplier))
							{
								//	Copy the source row
								AbstractMatrix<T> sourceRowElements = RowWithIndex(sourceRow);

								//	Multiply the source row
								sourceRowElements *= sourceRowMultiplier;

								//	Copy original matrix
								AbstractMatrix<T> result = *this;

								//	Add the content of the result and modified source row
								for (unsigned int c = 0; c < m_cols; ++c)
								{
									result.at(result.indexOf(destinationRow - 1, c)) += sourceRowElements.at(sourceRowElements.indexOf(0, c));
								}
								return result;
							}
							else
							{
								fprintf(stderr, "Row multiplier cannot be zero.\n");
								throw MAT_INVALID_OPERATION;
							}
						}
						else
						{
							fprintf(stderr, "Row indices must be unique.\n");
							throw MAT_INVALID_ROW_INDEX;
						}
					}
					else
					{
						fprintf(stderr, "Source row index %i, is invalid.\n", sourceRow);
						throw MAT_INVALID_ROW_INDEX;
					}
				}
				else
				{
					fprintf(stderr, "Destination row index %i, is invalid.\n", destinationRow);
					throw MAT_INVALID_ROW_INDEX;
				}
			}

			///	Is the matrix in row echelon form?
			///
			///	Row echelon form means the following conditions are
			///	met:<br>
			///	1.	Non-zero rows are at the top of the matrix<br>
			///	2.	Leading entries increase as the we move down
			///	the matrix<br>
			///	3.	Lead entries are always 1.0<br>
			///	4.	Columns containing lead entries has all
			///	other entries of zero<br>
			///
			///	@return boolean, true if all the conditions are met.
//			bool IsInRowEchelonForm() const
//			{
//				unsigned int* leadingEntryIndices = new unsigned int[m_rows];
//				T* leadingEntryValues = new T[m_rows];
//				vector<unsigned int> columnsWithLeadValues;

//				//	Calculate properties of the matrix
//				//	List the indices and values of the leading
//				//	entries for each row.  Also store the
//				//	number of columns with lead entries.
//				for (unsigned int r = 1; r < m_rows + 1; ++r)
//				{
//					leadingEntryIndices[r - 1] = LeadingEntryIndexOfRow(r);
//					leadingEntryValues[r - 1] = LeadingEntryValueOfRow(r);

//					//	Add the column number to the list of colums with
//					//	lead entries - a zero value means no lead entries
//					if (leadingEntryIndices[r - 1] != 0)
//					{
//						columnsWithLeadValues.push_back(leadingEntryIndices[r - 1]);
//					}
//				}

//				//	1.	Check the non-zero entries are at the top of the row
//				bool zeroRow = false;

//				for (unsigned int r = 0; r < m_rows; ++r)
//				{
//					if (leadingEntryIndices[r] > 0)
//					{
//						if (zeroRow)
//						{
//							//	Row contains non-zero values
//							//	and we've already seen a zero row
//							//	so fail the test
//							return false;
//						}
//					}
//					else
//					{
//						//	Row contains all zeroes
//						//	set that we have seen a zero
//						//	row
//						zeroRow = true;
//					}
//				}

//				//	2.	Leading entries move across the matrix
//				unsigned int maxCol = 0;

//				for (unsigned int r = 0; r < m_rows; ++r)
//				{
//					if (leadingEntryIndices[r] > maxCol)
//					{
//						//	Lead entry index is bigger
//						//	than the previous
//						maxCol = leadingEntryIndices[r];
//					}
//					else
//					{
//						//	Lead entry index is not bigger
//						//	than the previous, so fail if
//						//	it isn't zero
//						if (leadingEntryIndices[r] != 0)
//						{
//							return false;
//						}
//					}
//				}

//				//	3.	All lead entries are one
//				for (unsigned int r = 0; r < m_rows; ++r)
//				{
//					if (!(isOne(leadingEntryValues[r]) || isZero(leadingEntryValues[r])))
//					{
//						return false;
//					}
//				}

//				//	4.	Columns containing lead entries has all
//				//	other entries of zero
//				for (unsigned int i = 0; i < columnsWithLeadValues.size(); ++i)
//				{
//					if (NonZeroValueCountInColumn(columnsWithLeadValues.at(i)) > 1)
//					{
//						return false;
//					}
//				}
//				return true;
//			}

			///	Create a submatrix by trimming columns
			///
			///	Creates a submatrix by trimming columns left of the given
			///	column index.
			///
			///	@param col, the leftmost column in the resultant matrix.
			///	@returns Matrix, a matrix with columns left of col removed.
			///	@throws MAT_INVALID_COL_INDEX, thrown if an invalid column
			///	index is given.
//			AbstractMatrix<T> DeleteColumnsLeftOf(const unsigned int col)const
//			{
//				if (ColumnIndexIsValid(col))
//				{
//					//	-1 because we want to keep this column
//					unsigned int colsToRemove = m_cols - col - 1;
//					unsigned int colsInSubMatrix = m_cols - colsToRemove;

//					T* values = new T[m_rows*colsInSubMatrix];

//					unsigned int i = 0;

//					for (unsigned int r = 0; r < m_rows; ++r)
//					{
//						//	-1 because we want to keep this column
//						for (unsigned int c = col - 1; c < m_cols; ++c)
//						{
//							values[i] = at(indexOf(r, c));
//							++i;
//						}
//					}
//					return AbstractMatrix<T>(m_rows, colsInSubMatrix, values);
//				}
//				else
//				{
//					fprintf(stderr, MAT_INVALID_COL_INDEX_MSG, col);
//					throw MAT_INVALID_COL_INDEX;
//				}
//			}

			///	Create a submatrix by trimming columns
			///
			///	Creates a submatrix by trimming columns right of the given
			///	column index.
			///
			///	@param col, the rightmost column in the resultant matrix.
			///	@returns Matrix, a matrix with columns right of col removed.
			///	@throws MAT_INVALID_COL_INDEX, thrown if an invalid column
			///	index is given.
//			AbstractMatrix<T> DeleteColumnsRightOf(const unsigned int col)const
//			{
//				if (ColumnIndexIsValid(col))
//				{
//					unsigned int colsToRemove = m_cols - col;
//					unsigned int colsInSubMatrix = m_cols - colsToRemove;

//					T* values = new T[m_rows*colsInSubMatrix];

//					unsigned int i = 0;

//					for (unsigned int r = 0; r < m_rows; ++r)
//					{
//						for (unsigned int c = 0; c < (m_cols - colsToRemove); ++c)
//						{
//							values[i] = at(indexOf(r, c));
//							++i;
//						}
//					}
//					return AbstractMatrix<T>(m_rows, colsInSubMatrix, values);
//				}
//				else
//				{
//					fprintf(stderr, MAT_INVALID_COL_INDEX_MSG, col);
//					throw MAT_INVALID_COL_INDEX;
//				}
//			}

#pragma endregion Public Functions (Rows)
#pragma region Constructors and Destructors
		public:
			///	Default constructor
			///
			///	Create a new AbstractMatrix<ElementType> with empty elements
			///	of size (0x0)
			///
			/// \since	?
			/// \author	Dean
			/// \author	Dean: change to initialize to 0*0 [26-10-2015]
			///
			AbstractMatrix<T>()
				: AbstractMatrix<T>(0, 0) {	}

			///	Override constructor
			///
			///	Create a new AbstractMatrix<ElementType> with empty elements
			///	of the specified size
			///
			///	\param	rows: number of rows
			///	\param	cols: number of cols
			/// \since	?
			/// \author	Dean
			/// \author	Dean: make 0*0 matrices allowable [26-10-2015]
			/// \author Dean: modify to become standard initializer [26-10-2015]
			///
			AbstractMatrix<T>(const size_type& rows, const size_type& cols)
			{
				m_rows = rows;
				m_cols = cols;

				//	Create blank complex elements for the
				//	data.
				m_values.resize(m_rows * m_cols);

				//	Initialize random number generator
				srand((unsigned)time(NULL));
			}

			///
			/// \brief		Copy contructor
			/// \param rhs
			/// \since		26-10-2015
			/// \author		Dean
			///
			AbstractMatrix<T>(const AbstractMatrix<T>& rhs)
				: AbstractMatrix<T>(rhs.m_rows, rhs.m_cols)
			{
				//	Make sure the matrix sizes match
				assert(m_values.size() == rhs.m_values.size());

				//	Clone the values
				std::copy(rhs.m_values.cbegin(),
						  rhs.m_values.cend(),
						  m_values.begin());
			}

			AbstractMatrix<T>(const size_type& rows,
							  const size_type& cols,
							  const T& value)
				: AbstractMatrix<T>(rows, cols)
			{
				assert (m_values.size() == (rows * cols));

				std::fill(m_values.begin(), m_values.end(), value);
			}

			///	Override constructor
			///
			///	Create a new AbstractMatrix<ElementType> and fill the elements
			///	with the values specified in the passed array
			///
			///	@param rows: number of rows (must be >0)
			///	@param cols: number of cols (must be >0)
			///	@param values: a 1D array of values, specified row
			///	by row
//			AbstractMatrix<T>(const size_type& rows, const size_type& cols, const T values[])
//			{
//				assert(rows != 0);
//				assert(cols != 0);

//				init(rows, cols);
//				fill(rows * cols, values);
//			}

			///	Override constructor
			///
			///	Create a new AbstractMatrix<ElementType> and fill the elements
			///	with the values specified in the passed vector
			///
			///	@param rows: number of rows (must be >1)
			///	@param cols: number of cols (must be >1)
			///	@param values: a vecotr of values, specified row
			///	by row.  Size must equal <i>rows * cols</i>.
			///
			AbstractMatrix<T>(const size_type& rows,
							  const size_type& cols,
							  const std::vector<T> values)
				: AbstractMatrix<T>(rows, cols)
			{
				assert(m_values.size() == (rows * cols));
				assert(values.size() == (rows * cols));

				//	Clone the values
				std::copy(values.cbegin(),
						  values.cend(),
						  m_values.begin());
			}

			///	Override constructor
			///
			///	Create a new 2x2 AbstractMatrix<ElementType> and fill the elements
			///	with the values specified
			///
			///	@param e11..e22: double precision floating point
			///	value at each position on the matrix.
			/// \since		?
			/// \author		Dean
			/// \author		Dean: modify to use standard initialization
			///				constructor [26-10-2015]
			///
			AbstractMatrix<T>(const T& e11, const T& e12,
							  const T& e21, const T& e22)
				: AbstractMatrix<T>(2, 2)
			{
				assert(m_values.size() == 4);

				m_values[0] = e11;
				m_values[1] = e12;
				m_values[2] = e21;
				m_values[3] = e22;
			}

			///	Override constructor
			///
			///	Create a new 3x3 AbstractMatrix<ElementType> and fill the elements
			///	with the values specified
			///
			///	@param e11..e33: double precision floating point
			///	value at each position on the matrix.
			/// \since		?
			/// \author		Dean
			/// \author		Dean: modify to use standard initialization
			///				constructor [26-10-2015]
			///
			AbstractMatrix<T>(const T& e11, const T& e12, const T& e13,
							  const T& e21, const T& e22, const T& e23,
							  const T& e31, const T& e32, const T& e33)
				: AbstractMatrix<T>(3, 3)
			{
				assert(m_values.size() == 9);

				m_values[0] = e11;
				m_values[1] = e12;
				m_values[2] = e13;
				m_values[3] = e21;
				m_values[4] = e22;
				m_values[5] = e23;
				m_values[6] = e31;
				m_values[7] = e32;
				m_values[8] = e33;
			}

			///	Override constructor
			///
			///	Create a new 4x4 AbstractMatrix<ElementType> and fill the elements
			///	with the values specified
			///
			///	@param e11..e44: double precision floating point
			///	value at each position on the matrix.
			AbstractMatrix<T>(const T& e11, const T& e12, const T& e13, const T& e14,
							  const T& e21, const T& e22, const T& e23, const T& e24,
							  const T& e31, const T& e32, const T& e33, const T& e34,
							  const T& e41, const T& e42, const T& e43, const T& e44)
				: AbstractMatrix<T>(4, 4)
			{
				assert(m_values.size() == 16);

				m_values[ 0] = e11;
				m_values[ 1] = e12;
				m_values[ 2] = e13;
				m_values[ 3] = e14;
				m_values[ 4] = e21;
				m_values[ 5] = e22;
				m_values[ 6] = e23;
				m_values[ 7] = e24;
				m_values[ 8] = e31;
				m_values[ 9] = e32;
				m_values[10] = e33;
				m_values[11] = e34;
				m_values[12] = e41;
				m_values[13] = e42;
				m_values[14] = e43;
				m_values[15] = e44;
			}

			///	Destructor
			~AbstractMatrix<T>(void)
			{
			}
#pragma endregion Constructors and Destructors
#pragma region Member Operators (Accessors / equality)

		public:
			///	Accessor operator (write)
			///
			///	Access elements of the matrix using a row and column
			///	index pair.  This is to be used instead of the conventional
			///	[int] accessor method which is meaningless in this context.
			///
			///	@param pair<int,int> a pair of integers representing the row
			/// and column indices of the element (running from 1..n, 1..m).
			///	To access element r,c of the matrix call as follows:
			///	<i>myMatrix[{r,c}]</i>.
			///	@return an element of the matrix with the given row and
			///	column index, with write access.
			T& operator[](pair<int, int> index)
			{
				return m_values.at(indexOf((index.first - 1), (index.second - 1)));
			}

			///	Accessor operator (read only)
			///
			///	Access elements of the matrix using a row and column
			///	index pair.  This is to be used instead of the conventional
			///	[int] accessor method which is meaningless in this context.
			///
			///	@param pair<int,int> a pair of integers representing the row
			/// and column indices of the element (running from 1..n, 1..m).
			///	To access element r,c of the matrix call as follows:
			///	<i>myMatrix[{r,c}]</i>.
			///	@return an element of the matrix with the given row and
			///	column index, with read-only access.
			T operator[](pair<int, int> index) const
			{
				return at(indexOf((index.first - 1), (index.second - 1)));
			}

			///	Assignment operator
			///
			///	Assign this Matrix the contents of another
			///
			///	@param rhs: the Matrix to be copied
			///	@return: a Matrix clone of the original data
			AbstractMatrix<T>& operator =(const AbstractMatrix<T>& rhs)
			{
				for (unsigned int i = 0; i < rhs.m_values.size(); ++i)
				{
					m_values.at(i) = rhs.m_values.at(i);
				}
				return *this;
			}

			///	Equality operator
			///
			///	Checks if two Matrices of the same element type are equal
			///
			///	@param rhs: the matrix to check this against
			///	@result: true if the two matrices are the same
			bool operator == (const AbstractMatrix<T>& rhs)const
			{
				if (m_values.size() != rhs.m_values.size())
				{
					return false;
				}
				else
				{
					for (unsigned int i = 0; i < m_values.size(); ++i)
					{
						if (m_values.at(i) != rhs.m_values.at(i))
						{
							return false;
						}
					}
					return true;
				}
			}

			///	Un-equality operator
			///
			///	Checks if two RealMatrices are different
			///
			///	@param rhs: the matrix to check this against
			///	@result: true if the two matrices are the different
			bool operator != (const AbstractMatrix<T>& rhs)const
			{
				if (size() != rhs.size())
				{
					return true;
				}
				else
				{
					for (unsigned int i = 0; i < size(); ++i)
					{
						if (at(i) != rhs.at(i))
						{
							return true;
						}
					}
					return false;
				}
			}

#pragma endregion Member Operators(Accessors / equality)
#pragma region Member Operators (Matrix-Matrix)

			///	Matrix-Matrix addition
			///
			///	Add this Matrix to another of the same element type
			///
			///	@param rhs: the matrix to be added with
			///	@result: a Matrix that is the result of summing
			///	two Matrices together
			AbstractMatrix<T> operator +(const AbstractMatrix<T>& rhs)const
			{
				if ((m_rows == rhs.m_rows) && (m_cols == rhs.m_cols))
				{
					AbstractMatrix<T> result(m_rows, m_cols);

					for (unsigned int i = 0; i < m_rows * m_cols; ++i)
					{
						result.at(i) = at(i) + rhs.at(i);
					}
					return result;
				}
				else
				{
					fprintf(stderr, "Dimensions of matrices do not match.\n");
					throw MAT_INVALID_OPERATION;
				}
			}

			///	Matrix-Matrix addition
			///
			///	Add this Matrix to another of the same element type
			///	and return in this variables.
			///
			///	@param rhs: the matrix to be added with
			///	@result: a Matrix that is the result of summing
			///	two Matrices together, stored in this variable.
			AbstractMatrix<T>& operator +=(const AbstractMatrix<T>& rhs)
			{
				assert((m_rows == rhs.m_rows) && (m_cols == rhs.m_cols));

				for (unsigned int i = 0; i < m_rows * m_cols; ++i)
				{
					m_values.at(i) += rhs.m_values.at(i);
				}
				return *this;
			}

			///	Matrix-Matrix subtraction
			///
			///	Subtract a matrix of the same element type
			///	from this matrix.
			///
			///	@param rhs: the matrix to be subtracted from this.
			///	@result: a Matrix that is the result of matrix
			///	subtraction.
			AbstractMatrix<T> operator -(const AbstractMatrix<T>& rhs)const
			{
				assert((m_rows == rhs.m_rows) && (m_cols == rhs.m_cols));

				AbstractMatrix<T> result(m_rows, m_cols);

				for (unsigned int i = 0; i < m_rows * m_cols; ++i)
				{
					result.m_values.at(i) = m_values.at(i) - rhs.m_values.at(i);
				}
				return result;
			}

			///	Matrix-Matrix subtraction
			///
			///	Subtract a matrix of the same element type
			///	from this matrix and store in this variable.
			///
			///	@param rhs: the matrix to be subtracted from this.
			///	@result: a Matrix that is the result of matrix
			///	subtraction, stored in this variable.
			AbstractMatrix<T>& operator -=(const AbstractMatrix<T>& rhs)
			{
				if ((m_rows == rhs.m_rows) && (m_cols == rhs.m_cols))
				{
					for (unsigned int i = 0; i < m_rows * m_cols; ++i)
					{
						at(i) -= rhs.at(i);
					}
					return *this;
				}
				else
				{
					fprintf(stderr, "Dimensions of matrices do not match.\n");
					throw MAT_INVALID_OPERATION;
				}
			}

			///	Matrix-Matrix multiplication
			///
			///	Multiply this Matrix with another of the same element type
			///
			///	@param rhs: the matrix to be multiplied by
			///	@result: a Matrix that is the result of mutliplying
			///	two Matrices together
			AbstractMatrix<T> operator* (const AbstractMatrix<T>& rhs)const
			{
				assert(CanMultiply(rhs));
				const AbstractMatrix<T> lhs = (*this);
				//	Note the following
				//	==================
				//
				//	1.	If lhs is an m*n matrix and rhs is an n*p matrix
				//		the resultant will be a m*p matrix.  If this is the
				//		case rhs*lhs has a defined answer and lhs*rhs does
				//		not.
				//	2.	Row vector * matrix = row vector
				//	3.	matrix * column vector = column vector
				//	4.	row vector * columne vector = scalar
				//	5.	column vector * row vector = matrix
				const auto m = lhs.m_rows;
				const auto n1 = lhs.m_cols;
				//int n2 = rhs.m_rows;
				const auto p = rhs.m_cols;

				AbstractMatrix<T> result(m, p);

				for (int i = 0; i < m; ++i)
				{
					for (int j = 0; j < p; ++j)
					{
						for (int k = 0; k < n1; ++k)
						{
							//  Element at [r,c] is the sum of the
							//  products of a row and column
							//  with the same index
							result.m_values.at(result.indexOf(i, j)) += lhs.m_values.at(lhs.indexOf(i, k)) * rhs.m_values.at(rhs.indexOf(k, j));
						}
					}
				}

				return result;
			}

			///	Matrix-Matrix multiplication
			///
			///	Multiply this Matrix with another of the same element type
			///
			///	@param rhs: the matrix to be multiplied by
			///	@result: a Matrix that is the result of mutliplying
			///	two Matrices together
			AbstractMatrix<T>& operator *=(const AbstractMatrix<T>& rhs)
			{
				const AbstractMatrix<T> lhs = (*this);
				//	Note the following
				//	==================
				//
				//	1.	If lhs is an m*n matrix and rhs is an n*p matrix
				//		the resultant will be a m*p matrix.  If this is the
				//		case rhs*lhs has a defined answer and lhs*rhs does
				//		not.
				//	2.	Row vector * matrix = row vector
				//	3.	matrix * column vector = column vector
				//	4.	row vector * columne vector = scalar
				//	5.	column vector * row vector = matrix
				const auto m = lhs.m_rows;
				const auto n1 = lhs.m_cols;
				//const auto n2 = rhs.m_rows;
				const auto p = rhs.m_cols;

				assert(CanMultiply(rhs));

				setSizeTo(m, p);

				for (unsigned int i = 0; i < m; ++i)
				{
					for (unsigned int j = 0; j < p; ++j)
					{
						for (unsigned int k = 0; k < n1; ++k)
						{
							//  Element at [r,c] is the sum of the
							//  products of a row and column
							//  with the same index
							m_values.at(indexOf(i, j)) += lhs.m_values.at(lhs.indexOf(i, k)) * rhs.m_values.at(rhs.indexOf(k, j));
						}
					}
				}
				return *this;
			}

#pragma endregion Member Operators (Matrix-Matrix)
#pragma region Member Operators (Matrix-Scalar)

			AbstractMatrix<T> operator +(const T& rhs)const
			{
				AbstractMatrix<T> result(m_rows, m_cols);

				for (unsigned int i = 0; i < m_rows * m_cols; ++i)
				{
					result.at(i) = at(i) + rhs;
				}
				return result;
			}

			AbstractMatrix<T>& operator +=(const T& rhs)
			{
				for (unsigned int i = 0; i < m_rows * m_cols; ++i)
				{
					at(i) += rhs;
				}
				return *this;
			}

			AbstractMatrix<T> operator -(const T& rhs)const
			{
				AbstractMatrix<T> result(m_rows, m_cols);

				for (unsigned int i = 0; i < m_rows * m_cols; ++i)
				{
					result.at(i) = at(i) - rhs;
				}
				return result;
			}

			AbstractMatrix<T>& operator -=(const T& rhs)
			{
				for (unsigned int i = 0; i < m_rows * m_cols; ++i)
				{
					at(i) -= rhs;
				}
				return *this;
			}

			///	AbstractMatrix<ElementType>-Scalar multiplication
			///
			///	Multiply this AbstractMatrix<ElementType> by a scalar value
			///
			///	@param rhs: the scalar value to be multiplied by
			///	@result: a AbstractMatrix<ElementType> that is the result of mutliplying
			///	a AbstractMatrix<ElementType> and a scalar value together
			AbstractMatrix<T> operator *(const T& rhs) const
			{
				std::vector<T> values;
				values.reserve(m_rows * m_cols);

				//	Access each element linearly
				for (unsigned int i = 0; i < m_values.size(); i++)
				{
					values[i] = m_values.at(i) * rhs;
				}
				return AbstractMatrix<T>(m_rows, m_cols, values);
			}

			///	AbstractMatrix<ElementType>-Scalar multiplication
			///
			///	Multiply this AbstractMatrix<ElementType> with by a Scalar value
			///	and store the result in this variable
			///
			///	@param rhs: the scalar value to be multiplied by
			///	@result: a AbstractMatrix<ElementType> that is the result of mutliplying
			///	a AbstractMatrix<ElementType> and a scalar value together, stored in
			///	this variable
			AbstractMatrix<T>& operator *=(const T& rhs)
			{
				//	Access each element linearly
				for (unsigned int i = 0; i < m_values.size(); i++)
				{
					m_values.at(i) *= rhs;
				}
				return *this;
			}

#pragma endregion Member Operators (Matrix-Scalar)
#pragma region Static functions

		public:
			///	Return a identity matrix
			///
			///	Return a square identity matrix of the specified order.
			///
			///	@param order: the width and height of the matrix
			///	@return: a AbstractMatrix<ElementType> where all values on the
			///	diagonal are equal to <i>1</i>.  All other values
			///	equal <i>0</i>.
			static AbstractMatrix<T> Identity(int order)
			{
				assert(order > 1);
				AbstractMatrix<T> result(order, order);

				for (int i = 0; i < order; ++i)
				{
					result.m_values.at(result.indexOf(i, i)) = 1;
				}
				return result;
			}

			///	Return a matrix of zeroes
			///
			///	Return a square matrix of the specified order where
			///	each element is initialised to <i>0</i>.
			///
			///	@param order: the width and height of the matrix
			///	@return: a AbstractMatrix<ElementType> where all values
			///	are equal to <i>0</i>.
			static AbstractMatrix<T> Zeroes(int order)
			{
				if (order > 1)
				{
					AbstractMatrix<T> result(order, order);

					return result;
				}
				else
				{
					fprintf(stderr, "Invalid dimension for matrix specified.  Must be 2x2 or larger.\n");
					throw MAT_INVALID_DIMENSIONALITY;
				}
			}

			///	Return a matrix of ones
			///
			///	Return a square matrix of the specified order where
			///	each element is initialised to <i>1</i>.
			///
			///	@param order: the width and height of the matrix
			///	@return: a AbstractMatrix<ElementType> where all values
			///	are equal to <i>1</i>.
			static AbstractMatrix<T> Ones(int order)
			{
				if (order > 1)
				{
					AbstractMatrix<T> result(order, order);

					for (int i = 0; i < order*order; ++i)
					{
						result.at(i) = 1;
					}
					return result;
				}
				else
				{
					fprintf(stderr, "Invalid dimension for matrix specified.  Must be 2x2 or larger.\n");
					throw MAT_INVALID_DIMENSIONALITY;
				}
			}
			///	Return the direct sum of 2 matrices
			///
			///	Gives the direct sum of two matrices, not necessarily of
			///	the same dimensions.  The direct sum see's each matrix put
			///	onto the main diagonal of the result.
			///
			///	@param lhs: the top left part of the resultant matrix.
			///	@param rhs: the bottom right part of the resultant matrix.
			///	@return a matrix that is the direct sum of the two matrices.
			static AbstractMatrix<T> DirectSum(const AbstractMatrix<T>& lhs, const AbstractMatrix<T>& rhs)
			{
				AbstractMatrix<T> result(lhs.m_rows + rhs.m_rows, lhs.m_cols + rhs.m_cols);

				//	Copy this matrix to the result
				for (unsigned int r1 = 0; r1 < lhs.m_rows; r1++)
				{
					for (unsigned int c1 = 0; c1 < lhs.m_cols; c1++)
					{
						result.at(result.indexOf(r1, c1)) = lhs.at(lhs.indexOf(r1, c1));
					}
				}

				//	Copy the second matrix to the result
				for (unsigned int r2 = 0; r2 < rhs.m_rows; r2++)
				{
					for (unsigned int c2 = 0; c2 < rhs.m_cols; c2++)
					{
						result.at(result.indexOf(lhs.m_rows + r2, lhs.m_cols + c2)) = rhs.at(rhs.indexOf(r2, c2));
					}
				}
				return result;
			}

			///	Return the direct sum of 3 matrices
			///
			///	Gives the direct sum of three matrices, not necessarily of
			///	the same dimensions.  The direct sum see's each matrix put
			///	onto the main diagonal of the result.
			///
			///	@param lhs: the top left part of the resultant matrix.
			///	@param rhs1: the centre part of the resultant matrix.
			///	@param rhs2: the bottom right part of the resultant matrix.
			///	@return a matrix that is the direct sum of the three matrices.
			static AbstractMatrix<T> DirectSum(const AbstractMatrix<T>& lhs, const AbstractMatrix<T>& rhs1, const AbstractMatrix<T>& rhs2)
			{
				AbstractMatrix<T> temp = DirectSum(lhs, rhs1);

				return DirectSum(temp, rhs2);
			}

			///	Round the number
			///
			///	Round a double precision number to the specified number of
			///	decimal places.
			///
			///	@param rhs: the number to be rounded
			///	@param dp: number of decimal places
			///	@return double, the number after rounding has been completed.
			inline static double RoundDP(const double& rhs, const unsigned int dp)
			{
				double scale = pow(10.0, dp);

				return (floor((rhs * scale) + 0.5)) / scale;
			}

			///	Round the number
			///
			///	Round a Complex number to the specified number of
			///	decimal places.
			///
			///	@param rhs: the number to be rounded
			///	@param dp: number of decimal places for the real / imag parts.
			///	@return Complex, the number after rounding has been completed.
			//		inline static Complex RoundDP(const Complex& rhs, const unsigned int dp)
			//		{
			//			double scale = pow(10.0, dp);

			//			double re = (floor((rhs.Real() * scale) + 0.5)) / scale;
			//			double im = (floor((rhs.Imag() * scale) + 0.5)) / scale;

			//			return Complex(re, im);
			//		}

			///	Return the 'positive form' of the number.
			///
			///	Convert the double value to a positive number.
			///
			///	@param rhs: the double to be converted
			///	@return double, the number with positive value.
			//		inline static double positiveValue(const double& rhs)
			//		{
			//			return fabs(rhs);
			//		}

			///	Return the 'positive form' of the number.
			///
			///	Convert real and negative elements of the
			///	number to positive.
			///
			///	@param rhs: the ComplexNumber to be converted
			///	@return Complex, the number with positive real
			///	and imaginary parts.
			//		inline static Complex positiveValue(const Complex& rhs)
			//		{
			//			using std::fabs;

			//			const double real = fabs(rhs.Real());
			//			const double imag = fabs(rhs.Imag());

			//			return Complex(real, imag);
			//		}

			///	Check for zero
			///
			///	Checks if the number is equal to zero.
			///
			///	@param rhs: the number to check.
			///	@return bool, true if the number is 0.
			//			inline static bool isZero(const double& rhs)
			//			{
			//				return (RoundDP(positiveValue(rhs), 15) == 0.0);
			//			}

			///	Check for zero
			///
			///	Checks if the number is equal to zero.
			///
			///	@param rhs: the number to check.
			///	@return bool, true if the number is 0+0i.
			//			inline static bool isZero(const Complex& rhs)
			//			{
			//				return (RoundDP(positiveValue(rhs), 15) == Complex(0.0, 0.0));
			//			}

			///	Check for a value of one
			///
			///	Checks if the number is equal to one.
			///
			///	@param rhs: the number to check.
			///	@return bool, true if the number is 1.
			//			inline static bool isOne(const double& rhs)
			//			{
			//				return (RoundDP(rhs, 15) == 1.0);
			//			}

			///	Check for a value of one
			///
			///	Checks if the number is equal to one.
			///
			///	@param rhs: the number to check.
			///	@return bool, true if the number is 1+0i.
			//			inline static bool isOne(const Complex& rhs)
			//			{
			//				return (RoundDP(rhs, 15) == Complex(1.0, 0.0));
			//			}

#pragma endregion Static functions
	};
#pragma region Non-member operators

	//	Note: cannot use a generic template for lhs at present as
	//	operators are not defined for lhs's of type int etc...
	//


	//template <typename MatrixType>
	//MatrixType operator +(const double lhs, const MatrixType& rhs)
	//{
	//	return rhs + lhs;
	//}

	//template <typename MatrixType>
	//MatrixType operator +(const Complex& lhs, const MatrixType& rhs)
	//{
	//	return rhs + lhs;
	//}

	//template <typename MatrixType>
	//MatrixType operator -(const double lhs, const MatrixType& rhs)
	//{
	//	Create a matrix filled with the value in 'left'
	//	MatrixType result(rhs.RowCount(), rhs.ColCount(), lhs);
	//
	//	result -= rhs;
	//
	//	return result;
	//}

	//template <typename MatrixType>
	//MatrixType operator -(const Complex& lhs, const MatrixType& rhs)
	//{
	//	Create a matrix filled with the value in 'left'
	//	MatrixType result(rhs.RowCount(), rhs.ColCount(), lhs);
	//
	//	result -= rhs;
	//
	//	return result;
	//}

	//template <typename MatrixType>
	//MatrixType operator *(const double lhs, const MatrixType& rhs)
	//{
	//	return rhs * lhs;
	//}

	//template <class MatrixType>
	//MatrixType operator *(const Complex& lhs, const MatrixType& rhs)
	//{
	//	return rhs * lhs;
	//}

#pragma endregion Non-member operators
};

#endif _AbstractMatrix<ElementType>_h_
