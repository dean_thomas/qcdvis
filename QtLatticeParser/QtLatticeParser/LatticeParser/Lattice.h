//==============================================//
//	My user classes are specified in			//
//	'Additional Includes' and requires the		//
//	setting of the DROPBOX path variable		//
//	on the compile system.						//
//==============================================//
#ifndef LATTICE_H
#define LATTICE_H

#include "LatticeSite.h"
//#include "BasicStructs.h"
//#include "LinearAlgebra.h"
//#include "ParserGeneric.h"
#include "AveragePlaquetteMatrix.h"

#include <Vector4.h>
#include <Vector3.h>

//#include "TransformationLattice.h"
//#include "ScalarVolume.h"
//#include "LinkDirection.h"
//#include "Vec3Volume.h"
//#include "Vec3.h"

//using LinearAlgebra::Complex;

#ifndef __func__
#define __func__ __FUNCTION__
#endif

class Lattice
{
public:
    using size_type = size_t;
    using float_type = double;

    struct Vec3
    {
        float_type x;
        float_type y;
        float_type z;
    };

    using LinkDirection = LatticeSite::LinkDirection;
    using Complex = std::complex<double>;
    using Array4f = Storage::Vector4<float_type>;
    using Array3f = Storage::Vector3<float_type>;
    using Array4v3 = Storage::Vector4<Vec3>;
    using Array4ls = Storage::Vector4<LatticeSite>;
    struct Index3
    {
        size_type x;
        size_type y;
        size_type z;
    };

    struct Index4
    {
        size_type x;
        size_type y;
        size_type z;
        size_type t;
    };

    static Lattice GenerateGroundStateLattice(const size_t dimX,
                                              const size_t dimY,
                                              const size_t dimZ,
                                              const size_t dimT);

private:
    //	Private variables
    Array4ls m_values;
    //size_type sizeX;
    //size_type sizeY;
    //size_type sizeZ;
    //size_type sizeT;

public:
    //	Accessors
    size_type SizeX() const{ return m_values.size_x(); }
    size_type SizeY() const{ return m_values.size_y(); }
    size_type SizeZ() const{ return m_values.size_z(); }
    size_type SizeT() const{ return m_values.size_w(); }
    size_type NumberOfSites() const { return m_values.size_x() * m_values.size_y() * m_values.size_z() * m_values.size_w(); }

public:
    Array4f calculateSpaceLikePlaquetteField() const;
    Array4f calculateTimeLikePlaquetteField() const;
    Array4f calculateTopologicalChargeDensityField() const;
    Array3f CalculatePolyakovLoopField() const;
    Array4v3 calculateMagneticField() const;
    Array4v3 calculateElectricField() const;

    //	Public methods
    ComplexMatrix CalculatePlaquette(Index4 siteA, LinkDirection dir1, LinkDirection dir2) const;
    //double CalculatePolyakovLoop(Index3 coordinate, LinkDirection directionToVary) const;

    //bool FillLattice(const ParserGeneric* parser);
    AveragePlaquetteMatrix CalculateAveragePlaquettes() const;
    double CalculatePolyakovLoopAtSite(const size_type & x, const size_type & y, const size_type & z) const;
    //Lattice ApplyTransformationLattice(TransformationLattice transformationLattice) const;
    std::string ToString() const;

    double CalculateSpaceLikePlaquettesAtSite(const Index4& coordinate)const;
    double CalculateTimeLikePlaquettesAtSite(const Index4& coordinate)const;



    double CalculateTopologicalChargeDensityAtSite(const Index4& coordinate)const;

    Vec3 CalculateMagneticFieldAtSite(const Index4& coordinate) const;

    Vec3 CalculateElectricFieldAtSite(const Index4& coordinate) const;
public:
    //	Constructors / destructors
    Lattice(const size_type& xDim,
            const size_type& yDim,
            const size_type& zDim,
            const size_type& tDim);
    Lattice(const size_type& xyzDim,
            const size_type& tDim);
    Lattice() {  }
public:
    //	Operators
    LatticeSite operator [](Index4 latticeCoordinates) const;
    LatticeSite& operator [](Index4 latticeCoordinates);

    LatticeSite at(const size_type& x,
                   const size_type& y,
                   const size_type& z,
                   const size_type& t) const;
    LatticeSite& at(const size_type& x,
                    const size_type& y,
                    const size_type& z,
                    const size_type& t);
};

#endif
