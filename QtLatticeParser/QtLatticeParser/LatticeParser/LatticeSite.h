
///	@file:		LatticeSite.h
///
///	@brief:		...
///	@since:		...
///
///	@details:	...
///
///	@copyright:	....
///
///	@todo:		Wrap U11 / U12 arrays in a ComplexArray type

#ifndef LATTICE_SITE
#define LATTICE_SITE

#include <array>
#include <memory>

#include "ComplexMatrix.h"

using LinearAlgebra::ComplexMatrix;

class LatticeSite //: vector<ComplexMatrix>
{
	public:
		using size_type = size_t;
		using UPtr_ComplexMatrix = std::unique_ptr<ComplexMatrix>;
		//using LinkList = std::array<UPtr_ComplexMatrix, 4>;
		using LinkList = std::vector<ComplexMatrix>;

		enum class LinkDirection
		{
			X	=	0x00,
			Y	=	0x01,
			Z	=	0x02,
			T	=	0x03
		};
	private:
		//	Private variables
		LinkList values;
	public:
		//	Accessors
		ComplexMatrix LinkX() const { return values[(size_type)LinkDirection::X]; }
		ComplexMatrix& LinkX() { return values[(size_type)LinkDirection::X]; }

		ComplexMatrix LinkY() const { values[(size_type)LinkDirection::Y]; }
		ComplexMatrix& LinkY() { return values[(size_type)LinkDirection::Y]; }

		ComplexMatrix LinkZ() const { return values[(size_type)LinkDirection::Z]; }
		ComplexMatrix& LinkZ() { return values[(size_type)LinkDirection::Z]; }

		ComplexMatrix LinkT() const { return values[(size_type)LinkDirection::T]; }
		ComplexMatrix& LinkT() { return values[(size_type)LinkDirection::T]; }
	public:
		//	Constructors / destructors
		LatticeSite();
		~LatticeSite();
	private:
		//	Private methods
		void init();
	public:
		//	Public methods
		void Clear();

	public:
		//	Operators
		ComplexMatrix operator[](const unsigned int rhs) const;
		ComplexMatrix& operator[](const unsigned int rhs);

		ComplexMatrix operator[](const LinkDirection& rhs) const;
		ComplexMatrix& operator[](const LinkDirection& rhs);
};

#endif LATTICE_SITE
