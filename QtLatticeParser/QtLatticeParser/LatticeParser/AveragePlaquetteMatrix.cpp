#include "AveragePlaquetteMatrix.h"

void AveragePlaquetteMatrix::init()
{
	data = new double*[4];

	for (int i = 0; i < 4; ++i)
	{
		data[i] = new double[4];
	}
}

AveragePlaquetteMatrix::AveragePlaquetteMatrix()
{
	init();

	for (int x = 0; x < 4; ++x)
	{
		for (int y = 0; y < 4; ++y)
		{
			data[x][y] = 0.0;
		}
	}
}

AveragePlaquetteMatrix::~AveragePlaquetteMatrix()
{
}

double AveragePlaquetteMatrix::operator [](Pair directions) const
{
	return data[(size_type)directions.first][(size_type)directions.second];
}

double& AveragePlaquetteMatrix::operator [](Pair directions)
{
	return data[(size_type)directions.first][(size_type)directions.second];
}

string AveragePlaquetteMatrix::ToString()
{
	stringstream result;
	result.precision(16);

	char dirs[4] = { 'X', 'Y', 'Z', 'T' };

	for (int i1 = 0; i1 < 4; i1++)
	{
		for (int i2 = 0; i2 < 4; i2++)
		{
			if (i1 != i2)
			{
				result << "[";
				result << dirs[i1];
				result << dirs[i2];
				result << "]";
				result << " = ";
				result << data[i1][i2];
				result << "\n";
			}
		}
	}
	return result.str();
}
