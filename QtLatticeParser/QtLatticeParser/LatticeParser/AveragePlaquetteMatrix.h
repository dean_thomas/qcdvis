//==============================================//
//	My user classes are specified in			//
//	'Additional Includes' and requires the		//
//	setting of the DROPBOX path variable		//
//	on the compile system.						//
//==============================================//
#ifndef AVERAGE_PLAQUETTEMATRIX_H
#define AVERAGE_PLAQUETTEMATRIX_H

#include <string>
#include <sstream>
#include <tuple>

#include "LatticeSite.h"

using std::string;
using std::stringstream;

class AveragePlaquetteMatrix
{
	private:
		using size_type = size_t;
		using LinkDirection = LatticeSite::LinkDirection;
		using Pair = std::pair<LinkDirection, LinkDirection>;
		double** data;


	public:
		double XY() const { return data[(size_type)LinkDirection::X][(size_type)LinkDirection::Y]; }
		double& XY() { return data[(size_type)LinkDirection::X][(size_type)LinkDirection::Y]; }

		double XZ() const { return data[(size_type)LinkDirection::X][(size_type)LinkDirection::Z]; }
		double& XZ() { return data[(size_type)LinkDirection::X][(size_type)LinkDirection::Z]; }

		double XT() const { return data[(size_type)LinkDirection::X][(size_type)LinkDirection::T]; }
		double& XT() { return data[(size_type)LinkDirection::X][(size_type)LinkDirection::T]; }

		double YX() const { return data[(size_type)LinkDirection::Y][(size_type)LinkDirection::X]; }
		double& YX() { return data[(size_type)LinkDirection::Y][(size_type)LinkDirection::X]; }

		double YZ() const { return data[(size_type)LinkDirection::Y][(size_type)LinkDirection::Z]; }
		double& YZ() { return data[(size_type)LinkDirection::Y][(size_type)LinkDirection::Z]; }

		double YT() const { return data[(size_type)LinkDirection::Y][(size_type)LinkDirection::T]; }
		double& YT() { return data[(size_type)LinkDirection::Y][(size_type)LinkDirection::T]; }

		double ZX() const { return data[(size_type)LinkDirection::Z][(size_type)LinkDirection::X]; }
		double& ZX() { return data[(size_type)LinkDirection::Z][(size_type)LinkDirection::X]; }

		double ZY() const { return data[(size_type)LinkDirection::Z][(size_type)LinkDirection::Y]; }
		double& ZY() { return data[(size_type)LinkDirection::Z][(size_type)LinkDirection::Y]; }

		double ZT() const { return data[(size_type)LinkDirection::Z][(size_type)LinkDirection::T]; }
		double& ZT() { return data[(size_type)LinkDirection::Z][(size_type)LinkDirection::T]; }

		double TX() const { return data[(size_type)LinkDirection::T][(size_type)LinkDirection::X]; }
		double& TX() { return data[(size_type)LinkDirection::T][(size_type)LinkDirection::X]; }

		double TY() const { return data[(size_type)LinkDirection::T][(size_type)LinkDirection::Y]; }
		double& TY() { return data[(size_type)LinkDirection::T][(size_type)LinkDirection::Y]; }

		double TZ() const { return data[(size_type)LinkDirection::T][(size_type)LinkDirection::Z]; }
		double& TZ() { return data[(size_type)LinkDirection::T][(size_type)LinkDirection::Z]; }

	private:
		void init();

	public:
		AveragePlaquetteMatrix();
		~AveragePlaquetteMatrix();

	public:
		double operator [](Pair directions) const;
		double& operator [](Pair directions);

	public:
		string ToString();
};

#endif AVERAGE_PLAQUETTE_MATRIX_H
