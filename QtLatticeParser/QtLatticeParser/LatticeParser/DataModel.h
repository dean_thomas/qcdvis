#ifndef DATAMODEL_H
#define DATAMODEL_H

///	Holds a lattice and information for a single cooling step

#include <Vector3.h>
#include <Vector4.h>

#include <map>
#include <string>

#include "Lattice.h"
//#include "parserSU2HMC.h"
//#include "ParserGeneric.h"
//#include "ScalarVolumeArray.h"
//#include "Vec3VolumeArray.h"

class DataModel
{
	public:
		using size_type = size_t;
		using float_type = Lattice::float_type;
		using Array4f = Lattice::Array4f;
		using Array3f = Lattice::Array3f;
		using Array4v3 = Lattice::Array4v3;

	private:
		//	Holds the actual data
		Lattice m_lattice;
		std::string m_configurationName;
		size_type m_cools;
		size_type m_mu;
		size_type m_beta;
		size_type m_k;
		size_type m_j;

		mutable bool m_spaceLikePlaquettesAreComputed = false;
		mutable Array4f m_spaceLikePlaquettes;

		mutable bool m_timeLikePlaquettesAreComputed = false;
		mutable Array4f m_timeLikePlaquettes;

		mutable bool m_averagePlaquettesAreComputed = false;
		mutable Array4f m_averagePlaquettes;

		mutable bool m_differencePlaquettesAreComputed = false;
		mutable Array4f m_differencePlaquettes;

		mutable bool m_topologicalChargeDensityFieldIsComputed = false;
		mutable Array4f m_topologicalChargeDensityField;

		mutable bool m_polyakovLoopFieldIsComputed = false;
		mutable Array3f m_polyakovLoopField;

		mutable bool m_magneticFieldIsComputed = false;
		mutable Array4v3 m_magneticField;

		mutable bool m_electricFieldIsComputed = false;
		mutable Array4v3 m_electricField;
	public:
		size_type SizeX() const { return m_lattice.SizeX(); }
		size_type SizeY() const { return m_lattice.SizeY(); }
		size_type SizeZ() const { return m_lattice.SizeZ(); }
		size_type SizeT() const { return m_lattice.SizeT(); }
		size_type CoolingCycle() const { return m_cools; }

	private:
		void markCacheAsDirty();

		Array4f calculateAveragePlaquetteField() const;
		Array4f calculateDifferencePlaquetteField() const;

	public:
		bool SaveToFile(const std::string& filename, const Array3f& data) const;
		bool SaveToFile(const std::string& filename, const Array4f&) const;

		static DataModel GenerateGroundStateLattice(const size_t xDim,
													const size_t yDim,
													const size_t zDim,
													const size_t tDim);

		DataModel()
			:  DataModel("", "untitled configuration", 0, 0, 0, 0, 0) { }
		DataModel(const string& filename,
				  const string& conf,
				  const size_type& cools,
				  const size_type& b,
				  const size_type& k,
				  const size_type& mu,
				  const size_type& j);

		bool LoadConfiguration(const string & filename);


		Array4f GetDifferencePlaquetteField() const;
		Array4f GetAveragePlaquetteField() const;
		Array4f GetSpaceLikePlaquetteField() const;
		Array4f GetTimeLikePlaquetteField() const;
		Array3f GetPolyakovLoopField() const;
		Array4f GetTopologicalChargeDensityField() const;
		Array4v3 GetMagneticField() const;
		Array4v3 GetElectricField() const;

		AveragePlaquetteMatrix GetAveragePlaquetteMatrix() const
		{ return m_lattice.CalculateAveragePlaquettes(); }
};

#endif
