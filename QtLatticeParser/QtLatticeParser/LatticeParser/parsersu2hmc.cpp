#include "parsersu2hmc.h"

using std::reverse_copy;
using std::ifstream;
using std::ios;
using std::cerr;
using std::cout;
using std::endl;

///
///	Fill the lattice with the supplied values
///
///	Using the values stored in the u11 and u12 vectors
///	fill the lattice sites in the required order.
///
///	@param u11: a vector of Complex numbers (in range -1..+1)
///	@param u12: a vector of Complex numbers (in range -1..+1)
///	@return: true if successful
///	\since
/// \author		Dean
/// \author		Dean: moved to functor [21-10-2015]
///
Lattice ParserSU2HMC::fillLattice4(const FileFormatLookupTable::Format& fileFormat) const
{
    Lattice result(fileFormat.sizeXYZ,
                   fileFormat.sizeT);

    auto counter = 0;

    for (auto linkDirection = 0; linkDirection < 4; ++linkDirection)
    {
        for (auto t = 0; t < fileFormat.sizeT; ++t)
        {
            for (auto z = 0; z < fileFormat.sizeXYZ; ++z)
            {
                for (auto y = 0; y < fileFormat.sizeXYZ; ++y)
                {
                    for (auto x = 0; x < fileFormat.sizeXYZ; ++x)
                    {
                        ComplexMatrix value(u11[counter],
                                            u12[counter],
                                            u21[counter],
                                            u22[counter]);

                        result.at(x, y, z, t)[linkDirection] = value;

                        ++counter;
                    }
                }
            }
        }
    }

    return result;
}

///
///	Fill the lattice with the supplied values
///
///	Using the values stored in the u11, u12, u21 and u22 vectors
///	fill the lattice sites in the required order.
///
///	@param u11: a vector of Complex numbers (in range -1..+1)
///	@param u12: a vector of Complex numbers (in range -1..+1)
///	@param u21: a vector of Complex numbers (in range -1..+1)
///	@param u22: a vector of Complex numbers (in range -1..+1)
///	@return: true if successful
///	\since
/// \author		Dean
/// \author		Dean: moved to functor [21-10-2015]
///
Lattice ParserSU2HMC::fillLattice2(const FileFormatLookupTable::Format& fileFormat) const
{
    Lattice result(fileFormat.sizeXYZ,
                   fileFormat.sizeT);

    auto counter = 0;

    for (auto linkDirection = 0; linkDirection < 4; ++linkDirection)
    {
        for (auto t = 0; t < fileFormat.sizeT; ++t)
        {
            for (auto z = 0; z < fileFormat.sizeXYZ; ++z)
            {
                for (auto y = 0; y < fileFormat.sizeXYZ; ++y)
                {
                    for (auto x = 0; x < fileFormat.sizeXYZ; ++x)
                    {
                        ComplexMatrix value(u11[counter],
                                            u12[counter],
                                            -conj(u12[counter]),
                                            conj(u11[counter]));

                        result.at(x, y, z, t)[linkDirection] = value;

                        ++counter;
                    }
                }
            }
        }
    }
    return result;
}
///
/// \brief ParserSU2HMC::operator ()
/// \param filename
/// \return
/// \since		20-10-2015
/// \author		Dean
///
Lattice ParserSU2HMC::operator()(const std::string& filename)
{
    std::cout << "Identifying filetype" << std::endl;
    std::cout << filename << std::endl;

    auto fileSize = getFileSize(filename);

    std::cout << "File is " << fileSize << " bytes" << std::endl;

    assert(fileSize > 0);

    auto fileFormatIt = m_fileFormatLookupTable.m_entries.find(fileSize);
    if (fileFormatIt == m_fileFormatLookupTable.m_entries.cend())
    {
        std::cerr << "Unrecognized file format!" << std::endl;

        throw std::invalid_argument("Filetype is not a recognized "
                                    "lattice format");
    }
    else
    {
        FileFormatLookupTable::Format fileFormat = fileFormatIt->second;

        if (fileFormat.isCompressed)
        {
            if (parseCompressedFile(filename,
									Endianness::E_LITTLE_ENDIAN,
									Endianness::E_BIG_ENDIAN,
                                    fileFormat.entries))
            {
                return fillLattice2(fileFormat);
            }
            else
            {
                return Lattice();
            }
        }
        else
        {
            if (parseUncompressedFile(filename,
									  Endianness::E_LITTLE_ENDIAN,
									  Endianness::E_BIG_ENDIAN,
                                      fileFormat.entries))
            {
                return fillLattice4(fileFormat);
            }
            else
            {
                return Lattice();
            }
        }
    }
}

///
/// \brief ParserSU2HMC::getFileSize
/// \param filename
/// \return
/// \since		21-10-2015
/// \author		Dean
///
ParserSU2HMC::size_type ParserSU2HMC::getFileSize(const std::string& filename) const
{
    std::ifstream file(filename, std::ios::binary | std::ios::ate);

    if (!file.is_open()) return 0;

    auto fileSize = file.tellg();
    file.close();

    return fileSize;
}

///
/// \brief ParserSU2HMC::makeComplex
/// \param realBytes
/// \param imagBytes
/// \param sourceEndianess
/// \param destEndianness
/// \return
/// \since		20-10-2015
/// \author		Dean
///
ParserSU2HMC::complex_type ParserSU2HMC::makeComplex(const float_buffer_type& realBytes,
                                                     const float_buffer_type& imagBytes,
                                                     const Endianness& sourceEndianess,
                                                     const Endianness& destEndianness) const
{
    union
    {
        double doubleVal;
        float_buffer_type bytes;
    } real, imag;

    if (sourceEndianess != destEndianness)
    {
        //	Swap Endianness
        reverse_copy(realBytes.cbegin(), realBytes.cend(), real.bytes.begin());
        reverse_copy(imagBytes.cbegin(), imagBytes.cend(), imag.bytes.begin());
    }
    else
    {
        //	Constant endianness
        real.bytes = realBytes;
        imag.bytes = imagBytes;
    }
    return complex_type(real.doubleVal, imag.doubleVal);
}

///	Parse the given Uncompressed configuration
///
///	Returns the values stored in the specified binary format file as lists of
///	Complex numbers in the u11, u12, u21 and u22 vectors.  These can then be processed
///	to produce the required SU(2) matrices.
///
///	@param filename: the name and path of the file to be read in.
///	@param endianess: the endianness of the machine used to generate the file.
///	@param numberOfValues: the expected number of values to be found in each vector.
///	@return bool: true if the file was sucessfully parsed and stored in u11 and u12.
/// \since		20-102-015
/// \author		Dean
bool ParserSU2HMC::parseUncompressedFile(const std::string& filename,
                                         const Endianness& fileEndianness,
                                         const Endianness& machineEndianness,
                                         const size_type& numberOfValues)
{
    header_buffer_type headerBytes;
    //float_buffer_type seedBytes;
    footer_buffer_type footerBytes;

    u11.clear();
    u12.clear();
    u21.clear();
    u22.clear();

    u11.resize(numberOfValues);
    u12.resize(numberOfValues);
    u21.resize(numberOfValues);
    u22.resize(numberOfValues);

    ifstream file(filename.c_str(), ios::in | ios::binary);

    if (file.is_open())
    {
        //fprintf(stdout, "File: %s\n", filename.c_str());
        //	Read the header
        file.read(&headerBytes[0], FORTRAN_HEADER_BYTE_COUNT);
        //fprintf(stdout, "File header: %X %X %X %X\n", headerBytes[0], headerBytes[1], headerBytes[2], headerBytes[3]);

        for (auto i = 0; i < numberOfValues; ++i)
        {
            float_buffer_type u11_realBuffer;
            float_buffer_type u11_imagBuffer;
            float_buffer_type u12_realBuffer;
            float_buffer_type u12_imagBuffer;
            float_buffer_type u21_realBuffer;
            float_buffer_type u21_imagBuffer;
            float_buffer_type u22_realBuffer;
            float_buffer_type u22_imagBuffer;

            //	Read complex number elements
            file.read(&u11_realBuffer[0], FLOATING_POINT_BYTES);
            file.read(&u11_imagBuffer[0], FLOATING_POINT_BYTES);

            file.read(&u21_realBuffer[0], FLOATING_POINT_BYTES);
            file.read(&u21_imagBuffer[0], FLOATING_POINT_BYTES);

            file.read(&u12_realBuffer[0], FLOATING_POINT_BYTES);
            file.read(&u12_imagBuffer[0], FLOATING_POINT_BYTES);

            file.read(&u22_realBuffer[0], FLOATING_POINT_BYTES);
            file.read(&u22_imagBuffer[0], FLOATING_POINT_BYTES);

            //	Convert to complex numbers
            complex_type u11val = makeComplex(u11_realBuffer,
                                              u11_imagBuffer,
                                              fileEndianness,
                                              machineEndianness);
            complex_type u12val = makeComplex(u12_realBuffer,
                                              u12_imagBuffer,
                                              fileEndianness,
                                              machineEndianness);
            complex_type u21val = makeComplex(u21_realBuffer,
                                              u21_imagBuffer,
                                              fileEndianness,
                                              machineEndianness);
            complex_type u22val = makeComplex(u22_realBuffer,
                                              u22_imagBuffer,
                                              fileEndianness,
                                              machineEndianness);

            //	Add to u11 vector
            u11[i] = u11val;
            u12[i] = u12val;
            u21[i] = u21val;
            u22[i] = u22val;
        }

        //	Read in the seed - not currently written to cooled configs
        //file.read((char*)seedBytes, 8);

        //	Read in the footer
        file.read(&footerBytes[0], FORTRAN_FOOTER_BYTE_COUNT);
        //fprintf(stdout, "File Footer: %X %X %X %X\n", footerBytes[0], footerBytes[1], footerBytes[2], footerBytes[3]);

        //	Check for EOF?

        //	Don't forget to close file after
        file.close();

        //	Header and footer should match on valid files
        if ((headerBytes[0] == footerBytes[0]) && (headerBytes[1] == footerBytes[1])
                && (headerBytes[2] == footerBytes[2]) && (headerBytes[3] == footerBytes[3]))
        {
            //	Set the compression flag
            m_isGaugeCompressed = false;

            return true;
        }
        else
        {
            return false;
        }
    }
    else
    {
        //	File wasn't opened
        cerr << "File " << filename << " could not be opened." << endl;
        return false;
    }
}

///	Parse the given compressed configuration
///
///	Returns the values stored in the specified binary format file as lists of
///	Complex numbers in the u11 and u12 vectors.  These can then be processed
///	to produce the required SU(2) matrices.
///
///	@param filename: the name and path of the file to be read in.
///	@param endianess: the endianness of the machine used to generate the file.
///	@param numberOfValues: the expected number of values to be found in each vector.
///	@return bool: true if the file was sucessfully parsed and stored in u11 and u12.
/// \since		20-102-015
/// \author		Dean
///
bool ParserSU2HMC::parseCompressedFile(const std::string& filename,
                                       const Endianness& fileEndianness,
                                       const Endianness& machineEndianness,
                                       const size_type& numberOfValues)
{
    header_buffer_type headerBytes;
    float_buffer_type seedBytes;
    footer_buffer_type footerBytes;

    u11.clear();
    u12.clear();
    u21.clear();
    u22.clear();

    u11.resize(numberOfValues);
    u12.resize(numberOfValues);

    std::ifstream file(filename, std::ios::in | std::ios::binary);

    if (file.is_open())
    {
        //fprintf(stdout, "File: %s\n", filename.c_str());
        //	Read the header
        file.read(&headerBytes[0], FORTRAN_HEADER_BYTE_COUNT);
        //fprintf(stdout, "File header: %X %X %X %X\n", headerBytes[0], headerBytes[1], headerBytes[2], headerBytes[3]);

        //	Read the values from the file - u11 buffer first
        for (auto i = 0; i < numberOfValues; ++i)
        {
            float_buffer_type realBuffer;
            float_buffer_type imagBuffer;

            //	Read complex number elements
            file.read(&realBuffer[0], FLOATING_POINT_BYTES);
            file.read(&imagBuffer[0], FLOATING_POINT_BYTES);

            //	Convert to complex number
            auto u11val = makeComplex(realBuffer,
                                      imagBuffer,
                                      fileEndianness,
                                      machineEndianness);

            //	Add to u11 vector
            u11[i] = u11val;
        }

        //	Read the values from the file - then u12 buffer
        for (auto i = 0; i < numberOfValues; ++i)
        {
            float_buffer_type realBuffer;
            float_buffer_type imagBuffer;

            //	Read complex number elements
            file.read(&realBuffer[0], FLOATING_POINT_BYTES);
            file.read(&imagBuffer[0], FLOATING_POINT_BYTES);

            //	Convert to complex number
            auto u12val = makeComplex(realBuffer,
                                      imagBuffer,
                                      fileEndianness,
                                      machineEndianness);

            //	Add to u11 vector
            u12[i] = u12val;
        }

        //	Read in the seed
        file.read(&seedBytes[0], FLOATING_POINT_BYTES);

        //	Read in the footer
        file.read(&footerBytes[0], FORTRAN_FOOTER_BYTE_COUNT);
        //fprintf(stdout, "File Footer: %X %X %X %X\n", footerBytes[0], footerBytes[1], footerBytes[2], footerBytes[3]);

        //	Check for EOF?

        //	Don't forget to close file after
        file.close();

        //	Header and footer should match on valid files
        if ((headerBytes[0] == footerBytes[0]) && (headerBytes[1] == footerBytes[1])
                && (headerBytes[2] == footerBytes[2]) && (headerBytes[3] == footerBytes[3]))
        {
            //	Set the compression flag
            m_isGaugeCompressed = true;

            return true;
        }
        else
        {
            return false;
        }
    }
    else
    {
        //	File wasn't opened
        cerr << "File " << filename << " could not be opened." << endl;
        return false;
    }
}
