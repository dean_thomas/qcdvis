#ifndef PARSER_SU2_HMC_H
#define PARSER_SU2_HMC_H

#include "Lattice.h"
#include <string>
#include <array>
#include <vector>
#include <complex>
#include <fstream>
#include <iostream>
#include "fileformatlookuptable.h"

#define FORTRAN_HEADER_BYTE_COUNT 4
#define FORTRAN_FOOTER_BYTE_COUNT 4

//	8 bytes for double precision
//	4 bytes for single precision
#define FLOATING_POINT_BYTES 8

class ParserSU2HMC
{
	private:
		FileFormatLookupTable m_fileFormatLookupTable;

	public:
		using size_type = size_t;
		using complex_type = std::complex<double>;
		using vector_type = std::vector<complex_type>;
		using float_buffer_type = std::array<char, FLOATING_POINT_BYTES>;
		using header_buffer_type = std::array<char, FORTRAN_HEADER_BYTE_COUNT>;
		using footer_buffer_type = std::array<char, FORTRAN_FOOTER_BYTE_COUNT>;

		vector_type u11;
		vector_type u12;
		vector_type u21;
		vector_type u22;

		bool m_isGaugeCompressed = false;

		enum class Endianness
		{
                        E_LITTLE_ENDIAN,
                        E_BIG_ENDIAN
		};

		ParserSU2HMC()
		{
			std::cout << m_fileFormatLookupTable.ToString() << std::endl;
		}

		Lattice operator()(const std::string& filename);

		size_type getFileSize(const std::string& filename) const;

		complex_type makeComplex(const float_buffer_type& realBytes,
								 const float_buffer_type& imagBytes,
								 const Endianness& sourceEndianess,
								 const Endianness& destEndianness) const;

		bool parseCompressedFile(const std::string& filename,
								 const Endianness& fileEndianness,
								 const Endianness& machineEndianness,
								 const size_type& numberOfValues);
		bool parseUncompressedFile(const std::string& filename,
								   const Endianness& fileEndianness,
								   const Endianness& machineEndianness,
								   const size_type& numberOfValues);

		Lattice fillLattice2(const FileFormatLookupTable::Format& fileFormat) const;
		Lattice fillLattice4(const FileFormatLookupTable::Format& fileFormat) const;
};

#endif // PARSER_SU2_HMC_H

