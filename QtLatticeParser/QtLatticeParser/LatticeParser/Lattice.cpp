#include "Lattice.h"

#include <iostream>

using std::endl;
using std::cout;
using std::make_pair;

#define TEMP_DATA_NAME "conp0050"
#define TEMP_COOL_STEP 0

///
/// \brief		Generates a ground state configuration of specified
///				dimensionality (where all links are identity matrix)
/// \param xDim
/// \param yDim
/// \param zDim
/// \param tDim
/// \return
/// \author		Dean
/// \since		29-02-2016
///
Lattice Lattice::GenerateGroundStateLattice(const size_t dimX,
                                            const size_t dimY,
                                            const size_t dimZ,
                                            const size_t dimT)
{
    Lattice result(dimX, dimY, dimZ, dimT);

    //	Loop over every site
    for (auto x = 0; x < dimX; ++x)
    {
        for (auto y = 0; y < dimY; ++y)
        {
            for (auto z = 0; z < dimZ; ++z)
            {
                for (auto t = 0; t < dimT; ++t)
                {
                    //	Set everything to the identity matrix
                    const ComplexMatrix identity = ComplexMatrix::Identity(2);

                    //	Ugly...should really encapsulate these pointers
                    result.at(x, y, z, t).LinkX() = ComplexMatrix(identity);
                    result.at(x, y, z, t).LinkY() = ComplexMatrix(identity);
                    result.at(x, y, z, t).LinkZ() = ComplexMatrix(identity);
                    result.at(x, y, z, t).LinkT() = ComplexMatrix(identity);
                }
            }
        }
    }

    return result;
}

///	Calculate the differnec plaquettes at each site on the lattice for a given time slice
///
///	Calculates the plaquettes in the XT, YT and ZT planes and returns the average
///	at each site and stores them as a scalar volume.
///
///	@param coordinate: a x,y,z,t coordinate in Euclidean space-time.
///	@return ScalarVolume: the average timelike plaquette at each site on the lattice
/// \author		Dean
/// \author		Dean: update to use C++11 [20-10-2015]
///
Lattice::Array4v3 Lattice::calculateMagneticField() const
{
    Array4v3 result(m_values.size_x(), m_values.size_y(), m_values.size_z(), m_values.size_w());

    for (auto x = 0ul; x < m_values.size_x(); ++x)
    {
        for (auto y = 0ul; y < m_values.size_y(); ++y)
        {
            for (auto z = 0ul; z < m_values.size_z(); ++z)
            {
                for (auto t = 0ul; t < m_values.size_w(); ++t)
                {
                    result.at(x, y, z, t) = CalculateMagneticFieldAtSite({ x, y, z, t });
                }
            }
        }
    }
    return result;
}

///	Calculate the differnec plaquettes at each site on the lattice for a given time slice
///
///	Calculates the plaquettes in the XT, YT and ZT planes and returns the average
///	at each site and stores them as a scalar volume.
///
///	@param coordinate: a x,y,z,t coordinate in Euclidean space-time.
///	@return ScalarVolume: the average timelike plaquette at each site on the lattice
/// \author		Dean
/// \author		Dean: update to use C++11 [20-10-2015]
///
Lattice::Array4v3 Lattice::calculateElectricField() const
{
    Array4v3 result(m_values.size_x(), m_values.size_y(), m_values.size_z(), m_values.size_w());

    for (auto x = 0ul; x < m_values.size_x(); ++x)
    {
        for (auto y = 0ul; y < m_values.size_y(); ++y)
        {
            for (auto z = 0ul; z < m_values.size_z(); ++z)
            {
                for (auto t = 0ul; t < m_values.size_w(); ++t)
                {
                    result.at(x, y, z, t) = CalculateElectricFieldAtSite({ x, y, z, t });
                }
            }
        }
    }
    return result;
}

///
///	\brief		Calculates the Polyakov loop values for each site
///				on the lattice
///	\details
///	\author		Dean
///	\author		Dean: update to use C++11 features [08-10-2015]
///
///
Lattice::Array3f Lattice::CalculatePolyakovLoopField() const
{
    Array3f result(m_values.size_x(), m_values.size_y(), m_values.size_z());

    double total = 0.0;

    //	Calculate results
    for (auto x = 0; x < m_values.size_x(); ++x)
    {
        for (auto y = 0; y < m_values.size_y(); ++y)
        {
            for (auto z = 0; z < m_values.size_z(); ++z)
            {
                auto poly = CalculatePolyakovLoopAtSite(x, y, z);
                result.at(x, y, z) = poly;

                total += poly;
            }
        }
    }

    total /= (m_values.size_x() * m_values.size_y() * m_values.size_z());

    cout << "Average polyakov: " << total << endl;

    return result;
}

///	Calculate the differnec plaquettes at each site on the lattice for a given time slice
///
///	Calculates the plaquettes in the XT, YT and ZT planes and returns the average
///	at each site and stores them as a scalar volume.
///
///	@param coordinate: a x,y,z,t coordinate in Euclidean space-time.
///	@return ScalarVolume: the average timelike plaquette at each site on the lattice
/// \author		Dean
/// \author		Dean: update to use C++11 [20-10-2015]
/// \author		Dean: return a 4D array (not multiple 3D arrays) [22-10-2015]
///
Lattice::Array4f Lattice::calculateTopologicalChargeDensityField() const
{
    Array4f result(m_values.size_x(), m_values.size_y(), m_values.size_z(), m_values.size_w());

    for (auto x = 0ul; x < m_values.size_x(); ++x)
    {
        for (auto y = 0ul; y < m_values.size_y(); ++y)
        {
            for (auto z = 0ul; z < m_values.size_z(); ++z)
            {
                for (auto t = 0ul; t < m_values.size_w(); ++t )
                {
                    //result[{ x, y, z }] = CalculateTopologicalChargeDensityAtSite({ x, y, z, time });
                    result.at(x, y, z, t) = CalculateTopologicalChargeDensityAtSite({ x, y, z, t });
                }
            }
        }
    }
    return result;
}

///	Calculate the average timelike plaquettes at each site on the lattice for a given time slice
///
///	Calculates the plaquettes in the XT, YT and ZT planes and returns the average
///	at each site and stores them as a scalar volume.
///
///	@param coordinate: a x,y,z,t coordinate in Euclidean space-time.
///	@return ScalarVolume: the average timelike plaquette at each site on the lattice
/// \author		Dean
/// \author		Dean: update to use C++11 [20-10-2015]
///
Lattice::Array4f Lattice::calculateTimeLikePlaquetteField() const
{
    Array4f result(m_values.size_x(),m_values.size_y(),m_values.size_z(),m_values.size_w());

    for (auto x = 0ul; x < m_values.size_x(); ++x)
    {
        for (auto y = 0ul; y < m_values.size_y(); ++y)
        {
            for (auto z = 0ul; z < m_values.size_z(); ++z)
            {
                for (auto t = 0ul; t < m_values.size_w(); ++t)
                {
                    //	Normalized to 0--1
                    auto value = CalculateTimeLikePlaquettesAtSite({ x, y, z, t });
                    assert((value >= -1.0) && (value <= +1.0));

                    //	Put into the range 0.0..+2.0
                    value = 1.0 - value;
                    assert((value >= 0.0) && (value <= +2.0));

                    //	Finally put into the range 0.0..+1.0
                    value /= 2.0;
                    assert((value >= 0.0) && (value <= 1.0));

                    result.at(x, y, z, t) = value;
#ifndef NDEBUG
                    cout << "Computed timelike plaquette at {"
                         << x << ", " << y << ", " << z << ", " << t << "}"
                         << " (index: " << result.index4DtoIndex1D(x, y, z, t)
                         << ") = " << std::setprecision(4) << std::fixed << value << "." << endl;
#endif
                }
            }
        }
    }
    return result;
}

///	Calculate the average spacelike plaquettes at each site on the lattice for a given time slice
///
///	Calculates the plaquettes in the XY, XZ and YZ planes and returns the average
///	at each site and stores them as a scalar volume.
///
///	@param coordinate: a x,y,z,t coordinate in Euclidean space-time.
///	@return ScalarVolume: the average spacelike plaquette at each site on the lattice
/// \author		Dean
/// \author		Dean: update to use C++11 [20-10-2015]
///
Lattice::Array4f Lattice::calculateSpaceLikePlaquetteField() const
{
    Array4f result(m_values.size_x(), m_values.size_y(), m_values.size_z(), m_values.size_w());

    for (auto x = 0ul; x < m_values.size_x(); ++x)
    {
        for (auto y = 0ul; y < m_values.size_y(); ++y)
        {
            for (auto z = 0ul; z < m_values.size_z(); ++z)
            {
                for (auto t = 0ul; t < m_values.size_w(); ++t)
                {
                    auto value = CalculateSpaceLikePlaquettesAtSite({ x, y, z, t });
                    assert((value >= -1.0) && (value <= +1.0));

                    //	Put into the range 0.0..+2.0
                    value = 1.0 - value;
                    assert((value >= 0.0) && (value <= +2.0));

                    //	Finally put into the range 0.0..+1.0
                    value /= 2.0;
                    assert((value >= 0.0) && (value <= 1.0));

                    result.at(x, y, z, t) = value;
#ifndef NDEBUG
                    cout << "Computed spacelike plaquette at {"
                         << x << ", " << y << ", " << z << ", " << t << "}"
                         << " (index: " << result.index4DtoIndex1D(x, y, z, t)
                         << ") = " << std::setprecision(4) << std::fixed << value << "." << endl;
#endif
                }
            }
        }
    }
    return result;
}

///
/// \brief Lattice::at
/// \param x
/// \param y
/// \param z
/// \param t
/// \return
/// \since		21-10-2015
/// \author		Dean
///
LatticeSite Lattice::at(const size_type& x,
                        const size_type& y,
                        const size_type& z,
                        const size_type& t) const
{
    assert((x < m_values.size_x()) && (y < m_values.size_y()) && (z < m_values.size_z()) && (t < m_values.size_w()));

    return m_values.at(x, y, z, t);
}


///
/// \brief Lattice::at
/// \param x
/// \param y
/// \param z
/// \param t
/// \return
/// \since		21-10-2015
/// \author		Dean
///
LatticeSite& Lattice::at(const size_type& x,
                         const size_type& y,
                         const size_type& z,
                         const size_type& t)
{
    assert((x < m_values.size_x()) && (y < m_values.size_y()) && (z < m_values.size_z()) && (t < m_values.size_w()));

    return m_values.at(x, y, z, t);
}

///	Constructor
///
///	Initialize a lattice with the specified dimensions.
///
///	@param xDim: an unsigned integer representing the number
///	of sites on the x-axis
///	@param yDim: an unsigned integer representing the number
///	of sites on the y-axis
///	@param zDim: an unsigned integer representing the number
///	of sites on the z-axis
///	@param tDim: an unsigned integer representing the number
///	of sites on the t-axis
Lattice::Lattice(const size_type& xDim,
                 const size_type& yDim,
                 const size_type& zDim,
                 const size_type& tDim)
{
    cout << __func__ << endl;

    m_values = Array4ls(xDim, yDim, zDim, tDim);
}

///	Constructor
///
///	Initialize a lattice with the specified dimensions.
///
///	@param xyzDim: an unsigned integer representing the number
///	of sites on the x, y and z axes
///	@param tDim: an unsigned integer representing the number
///	of sites on the t-axis
Lattice::Lattice(const size_type& xyzDim, const size_type& tDim)
    : Lattice(xyzDim, xyzDim, xyzDim, tDim) { }

///	Accessor
///
///	Return the lattice site with the specified coorinate quad.
///
///	@param latticeCoordinates: the coordinates on the lattice of
///	the site to be retrieved.  Recomended to be called using the
///	<i>myLattice[{x,y,z,t}]</i> convention.
///	@return a Lattice site <b>(Read-only)</b>.
LatticeSite Lattice::operator [](Index4 latticeCoordinates) const
{
    return m_values.at(latticeCoordinates.x,
                       latticeCoordinates.y,
                       latticeCoordinates.z,
                       latticeCoordinates.t);
}

///	Accessor
///
///	Return the lattice site with the specified coorinate quad.
///
///	@param latticeCoordinates: the coordinates on the lattice of
///	the site to be retrieved.  Recomended to be called using the
///	<i>myLattice[{x,y,z,t}]</i> convention.
///	@return a Lattice site with write access.
LatticeSite& Lattice::operator [](Index4 latticeCoordinates)
{
    return m_values.at(latticeCoordinates.x,
                       latticeCoordinates.y,
                       latticeCoordinates.z,
                       latticeCoordinates.t);
}

///	Output the Lattice values to the console or a file
///
///	Iterate through the entire lattice and return the ComplexMatrix for
///	each link at every site.  Output can be to the console or to a
///	file.
///
///	@param filename: the file to write the values to.  If blank output
///	will be via the console.
/// \since		21-10-2015
/// \author		Dean
///
string Lattice::ToString() const
{
    stringstream result;

    //	Loop through the lattice values
    for (auto x = 0; x < m_values.size_x(); ++x)
    {
        for (auto y = 0; y < m_values.size_y(); ++y)
        {
            for (auto z = 0; z < m_values.size_z(); ++z)
            {
                for (auto t = 0; t < m_values.size_w(); ++t)
                {
                    result << "[";
                    result << x << ", " << y << ", " << z << ", " << t;
                    result << "]";
                    result << endl;
                    for (auto d = 0; d < 4; ++d)
                    {
                        result << "Link " << d << endl;
                        result << m_values.at(x, y, z, t)[d].ToString();
                        result << endl;
                    }
                }
            }
        }
    }

    return result.str();
}

///	Calculatte the plaquette value
///
///	Using the supplied coordinates, calculate the plaquette value by
/// completing a path in the two supplied directions.  <b>Note:
///	the two directions must differ to complete a closed loop.</b>
///
///	@param siteA: the origin of the plaquette closed loop
///	@param dir1: the direction to travel in on the outward journey
///	@param dir2: the direction to travel after moving one step in dir1
///	@return: the SU(2) matrix representing the plaquette
///	value in the given plane
ComplexMatrix Lattice::CalculatePlaquette(Index4 siteA, LinkDirection dir1, LinkDirection dir2) const
{
    assert(dir1 != dir2);

    //	Find coordinates of siteB
    Index4 siteB = siteA;

    switch (dir1)
    {
    case LinkDirection::X:
        if (siteA.x < m_values.size_x() - 1)
            siteB.x++;
        else
            siteB.x = 0;
        break;
    case LinkDirection::Y:
        if (siteA.y < m_values.size_y() - 1)
            siteB.y++;
        else
            siteB.y = 0;
        break;
    case LinkDirection::Z:
        if (siteA.z < m_values.size_z() - 1)
            siteB.z++;
        else
            siteB.z = 0;
        break;
    case LinkDirection::T:
        if (siteA.t < m_values.size_w() - 1)
            siteB.t++;
        else
            siteB.t = 0;
        break;
    default:
        fprintf(stderr, "Invalid link index: %i.\n", dir1);
        throw 0xFF;
    }

    //	Find coordinates of siteB
    Index4 siteD = siteA;

    switch (dir2)
    {
    case LinkDirection::X:
        if (siteA.x < m_values.size_x() - 1)
            siteD.x++;
        else
            siteD.x = 0;
        break;
    case LinkDirection::Y:
        if (siteA.y < m_values.size_y() - 1)
            siteD.y++;
        else
            siteD.y = 0;
        break;
    case LinkDirection::Z:
        if (siteA.z < m_values.size_z() - 1)
            siteD.z++;
        else
            siteD.z = 0;
        break;
    case LinkDirection::T:
        if (siteA.t < m_values.size_w() - 1)
            siteD.t++;
        else
            siteD.t = 0;
        break;
    default:
        fprintf(stderr, "Invalid link index: %i.\n", dir1);
        throw 0xFF;
    }

    //	Find coordinates of siteC - using siteB as a reference
    Index4 siteC = siteB;

	switch (static_cast<size_t>(dir2))
    {
	case 0:
        if (siteB.x < m_values.size_x() - 1)
            siteC.x++;
        else
            siteC.x = 0;
        break;
    case 1:
        if (siteB.y < m_values.size_y() - 1)
            siteC.y++;
        else
            siteC.y = 0;
        break;
    case 2:
        if (siteB.z < m_values.size_z() - 1)
            siteC.z++;
        else
            siteC.z = 0;
        break;
    case 3:
        if (siteB.t < m_values.size_w() - 1)
            siteC.t++;
        else
            siteC.t = 0;
        break;
    default:
        fprintf(stderr, "Invalid link index: %i.\n", dir2);
        throw 0xFF;
    }


    //fprintf(stdout, "siteA: %i %i %i %i\n", siteA.x, siteA.y, siteA.z, siteA.t);
    //fprintf(stdout, "siteB: %i %i %i %i\n", siteB.x, siteB.y, siteB.z, siteB.t);
    //fprintf(stdout, "siteC: %i %i %i %i\n", siteC.x, siteC.y, siteC.z, siteC.t);
    //fprintf(stdout, "siteD: %i %i %i %i\n", siteD.x, siteD.y, siteD.z, siteD.t);


    /**
        *      sD<=========sC
        *      ||          /\
        *      ||          ||
        *      ||          ||
        *      \/          ||
        *      sA=========>sB
        **/
    const LatticeSite latticeSiteA = m_values.at(siteA.x, siteA.y, siteA.z, siteA.t);
    const LatticeSite latticeSiteB = m_values.at(siteB.x, siteB.y, siteB.z, siteB.t);
    const LatticeSite latticeSiteC = m_values.at(siteC.x, siteC.y, siteC.z, siteC.t);
    const LatticeSite latticeSiteD = m_values.at(siteD.x, siteD.y, siteD.z, siteD.t);

    const auto a = latticeSiteA[dir1];
    const auto b = latticeSiteB[dir2];
    const auto c = latticeSiteD[dir1].ConjugateTranspose();
    const auto d = latticeSiteA[dir2].ConjugateTranspose();

    const ComplexMatrix w = a * b * c * d;

    //double s = (0.5 * w.Trace().Real());

    assert(w.Trace().imag() == 0.0);
    assert((w.Trace().real() >= -2.0) && (w.Trace().real() <= +2.0));

    return w;
}

///	\brief			Calculate the polyakov loop at a given site
///
///	\details		For a given coordinate loop through all the values
///					in the 't' dimension and return the total.
///
///	\param coordinate	three values representing the x,y,z  position in
///						Euclidean space-time.
///	\return double precision floating point that is the real part of the
///	trace of the total sum of links in the specified direction.
///
///	\author			Dean
///	\author			Dean: only loop over the 't' domain and update to use
///					C++11 features [08-10-2015]
///
double Lattice::CalculatePolyakovLoopAtSite(const size_type& x, const size_type& y, const size_type& z) const
{
    auto total = (m_values.at(x, y, z, 0)[LinkDirection::T]);

    for (auto t = 1; t < m_values.size_w(); ++t)
    {
        total *= (m_values.at(x, y, z, t).LinkT());

        //cout << total.ToString() << endl;
    }
    return 0.5 * (total.Trace().real());
}

///	Apply a transformation lattice to the data
///
///	To verify that the lattice is correctly aligned it should be able to
///	stand upto random gauge transformations.  This means each site will
///	be modified by a pre-determined random unitary matrix.  Average plaquette
///	values etc should not be affeted by this operation.
///
///	@param transformationLattice a lattice of pre-determined random unitary
///	matrices
///	@return Lattice of the same dimensions of the original with each point
///	modified according to the values on the transformation lattice.
//Lattice Lattice::ApplyTransformationLattice(TransformationLattice transformationLattice) const
//{
//	Lattice result(m_values.size_x(),m_values.size_y(),m_values.size_z(),m_values.size_w());

//	for (unsigned int x1 = 0; x1 < m_values.size_x(); ++x1)
//	{
//		for (unsigned int y1 = 0; y1 < m_values.size_y(); ++y1)
//		{
//			for (unsigned int z1 = 0; z1 < m_values.size_z(); ++z1)
//			{
//				for (unsigned int t1 = 0; t1 < m_values.size_w(); ++t1)
//				{
//					for (unsigned int dir = 0; dir < 4; ++dir)
//					{
//						//	Find the coordinates of the
//						//	neighbouring site
//						unsigned int x2 = x1;
//						unsigned int y2 = y1;
//						unsigned int z2 = z1;
//						unsigned int t2 = t1;

//						switch (dir)
//						{
//						case 0:
//							//	X-direction
//							if (x1 < m_values.size_x() - 1)
//								x2++;
//							else
//								x2 = 0;
//							break;
//						case 1:
//							//	Y-direction
//							if (y1 < m_values.size_y() - 1)
//								y2++;
//							else
//								y2 = 0;
//							break;
//						case 2:
//							//	Z-direction
//							if (z1 < m_values.size_z() - 1)
//								z2++;
//							else
//								z2 = 0;
//							break;
//						case 3:
//							//	T-direction
//							if (t1 < m_values.size_w() - 1)
//								t2++;
//							else
//								t2 = 0;
//							break;
//						}

//						ComplexMatrix omega = transformationLattice[{x1, y1, z1, t1}];
//						ComplexMatrix u = latticeSites[x1][y1][z1][t1][dir];
//						ComplexMatrix omegaDag = transformationLattice[{x2, y2, z2, t2}].ConjugateTranspose();

//						result[{x1, y1, z1, t1}][dir] = omega * u * omegaDag;
//					}
//				}
//			}
//		}
//	}
//	return result;
//}

///	Calculate the average of the spatial plaquettes at the given site
///
///	Calculates the plaquettes in the XY, XZ and YZ planes and then
///	returns the average.
///
///	@param coordinate: a x,y,z,t coordinate in Euclidean space-time.
///	@return double: the average plaquette across the three spatial planes.
double Lattice::CalculateSpaceLikePlaquettesAtSite(const Index4 &coordinate)const
{
    const auto x = coordinate.x;
    const auto y = coordinate.y;
    const auto z = coordinate.z;
    const auto t = coordinate.t;

    ComplexMatrix result(2,2);

    //fprintf(stdout, "Site [%i, %i, %i, %i]:\n", x,y,z,t);

    //cout << "Zero matrix: " << result.ToString() << endl;

    result += CalculatePlaquette({ x, y, z, t }, LinkDirection::X, LinkDirection::Y);
    result += CalculatePlaquette({ x, y, z, t }, LinkDirection::X, LinkDirection::Z);
    result += CalculatePlaquette({ x, y, z, t }, LinkDirection::Y, LinkDirection::Z);

    //cout << "Resultant matrix: " << result.ToString() << endl;

    auto plaq = 0.5 * result.Trace().real();
    plaq /= 3.0;

    assert((plaq >= -1.0) && (plaq <= +1.0));

    return plaq;
}

double Lattice::CalculateTimeLikePlaquettesAtSite(const Index4 &coordinate)const
{
    const auto x = coordinate.x;
    const auto y = coordinate.y;
    const auto z = coordinate.z;
    const auto t = coordinate.t;

    ComplexMatrix result(2,2);

    //fprintf(stdout, "Site [%i, %i, %i, %i]:\n", x, y, z, t);

    result += CalculatePlaquette({ x, y, z, t }, LinkDirection::X, LinkDirection::T);
    result += CalculatePlaquette({ x, y, z, t }, LinkDirection::Y, LinkDirection::T);
    result += CalculatePlaquette({ x, y, z, t }, LinkDirection::Z, LinkDirection::T);

    auto plaq = 0.5 * result.Trace().real();
    plaq /= 3.0;

    assert((plaq >= -1.0) && (plaq <= +1.0));

    return plaq;
}

double Lattice::CalculateTopologicalChargeDensityAtSite(const Index4 &coordinate)const
{
    //fprintf(stdout, "Site [%i, %i, %i, %i]: \n\n", coordinate.x, coordinate.y, coordinate.z, coordinate.t);

    //	First we need to calculate the 6 unique plaqettes for the lattice site
    const auto plaqXY = CalculatePlaquette(coordinate, LinkDirection::X, LinkDirection::Y);
    const auto plaqXZ = CalculatePlaquette(coordinate, LinkDirection::X, LinkDirection::Z);
    const auto plaqXT = CalculatePlaquette(coordinate, LinkDirection::X, LinkDirection::T);
    const auto plaqYZ = CalculatePlaquette(coordinate, LinkDirection::Y, LinkDirection::Z);
    const auto plaqYT = CalculatePlaquette(coordinate, LinkDirection::Y, LinkDirection::T);
    const auto plaqZT = CalculatePlaquette(coordinate, LinkDirection::Z, LinkDirection::T);

    //	Take the difference between the plaquette and it's conjugate transpose
    const auto diffPlaqXY = plaqXY - plaqXY.ConjugateTranspose();
    const auto diffPlaqXZ = plaqXZ - plaqXZ.ConjugateTranspose();
    const auto diffPlaqXT = plaqXT - plaqXT.ConjugateTranspose();
    const auto diffPlaqYZ = plaqYZ - plaqYZ.ConjugateTranspose();
    const auto diffPlaqYT = plaqYT - plaqYT.ConjugateTranspose();
    const auto diffPlaqZT = plaqZT - plaqZT.ConjugateTranspose();

    //	Outtput debug data
    /*
        fprintf(stdout, "difPlaqXY = \n%s\n", diffPlaqXY.ToString().c_str());
        fprintf(stdout, "difPlaqXZ = \n%s\n", diffPlaqXZ.ToString().c_str());
        fprintf(stdout, "difPlaqXT = \n%s\n", diffPlaqXT.ToString().c_str());
        fprintf(stdout, "difPlaqYZ = \n%s\n", diffPlaqYZ.ToString().c_str());
        fprintf(stdout, "difPlaqYT = \n%s\n", diffPlaqYT.ToString().c_str());
        fprintf(stdout, "difPlaqZT = \n%s\n", diffPlaqZT.ToString().c_str());
        */
    //	Calculate f * fdual
    double ffdd = 0;
    ffdd += (diffPlaqXY * diffPlaqZT).Trace().real();
    ffdd -= (diffPlaqXZ * diffPlaqYT).Trace().real();
    ffdd += (diffPlaqXT * diffPlaqYZ).Trace().real();

    //fprintf(stdout, "ffdd = %f\n\n", ffdd);

    return 2*ffdd;
}




///	Calculates the Magnetic field by taking the component spacelike plaquettes
///	these are then arranged as a vector
Lattice::Vec3 Lattice::CalculateMagneticFieldAtSite(const Index4 &coordinate) const
{
    //	TODO: check that this is the correct evaluation of the plaquette
    auto bx = 0.5 * CalculatePlaquette(coordinate, LinkDirection::Y, LinkDirection::Z).Trace().real();
    auto by = 0.5 * CalculatePlaquette(coordinate, LinkDirection::Z, LinkDirection::X).Trace().real();
    auto bz = 0.5 * CalculatePlaquette(coordinate, LinkDirection::X, LinkDirection::Y).Trace().real();

    return { bx, by, bz };
}

/////	Calculates the Electric field by taking the component timelike plaquettes
/////	these are then arranged as a vector
Lattice::Vec3 Lattice::CalculateElectricFieldAtSite(const Index4 &coordinate) const
{
    //	TODO: check that this is the correct evaluation of the plaquette
    auto ex = 0.5 * CalculatePlaquette(coordinate, LinkDirection::X, LinkDirection::T).Trace().real();
    auto ey = 0.5 * CalculatePlaquette(coordinate, LinkDirection::Y, LinkDirection::T).Trace().real();
    auto ez = 0.5 * CalculatePlaquette(coordinate, LinkDirection::Z, LinkDirection::T).Trace().real();

    return { ex, ey, ez };
}

//	Creates a lattice structure and fills it using the passed parser
//Lattice::Lattice(const ParserGeneric* parser)
//	: Lattice(parser->ParsedFileAttributes().m_values.size_x(), parser->ParsedFileAttributes().m_values.size_y(), parser->ParsedFileAttributes().m_values.size_z(), parser->ParsedFileAttributes().m_values.size_w())
//{
//	FillLattice(parser);
//}

///	Calculate the average plaquette for each plane
///
///	Iterate through all values on the lattice and calculate
///	the average plaquette for each plane.
///
///	@return an AveragePlaquetteMatrix structure that holds
///	the average value for each plane in 4 dimensions.
AveragePlaquetteMatrix Lattice::CalculateAveragePlaquettes() const
{
    AveragePlaquetteMatrix result;
    unsigned int samples = 0;

    //	Loop through X, Y, Z and T planes
    for (auto x = 0ul; x < m_values.size_x(); ++x)
    {
        for (auto y = 0ul; y < m_values.size_y(); ++y)
        {
            for (auto z = 0ul; z < m_values.size_z(); ++z)
            {
                for (auto t = 0ul; t < m_values.size_w(); ++t)
                {
                    //	Loop through link directions
                    for (auto dir1 = 0; dir1 < 4; ++dir1)
                    {
                        for (auto dir2 = 0; dir2 < 4; ++dir2)
                        {
                            LinkDirection d1;
                            LinkDirection d2;

                            switch (dir1)
                            {
                            case 0:
                                d1 = LinkDirection::X;
                                break;
                            case 1:
                                d1 = LinkDirection::Y;
                                break;
                            case 2:
                                d1 = LinkDirection::Z;
                                break;
                            case 3:
                                d1 = LinkDirection::T;
                                break;
                            default:
                                assert(false);
                            }

                            switch (dir2)
                            {
                            case 0:
                                d2 = LinkDirection::X;
                                break;
                            case 1:
                                d2 = LinkDirection::Y;
                                break;
                            case 2:
                                d2 = LinkDirection::Z;
                                break;
                            case 3:
                                d2 = LinkDirection::T;
                                break;
                            default:
                                assert(false);
                            }

                            //	Skip non-unique directions
                            if (dir1 != dir2)
                                result[make_pair(d1, d2)] += (0.5 * CalculatePlaquette({ x, y, z, t }, (LinkDirection)dir1, (LinkDirection)dir2).Trace().real());
                        }
                    }

                    //	Used for calculation averages
                    ++samples;
                }
            }
        }
    }

    //  Take the totals and calculate the averages
    for (auto dir1 = 0; dir1 < 4; ++dir1)
    {
        for (auto dir2 = 0; dir2 < 4; ++dir2)
        {
            LinkDirection d1;
            LinkDirection d2;

            switch (dir1)
            {
            case 0:
                d1 = LinkDirection::X;
                break;
            case 1:
                d1 = LinkDirection::Y;
                break;
            case 2:
                d1 = LinkDirection::Z;
                break;
            case 3:
                d1 = LinkDirection::T;
                break;
            default:
                assert(false);
            }

            switch (dir2)
            {
            case 0:
                d2 = LinkDirection::X;
                break;
            case 1:
                d2 = LinkDirection::Y;
                break;
            case 2:
                d2 = LinkDirection::Z;
                break;
            case 3:
                d2 = LinkDirection::T;
                break;
            default:
                assert(false);
            }

            if (dir1 != dir2)
            {
                result[make_pair(d1, d2)] /= samples;
            }
        }
    }

    return result;
}
