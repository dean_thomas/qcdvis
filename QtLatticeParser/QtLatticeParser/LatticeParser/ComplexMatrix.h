#ifndef COMPLEX_MATRIX_H
#define COMPLEX_MATRIX_H

#include "DoubleMatrix.h"
#include "AbstractMatrix.h"

#include <complex>
#include <cassert>

using LinearAlgebra::AbstractMatrix;
//using LinearAlgebra::Complex;
using LinearAlgebra::DoubleMatrix;


namespace LinearAlgebra
{
	class ComplexMatrix : public AbstractMatrix<std::complex<double>>
	{
			using size_type = size_t;
			using Complex = std::complex<double>;

#pragma region Constructors inherited from AbstractMatrix
		public:
			///	Conversion from an abstract matrix
			//	Could this work? No need for call to base constructor...
			//	http://en.cppreference.com/w/cpp/language/reinterpret_cast
			ComplexMatrix(const AbstractMatrix<Complex>& rhs)
				: AbstractMatrix(rhs){}

			///	Directly inherited constructors
			ComplexMatrix()
				: AbstractMatrix() {}
			ComplexMatrix(const size_type& rows, const size_type& cols)
				: AbstractMatrix<Complex>(rows, cols){}
			ComplexMatrix(const size_type& rows, const size_type& cols, Complex value)
				: AbstractMatrix<Complex>(rows, cols, value){}
//			ComplexMatrix(const size_type& rows, const size_type& cols, Complex values[])
//				: AbstractMatrix<Complex>(rows, cols, values){}
			ComplexMatrix(const size_type& rows, const size_type& cols, std::vector<Complex> values)
				: AbstractMatrix<Complex>(rows, cols, values){}
			ComplexMatrix(Complex e11, Complex e12, Complex e21, Complex e22)
				: AbstractMatrix<Complex>(e11, e12, e21, e22){}
			ComplexMatrix(Complex e11, Complex e12, Complex e13, Complex e21, Complex e22, Complex e23, Complex e31, Complex e32, Complex e33)
				: AbstractMatrix<Complex>(e11, e12, e13, e21, e22, e23, e31, e32, e33){}
			ComplexMatrix(Complex e11, Complex e12, Complex e13, Complex e14, Complex e21, Complex e22, Complex e23, Complex e24, Complex e31, Complex e32, Complex e33, Complex e34, Complex e41, Complex e42, Complex e43, Complex e44)
				: AbstractMatrix<Complex>(e11, e12, e13, e14, e21, e22, e23, e24, e31, e32, e33, e34, e41, e42, e43, e44){}

#pragma endregion Constructors inherited from AbstractMatrix
#pragma region Public functions inherited from AbstractMatrix
			//	Methods with modified overrides in ComplexMatrix that
			//	should allow access to the implementation in AbstractMatrix also
		public:
			using AbstractMatrix::MultiplyRow;
			using AbstractMatrix::AddScalarMultipleOfRow;

#pragma endregion
#pragma region Public Functions specific to ComplexMatrix
			///	Return the matrix as a string
			///
			///	Iteratrate through this matrix and output as
			///	a string of Complex numbers on multiple lines
			///
			///	@param targetLength: the intended length of the string
			///	@param precision: the number of decimal places in the
			///	ComplexNumber elements
			///	@return: a string representing the matrix
			///
			virtual string ToString()
			{
				unsigned int targetLength = 10;
				//unsigned int precision = 4;

				stringstream ss;

				//	Work out the intended length per Complex number string part
				//	a length of 7 will allow upto 4 places after decimal point
				//	for a 0-10 number.
				unsigned int elementLength = targetLength;

				//	Reserve space for leading sign (1), central sign (3) and trailing 'i' (1)
				elementLength -= 5;

				//	Element length should be half the remainder
				elementLength /= 2;

				for (unsigned int r = 0; r < m_rows; ++r)
				{
					ss << "|";
					ss << " ";

					for (unsigned int c = 0; c < m_cols; ++c)
					{
						ss << m_values.at(indexOf(r, c));
						ss << "  ";
					}
					ss << "|";
					ss << "\n";
				}

				return ss.str();
			}

			///	Return a Matlab formatted string
			///
			/// Return a string representative of
			/// the ComplexMatrix formatted for use
			/// in Matlab
			string ToMatlabString()
			{
				stringstream ss;

				ss << "[";

				for (unsigned int r = 0; r < m_rows; ++r)
				{
					for (unsigned int c = 0; c < m_cols; ++c)
					{
						ss << " ";
						//ss << at(indexOf(r, c)).ToMatlabString();
					}

					ss << ";";
				}

				ss << " ]";
				return ss.str();
			}

			///	Tests if the matrix is Hermitian
			///
			/// Checks if the Hermitian property is upheld by the
			/// matrix by checking that the Conjugate Transpose
			///	equals the original form
			///
			///	@result: boolean, true if the matrix is Hermitian
			bool IsHermitian() const
			{
				if (IsSquare())
				{
					ComplexMatrix conjTrans = ConjugateTranspose();

					if (conjTrans == (*this))
					{
						return true;
					}
				}
				return false;
			}

			///	Return the conjugate transpose of the matrix
			///
			///	Return the matrix that represents the conjugate transpose
			///	of the matrix - where elements either side of the
			///	diagonal swap position and are the complex conjugate
			///	of the exisiting value.  If the matrix is square,
			///	a square matrix of the same order is returned.  If
			///	not square a <i>r x c</i> matrix will return an
			///	<i>c x r </i> matrix.
			///
			///	@return a ComplexMatrix which is this matrix with
			///	complex conjugate elements swapped either
			///	side of the diagonal.
			ComplexMatrix ConjugateTranspose() const
			{
				using std::conj;

				ComplexMatrix result(m_cols, m_rows);

				for (unsigned int r = 0; r < m_rows; ++r)
				{
					for (unsigned int c = 0; c < m_cols; ++c)
					{
						result.m_values.at(indexOf(c, r)) = conj(m_values.at(indexOf(r, c)));
					}
				}
				return result;
			}

			///	Tests if the matrix is Unitary
			///
			///  Checks if the Unitary property is upheld by the
			///  matrix by checking that multiplying by its
			///  inverse equals the identity matrix
			///
			///	@result: boolean, true if the matrix is Unitary
//			bool IsUnitary() const
//			{
//				if (IsSquare())
//				{
//					ComplexMatrix identityMatrix = ComplexMatrix::Identity(rows);
//					ComplexMatrix u_inverseU = Inverse() * (*this);

//					return (u_inverseU.RoundDP(15) == identityMatrix);
//				}
//				else
//				{
//					return false;
//				}
//			}

			/**
		*  Return the Inverse of the matrix
		*  by first converting to 4x4 real
		*  matrix, calculating the inverse
		*  and then converting back to a
		*  complex matric
		**/
//			ComplexMatrix Inverse() const
//			{
//				assert(IsSquare());
//				assert(m_rows == 2);

//				//  Convert to a 4x4 real matrix
//				DoubleMatrix doubleMatrix(m_values.at(indexOf(0, 0)).real(), m_values.at(indexOf(0, 1)).real(), m_values.at(indexOf(0, 0)).imag(), m_values.at(indexOf(0, 1)).imag(),
//										  m_values.at(indexOf(1, 0)).real(), m_values.at(indexOf(1, 1)).real(), m_values.at(indexOf(1, 0)).imag(), m_values.at(indexOf(1, 1)).imag(),
//										  -m_values.at(indexOf(0, 0)).imag(), -m_values.at(indexOf(0, 1)).imag(), m_values.at(indexOf(0, 0)).real(), m_values.at(indexOf(0, 1)).real(),
//										  -m_values.at(indexOf(1, 0)).imag(), -m_values.at(indexOf(1, 1)).imag(), m_values.at(indexOf(1, 0)).real(), m_values.at(indexOf(1, 1)).real());

//				//  Invert that
//				DoubleMatrix invertedRealMatrix = doubleMatrix.Inverse();

//				//  |   a   b   w   x   |
//				//  |   c   d   y   z   |
//				//  |   -w  -x  a   b   |
//				//  |   -y  -z  c   d   |
//				//
//				//  becomes
//				//
//				//  |   a + wi  b + xi  |
//				//  |   c + yi  d + zi  |
//				//
//				//  Then convert back to a complex matrix
//				ComplexMatrix result = ComplexMatrix(
//							Complex(invertedRealMatrix[{1, 1}], invertedRealMatrix[{1, 3}]),
//						Complex(invertedRealMatrix[{1, 2}], invertedRealMatrix[{1, 4}]),
//						Complex(invertedRealMatrix[{2, 1}], invertedRealMatrix[{2, 3}]),
//						Complex(invertedRealMatrix[{2, 2}], invertedRealMatrix[{2, 4}]));

//				return result;

//			}

#pragma endregion Public Functions specific to ComplexMatrix
#pragma region Public operators inherited from AbstractMatrix
			using AbstractMatrix::operator*;
			using AbstractMatrix::operator*=;
#pragma endregion Public operators inherited from AbstractMatrix
#pragma region Public Operators with double arguments

			///	Multiply a single row by a double precision value
			///
			///	Multiply each value in a row by a double precision
			///	floating point number. <b>Note: multiplication by
			///	a Complex is defined in AbstractMatrix</b>.
			///
			///	@param row: the row to be multiplied.
			///	@param multiplier: the non-zero value to multiply the row
			///	by.
			///	@return ComplexMatrix: the matrix after the row
			///	multiplication applied.
			///	@throws MAT_INVALID_ROW_INDEX: if the row number is
			///	invalid.
			/// @throws MAT_INVALID_OPERATION: if the multiplier is zero.
//			ComplexMatrix MultiplyRow(const unsigned int row, const double multiplier)
//			{
//				assert (RowIndexIsValid(row));
//				//assert (!isZero(multiplier));

//				ComplexMatrix result = *this;

//				for (unsigned int c = 0; c < m_cols; ++c)
//				{
//					result.m_values.at(result.indexOf(row - 1, c)) *= multiplier;
//				}
//				return result;
//			}

//			ComplexMatrix AddScalarMultipleOfRow(const unsigned int destinationRow, const unsigned int sourceRow, const double sourceRowMultiplier)
//			{
//				assert(RowIndexIsValid(destinationRow));
//				assert(RowIndexIsValid(sourceRow));
//				assert(destinationRow != sourceRow);
//				//assert(!isZero(sourceRowMultiplier));

//				//	Copy the source row
//				ComplexMatrix sourceRowElements = RowWithIndex(sourceRow);

//				//	Multiply the source row
//				sourceRowElements *= sourceRowMultiplier;

//				//	Copy original matrix
//				ComplexMatrix result = *this;

//				//	Add the content of the result and modified source row
//				for (unsigned int c = 0; c < m_cols; ++c)
//				{
//					result.m_values.at(result.indexOf(destinationRow - 1, c)) += sourceRowElements.m_values.at(sourceRowElements.indexOf(0, c));
//				}
//				return result;
//			}

		public:
			///ComplexMatrix-double multiplication
			///
			///	Multiply this ComplexMAtrix by a double precision scalar value
			///
			///	@param rhs: the double precision scalar value to be multiplied by
			///	@result: a ComplexMatrix that is the result of mutliplying
			///	a Complex and a double value together
			ComplexMatrix operator *(const double rhs) const
			{
				ComplexMatrix result(m_rows, m_cols);

				//	Access each element linearly
				for (unsigned int i = 0; i < m_values.size(); i++)
				{
					result.m_values[i] = m_values.at(i) * rhs;
				}
				return result;
			}

#pragma endregion Public Operators with double arguments
#pragma region Static Functions
			///	Generate a random unitary matrix
			///
			///	Create a random unitary matrix
			/// ie the matrix multiplied by its inverse
			///  will equal the identity matrix
			///
			///	@param order: the order of the square matrix returned <b>(currently limited
			///	to <i>2 x 2</i> only.</b>
			///	@return a ComplexMatrix with the property of being unitary
			static ComplexMatrix RandomUnitaryMatrix(int order)
			{
				assert(order == 2);


				//  Four random numbers -1..+1
				double r0 = (2 * rand()) - 1;
				double r1 = (2 * rand()) - 1;
				double r2 = (2 * rand()) - 1;
				double r3 = (2 * rand()) - 1;

				//  Normalize the numbers
				double n0 = r0 / sqrt((r0 * r0) + (r1 * r1) + (r2 * r2) + (r3 * r3));
				double n1 = r1 / sqrt((r0 * r0) + (r1 * r1) + (r2 * r2) + (r3 * r3));
				double n2 = r2 / sqrt((r0 * r0) + (r1 * r1) + (r2 * r2) + (r3 * r3));
				double n3 = r3 / sqrt((r0 * r0) + (r1 * r1) + (r2 * r2) + (r3 * r3));

				//  Turn four values into complex matrix
				Complex c11(n0, n3);
				Complex c12(n2, n1);
				Complex c21(-n2, n1);
				Complex c22(n0, -n3);

				//  Return the complex matrix
				return ComplexMatrix(c11, c12, c21, c22);
			}
#pragma endregion Static Functions
	};	//ComplexMatrix

#pragma region Non-member operators

#pragma endregion
}	//	Linear-Algebra

#endif COMPLEX_MATRIX_H
