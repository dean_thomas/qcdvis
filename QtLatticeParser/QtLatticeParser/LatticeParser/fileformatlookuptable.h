#ifndef FILEFORMATLOOKUPTABLE
#define FILEFORMATLOOKUPTABLE

#include <regex>
#include <string>
#include <map>
#include <fstream>
#include <cassert>
#include <sstream>

struct FileFormatLookupTable
{
    using size_type = size_t;

    struct Format
    {
        size_type bytes;
        size_type entries;
        size_type sizeXYZ;
        size_type sizeT;
        bool isCompressed;
        bool isDouble;
    };

    std::map<const size_type, Format> m_entries;

    FileFormatLookupTable()
    {
        std::string pattern = "^(\\d+),\\s*(\\d+),\\s*(\\d+),\\s*(\\d+),"
                              "\\s*(\\d+),\\s*(\\d+),"
                              "\\s*(\\btrue\\b|\\bfalse\\b),"
                              "\\s*(\\bsingle\\b|\\bdouble\\b|\\bquad\\b)";
        std::regex regex(pattern);

        std::cout << "Loading known file formats"  << std::endl;
#ifdef __linux__
        std::ifstream inputFile("/home/dean/qcdvis/QtLatticeParser/QtLatticeParser/FileFormats.txt");
#else
        std::ifstream inputFile(R"(C:\Users\4thFloor\Documents\Qt\QtFlexibleIsosurfaces\QtLatticeParser\QtLatticeParser\FileFormats.txt)");
#endif
        while (inputFile.is_open() && (!inputFile.eof()))
        {
            std::string currentLine;
            std::getline(inputFile, currentLine);

            std::smatch regexMatch;
            std::regex_match(currentLine, regexMatch, regex);

            if (regexMatch.size() > 1)
            {
                assert(regexMatch.size() == 9);

                //	Assume our regex is correct so that we can use atoi()
                auto bytes = atoi(regexMatch[1].str().c_str());
                auto values = atoi(regexMatch[2].str().c_str());
                auto sizeX = atoi(regexMatch[3].str().c_str());
                auto sizeY = atoi(regexMatch[4].str().c_str());
                auto sizeZ = atoi(regexMatch[5].str().c_str());
                auto sizeT = atoi(regexMatch[6].str().c_str());
                auto isCompressed = (regexMatch[7].str() == "true");
                auto isDouble = (regexMatch[8].str() == "double");

                std::cout << sizeX << " " << sizeY << " " << sizeZ << " " << std::endl;

                assert((sizeX == sizeY) && (sizeX == sizeZ));

                //	Create the new format and add to the list
                Format newFormat = { bytes,
                                     values,
                                     sizeX,
                                     sizeT, isCompressed, isDouble };
                m_entries[bytes] = newFormat;

            }
        }

        if (inputFile.is_open())
            inputFile.close();

        std::cout << "Recognised file formats: " << std::endl
                  << ToString() << std::endl;

        fflush(stdout);
    }

    std::string ToString() const
    {
        std::stringstream result;

        for (auto it = m_entries.cbegin(); it != m_entries.cend(); ++it)
        {
            result << "[ " << it->second.bytes << ", ";
            result << it->second.entries << ", ";
            result << it->second.sizeXYZ << ", ";
            result << it->second.sizeT << ", ";
            result << it->second.isCompressed << ", ";
            result << it->second.isDouble << "]";
            result << std::endl;
        }
        return result.str();
    }
};

#endif // FILEFORMATLOOKUPTABLE

