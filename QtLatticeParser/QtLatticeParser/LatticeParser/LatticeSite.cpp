#include "LatticeSite.h"

///	Private initializer
///
///	Fill the lattice site with four empty
///	ComplexMatrix elements to avoid null
///	pointer errors
void LatticeSite::init()
{
	Clear();
}

///	Default constructor
///
///	Construct and initialize the site with
///	values of zero on each link.
LatticeSite::LatticeSite()
{
	init();
}

///	Destructor
LatticeSite::~LatticeSite()
{
}

///	Accesssor method
///
///	Return the link at the specified index with read-only access
///
///	@param rhs, an unsigned integer (0-3) specifying
///	with link direction to access
///	@return a ComplexMatrix representing the link at the
///	specified position
ComplexMatrix LatticeSite::operator[](const unsigned int rhs) const
{
	if (rhs < 4)
	{
		return values[rhs];
	}
	else
	{
		fprintf(stderr, "Invalid direction index (%i).\n", rhs);
		throw 0xFF;
	}
}

///	Accesssor method
///
///	Return the link at the specified index with read/write access
///
///	@param rhs, an unsigned integer (0-3) specifying
///	with link direction to access
///	@return a ComplexMatrix representing the link at the
///	specified position
ComplexMatrix& LatticeSite::operator[](const unsigned int rhs)
{
	if (rhs < 4)
	{
		return values[rhs];
	}
	else
	{
		fprintf(stderr, "Invalid direction index (%i).\n", rhs);
		throw 0xFF;
	}
}

///	Accesssor method
///
///	Return the link at the specified index with read-only access
///
///	@param rhs: a LinkDirection enumeration representing the
/// direction to be returned.
///	@return a ComplexMatrix representing the link at the
///	specified position
ComplexMatrix LatticeSite::operator[](const LinkDirection& rhs) const
{
	return values[(unsigned int)rhs];
}

///	Accesssor method
///
///	Return the link at the specified index with read/write access
///
///	@param rhs: a LinkDirection enumeration representing the
/// direction to be returned.
///	@return a ComplexMatrix representing the link at the
///	specified position
ComplexMatrix& LatticeSite::operator[](const LinkDirection& rhs)
{
	return values[(unsigned int)rhs];
}

///	Clear method
///
///	Sets the ComplexMatrix at each link direction to zero
void LatticeSite::Clear()
{
	values.clear();
	values.reserve(4);

	values.push_back(ComplexMatrix({ 0, 0 }, { 0, 0 }, { 0, 0 }, { 0, 0 }));
	values.push_back(ComplexMatrix({ 0, 0 }, { 0, 0 }, { 0, 0 }, { 0, 0 }));
	values.push_back(ComplexMatrix({ 0, 0 }, { 0, 0 }, { 0, 0 }, { 0, 0 }));
	values.push_back(ComplexMatrix({ 0, 0 }, { 0, 0 }, { 0, 0 }, { 0, 0 }));
}
