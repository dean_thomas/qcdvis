#include "MainWindow.h"
#include "ui_MainWindow.h"

//	Common to QCDVis applications
#include <Filetypes/Vol1.h>
#include <Filetypes/HVol1FileWriter.h>

//	Qt headers
#include <QFileDialog>
#include <QMessageBox>
#include <QDirIterator>
#include <QDebug>
#include <QStandardItemModel>
#include <QProgressDialog>
#include <QFutureWatcher>
#include <QThread>
#include <QtConcurrent/QtConcurrent>
#include <QHeaderView>

//	STL headers
#include <chrono>
#include <functional>
#include <algorithm>

using namespace std;

#ifdef __linux__
	#define FFDUAL_SCRIPT_PY "/home/dean/qcdvis/Python/visualize_cooling_data/visualize_ffdual.py"
	#define ACTION_SCRIPT_PY "/home/dean/qcdvis/Python/visualize_cooling_data/visualize_action.py"
#else
	#define FFDUAL_SCRIPT_PY R"|(C:\Users\4thFloor\Documents\Qt\QtFlexibleIsosurfaces\Python\visualize_cooling_data\visualize_ffdual.py)|"
	#define ACTION_SCRIPT_PY R"|(C:\Users\4thFloor\Documents\Qt\QtFlexibleIsosurfaces\Python\visualize_cooling_data\visualize_action.py)|"
#endif

void MainWindow::saveSettings() const
{
	QSettings settings("Swansea University: graphics group", "QCDVis Parser");

	//	Save the last input / output directory for next time
	settings.setValue("input dir", m_inputDir.absolutePath());
	settings.setValue("output dir", m_outputDir.absolutePath());

	//	Fields selected by the user
	settings.beginGroup("Fields");
	{
		auto count = ui->listWidget->count();
		for (auto i = 0; i < count; ++i)
		{
			auto cb = ui->listWidget->item(i);
			settings.setValue(cb->text(), cb->checkState());
		}
	}
	settings.endGroup();
}

void MainWindow::restoreSettings()
{
	QSettings settings("Swansea University: graphics group", "QCDVis Parser");

	//	Input / output directory
	auto inDir = settings.value("input dir", DEFAULT_LOAD_DIR.absolutePath()).toString();
	SetInputDir(QDir(inDir));
	auto outDir = settings.value("out dir", DEFAULT_SAVE_DIR.absolutePath()).toString();
	SetOutputDir(QDir(outDir));

	settings.beginGroup("Fields");
	{

	}
	settings.endGroup();
}

void MainWindow::closeEvent(QCloseEvent*)
{
	saveSettings();
}

///
/// \brief MainWindow::colSelect
/// \param col
/// \param checked
/// \since		02-11-2015
/// \author		Dean
///
void MainWindow::colSelect(const int& col, const bool& checked)
{
	assert(col < m_tableModel->columnCount());

	//	Set the selected status of each cell in the row
	for (auto r = 0; r < m_tableModel->rowCount(); ++r)
	{
		auto cell = m_tableModel->item(r, col);

		if (cell->isEnabled())
		{
			cell->setCheckState(checked ? Qt::Checked : Qt::Unchecked);
		}
	}
}

///
/// \brief MainWindow::rowSelect
/// \param row
/// \param checked
/// \since		02-11-2015
/// \author		Dean
///
void MainWindow::rowSelect(const int& row, const bool& checked)
{
	assert(row < m_tableModel->rowCount());

	//	Set the selected status of each cell in the row
	for (auto c = 0; c < m_tableModel->columnCount(); ++c)
	{
		auto cell = m_tableModel->item(row, c);

		if (cell->isEnabled())
		{
			cell->setCheckState(checked ? Qt::Checked : Qt::Unchecked);
		}
	}
}

///
/// \brief		Creates a context menu for the table view widget
/// \since		16-10-2015
/// \author		Dean
///
void MainWindow::createTableViewContextMenu()
{
	//	Enable entire row
	QAction* m_actionEnableAcrossEnsemble = new QAction("Enable across entire ensemble", this);
	connect(m_actionEnableAcrossEnsemble, &QAction::triggered, [&]{
		assert (m_tableModel != nullptr);

		auto index = ui->tableViewConfigurations->currentIndex();

		rowSelect(index.row(), true);
	});

	QAction* m_actionDisableAcrossEnsemble = new QAction("Disable across entire ensemble", this);
	connect(m_actionDisableAcrossEnsemble, &QAction::triggered, [&]{
		assert (m_tableModel != nullptr);

		auto index = ui->tableViewConfigurations->currentIndex();

		rowSelect(index.row(), false);
	});

	//	Enable entire column
	QAction* m_actionEnableEntireCoolingSeries = new QAction("Enable entire cooling series", this);
	connect(m_actionEnableEntireCoolingSeries, &QAction::triggered, [&]{
		assert (m_tableModel != nullptr);

		auto index = ui->tableViewConfigurations->currentIndex();

		colSelect(index.column(), true);
	});
	QAction* m_actionDisableEntireCoolingSeries = new QAction("Disable entire cooling series", this);
	connect(m_actionDisableEntireCoolingSeries, &QAction::triggered, [&]{
		assert (m_tableModel != nullptr);

		auto index = ui->tableViewConfigurations->currentIndex();

		colSelect(index.column(), false);
	});

	//	Show FFDdal for cooling series
	QAction* m_actionVisualizeFFDual = new QAction("Visual FFDual for configuration", this);
	connect(m_actionVisualizeFFDual, &QAction::triggered, [&]{
		assert (m_tableModel != nullptr);

		auto index = ui->tableViewConfigurations->currentIndex();

		qDebug() << index.column();
	});


	QAction* m_actionSeparator1 = new QAction(this);
	m_actionSeparator1->setSeparator(true);
	QAction* m_actionSeparator2 = new QAction(this);
	m_actionSeparator2->setSeparator(true);
	QAction* m_actionSeparator3 = new QAction(this);
	m_actionSeparator3->setSeparator(true);

	ui->tableViewConfigurations->addAction(m_actionEnableAcrossEnsemble);
	ui->tableViewConfigurations->addAction(m_actionDisableAcrossEnsemble);
	ui->tableViewConfigurations->addAction(m_actionSeparator1);
	ui->tableViewConfigurations->addAction(m_actionEnableEntireCoolingSeries);
	ui->tableViewConfigurations->addAction(m_actionDisableEntireCoolingSeries);
	ui->tableViewConfigurations->addAction(m_actionSeparator2);
	ui->tableViewConfigurations->addAction(ui->actionEnableAll);
	ui->tableViewConfigurations->addAction(ui->actionDisableAll);
	ui->tableViewConfigurations->addAction(m_actionSeparator3);
	ui->tableViewConfigurations->addAction(m_actionVisualizeFFDual);

	ui->tableViewConfigurations->setContextMenuPolicy(Qt::ActionsContextMenu);
}

///
/// \brief		Turns the boolean matrix of available configurations into
///				a model for display by the QTableView widget
/// \param availableConfigurations
/// \return
/// \since		16-10-2015
/// \author		Dean
///
QStandardItemModel* MainWindow::generateEnsembleModel(EnsembleData &availableConfigurations)
{
	QBrush redBrush(QColor(255, 128, 128));
	QBrush greenBrush(QColor(192, 255, 128));

	//	Create our new model
	QStandardItemModel* result = new QStandardItemModel(this);

	//	Make sure the configurations are listed in ascending order
	sort(availableConfigurations.availableConfigurations.begin(),
		 availableConfigurations.availableConfigurations.end(),
		 [](const EnsembleData::Configuration& a, const EnsembleData::Configuration& b)
	{
		//	Sort by configuration name
		return a.name < b.name;
	});

	//	Generate column headers ('conp0000', 'conp0001')
	QStringList columnHeaderLabels;
	for (auto it = availableConfigurations.availableConfigurations.cbegin();
		 it != availableConfigurations.availableConfigurations.cend();
		 ++it)
	{
		columnHeaderLabels.push_back(it->name);
	}

	//	Then assign them to the model
	result->setHorizontalHeaderLabels(columnHeaderLabels);

	//	Generate row headers ('Uncooled', 'cool 0', 'cool 1')
	QStringList rowHeaderLabels;
	rowHeaderLabels << "Uncooled";
	for (auto it = 1; it < availableConfigurations.maxCools + 1; ++it)
	{
		rowHeaderLabels.push_back(QString("cool %1").arg(it));
	}
	result->setVerticalHeaderLabels(rowHeaderLabels);

	//	Dimensions of grid
	auto colMax = availableConfigurations.availableConfigurations.size();
	auto rowMax = availableConfigurations.maxCools + 1;

	//	Fill in the grid
	for (auto col = 0; col < colMax; ++col)
	{
		for (auto row = 0; row < rowMax; ++row)
		{
			bool coolIsAvailable = false;
			QString fileAbsolutePath = "";

			auto currentConfiguration = availableConfigurations.availableConfigurations[col];

			auto coolEntry = currentConfiguration.coolingSliceFileList.find(row);

			//	Found an entry for the cooling cycle
			if (coolEntry != currentConfiguration.coolingSliceFileList.cend())
			{
				coolIsAvailable = true;
				fileAbsolutePath = coolEntry->second;
			}
			else
			{
				coolIsAvailable = false;
				fileAbsolutePath = "";
			}

			//	If a ensembled hasn't been cooled to uniform amount, we may
			//	exhaust the list of available configurations early.  In this
			//	case we can mark it as unavailable, if not query it directly.
			//if (row < availableConfigurations.availableConfigurations[col].coolingSlices.size())
			//{

			//std::find()
			//coolIsAvailable =
			//		availableConfigurations.availableConfigurations[col].coolingSlices[row];
			//}

			//	Create the new item
			auto newItem = new QStandardItem(coolIsAvailable ? "yes" : "no");
			newItem->setBackground(coolIsAvailable ? greenBrush : redBrush);
			newItem->setTextAlignment(Qt::AlignHCenter | Qt::AlignVCenter);
			newItem->setCheckable(true);
			newItem->setEnabled(coolIsAvailable);

			//	If the file is available it is stored in the data variable
			newItem->setData(fileAbsolutePath,PATH_TO_FILE_INDEX);
			newItem->setData(currentConfiguration.name, CONFIGURATION_NAME_INDEX);
			newItem->setData(row, COOLING_SLICE_INDEX);

			//	Add the item to the grid
			result->setItem(row, col, newItem);
		}
	}
	return result;
}

void MainWindow::updateMaximaTable(const QString& filename)
{
	QFileInfo fileInfo(filename);

	if (fileInfo.exists())
	{
		MinMaxFileParser minMaxParser(filename);

		auto minMaxListing = minMaxParser();
		auto tableModel = generateMinMaxTable(minMaxListing);

		ui->tableViewMinMax->setModel(tableModel);
		ui->tableViewMinMax->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
		ui->tableViewMinMax->setAlternatingRowColors(true);
	}
	else
	{
		ui->tableViewMinMax->setModel(nullptr);
	}
}

void MainWindow::setupTableViewConfigurationsActions()
{
	//	Double clicking a row/column header will select all cells in that row
	connect(ui->tableViewConfigurations->verticalHeader(), &QHeaderView::sectionDoubleClicked,
			[&](const int& rowIndex){
		rowSelect(rowIndex, true);
	});

	connect(ui->tableViewConfigurations->horizontalHeader(), &QHeaderView::sectionClicked,
			[&](const int& colIndex)
	{
		//	Convert the model index to an actual item (choose the 0th item)
		auto clickedItemIndex = ((QStandardItemModel*)ui->tableViewConfigurations->model())->index(0, colIndex);
		auto clickedItem = ((QStandardItemModel*)ui->tableViewConfigurations->model())->itemFromIndex(clickedItemIndex);
		assert(clickedItem != nullptr);

		//	Extract the cooling slice and configuration name
		auto configurationName = clickedItem->data(CONFIGURATION_NAME_INDEX);
		//auto coolingSlice = clickedItem->data(COOLING_SLICE_INDEX);

		qDebug() << "User clicked configuration header";

		emit on_clickConfiguration(configurationName.toString());

		//	Clear the selected row(s)
		ui->tableViewMinMax->selectRow(-1);
	});
	connect(ui->tableViewConfigurations->horizontalHeader(), &QHeaderView::sectionDoubleClicked,
			[&](const int& colIndex){
		colSelect(colIndex, true);
	});

	//	Clicking any cell in the grid will trigger a signal with the selected
	//	configuration name
	connect(ui->tableViewConfigurations, &QTableView::clicked,
			[&](const QModelIndex& modelIndex)
	{
		//	Convert the model index to an actual item
		auto clickedItem = ((QStandardItemModel*)ui->tableViewConfigurations->model())->itemFromIndex(modelIndex);
		assert(clickedItem != nullptr);

		//	Extract the cooling slice and configuration name
		auto configurationName = clickedItem->data(CONFIGURATION_NAME_INDEX);
		auto coolingSlice = clickedItem->data(COOLING_SLICE_INDEX);

		qDebug() << configurationName;
		qDebug() << coolingSlice;

		//	Emit signals for updating the UI etc.
		emit on_clickConfiguration(configurationName.toString());
		emit on_clickCoolingSlice(coolingSlice.toUInt());
	});

	//	When the user clicks a cell, it will return the identifier string
	//	for that configuration
	connect(this, &MainWindow::on_clickConfiguration,
			[&](const QString& configurationName)
	{
		qDebug() << "User clicked column:" << configurationName;

		//	Load the location of maxima from the maxima file generated by
		//	FORTRAN
		auto filename =
				m_inputDir.absoluteFilePath("maxima_" + configurationName + ".txt");
		updateMaximaTable(filename);

		ui->dockWidgetMinMax->setWindowTitle(QString("Location of minima and maxima (%1)").arg(configurationName));
	});

	//	When the user clicks a cell, it will return the identifier string
	//	for that configuration
	connect(this, &MainWindow::on_clickCoolingSlice,
			[&](const size_t& coolingSlice)
	{
		qDebug() << "User clicked row:" << coolingSlice;

		ui->tableViewMinMax->selectRow(coolingSlice);
	});
}

///
/// \brief		MainWindow::MainWindow
/// \param		parent
/// \since		13-10-2015
/// \author		Dean
///
MainWindow::MainWindow(QWidget *parent) :
	QMainWindow(parent),
	ui(new Ui::MainWindow)
{
	ui->setupUi(this);

	createTableViewContextMenu();

	SetInputDir(DEFAULT_LOAD_DIR);
	SetOutputDir(DEFAULT_SAVE_DIR);

	//	Setup signals and slots for the configuration table
	setupTableViewConfigurationsActions();

	connect(ui->tableViewMinMax, &QTableView::clicked,
			[&](const QModelIndex& modelIndex)
	{
		//auto row = modelIndex.row();
		//auto col ...
		//ui->tableViewConfigurations->selectRow(row);
	});

	connect(ui->tableViewConfigurations, &QTableView::clicked,
			[&](const QModelIndex& index)
	{
		//auto filePath =
		//		ui->tableViewConfigurations->model()->data(index, PATH_TO_FILE_INDEX).toString();

		//cout << filePath.toStdString() << endl;

		//executePythonHistogramScript(QString());
	});

	connect(ui->listWidget, &QListWidget::itemChanged,
			[&](){
		qDebug() << "Click";

		//	Count the number of checked items
		size_type checkCount = 0;
		for (auto i = 0; i < ui->listWidget->count(); ++i)
		{
			auto item = ui->listWidget->item(i);

			if (item->checkState() == Qt::Checked) ++checkCount;
		}

		//	Disable the ensemble buttons if no variables are checked
		ui->groupBoxComputations->setEnabled(checkCount > 0);
	});

	restoreSettings();
	//connect(ui->treeWidget, SIGNAL(itemClicked(QTreeWidgetItem*,int)),
	//		this, SLOT(onTreeWidgetItemClicked(QTreeWidgetItem*,int)));
}

///
/// \brief		MainWindow::~MainWindow
/// \since		13-10-2015
/// \author		Dean
///
MainWindow::~MainWindow()
{
	delete ui;
}

///
/// \brief		Set the application to look for data in the specified directory
/// \param		dir
/// \since		13-10-2015
/// \author		Dean
///
void MainWindow::SetInputDir(const QDir& dir)
{
	//assert(isValidEnsembleDir(dir));

	m_inputDir = dir;

#ifdef SHOW_FULLPATH_INLINE
	//	Shows the shortened and full path in the input field
	ui->lineEditInput->setText(QString("%1 [%2]").arg(dir.dirName(),
													  dir.absolutePath()));
#else
	//	Shows only the shortened name in the input field
	ui->lineEditInput->setText(dir.dirName());
#endif

	//	Set the status bar hint to the fullpath
	ui->lineEditInput->setStatusTip(dir.absolutePath());
	ui->labelInputEnsemble->setStatusTip(dir.absolutePath());

	updateConfigurationTable();
}

///
/// \brief		Set the application to output field data in the specified
///				directory
/// \param		dir
/// \since		15-10-2015
/// \author		Dean
///
void MainWindow::SetOutputDir(const QDir& dir)
{
	m_outputDir = dir;

#ifdef SHOW_FULLPATH_INLINE
	//	Shows the shortened and full path in the input field
	ui->lineEditOutput->setText(QString("%1 [%2]").arg(dir.dirName(),
													   dir.absolutePath()));
#else
	//	Shows only the shortened name in the input field
	ui->lineEditOutput->setText(dir.dirName());
#endif

	//	Set the status bar hint to the fullpath
	ui->lineEditOutput->setStatusTip(dir.absolutePath());
	ui->labelOutputDirectory->setStatusTip(dir.absolutePath());
}

///
/// \brief		Checks that the supplied directory is a valid ensemble
/// \param		dir
/// \return
/// \since		13-10-2015
/// \author		Dean
///
bool MainWindow::isValidEnsembleDir(const QDir& dir) const
{
	//	Examples:	"config.b190k1680mu0000j00s12t24",
	//				"config.b190k1680mu0700j02s16t08"
	const QRegExp configRegEx("^config.");

	return dir.dirName().contains(configRegEx);
}

///
/// \brief		Fills the ensemble table view with all the configurations and
///				cooled configurations within
/// \since		15-10-2015
/// \author		Dean
///
void MainWindow::updateConfigurationTable()
{
	//	Parse the ensemble directory
	m_currentEnsembleData = EnsembleData(m_inputDir);

	//	Turn the parsed ensemble data into a model for the table view
	m_tableModel = generateEnsembleModel(m_currentEnsembleData);
	assert (m_tableModel != nullptr);

	//	And update the table view
	ui->tableViewConfigurations->setModel(m_tableModel);
}

///
/// \brief		Launches a dialog to select where generated data should be
///				stored
/// \since		13-10-2015
/// \author		Dean
///
void MainWindow::on_actionBrowseOutput_triggered()
{
	//#define ERR_STRING 	"The selected directory is not a valid ensemble.  " \
	//	"Valid ensemble directories begin with 'config.'"

	//	Give the user the ability to select an ensemble for generation
	QFileDialog dialog;

	auto selectedDir = dialog.getExistingDirectory(0,
												   "Select output folder",
												   m_outputDir.absolutePath(),
												   QFileDialog::ShowDirsOnly);

	//	User clicked cancel
	if (selectedDir == "") return;

	QDir dir(selectedDir);
	SetOutputDir(dir);
}

///
/// \brief		Launches a dialog to select an ensemble data set
/// \since		13-10-2015
/// \author		Dean
///
void MainWindow::on_actionBrowseInput_triggered()
{
#define ERR_STRING 	"The selected directory is not a valid ensemble.  " \
	"Valid ensemble directories begin with 'config.'"

	//	Give the user the ability to select an ensemble for generation
	QFileDialog dialog;
	auto selectedDir = dialog.getExistingDirectory(0,
												   "Select ensemble folder",
												   DEFAULT_LOAD_DIR.absolutePath(),
												   QFileDialog::ShowDirsOnly);

	//	User clicked cancel
	if (selectedDir == "") return;

	QDir dir(selectedDir);
	if (!isValidEnsembleDir(dir))
	{
		//	Not an ensemble
		QMessageBox::critical(this,
							  "Incorrect directory",
							  ERR_STRING);
	}
	else
	{
		SetInputDir(dir);
	}
}

///
/// \brief		Enables all (available only) configurations globally
/// \since		16-10-2015
/// \author		Dean
///
void MainWindow::on_actionEnableAll_triggered()
{
	assert(m_tableModel != nullptr);

	for (auto r = 0; r < m_tableModel->rowCount(); ++r)
	{
		for (auto c = 0; c < m_tableModel->columnCount(); ++c)
		{
			//	Select the cell
			auto cell = m_tableModel->item(r, c);

			//	And enable it (if the configuration is present)
			if (cell->isEnabled()) cell->setCheckState(Qt::Checked);
		}
	}
}

///
/// \brief		Disables all configurations globally
/// \since		16-10-2015
/// \author		Dean
///
void MainWindow::on_actionDisableAll_triggered()
{
	assert(m_tableModel != nullptr);

	for (auto r = 0; r < m_tableModel->rowCount(); ++r)
	{
		for (auto c = 0; c < m_tableModel->columnCount(); ++c)
		{
			//	Select the cell
			auto cell = m_tableModel->item(r, c);

			//	And disanable it
			cell->setCheckState(Qt::Unchecked);
		}
	}
}

MainWindow::ConfigurationParameters MainWindow::parseConfigurationParameters(
		const QString& absolutePath) const
{
	const QRegularExpression ensembleRegex("config.b(\\d+)k(\\d+)mu(\\d+)"
										   "j(\\d+)s(\\d+)t(\\d+)");
	const QRegularExpression configRegex("cool(\\d{4})_conp(\\d{4})");


	//	For extraction of cooling number etc.
	QRegularExpressionMatch ensembleMatches;
	QRegularExpressionMatch configMatches;

	cout << __FUNCTION__ << endl;
	cout << absolutePath.toStdString() << endl;

	auto matchedEnsembleData = absolutePath.contains(ensembleRegex, &ensembleMatches);
	auto matchedConfigData = absolutePath.contains(configRegex, &configMatches);

	cout << "Matched ensemble: " << (matchedEnsembleData ? "true" : "false") << endl;
	cout << "Matched config: " << (matchedConfigData ? "true" : "false") << endl;

	if (matchedConfigData && matchedEnsembleData)
	{
		cout << "Found match" << endl;

		auto ensemble = ensembleMatches.captured(0).toStdString();
		auto conf = configMatches.captured(2).toStdString();

		//	For checking string -> int conversions
		bool ok;


		auto cools = configMatches.captured(1).toULong(&ok);
		assert(ok);

		auto b = ensembleMatches.captured(1).toULong(&ok);
		assert(ok);

		auto k = ensembleMatches.captured(2).toULong(&ok);
		assert(ok);

		auto mu = ensembleMatches.captured(3).toULong(&ok);
		assert(ok);

		auto j = ensembleMatches.captured(4).toULong(&ok);
		assert(ok);

		auto s = ensembleMatches.captured(5).toULong(&ok);
		assert(ok);

		auto t = ensembleMatches.captured(6).toULong(&ok);
		assert(ok);

		return { ensemble, conf, cools, b, k, mu, j, s, t };
	}
	return { "Unknown", "Unknown", 0, 0, 0, 0, 0, 0, 0 };
}

void MainWindow::on_actionTest_Accessors_triggered()
{
	QString filename = "E:\\Dean\\QCD Data\\Cooled Configurations\\"
					   "Pygrid\\16x08\\mu0400\\"
					   "config.b190k1680mu0400j02s16t08\\"
					   "cool0015_conp0090";

	ConfigurationParameters params = parseConfigurationParameters(filename);

	cout << "Loading file: " << filename.toStdString() << endl;
	cout << "Parameters: confname = " << params.confName << ", ";
	cout << "cools = " << params.cools << ", beta = " << params.b;
	cout << ", kappa = " << params.k << ", mu = " << params.mu;
	cout << ", di-quark source = " << params.j << endl;

	DataModel m_dataModel(filename.toStdString(),
						  params.confName,
						  params.cools,
						  params.b,
						  params.k,
						  params.mu,
						  params.j);


	std::chrono::time_point<std::chrono::system_clock> start, end;
	start = std::chrono::system_clock::now();

	//	Do calculation

	cout << "Space-like plaquette" << endl;
	HVol1FileWriter hfw(params.ensembleName,
						"Spacelike Plaquette",
						params.confName,
						params.cools);
	auto spaceLikeFilename = generateFullOutputFilename(m_outputDir,
														QString::fromStdString(params.ensembleName),
														QString::fromStdString(params.confName),
														params.cools,
														"spacelike plaquette");
	cout << "file: " << spaceLikeFilename << " saved correctly: ";
	cout << (hfw(spaceLikeFilename,
				 m_dataModel.GetSpaceLikePlaquetteField()) ? "true" : "false") << endl;

	cout << "Time-like plaquette" << endl;
	//m_dataModel.GetTimeLikePlaquetteField();

	cout << "Average plaquette" << endl;
	//m_dataModel.GetAveragePlaquetteField();

	cout << "Difference plaquette" << endl;
	//m_dataModel.GetDifferencePlaquetteField();

	cout << "Topological charge density" << endl;
	//m_dataModel.GetTopologicalChargeDensityField();

	cout << "Polyakov loop" << endl;
	auto polyakovFilename = generateFullOutputFilename(m_outputDir,
													   QString::fromStdString(params.ensembleName),
													   QString::fromStdString(params.confName),
													   params.cools,
													   "polyakov");
	//auto polyakovFile = polyakovDir + "\\polyakov_" +
	Vol1Writer<Decimal> fw(params.ensembleName,
						   "Polyakov loop",
						   (string("conp") + params.confName),
						   params.cools,
						   0,
						   m_dataModel.GetPolyakovLoopField());

	cout << "file: " << polyakovFilename << " saved correctly: ";
	cout << (fw(polyakovFilename) ? "true" : "false") << endl;


	cout << "Magnetic field" << endl;
	//m_dataModel.GetMagneticField();

	cout << "Electric field" << endl;
	//m_dataModel.GetElectricField();

	//	Time check
	end = std::chrono::system_clock::now();
	std::chrono::duration<double> elapsed_seconds = end-start;
	std::time_t end_time = std::chrono::system_clock::to_time_t(end);

	std::cout << "finished computation at " << std::ctime(&end_time)
			  << "elapsed time: " << elapsed_seconds.count() << "s\n";
}

string MainWindow::generateFullOutputFilename(const QDir& outputBase,
											  const QString& ensembleName,
											  const QString& configurationName,
											  const size_type& cool,
											  const QString& variableName) const
{
	//	example: ..\config.b190k1680mu0400j02s16t08\conp0015\cool0005\average_plaquette.hvol1
	QDir outputDir = QDir(outputBase.absolutePath()
						  + "/" + ensembleName
						  + "/conp" + configurationName
						  + "/cool" + QString("%1").arg(cool, 4, 10, QChar('0'))
						  + "/");

	//	Make the directory if it doesn't exist
	if (!outputDir.exists()) outputDir.mkpath(".");

	return outputDir.absoluteFilePath(variableName).toStdString();
}

///
/// \brief		Returns a list of files checked by the user
/// \return
/// \since		26-10-2015
/// \author		Dean
///
MainWindow::FileList MainWindow::generateMarkedFileList() const
{
	FileList result;

	//	Convienience accessor
	auto model = ui->tableViewConfigurations->model();

	//	Iterate over the entire grid
	for (auto r = 0; r < model->rowCount(); ++r)
	{
		for (auto c = 0; c < model->columnCount(); ++c)
		{
			//	Access the cell
			auto cell = model->index(r, c);

			//	We only want configurations marked by the user
			if (cell.data(Qt::CheckStateRole) == Qt::Checked)
			{
				//	Access the file path
				auto filePath = cell.data(PATH_TO_FILE_INDEX).toString();

				//	And add to the result
				result.push_back(filePath);
			}
		}
	}
	return result;
}

///
/// \brief doPolyakovCalculation
/// \param dataModel
/// \param outFile
/// \return
/// \since		29-10-2015
/// \author		Dean
///
bool doPolyakovCalculation(const string& ensembleName,
						   const string& configurationName,
						   DataModel& dataModel,
						   const size_t cools,
						   const string& outFile)
{
	Vol1Writer<MainWindow::Decimal> fw(ensembleName,
									   "Polyakov loop",
									   configurationName,
									   cools,
									   0,
									   dataModel.GetPolyakovLoopField());

	return fw(outFile);
}

///
/// \brief doSpaceLikeCalculation
/// \param dataModel
/// \param outFile
/// \return
/// \since		29-10-2015
/// \author		Dean
///
bool doSpaceLikeCalculation(const string& ensembleName,
							const string& configurationName,
							DataModel& dataModel,
							const size_t& cools,
							const string& outFile)
{
	HVol1FileWriter fw(ensembleName,
					   "Spacelike Plaquette",
					   configurationName,
					   cools);

	return fw(outFile, dataModel.GetSpaceLikePlaquetteField());
}

///
/// \brief doTimeLikeCalculation
/// \param dataModel
/// \param outFile
/// \return
/// \since		29-10-2015
/// \author		Dean
///
bool doTimeLikeCalculation(const string& ensembleName,
						   const string& configurationName,
						   DataModel& dataModel,
						   const size_t& cools,
						   const string& outFile)
{
	HVol1FileWriter fw(ensembleName,
					   "Timelike Plaquette",
					   configurationName,
					   cools);

	return fw(outFile, dataModel.GetTimeLikePlaquetteField());
}

///
/// \brief doAveragePlaquetteCalculation
/// \param dataModel
/// \param outFile
/// \return
/// \since		29-10-2015
/// \author		Dean
///
bool doAveragePlaquetteCalculation(const string& ensembleName,
								   const string& configurationName,
								   DataModel& dataModel,
								   const size_t& cools,
								   const string& outFile)
{
	HVol1FileWriter fw(ensembleName,
					   "Average Plaquette",
					   configurationName,
					   cools);

	return fw(outFile, dataModel.GetAveragePlaquetteField());
}

///
/// \brief doDifferencePlaquetteCalculation
/// \param dataModel
/// \param outFile
/// \return
/// \since		29-10-2015
/// \author		Dean
///
bool doDifferencePlaquetteCalculation(const string& ensembleName,
									  const string& configurationName,
									  DataModel& dataModel,
									  const size_t& cools,
									  const string& outFile)
{
	HVol1FileWriter fw(ensembleName,
					   "Difference Plaquette",
					   configurationName,
					   cools);

	return fw(outFile, dataModel.GetDifferencePlaquetteField());
}

///
/// \brief doTopologicalChargeDensityCalculation
/// \param dataModel
/// \param outFile
/// \return
/// \since		29-10-2015
/// \author		Dean
///
bool doTopologicalChargeDensityCalculation(const string& ensembleName,
										   const string& configurationName,
										   DataModel& dataModel,
										   const size_t& cools,
										   const string& outFile)
{
	HVol1FileWriter fw(ensembleName,
					   "Topological charge density",
					   configurationName,
					   cools);

	return fw(outFile, dataModel.GetTopologicalChargeDensityField());
}


///
/// \brief MainWindow::generateFieldVariables
/// \param checkedFileList
/// \param spaceLikePlaquette
/// \param timeLikePlaquette
/// \param averagePlaquette
/// \param differencePlaquette
/// \param polyakovLoop
/// \param topologicalChargeDensity
/// \since		29-10-2015
/// \author		Dean
///
MainWindow::FileList MainWindow::generateFieldVariables(const FileList& checkedFileList,
														const bool &spaceLikePlaquette,
														const bool &timeLikePlaquette,
														const bool &averagePlaquette,
														const bool &differencePlaquette,
														const bool &polyakovLoop,
														const bool &topologicalChargeDensity)
{
	FileList globalFileList;

	//	TODO: accept a list of functions to complete

	auto fieldCount = 0;

	//	How many fields are to be calculated?
	if (spaceLikePlaquette) ++fieldCount;
	if (timeLikePlaquette) ++fieldCount;
	if (differencePlaquette) ++fieldCount;
	if (averagePlaquette) ++fieldCount;
	if (polyakovLoop) ++fieldCount;
	if (topologicalChargeDensity) ++fieldCount;

	auto operationCount = checkedFileList.size() * fieldCount;

	QProgressDialog progressDialog("Computing field variables",
								   QString("cancel"),
								   0,
								   operationCount,
								   this);
	progressDialog.setWindowModality(Qt::WindowModal);


	QFutureWatcher<FileList> futureWatcher;

	QObject::connect(&futureWatcher, SIGNAL(finished()), &progressDialog, SLOT(reset()));
	QObject::connect(&progressDialog, SIGNAL(canceled()), &futureWatcher, SLOT(cancel()));
	QObject::connect(&futureWatcher, SIGNAL(progressRangeChanged(int,int)), &progressDialog, SLOT(setRange(int,int)));
	QObject::connect(&futureWatcher, SIGNAL(progressValueChanged(int)), &progressDialog, SLOT(setValue(int)));

	//	Our main worker threads will return a list of the files it produces
	std::function<FileList(QString)> processFunctor =
			[&](QString inputFilename) -> FileList
	{
		FileList localFileList;

		ConfigurationParameters params = parseConfigurationParameters(inputFilename);

		//	Load in the configuration
		DataModel configuration(inputFilename.toStdString(),
								params.confName,
								params.cools,
								params.b,
								params.k,
								params.mu,
								params.j);

		//	Statically call each function of the lattice that is desired


		//	Polyakov loop
		if (polyakovLoop)
		{
			auto outputFilename = generateFullOutputFilename(m_outputDir,
															 QString::fromStdString(params.ensembleName),
															 QString::fromStdString(params.confName),
															 params.cools,
															 "polyakov.vol1");

			if (doPolyakovCalculation(params.ensembleName,
									  (string("conp") + params.confName),
									  configuration,
									  params.cools,
									  outputFilename))
			{
				localFileList.push_back(QString::fromStdString(outputFilename));
			}
		}

		if (spaceLikePlaquette)
		{
			auto outputFilename = generateFullOutputFilename(m_outputDir,
															 QString::fromStdString(params.ensembleName),
															 QString::fromStdString(params.confName),
															 params.cools,
															 "spacelike plaquette.hvol1");

			if (doSpaceLikeCalculation(params.ensembleName,
									   (string("conp") + params.confName),
									   configuration,
									   params.cools,
									   outputFilename))
			{
				localFileList.push_back(QString::fromStdString(outputFilename));
			}
		}

		if (timeLikePlaquette)
		{
			auto outputFilename = generateFullOutputFilename(m_outputDir,
															 QString::fromStdString(params.ensembleName),
															 QString::fromStdString(params.confName),
															 params.cools,
															 "timelike plaquette.hvol1");

			if (doTimeLikeCalculation(params.ensembleName,
									  (string("conp") + params.confName),
									  configuration,
									  params.cools,
									  outputFilename))
			{
				localFileList.push_back(QString::fromStdString(outputFilename));
			}
		}

		if (averagePlaquette)
		{
			auto outputFilename = generateFullOutputFilename(m_outputDir,
															 QString::fromStdString(params.ensembleName),
															 QString::fromStdString(params.confName),
															 params.cools,
															 "average plaquette.hvol1");

			if (doAveragePlaquetteCalculation(params.ensembleName,
											  (string("conp") + params.confName),
											  configuration,
											  params.cools,
											  outputFilename))
			{
				localFileList.push_back((QString::fromStdString(outputFilename)));
			}
		}

		if (differencePlaquette)
		{
			auto outputFilename = generateFullOutputFilename(m_outputDir,
															 QString::fromStdString(params.ensembleName),
															 QString::fromStdString(params.confName),
															 params.cools,
															 "difference plaquette.hvol1");

			if (doDifferencePlaquetteCalculation(params.ensembleName,
												 (string("conp") + params.confName),
												 configuration,
												 params.cools,
												 outputFilename))
			{
				localFileList.push_back((QString::fromStdString(outputFilename)));
			}
		}

		if (topologicalChargeDensity)
		{
			auto outputFilename = generateFullOutputFilename(m_outputDir,
															 QString::fromStdString(params.ensembleName),
															 QString::fromStdString(params.confName),
															 params.cools,
															 "topological charge density.hvol1");

			if (doTopologicalChargeDensityCalculation(params.ensembleName,
													  (string("conp") + params.confName),
													  configuration,
													  params.cools,
													  outputFilename))
			{
				localFileList.push_back((QString::fromStdString(outputFilename)));
			}
		}

		return localFileList;
	};

	//	Flatten the file list from each process into a single list for
	//	return
	auto reducer = [&](FileList& fileList, const FileList& subList)
	{
		for (auto it = subList.cbegin(); it != subList.cend(); ++it)
		{
			fileList.push_back(*it);
		}
	};

	//	Create a future that will produce a set of field array files for
	//	each input configuration
	auto future = QtConcurrent::mappedReduced<FileList>(checkedFileList.cbegin(),
														checkedFileList.cend(),
														processFunctor,
														reducer);

	//	Set the future and show the dialog
	futureWatcher.setFuture(future);
	progressDialog.exec();

	//	Dump the list of files produced to the console
	cout << "Dumping results" <<endl;
	/*
	for (auto it = future.begin(); it != future.end(); ++it)
	{
		for (auto it2 = it->cbegin(); it2 != it->cend(); ++it2)
		{
			cout << it2->toStdString() << endl;

			globalFileList.push_back(*it2);
		}
	}
	*/
	return globalFileList;
}

void MainWindow::execApplication(const QString& applicationFilename,
								 const FileList& files,
								 const QString& stdoutFile,
								 const QString& stderrFile)
{
	//	Make sure the application path is valid
	QFile execFile(applicationFilename);
	assert(execFile.exists());

	cout << "Executing QCDVis" << endl;

	QProcess *process = new QProcess(this);


	connect(process, static_cast<void(QProcess::*)(QProcess::ProcessError)>(&QProcess::error),
			[=](QProcess::ProcessError error)
	{
		std::cerr << "Error in external application:" << error << endl;
	});

	QStringList args;

	//	Qt will wrap the parameters in quotes
	args << "nogui";
	if (stderrFile != "") args << QString("stderr=%1").arg(stderrFile);
	if (stdoutFile != "") args << QString("stdout=%1").arg(stdoutFile);
	for (auto it = files.cbegin(); it != files.cend(); ++it)
	{
		QFile fieldFile(*it);
		if (fieldFile.exists())
		{
			//QString posixFilename = *it;
			//QString winFilename = posixFilename.replace("/", "\\\\");
			args << QString("infile=%1").arg(*it);
		}
		else
		{
			cerr << "Could not find file: " << it->toStdString()
				 << " - skipping." << endl;
		}
	}

	QStringList envr = QProcess::systemEnvironment();
	process->setEnvironment(envr);

	cout << "List QCDVis argument list:" << endl;
	for (auto it = args.cbegin(); it != args.cend(); ++it)
	{
		cout << it->toStdString() << endl;
	}

	auto result = process->execute(applicationFilename, args);

	//auto result = process->startDetached(applicationFilename, args, "C:\\Users\\4thFloor\\Documents\\Qt\\QtFlexibleIsosurfaces\\QTFlexibleIsosurfaces_Modified");
	//assert((result == true));

	cout << std::boolalpha << result << endl;
}

///
/// \brief MainWindow::on_pushButton_4_clicked
/// \since		29-10-2015
/// \author		Dean
///
void MainWindow::on_pushButton_4_clicked()
{
	auto checkedFileList = generateMarkedFileList();

	//	Take the whole file list for now (it might be a good idea to do the
	//	actual processing in batches of 10 or so)
	auto fileList = generateFieldVariables(checkedFileList,
										   ui->listWidget->item(SPACELIKE_INDEX)->checkState() == Qt::Checked,
										   ui->listWidget->item(TIMELIKE_INDEX)->checkState() == Qt::Checked,
										   ui->listWidget->item(AVERAGE_INDEX)->checkState() == Qt::Checked,
										   ui->listWidget->item(DIFFERENCE_INDEX)->checkState() == Qt::Checked,
										   ui->listWidget->item(POLYAKOV_INDEX)->checkState() == Qt::Checked,
										   ui->listWidget->item(TOPOLOGICAL_CHARGE_DENSITY_INDEX)->checkState() == Qt::Checked);

	cout << "Results returned from function" << endl;
	for (auto it = fileList.cbegin(); it != fileList.cend(); ++it)
	{
		cout << it->toStdString() << endl;
	}

	execApplication(QCDVIS_EXE,
					fileList,
					R"(C:\Users\4thFloor\Desktop\stdout.txt)",
					R"(C:\Users\4thFloor\Desktop\stderr.txt)");
}

bool MainWindow::executePythonHistogramScript(const QString& filename)
{
	QProcess *process = new QProcess(this);

	connect(process, static_cast<void(QProcess::*)(QProcess::ProcessError)>(&QProcess::error),
			[=](QProcess::ProcessError error)
	{
		std::cerr << "Error in external application:" << error << endl;
	});

	QStringList args;
	args << QString::fromStdString(HISTOGRAM_4D_PY);
	args << m_inputDir.absolutePath() << filename;

	//	Allow the python script to run in the background
	auto result = process->startDetached(PYTHON_EXE, args);

	cout << std::boolalpha << result << endl;

	return result;
}

void MainWindow::on_pushButtonViewAction_clicked()
{
	QProcess *process = new QProcess(this);

	connect(process, static_cast<void(QProcess::*)(QProcess::ProcessError)>(&QProcess::error),
			[=](QProcess::ProcessError error)
	{
		std::cerr << "Error in external application:" << error << endl;
	});

	QStringList args;
	args << QString::fromStdString(ACTION_SCRIPT_PY);
	args << m_inputDir.absolutePath() << m_currentEnsembleData.ensembleName;

	//	Allow the python script to run in the background
	auto result = process->startDetached(PYTHON_EXE, args);

	cout << std::boolalpha << result << endl;
}

void MainWindow::on_pushButtonViewFFdual_clicked()
{
	QProcess *process = new QProcess(this);

	connect(process, static_cast<void(QProcess::*)(QProcess::ProcessError)>(&QProcess::error),
			[=](QProcess::ProcessError error)
	{
		std::cerr << "Error in external application:" << error << endl;
	});

	QStringList args;
	args << QString::fromStdString(FFDUAL_SCRIPT_PY);
	args << m_inputDir.absolutePath() << m_currentEnsembleData.ensembleName;

	//	Allow the python script to run in the background
	auto result = process->startDetached(PYTHON_EXE, args);

	cout << std::boolalpha << result << endl;
}

///
/// \brief		Output field variables for specified variables
/// \since		26-10-2015
/// \author		Dean
///
void MainWindow::on_pushButtonGenerateFields_clicked()
{
	//	Get a list of files for processing
	auto checkedFileList = generateMarkedFileList();

	//	Do the processing
	generateFieldVariables(checkedFileList,
						   ui->listWidget->item(SPACELIKE_INDEX)->checkState() == Qt::Checked,
						   ui->listWidget->item(TIMELIKE_INDEX)->checkState() == Qt::Checked,
						   ui->listWidget->item(AVERAGE_INDEX)->checkState() == Qt::Checked,
						   ui->listWidget->item(DIFFERENCE_INDEX)->checkState() == Qt::Checked,
						   ui->listWidget->item(POLYAKOV_INDEX)->checkState() == Qt::Checked,
						   ui->listWidget->item(TOPOLOGICAL_CHARGE_DENSITY_INDEX)->checkState() == Qt::Checked);

	//	Show the output directory in the native file browser
	//	For windows explorer we need the absolute path as an argument
	QStringList args;
	args << QDir::toNativeSeparators(m_outputDir.absolutePath());

	//	Run the program externally
	QProcess process;
	process.setArguments(args);
	process.startDetached("explorer.exe",args);
}

///
/// \brief MainWindow::generateMinMaxTable
/// \param data
/// \return
/// \since		11-11-2015
/// \author		Dean
///
QStandardItemModel* MainWindow::generateMinMaxTable(const MinMaxFileParser::CoolingSliceFileList& data)
{
#define COL_SMAX 0
#define COL_QMAX 1
#define COL_QMIN 2

	//	Create our new model
	QStandardItemModel* result = new QStandardItemModel(this);

	//	Generate column headers ('conp0000', 'conp0001')
	QStringList columnHeaderLabels;
	columnHeaderLabels << "S_max" << "Q_max" << "Q_min";

	//	Generate row headers ('Uncooled', 'cool 0', 'cool 1')
	QStringList rowHeaderLabels;
	for (auto i = 0; i < data.size(); ++i)
	{
		if (i == 0)
		{
			rowHeaderLabels << "uncooled";
		}
		else
		{
			rowHeaderLabels << QString("cool %1").arg(i);
		}
	}

	//	Set the row/col headers
	result->setHorizontalHeaderLabels(columnHeaderLabels);
	result->setVerticalHeaderLabels(rowHeaderLabels);

	for (auto i = 0; i < data.size(); ++i)
	{
		Index4 sMax = data[i].posMaxS;
		Index4 qMax = data[i].posMaxQ;
		Index4 qMin = data[i].posMinQ;

		auto sMaxItem = new QStandardItem(QString("(%1, %2, %3, %4)")
										  .arg(sMax.x)
										  .arg(sMax.y)
										  .arg(sMax.z)
										  .arg(sMax.t));
		sMaxItem->setTextAlignment(Qt::AlignRight | Qt::AlignVCenter);
		sMaxItem->setEditable(false);

		auto qMaxItem = new QStandardItem(QString("(%1, %2, %3, %4)")
										  .arg(qMax.x)
										  .arg(qMax.y)
										  .arg(qMax.z)
										  .arg(qMax.t));
		qMaxItem->setTextAlignment(Qt::AlignRight | Qt::AlignVCenter);
		qMaxItem->setEditable(false);

		auto qMinItem = new QStandardItem(QString("(%1, %2, %3, %4)")
										  .arg(qMin.x)
										  .arg(qMin.y)
										  .arg(qMin.z)
										  .arg(qMin.t));
		qMinItem->setTextAlignment(Qt::AlignRight | Qt::AlignVCenter);
		qMinItem->setEditable(false);

		result->setItem(i, COL_SMAX, sMaxItem);
		result->setItem(i, COL_QMAX, qMaxItem);
		result->setItem(i, COL_QMIN, qMinItem);
	}

	return result;
}

///
/// \brief MainWindow::on_actionTestMinMaxParse_triggered
/// \since		11-11-2015
/// \author		Dean
///
void MainWindow::on_actionTestMinMaxParse_triggered()
{
	MinMaxFileParser minMaxParser("E:\\Dean\\QCD Data\\Cooled Configurations\\Edinburgh\\V16X32\\J02\\MU0700\\config.b210k1577mu0700j02s16t32\\maxima_conp0005.txt");

	auto minMaxListing = minMaxParser();
	auto tableModel = generateMinMaxTable(minMaxListing);

	ui->tableViewMinMax->setModel(tableModel);
	ui->tableViewMinMax->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
}

void MainWindow::on_groundStateCheckButton_clicked()
{

}

void MainWindow::on_pushButton_clicked()
{

}

void MainWindow::on_actionGroundStateCheck_triggered()
{
	const std::string FILENAME_BASE = R"(C:\Users\4thFloor\Desktop\)";

	const std::string POLYAKOV_FILE = FILENAME_BASE + "Polyakov.vol1";
	const std::string TIMELIKE_FILE = FILENAME_BASE + "Timelike plaquette.hvol1";
	const std::string SPACELIKE_FILE = FILENAME_BASE + "Spacelike plaquette.hvol1";
	const std::string DIFFERENCE_FILE = FILENAME_BASE + "Difference plaquette.hvol1";
	const std::string AVERAGE_FILE = FILENAME_BASE + "Average plaquette.hvol1";
	const std::string TCD_FILE = FILENAME_BASE + "Topological charge density.hvol1";

	DataModel m_lattice = DataModel::GenerateGroundStateLattice(10, 10, 10, 10);

	cout << "Spacelike plaquette" << endl
		 << m_lattice.GetSpaceLikePlaquetteField().ToString() << endl;
	cout << "Timelike plaquette" << endl
		 << m_lattice.GetTimeLikePlaquetteField().ToString() << endl;
	cout << "Average plaquette" << endl
		 << m_lattice.GetAveragePlaquetteField().ToString() << endl;
	cout << "Difference plaquette" << endl
		 << m_lattice.GetDifferencePlaquetteField().ToString() << endl;
	cout << "Plaquette averages: " << endl
		 <<	m_lattice.GetAveragePlaquetteMatrix().ToString() << endl;

	cout << "Saving ground state Polyakov loop to:" << POLYAKOV_FILE << " "
		 << (doSpaceLikeCalculation("Ground state",
									"Ground state",
									m_lattice,
									0,
									POLYAKOV_FILE) ? "Suceeded" : "Failed") << endl;

	cout << "Saving ground state Timelike plaquettes to:" << TIMELIKE_FILE << " "
		 << (doTimeLikeCalculation("Ground state",
								   "Ground state",
								   m_lattice,
								   0,
								   TIMELIKE_FILE) ? "Suceeded" : "Failed") << endl;

	cout << "Saving ground state Spacelike plaquettes to:" << SPACELIKE_FILE << " "
		 << (doSpaceLikeCalculation("Ground state",
									"Ground state",
									m_lattice,
									0,
									SPACELIKE_FILE) ? "Suceeded" : "Failed") << endl;

	cout << "Saving ground state Average plaquettes to:" << AVERAGE_FILE << " "
		 << (doAveragePlaquetteCalculation("Ground state",
										   "Ground state",
										   m_lattice,
										   0,
										   AVERAGE_FILE) ? "Suceeded" : "Failed") << endl;

	cout << "Saving ground state Difference plaquettes to:" << DIFFERENCE_FILE << " "
		 << (doDifferencePlaquetteCalculation("Ground state",
											  "Ground state",
											  m_lattice,
											  0,
											  DIFFERENCE_FILE) ? "Suceeded" : "Failed") << endl;

	cout << "Saving ground state Topological charge density to:" << TCD_FILE << " "
		 << (doTopologicalChargeDensityCalculation("Ground state",
												   "Ground state",
												   m_lattice,
												   0,
												   TCD_FILE) ? "Suceeded" : "Failed") << endl;
}
