#-------------------------------------------------
#
# Project created by QtCreator 2015-10-13T15:21:04
#
#-------------------------------------------------

QT       += core gui concurrent
CONFIG	 += c++11

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = QtLatticeParser
TEMPLATE = app

win32: INCLUDEPATH += "C:/Users/4thFloor/Documents/Qt/QtFlexibleIsosurfaces/Common Classes"
else: INCLUDEPATH += "/home/dean/qcdvis/Common Classes"

CONFIG(debug, release|debug):DEFINES += _DEBUG

QMAKE_CXXFLAGS	+= -fpermissive

SOURCES += main.cpp\
	MainWindow.cpp \
    LatticeParser/DataModel.cpp \
    LatticeParser/Lattice.cpp \
    LatticeParser/LatticeSite.cpp \
    LatticeParser/AveragePlaquetteMatrix.cpp \
    LatticeParser/parsersu2hmc.cpp

HEADERS  += MainWindow.h \
    LatticeParser/DataModel.h \
    LatticeParser/Lattice.h \
    LatticeParser/LatticeSite.h \
    LatticeParser/ComplexMatrix.h \
    LatticeParser/AbstractMatrix.h \
    LatticeParser/AveragePlaquetteMatrix.h \
    LatticeParser/DoubleMatrix.h \
    LatticeParser/parsersu2hmc.h \
    LatticeParser/FileFormatLookupTable.h

FORMS    += MainWindow.ui

DISTFILES += \
    FileFormats.txt

RESOURCES += \
    resource.qrc
